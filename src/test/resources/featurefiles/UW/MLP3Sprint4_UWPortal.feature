Feature: Regression suite for VMIA MLP3

  @RegressionMLP3_Inprogress
  Scenario Outline: Start a new policy application for GPA with deviation and update base premium in finalize quote
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I update base premium
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | product | updatedbasepremium | students | multiplier | gst | stampduty | startdate   | client_number | status                   | pendingstatus        | days | uwusername     | uwpassword  | paymentstatus   | notes       |
      | GPA     |               3000 |    15000 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |   98 | underwriter_AT | Welcome@321 | PENDING-PAYMENT | baseepremmq |

  @RegressionMLP3_Inprogress
  Scenario Outline: Start a new policy with Deviation and then endorse it for GPA
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    #And I click on Exit Button
    Then I logout of the application
    Given I have the URL of VMIA
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    #And I click on Exit Button
    When I search the case id that is routed and I begin the case
    Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I update custom premium
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I want to update a policy for GPA
    And Click UW Continue button
    Then value of students given "<updatedstudents>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application

    Examples: 
      | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status1              | status                   | days | paymentstatus   | endpaymentstatus | pendingstatus        | uwusername     | uwpassword  |
      | GPA     |    11000 |             500 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE |  365 | PENDING-PAYMENT | PENDING-REFUND   | PENDING-UNDERWRITING | underwriter_AT | Welcome@321 |

  @RegressionMLP3_Inprogress123
  Scenario Outline: verify the error msg  Delegation limit is not sufficient to approve the premium limit exceeds 25000 as clilent Adviser
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Then I close the external link
    Given I have the URL of VMIA
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I update base premium
    And I accept the quote for uw
    And I verify the message "<limitmsg>"
    Then I logout of the application

    Examples: 
      | ClientAdviser | product | students | startdate   | pendingstatus        | limitmsg                                                  | Queue_value  |
      | ClientAdviser | GPA     |    15000 | currentdate | PENDING-UNDERWRITING | Delegation limit is not sufficient to approve the premium | Underwriting |

  @RegressionMLP3_Inprogress123
  Scenario Outline: verify the error msg  Delegation limit is not sufficient to approve the premium limit exceeds 25000 as clilent Adviser
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Then I close the external link
    Given I have the URL of VMIA
    
    Then I update base premium
    And I accept the quote for uw
    And I verify the message "<limitmsg>"
    Then I logout of the application

    Examples: 
      | ClientAdviser | product | students | startdate   | pendingstatus        | limitmsg                                                  | Queue_value  |
      | ClientAdviser | GPA     |    15000 | currentdate | PENDING-UNDERWRITING | Delegation limit is not sufficient to approve the premium | Underwriting |
