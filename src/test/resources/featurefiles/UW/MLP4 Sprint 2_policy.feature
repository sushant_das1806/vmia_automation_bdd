Feature: Initiate full term cancellation and Partial

@new_policy_stp_fullcancel 
  Scenario Outline: Start a new policy application for property with STP and initaiate a Full term cancellation 
  
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    And I validate the "<status>"
    Then I accept the quote
     And Now select "<yes/no>" and enter "<purchaseno>"
    And Click on submit button
    And I verify the payment "<paymentstatus>"
    And I save the policy no
    And Search the policy and click on the moreactions and click on the cancel policy   
    And Click UW Continue button
    And Now select "<yes/no>"
    And Now select reason for cancel "<reasonforcancel>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And Now capture the policy ID
    Then Click on the user ID
    And Click on the Log off button
    Given I have the URL of VMIA
    And  I enter the "<uwID>"
    And I enter "<uwpassword>"
  	And  I click login button
    And Now search with the policy ID in the UW portal
    And Now click on the begin button
    And Now click on the approve quote
    And Click on submit button
    And I verify the final status for updation "<cancelstatus>"

Examples:
    | username                       | password      | uwID        |uwpassword  | cancelstatus         | reasonforcancel | yes/no | purchaseno | product | startdate   |startdatecancel | endpaymentstatus | paymentstatus   | name      | value          | propertyvalue | minimumvalue | percentage | gst | stampduty | startdate   | client_number | status                   | pendingstatus        | days | paymentstatus   |
    | pavan.gangone@areteanstech.com | Areteans@1981 | Underwriter | Welcome@321| PENDING-REFUND       | Property sold   | Yes     | MAN123     | PRO     | currentdate   | futuredate     | PENDING-PAYMENT  | PENDING-PAYMENT | Lowanna27 | Up to $250,000 |        100000 |          950 |          1 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |   98 | PENDING-PAYMENT |
    


   
    #@new_policy_stp_partialcancel 
  #Scenario Outline: Start a new policy application for property with STP and initaiate a partial cancellation 
    #Given I have the external URL of VMIA
    #When I enter the "<username>" for azure
    #And I enter "<password>" for azure
    #And I click login button for azure
    #And I Select the Organization "<name>"
    #Then I want to start a new application for VMIA insurance
    #When I select "<product>" and submit
    #And Click UW Continue button
    #Then I enter the "<startdate>" of the insurance application
    #And I select the property "<value>" as "<propertyvalue>"
    #And Click UW Continue button
    #And I validate the property value "<value>"
    #And I check the declaration
    #Then I Click Finish button
    #Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    #And I validate the "<status>"
    #Then I accept the quote
     #And Now select "<yes/no>" and enter "<purchaseno>"
    #And Click on submit button
    #And I verify the payment "<paymentstatus>"
    #And I save the policy no
    #And Search the policy and click on the moreactions and click on the cancel policy   
    #And Click UW Continue button
    #And Now select "<yes/no>"
    #And I enter the cancel date of policy "<startdatecancel>"
    #And Now select reason for cancel "<reasonforcancel>"
    #And Click UW Continue button
    #And I check the declaration
    #Then I Click Finish button
    #And Now capture the policy ID
    #Then Click on the user ID
#		And Click on the Log off button
#		Given I have the URL of VMIA
#		And  I enter the "<uwID>"
    #And I enter "<uwpassword>"
  #	And   I click login button
#		And Now search with the policy ID in the UW portal 
#		And Now click on the begin button
#		And Now click on the approve quote
#		And Click on submit button
#		And I verify the final status for updation "<cancelstatus>"
#		
#		
    #
#
#Examples:
    #| username                       | password      | uwID        |uwpassword  | cancelstatus         | reasonforcancel | yes/no | purchaseno | product | startdate   |startdatecancel | endpaymentstatus | paymentstatus   | name      | value          | propertyvalue | minimumvalue | percentage | gst | stampduty | startdate   | client_number | status                   | pendingstatus        | days | paymentstatus   |
   #| pavan.gangone@areteanstech.com | Areteans@1981 | Underwriter | Welcome@321| PENDING-REFUND       | Property sold   | No    | MAN123     | PRO     | pastdate   | currentdate    | PENDING-PAYMENT  | PENDING-PAYMENT | Lowanna27 | Up to $250,000 |        100000 |          950 |          1 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |   98 | PENDING-PAYMENT |
    #| pavan.gangone@areteanstech.com | Areteans@1981 | Underwriter | Welcome@321| PENDING-REFUND       | Property sold   | No    | MAN123     | PRO     | currentdate   | futuredate     | PENDING-PAYMENT  | PENDING-PAYMENT | Lowanna27 | Up to $250,000 |        100000 |          950 |          1 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |   98 | PENDING-PAYMENT |
 #
 
 
 @MLP4_US_15127_FAES_cancel
  Scenario Outline: Cancellation a new Fine Arts Exhibitions - Static    
    
    
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    And Click UW Continue button
    And Add Testdocument attachment "<Attachment >"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID 
    And I validate the "<status>"
    Then I logout of the application
   
    Given I have the URL of VMIA
    And  I enter the "<RiskAdvisor>"
    And I enter "<RKpassword>"
    And I click login button
    And Now search the policy no
    And Now click on the case and begin
    And Now select "<yes/no>"
    And Click on submit button
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal

    Given I have the URL of VMIA
    And  I enter the "<uwID>"
    And I enter "<uwpassword>"
    And   I click login button
    And Now fetch the policy ID in the UW portal
    And Now click on the begin button
    And  Click on the Create Qoute
    And Enter the "<Annual Permium>" and "<Underwriting Notes>"
   	And I submit the case to get routed to the user
    Then I accept the quote
    And Click on submit button
     And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    And I open the approved case
    Then I accept the quote
    And Now select "<Yes/No>" and enter "<purchaseno>"
    And Click on submit button
    And I validate the "<finalstatus>"
    And I save the policy no
    And Search the policy and click on the moreactions and click on the cancel policy   
    And Click UW Continue button
    And Now select "<Yes/No>"
    And Now select reason for cancel "<reasonforcancel>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And Now capture the policy ID
    Then Click on the user ID
    And Click on the Log off button
    Given I have the URL of VMIA
    And  I enter the "<uwID>"
    And I enter "<uwpassword>"
  	And   I click login button
    And Now search with the policy ID in the UW portal 
    And Now click on the begin button
    And Now click on the approve quote
    And Click on submit button
    And I verify the final status for updation "<cancelstatus>"
    
    
     Examples: 
      | RiskAdvisor | RKpassword | yes/no | uwID        | uwpassword  | Annual Permium | Underwriting Notes | username                       | password      | name              |product | Attachment                                                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |cancelstatus         | reasonforcancel |
      | RiskAdvisor | rules      | No     | Underwriter | Welcome@321 |           10.0 | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | AutoB&I           | FAES   | C:\Users\Mani Yadav Darga\Downloads\testdocumentpdf_20210430t100524762gmt.pdf | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    | PENDING-REFUND      | Property sold  |

