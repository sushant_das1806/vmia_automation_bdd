Feature: Create New Application Case

  @Sprint3 @UW @MSC @MSCNoDeviation
  Scenario Outline: Start a new policy application for GPA with no deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for "<parkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | parkingslots | multiplier | gst | stampduty | startdate   | status                   | days | paymentstatus   |
      | Operator01 | abhinav@1234 | MSC     |          400 |      11.98 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |   34 | PENDING-PAYMENT |

  #| Operator01 | abhinav@1234 | MSC     |          9999 |      11.98 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |   34 | PENDING-PAYMENT |
  @Sprint3 @UW @MSC @MSCWithDeviation
  Scenario Outline: Start a new policy application for GPA with no deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I validate the premium for "<parkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I open the approved case
    And I validate the "<status>"
    #Then I validate the premium for "<students>" for "<days>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | uwusername  | uwpassword  | product | parkingslots | multiplier | gst | stampduty | startdate | status                   | days | paymentstatus   | pendingstatus        |
      | Operator01 | abhinav@1234 | underwriter | Welcome@321 | MSC     |          400 |      11.98 | 0.1 |       0.1 | 1/07/2019 | PENDING-CLIENTACCEPTANCE |  365 | PENDING-PAYMENT | PENDING-UNDERWRITING |

  @Sprint3 @UW @MV @MVWithDeviation
  Scenario Outline: Start a new policy application for Motor Vehicle with deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the risk page "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And Click UW Continue button
    #And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I individually provide the "<basepremium>" and "<notes>"
    Then I validate the premium for MV for "<basepremium>" "<gst>" "<stampduty>" "<days>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | uwusername  | uwpassword  | product | basepremium | notes        | gst | stampduty | startdate   | status                   | days | pendingstatus        | paymentstatus   | lightvehicleflag | busflag | trailerflag | otherflag | typeofvehicle | manufactureyear | make | model | regno | currentmarketvalue | numberofvehicles |
      | Operator01 | abhinav@1234 | underwriter | Welcome@321 | MVF     |        1000 | underwritten | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |   30 | PENDING-UNDERWRITING | PENDING-PAYMENT | true             | false   | false       | true      | SUV           |            2011 | ABC  | n97   |  8798 |              40000 |                1 |

  @Sprint3 @UW @MV @MVWithoutDeviation
  Scenario Outline: Start a new policy application for Motor Vehicle with deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the risk page "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And Click UW Continue button
    #And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for MV "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | parkingslots | multiplier | gst | stampduty | startdate   | status                   | days | paymentstatus   | lightvehicleflag | busflag | trailerflag | otherflag | typeofvehicle | manufactureyear | make | model | regno | currentmarketvalue | numberofvehicles | pricelightvehicle | pricebusunder50 | pricebusover50 | pricetrailer |
      | Operator01 | abhinav@1234 | MVF     |          400 |      11.98 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |   30 | PENDING-PAYMENT | true             | true    | true        | false     | SUV           |            2011 | ABC  | n97   |  8798 |              40000 |                1 |               450 |             620 |         891.78 |           50 |
