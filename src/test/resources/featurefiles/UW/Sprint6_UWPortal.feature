Feature: Prod bugs and new features of spr-6 


@ggttyoopp_1 @us_146
Scenario Outline: Start a new policy application  for property and Make Endorsement under client Adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    #Then I enter the Start Date  of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    And I validate the "<status>"
    Then I accept the quote
    # And I verify the payment "<paymentstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
     And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<updatedvalue>" as "<propertyvalue>"
    And I do not want to change anymore values
    And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for property "<propertyvalue>" "<minimumvalue>" "<updatedminimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logoff from client adviser portal
  

     Examples: 
      | username          | password       | product | value          | updatedvalue   | propertyvalue | minimumvalue | updatedminimumvalue | percentage | gst | stampduty | startdate                    | client_number | status                   | pendingstatus        | days | uwusername     | uwpassword  | paymentstatus   | endpaymentstatus |
      | AutoClientAdviser | Welcome@1234   | PRO     | Up to $250,000 | Up to $500,000 |        100000 |          950 |                1500 |          1 | 0.1 |       0.1 | currentdate                   | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter_AT | Welcome@321 | PENDING-PAYMENT | PENDING-PAYMENT  |                       
      
      
      
     @ggttyoopp_1 @us_146
    Scenario Outline: Start a new policy and then endorse it for GPA in client adviser portal and make two continuos Endorsement from client Adviser portal only 
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    # And I provide some "<action>"
    # Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    #  And I verify the payment "<paymentstatus>"
    And I verify the payment status "<paymentstatus>"
    # And I save the policy number
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
    #Then I want to start a new application for client adviser
    And Click UW Continue button
    Then I enter number of "<updatedstudents>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    #Then New endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
    #Then I want to start a new application for client adviser
    And Click UW Continue button
    Then I enter number of "<updatedstudents>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    #Then New endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus_1>"
    Then I logoff from client adviser portal

    @GPAEndorsement @Regression @w5 @regb
    Examples: 
      | username          | password           | product | students | updatedstudents | multiplier | gst | stampduty | startdate    | client_number | status                   | days | paymentstatus   | endpaymentstatus |endpaymentstatus_1|
      | AutoClientAdviser | Welcome@1234       | GPA     |      200 |             500 |        5.5 | 0.1 |       0.1 | currentdate  | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |  195 | PENDING-PAYMENT | PENDING-PAYMENT  |RESOLVED-COMPLETED |
      
      
      
     @ggttyoopp_1 @us_146
      Scenario Outline: Start a new policy application for MSC in client adviser with no deviation and make one Endorsement
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    # Then I enter the number of "<parkingslots>" and "<startdate>"
    Then I enter the number of "<parkingslots>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
   # Then I validate the premium for "<parkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    #And I verify the payment "<paymentstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
    And Click UW Continue button
    Then I enter the number of "<updatedparkingslots>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
 #   Then I validate the endorsement premium for "<parkingslots>" "<updatedparkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logoff from client adviser portal
 Examples: 
      | username                         | password      | product | parkingslots | updatedparkingslots | multiplier | gst | stampduty | startdate   | status                   | days | endpaymentstatus   | paymentstatus   |
      | AutoClientAdviser                | Welcome@1234  | MSC     |          400 |                 500 |         12 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |  195  | PENDING-PAYMENT    | PENDING-PAYMENT |
      
      @ggttyoopp_1 @us_146
      
     Scenario Outline: Start a new policy application for Motor Vehicle in client adviser without deviation and make one endorsement 
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    #Then I enter the Start Date  of the insurance application
    Then I fill up the MV details in the risk page "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And Click UW Continue button
    #And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    #  Then I validate the premium for MV "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    #And I verify the payment "<paymentstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    #Then I enter the Start Date  of the insurance application
    Then I fill up the MV details in the endorsement risk page "<Endorsedlightvehicleflag>" "<Endorsedbusflag>" "<Endorsedtrailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And I do not want to change anymore values
    #And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for MV "<Endorsedlightvehicleflag>" "<Endorsedbusflag>" "<Endorsedtrailerflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logoff from client adviser portal

    @MVEndorsement @Regression @regb
    Examples: 
      | username                         | password      | product | parkingslots | multiplier | gst | stampduty | startdate   | status                   | days | endpaymentstatus | paymentstatus   | lightvehicleflag | Endorsedlightvehicleflag | busflag | Endorsedbusflag | trailerflag | Endorsedtrailerflag | otherflag | typeofvehicle | manufactureyear | make | model | regno | currentmarketvalue | numberofvehicles | pricelightvehicle | pricebusunder50 | pricebusover50 | pricetrailer |  |
      |  AutoClientAdviser               | Welcome@1234  | MVF     |          400 |      11.98 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |  208 | PENDING-PAYMENT  | PENDING-PAYMENT | true             | false                    | true    | false           | false       | true                | false     | SUV           |            2011 | ABC  | n97   |  8798 |              40000 |                1 |               450 |             620 |         891.78 |           50 |  |
      
      
    @ggttyoopp_1 @us_146 
      Scenario Outline: Start a new policy and then endorse it with negative premium for GPA in client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    # And I provide some "<action>"
    # Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    #  And I verify the payment "<paymentstatus>"
    And I verify the payment status "<paymentstatus>"
    # And I save the policy number
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
    #Then I want to start a new application for client adviser
    And Click UW Continue button
    Then I enter number of "<updatedstudents>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    #Then New endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logoff from client adviser portal

    @GPAEndorsement @Regression @w5 @regb
    Examples: 
      | username          | password          | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   | endpaymentstatus |
      | AutoClientAdviser | Welcome@1234      | GPA     |      500 |             200 |        5.5 | 0.1 |       0.1 | currentdate   | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |  194 | PENDING-PAYMENT | PENDING-REFUND  |   
      
      
      
      
  @gpa_uw_full_po  @us_146
  Scenario Outline: Initiate a new application for GPA with full year deviation and complete the uw process under the client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    #Then I am logged in to the insurance portal
   Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    #Then I check the status to be "<pendingstatus>"
    And I verify the payment status "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
     And I Approve and submit the case 
     Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
     Then I accept the quote
     And I verify the payment status "<paymentstatus>"
     Then I logoff from client adviser portal
    
    
    Examples: 
      | username          | password          | product | students | updatedstudents | multiplier | gst | stampduty | startdate    | client_number | status                   | days | pendingstatus         | endpaymentstatus |Queue_value | paymentstatus |
      | AutoClientAdviser |Welcome@1234       | GPA     |      500 |             200 |        5.5 | 0.1 |       0.1 | 01/07/2020  | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |  365 | PENDING-UNDERWRITING   | PENDING-REFUND  | Underwriting |PENDING-PAYMENT|
    
      @gpa_uw   @us_146
    Scenario Outline: Start a new policy application for MSC in client adviser with deviation and complete the underwritting process by applying uw filter under the client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    # Then I enter the number of "<parkingslots>" and "<startdate>"
     Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<pendingstatus>"
    Then I logoff from client adviser portal
     Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
     And I Approve and submit the case 
    # Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
     Then I accept the quote
     And I verify the payment status "<paymentstatus>"
     Then I logoff from client adviser portal
     
     
      Examples: 
      | username                         | password      | product | parkingslots | updatedparkingslots | multiplier | gst | stampduty | startdate   | status                   | days | endpaymentstatus   | paymentstatus   |pendingstatus       |Queue_value |
      | AutoClientAdviser                | Welcome@1234  | MSC     |          400 |                 500 |         12 | 0.1 |       0.1 | 1/07/2020 | PENDING-CLIENTACCEPTANCE   |  365  | PENDING-PAYMENT    | PENDING-PAYMENT |PENDING-UNDERWRITING|Underwriting | 
      
      
       @msc_uw_Endo @us_146
    Scenario Outline: Start a new policy application for MSC in client adviser with deviation and complete the underwritting process by applying uw filter  then make one endorsement that is also in client adviser only Us-146
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    # Then I enter the number of "<parkingslots>" and "<startdate>"
     Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<pendingstatus>"
    Then I logoff from client adviser portal
     Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
     And I Approve and submit the case 
    # Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
     Then I accept the quote
     And I verify the payment status "<paymentstatus>"
     Then I logoff from client adviser portal
     Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
    And Click UW Continue button
    Then I enter the number of "<updatedparkingslots>" and "<endo_startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
 #   Then I validate the endorsement premium for "<parkingslots>" "<updatedparkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    #And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logoff from client adviser portal
     
     
      Examples: 
      | username                         | password      | product | parkingslots | updatedparkingslots | multiplier | gst | stampduty | startdate   | status                   | days | endpaymentstatus   | paymentstatus   |pendingstatus       |Queue_value |endo_startdate|
      | AutoClientAdviser                | Welcome@1234  | MSC     |          400 |                 500 |         12 | 0.1 |       0.1 | 1/07/2020 | PENDING-CLIENTACCEPTANCE   |  365  | PENDING-PAYMENT    | PENDING-PAYMENT |PENDING-UNDERWRITING|Underwriting|currentdate  |
      
      
  @liabiity_Endo @us_146
  Scenario Outline: Start a new policy application for Liability  with no deviation and make endorsement under clientadviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    # Then I enter the "<startdate>" of the insurance application
    Then I enter the Start Date  of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    #And I verify the payment "<paymentstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
     And Click UW Continue button
    # Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<updatedhiredays>"
    And I do not want to change anymore values
    #And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
   # Then I validate the endorsement premium for liability for "<basepremium>" "<updatedbasepremium>" "<gst>" "<stampduty>"
    #And Negative endorsement"<basepremium>" "<updatedbasepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
   Then I logoff from client adviser portal

    @ClientAdviserPPLUW @jj
   
      
       Examples: 
      | username                         | password      | product | basepremium | updatedbasepremium | paymentstatus   | endpaymentstatus | updatedhiredays       | hiredays          | gst | stampduty | startdate    | client_number | status                   |
      | AutoClientAdviser                 | Welcome@1234 | PPL     |         320 |                610 | PENDING-PAYMENT | PENDING-PAYMENT   | 50 to 100 days        | Less than 50 days | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |
      
      
      
  @liabiity_Endo_Refund @us_146
  Scenario Outline: Start a new policy application for Liability  with no deviation and make endorsement with negative premium under clientadviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    # Then I enter the "<startdate>" of the insurance application
    Then I enter the Start Date  of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    #And I verify the payment "<paymentstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
     And Click UW Continue button
    # Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<updatedhiredays>"
    And I do not want to change anymore values
    #And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
   # Then I validate the endorsement premium for liability for "<basepremium>" "<updatedbasepremium>" "<gst>" "<stampduty>"
   # And Negative endorsement"<basepremium>" "<updatedbasepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
   Then I logoff from client adviser portal

    @ClientAdviserPPLUW @jj
   
      
       Examples: 
      | username                         | password      | product | basepremium | updatedbasepremium | paymentstatus   | endpaymentstatus | updatedhiredays       | hiredays          | gst | stampduty | startdate    | client_number | status                   |
      | AutoClientAdviser                | Welcome@1234  | PPL     |         320 |                610 | PENDING-PAYMENT | PENDING-REFUND   | Less than 50 days     |  50 to 100 days   | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |
      
      
       @Pro_dev @us_146 @check28122
    Scenario Outline: Start a new policy application from client adviser for property with deviation and complete the uw process in client Adviser portal 
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    #Then I enter the Start Date  of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>" 
    Then I accept the quote
     And I verify the payment status "<paymentstatus>"
     Then I logoff from client adviser portal


    Examples: 
      | username          | password     | product | value          | propertyvalue | minimumvalue | percentage | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername     | uwpassword  | paymentstatus   |Queue_value |
      | AutoClientAdviser | Welcome@1234 | PRO     | Up to $250,000 |        100000 |          950 |          1 | 0.1 |       0.1 | 1/07/2020 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter_AT | Welcome@321 | PENDING-PAYMENT |Underwriting |
       
     
     
     @Pro_dev_End @us_146 @check28122
    Scenario Outline: Start a new policy application from client adviser for property with deviation and complete the uw process  and make one endorsement that is also in client adviser portal only 
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    #Then I enter the Start Date  of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>" 
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
     And Click UW Continue button
    Then I enter the "<u_startdate>" of the insurance application
    And I select the property "<updatedvalue>" as "<propertyvalue>"
    And I do not want to change anymore values
    And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for property "<propertyvalue>" "<minimumvalue>" "<updatedminimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    Then I accept the quote
   # And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logoff from client adviser portal


   Examples: 
      | username          | password       | product | value          | updatedvalue   | propertyvalue | minimumvalue | updatedminimumvalue | percentage | gst | stampduty | startdate  | client_number | status                   | pendingstatus        | days | uwusername     | uwpassword  | paymentstatus   | endpaymentstatus |Queue_value |u_startdate |
      | AutoClientAdviser | Welcome@1234   | PRO     | Up to $250,000 | Up to $500,000 |        100000 |          950 |                1500 |          1 | 0.1 |       0.1 | 01/07/2020 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter_AT | Welcome@321 | PENDING-PAYMENT | PENDING-PAYMENT  |Underwriting|currentdate | 
      
      