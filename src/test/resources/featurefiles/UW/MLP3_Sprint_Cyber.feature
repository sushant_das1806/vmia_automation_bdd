Feature: Regression suite for VMIA MLP3

  Scenario Outline: Start a new policy application for CYBR with deviation
  Given I have the external URL of VMIA
  When User enter valid username  "<username>" and password "<password>" click on Login button
  And I Select the Organization "<OrgName>"
  Then I want to start a new application for VMIA insurance
  When I select "<product>" and submit
  Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
  And Click UW Continue button
  Then I enter the revenue details "<Privious>" "<curent>"
  And Click Continue button
  Then I enter details in the Data protection "<Bywhom>" "<provideDetails>" Screen
  And Click Continue button
  And I enter the details in Data access and recovery Screen
  And Click Continue button
  And I enter the details in  Outsourcing activities Screen
  And Click Continue button
  And I enter the details Claims information Screen
  And Click Continue button
  And I check the declaration
  Then I Click Finish button
  Then I logout of the application
  Then I close the external link
  Given I have the URL of VMIA
  When I enter the "<uwusername>"
  And I enter "<uwpassword>"
  And I click login button
  When I search the case id that is routed and I begin the case
  And I accept the quote for uw
  Then I submit the case to get routed to the user
  Then I logout of the application
  Then I close the external link
  Given I have the external URL of VMIA
  When I enter the "<username>" for azure
  And I enter "<password>" for azure
  And I click login button for azure
  And I Select the Organization "<name>"
  Then I open the approved case
  And I validate the "<status>"
  Then I accept the quote
  And I verify the payment "<paymentstatus>"
  Then I logout of the application
  
  Examples:
  | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |
  | CYBR    |             100 |            200 | ect    | testing         |



  Scenario Outline: Property Risk Information-Insurance
  Given I have the external URL of VMIA
  When User enter valid username  "<username>" and password "<password>" click on Login button
  And I Select the Organization "<OrgName>"
  Then I want to start a new application for VMIA insurance
  When I select "<product>" and submit
  Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
  And Click UW Continue button
  And I provide a schedule of "<StreetNumber>" "<StreetName>""<suburb>" "<pindcode>""<State>""<country>"assets the organisation
  And I provide details facility "<Facilitytype>" "<NameDescriptionofasset>" "<BuildingValue>" "<BuildingValueType>" "<ContentsValue>" "<ContentsValueType>"
  And I provide details consequential financial loss cover"<Totalannualgross >" "<Estimatedincreasedcover>"
  And I provide preferred deductible to apply in the event of a claim "<preferreddeductible>"
  And I check the declaration
  Then I Click Finish button
  Then I logout of the application
  Given I have the URL of VMIA
  When User when logs in as "<Underwriter>"
  And I click login button
  When I search the case id that is routed and I begin the case
  And I accept the quote for uw
  Then I submit the case to get routed to the user
  Then I logout of the application
  Then I close the external link
  Given I have the external URL of VMIA
  When User enter valid username  "<username>" and password "<password>" click on Login button
  And I validate the "<status>"
  Then I accept the quote
  And I verify the payment "PENDING-PAYMENT"
  Then I logout of the application
  
  Examples:
  | Underwriter | product | StreetNumber | StreetName | suburb | pindcode | State    | country   | Facilitytype | NameDescriptionofasset | BuildingValue | BuildingValueType | ContentsValue | ContentsValueType | Totalannualgross | Estimatedincreasedcover | preferreddeductible |
  | Underwriter | pro     |         1234 | Street1    | hyd    |     5001 | Victoria | AUSTRALIA | Bore         | Test1                  |            10 | Replacement       |             1 | Replacement       |               10 |                      20 | $1000               |
 

  Scenario Outline: InitaCombined liability with  deviation
   
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    And User enters the required details "<Operationalrevenue>"	"<VictorGovfunding>" "<Otherfunding>" for Public and products liability page
    And Click UW Continue button
    And User enters the required details for Incidents and potential claims
    And Click UW Continue button
    And User enters the required details for Interstate or overseas work
    And Click UW Continue button
    And User enters the required details for Affliation
    And Click UW Continue button
    And User enters the required details "<ProtectionLicense>" "<yourpremises>"for Storage of hazardous substances or dangerous goods
    And Click UW Continue button
    And User enters the required details for Airfield liability
    And Click UW Continue button
    And User enters the required details for Product liability
    And Click UW Continue button
    And User enters the required details for Contractors Sub contractors
    And Click UW Continue button
    And User enters the required details "<nofvisitors>" "<UnitLevel>" "<SouthWales>" "<StreetNumber>" "<StreetName>" "<Suburb>" "<State>" "<Postcode>" "<Country>"for Visitors
    And Click UW Continue button
    And User enters the required details "<Qualifiedstaff>" for General information
    And Click UW Continue button
    And User enters the required details for Insurance history
    And Click UW Continue button
    And User enters the required details "<proact>""<Natureofwork	>""<percentage>"for Professional activities
    And Click UW Continue button
    And User enters the required details for Joint ventures
    And Click UW Continue button
    And User enters the required details for Overseas work
    And Click UW Continue button
    And User enters the required details for Fee income
    And Click UW Continue button
    And User enters the required details "< potentialindemnities>""<grossprofessionalfees>" for Contractors and sub contractors
    And Click UW Continue button
    And User enters the required details for Risk management
    And Click UW Continue button
    And User enters the required details for Claims and circumstances
    And Click UW Continue button
    And User enters the required details for Summary and review
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<Underwriter>"
    And I click login button
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "PENDING-PAYMENT"
    Then I logout of the application

    Examples: 
      | Underwriter | product | Operationalrevenue | VictorGovfunding | Otherfunding | ProtectionLicense                            | yourpremises                     | nofvisitors | UnitLevel | StreetNumber | StreetName | Suburb | State           | Postcode | Country   | Qualifiedstaff | proact | Natureofwork | percentage | potentialindemnities | grossprofessionalfees |
      | Underwriter | CBL     |                  3 |                4 |            5 | Storage, transportation, handling and/or use of chemical or fuel in bulk | Contaminated soil or groundwater |           3 |         1 | test123      | abc        | hyd    | South Australia |     1234 | AUSTRALIA |              3 | test   | test1        |         10 | test1                |                   100 |
