Feature: Create New Application Case

  @MLP3Sprint1 @UW @JRN @PropAndPlan @NewApplication 
  Scenario Outline: Start a new Journey policy for Property and Planning client with no deviation and then endorse it - Client Portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Prop and Planning Organisation
    Then I select update policy from more actions
    Then I enter updated values of "<updatedfte>" and "<startdate>"
    And I select no previous journey insurance
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium of Journey for "<fte>" "<updatedfte>" for "<startdate>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @JRNEndorsement
    Examples: 
      | username                     | password        | orgname          | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | venugopal.kadiri@cigniti.com | vmiatesting@123 | Automation PAP 2 | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  @MLP3Sprint1 @UW @JRN @PropAndPlan @NewApplication @Test2
  Scenario Outline: Start a new Journey policy for Property and Planning client with deviation through Client portal and then endorse it through Client Adviser Portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    Then I logout of the application
    ##### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    ##### Client Adviser - Endorsement
    Given I have the URL of VMIA
    When I enter the "<clientadviserusername>"
    And I enter "<clientadviserpassword>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I search and select org using "<orgname>" and "<zipcode>"
    Then I select the policy
    And Click UW Continue button
    Then I enter updated values of "<updatedfte>" and "<startdate>"
    And I select no previous journey insurance
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<UWStatus>"
    And I note down the case ID
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then I validate the endorsement premium of Journey for "<fte>" "<updatedfte>" for "<startdate>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logoff from client adviser portal

    @JRNEndorsement
    Examples: 
      | username                     | password        | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | venugopal.kadiri@cigniti.com | vmiatesting@123 | Automation PAP 2 | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_AT | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |

  @MLP3Sprint1 @UW @JRN @PropAndPlan @NewApplication @Cancellation
  Scenario Outline: Start a new Journey policy for Property and Planning client with no deviation and then cancel it - Client Portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Prop and Planning Organisation
    Then I cancel a policy from more actions
    Then I check the case status to be "<UWStatus>"
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile
    Then I logout of the application

    @JRNCancellation
    Examples: 
      | username                     | password        | orgname          | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      | venugopal.kadiri@cigniti.com | vmiatesting@123 | Automation PAP 2 | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |

  @MLP3Sprint1 @UW @GPA @PropAndPlan @NewApplication 
  Scenario Outline: Start a new GPA policy for Property and Planning client with no deviation and then endorse it - Client Portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and details of persons "<volunteers>"
    And I select No to previous GPA policy details
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Prop and Planning Organisation
    Then I select update policy from more actions
    Then I enter updated values of PnP GPA "<startdate>" and details of persons "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>"
    And I select No to previous GPA policy details
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium of PnP GPA for "<volunteers>" "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>" from "<startdate>" using values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @GPAEndorsement
    Examples: 
      | username                     | password        | orgname          | product | volunteers | updatedvolunteers | boardMembers | workExperienceStudents | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | venugopal.kadiri@cigniti.com | vmiatesting@123 | Automation PAP 2 | GPA     |       2000 |              6000 |           50 |                   5000 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  @MLP3Sprint1 @UW @GPA @PropAndPlan @NewApplication @Test2
  Scenario Outline: Start a new GPA policy for Property and Planning client with deviation and then endorse it through Client Adviser portal - Client Portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and details of persons "<volunteers>"
    And I select No to previous GPA policy details
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    Then I logout of the application
    ##### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    ##### Client Adviser - Apply Endorsement and accept quote
    Given I have the URL of VMIA
    When I enter the "<clientadviserusername>"
    And I enter "<clientadviserpassword>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I search and select org using "<orgname>" and "<zipcode>"
    Then I select the policy
    And Click UW Continue button
    Then I enter updated values of PnP GPA "<startdate>" and details of persons "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>"
    And I select No to previous GPA policy details
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<UWStatus>"
    And I note down the case ID
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then I validate the endorsement premium of PnP GPA for "<volunteers>" "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>" from "<startdate>" using values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logoff from client adviser portal

    @GPAEndorsement
    Examples: 
      | username                     | password        | orgname          | product | volunteers | updatedvolunteers | boardMembers | workExperienceStudents | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | venugopal.kadiri@cigniti.com | vmiatesting@123 | Automation PAP 2 | GPA     |     101000 |            106000 |           50 |                   5000 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_AT | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |

  @MLP3Sprint1 @UW @GPA @PropAndPlan @NewApplication @Cancellation 
  Scenario Outline: Start a new GPA policy for Property and Planning client with no deviation and then Cancel it - Client Portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and details of persons "<volunteers>"
    And I select No to previous GPA policy details
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Prop and Planning Organisation
    Then I cancel a policy from more actions
    Then I check the case status to be "<UWStatus>"
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile
    Then I logout of the application

    @GPACancellation
    Examples: 
      | username                     | password        | orgname                   | product | volunteers | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      | venugopal.kadiri@cigniti.com | vmiatesting@123 | Automation Cemetery Trust | GPA     |       2000 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |

  #### New Business Travel application with minimum premium case
  @MLP3Sprint1 @UW @BTV @PropAndPlan @NewApplication 
  Scenario Outline: Start a new BTV policy for Property and Planning client with no deviation and then endorse it - Client Portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I enter International travel "<InternationalTrips>" details with NO travel to listed countries
    Then I enter Domestic travel "<DomesticTrips>" details
    Then I select No to travel exceeding 180 days
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Prop and Planning Organisation
    Then I select update policy from more actions
    Then I enter the start date of insurance "<startdate>"
    Then I enter International travel "<UpdatedInternationalTrips>" details with NO travel to listed countries
    Then I enter Domestic travel "<UpdatedDomesticTrips>" details
    Then I select No to travel exceeding 180 days
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<UpdatedInternationalTrips>" "<UpdatedDomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @BTVEndorsement
    Examples: 
      | username                     | password        | orgname          | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
      | venugopal.kadiri@cigniti.com | vmiatesting@123 | Automation PAP 2 | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |

  #### New Business Travel application with minimum premium case and endorse to exceed minimum premium
  @MLP3Sprint1 @UW @BTV @PropAndPlan @NewApplication @Test2
  Scenario Outline: Start a new BTV policy for Property and Planning client with deviation and then endorse it through Client Adviser portal - Client Portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I enter International travel "<InternationalTrips>" details with NO travel to listed countries
    Then I enter Domestic travel "<DomesticTrips>" details
    Then I select Yes to travel exceeding 180 days and enter "<EstimateNumber>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    Then I logout of the application
    #### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    #### Client - Accept the quote
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    Then I logout of the application
    ##### Client Adviser - Apply Endorsement and accept quote
    Given I have the URL of VMIA
    When I enter the "<clientadviserusername>"
    And I enter "<clientadviserpassword>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I search and select org using "<orgname>" and "<zipcode>"
    Then I select the policy
    And Click UW Continue button
    Then I enter International travel "<UpdatedInternationalTrips>" details with NO travel to listed countries
    Then I enter Domestic travel "<UpdatedDomesticTrips>" details
    Then I select Yes to travel exceeding 180 days and enter "<EstimateNumber>"
    Then I enter the start date of insurance "<startdate>"
    And I do not want to change anymore values
    Then I note the case ID
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<UWStatus>"
    And I note down the case ID
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When I enter the "<clientadviserusername>"
    And I enter "<clientadviserpassword>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then I validate the endorsement premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<UpdatedInternationalTrips>" "<UpdatedDomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logoff from client adviser portal

    @BTVEndorsement
    Examples: 
      | username                     | password        | orgname          | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips | EstimateNumber | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | venugopal.kadiri@cigniti.com | vmiatesting@123 | Automation PAP 2 | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |            200 | underwriter_AT | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |

  #### New Business Travel application with minimum premium case
  @MLP3Sprint1 @UW @BTV @PropAndPlan @NewApplication @Cancellation 
  Scenario Outline: Start a new BTV policy for Property and Planning client with no deviation and then cancel it - Client Portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I enter International travel "<InternationalTrips>" details with NO travel to listed countries
    Then I enter Domestic travel "<DomesticTrips>" details
    Then I select No to travel exceeding 180 days
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Prop and Planning Organisation
    Then I cancel a policy from more actions
    Then I check the case status to be "<UWStatus>"
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile
    Then I logout of the application

    @BTVEndorsement
    Examples: 
      | username                     | password        | orgname          | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | UWStatus             |
      | venugopal.kadiri@cigniti.com | vmiatesting@123 | Automation PAP 2 | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |
