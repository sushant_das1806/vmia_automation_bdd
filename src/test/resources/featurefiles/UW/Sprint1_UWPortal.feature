Feature: Create New Application Case

  @Sprint1 @UW @GPA 
  Scenario Outline: Start a new policy application
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    # And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | students | startdate   | client_number | status                 |
      | Operator01 | abhinav@1234 | GPA     |      400 | currentdate | BUNIT-9515    | PENDING-CLIENTRESPONSE |
      | Operator01 | abhinav@1234 | GPA     |      400 | 1/06/2020   | BUNIT-9515    | PENDING-CLIENTRESPONSE |

  @Sprint1 @UW @GPA
  Scenario Outline: Start a new policy application with deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    # And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | students | startdate | client_number | pendingstatus        |
      | Operator01 | abhinav@1234 | GPA     |    14000 | 1/06/2020 | BUNIT-9515    | PENDING-UNDERWRITING |
     #| Operator01 | abhinav@1234 | GPA     |      400 | 1/05/2020    | BUNIT-9515    | PENDING-UNDERWRITING     |
     #| Operator01 | abhinav@1234 | GPA     |      500 | currentdate | BUNIT-9515    | PENDING-UNDERWRITING     |
  #|Operator01 | abhinav@1234 | GPA     |      500 | pastdate    | BUNIT-9515    | PENDING-UNDERWRITING     |
  #| Operator01 | abhinav@1234 | GPA     |      500 | futuredate  | BUNIT-9515    | PENDING-UNDERWRITING     |
  #| Operator01 | abhinav@1234 | GPA     |    14000 | currentdate | BUNIT-9515    | PENDING-UNDERWRITING     |
  #| Operator01 | abhinav@1234 | GPA     |    14000 | pastdate    | BUNIT-9515    | PENDING-UNDERWRITING     |
  #| Operator01 | abhinav@1234 | GPA     |    14000 | futuredate  | BUNIT-9515    | PENDING-UNDERWRITING     |
  
  @Sprint1 @UW @Worklist
  Scenario Outline: Start a new policy application
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I route to my open actions
    
    Then I logout of the application

    Examples: 
      | username   | password     |
      | Operator01 | abhinav@1234 |
      #| Operator01 | abhinav@1234 | GPA     |      400 | 1/06/2020   | BUNIT-9515    | PENDING-CLIENTRESPONSE |
