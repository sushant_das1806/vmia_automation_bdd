Feature: Regression suite for VMIA MLP3

  #US-6133	Policy process for remaining products (Motor vehicle, Cons risk)
  #Update, cancel policy for Construction risk from client portal.
  #Create Motor vehicle from Client portal
  #Update, cancel policy for Motor vehicle from client portal.
  #Create  Construction risk Motor vehicle from client Adviser
  Scenario Outline: Create Motor vehicle from client portal P&P
  Given I have the external URL of VMIA
  When User enter valid username  "<username>" and password "<password>" click on Login button
  And I Select the Organization "<PropAndPlanningOrgName>"
  Then I want to start a new application for VMIA insurance
  When I select "<product>" and submit
  Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
  And Click UW Continue button
  Then I enter the required details "<ExecutiveVehicles1>""<ExecutiveVehicles1>" "<carrying vehicles>" for Vehicle details page
  And Click UW Continue button
  Then I enter the required details for Drivers page
  And Click UW Continue button
  Then I enter the required details for Insurance history page
  And Click UW Continue button
  Then I enter the requred details for Accumulation page
  And Click UW Continue button
  Then I enter the reuired details for Summary and review page
  Then I Click Finish button
  Then I logout of the application
  Given I have the URL of VMIA
  When User when logs in as "<Underwriter>"
  And I click login button
  When I search the case id that is routed and I begin the case
  Then I individually provide the "<basepremium>" and "<notes>"
  And I accept the quote for uw
  Then I submit the case to get routed to the user
  Then I logout of the application
  Then I close the external link
  Given I have the external URL of VMIA
  When User enter valid username  "<username>" and password "<password>" click on Login button
  And I validate the "<status>"
  Then I accept the quote
  And I verify the payment "PENDING-PAYMENT"
  Then I logout of the application
  
  Examples:
  | Underwriter | product | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | notes   |
  | Underwriter | MVF     |                  1 |                  2 | testing           |         200 | Testing |
 
  Scenario Outline: Create Motor vehicle from client portal VicFleet
  Given I have the external URL of VMIA
  When User enter valid username  "<username>" and password "<password>" click on Login button
  And I Select the Organization "<VicFleet1>"
  Then I want to start a new application for VMIA insurance
  When I select "<product>" and submit
  Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
  And Click UW Continue button
  Then I enter the required details "<ExecutiveVehicles1>""<ExecutiveVehicles1>" "<carrying vehicles>" for Vehicle details page
  And Click UW Continue button
  Then I enter the required details for Drivers page
  And Click UW Continue button
  Then I enter the required details for Insurance history page
  And Click UW Continue button
  Then I enter the requred details for Accumulation page
  And Click UW Continue button
  Then I enter the reuired details for Summary and review page
  Then I Click Finish button
  Then I logout of the application
  Given I have the URL of VMIA
  When User when logs in as "<Underwriter>"
  And I click login button
  When I search the case id that is routed and I begin the case
  Then I individually provide the "<basepremium>" and "<notes>"
  And I accept the quote for uw
  Then I submit the case to get routed to the user
  Then I logout of the application
  Then I close the external link
  Given I have the external URL of VMIA
  When User enter valid username  "<username>" and password "<password>" click on Login button
  And I validate the "<status>"
  Then I accept the quote
  And I verify the payment "PENDING-PAYMENT"
  Then I logout of the application
  
  Examples:
  | Underwriter | product | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | notes   |
  | Underwriter | MVF     |                  1 |                  2 | testing           |         200 | Testing |
  
  Scenario Outline: Start a new policy application for VicFleet Org MVF with deviation in client adviser portal
  Given I have the URL of VMIA
  When User when logs in as "<ClientAdviser>"
  Then I am logged in to the client adviser portal
  Then I click on make a new application
  Then I enter "<OrganizationName>" and Click find button from Newapplication page
 # Then I want to start a new application for client adviser
  When I select "<product>" and submit
  Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
  And Click UW Continue button
  Then I enter the required details "<ExecutiveVehicles1>""<ExecutiveVehicles1>" "<carrying vehicles>" for Vehicle details page
  And Click UW Continue button
  Then I enter the required details for Drivers page
  And Click UW Continue button
  Then I enter the required details for Insurance history page
  And Click UW Continue button
  Then I enter the requred details for Accumulation page
  And Click UW Continue button
  Then I enter the reuired details for Summary and review page
  Then I Click Finish button
  Then I logout of the application
  Given I have the URL of VMIA
  When User when logs in as "<Underwriter>"
  And I click login button
  When I search the case id that is routed and I begin the case
  Then I individually provide the "<basepremium>" and "<notes>"
  And I accept the quote for uw
  Then I submit the case to get routed to the user
  Then I logout of the application
  Then I close the external link
  Given I have the external URL of VMIA
  When User enter valid username  "<username>" and password "<password>" click on Login button
  And I validate the "<status>"
  Then I accept the quote
  And I verify the payment "PENDING-PAYMENT"
  Then I logout of the application
  
  Examples:
  | ClientAdviser| Underwriter | product | OrganizationName | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | notes   |
  |ClientAdviser| Underwriter | MVF     | VicFleet3        |                  1 |                  2 | testing           |         200 | Testing |
 @MLP3Sprint3 @UW @CON @PropAndPlan @NewApplication
  Scenario Outline: Start a new Construction Risks policy for Property and Planning client and then endorse it using Client Adviser Portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I fill the Constrution Risk information questions
    Then I click Add button to add a contract
    Then I fill contract details "<ContractType>" "<EstimatedProjectValue>"
    Then I fill the contract location details
    Then I fill the Optional Extensions details "<LargestSingleContractVal>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the status of the case to be "PENDING-UNDERWRITING"
    And I click on Exit Button
    Then I logout of the application
    ##### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    And I validate the status of the case to be "PENDING-CLIENTACCEPTANCE"
    Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I validate the status of the case to be "PENDING-PAYMENT"
    And I save the policy number
    Then I logout of the application
    ##### Client Adviser - Apply Endorsement
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I search and select org using client segment "<OrgClientSegment>"
    Then I select the policy
    And Click UW Continue button
    Then I fill updated contract details "<NewContractType>" "<NewEstimatedProjectValue>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the status of the case to be "PENDING-UNDERWRITING"
    And I note down the case ID
    Then I logoff from client adviser portal
    ##### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the endorsement premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<NewEstimatedProjectValue>" "<NewMaterialDamageMul>" "<NewLiabilityMul>" "<gst>" "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    #### Client Adviser - Accept quote
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser My cases tab
    Then I validate the endorsement premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<NewEstimatedProjectValue>" "<NewMaterialDamageMul>" "<NewLiabilityMul>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I validate the status of the case to be "PENDING-PAYMENT"
    Then I logoff from client adviser portal

    Examples: 
      | username                     | password        | OrgClientSegment | product | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
      | venugopal.kadiri@cigniti.com | vmiatesting@123 | PAP              | CON     | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |

  @MLP3Sprint3 @UW @CON @PropAndPlan @NewApplication @Cancellation
  Scenario Outline: Start a new Construction Risks policy for Property and Planning client and then cancel the policy
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I fill the Constrution Risk information questions
    Then I click Add button to add a contract
    Then I fill contract details "<ContractType>" "<EstimatedProjectValue>"
    Then I fill the contract location details
    Then I fill the Optional Extensions details "<LargestSingleContractVal>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the status of the case to be "PENDING-UNDERWRITING"
    And I click on Exit Button
    Then I logout of the application
    ##### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    ##### Client - Accept the quote and apply for policy cancellation
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    And I validate the status of the case to be "PENDING-CLIENTACCEPTANCE"
    Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I validate the status of the case to be "PENDING-PAYMENT"
    And I save the policy number
    Then I click the exit button
    ##### Cancel the policy
    And I Select the Prop and Planning Organisation
    Then I cancel a policy from more actions
    Then I validate the status of the case to be "PENDING-UNDERWRITING"
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile 
    Then I logout of the application

    Examples: 
      | username                     | password        | OrgClientSegment | product | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | LargestSingleContractVal |
      | venugopal.kadiri@cigniti.com | vmiatesting@123 | PAP              | CON     | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  |                   156000 |
      
 
  Scenario Outline: Create Motor vehicle from client portal for Cemetry trust
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    #And I Select the Organization "<PropAndPlanningOrgName>"
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I enter the required details "<NumberofVehicles1>""<NumberofVehicles2>" "<carrying vehicles>" for Vehicle details page for cemetry trust
    And Click UW Continue button
    Then I enter the required details for Drivers page
    And Click UW Continue button
    Then I enter the required details for Insurance history page
    And Click UW Continue button
    Then I enter the requred details for Accumulation page
    And Click UW Continue button
    Then I enter the reuired details for Summary and review page
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<Underwriter>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I individually provide the "<basepremium>" and "<notes>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "PENDING-PAYMENT"
    Then I logout of the application

    Examples: 
      | Underwriter | product | NumberofVehicles1 | NumberofVehicles2 | carrying vehicles | basepremium | notes   |
      | Underwriter | MVF     |                 1 |                 2 | testing           |         200 | Testing |

  #Scenario Outline: Verify edit limits or sublimits and deductibles on finalise quote screen
    #Given I have the external URL of VMIA
    #When User enter valid username  "<username>" and password "<password>" click on Login button
    #And I Select the Organization "<PropAndPlanningOrgName>"
    #And I Select the Prop and Planning Organisation
    #Then I want to start a new application for VMIA insurance
    #Examples: 
    #| ClientAdviser | Underwriter | product | OrganizationName | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | notes   |
     #| ClientAdviser | Underwriter | MVF     | VicFleet3        |                  1 |                  2 | testing           |         200 | Testing |
    #
  #Scenario Outline: Start a new policy application for VicFleet Org MVF with deviation in client adviser portal
    #Given I have the URL of VMIA
    #When User when logs in as "<ClientAdviser>"
    #Then I am logged in to the client adviser portal
    #Then I click on make a new application
    #Then I enter "<OrganizationName>" and Click find button from Newapplication page
    #Then I want to start a new application for client adviser
    #When I select "<product>" and submit
    
          
 @MLP3Sprint11234567999
  Scenario Outline: Start a new MVF policy for Property and Planning with deviation and then Cancel it - Client Portal
    
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    #And I Select the Organization "<PropAndPlanningOrgName>"
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the required details "<ExecutiveVehicles1>""<ExecutiveVehicles1>" "<carrying vehicles>" for Vehicle details page
    And Click UW Continue button
    Then I enter the required details for Drivers page
    And Click UW Continue button
    Then I enter the required details for Insurance history page
    And Click UW Continue button
    Then I enter the requred details for Accumulation page
    And Click UW Continue button
    Then I enter the reuired details for Summary and review page
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<Underwriter>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I individually provide the "<basepremium>" and "<notes>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I validate the "<status>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Prop and Planning Organisation
    Then I cancel a policy from more actions
    Then I check the case status to be "<UWStatus>"
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile
    Then I logout of the application

    
    Examples: 
      | Underwriter | product | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | notes   | UWStatus             |
      | Underwriter | MVF     |                  1 |                  2 | testing           |         200 | Testing | PENDING-UNDERWRITING |

  
