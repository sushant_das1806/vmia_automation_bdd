Feature: Manage Purchase orderr

  @insurance_manageorder_RA
  Scenario Outline: Availabity of Manage Purchase Order from risk advisor
   Given I have the URL of VMIA
    And  I enter the "<username>"
    And I enter "<password>"
  	And   I click login button
    And Click on the new button
    And click on the Manage purchase order button 
    And Now enter the policy no "<policyno>"
    And Now click on the search button
    And Now click on the radio button
    And Click UW Continue button
    And Clear the predefined purchase order and enter new purchase order"<purchaseorder>"
    And Click UW Continue button
    Then I Click Finish button
    And I verify the final status for updation "<status>"

    

@US-15113
   Examples: 
				| username    | password  | policyno | purchaseorder | status |
				| RiskAdvisor | rules | V011023 | ABCD3021 |RESOLVED-COMPLETED|