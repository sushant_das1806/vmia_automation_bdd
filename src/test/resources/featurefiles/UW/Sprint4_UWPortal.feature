Feature: Create New Application Case and update it

  #@Sprint4 @UW @MSC @MSCNoDeviation @Endorsement
  Scenario Outline: Start a new policy application and then update it for MSC
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for "<parkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I want to update a policy
    Then I select the policy to be updated
    And Click UW Continue button
    Then I enter the number of "<updatedparkingslots>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for "<parkingslots>" "<updatedparkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @Sprint4 @UW @MSC @MSCNoDeviation @MSCEndorsement
    Examples: 
      | username   | password    | product | parkingslots | updatedparkingslots | multiplier | gst | stampduty | startdate   | status                   | days | paymentstatus      |
      | Operator01 | abhinav@123 | MSC     |          400 |                 500 |         12 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |   20 | PENDING-PAYMENT    |
      | Operator01 | abhinav@123 | MSC     |          500 |                 400 |         12 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |   20 | PENDING-REFUND     |
      | Operator01 | abhinav@123 | MSC     |          500 |                 500 |         12 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |   20 | RESOLVED-COMPLETED |

    @Sprint4
    Examples: 
      | username   | password    | product | parkingslots | updatedparkingslots | multiplier | gst | stampduty | startdate   | status                   | days | paymentstatus   |
      | ahs@school | Welcome@321 | MSC     |          400 |                 500 |         12 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |   22 | PENDING-PAYMENT |

  Scenario Outline: Start a new policy and then update it for GPA
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    # And I provide some "<action>"
    Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I want to update a policy
    Then I select the policy to be updated
    And Click UW Continue button
    Then I enter number of "<updatedstudents>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @GPAEndorsement
    Examples: 
      | username   | password    | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   |
      | Operator01 | abhinav@123 | GPA     |      200 |             500 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |   19 | PENDING-PAYMENT |

  #| Operator01 | abhinav@123 | GPA     |      500 |             200 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |   19 | PENDING-PAYMENT |
  #| Operator01 | abhinav@123 | GPA     |      200 |             200 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |   19 | PENDING-PAYMENT |
  Scenario Outline: Start a new policy and then update it for GPA for refund or no action
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    # And I provide some "<action>"
    Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I want to update a policy
    Then I select the policy to be updated
    And Click UW Continue button
    Then I enter number of "<updatedstudents>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    #And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @GPAEndorsement
    Examples: 
      | username   | password    | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus      |
      #| Operator01 | abhinav@123 | GPA     |      200 |             500 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |   19 | PENDING-PAYMENT |
      | Operator01 | abhinav@123 | GPA     |      500 |             200 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |   19 | PENDING-REFUND     |
      | Operator01 | abhinav@123 | GPA     |      200 |             200 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |   19 | RESOLVED-COMPLETED |

  Scenario Outline: Start a new policy application for property and update it
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I want to update a policy
    Then I select the policy to be updated
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<updatedvalue>" as "<propertyvalue>"
    And I do not want to change anymore values
    And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for property "<propertyvalue>" "<minimumvalue>" "<updatedminimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @PROEndorsement
    Examples: 
      | username   | password    | product | value          | updatedvalue   | propertyvalue | minimumvalue | updatedminimumvalue | percentage | gst | stampduty | startdate   | client_number | status                   | pendingstatus        | days | uwusername  | uwpassword  | paymentstatus   |
      | Operator01 | abhinav@123 | PRO     | Up to $250,000 | Up to $500,000 |        100000 |          950 |                1500 |          1 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  383 | underwriter | Welcome@321 | PENDING-PAYMENT |

  Scenario Outline: Start a new policy application for Liability with no deviation and risk question as yes and update it
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I want to update a policy
    Then I select the policy to be updated
    And Click UW Continue button
    # Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<updatedhiredays>"
    And I do not want to change anymore values
    #And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for liability for "<basepremium>" "<updatedbasepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @PPLEndorsement
    Examples: 
      | username   | password    | product | basepremium | updatedbasepremium | paymentstatus   | hiredays          | updatedhiredays | gst | stampduty | startdate   | client_number | status                   |
      | Operator01 | abhinav@123 | PPL     |         320 |                560 | PENDING-PAYMENT | Less than 50 days | 50 to 100 days  | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |

  Scenario Outline: Start a new policy application for Liability with no deviation and risk question as yes and update it negative premium
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I want to update a policy
    Then I select the policy to be updated
    And Click UW Continue button
    # Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<updatedhiredays>"
    And I do not want to change anymore values
    #And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for liability for "<basepremium>" "<updatedbasepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @PPLEndorsementRefundNoPremium
    Examples: 
      | username   | password    | product | basepremium | updatedbasepremium | paymentstatus      | hiredays          | updatedhiredays   | gst | stampduty | startdate   | client_number | status                   |
      | Operator01 | abhinav@123 | PPL     |         320 |                320 | RESOLVED-COMPLETED | Less than 50 days | Less than 50 days | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |
      | Operator01 | abhinav@123 | PPL     |         560 |                320 | PENDING-REFUND     | 50 to 100 days    | Less than 50 days | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |

  #@Sprint3 @UW @MV @MVWithoutDeviation @Regression @Four
  Scenario Outline: Start a new policy application for Motor Vehicle without deviation and update it
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the risk page "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And Click UW Continue button
    #And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for MV "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I want to update a policy
    Then I select the policy to be updated
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the endorsement risk page "<Endorsedlightvehicleflag>" "<Endorsedbusflag>" "<Endorsedtrailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And I do not want to change anymore values
    #And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for MV "<Endorsedlightvehicleflag>" "<Endorsedbusflag>" "<Endorsedtrailerflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @MVEndorsement
    Examples: 
      | username   | password    | product | parkingslots | multiplier | gst | stampduty | startdate   | status                   | days | paymentstatus   | lightvehicleflag | Endorsedlightvehicleflag | busflag | Endorsedbusflag | trailerflag | Endorsedtrailerflag | otherflag | typeofvehicle | manufactureyear | make | model | regno | currentmarketvalue | numberofvehicles | pricelightvehicle | pricebusunder50 | pricebusover50 | pricetrailer |  |
      | Operator01 | abhinav@123 | MVF     |          400 |      11.98 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |   19 | PENDING-PAYMENT | true             | false                    | true    | false           | false       | true                | false     | SUV           |            2011 | ABC  | n97   |  8798 |              40000 |                1 |               450 |             620 |         891.78 |           50 |  |

  Scenario Outline: Start a new policy application for GPA with no deviation and then manually process payment
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    # And I provide some "<action>"
    Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    #And I verify the payment "<paymentstatus>"
    Then I logout of the application
    When I enter the "<financeusername>"
    And I enter "<financepassword>"
    And I click login button
    Then I manually process payment from finance portal
    Then I verify manual resolve "<resolvedstatus>"
    Then I logout of the application

    @financeportal
    Examples: 
      | username   | password    | financeusername | financepassword | product | students | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   | resolvedstatus    |
      | Operator01 | abhinav@123 | finance@vmia    | Welcome@2       | GPA     |      400 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |   17 | PENDING-PAYMENT | RESOLVED-MANUALLY |

  Scenario Outline: Start a new policy application for Liability with no deviation and risk question as yes and manually process payment
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application
    When I enter the "<financeusername>"
    And I enter "<financepassword>"
    And I click login button
    Then I manually process payment from finance portal
    Then I verify manual resolve "<resolvedstatus>"
    Then I logout of the application

    @hhhh
    Examples: 
      | username   | password    | financeusername | financepassword | product | basepremium | paymentstatus   | resolvedstatus    | hiredays          | gst | stampduty | startdate   | client_number | status                   |
      | Operator01 | abhinav@123 | finance@vmia    | Welcome@2       | PPL     |         320 | PENDING-PAYMENT | RESOLVED-MANUALLY | Less than 50 days | 0.1 |       0.1 | 1/07/2020 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |
