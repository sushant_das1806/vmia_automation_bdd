Feature: Create New Application Case

  @Sprint2 @UW @GPA @GPANoDeviation @UpdateApplication
  Scenario Outline: Start a new policy application for GPA with no deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    #And I provide some "<action>"
    #Then I validate the premium for "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   | action |
      | Operator01 | abhinav@1234 | GPA     |      400 |             500 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9515    | PENDING-CLIENTACCEPTANCE |   48 | PENDING-PAYMENT | update |

  @Sprint2 @UW @GPA @GPANoDeviation @CancelApplication
  Scenario Outline: Start a new policy application for GPA with no deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    And I provide some "<action>" with value to be updated if required as "<updatedstudents>"
    And I verify the cancelled "<cancelledstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | students | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   | action | cancelledstatus    |
      | Operator01 | abhinav@1234 | GPA     |      400 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9515    | PENDING-CLIENTACCEPTANCE |   28 | PENDING-PAYMENT | cancel | RESOLVED-CANCELLED |

  @Sprint2 @UW @GPA @GPAWithDeviation
  Scenario Outline: Start a new policy application for GPA with deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I open the approved case
    And I validate the "<status>"
    #Then I validate the premium for "<students>" for "<days>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | students | multiplier | gst | stampduty | startdate   | client_number | status                   | pendingstatus        | days | uwusername  | uwpassword  | paymentstatus   |
      | Operator01 | abhinav@1234 | GPA     |    12000 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9515    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |   48 | underwriter | Welcome@321 | PENDING-PAYMENT |

  #| Operator01 | abhinav@1234 | GPA     |      400 |        5.5 | 0.1 |       0.1 | 1/05/2020   | BUNIT-9515    | PENDING-CLIENTRESPONSE     | PENDING-UNDERWRITING |   54 | underwriter | Welcome@1234 | PENDING-PAYMENT |
  #| Operator01 | abhinav@1234 | GPA     |    10000 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9515    | PENDING-CLIENTRESPONSE     | PENDING-UNDERWRITING |   54 | underwriter | Welcome@1234 | PENDING-PAYMENT |
  #|Operator01  | abhinav@1234 | GPA     |    10000 |        5.5 | 0.1 |       0.1 | pastdate    | BUNIT-9515    | PENDING-CLIENTRESPONSE     | PENDING-UNDERWRITING |   54 | underwriter | Welcome@1234 | PENDING-PAYMENT |
  #| Operator01 | abhinav@1234 | GPA     |    10000 |        5.5 | 0.1 |       0.1 | futuredate  | BUNIT-9515    | PENDING-CLIENTRESPONSE     | PENDING-UNDERWRITING |   54 | underwriter | Welcome@1234 | PENDING-PAYMENT |
  #| Operator01 | abhinav@1234 | GPA     |    14000 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9515    | PENDING-CLIENTRESPONSE     | PENDING-UNDERWRITING |   54 | underwriter | Welcome@1234 | PENDING-PAYMENT |
  #| Operator01 | abhinav@1234 | GPA     |    14000 |        5.5 | 0.1 |       0.1 | pastdate    | BUNIT-9515    | PENDING-CLIENTRESPONSE     | PENDING-UNDERWRITING |   54 | underwriter | Welcome@1234 | PENDING-PAYMENT |
  #| Operator01 | abhinav@1234 | GPA     |    14000 |        5.5 | 0.1 |       0.1 | futuredate  | BUNIT-9515    | PENDING-CLIENTRESPONSE     | PENDING-UNDERWRITING |   54 | underwriter | Welcome@1234 | PENDING-PAYMENT |
  @Sprint2 @UW @GPA @GPAWithDeviationFullYear
  Scenario Outline: Start a new policy application for GPA with deviation for a full year
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I open the approved case
    And I validate the "<status>"
    #Then I validate the premium for "<students>" for "<days>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | students | multiplier | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername  | uwpassword  | paymentstatus   |
      | Operator01 | abhinav@1234 | GPA     |      100 |        5.5 | 0.1 |       0.1 | 1/07/2019 | BUNIT-9515    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter | Welcome@321 | PENDING-PAYMENT |

  @Sprint2 @UW @PRO @PRONoDeviation
  Scenario Outline: Start a new policy application for property with STP
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | value         | propertyvalue | minimumvalue | percentage | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername  | uwpassword  | paymentstatus   |
      #| Operator01 | abhinav@1234 | PRO     | Up to $250,000 |        100000 |          950 |          1 | 0.1 |       0.1 | currentdate | BUNIT-9515    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |   48 | underwriter | Welcome@1234 | PENDING-PAYMENT |
      #| Operator01 | abhinav@1234 | PRO     | Up to $500,000 |        450000 |          1500 |          1 | 0.1 |       0.1 | 1/07/2019 | BUNIT-9515    | PENDING-CUSTOMERRESPONSE | PENDING-UNDERWRITING |  366 | underwriter | Welcome@1234 |
      #| Operator01 | abhinav@1234 | PRO     | Over $500,000  |        900000 |         1500 |       0.25 | 0.1 |       0.1 | 1/07/2019 | BUNIT-9515    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter | Welcome@1234 | PENDING-PAYMENT |
      | Operator01 | abhinav@1234 | PRO     | Over $500,000 |       900000 |        10000 |       0.15 | 0.1 |       0.1 | 1/07/2019 | BUNIT-9515    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter | Welcome@321 | PENDING-PAYMENT |

  @Sprint2 @UW @PRO @PROWithDeviation
  Scenario Outline: Start a new policy application for property with deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | value         | propertyvalue | minimumvalue | percentage | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername  | uwpassword  | paymentstatus   |
      #| Operator01 | abhinav@1234 | PRO     | Up to $250,000 |        100000 |          950 |          1 | 0.1 |       0.1 | 1/07/2019 | BUNIT-9515    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter | Welcome@1234 |
      #| Operator01 | abhinav@1234 | PRO     | Up to $500,000 |        450000 |          1500 |          1 | 0.1 |       0.1 | 1/07/2019 | BUNIT-9515    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter | Welcome@1234 |
      #| Operator01 | abhinav@1234 | PRO     | Over $500,000 |        900000 |         1500 |       0.25 | 0.1 |       0.1 | 1/07/2019 | BUNIT-9515    | PENDING-CLIENTACCEPTANCE   | PENDING-UNDERWRITING |  365 | underwriter | Welcome@1234 | PENDING-PAYMENT |
      #| Operator01 | abhinav@1234 | PRO     | Over $500,000 |       1200000 |         2500 |      0.225 | 0.1 |       0.1 | 1/07/2019 | BUNIT-9515    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter | Welcome@1234 | PENDING-PAYMENT |
      #| Operator01 | abhinav@1234 | PRO     | Over $500,000 |       4500000 |         4500 |        0.2 | 0.1 |       0.1 | 1/07/2019 | BUNIT-9515    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter | Welcome@1234 | PENDING-PAYMENT |
      | Operator01 | abhinav@1234 | PRO     | Over $500,000 |       9000000 |        10000 |       0.15 | 0.1 |       0.1 | 1/07/2019 | BUNIT-9515    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter | Welcome@321 | PENDING-PAYMENT |

  @Sprint2 @UW @PRO @PROWithDeviationOver10Million
  Scenario Outline: Start a new policy application for property above 10 million
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    #Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And I provide additional details for property above ten million "<propertyvalue>" "<singlelocation>" "<locationsecurity>" "<construction>" "<additionaldetails>" "<fireprotection>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | value         | propertyvalue | singlelocation | locationsecurity | construction       | additionaldetails | fireprotection | minimumvalue | percentage | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername  | uwpassword  | paymentstatus   |
      | Operator01 | abhinav@1234 | PRO     | Over $500,000 |      30000000 | Yes            | Local alarms     | Mixed construction | Timber            | Sprinklers     |        15000 |      0.125 | 0.1 |       0.1 | 1/07/2019 | BUNIT-9515    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter | Welcome@321 | PENDING-PAYMENT |

  @Sprint2 @UW @PPL @PPLNoDeviation @RiskQuestionYes
  Scenario Outline: Start a new policy application for Liability with no deviation and risk question as yes
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | basepremium | paymentstatus   | hiredays          | gst | stampduty | startdate   | client_number | status                   |
      | Operator01 | abhinav@123 | PPL     |         320 | PENDING-PAYMENT | Less than 50 days | 0.1 |       0.1 | currentdate | BUNIT-9515    | PENDING-CLIENTACCEPTANCE |

  #| Operator01 | abhinav@1234 | PPL     |         560 | PENDING-PAYMENT | 50 to 100 days    | 0.1 |       0.1 | currentdate | BUNIT-9515    | PENDING-CLIENTACCEPTANCE |
  @Sprint2 @UW @PPL @PPLNoDeviation
  Scenario Outline: Start a new policy application for Liability with no deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as no
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | basepremium | paymentstatus   | gst | stampduty | startdate   | client_number | status                   | days |
      | Operator01 | abhinav@1234 | PPL     |         320 | PENDING-PAYMENT | 0.1 |       0.1 | currentdate | BUNIT-9515    | PENDING-CLIENTACCEPTANCE |   48 |

  @Sprint2 @UW @PPL @PPLWithDeviation @RiskQuestionYes
  Scenario Outline: Start a new policy application for Liability with no deviation and risk question as yes
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I individually provide the "<basepremium>" and "<notes>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | uwusername  | uwpassword  | hiredays           | product | basepremium | notes        | pendingstatus        | paymentstatus   | gst | stampduty | startdate   | client_number | status                   |
      | Operator01 | abhinav@1234 | underwriter | Welcome@321 | More than 100 days | PPL     |        1000 | underwritten | PENDING-UNDERWRITING | PENDING-PAYMENT | 0.1 |       0.1 | currentdate | BUNIT-9515    | PENDING-CLIENTACCEPTANCE |

  @Sprint2 @UW @PPL @PPLNoDeviation @RiskQuestionYes @MarketStall @Testtt
  Scenario Outline: Start a new policy application for Liability with no deviation and risk question as yes with market stall event
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    Then I add "<marketstallholders>" and "<numberofdates>" as market stall events
    #And I provide an attachment having filepath as "<attachfilepath>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | product | basepremium | marketstallholders | numberofdates | attachfilepath                        | paymentstatus   | hiredays          | gst | stampduty | startdate   | client_number | status                   |
      | Operator01 | abhinav@1234 | PPL     |         560 |                 60 |             3 | C:\\Attachments\\Assertions_Added.txt | PENDING-PAYMENT | Less than 50 days | 0.1 |       0.1 | currentdate | BUNIT-9515    | PENDING-CLIENTACCEPTANCE |

  @Sprint2 @UW @PPL @PPLWithDeviation @RiskQuestionYes @MarketStall @MarketStallDeviation
  Scenario Outline: Start a new policy application for Liability with deviation and risk question as yes with market stall event
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    Then I add "<marketstallholders>" and "<numberofdates>" as market stall events
    And I provide an attachment having filepath as "<attachfilepath>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I individually provide the "<basepremium>" and "<notes>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    Examples: 
      | username   | password     | uwusername  | uwpassword  | pendingstatus        | product | basepremium | notes        | attachfilepath                        | marketstallholders | numberofdates | paymentstatus   | hiredays          | gst | stampduty | startdate   | client_number | status                   |
      | Operator01 | abhinav@1234 | underwriter | Welcome@321 | PENDING-UNDERWRITING | PPL     |        1000 | underwritten | C:\\Attachments\\Assertions_Added.txt |                 60 |            12 | PENDING-PAYMENT | Less than 50 days | 0.1 |       0.1 | currentdate | BUNIT-9515    | PENDING-CLIENTACCEPTANCE |
