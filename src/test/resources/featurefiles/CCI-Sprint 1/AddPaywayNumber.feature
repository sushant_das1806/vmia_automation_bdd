Feature: Create an organization and add a Payway number

  Scenario Outline: Create a new organization as a Client Adviser and Add a payway number to the organization
    Given I have the URL of VMIA
    And User when logs in as "<Operator>"
    Then I am logged in to the client adviser portal
    Then I click on create new organisation
    Then I provide organisation name "<Neworgname>"
    Then I want to create a new customer
#    Then I fill the form
    ### Below step has loading issue. Its failing some times due to slow response
    And I note down the PayWay number ID
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    And User when logs in as "Finance"
    Then I open the "Payway Account" workbasket in the finance portal
    And I open the Payway Case and enter the Payway Number
    ### Below logout step got added
    Then I logout of the application

  @CCISprint1 @RegressionMLP5Set2 @ClientAdvisorLogin @CCI @CCISprint01
    Examples:
    |Neworgname           | Operator      |
    |CemeteryQAAutolive62 | ClientAdviser |

    @Sprint1 @RegressionMLP5Set2 @RiskAdvisorLogin @CCI @CCISprint01
    Examples:
      |Neworgname|Operator|
      |CemeteryQAAutolive621|RiskAdvisor|

