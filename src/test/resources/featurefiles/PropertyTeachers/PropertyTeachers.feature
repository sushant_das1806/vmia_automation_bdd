Feature: Property Teachers

  Scenario Outline: Start a new Property Teachers Policy for Education Clients and client acceptance

    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    Then Click on all products in segments in client advisor
    And click on the select button of the "<product>"
#    And Click "Yes" to reapply the policy
    And I capture the policy ID
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I Add Additional Deductibles "TestDeductible" "1500" "Description"
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MPL4 @AutoB&I @PolicyCreation @clientacceptance @Property_teachers  @All
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname          |product                                               | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | AutoEdu          |Property - Teachers and Principals notebook program   | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |

  Scenario Outline: Start a new Property Teachers Policy for Education Clients and endorsement and client acceptance

    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    Then Click on all products in segments in client advisor
    And click on the select button of the "<product>"
    And Click "Yes" to reapply the policy
    And I capture the policy ID
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I Add Additional Deductibles "TestDeductible" "1500" "Description"
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number from RiskAdvisor
    Then I logoff from client adviser portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

@MPL4 @AutoEdu @PolicyCreation @clientacceptance @Property_teachers
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname          |product                                               | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | AutoEdu          |Property - Teachers and Principals notebook program   | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |


  Scenario Outline: Start a new Property Teachers Policy for Education Clients Risk Advisor acceptance and endorsement
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    Then Click on all products in segments in client advisor
    And click on the select button of the "<product>"
    And Click "Yes" to reapply the policy
    And I capture the policy ID
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I Add Additional Deductibles "Participating bodies" "30" "Property of teacher"
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
     And I save the policy number from RiskAdvisor
    Then I logoff from client adviser portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I open the case for Review
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
   When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I open the case for Review
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

@MPL4 @AutoEdu @PolicyCreation @clientacceptance @Property_teachers
    Examples:
      | decsion  |  Permium   | UnderwritingNotes  | username                       | password      | orgname          |product                                               | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | Yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | AutoEdu          |Property - Teachers and Principals notebook program   | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |


  Scenario Outline: Start a new Property Teachers Policy for Education Clients and cancel

    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    Then Click on all products in segments in client advisor
    And click on the select button of the "<product>"
    And Click "Yes" to reapply the policy
    And I capture the policy ID
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I Add Additional Deductibles "Participating bodies" "30" "Property of teacher"
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number from RiskAdvisor
    Then I logoff from client adviser portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then I cancel a policy from more actions
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @MPL4 @AutoEdu @PolicyCreation @clientacceptance @Property_teachers
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname          |product                                               | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | AutoEdu          |Property - Teachers and Principals notebook program   | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |

