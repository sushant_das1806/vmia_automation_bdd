Feature: Client Adviser suite for VMIA MLP1

  Scenario Outline: Verify Risk adviosr is able to add asset and underwriter approve the same asset
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Add asset" in AssetManagement
    And Select the organization "<organization>" for Adding Asset
    And Select the contact associate with request "No"
    And Click Continue Button
    And Add Asset primary Details "<Location>" "<AssetType>" "<AssetName>" "<AssetBand>"
    And Click Continue Button
    And Click Continue Button
    And Click Finish Button
    And Capture the Asset Details and Click submit asset
    Then Verify the case status "PENDING- APPROVAL" and capture the case id
    Then I logoff from client adviser portal
    And I have the URL of VMIA
    When User when logs in as "UWPortfoliaManger"
    And Open Asset from "Asset Management" Underwriter Queue
    Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
    Then I check the case status to be "<FinalStatus>"
    And I logout of the application
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    And Click New and Select "Search asset" in AssetManagement
    And Search the asset by id "" and open the asset "History"
    And Validate the following info in assethistory section of assettype "<AssetName>" AssetName "<AssetType>" and AssetID
   @AddAsset
    Examples:
      | username          | password     | organization | Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStatus|
      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED|
#      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|No  |RESOLVED-COMPLETED|
#      | AutoClientAdviser | Welcome@1234 | Scotland PNP | MELBOURNE AIRPORT  | Contents     | Test 		| 2				|Reject |No   |RESOLVED-REJECTED|
#      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Request more information |No   |RESOLVED-RFI|



   Scenario Outline: Verify Risk adviosr is able to Update and underwriter approve the same asset
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Update asset" in AssetManagement
    And Select the organization "<organization>" for Adding Asset
    And Select the contact associate with request "No"
    And Click Continue Button
    And Search the Assetby "Asset name" using value "<AssetName>"
    And Select the Asset result by name "<AssetName>"
    And Update the selected asset by "<AssetName>"
    And Click Continue Button
    And Click Continue Button
    And Click Finish Button
    And Capture the Asset Details and Click submit asset
    Then Verify the case status "PENDING- APPROVAL" and capture the case id
    Then I logoff from client adviser portal
    And I have the URL of VMIA
    When User when logs in as "UWPortfoliaManger"
    And Open Asset from "Asset Management" Underwriter Queue
    Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
    Then I check the case status to be "<FinalStaus>"
    And I logout of the application

    @ghgg @CS @ClientAdviser
    Examples:
      | username          | password     | organization | Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStaus|
      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED |
#      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|No  |RESOLVED-COMPLETED |
#      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Reject|Yes   |RESOLVED-REJECTED |
#      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Request more information|Yes   |RESOLVED-RFI |

    Scenario Outline: Verify Risk adviosr is able to Dispose the asset and underwriter approve the same asset
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Dispose asset" in AssetManagement
    And Select the organization "<organization>" for Adding Asset
    And Select the contact associate with request "No"
    And Click Continue Button
    And Search the Assetby "Asset name" using value "test"
    And Select the Asset result by name "test"
    And Click Continue Button
    And Click Continue Button
    And Click Finish Button
    And Capture the Asset Details and Click submit asset
    Then Verify the case status "PENDING- APPROVAL" and capture the case id
    Then I logoff from client adviser portal
    And I have the URL of VMIA
    When User when logs in as "UWPortfoliaManger"
    And Open Asset from "Asset Management" Underwriter Queue
    Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
    Then I check the case status to be "<FinalStaus>"
    And I logout of the application


    Examples:
    	 | username          | password     | organization | Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStaus|
      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED |
#      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|No  |RESOLVED-COMPLETED |
#      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Reject|Yes   |RESOLVED-REJECTED |
#      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Request more information|Yes   |RESOLVED-RFI |



    Scenario Outline: Verify Risk adviosr is able to Transfer the asset and underwriter approve the same asset
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Transfer asset" in AssetManagement
    And Select the organization "<organization>" for Adding Asset
    And Select the contact associate with request "No"
    And Click Continue Button
    And Select the organization "<DestinationOrg>" for Adding Asset
    And Click Continue Button
    And Search the Assetby "Asset name" using value "Test"
    And Select the Asset result by name "Test"
    And Click Continue Button
    And Click Continue Button
    And Click Finish Button
    And Capture the Asset Details and Click submit asset
    And Link the policy "V010874" to the asset
    Then Verify the case status "PENDING- APPROVAL" and capture the case id
    Then I logoff from client adviser portal
    And I have the URL of VMIA
    When User when logs in as "UWPortfoliaManger"
    And Open Asset from "Asset Management" Underwriter Queue
    Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
    Then I check the case status to be "<FinalStatus>"
    And I logout of the application


    Examples:
      | username          | password     | organization |DestinationOrg| Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStatus|
      | AutoClientAdviser | Welcome@1234 | Scotland PNP |Lowanna27      |Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED|
#      | AutoClientAdviser | Welcome@1234 | Scotland PNP |Lowanna27      |Sydney cot  | Bore     | Test 		| 2				|Approve|No   |RESOLVED-COMPLETED|
#      | AutoClientAdviser | Welcome@1234 | Scotland PNP |               |Sydney cot  | Bore     | Test 		| 2				|Reject|Yes   |RESOLVED-REJECTED |
#      | AutoClientAdviser | Welcome@1234 | Scotland PNP |               |Sydney cot  | Bore     | Test 		| 2				|Request more information|Yes   |RESOLVED-RFI |



  Scenario Outline: Verify Risk adviosr is able to upload the bulk upload template
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Bulk add/update assets" in AssetManagement
    And Attach the bulk upload file "<file>" and click submit
    Then I logoff from client adviser portal
    And I have the URL of VMIA
    When User when logs in as "UWPortfoliaManger"
    And Open Asset from "Asset Management" Underwriter Queue
    Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
    Then I check the case status to be "<FinalStatus>"
    And I logout of the application

    Examples:
      | username          | password     | file														  | Action |FinalStatus|  IsEndorseNeed|
		| AutoClientAdviser | Welcome@1234 |BulkUploadTemplateTest_Updated.csv|Approve|RESOLVED-COMPLETED|No|
#      | AutoClientAdviser | Welcome@1234 |BulkUploadTemplateTest_Updated.csv|Approve|RESOLVED-COMPLETED|Yes|
#      | AutoClientAdviser | Welcome@1234 |BulkUploadTemplateTest_Updated.csv|Reject|RESOLVED-REJECTED|Yes|
#      | AutoClientAdviser | Welcome@1234 |BulkUploadTemplateTest_Updated.csv|Request more information|RESOLVED-RFI|Yes|


  Scenario Outline: Verify the crestazone is auto pop based on the postal code
    Given I have the URL of VMIA
    When User when logs in as "GeospatialCoordinator"
    And  click hamburger and Select the "New"
    And  Select option in asset "Add asset"
    And Select the organization "<organization>" for Adding Asset
    And Select the contact associate with request "No"
    And Click Continue Button
    And Click on Add Location and verify the added crestazone "<CrestaZone>" is displayed for the Postcode "<Postcode>"
    And I logout of the application

    Examples:
      |organization| CrestaZone| Postcode|
      |Scotland PNP| Test      | 9999    |


  Scenario Outline: Verify user is able to add crestazone business rule
    Given I have the URL of VMIA
    When User when logs in as "GeospatialCoordinator"
    And  click hamburger and Select the "Business Rules"
    And  click edit button and Add Cresta zone
    Then Enter the crestazone "<Crestaid>" "<postalcode>" "<lowcresid>" "<highzone>" "<lowzone>"
    And  I logout of the application

    Examples:
    #  unique value should be passed to the cresta zone business rules
      | Crestaid|postalcode|lowcresid|highzone|lowzone|
      |AUS_9999 | 9999     | Aus_99  |AutoHigh| AutoLow |

  Scenario Outline: Verify the asset details are displayed properly in internal portal
    Given I have the URL of VMIA
    When User when logs in as "GeospatialCoordinator"
    And  click hamburger and Select the "Search"
    And  Search the asset by id "<AssetId>" and open the asset "View details"
    And verify the following values are displayed in asset name section in Asset details in internalportal
         |Asset type   |<Assettype>|
         |VMIA asset ID|<AssetId>  |
         |Asset status |<Status>   |
         |Latitude     |<Latitude> |
         |Longitude    |<Longitude>|
         |Address      |<Address>  |
    And  I logout of the application

    Examples:
      |Assettype| AssetId |Status |Latitude     |Longitude   |Address|
      |Bore     |VAR000260| 9999  |-38.16801245 |146.78562341|SYDNEY COTTAGE 153 WILLUNG RD, ROSEDALE VIC 3847|

  Scenario Outline: Verify the asset details are displayed properly in client portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    Then I want to click view organization profile
    And Switch to Asset tab and Open the Asset "<AssetId>"
    And verify the following values are displayed in asset name section in Asset details in clientPortal
      |VMIA asset ID|<AssetId>  |
      |Asset status |<Status>   |
      |Latitude     |<Latitude> |
      |Longitude    |<Longitude>|
      |Address      |<Address>  |
      |Start date-End date|<StartAndEndDate>|
      |Client asset ID|<ClientAssetID>|
   And I logout of the application
    Examples:
      |ClientAssetID| AssetId |Status |Latitude     |Longitude     |StartAndEndDate      |Address|
      |H-1244332    |VAR000285| Active  |-38.16801245 |146.78652341|07/06/2021-17/06/2021|SYDNEY COTTAGE 153 WILLUNG RD, ROSEDALE VIC 3847|