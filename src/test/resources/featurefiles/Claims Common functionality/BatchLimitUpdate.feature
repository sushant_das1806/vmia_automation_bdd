@MPL4_sprint1_us_6103
Feature: Option to increase batch limit temporarily

  @MPL4_sprint1_us_6103
  Scenario Outline: Verify Claims manger is able to increase batch limit temporarily and approved by finince officer
    Given User have to entter the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    And Click on the Humburger button
    And Click on the New button under portfolio manager
    And Click on the request new batch limit
    And Enter the new batch limit "<batch limit>"
    And Click on the calendar
    And Now click on the today date
    Then Click on submit button
		#And Now verify the text
    And validate the status1 "<Status1>"
    And Now click on close button
    Then Click on the user ID
    And Click on the Log off button
		#log in to finance portal
    And  I enter the "<financeID>"
    And I enter "<financepassword>"
    And I click login button
    And Click on the work owner and select batch limit
    And Click on the casenumber and select apply filter and enter the BL number
    And click on the Approve button
    Then I validate the "<finalstatus>" for batchlimit
    And Now click on close button


    Examples:
      | username                | password     | batch limit | financeID | financepassword | Status1 |finalstatus |
      | ClaimsPortfolioManager1 | Welcome@1234 | 15000002    | finance@vmia | Welcome@2 | PENDING-APPROVAL | RESOLVED-COMPLETED |