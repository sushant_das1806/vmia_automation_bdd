Feature: Claims MLP4 Sprint 4

  @Recoveries @RecoveryhistoryChange
  Scenario Outline: Initiate a Claim and add recovery, verify the recovery history is updated for the added recovey
#    Given I have the external URL of VMIA
#    When User enter valid username  "<username>" and password "<password>" click on Login button
#    And I Select the Organization "<name>"
#    Then I am logged in to the insurance portal
#    Then I want to make a claim
#    And I enter the date and time of loss
#    And I select the type of "<loss>"
#    And Click Continue button
#    When I select the business row and verify listed policies for "<loss>"
#    And Click Continue button
#    And Click Continue button
#    Then I am routed to Client Banking Account details page
#    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
#    And Click Continue button
#    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
#    And Click Continue button
#    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
#    And Click on Continue button for "4" from claims pages
#    And I check the declaration for Claims
#    Then Click Finish button
#    Then I validate the "<finalstatus>" for claims
#    And Extarct the claim number
#    Then I logout of the application
#    Then I close the external link
#    Given I have the URL of VMIA
#    When User when logs in as "<TriageOfficer>"
#    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
#    Then I click its not a duplicate
#    Then verify "<finalstatustriage>"
#    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<PortfolioManager>"
#    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "<ClaimsOfficer>"
#    Then I open claim unit from worklist
    Then Open the claim from "Unassigned Claims" queue
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100"
    Then create Recovery estimate  "Client excess" "50"
    And Add create recovery "Settlement" "Business" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "20" "14/May/2021"
    Then I logout of the application
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    And Verify the recovery history "" ""

    @Regression2_MLP2_Set3 @Golive_26_Today
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | recoverabletype | name     | resolvestatus      | awaitreason | approvestatus         | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername         | portfoliopassword | finalstatustriage        | triagecasestatus | username                       | password      | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | Reinsurance     | Lowanna4 | RESOLVED-COMPLETED | NA          | PENDING-INVESTIGATION |           100 | File review       |                20 | approve        | claimsofficer1    | rules             | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 R3 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | pavan.gangone@areteanstech.com | Areteans@1981 | TriageClaimsOfficer1R3      | rules                       | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |
