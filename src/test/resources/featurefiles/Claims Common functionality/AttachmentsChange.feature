Feature: Professional And Medical Indeminity claims test

  @MPL4 @Indeminity @US-8003
  Scenario Outline: Verify user is able to add document and verify the history for the same
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I am logged in to the insurance portal
    Then I want to make a claim from TriageOfficer
    Then Search and select Oraganization "<Organization>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    And Open the claim from "Triage Claims" queue
    And Add attachment with details following details "<AttachfilePath>" "<AttachmentCategeroy>" "<Author>" "<AttachDocDate>"
    And Verify the history of the document "<AttachfilePath>" "<AttachmentCategeroy>" "<AttachDocDate>"
    And Updated the attachment data  "<NewFilanamename>" "<UpdatedAuthor>" "<AttachmentCategeroy>" "<AttachDocDate>"
    Then I logout of the application

    Examples:
      |Organization        | TriageOfficer |  loss                    |  finalstatus       |AttachfilePath            |Author    |AttachDocDate|AttachmentCategeroy|UpdatedAuthor|NewFilanamename|
      |Rahara Cemetery Trust| TriageOfficer| Professional indemnity | RESOLVED-COMPLETED |Claim_check_attachment.txt|AutoTester|20/06/2021   |Claim              |tester      |UpdatedFile|

  Scenario Outline:Verify user is able to Search the document and validate edit and history is working properly
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I am logged in to the insurance portal
    And click hamburger and Select the "Search"
    And Select "Documents" tab in search
    And Enter the documentname "<Documents>" and click search
    And click edit from the document search result and update the author
    And Verify the history is added for the update
    Then I logout of the application

    Examples:
    |TriageOfficer |Documents                  |
     |  TriageOfficer|Claim_check_attachment12341|