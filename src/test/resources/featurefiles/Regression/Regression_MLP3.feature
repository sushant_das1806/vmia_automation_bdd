Feature:MLP3 Regression

  @MLP3Sprint1 @UW @JRN @PropAndPlan @NewApplication @RegMLP312345 @RegressionMLP3
  Scenario Outline: Start a new Journey policy for Property and Planning client with no deviation and then endorse it - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click on Exit Button
    And I Select the Prop and Planning Organisation
    And Select policy and Click on Update Policy
    And Click UW Continue button
    Then I enter updated values of "<updatedfte>" and "<startdate>"
    And I select no previous journey insurance
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium of Journey for "<fte>" "<updatedfte>" for "<startdate>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @JRNEndorsement @RegressionMLP3Scenarios
    Examples:
      | username                          | password     | orgname                        | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | RegressionSet2_PropertyPlanning| JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  @MLP3Sprint1 @UW @JRN @PropAndPlan @NewApplication @Test2 @RegMLP312345 @RegressionMLP3 @newwwww @MLP3SearchFIX @Rerun
  Scenario Outline: Start a new Journey policy for Property and Planning client with deviation through Client portal and then endorse it through Client Adviser Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    Then I logout of the application
    ##### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    ### Need to manage stalelement exception from the below step(UWStepDefinition.java:910, UW.java:892, SeleniumFunc.java:2292)
    When I search the case id that is routed and I begin the case
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    ### Then I logout of the application
    ### Commented the above line and added the below 13 steps
    Then I click on Exit Button
    And I Select the Prop and Planning Organisation
    And Select policy and Click on Update Policy
    And Click UW Continue button
    Then I enter updated values of "<updatedfte>" and "<startdate>"
    And I select no previous journey insurance
    And I do not want to change anymore values
    And I note down the case ID
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    #And I validate the "<status>"
    And I validate the "<UWStatus>"
#    Then I validate the endorsement premium of Journey for "<fte>" "<updatedfte>" for "<startdate>" values "<multiplier>" "<gst>" and "<stampduty>"
#    Then I accept the quote
#    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application
    ##### Client Adviser - Endorsement
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
#    Then I am logged in to the client adviser portal
#    Then I click on update a policy
#    Then I search and select org using "<orgname>" and "<zipcode>"
#    Then I select the policy
#    And Click UW Continue button
#    Then I enter updated values of "<updatedfte>" and "<startdate>"
#    And I select no previous journey insurance
#    And I do not want to change anymore values
#    And I note down the case ID
#    And Click UW Continue button
#    And I check the declaration
#    Then I Click Finish button
#    And I verify the payment status "<UWStatus>"
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    ### added the below step
    Then I logoff from client adviser portal
    ### Need to go back to client portal, so added the below four steps
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    Then I validate the endorsement premium of Journey for "<fte>" "<updatedfte>" for "<startdate>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    ### Then I logoff from client adviser portal
    ### Commented the above and added the below step
    Then I logout of the application

    @JRNEndorsement
    Examples:
      | username                          | password     | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername    | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Regression_PAP   | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    2601 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |

  @MLP3Sprint1 @UW @JRN @PropAndPlan @NewApplication @Cancellation @RegMLP312345211212 @RegressionMLP3
  Scenario Outline: Start a new Journey policy for Property and Planning client with no deviation and then cancel it - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Prop and Planning Organisation
    Then I cancel a policy from more actions
    Then I check the case status to be "<UWStatus>"
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile
    Then I logout of the application

    @JRNCancellation @RegressionMLP3Scenarios
    Examples:
      | username                          | password     | orgname                     | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      | manoj.ramanathan@areteanstech.com | Test@1234567 |RegressionMLP3_SchoolCouncil | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |

  @MLP3Sprint1 @UW @GPA @PropAndPlan @NewApplication @RegMLP3 @RegressionMLP3
  Scenario Outline: Start a new GPA policy for Property and Planning client with no deviation and then endorse it - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and details of persons "<volunteers>"
    And I select No to previous GPA policy details
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Prop and Planning Organisation
    Then I select update policy from more actions
    Then I enter updated values of PnP GPA "<startdate>" and details of persons "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>"
    And I select No to previous GPA policy details
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    ### In Endorsement, Its validating with Total premium instead of Endorsement premium so updated with locator here
    Then I validate the endorsement premium of PnP GPA for "<volunteers>" "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>" from "<startdate>" using values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @GPAEndorsement @RegressionMLP3Scenarios
    Examples:
      | username                          | password     | orgname        | product | volunteers | updatedvolunteers | boardMembers | workExperienceStudents | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Regression_PAP | GPA     |       2000 |              6000 |           50 |                   5000 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  @MLP3Sprint1 @UW @GPA @PropAndPlan @NewApplication123 @Test2 @RegMLP312111123 @RegressionMLP3 @RegressionFix123412qe @Rerun
  Scenario Outline: Start a new GPA policy for Property and Planning client with deviation and then endorse it through Client Adviser portal - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and details of persons "<volunteers>"
    And I select No to previous GPA policy details
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    Then I logout of the application
    ##### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    ##### Client Adviser - Apply Endorsement and accept quote
    Given I have the URL of VMIA
    Given User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I search and select org using "<orgname>" and "<zipcode>"
    Then I select the policy
    And I note down the case ID
    And Click UW Continue button
    Then I enter updated values of PnP GPA "<startdate>" and details of persons "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>"
    And I select No to previous GPA policy details
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<UWStatus>"
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then I validate the endorsement premium of PnP GPA for "<volunteers>" "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>" from "<startdate>" using values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logoff from client adviser portal

    @GPAEndorsement
    Examples:
      | username                          | password     | orgname        | product | volunteers | updatedvolunteers | boardMembers | workExperienceStudents | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Regression_PAP | GPA     |     100001 |            100020 |           50 |                   4500 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Underwriter1 | Welcome@321 |    2601 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |

  @MLP3Sprint1 @UW @GPA @PropAndPlan @NewApplication @Cancellation @RegMLP3 @RegressionMLP3 @RegressionMLP312344 @Rerun
  Scenario Outline: Start a new GPA policy for Property and Planning client with no deviation and then Cancel it - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and details of persons "<volunteers>"
    And I select No to previous GPA policy details
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Prop and Planning Organisation
    Then I cancel a policy from more actions
    Then I check the case status to be "<UWStatus>"
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile
    Then I logout of the application

    @GPACancellation @RegressionMLP3Scenarios
    Examples:
      | username                     	 | password     | orgname                   | product | volunteers | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      |manoj.ramanathan@areteanstech.com | Test@1234567 | Automation Cemetery Trust | GPA     |       2000 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |

  #### New Business Travel application with minimum premium case
  @MLP3Sprint1 @UW @BTV @PropAndPlan @NewApplication @RegMLP3 @RegressionMLP3 @RegressionFix
  Scenario Outline: Start a new BTV policy for Property and Planning client with no deviation and then endorse it - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I enter International travel "<InternationalTrips>" details with NO travel to listed countries
    Then I enter Domestic travel "<DomesticTrips>" details
    Then I select No to travel exceeding 180 days
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Prop and Planning Organisation
    ### Script getting failed in the below step so Updated the Locator - More button
    Then I select update policy from more actions
    Then I enter the start date of insurance "<startdate>"
    Then I enter International travel "<UpdatedInternationalTrips>" details with NO travel to listed countries
    Then I enter Domestic travel "<UpdatedDomesticTrips>" details
    Then I select No to travel exceeding 180 days
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<UpdatedInternationalTrips>" "<UpdatedDomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @BTVEndorsement @RegressionMLP3Scenarios
    Examples:
      | username                     	  | password     | orgname        | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Regression_PAP | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |

  #### New Business Travel application with minimum premium case and endorse to exceed minimum premium
  @MLP3Sprint1 @UW @BTV12333 @PropAndPlan @NewApplication @Test2 @RegMLP3 @RegressionMLP3 @1234333 @MLP3Fix
  Scenario Outline: Start a new BTV policy for Property and Planning client with deviation and then endorse it through Client Adviser portal - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I enter International travel "<InternationalTrips>" details with NO travel to listed countries
    Then I enter Domestic travel "<DomesticTrips>" details
    Then I select Yes to travel exceeding 180 days and enter "<EstimateNumber>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    Then I logout of the application
    #### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    #### Client - Accept the quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    Then I logout of the application
    ##### Client Adviser - Apply Endorsement and accept quote
    Given I have the URL of VMIA
    When I enter the "<clientadviserusername>"
    And I enter "<clientadviserpassword>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I search and select org using "<orgname>" and "<zipcode>"
    Then I select the policy
    And I note down the case ID
    And Click UW Continue button
    Then I enter International travel "<UpdatedInternationalTrips>" details with NO travel to listed countries
    Then I enter Domestic travel "<UpdatedDomesticTrips>" details
    Then I select Yes to travel exceeding 180 days and enter "<EstimateNumber>"
    Then I enter the start date of insurance "<startdate>"
    And I do not want to change anymore values
    Then I note the case ID
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<UWStatus>"
    Then I logoff from client adviser portal
    ## underwriter 3 steps
    Given I have the URL of VMIA
    When I enter the "<clientadviserusername>"
    And I enter "<clientadviserpassword>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    ##Client portal
    Then I validate the endorsement premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<UpdatedInternationalTrips>" "<UpdatedDomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logoff from client adviser portal

    @BTVEndorsement
    Examples:
      | username                     | password        | orgname          | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips | EstimateNumber | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Regression_PAP   | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |            200 | Underwriter1 | Welcome@321 |    2601 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |

  #### New Business Travel application with minimum premium case
  @MLP3Sprint1 @UW @BTV @PropAndPlan @NewApplication @Cancellation12345 @RegMLP3 @RegressionMLP3
  Scenario Outline: Start a new BTV policy for Property and Planning client with no deviation and then cancel it - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I enter International travel "<InternationalTrips>" details with NO travel to listed countries
    Then I enter Domestic travel "<DomesticTrips>" details
    Then I select No to travel exceeding 180 days
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Prop and Planning Organisation
    Then I cancel a policy from more actions
    Then I check the case status to be "<UWStatus>"
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile
    Then I logout of the application

    @BTVEndorsement @RegressionMLP3Scenarios
    Examples:
      | username                          | password     | orgname       | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | UWStatus             |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Automation PAP| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |

  #US-6133	Policy process for remaining products (Motor vehicle, Cons risk)
  #Update, cancel policy for Construction risk from client portal.
  #Create Motor vehicle from Client portal
  #Update, cancel policy for Motor vehicle from client portal.
  #Create  Construction risk Motor vehicle from client Adviser
  @RegMLP3
  Scenario Outline: Create Motor vehicle from client portal P&P
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<PropAndPlanningOrgName>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the required details "<ExecutiveVehicles1>""<ExecutiveVehicles1>" "<carrying vehicles>" for Vehicle details page
    And Click UW Continue button
    Then I enter the required details for Drivers page
    And Click UW Continue button
    Then I enter the required details for Insurance history page
    And Click UW Continue button
    Then I enter the requred details for Accumulation page
    And Click UW Continue button
    Then I enter the reuired details for Summary and review page
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<Underwriter>"
    And I click login button
    When I search the case id that is routed and I begin the case
    And Enter the "<basepremium>" and "<notes>" and Submit the Policy
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "PENDING-PAYMENT"
    Then I logout of the application

    Examples:
      | Underwriter | product | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | notes   |
      | Underwriter | MVF     |                  1 |                  2 | testing           |         200 | Testing |

  @RegMLP3 @RegressionMLP3  @debugVICfleet @MLP3Fix @MLP3SearchFIX
  Scenario Outline: Create Motor vehicle from client portal - VicFleet
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "Vicflleet"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the required details "<ExecutiveVehicles1>""<ExecutiveVehicles1>" "<carrying vehicles>" for Vehicle details page
    And Click UW Continue button
    Then I enter the required details for Drivers page
    And Click UW Continue button
    Then I enter the required details for Insurance history page
    And Click UW Continue button
    Then I enter the requred details for Accumulation page
    And Click UW Continue button
    Then I enter the reuired details for Summary and review page
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<Underwriter>"
    When I search the case id that is routed and I begin the case
    And Enter the "<basepremium>" and "<notes>" and "<EnableOrDisableProrate>" prorata
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "Vicflleet"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "PENDING-PAYMENT"
    Then I logout of the application

    @RegressionMLP3Scenarios
    Examples:
      | Underwriter | product | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | notes   |status				     |EnableOrDisableProrate|
      | Underwriter | MVF     |                  1 |                  2 | testing           |         200 | Testing |PENDING-CLIENTACCEPTANCE| Default Yes |

  @RegMLP3debug1234 @RegressionMLP3 @ClientAdvisorPolicyAccatpanc @MLP3Fix @MLP3fixing @MLP3SearchFIX
  Scenario Outline: Start a new policy application for VicFleet Org MVF with deviation in client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "<ClientAdviser>"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<OrganizationName>" and Click find button from Newapplication page
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the required details "<ExecutiveVehicles1>""<ExecutiveVehicles1>" "<carrying vehicles>" for Vehicle details page
    And Click UW Continue button
    Then I enter the required details for Drivers page
    And Click UW Continue button
    Then I enter the required details for Insurance history page
    And Click UW Continue button
    Then I enter the requred details for Accumulation page
    And Click UW Continue button
    Then I enter the reuired details for Summary and review page
    Then I Click Finish button
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "<Underwriter>"
    When I search the case id that is routed and I begin the case
    And Enter the "<basepremium>" and "<notes>" and "<EnableOrDisableProrate>" prorata
    Then I submit the case to get routed to the user
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<ClientAdviser>"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "PENDING-PAYMENT"
    Then I logoff from client adviser portal

    @RegressionMLP3ScenariosTest
    Examples:
      | ClientAdviser | Underwriter | product | OrganizationName | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | notes   |EnableOrDisableProrate|
      | ClientAdviser | Underwriter | MVF     | Vicflleet        |                  1 |                  2 | testing           |         200 | Testing |Default Yes           |

  @MLP3Sprint32121 @UW @CON @PropAndPlan @NewApplication1234332111 @RegMLP3 @RegressionMLP3 @RegressionMLP111 @MLP3Fix1231212 @timeoutfix1212 @Rerun
  Scenario Outline: Start a new Construction Risks policy for Property and Planning client and then endorse it using Client Adviser Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I fill the Constrution Risk information questions
#    Then I click Add button to add a contract
#    Then I fill contract details "<ContractType>" "<EstimatedProjectValue>"
#    Then I fill the contract location details
#    Then I fill the Optional Extensions details "<LargestSingleContractVal>"
    ### Commented above 4 steps and updated with below 3 steps
    Then I click ViewEdit all contracts button
    Then I click on Add item button
    Then I fill the contract details "<ContractType>" "<EstimatedProjectValue>" and click on submit
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I logout of the application
    ##### Client Adviser - Apply Endorsement
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on update a policy
     And I search and select org using "<OrgClientSegment>" and "string"
    Then I select the policy
    And I note down the case ID
    And Click UW Continue button
    Then I fill updated contract details "<NewContractType>" "<NewEstimatedProjectValue>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I logoff from client adviser portal
    ##### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the endorsement premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<NewEstimatedProjectValue>" "<NewMaterialDamageMul>" "<NewLiabilityMul>" "<gst>" "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    #### Client Adviser - Accept quote
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser My cases tab
    Then I validate the endorsement premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<NewEstimatedProjectValue>" "<NewMaterialDamageMul>" "<NewLiabilityMul>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I validate the status of the case to be "PENDING-PAYMENT"
    Then I logoff from client adviser portal
    @RegressionMLP3ScenariosTest
    Examples:
      | OrgClientSegment | product                                                   | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
      | Regression_PAP   | Construction Risks - Material Damage and Liability Annual | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
      ### product was Construction Risks – Material Damage and Liability Annual,
  @MLP3Sprint3 @UW @CON @PropAndPlan @NewApplication @Cancellation11111 @RegressionMLP3 @MLP3Fix @timeoutfix @Rerun
  Scenario Outline: Start a new Construction Risks policy for Property and Planning client and then cancel the policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I fill the Constrution Risk information questions
#    Then I click Add button to add a contract
#    Then I fill contract details "<ContractType>" "<EstimatedProjectValue>"
#    Then I fill the contract location details
#    Then I fill the Optional Extensions details "<LargestSingleContractVal>"
    ### Commented above 3 steps and updated with below 3 steps
    Then I click ViewEdit all contracts button
    Then I click on Add item button
    Then I fill the contract details "<ContractType>" "<EstimatedProjectValue>" and click on submit
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    ##### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    ##### Client - Accept the quote and apply for policy cancellation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    ##### Cancel the policy
    And I Select the Prop and Planning Organisation
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile
    Then I logout of the application
    @RegressionMLP3Scenarios11
    Examples:
      | product                                                    | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | LargestSingleContractVal | IsCancelFullTerm| cancellationdate| Cancelreason  |
      | Construction Risks - Material Damage and Liability Annual  | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  |                   156000 | No              | 31/10/2022      | Property sold |
   ### product was  Construction Risks – Material Damage and Liability Annual , cancellationdate was 22/07/2021
  @RegressionMLP3 @ijksdssa @MLP3Fix @MLP3SearchFIX @Rerun
  Scenario Outline: Create Motor vehicle from client portal for Cemetry trust
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "Vicflleet"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    Then I enter the required details "<ExecutiveVehicles1>""<ExecutiveVehicles1>" "<carrying vehicles>" for Vehicle details page
    And Click UW Continue button
    Then I enter the required details for Drivers page
    And Click UW Continue button
    Then I enter the required details for Insurance history page
    And Click UW Continue button
    Then I enter the requred details for Accumulation page
    And I note down the case ID
    And Click UW Continue button
    Then I enter the reuired details for Summary and review page
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<Underwriter>"
    When I search the case id that is routed and I begin the case
    And Enter the "<basepremium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "Vicflleet"
    Then I open the approved case
    And I validate the "PENDING-CLIENTACCEPTANCE"
    Then I accept the quote
    And I verify the payment "PENDING-PAYMENT"
    Then I logout of the application

    @RegressionMLP3Scenarios
    Examples:
      | Underwriter | product        | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | UnderwritingNotes  |EnableOrDisableProrate|
      | Underwriter | Motor Vehicle  |                 1  |                 2  | testing           |         200 | Testing            |Default Yes           |


  Scenario Outline: Verify edit limits or sublimits and deductibles on finalise quote screen
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<PropAndPlanningOrgName>"
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance

    Examples:
      | ClientAdviser | Underwriter | product | OrganizationName | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | notes   |
      | ClientAdviser | Underwriter | MVF     | Vicflleet        |                  1 |                  2 | testing           |         200 | Testing |


  @MLP3Sprint11234567999   @Cancellation124212121 @MLP3Fix @MLP3SearchFIX @timeoutfix1211 @Rerun
  Scenario Outline: Start a new MVF policy for Property and Planning with deviation and then Cancel it - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the required details "<ExecutiveVehicles1>""<ExecutiveVehicles1>" "<carrying vehicles>" for Vehicle details page
    And Click UW Continue button
    Then I enter the required details for Drivers page
    And Click UW Continue button
    Then I enter the required details for Insurance history page
    And Click UW Continue button
    Then I enter the requred details for Accumulation page
    And Click UW Continue button
    Then I enter the reuired details for Summary and review page
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<Underwriter>"
    When I search the case id that is routed and I begin the case
    And Enter the "<basepremium>" and "<notes>" and Submit the Policy
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    #And I Select the Prop and Planning Organisation
    #Then I cancel a policy from more actions
    #Then I check the case status to be "<UWStatus>"
    #And I note down the case ID
    #Then I click the exit button
    #Then I verify that More actions is disabled for the policy tile
    #Then I logout of the application

    Examples:
      | Underwriter | product | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | notes   | UWStatus             |status|
      | Underwriter | MVF     |                  1 |                  2 | testing           |         200 | Testing | PENDING-UNDERWRITING |PENDING-CLIENTACCEPTANCE|

  @RegressionMLP3_Inprogress @RegressionMLP3 @finilaizeeee222 @MLP3Fix12312 @MLP3SearchFIX
  Scenario Outline: Start a new policy application for GPA with deviation and update base premium in finalize quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
#    Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I update base premium
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @RegressionMLP3Scenarios
    Examples:
      | product | updatedbasepremium | students | multiplier | gst | stampduty | startdate   | client_number | status                   | pendingstatus        | days | uwusername   | uwpassword  | paymentstatus   | notes       |name                         |
      | GPA     |               3000 |    15000 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |   98 | Underwriter1 | Welcome@321 | PENDING-PAYMENT | baseepremmq |RegressionMLP3_SchoolCouncil |

  @RegressionMLP3_Inprogress @RegressionMLP3 @testfixi @MLP3Fix @MLP3SearchFIX1233 @Rerun
  Scenario Outline: Start a new policy with Deviation and then endorse it for GPA
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    #Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<name>"
    And  Select policy and Click on Update Policy
    And Click UW Continue button
    Then value of students given "<updatedstudents>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application

    @RegressionMLP3Scenarios
    Examples:
      |name 	       | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status1              | status                   | days | paymentstatus   | endpaymentstatus | pendingstatus        | uwusername   | uwpassword  |
      |School Council S| GPA     |    11000 |             500 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE |  365 | PENDING-PAYMENT | PENDING-REFUND   | PENDING-UNDERWRITING | Underwriter1 | Welcome@321 |

  @RegressionMLP3 @Viclfeelet @MLP3FixDelegationLimit @timeoutfix @Rerun
  Scenario Outline: verify the error msg  Delegation limit is not sufficient to approve the premium limit exceeds 25000 as clilent Adviser
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "Vicflleet"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the required details "<ExecutiveVehicles1>""<ExecutiveVehicles1>" "<carrying vehicles>" for Vehicle details page
    And Click UW Continue button
    Then I enter the required details for Drivers page
    And Click UW Continue button
    Then I enter the required details for Insurance history page
    And Click UW Continue button
    Then I enter the requred details for Accumulation page
    And Click UW Continue button
    Then I enter the reuired details for Summary and review page
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<Underwriter>"
    When I search the case id that is routed and I begin the case
    And Enter the premium "<basepremium>" and uwnotes "<notes>"
    And I accept the quote for uw
    And I verify the message "<limitmsg>"
    Then I logout of the application

    @RegressionMLP3Scenarios
    Examples:
      | Underwriter | product | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | notes   | limitmsg                                                 | EnableOrDisableProrate |
      | Underwriter | MVF     |                  1 |                  2 | testing           |     2000000 | Testing | Delegation limit is not sufficient to approve the premium| Default yes            |

  @RegressionMLP3
  Scenario Outline: Start a new policy application for CYBER
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the revenue details "<priviousrevenue>" "<cuurentrevenue>"
    And Click Continue button
    Then I enter details in the Data protection "<ByWhom>" "<provide_details>" Screen
    And Click Continue button
    And I enter the details in Data access and recovery Screen
    And Click Continue button
    And I enter the details in  Outsourcing activities Screen
    And Click Continue button
    And I enter the details Claims information Screen
    And Click Continue button
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application

    @CyberProduct @RegressionMLP3Scenarios
    Examples:
      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details | OrgName      	     | Permium | UnderwritingNotes| finalstatus    | uwstatus                | EnableOrDisableProrate |
      | Cyber   |             100 |            200 | ect    | testing         | Automation_Ambulance | 500     | test             | PENDING-PAYMENT| PENDING-CLIENTACCEPTANCE| Default Yes            |

  Scenario Outline: Start a new Journey policy for Property and Planning client with deviation through Client portal and then endorse it through Client Adviser Portal - Newly Developed Script
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    Then I logout of the application
    ##### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I click on Exit Button
    ##### Client - Apply Endorsement and accept quote(Endorse from Client advisor)
    And I Select the Prop and Planning Organisation
    And Select policy and Click on Update Policy
    And Click UW Continue button
    Then I enter updated values of "<updatedfte>" and "<startdate>"
    And I select no previous journey insurance
    And I do not want to change anymore values
    And I note down the case ID
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<UWStatus>"
    Then I logout of the application
    ##### Client Adviser - Approve the quote(Underwrier approve)
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then I logoff from client adviser portal
    ### Client advisor steps required

    ### Client portal - Accept the quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    Then I validate the endorsement premium of Journey for "<fte>" "<updatedfte>" for "<startdate>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    Examples:
      | username                          | password     | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername    | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Regression_PAP   | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    2601 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |

  Scenario Outline: Start a new BTV policy for Property and Planning client with deviation and then endorse it through Client Adviser portal - Client Portal  - Newly Developed Script
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I enter International travel "<InternationalTrips>" details with NO travel to listed countries
    Then I enter Domestic travel "<DomesticTrips>" details
    Then I select Yes to travel exceeding 180 days and enter "<EstimateNumber>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    Then I logout of the application
    #### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    #### Client - Accept the quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I click on Exit Button
    ### Client Portal - Update Policy
    And I Select the Prop and Planning Organisation
    Then I select update policy from more actions
    Then I enter the start date of insurance "<startdate>"
    Then I enter International travel "<UpdatedInternationalTrips>" details with NO travel to listed countries
    Then I enter Domestic travel "<UpdatedDomesticTrips>" details
    Then I select No to travel exceeding 180 days
    And I do not want to change anymore values
    And I note down the case ID
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<UWStatus>"
    Then I logout of the application

#  PENDING-UNDERWRITING 		UWSTATUS
#  PENDING-CLIENTACCEPTANCE 	Status

    ##### Client Adviser - Approve the quote
    Given I have the URL of VMIA
    When I enter the "<clientadviserusername>"
    And I enter "<clientadviserpassword>"
    And I click login button
#    Then I am logged in to the client adviser portal
#    Then I click on update a policy
#    Then I search and select org using "<orgname>" and "<zipcode>"
#    Then I select the policy
#    And I note down the case ID
#    And Click UW Continue button
#    Then I enter International travel "<UpdatedInternationalTrips>" details with NO travel to listed countries
#    Then I enter Domestic travel "<UpdatedDomesticTrips>" details
#    Then I select Yes to travel exceeding 180 days and enter "<EstimateNumber>"
#    Then I enter the start date of insurance "<startdate>"
#    And I do not want to change anymore values
#    Then I note the case ID
#    And Click UW Continue button
#    And I check the declaration
#    Then I Click Finish button
#    And I verify the payment status "<UWStatus>"

#    Then I logoff from client adviser portal
#    Given I have the URL of VMIA
#    When I enter the "<clientadviserusername>"
#    And I enter "<clientadviserpassword>"
#    And I click login button
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then I logoff from client adviser portal
     ### Need to go back to client portal, so added the below four steps
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    Then I validate the endorsement premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<UpdatedInternationalTrips>" "<UpdatedDomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    Examples:
      | username                          | password     | orgname          | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips | EstimateNumber | uwusername   | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Regression_PAP   | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |            200 | Underwriter1 | Welcome@321 |    2601 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |


  Scenario Outline: Start a new GPA policy for Property and Planning client with deviation and then endorse it through Client Adviser portal - Client Portal  - Newly Developed Script
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and details of persons "<volunteers>"
    And I select No to previous GPA policy details
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    Then I logout of the application
    ##### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    ##### Client Adviser - Apply Endorsement and accept quote
    Given I have the URL of VMIA
    Given User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I search and select org using "<orgname>" and "<zipcode>"
    Then I select the policy
    And I note down the case ID
    And Click UW Continue button
    Then I enter updated values of PnP GPA "<startdate>" and details of persons "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>"
    And I select No to previous GPA policy details
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<UWStatus>"
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then I validate the endorsement premium of PnP GPA for "<volunteers>" "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>" from "<startdate>" using values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logoff from client adviser portal

    @GPAEndorsement
    Examples:
      | username                          | password     | orgname        | product | volunteers | updatedvolunteers | boardMembers | workExperienceStudents | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername   | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Regression_PAP | GPA     |     100001 |            100020 |           50 |                   4500 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Underwriter1 | Welcome@321 |    2601 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |