Feature: Regression suite for VMIA MLP1

  @RegressionMLP1 @NewChangeTest @SchoolGPA
  Scenario Outline: Start a new policy with Deviation and then endorse it for GPA
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    #And I Select the Organization "<name>" for "<product>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
#    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then Now I validate the premium for "<students>" for "<startdate>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    #Then I validate the premium for "<students>" for "<days>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
#    Then I enter number of "<updatedstudents>" and "<startdate>"
    Then value of students given "<updatedstudents>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
#    Then I validate the endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I validate the endorsement premium for "<students>" "<updatedstudents>" with endorsement effective date "<endEffectiveDate>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application
    ### No values where passed for startdate, pendingstatus so we have added here
    @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | name            | product | students | updatedstudents | status1              | status                   | paymentstatus   | endpaymentstatus | endEffectiveDate | startdate   | pendingstatus        |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Auto Claim Schl | GPA     | 11000    | 500             | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | PENDING-REFUND   | current date     | currentdate | PENDING-UNDERWRITING |

  @RegressionMLP1
  Scenario Outline: Start a new policy application for Liability with deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    And I am routed to UW Client Details page and validate the fields for "PPL" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<basepremium>" and "<notes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set1_golive_26 @RegressionMLP1Scenarios
    Examples:
      | EnableOrDisableProrate | hiredays           | product | basepremium | notes        | pendingstatus        | paymentstatus   | gst | stampduty | startdate  | client_number | status                   | name            |
      | Yes                    | More than 100 days | PPL     | 1000        | underwritten | PENDING-UNDERWRITING | PENDING-PAYMENT | 0.1 | 0.1       | currentdate| BUNIT-9514    | PENDING-CLIENTACCEPTANCE | Auto Claim Schl |
      # startdate was 1/09/2021
  @RegressionMLP1 @SchoolGPA
  Scenario Outline: Start a new policy application for GPA with no deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then Now I validate the premium for "<students>" for "<startdate>"
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set1 @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | product | students | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   | OrgName         |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | GPA     | 400      | 5.5        | 0.1 | 0.1       | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | 98   | PENDING-PAYMENT | Auto Claim Schl |

  @RegressionMLP1 @SchoolGPA
  Scenario Outline: Start a new policy application for GPA with no deviation and then update it before accepting quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then Now I validate the premium for "<students>" for "<startdate>"
    And I provide some "<action>" with value to be updated if required as "<updatedstudents>"
    Then Now I validate the premium for updated "<updatedstudents>" for "<startdate>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set1
    Examples:
      | username                          | password     | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   | action | OrgName         |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | GPA     | 400      | 500             | 5.5        | 0.1 | 0.1       | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | 98   | PENDING-PAYMENT | update | Auto Claim Schl |

  @RegressionMLP1 @SchoolGPA
  Scenario Outline: Start a new policy application for GPA with no deviation and withdraw it
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then value of students given "<students>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    And I provide some "<action>" with value to be updated if required as "<updatedstudents>"
    And I verify the cancelled "<cancelledstatus>"
    Then I logout of the application

    @Regression_MLP1_Set1 @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | product | students | status                   | action               | cancelledstatus    | OrgName         |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | GPA     | 400      | PENDING-CLIENTACCEPTANCE | Withdraw application | RESOLVED-WITHDRAWN | Auto Claim Schl |

  @RegressionMLP1 @SchoolGPA
  Scenario Outline: Start a new policy application for GPA with no deviation discuss with VMIA call us now
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then Now I validate the premium for "<students>" for "<startdate>"
    And I provide some "<action>" with value to be updated if required as "<updatedstudents>"
    And I verify the final status for updation "<status>"
    Then I logout of the application

    @Regression_MLP1_Set1
    Examples:
      | username                          | password     | product | students | startdate   | status                   | action              | cancelledstatus    | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | GPA     | 400      | currentdate | PENDING-CLIENTACCEPTANCE | discuss call us now | RESOLVED-CANCELLED | Auto Claim Schl |

  @RegressionMLP1 @SchoolGPA
  Scenario Outline: Start a new policy application for GPA with no deviation discuss with VMIA call back
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then Now I validate the premium for "<students>" for "<startdate>"
    And I provide some "<action>" with value to be updated if required as "<updatedstudents>"
    And I verify the final status for updation "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set1
    Examples:
      | username                          | password     | product | students | startdate   | status                   | paymentstatus    | action            |  name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | GPA     | 400      | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-CALLBACK | discuss call back |  Auto Claim Schl |

  @RegressionMLP1 @SchoolGPA
  Scenario Outline: Start a new policy application for GPA with no deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then Now I validate the premium for "<students>" for "<startdate>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set1
    Examples:
      | username                          | password     | product | students | startdate   | status                   | paymentstatus   | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | GPA     | 400      | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Auto Claim Schl |

#   Following scenario is already covered in 'Start a new policy with Deviation and then endorse it for GPA'
#  @RegressionMLP1
#  Scenario Outline: Start a new policy application for GPA with deviation
#    Given I have the external URL of VMIA
#    When User enter valid username  "<username>" and password "<password>" click on Login button
#    And I Select the Organization "<name>"
#    Then I want to start a new application for VMIA insurance
#    When I select "<product>" and submit
#    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
#    And Click UW Continue button
#    Then I enter number of "<students>" and "<startdate>"
#    And Click UW Continue button
#    And I validate the number of "<students>"
#    And I check the declaration
#    Then I Click Finish button
#    Then I check the status to be "<pendingstatus>"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "Underwriter"
#    When I search the case id that is routed and I begin the case
##    And I accept the quote for uw
#    And Now click on the approve quote
#    Then I submit the case to get routed to the user
#    Then I logout of the application
##    Then I close the external link
#    Given I have the external URL of VMIA
#    When User enter valid username  "<username>" and password "<password>" click on Login button
#    And I Select the Organization "<name>"
#    Then I open the approved case
#    And I validate the "<status>"
#    Then I accept the quote
#    And I verify the payment "<paymentstatus>"
#    Then I logout of the application
#
#    Examples:
#      | username                          | password     | product | students | multiplier | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername   | uwpassword  | paymentstatus   | name            |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | GPA     | 15000    | 5.5        | 0.1 | 0.1       | 1/07/2021 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING | 365  | Underwriter1 | Welcome@321 | PENDING-PAYMENT | Auto Claim Schl |

#  Following scenario is already covered in 'Start a new policy with Deviation and then endorse it for GPA'
#  @RegressionMLP1
#  Scenario Outline: Start a new policy application for GPA with deviation for a full year
#    Given I have the external URL of VMIA
#    When User enter valid username  "<username>" and password "<password>" click on Login button
#    And I Select the Organization "<name>"
#    Then I want to start a new application for VMIA insurance
#    When I select "<product>" and submit
#    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
#    And Click UW Continue button
#    Then I enter number of "<students>" and "<startdate>"
#    And Click UW Continue button
#    And I validate the number of "<students>"
#    And I check the declaration
#    Then I Click Finish button
#    Then I check the status to be "<pendingstatus>"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "Underwriter"
#    When I search the case id that is routed and I begin the case
#    Then Now I validate the premium for "<students>" for "<days>"
#    And I accept the quote for uw
#    Then I submit the case to get routed to the user
#    Then I logout of the application
#    Given I have the external URL of VMIA
#    When User enter valid username  "<username>" and password "<password>" click on Login button
#    And I Select the Organization "<name>"
#    Then I open the approved case
#    And I validate the "<status>"
#    Then I accept the quote
#    And I verify the payment "<paymentstatus>"
#    Then I logout of the application
#
#    Examples:
#      | username                          | password     | product | students | multiplier | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername   | uwpassword  | paymentstatus   | name            |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | GPA     | 11000    | 5.5        | 0.1 | 0.1       | 1/07/2021 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING | 98   | Underwriter1 | Welcome@321 | PENDING-PAYMENT | Auto Claim Schl |

  @RegressionMLP1 @SchoolGPA
  Scenario Outline: Start a new policy application for GPA with deviation and update base premium in finalize quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then Now I validate the premium for "<students>" for "<startdate>"
    Then I update base premium
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application
    @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | name            | product | updatedbasepremium | students | multiplier | gst | stampduty | startdate   | client_number | status                   | pendingstatus        | days | uwusername   | uwpassword  | paymentstatus   | notes       |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Auto Claim Schl | GPA     | 3000               | 15000    | 5.5        | 0.1 | 0.1       | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING | 98   | Underwriter1 | Welcome@321 | PENDING-PAYMENT | baseepremmq |

  @RegressionMLP1  @SchoolProperty
  Scenario Outline: Start a new policy application for School Property with STP
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    #Then I am logged in to the insurance portal Start a new policy application for property with deviation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    #Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<startdate>"
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application
    @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | product | value          | propertyvalue | minimumvalue | percentage | gst | stampduty | startdate   | client_number | status                   | pendingstatus        | days | uwusername   | uwpassword  | paymentstatus   | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | PRO     | Up to $250,000 | 100000        | 950          | 1          | 0.1 | 0.1       | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING | 98   | Underwriter1 | Welcome@321 | PENDING-PAYMENT | Auto Claim Schl |

  @RegressionMLP1
  Scenario Outline: Start a new policy application for property with deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<startdate>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application
    @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | product | value          | propertyvalue | minimumvalue | percentage | gst | stampduty | startdate   | client_number | status                   | pendingstatus        | days | uwusername   | uwpassword  | paymentstatus   | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | PRO     | Up to $250,000 | 100000        | 950          | 1          | 0.1 | 0.1       | 1/07/2022   | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING | 365  | Underwriter1 | Welcome@321 | PENDING-PAYMENT | Auto Claim Schl |

  @RegressionMLP1 @DebugSPR4
  Scenario Outline: Start a new policy application for property above 10 million with deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And I provide additional details for property above ten million "<propertyvalue>" "<singlelocation>" "<locationsecurity>" "<construction>" "<additionaldetails>" "<fireprotection>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<startdate>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    # And I click on Refresh policy
    Then I logout of the application
    @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | product | value         | propertyvalue | singlelocation | locationsecurity | construction       | additionaldetails | fireprotection | minimumvalue | percentage | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername   | uwpassword  | paymentstatus   | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | PRO     | Over $500,000 | 30000000      | Yes            | Local alarms     | Mixed construction | Timber            | Sprinklers     | 15000        | 0.125      | 0.1 | 0.1       | 1/07/2022 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING | 98   | Underwriter1 | Welcome@321 | PENDING-PAYMENT | Auto Claim Schl |

  @RegressionMLP1
  Scenario Outline: Start a new policy application for Liability with no deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    #And I verify the payment "<paymentstatus>"
    And I verify the payment status "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set1
    Examples:
      | username                          | password     | product | basepremium | paymentstatus   | hiredays          | gst | stampduty | startdate   | client_number | status                   | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | PPL     | 320         | PENDING-PAYMENT | Less than 50 days | 0.1 | 0.1       | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | Auto Claim Schl |

  @RegressionMLP1
  Scenario Outline: Start a new policy application for Liability with no deviation and risk question as yes with market stall event
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    Then I add "<marketstallholders>" and "<numberofdates>" as market stall events
    #And I provide an attachment having filepath as "<attachfilepath>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set2
    Examples:
      | username                          | password     | product | basepremium | marketstallholders | numberofdates | attachfilepath                 | paymentstatus   | hiredays          | gst | stampduty | startdate   | client_number | status                   | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | PPL     | 560         | 60                 | 3             | C:\\Shared\\assertion_file.txt | PENDING-PAYMENT | Less than 50 days | 0.1 | 0.1       | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | Auto Claim Schl |

  @RegressionMLP1
  Scenario Outline: Start a new policy application for Liability with deviation and risk question as yes with market stall event
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    Then I add "<marketstallholders>" and "<numberofdates>" as market stall events
    #    And I provide an attachment having filepath as "<attachfilepath>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    #Then I close the external link
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    #Then I individually provide the "<basepremium>" and "<notes>"
    Then I individually provide for Liability "<basepremium>" and "<notes>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set2
    Examples:
      | username                          | password     | uwusername   | uwpassword  | pendingstatus        | product | basepremium | notes        | attachfilepath                 | marketstallholders | numberofdates | paymentstatus   | hiredays          | gst | stampduty | startdate | client_number | status                   | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Underwriter1 | Welcome@321 | PENDING-UNDERWRITING | PPL     | 1000        | underwritten | C:\\Shared\\assertion_file.txt | 60                 | 12            | PENDING-PAYMENT | Less than 50 days | 0.1 | 0.1       | 1/07/20   | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | Auto Claim Schl |

  @RegressionMLP1
  Scenario Outline: Start a new policy application for MSC with no deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for "<parkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set2
    Examples:
      | username                          | password     | product | parkingslots | multiplier | gst | stampduty | startdate   | status                   | days | paymentstatus   | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | MSC     | 400          | 12         | 0.1 | 0.1       | currentdate | PENDING-CLIENTACCEPTANCE | 98   | PENDING-PAYMENT | Auto Claim Schl |

  @RegressionMLP1
  Scenario Outline: Start a new policy application for MSC with deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
#    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set1_golive_26 @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | uwusername   | uwpassword  | product                     | parkingslots | multiplier | gst | stampduty | startdate | status                   | days | paymentstatus   | pendingstatus        | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Underwriter1 | Welcome@321 | Motor – Special Contingency | 400          | 12         | 0.1 | 0.1       | 1/07/2022 | PENDING-CLIENTACCEPTANCE | 365  | PENDING-PAYMENT | PENDING-UNDERWRITING | Auto Claim Schl |

  @Regression_MLP1
  Scenario Outline: Start a new policy application for Motor Vehicle without deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the risk page "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for MV "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set2
    Examples:
      | username                          | password     | product | parkingslots | multiplier | gst | stampduty | startdate   | status                   | days | paymentstatus   | lightvehicleflag | busflag | trailerflag | otherflag | typeofvehicle | manufactureyear | make | model | regno | currentmarketvalue | numberofvehicles | pricelightvehicle | pricebusunder50 | pricebusover50 | pricetrailer | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | MVF     | 400          | 11.98      | 0.1 | 0.1       | currentdate | PENDING-CLIENTACCEPTANCE | 98   | PENDING-PAYMENT | true             | true    | true        | false     | SUV           | 2011            | ABC  | n97   | 8798  | 40000              | 1                | 450               | 620             | 891.78         | 50           | Auto Claim Schl |

  @RegressionMLP1
  Scenario Outline: Start a new policy application for Motor Vehicle with deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the risk page "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Then I close the external link
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    #Then I provide individually "<basepremium>" and "<notes>"
    #Then I validate the premium for MV for "<basepremium>" "<gst>" "<stampduty>" "<days>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application
    @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | uwusername   | uwpassword  | product | basepremium | notes           | gst | stampduty | startdate  | status                   | days | pendingstatus        | paymentstatus   | lightvehicleflag | busflag | trailerflag | otherflag | typeofvehicle | manufactureyear | make | model | regno | currentmarketvalue | numberofvehicles | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Underwriter1 | Welcome@321 | MVF     | 1000        | underwritten  1 | 0.1 | 0.1       | 01/07/2022 | PENDING-CLIENTACCEPTANCE | 98   | PENDING-UNDERWRITING | PENDING-PAYMENT | true             | false   | false       | false     | SUV           | 2011            | ABC  | n97   | 8798  | 40000              | 1                | Auto Claim Schl |

  @RegressionMLP1
  Scenario Outline: Initiate a Claim for Property
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "1" from claims pages
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details "<Where>" "<What>" "<How>"
    And Click Continue button
    Then I provide SCIP details "<purchasedescription>" "<cost>" "<salvagecost>"
    And Click Continue button
    Then I attach an evidence file "<filepath>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @Regression_MLP1_Set2
    Examples:
      | username                          | password     | loss     | Where   | What       | How          | purchasedescription | cost | salvagecost | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Property | Highway | TyreBusted | Overspeeding | Tyre                | 1000 | 500         | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 | 3475648       | Commonwealth Bank | comm@gm.com | Auto Claim Schl |

  Scenario Outline: Initiate a Claim for Liability
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    ## Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" "<addline1>" "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    # Then I attach one evidence file "<filepath>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @Regression_MLP1_Set2
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1 | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Public and product liability | Highway | liabClaimant | Sydney   | Central  | Chatswood | New South Wales | 9897     | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 | 3475648       | Commonwealth Bank | comm@gm.com | Auto Claim Schl |

  Scenario Outline: Initiate a Claim with no policy @P4
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    #(US-20, acceptance criteria - 7)
    Then No eligible policy validation message is displayed
    Then I logout of the application

    @Regression_MLP1_Set2_need
    Examples:
      | username                         | password      | loss                         |
      | sibam.chowdhury@areteanstech.com | Areteans@2021 | Public and product liability |

  #Negative scenario
  Scenario Outline: Initiate a Claim with future date
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the "<date>" and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    Then future date not allowed message is displayed
    Then I logout of the application

    @Regression_MLP1_Set2
    Examples:
      | username                          | password     | loss     | date                | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Property | 26/04/2030 11:30 AM | Auto Claim Schl |

  @Claims @Regression
  Scenario Outline: Initiate a Claim for GPA
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application

    @Regression_MLP1_Set2
    Examples:
      | username                          | password     | claimsofficerusername | claimsofficerpassword | loss                    | AccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | ClaimsOfficer         | Welcome@321           | Group Personal Accident | Flinders    | 123236 | 3475648       | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria | 2654     | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    | 2654           | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | Auto Claim Schl |

  @RegressionMLP1 @RegressionMLP1_Failed1 @Endorse @fixed1222111
  Scenario Outline: Start a new policy application and then make Endorsement for MSC
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
#    Then Now I validate the premium for "<parkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    Then I enter the number of "<updatedparkingslots>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set2 @Regression_MLP1_Set1_golive_26 @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | product                     | parkingslots | updatedparkingslots | multiplier | gst | stampduty | startdate   | status                   | days | endpaymentstatus | paymentstatus   | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Motor – Special Contingency | 400          | 500                 | 12         | 0.1 | 0.1       | currentdate | PENDING-CLIENTACCEPTANCE | 203  | PENDING-PAYMENT  | PENDING-PAYMENT | Auto Claim Schl |

  #
  #Scenario Outline: Start a new policy with Deviation and then endorse it for GPA
  #Given I have the external URL of VMIA
  #When I enter the "<username>" for azure
  #And I enter "<password>" for azure
  #And I click login button for azure
  #And I Select the Organization "<name>"
  #Then I am logged in to the insurance portal
  #Then I want to start a new application for VMIA insurance
  #When I select "<product>" and submit
  #Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
  #And Click UW Continue button
  #Then I enter number of "<students>" and "<startdate>"
  #And Click UW Continue button
  #And I validate the number of "<students>"
  #And I check the declaration
  #Then I Click Finish button
  #Then I check the status to be "<pendingstatus>"
  #Then I logout of the application
  #Given I have the URL of VMIA
  #When I enter the "<uwusername>"
  #And I enter "<uwpassword>"
  #And I click login button
  #When I search the case id that is routed and I begin the case
  #Then Now I validate the premium for "<students>" for "<days>"
  #And I accept the quote for uw
  #Then I submit the case to get routed to the user
  #Then I logout of the application
  #Given I have the external URL of VMIA
  #When I enter the "<username>" for azure
  #And I enter "<password>" for azure
  #And I click login button for azure
  #And I Select the Organization "<name>"
  #Then I open the approved case
  #And I validate the "<status>"
  #Then I validate the premium for "<students>" for "<days>"
  #Then I accept the quote
  #And I verify the payment "<paymentstatus>"
  #And I save the policy number
  #Then I logout of the application
  #Given I have the external URL of VMIA
  #When I enter the "<username>" for azure
  #And I enter "<password>" for azure
  #And I click login button for azure
  #And I Select the Organization "<name>"
  #And I click on Refresh policy
  # Then I want to update a policy
  #Then I want to update a policy for GPA
  #And Click UW Continue button
  #Then I enter number of "<updatedstudents>" and "<startdate>"
  #Then value of students given "<updatedstudents>"
  #And I do not want to change anymore values
  #And Click UW Continue button
  #And I check the declaration
  #Then I Click Finish button
  #And I validate the "<status>"
  #Then I validate the endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
  #Then I accept the quote
  #And I verify the payment for endorsement "<endpaymentstatus>"
  #Then I logout of the application
  #
  #
  #Examples:
  #| username                       | password      | name     | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   | endpaymentstatus | pendingstatus        | uwusername     | uwpassword  |
  #| manoj.ramanathan@areteanstech.com | Test@1234567 | Auto Claim Schl | GPA     |    11000 |             500 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |  365 | PENDING-PAYMENT | PENDING-PAYMENT  | PENDING-UNDERWRITING | Underwriter1 | Welcome@321 |
  Scenario Outline: Start a new policy and then update it for GPA for refund or no action
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    #Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then Now I validate the premium for "<students>" for "<days>"
    #Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    #  And I verify the payment "<paymentstatus>"
    And I verify the payment status "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    And I click on Refresh policy
    #Then I want to update a policy
    Then Select policy and Click on Update Policy
    # Then I select the policy to be updated
    And Click UW Continue button
    Then I enter number of "<updatedstudents>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    #Then I validate the endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set2
    Examples:
      | username                          | password     | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status                   | days | endpaymentstatus | paymentstatus   | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | GPA     | 500      | 200             | 5.5        | 0.1 | 0.1       | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | 203  | PENDING-REFUND   | PENDING-PAYMENT | Auto Claim Schl |

  @RegressionMLP1 @RegressionMLP1_Rerun15 @mpl1323454 @Rerun1231212
  Scenario Outline: Start a new policy with Deviation and then make 2 continuos Endorsement for GPA
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
#    Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then Now I validate the premium for "<students>" for "<startdate>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
#    Then I validate the premium for "<students>" for "<days>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<name>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    Then I enter number of "<updatedstudents>" and "<startdate>"
    Then value of students given "<updatedstudents>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
#    And I save the policy number
    And I click the exit button
    And I Select the Organization "<name>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    Then value of students given "<updatedstudents>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote by selecting no in the invoice no
    And I verify the payment for endorsement "<endpaymentstatus_1>"
    Then I logout of the application

    @Regression_MLP1_Set1_golive_26_Rerun @Regression_MLP1_Set1_golive_26 @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   | endpaymentstatus | pendingstatus        | uwusername   | uwpassword  | endpaymentstatus_1 | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | GPA     | 11000    | 500             | 5.5        | 0.1 | 0.1       | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | 365  | PENDING-PAYMENT | PENDING-PAYMENT  | PENDING-UNDERWRITING | Underwriter1 | Welcome@321 | RESOLVED-COMPLETED | Auto Claim Schl |

  @Endorse @Rerun123
  Scenario Outline: Start a new policy application for property and endorse it
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<startdate>"
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<updatedvalue>" as "<propertyvalue>"
    And I do not want to change anymore values
    And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for property "<propertyvalue>" "<minimumvalue>" "<updatedminimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set2 @Regression_MLP1_Set1_golive_26_Rerun @Regression_MLP1_Set1_golive_26 @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | product | value          | updatedvalue   | propertyvalue | minimumvalue | updatedminimumvalue | percentage | gst | stampduty | startdate   | client_number | status                   | pendingstatus        | days | uwusername   | uwpassword  | paymentstatus   | endpaymentstatus | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | PRO     | Up to $250,000 | Up to $500,000 | 100000        | 950          | 1500                | 1          | 0.1 | 0.1       | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING | 365  | Underwriter1 | Welcome@321 | PENDING-PAYMENT | PENDING-PAYMENT  | Auto Claim Schl |

  Scenario Outline: Start a new policy application for Liability with no deviation and risk question as yes and endorse it
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    #Then I enter the "<startdate>" of the insurance application
    Then I enter the Start Date  of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    #Then I want to update a policy
    Then I want to update a policy for Liability
    # Then I select the policy to be updated
    And Click UW Continue button
    # Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<updatedhiredays>"
    And I do not want to change anymore values
    #And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for liability for "<basepremium>" "<updatedbasepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set3
    Examples:
      | username                          | password     | product | basepremium | updatedbasepremium | paymentstatus   | endpaymentstatus | hiredays          | updatedhiredays | gst | stampduty | startdate | client_number | status                   | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | PPL     | 320         | 560                | PENDING-PAYMENT | PENDING-PAYMENT  | Less than 50 days | 50 to 100 days  | 0.1 | 0.1       | 1/07/2021 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | Auto Claim Schl |

  @RegressionMLP1 @RegressionMLP1_Rerun15_1 @Endorse @Rerun1
  Scenario Outline: Start a new policy application for Liability with no deviation and risk question as yes and endorse it negative premium
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    #Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    #Then I enter the "<startdate>" of the insurance application
    Then I enter the Start Date  of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    #And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    #Then I want to update a policy
    And Select policy and Click on Update Policy
    # Then I select the policy to be updated
    And Click UW Continue button
    # Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<updatedhiredays>"
    And I do not want to change anymore values
    #And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for liability for "<basepremium>" "<updatedbasepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set3 @Regression_MLP1_Set1_golive_26_Rerun1234 @Regression_MLP1_Set1_golive_26 @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | product | basepremium | updatedbasepremium | paymentstatus   | endpaymentstatus | hiredays       | updatedhiredays   | gst | stampduty | startdate | client_number | status                   | name               |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | PPL     | 560         | 320                | PENDING-PAYMENT | PENDING-REFUND   | 50 to 100 days | Less than 50 days | 0.1 | 0.1       | 1/07/2021 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | Automation_School  |
      # The Org was RegressionSet2_SchoolCouncils
  @RegressionMLP1 @RegressionMLP1_Failed12333 @Endorse
  Scenario Outline: Start a new policy application for Motor Vehicle without deviation and endorse it
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the risk page "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And Click UW Continue button
    #And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for MV "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the endorsement risk page "<Endorsedlightvehicleflag>" "<Endorsedbusflag>" "<Endorsedtrailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for MV "<Endorsedlightvehicleflag>" "<Endorsedbusflag>" "<Endorsedtrailerflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    # And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application

    @MVEndorsement @Regression @regb @Regression_MLP1_Set3_run @Regression_MLP1_Set1_golive_26 @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | product | parkingslots | multiplier | gst | stampduty | startdate   | status                   | days | endpaymentstatus | paymentstatus   | lightvehicleflag | Endorsedlightvehicleflag | busflag | Endorsedbusflag | trailerflag | Endorsedtrailerflag | otherflag | typeofvehicle | manufactureyear | make | model | regno | currentmarketvalue | numberofvehicles | pricelightvehicle | pricebusunder50 | pricebusover50 | pricetrailer | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | MVF     | 400          | 11.98      | 0.1 | 0.1       | currentdate | PENDING-CLIENTACCEPTANCE | 206  | PENDING-PAYMENT  | PENDING-PAYMENT | true             | false                    | true    | false           | false       | true                | false     | SUV           | 2011            | ABC  | n97   | 8798  | 40000              | 1                | 450               | 620             | 891.78         | 50           | Auto Claim Schl |

  @RegressionMLP1 @RegressionMLP1_Failed09041234 @finiance12311
  Scenario Outline: Start a new policy application for GPA with no deviation and then manually process payment
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then value of students given "<students>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When I enter the "<financeusername>"
    And I enter "<financepassword>"
    And I click login button
    ### Added single step in a method for below step to navigate to home page
    Then I manually process payment from finance portal
    Then I verify manual resolve "<resolvedstatus>"
    Then I logout of the application

    @Regression_MLP1_Set3_run @Regression_MLP1_Set1_golive_26_Rerun123 @Regression_MLP1_Set1_golive_26 @RegressionMLP1Scenarios
    Examples:
      | username                          | password     | financeusername | financepassword | product | students | multiplier | gst | stampduty | startdate | client_number | status                   | days | paymentstatus   | resolvedstatus    | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | finance@vmia    | rules           | GPA     | 400      | 5.5        | 0.1 | 0.1       | 1/07/2021 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | 365  | PENDING-PAYMENT | RESOLVED-MANUALLY | Auto Claim Schl |
    ### The financepassword was Welcome@2 then updated to rules
  Scenario Outline: Start a new policy application for Liability with no deviation and risk question as yes and manually process payment
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    #    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    Then I add "<marketstallholders>" and "<numberofdates>" as market stall events
    #And I provide an attachment having filepath as "<attachfilepath>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application
    Then I close the external link
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    Then I manually process payment from finance portal
    Then I verify manual resolve "<resolvedstatus>"
    Then I logout of the application

    @Regression_MLP1_Set3_run
    Examples:
      | username                          | password     | product | basepremium | marketstallholders | numberofdates | attachfilepath                 | paymentstatus   | hiredays          | gst | stampduty | startdate   | client_number | status                   | financeusername | financepassword | resolvedstatus    | name            |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | PPL     | 560         | 60                 | 3             | C:\\Shared\\assertion_file.txt | PENDING-PAYMENT | Less than 50 days | 0.1 | 0.1       | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | finance@vmia    | Welcome@2       | RESOLVED-MANUALLY | Auto Claim Schl |

  @RegressionMLP1 @MLP1fixes
  Scenario Outline: Raise a general service request in client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on new interaction
    And I select the organization for new interaction "<organization>"
    ### we should select the policy here
    Then I add task and do a general service request
    Then I enter the service request query parameters "<enquiryarea>" "<enquirycategory>" and I assert "<thankyoumessage>" "<gsrstatus>"
    Then I logoff from client adviser portal

    @Regression_MLP1_Set3 @Regression_MLP1_Set1_golive_26 @RegressionMLP1Scenarios
    Examples:
      | username       | password | organization | enquiryarea | enquirycategory | thankyoumessage                                                    | gsrstatus          |
      | ClientAdviser3 | rules    | Geelong      | Insurance   | Policy Coverage | The case, General enquiry is successfully created. Please confirm! | Resolved-Completed |
      # enquirycategory was Policy advice and also updated the thankyoumessage value
  Scenario Outline: Raise a general service request in client adviser portal pending investigation
    Given I have the URL of VMIA
    And User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on new interaction
    And I select the organization for new interaction "<organization>"
    Then I add task and do a general service request
    Then I enter the service request query parameters which cannot be handled via phone "<enquiryarea>" "<enquirycategory>" and I assert "<thankyoumessage>" "<gsrstatus>"
    Then I logoff from client adviser portal

    @Regression_MLP1_Set3
    Examples:
      | username          | password | organization | enquiryarea | enquirycategory | thankyoumessage                                            | gsrstatus             |
      | AutoClientAdviser | rules    | Geelong      | Insurance   | Policy advice   | General enquiry case successfully created, please confirm! | Pending-Investigation |

  Scenario Outline: Make a claim from client adviser portal liability
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on make a claim
    Then I enter business details for claim
    Then I want to make a claim for client adviser
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" "<addline1>" "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    #Then I attach an evidence file "<filepath>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logoff from client adviser portal

    @Regression_MLP1_Set3_run
    Examples:
      | AccountName | BSB    | AccountNumber | BankName          | EmailID     | username           | password | loss                         | what    | claimantname | addline1 | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        |
      | Flinders    | 123236 | 3475648       | Commonwealth Bank | comm@gm.com | AutoClientAdviser1 | rules    | Public and product liability | Highway | liabClaimant | Sydney   | Central  | Chatswood | New South Wales | 9897     | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED |

  @RegressionMLP1_Failed0904
  Scenario Outline: Initiate a claim from client adviser portal for GPA
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on make a claim
    Then I enter business details for claim
    Then I want to make a claim for client adviser
    And I enter the date and time of loss
    #And I select the loss type of "<loss>"
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logoff from client adviser portal

    @Regression_MLP1_Set3 @Regression_MLP1_Set1_golive_26
    Examples:
      | username       | password | claimsofficerusername | claimsofficerpassword | loss                    | AccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | ClientAdviser3 | rules    | ClaimsOfficer         | Welcome@321           | Group Personal Accident | Flinders    | 123236 | 3475648       | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb | Victoria | 2654     | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    | 2654           | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  Scenario Outline: Start a new policy application for GPA with no deviation in client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    #Then I want to start a new application for client adviser
    When I select "<product>" and submit
    #Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then value of students given "<students>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    #Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    #And I verify the payment "<paymentstatus>"
    Then I logoff from client adviser portal

    @Regression_MLP1_Set3
    Examples:
      | username          | password | product | students | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   |
      | AutoClientAdviser | rules    | GPA     | 400      | 5.5        | 0.1 | 0.1       | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | 303  | PENDING-PAYMENT |

  @ch2 @sl10 @slot2 @Sprint2 @UW @PRO @PRONoDeviation @Regression @reg3 @clientadviserPROIns @jj @P2 @check14213 @P2_01 @check14212
  Scenario Outline: Start a new policy application from client adviser for property with STP
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    #Then I want to start a new application for client adviser
    When I select "<product>" and submit
    #Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    # Then I enter the "<startdate>" of the insurance application
    Then I enter the Start Date  of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    Then I logoff from client adviser portal

    @Regression_MLP1_Set3
    Examples:
      | username          | password | product | value          | propertyvalue | minimumvalue | percentage | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername   | uwpassword  | paymentstatus   |
      | AutoClientAdviser | rules    | PRO     | Up to $250,000 | 100000        | 950          | 1          | 0.1 | 0.1       | 1/09/2021 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING | 303  | Underwriter1 | Welcome@321 | PENDING-PAYMENT |

  Scenario Outline: Start a new policy application for Liability from client adviser with no deviation
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    #Then I want to start a new application for client adviser
    #Then I click on new interaction
    When I select "<product>" and submit
    #Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    #Then I enter the "<startdate>" of the insurance application
    Then I enter the Start Date  of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    Then I logoff from client adviser portal

    @Regression_MLP1_Set3
    Examples:
      | username          | password | product | basepremium | paymentstatus   | hiredays          | gst | stampduty | startdate | client_number | status                   |
      | AutoClientAdviser | rules    | PPL     | 320         | PENDING-PAYMENT | Less than 50 days | 0.1 | 0.1       | 1/09/2021 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |

  @RegressionMLP11 @RegressionMLP1_Failed09041
  Scenario Outline: Start a new policy application for MSC in client adviser with no deviation
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    #Then I want to start a new application for client adviser
    When I select "<product>" and submit
    #Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    # Then I enter the number of "<parkingslots>"
    And Click UW Continue button
    #And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    #Then I validate the premium for "<parkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    #And I verify the payment status "<paymentstatus>"
    Then I logoff from client adviser portal

    @ClientAdviserMSCUW @jj @Regression_MLP1_Set3 @Regression_MLP1_Set1_golive_26
    Examples:
      | username       | password | product | parkingslots | multiplier | gst | stampduty | startdate   | status                   | days | paymentstatus   |
      | ClientAdviser3 | rules    | MSC     | 400          | 12         | 0.1 | 0.1       | currentdate | PENDING-CLIENTACCEPTANCE | 303  | PENDING-PAYMENT |

  Scenario Outline: Start a new policy application for Motor Vehicle in client adviser without deviation
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    #Then I want to start a new application for client adviser
    When I select "<product>" and submit
    #Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    #Then I enter the start date of insurance "<startdate>"
    Then I enter the Start Date  of the insurance application
    Then I fill up the MV details in the risk page "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And Click UW Continue button
    #And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    #Then I validate the premium for MV "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    Then I logoff from client adviser portal

    @ClientAdviserMWUW @Regression_MLP1_Set3
    Examples:
      | username          | password | product | parkingslots | multiplier | gst | stampduty | startdate | status                   | days | paymentstatus   | lightvehicleflag | busflag | trailerflag | otherflag | typeofvehicle | manufactureyear | make | model | regno | currentmarketvalue | numberofvehicles | pricelightvehicle | pricebusunder50 | pricebusover50 | pricetrailer |
      | AutoClientAdviser | rules    | MVF     | 400          | 11.98      | 0.1 | 0.1       | 1/09/2021 | PENDING-CLIENTACCEPTANCE | 303  | PENDING-PAYMENT | true             | true    | true        | false     | SUV           | 2011            | ABC  | n97   | 8798  | 40000              | 1                | 450               | 620             | 891.78         | 50           |

  Scenario Outline: Start a new policy and then endorse it for GPA in client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then value of students given "<students>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I accept the quote
    #And I verify the payment "<paymentstatus>"
    # And I save the policy number
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
    # Then I want to start a new application for client adviser
    And Click UW Continue button
    # Then I enter number of "<updatedstudents>" and "<startdate>"
    Then value of students given "<updatedstudents>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    #Then I validate the endorsement premium for "<students>" "<updatedstudents>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logoff from client adviser portal

    @Regression_MLP1_Set3_123
    Examples:
      | username          | password | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   | endpaymentstatus |
      | AutoClientAdviser | rules    | GPA     | 200      | 500             | 5.5        | 0.1 | 0.1       | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | 365  | PENDING-PAYMENT | PENDING-PAYMENT  |

  @Debug11234124
  Scenario Outline: Initiate a Claim for GPA and check reports and triage claim in claims officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I check the reports for status "<statusbeforetriaging>"
    Then I logout of the application

    @ClaimsOfficerReportsTriage @Regression_MLP1_Set3 @Regression_MLP1_Set1_golive_26
    Examples:
      | username                          | password     | claimsofficerusername | claimsofficerpassword | loss                    | AccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | name            | statusbeforetriaging |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | ClaimsOfficer1R3      | Welcome@1234          | Group Personal Accident | Flinders    | 123236 | 3475648       | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria | 2654     | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    | 2654           | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | Auto Claim Schl | Pending-Triage       |

  @RegressionMLP1_last
  Scenario Outline: Start a new policy application for Motor Loss of No Claims Bonus Risk Information with deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    And enter number of Vehicles "<number_Vehicles>"and Time period for using Vehicles
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    #Then I close the external link
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set1_golive_26 @Regression_MLP1_Set3 @Regression_MLP1_Set1_golive_26_Rerun123
    Examples:
      | product | startdate   | number_Vehicles | pendingstatus        | status                   | paymentstatus   | uwusername   | uwpassword  |
      | MLNCB   | currentdate | 200             | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Underwriter1 | Welcome@321 |
#Note