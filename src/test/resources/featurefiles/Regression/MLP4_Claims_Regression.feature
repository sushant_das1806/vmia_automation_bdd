Feature: Regression suite for VMIA MLP4 Claims

  @AddAsset  @claims @Asset @rregressoion121
  Scenario Outline: Verify Risk adviosr is able to add asset and underwriter approve the same asset
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Add asset" in AssetManagement
    And Select the organization "<organization>" for Adding Asset
    And Select the contact associate with request "No"
    And Click Continue Button
#      And Add Asset Details "<Location>" "<AssetType>" "<AssetName>" "<AssetBand>"
#      And Click Continue Button
#      And Click Continue Button
#      And Click Finish Button
#      And Capture the Asset Details and Click submit asset
#      Then Verify the case status "PENDING- APPROVAL" and capture the case id
#      Then I logoff from client adviser portal
#      And I have the URL of VMIA
#      When User when logs in as "UWPortfoliaManger"
#      And Open Asset from "Asset Management" Underwriter Queue
#      Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
#      Then I check the case status to be "<FinalStatus>"
#      And I logout of the application
#      Given I have the URL of VMIA
#      When I enter the "<username>"
#      And I enter "<password>"
#      And I click login button
#      And Click New and Select "Search asset" in AssetManagement
#      And Search the asset by id "" and open the asset "History"
#      And Validate the following info in assethistory section of assettype "<AssetName>" AssetName "<AssetType>" and AssetID
    @AddAsset @dssasaa
    Examples:
      | username          | password     | organization | Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStatus|
      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED|

  @UpdateAsset  @claims @Asset @RegressionMLP4Claims121
  Scenario Outline: Verify Risk adviosr is able to Update and underwriter approve the same asset
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Update asset" in AssetManagement
    And Select the organization "<organization>" for Adding Asset
    And Select the contact associate with request "No"
    And Click Continue Button
    And Search the Assetby "Asset name" using value "<AssetName>"
    And Select the Asset result by name "<AssetName>"
    And Update the selected asset by "<AssetName>"
    And Click Continue Button
    And Click Continue Button
    And Click Finish Button
    And Capture the Asset Details and Click submit asset
    Then Verify the case status "PENDING- APPROVAL" and capture the case id
    Then I logoff from client adviser portal
    And I have the URL of VMIA
    When User when logs in as "UWPortfoliaManger"
    And Open Asset from "Asset Management" Underwriter Queue
    Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
    Then I check the case status to be "<FinalStaus>"
    And I logout of the application

    Examples:
      | username          | password     | organization | Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStaus|
      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED |

  @DisposeAsset  @claims @Asset @RegressionMLP4Claims
  Scenario Outline: Verify Risk adviosr is able to Dispose the asset and underwriter approve the same asset
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Dispose asset" in AssetManagement
    And Select the organization "<organization>" for Adding Asset
    And Select the contact associate with request "No"
    And Click Continue Button
    And Search the Assetby "Asset name" using value "test"
    And Select the Asset result by name "test"
    And Click Continue Button
    And Click Continue Button
    And Click Finish Button
    And Capture the Asset Details and Click submit asset
    Then Verify the case status "PENDING- APPROVAL" and capture the case id
    Then I logoff from client adviser portal
    And I have the URL of VMIA
    When User when logs in as "UWPortfoliaManger"
    And Open Asset from "Asset Management" Underwriter Queue
    Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
    Then I check the case status to be "<FinalStaus>"
    And I logout of the application
    Examples:
      | username          | password     | organization | Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStaus|
      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED |

  @TransferAsset  @claims @Asset @RegressionMLP4Claims
  Scenario Outline: Verify Risk adviosr is able to Transfer the asset and underwriter approve the same asset
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Transfer asset" in AssetManagement
    And Select the organization "<organization>" for Adding Asset
    And Select the contact associate with request "No"
    And Click Continue Button
    And Select the organization "<DestinationOrg>" for Adding Asset
    And Click Continue Button
    And Search the Assetby "Asset name" using value "Test"
    And Select the Asset result by name "Test"
    And Click Continue Button
    And Click Continue Button
    And Click Finish Button
    And Capture the Asset Details and Click submit asset
    And Link the policy "V010874" to the asset
    Then Verify the case status "PENDING- APPROVAL" and capture the case id
    Then I logoff from client adviser portal
    And I have the URL of VMIA
    When User when logs in as "UWPortfoliaManger"
    And Open Asset from "Asset Management" Underwriter Queue
    Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
    Then I check the case status to be "<FinalStatus>"
    And I logout of the application

    Examples:
      | username          | password     | organization |DestinationOrg| Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStatus|
      | AutoClientAdviser | Welcome@1234 | Scotland PNP |Lowanna27      |Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED|

  @BulkUploadAsset  @claims @Asset @RegressionMLP4Claims
  Scenario Outline: Verify Risk adviosr is able to upload the bulk upload template
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Bulk add/update assets" in AssetManagement
    And Attach the bulk upload file "<file>" and click submit
    Then I logoff from client adviser portal
    And I have the URL of VMIA
    When User when logs in as "UWPortfoliaManger"
    And Open Asset from "Asset Management" Underwriter Queue
    Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
    Then I check the case status to be "<FinalStatus>"
    And I logout of the application

    Examples:
      | username          | password     | file														  | Action |FinalStatus|  IsEndorseNeed|
      | AutoClientAdviser | Welcome@1234 |BulkUploadTemplateTest_Updated.csv|Approve|RESOLVED-COMPLETED|No|

  @TestCheck123
  Scenario Outline: Start a new policy application for Combined Libality
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    And Enter the Annual renveue "<OperationalRevenue>" "<VicrorianRevenue>" "<OtherFund>"
    And Enter the Incident, OverStateWork, AffilationWorkDetails
    And Enter the Storage Hazard details "<activityCasuedDamege>" "<presenceWater>"
    And Enter the Product, Airfield Liablity and Contract info "<Contractorinfo>"
    And Click UW Continue button
    And Add visitor info "<NoOfVisitor>" "<StreetName>" "<Subrubtown>" "<State>" "<postcode>"
    And Enter the Professional Indeminity info
    And Enter the insurance history details
    And Enter the Professional ActivityInfo
    And Enter the Join ventures and overseaswork
    And Enter the contract info for liablilty
    And Enter the Risk management details
    And Enter the claim circumstances details
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application

    @CombinedLiability @PAP @NewBusiness
    Examples:
      | product           |OrgName        |OperationalRevenue|VicrorianRevenue|OtherFund|Subrubtown|NoOfVisitor|State   |postcode|activityCasuedDamege|StreetName|presenceWater |Contractorinfo                                        |Permium|UnderwritingNotes|finalstatus    |uwstatus                |
      | Combined liability|PAPautomation  |500               |600             |700      |Sydney    |3          |Victoria|6421    |Not applicable      |Main Road|Not applicable|We do not engage Contractors, Sub Contractors or Agents|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|
#    @CombinedLiability  @DepartmentalDivision @NewBusiness
#    Examples:
#      | product           |OrgName                        |OperationalRevenue|VicrorianRevenue|OtherFund|Subrubtown|NoOfVisitor|State   |postcode|activityCasuedDamege|StreetName|presenceWater |Contractorinfo                                        |Permium|UnderwritingNotes|finalstatus    |uwstatus                |
#      | Combined liability|Automation_DepartmentalDivision|500               |600             |700      |Sydney    |3          |Victoria|6421    |Not applicable      |Main Road|Not applicable|We do not engage Contractors, Sub Contractors or Agents|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|

  @MLP4 @Indeminity
  Scenario Outline: Verify user is able to add document and verify the history for the same
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I am logged in to the insurance portal
    Then I want to make a claim from TriageOfficer
    Then Search and select Oraganization "<Organization>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    And Open the claim from "Triage Claims" queue
    And Add attachment with details following details "<AttachfilePath>" "<AttachmentCategeroy>" "<Author>" "<AttachDocDate>"
    And Verify the history of the document "<AttachfilePath>" "<AttachmentCategeroy>" "<AttachDocDate>"
    And Updated the attachment data  "<NewFilanamename>" "<UpdatedAuthor>" "<AttachmentCategeroy>" "<AttachDocDate>"
    Then I logout of the application

    Examples:
      |Organization        | TriageOfficer |  loss                    |  finalstatus       |AttachfilePath            |Author    |AttachDocDate|AttachmentCategeroy|UpdatedAuthor|NewFilanamename|
      |Rahara Cemetery Trust| TriageOfficer| Professional indemnity | RESOLVED-COMPLETED |Claim_check_attachment.txt|AutoTester|20/06/2021   |Claim              |tester      |UpdatedFile|
