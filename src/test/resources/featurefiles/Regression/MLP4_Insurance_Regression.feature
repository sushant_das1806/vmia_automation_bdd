Feature: Regression suite for VMIA MLP4 Insurance

  @RegressionMLP4121211
  Scenario Outline: Start a new policy application and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MPL4 @Auto_Law_Jus @Group_Personal_Accident_CFA @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Group Personal Accident CFA             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

#    @MPL4 @Auto_cem_trust @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance
#    Examples:
#      | decsion |  Permium | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
#      | No      |   10     | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto cem trust   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#
#    @MPL4 @Auto_pro_plan @Group_Personal_Accident_Heritage_Divers @PolicyCreation @clientacceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
#      | No      |   10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan  | Group Personal Accident - Heritage Divers | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @MPL4 @Auto_Law_Jus @Public_Products_Liability_Covid19 @PolicyCreation @clientacceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
#      | No      |     10    | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   | Public & Products Liability - Covid19     | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @MPL4 @Auto_Law_Jus @Group_Personal_Accident @PolicyCreation @clientacceptance @RegressionMLP41234
#    Examples:
#      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
#      | No      |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w  |  Group Personal Accident - Alpine Reserve	 | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline: Start a new policy application for EmRepss policy
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Enter the policy effective date "<PolicyStartDate>" and capture the Expirydate
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And validate the pro rata premium for the emRepss policy "Creation"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal


    @MPL4 @Auto_Law_Jus @Group_Personal_Accident_CFA @PolicyCreation @clientacceptance
    Examples:
      |orgname        |product                       | status               | uwstatus                 | finalstatus     |PolicyStartDate |
     |Auto Law & Jus |Directors & Officers - EmRePSS| PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | 21/06/2021     |

  @BundlePolicycreation
  Scenario Outline: Create the Bundle Policy and verify the mail is exist
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click CreateNew button
    And Enter the Orgdetails "Community Service Organisation" "Department of Education and Training" "tester" "8508356502" "Sydney "
    And Click New and Select "Manage contact"
    Then Search the contact using contactid "CONT-27587"
    And Add the contact to Organzation to the contact "" "Insurance;Finanace"
    And Click New and Select "Create policy bundle"
    And Select orgnazation by searching clientsegment "Community Service Organisation" and orgname
    Then Add the unincorproted orgnization "Test" and finish the policy
    Then Verify case status "RESOLVED-COMPLETED" and capture the case id
    And Click on the package case
    And Verify the policy document mail is present in attahcment tab
    Then I logoff from client adviser portal
    And I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And Select the Bundle Policy Orgnization
    And Verify all the policies are created and more action is disabled
    Then Check Apply for policy button is not displayed
    And I logoff from client adviser portal


    Examples:
      | username          | password     |
      | AutoClientAdviser | Welcome@1234 |


  Scenario Outline: Cancelling the basic product policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Group Personal Accident CFA              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
#
    @MPL4 @Auto_cem_trust @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |   10     | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto cem trust   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|

    @MPL4 @Auto_pro_plan @Group_Personal_Accident_Heritage_Divers @PolicyCreation @clientacceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |   10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan  | Group Personal Accident - Heritage Divers | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
    @MPL4 @Auto_Law_Jus @Public_Products_Liability_Covid19 @PolicyCreation @clientacceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |     10    | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   | Public & Products Liability - Covid19     | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|
    @MPL4 @Auto_Law_Jus @Group_Personal_Accident @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w          |  Group Personal Accident - Alpine Reserve | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |Yes             | 22/07/2021     |Property sold|
    @MPL4 @Auto_Law_Jus @Group_Personal_Accident @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w          |  Group Personal Accident - Flight Risks    | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|




  Scenario Outline: Start a new policy application for PPL product and select client acceptance in client portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Add Dedctible for the PPL policy
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"

    @MPL4 @Auto_Law_Jus @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  Scenario Outline: Start a new policy application for basic product and select client acceptance in risk advisor portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Add Dedctible for the professional Indeminity
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal


    #
    @MPL4 @Auto_Law_Jus @Professional_Indemnity @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | yes      |    10      | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Law & Jus   |Professional Indemnity              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |