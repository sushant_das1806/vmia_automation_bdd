Feature: Reapplying for existing product via internal portal user

  Scenario Outline: Start a new policy Basic application for Education Clients and Reapply from internal portal

    Given I have the external URL of VMIA
     When I enter the "<username>" for azure
     And I enter "<password>" for azure
     And I click login button for azure
     And I Select the Organization "<orgname>"
     Then I want to start a new application for VMIA insurance
     And click on the select button of the "<product>"
     And I capture the policy ID
     And Click UW Continue button
     And Click UW Continue button
     Then I Click Finish button
     And I validate the "<status>"
     Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
   Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    Then Click on all products in segments in client advisor
    And click on the select button of the "<product>"
    And Click "Yes" to reapply the policy
   And I capture the policy ID
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
 	When User when logs in as "RiskAdvisor"
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal


#    @MPL4 @AutoEdu @PolicyCreation @clientacceptance @Fine_Art_Exhibitions_Static
#    Examples:
#      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | AutoEdu         |Fine Art Exhibitions - Static              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#
   @MPL4 @AutoB&I @PolicyCreation @clientacceptance @Fine_Art_Exhibitions_Static
   Examples:
    | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
    | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | AutoB&I         |Fine Art Exhibitions - Static              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#
#   @MPL4 @Auto_Gov_&_Eco @PolicyCreation @clientacceptance @Fine_Art_Exhibitions_Static
#   Examples:
#    | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#    | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | Auto Gov & Eco         |Fine Art Exhibitions - Static              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#
#   @MPL4 @Auto_Law_&_Jus @PolicyCreation @clientacceptance @Fine_Art_Exhibitions_Static
#   Examples:
#    | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#    | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | Auto Law & Jus       |Fine Art Exhibitions - Static              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#
#   @MPL4 @AutoPro_&_plan  @PolicyCreation @clientacceptance @Fine_Art_Exhibitions_Static
#   Examples:
#    | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname                  |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#    | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | Auto pro & plan          |Fine Art Exhibitions - Static              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#
#   @MPL4 @Auto_cem_trust @PolicyCreation @clientacceptance @Fine_Art_Exhibitions_Static
#   Examples:
#    | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname                |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#    | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | Auto cem trust         |Fine Art Exhibitions - Static              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
