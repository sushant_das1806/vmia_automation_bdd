Feature: Claims

  @Sprint1
  Scenario Outline: Initiate a Claim
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    #Following statement also verifies that the policy years listed are as per business req (US-20, acceptance criteria - 4,5,6)
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #(US-20, acceptance criteria - 1)
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    #And Click Continue button
    Then Click Save and Exit button
    Then I logout of the application

    Examples: 
      | username       | password  | loss                    |
      | flindersschool | Rules@123 | Property                |
      | flindersschool | Rules@123 | Group Personal Accident |

  #Negative scenario
  @Sprint1 @Negative
  Scenario Outline: Initiate a Claim with no policy
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    #(US-20, acceptance criteria - 7)
    Then No eligible policy validation message is displayed
    Then I logout of the application

    Examples: 
      | username       | password  | loss                         |
      | flindersschool | Rules@123 | Public and Product Liability |

  #Negative scenario
  @Sprint1 @Negative
  Scenario Outline: Initiate a Claim with future date
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the "<date>" and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    Then future date not allowed message is displayed
    Then I logout of the application

    Examples: 
      | username       | password  | loss     | date                |
      | flindersschool | Rules@123 | Property | 26/04/2030 11:30 AM |

  @Sprint
  Scenario Outline: Initiate a Claim and add loss details
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    #Following statement also verifies that the policy years listed are as per business req (US-20, acceptance criteria - 4,5,6)
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #(US-20, acceptance criteria - 1)
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    #Sprint1 upto here
    And Click Continue button
    Then I provide loss details "<Where>" "<What>" "<How>"
    And Click Continue button
    Then I logout of the application

    Examples: 
      | username       | password  | loss     | Where   | What     | How         |
      | flindersschool | Rules@123 | Property | Kolkata | Keyboard | Waterlogged |
      
