#Author: pavan.gangone@pega.com
#USer Stroy Discription:users can view payees, edit and add new left side menu
#Epicid :, User story:US-8009
#Sprint 2
#Test case coverage || Total TC's - xx || Total automated TC's - xx.

Feature: MLP3 Sprint2 claims Scenarios
#ScenariosMLP3SP#2_1
@MLP3_Sprint2-Rerun
  Scenario Outline: Create payee and verify the in claims portal
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then Click on Manage Components link after clicking on hamburger menu
    And Click on Add Payee from Manage Componet page
    And Verify the landed to Manage components page "<PageTitle>"
    And User enter the following required details "<PayeeType>" "<payeeName>" "<payeeAccname>""<payeebsb>" "<payeeAccNum>""<payeeBankname>""<payeeEmail>"for Adding a payee
    Then User click on product attachemt "<attachement>" select the required document
    And Click on the submit and Verify the status "<Payee_Status>"
    And User extract the caseID after submit the payee
    Then User logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "<RiskAdmin>"
    Then User search the claim file and procede with aprrovals as a risk admin  "<Payee_Status >" "<finalstatus>"
    Then User logout of the application
    When User when logs in as "<TriageOfficer>"
    Then Click on Manage Components link after clicking on hamburger menu
    And User search for the payee with and verify the name of the payee
    Then User logout of the application

    
    Examples: 
      | TriageOfficer | RiskAdmin | PageTitle         | PayeeType | payeeName  | payeeAccname | payeebsb | payeeAccNum | payeeBankname | payeeEmail | Payee_Status     | finalstatus |attachement|
      | TriageOfficer | RiskAdmin | Add payee details | Employee  | AutoTester | SalaryBased  |   123456 |   123456789 | VimaBnk       | a@b.com    | PENDING-APPROVAL | Approved    |C:\\Users\\pega\\Document\\Vima1.txt|
#ScenariosMLP3SP#2_2
@MLP3_Sprint2
  Scenario Outline: Approve the updated payee details by risk admin in the claims portal
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then Click on Manage Components link after clicking on hamburger menu
    And Click on Add Payee from Manage Componet page
    And Verify the landed to Manage components page "<PageTitle>"
    And User enter the following required details "<PayeeType>" "<payeeName>" "<payeeAccname>""<payeebsb>" "<payeeAccNum>""<payeeBankname>""<payeeEmail>"for Adding a payee
    And Click on the submit and Verify the status "<Payee_Status>"
    And User extract the caseID after submit the payee
    Then User logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "<RiskAdmin>"
    Then User search the claim file and procede with aprrovals as a risk admin  "<Payee_Status >" "<finalstatus>"
    Then User logout of the application
    When User when logs in as "<TriageOfficer>"
    Then Click on Manage Components link after clicking on hamburger menu
    And User search for the payee with and verify the name of the payee
    And User select the payee which added and click Modify option from Action list
    And User update the payeename after click modify from Actions list  "<upDatePayeeName>"
    And Click on the submit and Verify the status "<Payee_Status>"
    Then User logout of the application

    
    Examples: 
      | TriageOfficer | RiskAdmin | PageTitle         | PayeeType | payeeName     | payeeAccname | payeebsb | payeeAccNum | payeeBankname | payeeEmail | Payee_Status     | upDatePayeeName | finalstatus |
      | TriageOfficer | RiskAdmin | Add payee details | Employee  | AutoTester123 | SalaryBased  |   123456 |   123456789 | VimaBnk       | a@b.com    | PENDING-APPROVAL | Autotester2     | Approved    |
#ScenariosMLP3SP#2_3
@MLP3_Sprint2
  Scenario Outline: Reject the create payee as a risk admin
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then Click on Manage Components link after clicking on hamburger menu
    And Click on Add Payee from Manage Componet page
    And Verify the landed to Manage components page "<PageTitle>"
    And User enter the following required details "<PayeeType>" "<payeeName>" "<payeeAccname>""<payeebsb>" "<payeeAccNum>""<payeeBankname>""<payeeEmail>"for Adding a payee
    And Click on the submit and Verify the status "<Payee_Status>"
    And User extract the caseID after submit the payee
    Then User logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "<RiskAdmin>"
    Then User search the claim file and procede with reject as a risk admin  "<Payee_Status >" "<finalstatus>"
    Then User logout of the application
    When User when logs in as "<TriageOfficer>"
    Then Click on Manage Components link after clicking on hamburger menu
    And User search for the payee with and verify the name of the payee
    Then User logout of the application

    
    Examples: 
      | TriageOfficer | RiskAdmin | PageTitle         | PayeeType | payeeName  | payeeAccname | payeebsb | payeeAccNum | payeeBankname | payeeEmail | Payee_Status     | finalstatus |
      | TriageOfficer | RiskAdmin | Add payee details | Employee  | AutoTester | SalaryBased  |   123456 |   123456789 | VimaBnk       | a@b.com    | PENDING-APPROVAL | Approved    |

  #Author: pavan.gangone@pega.com
  #USer Stroy Discription:users can view payees, edit and add new left side menu
  #Epicid :, User story:US-6014
  #Sprint 2
  #Test case coverage || Total TC's - 9 || Total automated TC's -9.
  
  #ScenariosMLP3SP#2_4
@MLP3_Sprint2
  Scenario Outline: Create Legal panel and verify the in claims portal
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then Click on Manage Components link after clicking on hamburger menu
    And Click on Leagal Payee from Manage Componet page
    And User enters the required fileds "<leagalName>" "<SelectClaimsType>" "<Address1>" "<Address2>" "<suburb>" "<postcode>" "<SolicitoName>" "<SolicitoEmail>" "<SolicitoPhone>"for AddLegalPanel
    And Click Submit from Add legal panel and search added leagal panel from the list "<leagalName>"
    And Verify the details of added leagal peanel "<SelectClaimsType>"
    Then User logout of the application

    Examples: 
      | TriageOfficer | leagalName         | SelectClaimsType | Address1          | Address2          | suburb  | postcode | SolicitoName | SolicitoEmail | SolicitoPhone | Payee_Status | finalstatus |
      | TriageOfficer | PavanAutomation123 | Motor            | Street1AutoTester | Street2AutoTester | Tester1 |     4001 | Tester1      | a@b.com       |      12345678 | Approved     | text        |
#ScenariosMLP3SP#2_4
@MLP3_Sprint2
  Scenario Outline: Updates Legal panel and verify updated leagal panel the in claims portal
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then Click on Manage Components link after clicking on hamburger menu
    And Click on Leagal Payee from Manage Componet page
    And User enters the required fileds "<leagalName>" "<SelectClaimsType>" "<Address1>" "<Address2>" "<suburb>" "<postcode>" "<SolicitoName>" "<SolicitoEmail>" "<SolicitoPhone>"for AddLegalPanel
    And Click Submit from Add legal panel and search added leagal panel from the list "<leagalName>"
    And Verify the details of added leagal peanel "<SelectClaimsType>"
    And User selects the update option from Action list for added leagal panel
    And User the updates the leagal panel details "<updateLegalPanelName>"
    And Click Submit from Add legal panel and search added leagal panel from the list "<updateLegalPanelName>"
    And verify the update Legal name "<updateLegalPanelName>"
    Then User logout of the application

    @MLP3_Sprint2
    Examples: 
      | TriageOfficer | leagalName        | updateLegalPanelName | SelectClaimsType | Address1          | Address2          | suburb  | postcode | SolicitoName | SolicitoEmail | SolicitoPhone |
      | TriageOfficer | PavanAutomation12 | PavanAutomation1234  | Motor            | Street1AutoTester | Street2AutoTester | Tester1 |     4001 | Tester1      | a@b.com       |      12345678 |
#ScenariosMLP3SP#2_5
@MLP3_Sprint2
  Scenario Outline: Remove the added Legal panel and verify leagal panel removed or not the in claims portal
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then Click on Manage Components link after clicking on hamburger menu
    And Click on Leagal Payee from Manage Componet page
    And User enters the required fileds "<leagalName>" "<SelectClaimsType>" "<Address1>" "<Address2>" "<suburb>" "<postcode>" "<SolicitoName>" "<SolicitoEmail>" "<SolicitoPhone>"for AddLegalPanel
    And Click Submit from Add legal panel and search added leagal panel from the list "<leagalName>"
    And Verify the details of added leagal peanel "<SelectClaimsType>"
    And User selects the Remove option from Action list for added leagal panel
    And User the Removes the leagal panel details from displayed list
    Then User logout of the application

    @MLP3_Sprint2
    Examples: 
      | TriageOfficer | leagalName           | SelectClaimsType | Address1          | Address2          | suburb  | postcode | SolicitoName | SolicitoEmail | SolicitoPhone |
      | TriageOfficer | PavanAutomation12345 | Motor            | Street1AutoTester | Street2AutoTester | Tester1 |     4001 | Tester1      | a@b.com       |      12345678 |

  #Author: pavan.gangone@pega.com
  #USer Stroy Discription:users can view payees, edit and add new left side menu
  #Epicid :, User story:US-6097
  #Sprint 2
  #Test case coverage || Total TC's - 18 || Total automated TC's - 18.
  #ScenariosMLP3SP#2_6
@MLP3_Sprint2
  Scenario Outline: Initiate a Claim for GPA and Cancel the claims from TriageOfficer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "1" from claims pages
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and cancel the claim from Actions Drop down
    And verify the Claims Files Status "<TriageCancelStatus>" after Cancel from Actions drop down
    Then I logout of the application

    Examples: 
      | TriageOfficer | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | TriageCancelStatus   | finalstatustriage | finalstatus        |
      | TriageOfficer | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | PENDING-CANCELLATION | PENDING-TRIAGE    | RESOLVED-COMPLETED |

  #Author: pavan.gangone@pega.com
  #USer Stroy Discription:users can view payees, edit and add new left side menu
  #Epicid :, User story:US-6097
  #Sprint 2
  #Test case coverage || Total TC's - xx || Total automated TC's - xx.
  #ScenariosMLP3SP#2_7
@MLP3_Sprint2
  Scenario Outline: Initiate a Claim for GPA and Withdrawn the claims from TriageOfficer
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then I want to make a claim from claims officer
    Then I enter client details "<ZibCode>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I search the claim file and WithDrawn the claim from Actions Drop down
    And verify the Claims Files Status "<TriageWithdrawStatus>" after Select Withdraw from Actions drop down
    Then I logout of the application

    Examples: 
      | TriageOfficer | loss                    | ZibCode | NumTime1 | TriageWithdrawStatus |
      | TriageOfficer | Group Personal Accident |    5001 |        1 | RESOLVED-WITHDRAWN   |

  #Author: pavan.gangone@pega.com
  #USer Stroy Discription:users can view payees, edit and add new left side menu
  #Epicid :, User story:US-6097
  #Sprint 2
  #Test case coverage || Total TC's - xx || Total automated TC's - xx.
  #ScenariosMLP3SP#2_8
@MLP3_Sprint2
  Scenario Outline: Initiate a Claim for GPA and Select the Payment as Final from ClaimsOfficer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "1" from claims pages
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "<PortfolioManager>"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I prepare estimatee "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then User select final payment option
    Then I verify claimunit status "<approvestatus>"
    Then I resolve claim filede
    Then I verify claimunit status "<resolvestatus>"
    Then I logout of the application

    Examples: 
      | TriageOfficer | PortfolioManager | ClaimsOfficer | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | TriageCancelStatus   | finalstatustriage | finalstatus        | payableamount | outstandingamount | recoverableamount |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | PENDING-CANCELLATION | PENDING-TRIAGE    | RESOLVED-COMPLETED |           100 | File-Review       |                20 |
#ScenariosMLP3SP#2_9
@MLP3_Sprint2
  Scenario Outline: Initiate a Claim for GPA and verify solicitor displying to a claim
    Given User enters the Internal URL from Browser
    When User when logs in as "<ClaimsOfficer>"
    Then User open required claims unit "<claimsFile>" from Workbasket
    And Select Managepayments from Actions
    And Select adjust From Actions of Managepayments
    And Update the adjustment
    Then I logout of the application

    Examples: 
      | ClaimsOfficer | ClaimsFile |
      | ClaimsOfficer | CF-22552   |
#ScenariosMLP3SP#2_10
@MLP3_Sprint2
  Scenario Outline: login as claims officer and Adjust payment.
    Given User enters the Internal URL from Browser
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then User adding solicitor to a claim
    And verify system generating the next legal panel atuomatically
    And Verify the error message for after adding same solicitor
    Then I logout of the application

    Examples: 
      | ClaimsOfficer |
      | ClaimsOfficer |
#ScenariosMLP3SP#2_11
@MLP3_Sprint2
  Scenario Outline: Initiate a Claim for GPA and Select the Payment as Final from ClaimsOfficer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "1" from claims pages
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "<PortfolioManager>"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I prepare estimatee "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then User select final payment option
    Then I verify claimunit status "<approvestatus>"
    Then I resolve claim filede
    Then I verify claimunit status "<resolvestatus>"
    Then I logout of the application

    Examples: 
      | TriageOfficer | PortfolioManager | ClaimsOfficer | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | TriageCancelStatus   | finalstatustriage | finalstatus        | payableamount | outstandingamount | recoverableamount |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | PENDING-CANCELLATION | PENDING-TRIAGE    | RESOLVED-COMPLETED |           100 | File-Review       |                20 |
