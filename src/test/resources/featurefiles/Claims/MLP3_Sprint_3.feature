#Author: pavan.gangone@pega.com
#USer Stroy US-13002
#Epicid :, User story:US-6011
#Sprint 2
#Test case coverage || Total TC's - xx || Total automated TC's - xx.
Feature: MLP3 Sprint3 claims Scenarios
#ScenariosMLP3SP#3_1
@MLP3_Sprint3
  Scenario Outline: Intiate a claim on behalf of a subsidiary organisation
  Given I have the external URL of VMIA
  When User enter valid username  "<username>" and password "<password>" click on Login button
  And I Select the Organization1 "<OrgName1>"
  Then I am logged in to the insurance portal
  Then I want to make a claim
  And I enter the date and time of loss
  And I select the type of "<loss>"
  And I Select the Suborg check box and SubOrg type "<SubsidiaryOrg>"
  And Click Continue button
  When I select the business row and verify listed policies for "<loss>"
  And Click on Continue button for "1" from claims pages
  Then I am routed to Client Banking Account details page
  And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
  And Click Continue button
  Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
  And Click Continue button
  Then I provide accident details "<description>" "<reportedto>" "<injury>"
  And Click on Continue button for "<NumTime1>" from claims pages
  And I check the declaration for Claims
  Then Click Finish button
  Then I validate the "<finalstatus>" for claims
  And I extract the claim unit number
  Then I logout of the application
  Given I have the URL of VMIA
  When User enter valid username  "<TriageofficerUsername>" and password "<Triageofficerpswd>" click on Login button for internal user
  And I search the claim file and edit the claim "<triagecasestatus>"
  
  Then I logout of the application
  
  Examples:
  | triagecasestatus | TriageofficerUsername  | Triageofficerpswd | loss                    | SubsidiaryOrg         | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | NumTime1 | NumTime2 |
  | Pending-Triage   | TriageClaimsOfficer1R3 | Welcome@1234      | Group Personal Accident | Nelson School Council | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |        4 |        6 |
#ScenariosMLP3SP#3_2
@MLP3_Sprint3
  Scenario Outline: Verify the claiming Oraganization from My claims tab
  
  Given I have the external URL of VMIA
  When User enter valid username  "<username>" and password "<password>" click on Login button
  And I Select the Organization1 "<OrgName1>"
  Then I am logged in to the insurance portal
  And I select the Show subsidiary claims check box
  Then Verify the Claiming organisation should be subsidiary org " <SubsidiaryOrg> "
  Then I logout of the application
  
  Examples:
  | SubsidiaryOrg         |
  | Nelson School Council |
  #Author: pavan.gangone@pega.com
  #USer Stroy US-8008,US-13002,US-6112
  #Sprint 3
  #Test case coverage || Total TC's - xx || Total automated TC's - xx.
  #ScenariosMLP3SP#3_3
@MLP3_Sprint3
  Scenario Outline: Initiate a Claim for GPA and add recovery third party from claims officer and fince officer
  Given I have the external URL of VMIA
  When User enter valid username  "<username>" and password "<password>" click on Login button
  And I Select the Organization1 "<name>"
  Then I am logged in to the insurance portal
  Then I want to make a claim
  And I enter the date and time of loss
  And I select the type of "<loss>"
  And Click Continue button
  When I select the business row and verify listed policies for "<loss>"
  And Click Continue button
  And Click Continue button
  Then I am routed to Client Banking Account details page
  And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
  And Click Continue button
  Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
  And Click Continue button
  Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
  And Click on Continue button for "4" from claims pages
  And I check the declaration for Claims
  Then Click Finish button
  Then I validate the "<finalstatus>" for claims
  And I extract the claim unit number
  Then I logout of the application
  Given I have the URL of VMIA
  When User when logs in as "<TriageOfficer>"
  Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
  Then I click its not a duplicate
  Then verify "<finalstatustriage>"
  Then I logout of the application
  When User when logs in as "<PortfolioManager>"
  Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
  Then I logout of the application
 
  When User when logs in as "<ClaimsOfficer>"
   Then I open claim unit from worklist
  Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
  Then I prepare recoverable for new third party "<recoverableamount>" "<recoverabletype>"
   Then I attach an evidence file for recovery "<filepath>" "<category>"
  Then I verify claimunit status "<approvestatus>"
  Then I logout of the application
  Then I close the external link
  Given I have the URL of VMIA
  When User when logs in as "<Finance>"
  Then I manually process recovery from recovery portal
  Then I verify manual resolve "<resolvedstatus>"
  Then I logout of the application
  
  Examples:
  | TriageOfficer | PortfolioManager | ClaimsOfficer | Finance | recoverabletype      | resolvestatus     | approvestatus         | payableamount | outstandingamount | recoverableamount | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | finalstatustriage        | triagecasestatus | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | filepath |
  | TriageOfficer | PortfolioManager | ClaimsOfficer | Finance | Third party recovery | PAYMENT-RECOVERED | PENDING-INVESTIGATION |           100 | File review       |               200 | perator01   | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |  C:\\Users\\pega\\Documents\\TRIMUserSetup.txt        |
#ScenariosMLP3SP#3_4
@MLP3_Sprint3
 
  Scenario Outline: Initiate a Claim for Journy and edit the claim in triage claim officer
  
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "1" from claims pages
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details for journy "<fristName>" "<lastName>" "<Addressline1>""<Suburb>""<State>""<Postcode>""<Country>""<Emailaddress>""<contactnumber>""<Occupation>""<Dateofbirth>"
    And Click Continue button
    Then I Provide Claim for injury or death "<injury>" "<injuryoccur>" "<Dateinjuryoccured>" "<testWhenn>"
    And Click Continue button
    Then I provide Medical details "<MedicalPractitioner>" "<Periodfrom>" "<Periodto>" "<Doctorsname>" "<Address>" "<condition>" "<Doctorsname1>" "<Address>"
    And Click Continue button
    #Then I provide Other insurance or benefits
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    Examples: 
      | loss              | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | Addressline1 | Suburb | State | Postcode | Country | Emailaddress | contactnumber | Occupation | Dateofbirth | injury | injuryoccur | Dateinjuryoccured | testWhenn | MedicalPractitioner | Periodfrom | Periodto | Doctorsname | Address | condition | Doctorsname1 | Address | finalstatus        |
      | Journey insurance | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Add1         | hyd1   | New South Wales   |     4001 | AUSTRALIA     | q@s.com      |     123456789 | automation | 6/05/2008   |        |             |                   |      |                     |            |          |             |         |           |              |         | RESOLVED-COMPLETED |
