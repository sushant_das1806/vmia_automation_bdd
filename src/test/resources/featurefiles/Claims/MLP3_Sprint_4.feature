#Author: pavan.gangone@pega.com
#USer Stroy
#Epicid :, User story:
#Sprint 4
#Test case coverage || Total TC's - xx || Total automated TC's - xx.
Feature: MLP3 Sprint4 claims Scenarios
@MLP3_Sprint4
  Scenario Outline: Create new and update the  organisation from client adviser
  Given User enters the Internal URL from Browser
  When User when logs in as "<ClientAdviser>"
  Then I am logged in to the client adviser portal
  Then I click on create new organisation
  Then I provide organisation name "<Neworgname>"
  Then I want to create a new customer
  Then I fill the form
  Then I provide organisation name "<Neworgname>"
  And I update the organization name "<updateOrag>" "<changeReason>"
  And I verify the succesful msg for Organization
  Then I logoff from client adviser portal
  
  Examples:
  | ClientAdviser | Neworgname           | updateOrag           | changeReason       |
  | ClientAdviser | CemeteryQAAutolive70 | CemeteryQAAutolive71 | Only a change of name |
  | ClientAdviser | CemeteryQAAutolive68 | CemeteryQAAutolive69 |A new legal entity |
  
 @MLP3_Sprint4
  
  Scenario Outline:verify  update the  organisation from client adviser
  Given User enters the Internal URL from Browser
  When User when logs in as "<ClientAdviser>"
  Then I am logged in to the client adviser portal
  Then I click on create new organisation
  Then I provide organisation name "<Neworgname>"
  Then I want to create a new customer
  Then I fill the form
  Then I provide organisation name "<Neworgname>"
  And I update the organization name "<updateOrag>" "<changeReason>"
  And I verify the succesful msg for Organization
  And I search for updated organization and verify the updated org name "<updateOrag>"
  Then I logoff from client adviser portal
  
  Examples:
  | ClientAdviser | Neworgname           | updateOrag           | changeReason       |
  | ClientAdviser | CemeteryQAAutolive70 | CemeteryQAAutolive71 | Only a change of name |
 @MLP3_Sprint4
  
  Scenario Outline: verify  update the  organisation from client adviser
    Given User enters the Internal URL from Browser
    When User when logs in as "<ClientAdviser>"
    Then I am logged in to the client adviser portal
    Then I click on create new organisation
    Then I provide organisation name "<Neworgname>"
    Then I want to create a new customer
    Then I fill the form
    Then I provide organisation name "<Neworgname>"
    And I update the organization name and slect the primary contact "<updateOrag>" "<changeReason>" "<PrimaryVMIAContact>"
    And I verify the succesful msg for Organization
    And I search for updated organization and verify the updated org name "<updateOrag>"
    Then I logoff from client adviser portal
    
    When User when logs in as "<ClaimsOfficer>"
    Then I am logged in to the client adviser portal
    And I click on Bell nofication and select update Oraganization notification
    And I provide required information before approval from client officer "<AprrovalNote>" "<updateOrag>"
    Then I logout of the application

    Examples: Mark organisation as inactive
      | ClientAdviser | ClaimsOfficer | Neworgname           | updateOrag           | changeReason       | PrimaryVMIAContact | AprrovalNote |
      | ClientAdviser | ClaimsOfficer | CemeteryQAAutolive91 | CemeteryQAAutolive92 | A new legal entity | ClaimsOfficer1R3   | Test         |
      | ClientAdviser | ClaimsOfficer | CemeteryQAAutolive91 | CemeteryQAAutolive92 | Related to a merger or acquisition | ClaimsOfficer1R3   | Test         |
      
      
      @MLP3_Sprint4
    Scenario Outline: Mark organisation as inactive from client adviser
    Given User enters the Internal URL from Browser
    When User when logs in as "<ClientAdviser>"
    Then I am logged in to the client adviser portal
    Then I click on create new organisation
    Then I provide organisation name "<Neworgname>"
    Then I want to create a new customer
    Then I fill the form
    Then I provide organisation name "<Neworgname>"
    And I update the organization name and slect the primary contact "<updateOrag>" "<changeReason>" "<PrimaryVMIAContact>"
    And I verify the succesful msg for Organization
    And I search for updated organization and verify the updated org name "<updateOrag>"
    Then I logoff from client adviser portal
    
    Examples: 
      | ClientAdviser | ClaimsOfficer | Neworgname           | updateOrag           | changeReason       | PrimaryVMIAContact | AprrovalNote |
      | ClientAdviser | ClaimsOfficer | CemeteryQAAutolive91 | CemeteryQAAutolive92 | A new legal entity | ClaimsOfficer1R3   | Test         |