Feature: Claims


  Scenario Outline: Start a new policy application for Motor Vehicle without deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the risk page with machinery "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<machineryflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And Click UW Continue button
    #And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for MV with machinery "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<machineryflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<pricemachinery>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @spr3MV
    Examples: 
      | username   | password    | product | parkingslots | multiplier | gst | stampduty | startdate  | status                   | days | paymentstatus   | lightvehicleflag | busflag | trailerflag | machineryflag | otherflag | typeofvehicle | manufactureyear | make | model | regno | currentmarketvalue | numberofvehicles | pricelightvehicle | pricebusunder50 | pricebusover50 | pricetrailer | pricemachinery |
      | Operator01 | abhinav@123 | MVF     |          400 |      11.98 | 0.1 |       0.1 | 21/09/2020 | PENDING-CLIENTACCEPTANCE |  283 | PENDING-PAYMENT | false            | false   | false       | true          | false     | SUV           |            2011 | ABC  | n97   |  8798 |              40000 |                1 |               450 |             620 |         891.78 |           50 |            620 |

  Scenario Outline: Validate client search functionality in claims officer portal for organisation search
    Given I have the URL of VMIA
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I search for the "<orgname>" and validate with org id as "<exporgid>"
    Then I logout of the application

    @spr3org
    Examples: 
      | claimsofficeruser | claimsofficerpass | orgname           | exporgid   |
      | claimsofficer1    | Welcome@1234      | Automation School | BUNIT-9514 |

  Scenario Outline: Validate client search functionality in claims officer portal for contact search
    Given I have the URL of VMIA
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I search for the "<firstname>" and validate with contact id as "<contactid>"
    Then I logout of the application

    @spr3contact
    Examples: 
      | claimsofficeruser | claimsofficerpass | firstname | contactid  |
      | claimsofficer1    | Welcome@1234      | Abhinav   | CONT-10224 |

  Scenario Outline: Initiate a Claim for Monument damage
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I provide interment holder details "<holdername>" "<holderlastname>" "<contactfirstname>" "<contactlastname>" "<addressloopupinput>" "<address>" "<contactphonenumber>"
    And Click Continue button
    Then I provide incident details "<firstname>" "<lastname>" "<incidentlocation>" "<plotnumber>" "<damagereason>" "<amountclaimed>"
    And Click Continue button
    Then I attach an evidence file without dropdown "<filepath>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @MonumentClaim
    Examples: 
      | firstname | lastname | incidentlocation | plotnumber | damagereason | amountclaimed | holdername | holderlastname | contactfirstname | contactlastname | addressloopupinput | address                                            | contactphonenumber | username   | password    | loss            | Where   | What       | How          | purchasedescription | cost | salvagecost | category                       | filepath                              | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |
      | Shane     | Watson   | Victoria         |         36 | earthquake   |          3000 | harry      | gurney         | Steve            | Smith           | Victoria           | Victoria Hotel, 82 Queen Street, BARRABA  NSW 2347 |         8978986787 | Operator02 | abhinav@123 | Monument damage | Highway | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | Repair or replacement invoices | C:\\Attachments\\Assertions_Added.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |

  Scenario Outline: Initiate a Claim for Property
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for property cyber "<damagereason>"
    And Click Continue button
    Then I attach an evidence file without dropdown "<filepath>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @propertyclaim
    Examples: 
      | damagereason | username   | password    | loss            | Where   | What       | How          | purchasedescription | cost | salvagecost | category                       | filepath                              | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |
      | earthquake   | Operator02 | abhinav@123 | Property damage | Highway | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | Repair or replacement invoices | C:\\Attachments\\Assertions_Added.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |

  Scenario Outline: Initiate a Claim for Cyber
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    Then I answer DPC question
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    Then I attach an evidence file "<filepath>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @cyberclaim
    Examples: 
      | incident | action | damagereason | username   | password    | loss  | Where   | What       | How          | purchasedescription | cost | salvagecost | category | filepath                              | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |
      | inc      | act    | earthquake   | Operator02 | abhinav@123 | Cyber | Highway | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | Invoice  | C:\\Attachments\\Assertions_Added.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |

  Scenario Outline: Initiate a Claim for Liability damage
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    Then I Click on claim log
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I add incident details for liability claims "<incident>" "<nameofclaimant>" "<incidentlocation>" "<address>"
    And Click Continue button
    Then I attach an evidence file "<filepath>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @LiabilityClaim
    Examples: 
      | incident | nameofclaimant | incidentlocation | plotnumber | damagereason | amountclaimed | holdername | holderlastname | contactfirstname | contactlastname | address                                            | contactphonenumber | username   | password    | loss             | Where   | What       | How          | purchasedescription | cost | salvagecost | category                       | filepath                              | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |
      | Shane    | Watson         | Victoria         |         36 | earthquake   |          3000 | harry      | gurney         | Steve            | Smith           | Victoria Hotel, 82 Queen Street, BARRABA  NSW 2347 |         8978986787 | Operator02 | abhinav@123 | Public liability | Highway | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | Repair or replacement invoices | C:\\Attachments\\Assertions_Added.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |

  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    #Then I am logged in to the insurance portal
    #Then I want to make a claim
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>" more than allowed
    Then I logout of the application
    When I enter the "<ceouser>"
    And I enter "<ceopass>"
    And I click login button
    Then I search the estimate approval file in workbasket and approve the estimate "<pendingapprstatus>" "<approvalstatus>"
    Then I logout of the application
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I open claim unit from worklist
    Then I process claim unit case "<clientdecision>" "<awaitreason>"
    Then I verify claimunit status "<approvestatus>"
    Then I resolve claim file
    Then I verify claimunit status "<resolvestatus>"
    Then I logout of the application

    @estimateapproval
    Examples: 
      | awaitreason | ceouser | ceopass      | pendingapprstatus | approvalstatus    | resolvestatus      | awaitreason | approvestatus      | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername       | portfoliopassword | finalstatustriage            | triagecasestatus | username   | password | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | NA          | ceo     | Welcome@1234 | PENDING-APPROVAL  | RESOLVED-APPROVED | RESOLVED-COMPLETED | NA          | PENDING-COMPLETION |        300000 |                20 |                20 | approve        | claimsofficer1    | Welcome@1234      | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | claimsportfoliomanager1 | Welcome@1234      | PENDING-CLAIMUNIT COMPLETION | PENDING-TRIAGE   | rps@school | rules    | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager make payment
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    #Then I am logged in to the insurance portal
    #Then I want to make a claim
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then I add single payment option
    Then I verify claimunit status "<approvestatus>"
    Then I resolve claim file
    Then I verify claimunit status "<resolvestatus>"
    Then I logout of the application

    @MakePayment
    Examples: 
      | resolvestatus      | awaitreason | approvestatus      | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername       | portfoliopassword | finalstatustriage            | triagecasestatus | username   | password | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | RESOLVED-COMPLETED | NA          | PENDING-COMPLETION |           100 |                20 |                20 | approve        | claimsofficer1    | Welcome@1234      | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | claimsportfoliomanager1 | Welcome@1234      | PENDING-CLAIMUNIT COMPLETION | PENDING-TRIAGE   | rps@school | rules    | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager make payment
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    #Then I am logged in to the insurance portal
    #Then I want to make a claim
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then I make payment and add new payee "<payeetypedropdown>" "<payeename>" "<BSB>" "<AccountNumber>" "<EmailID>"
    Then I logout of the application

    @PayeeCreation
    Examples: 
      | payeetypedropdown | payeename             | resolvestatus      | awaitreason | approvestatus      | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername       | portfoliopassword | finalstatustriage            | triagecasestatus | username   | password | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | Contractor        | Automation_Contractor | RESOLVED-COMPLETED | NA          | PENDING-COMPLETION |           100 |                20 |                20 | approve        | claimsofficer1    | Welcome@1234      | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | claimsportfoliomanager1 | Welcome@1234      | PENDING-CLAIMUNIT COMPLETION | PENDING-TRIAGE   | rps@school | rules    | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |
