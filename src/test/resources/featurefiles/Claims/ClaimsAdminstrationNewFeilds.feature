Feature: Claims management - New fields for litigated claims
 
    @MPL4 @Indeminity @ClaimsAdminstration
		Scenario Outline: Initaiting a liability claim and verify claim adminstration
		
    Given I have the URL of VMIA
    When User when logs in as "TriageOfficer"
    Then I am logged in to the insurance portal
    Then I want to make a claim from claims officer
    Then I enter client details "<ZibCode>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "<NumTime1>" from claims pages
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" "<addline1>" "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    And Open the claim from "Triage Claims" queue
   	And review claim and check dupilicate
   	And Validated the medical claim indemnity case status "PENDING-CLAIM COMPLETION"
   	Then I logout of the application
   	Given I have the URL of VMIA
   	When User when logs in as "PortfolioManager"
   	Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
   	And Select the "<Actions>"
   	And Verify the Claim Adminstration fields
   	Then I logout of the application
   	
   	    Examples: 
      | TriageofficerUsername  | Triageofficerpswd | ZibCode | loss                         | what    | claimantname | addline1 | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | NumTime1 | finalstatus        | Actions              |
      | TriageClaimsOfficer1R3 | Welcome@1234      |    1002 | Public and product liability | Highway | liabClaimant | Sydney   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |        1 | RESOLVED-COMPLETED | Claim administration |
  #