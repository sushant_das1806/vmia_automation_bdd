Feature: Claims


  @TriageDupCheckNotADuplicatePort
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    #Then I am logged in to the insurance portal
    #Then I want to make a claim
    Then I transfer the claim from portfolio manager to user "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application

    Examples: 
      | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername       | portfoliopassword | finalstatustriage            | triagecasestatus | username   | password | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | claimsportfoliomanager1 | Welcome@1234      | PENDING-CLAIMUNIT COMPLETION | PENDING-TRIAGE   | rps@school | rules    | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  Scenario Outline: Create new policy bundle
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on create new policy bundle "<clientname>" "<revenue>"
    Then I logoff from client adviser portal

    @bundle
    Examples: 
      | clientname | revenue | triagecasestatus | username           | password | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | Sudheer    |     200 | PENDING-TRIAGE   | Autoclientadviser1 | rules    | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  Scenario Outline: Create new policy bundle for single client
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on create new policy bundle "<clientname>" "<revenue>"
    Then I verify the bundle status as "<bundlestatus>"
    Then I verify package case
    Then I logoff from client adviser portal

    @bundle @Spr2
    Examples: 
      | clientname | revenue | bundlestatus       | username           | password |
      | Sudheer    |     200 | RESOLVED-COMPLETED | Autoclientadviser1 | rules    |

  Scenario Outline: Create new policy bundle for multiple client and verify all policies created
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on create new policy bundle for multiple clients "<revenue>"
    Then I verify the bundle status as "<bundlestatus>"
    Then I verify package case
    Then I verify the bundle policies created
    Then I logoff from client adviser portal

    @multbundle @Spr2
    Examples: 
      | clientname | revenue | bundlestatus       | username           | password |
      | Sudheer    |     200 | RESOLVED-COMPLETED | Autoclientadviser1 | rules    |

  @Spr3Full
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    #Then I am logged in to the insurance portal
    #Then I want to make a claim
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>" and review the claim unit as "<clientdecision>" from pending investigation screen "<awaitreason>"
    Then I verify claimunit status "<approvestatus>"
    Then I resolve claim file
    Then I verify claimunit status "<resolvestatus>"
    Then I logout of the application

    Examples: 
      | resolvestatus      | awaitreason | approvestatus      | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername       | portfoliopassword | finalstatustriage            | triagecasestatus | username   | password | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | RESOLVED-COMPLETED | NA          | PENDING-COMPLETION |           100 |                20 |                20 | approve        | claimsofficer1    | Welcome@1234      | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | claimsportfoliomanager1 | Welcome@1234      | PENDING-CLAIMUNIT COMPLETION | PENDING-TRIAGE   | rps@school | rules    | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  @Spr3FullDEN
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    #Then I am logged in to the insurance portal
    #Then I want to make a claim
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>" and review the claim unit as "<clientdecision>" from pending investigation screen "<awaitreason>"
    Then I verify claimunit status "<denialstatus>"
    Then I logout of the application

    Examples: 
      | awaitreason | denialstatus | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername       | portfoliopassword | finalstatustriage            | triagecasestatus | username   | password | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | NA          | PENDING-DENY |           100 |                20 |                20 | deny           | claimsofficer1    | Welcome@1234      | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | claimsportfoliomanager1 | Welcome@1234      | PENDING-CLAIMUNIT COMPLETION | PENDING-TRIAGE   | rps@school | rules    | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  @Spr3FullDENApprPort
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager then claims officer denies it portfolio mnager approves denial
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    #Then I am logged in to the insurance portal
    #Then I want to make a claim
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>" and review the claim unit as "<clientdecision>" from pending investigation screen "<awaitreason>"
    Then I verify claimunit status "<denialstatus>"
    Then I logout of the application
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    Then I open claim unit from worklist
    Then I approve the claim unit in portmanager
    Then I verify claimunit status "<denialstatusport>"
    Then I logout of the application
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I open claim unit from worklist
    Then I submit denial of claim after port manager approval
    Then I verify claimunit status "<finalclaimunitstatus>"
    Then I logout of the application

    Examples: 
      | finalclaimunitstatus | denialstatusport | awaitreason | denialstatus | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername       | portfoliopassword | finalstatustriage            | triagecasestatus | username   | password | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | RESOLVED-DENIED      | PENDING-DENY     | NA          | PENDING-DENY |           100 |                20 |                20 | deny           | claimsofficer1    | Welcome@1234      | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | claimsportfoliomanager1 | Welcome@1234      | PENDING-CLAIMUNIT COMPLETION | PENDING-TRIAGE   | rps@school | rules    | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  @Spr3FullDENRejectPort
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager then claims officer denies it portfolio mnager approves denial
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    #Then I am logged in to the insurance portal
    #Then I want to make a claim
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>" and review the claim unit as "<clientdecision>" from pending investigation screen "<awaitreason>"
    Then I verify claimunit status "<denialstatus>"
    Then I logout of the application
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    Then I open claim unit from worklist
    Then I reject the claim unit in portmanager
    Then I verify claimunit status "<approvalstatusport>"
    Then I logout of the application
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I open claim unit from worklist
    Then I verify claimunit status "<finalCUstatus>"
    Then I logout of the application

    Examples: 
      | finalCUstatus         | approvalstatusport    | awaitreason | denialstatus | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername       | portfoliopassword | finalstatustriage            | triagecasestatus | username   | password | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | PENDING-INVESTIGATION | PENDING-INVESTIGATION | NA          | PENDING-DENY |           100 |                20 |                20 | deny           | claimsofficer1    | Welcome@1234      | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | claimsportfoliomanager1 | Welcome@1234      | PENDING-CLAIMUNIT COMPLETION | PENDING-TRIAGE   | rps@school | rules    | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  @Spr3FullAwait
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager and awaiting decision
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    #Then I am logged in to the insurance portal
    #Then I want to make a claim
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>" and review the claim unit as "<clientdecision>" from pending investigation screen "<awaitreason>"
    Then I verify claimunit status "<awaitstatus>"
    Then I logout of the application

    Examples: 
      | awaitreason | awaitstatus         | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername       | portfoliopassword | finalstatustriage            | triagecasestatus | username   | password | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      #| Request for more info | PENDING- CLIENT ACTION |           100 |                20 |                20 | awaiting       | claimsofficer1    | Welcome@1234      | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | claimsportfoliomanager1 | Welcome@1234      | PENDING-CLAIMUNIT COMPLETION | PENDING-TRIAGE   | rps@school | rules    | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |
      | Litigation  | PENDING- LITIGATION |           100 |                20 |                20 | awaiting       | claimsofficer1    | Welcome@1234      | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | claimsportfoliomanager1 | Welcome@1234      | PENDING-CLAIMUNIT COMPLETION | PENDING-TRIAGE   | rps@school | rules    | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  @Spr3FullAddLegalPanel
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager then claims officer denies it portfolio mnager approves denial
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    #Then I am logged in to the insurance portal
    #Then I want to make a claim
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When I enter the "<claimsofficeruser>"
    And I enter "<claimsofficerpass>"
    And I click login button
    Then I open claim unit from worklist
    Then I add a legal panel
    Then I verify nmber of litigations
    Then I logout of the application

    Examples: 
      | finalclaimunitstatus | denialstatusport | awaitreason | denialstatus | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername       | portfoliopassword | finalstatustriage            | triagecasestatus | username   | password | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | RESOLVED-DENIED      | PENDING-DENY     | NA          | PENDING-DENY |           100 |                20 |                20 | deny           | claimsofficer1    | Welcome@1234      | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | claimsportfoliomanager1 | Welcome@1234      | PENDING-CLAIMUNIT COMPLETION | PENDING-TRIAGE   | rps@school | rules    | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  @SCIP
  Scenario Outline: Initiate a Claim for Property MLP2
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details "<Where>" "<What>" "<How>"
    Then I select the property damages
    And Click Continue button
    Then I add item description "<itemdesc>" "<purprice>" "<coverage>"
    And Click Continue button
    Then I attach an evidence file without dropdown "<filepath>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    Examples: 
      | filepath                              | username   | password | loss     | Where   | What       | How          | purchasedescription | cost | salvagecost | category                       | filepath                              | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |
      | C:\\Attachments\\Assertions_Added.txt | rps@school | rules    | Property | Highway | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | Repair or replacement invoices | C:\\Attachments\\Assertions_Added.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |
