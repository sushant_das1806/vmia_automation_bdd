Feature: VGRMF Flag at Organisation level

  Scenario Outline: Add VGRMF flag
  Given I have the URL of VMIA
  When I enter the "<username>"
  And I enter "<password>"
  And I click login button
  Then I am logged in to the client adviser portal
  And Click New and Select "Manage organisation"
  And Search the new Orgname and click CreateNew button
  And I add a VGRMF "<flag>"
  And Enter the Orgdetails "<clientSegment>" "<GovernmentDepartment>" "<Profession>" "<PhoneNumber>" <SearchAddress>
  And Click New and Select "Manage organisation"
  And Search the new Orgname and click UpdateButton
  And Update the following orgnization details "<isSubjectToGovernChange>" "<IsOrgMerged>" "<EffecitveDate>" "<comments>" "<MergeOrgDetail>"
  And click submit button in UpdateOrganization Screen
  And verify the VGRMF "<flag>"

Examples:
| username          | password     | isSubjectToGovernChange | IsOrgMerged    | EffecitveDate |MergeOrgDetail|comments|clientSegment|GovernmentDepartment|Profession|PhoneNumber|SearchAddress|flag|
#    | AutoClientAdviser | Welcome@1234 | Yes                     | Merged         | 14/06/2021    |Scotland PNP  |Updateorg|
| AutoClientAdviser | Welcome@1234 | Yes                     | Splited        | 14/06/2021    |Scotland PNP;CSEO 5|Updateorg|Community Service Organisation|Department of Education and Training|tester|8508356502|"Sydney " |Yes|

##
#  Scenario Outline: Verify the Split and merge organization details can be updated in manage Organization
#    Given I have the URL of VMIA
#    When I enter the "<username>"
#    And I enter "<password>"
#    And I click login button
#    Then I am logged in to the client adviser portal
#    And Click New and Select "Manage organisation"
#    And Search the new Orgname and click CreateNew button
#    And Enter the Orgdetails "<clientSegment>" "<GovernmentDepartment>" "<Profession>" "<PhoneNumber>" "<SearchAddress>" "<VGRMFflag>"
#    And Click New and Select "Manage organisation"
#    And Search the new Orgname and click UpdateButton
#    And Update the following orgnization details "<isSubjectToGovernChange>" "<IsOrgMerged>" "<EffecitveDate>" "<comments>" "<MergeOrgDetail>"
#    And click submit button in UpdateOrganization Screen
##    And Click New and Select "Manage contact"
##    Then Search the contact using contactid "CONT-27587"
##    And Add the contact to Organzation to the contact "" "Insurance;Finanace"
##    Then Verify case status "RESOLVED-COMPLETED" and capture the case id
#
#    Examples:
#      | username          | password     | isSubjectToGovernChange | IsOrgMerged    | EffecitveDate |MergeOrgDetail|comments|clientSegment|GovernmentDepartment|Profession|PhoneNumber|SearchAddress|VGRMFflag|
##      | AutoClientAdviser | Welcome@1234 | Yes                     | Merged         | 14/06/2021    |Scotland PNP  |Updateorg|
#      | AutoClientAdviser | Welcome@1234 | Yes                     | Splited          | 14/06/2021    |Scotland PNP;CSEO 5|Updateorg|Community Service Organisation|Department of Education and Training|tester|8508356502|Sydney |No|
##