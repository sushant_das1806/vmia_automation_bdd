
Feature: Sprint 6 claims scenario 
  
    @Us-154
    Scenario Outline: Verify portfolio manager is able to add reportees and added reportee is removed from the list
    Given I have the URL of VMIA
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    And I select Profile for portfoliomanager 
    And I select reportee"<reportee>"
    And Now I deselect the same reportee
    Then Now I logout of the application
    
    
      Examples: 
               | portfoliousername     |  portfoliopassword |reportee            |
               |claimsportfoliomanager1|Welcome@1234        |Balwyn High School  |
               
               
   @Us-179
    
    Scenario Outline: Initiate a FNOL case from Triage claim officer portal for GPA and make it resolved completed
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to Traige officer portal
    Then I click on make a claim From Traige officer
    Then I want to make a claim for client adviser
    Then I enter business details for claim For Triage 
    Then I want to make a claim for client adviser
    And I enter the date and time of loss
    #And I select the loss type of "<loss>"
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
   # Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    Then I attach an evidence file "<filepath>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
     And I extract the claim unit number
    Then Now I logout of the application
		
    Examples: 
      | username             | password        | claimsofficerusername | claimsofficerpassword | loss                    | AccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |filepath                                      | 
      | TriageClaimsOfficer1 | Welcome@1234    | ClaimsOfficer         | Welcome@321           | Group Personal Accident | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |C:\\Users\\pega\\Documents\\TRIMUserSetup.txt |
      
      
   @Us-179_1
   Scenario Outline: Make a FNOL case for Property from Triage claim officer portal and make the case resolve completed
   Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to Traige officer portal
    Then I click on make a claim From Traige officer
    Then I want to make a claim for client adviser
    Then I enter business details for claim For Triage 
    Then I want to make a claim for client adviser
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    ## Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details in Triage  "<Where>" "<What>" "<How>"
    And Click Continue button
    Then I provide SCIP details for traige"<purchasedescription>""<cost>""<salvagecost>"
    And Click Continue button
    Then I attach an evidence file "<filepath>" "<category>"
    Then I attach proof evidence file "<filepath_proof>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
     Then Now I logout of the application
		@Regression_MLP1
    @MLP1Regression
    Examples: 
      | username                             | password          | loss     | Where   | What       | How          | purchasedescription | cost | salvagecost | category                          | filepath                                     | finalstatus        | AccountName | BSB           | AccountNumber | BankName          | EmailID     | name     |filepath_proof                                |
      | TriageClaimsOfficer1                 |  Welcome@1234     | Property | Highway | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | Repair or replacement invoices    | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt| RESOLVED-COMPLETED | Flinders    | 123236        |       3475648 | Commonwealth Bank | comm@gm.com | Lowanna1 |C:\\Users\\pega\\Documents\\TRIMUserSet.txt   |
      
      
      

 
