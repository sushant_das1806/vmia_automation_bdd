Feature: Regression suite for VMIA MLP2

  #Completed1
  @RegressionMLP2_today_New
  Scenario Outline: Initiate a Claim for GPA as a triage claims Officer
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I want to make a claim from claims officer
    Then I enter client details "<ZibCode>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "<NumTime1>" from claims pages
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<description>" "<reportedto>" "<injury>"
    And Click on Continue button for "3" from claims pages
    #Then I attach an evidence file "<filepath>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    And Extarct the claim number
    Then I logout of the application

    @MLP2Regression1_Set1 @Golive_26_Today_mlp2
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | ZibCode | loss                    | AccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | description       | reportedto | injury     | filepath                                      | category                       | finalstatus        | NumTime1 | NumTime2 |
      | TriageOfficer | PortfolioManager | ClaimsOfficer |    1002 | Group Personal Accident | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road | Headmaster | Broken Leg | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | Repair or replacement invoices | RESOLVED-COMPLETED |        1 |        3 |

  #Completed2
  @RegressionMLP21
  Scenario Outline: Initiate a Claim for Liabliity in triage claimsOfficer
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I want to make a claim from claims officer
    Then I enter client details "<ZibCode>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "<NumTime1>" from claims pages
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" "<addline1>" "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    #Then I attach an evidence file "<filepath>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @MLP2Regression1_Set1 @Regression_MLP2_Set2_Rerun_12 @Golive_26_Today_mlp2
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | ZibCode | loss                         | what    | claimantname | addline1 | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | NumTime1 |
      | TriageOfficer | PortfolioManager | ClaimsOfficer |    1002 | Public and product liability | Highway | liabClaimant | Sydney   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |        1 |

  #Completed3
  @RegressionMLP21 @RegressionMLP20417  @RegressionMLP2_Fixed_1234  @Debug1231
  Scenario Outline: Initiate a Claim for GPA and edit the claim in triage claim officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    Then I am logged in to the insurance portal
    And I Select the Organization "<OrgName>"
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "1" from claims pages
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<description>" "<reportedto>" "<injury>"
    And Click on Continue button for "<NumTime1>" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and edit the claim "<triagecasestatus>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "2" from claims pages
    Then I am routed to Client Banking Account details page
    And Click on Continue button for "<NumTime2>" from claims pages
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @MLP2Regression1_Set1 @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | triagecasestatus | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | NumTime1 | NumTime2 |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | Pending-Triage   | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |        4 |        6 |

  #Completed4
  @RegressionMLP21 @RegressionMLP2_Rerun1234567 @Debug123345
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then Now I click its not a duplicate
    Then Now verify "<finalstatustriage>"
    Then Now I logout of the application

    @MLP2Regression1_Set1 @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | TriageOfficer| PortfolioManager | ClaimsOfficer | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | finalstatustriage        | triagecasestatus |
      | TriageOfficer| PortfolioManager | ClaimsOfficer | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |

  #Completed5
  @RegressionMLP21 @RegressionMLP2_Rerun123456 @Debug12341212
  Scenario Outline: Initiate a Claim for GPA and resolve claim as duplicate in triage claim officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then Now I resolve claim as duplicate
    Then Now verify duplicate status "<finalstatustriage>"
    Then Now I logout of the application

    @MLP2Regression1_Set1 @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | PortfolioManager | ClaimsOfficer | finalstatustriage  | triagecasestatus | username                       | password      | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | name     |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | PortfolioManager | ClaimsOfficer | RESOLVED-DUPLICATE | PENDING-TRIAGE   | pavan.gangone@areteanstech.com | Areteans@1981 | TriageClaimsOfficer1R3      | rules                       | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | Lowanna4 |

  #Completed6
  @RegressionMLP21 @RegressionMLP20405_Pass-123456
  Scenario Outline: Create new organisation client adviser
    Given I have the URL of VMIA
    When I enter the "<ClientAdvserUname>"
    And I enter "<ClientAdvserpswd>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on create new organisation
    Then I provide organisation name "<Neworgname>"
    Then I want to create a new customer
    Then I fill the form
    Then I logoff from client adviser portal

    @MLP2Regression1_Set1 @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | ClientAdvserUname | ClientAdvserpswd | Neworgname      |
      | AutoClientAdviser | Welcome@1234     | CemeteryQANew19 |

  #@pavan123
  #Scenario Outline: Add Organization under Manage contact
  #Given I have the URL of VMIA
  #When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
  #Then I am logged in to the client adviser portal
  #Then I click on Manage contacts
  #And I click on Add oraganization
  #And I enter "<OrgName>" "<commnicationref>" "<accountType>" and click submit
  #Then I logoff from client adviser portal
  #
  #@MLP2Regression1_Set1 @Golive_26_Today @Golive_26_Today_mlp2
  #Examples:
  #| ClientAdvserUname | ClientAdvserpswd | Neworgname     |
  #| AutoClientAdviser | rules     | CemeteryQA1234 |
  #Completed7
  @RegressionMLP22304
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then Now I click its not a duplicate
    Then Now verify "<finalstatustriage>"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<PortfolioManager>"
    Then Now I transfer the claim from portfolio manager to user "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then Now I logout of the application

    @MLP2Regression1_Set1_Remove @Golive_26
    Examples:
      | TriageOfficer | statusaftertransfer   | beforetransferclaimstatus | usertotransfer      | portfoliousername         | portfoliopassword | finalstatustriage        | triagecasestatus | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | TriageOfficer | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 R3 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | TriageClaimsOfficer1R3      | rules                       | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  #Completed8
  #Need to add contact everytime before we run the regressions
  @RegressionMLP21 @RegressionMLP20405_Pass
  Scenario Outline: Create new policy bundle
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on create new policy bundle "<clientname>" "<revenue>"
    Then I logoff from client adviser portal

    Examples:
      | ClientAdvserUname | ClientAdvserpswd | clientname         | revenue |
      | AutoClientAdviser | Welcome@1234     | CemeteryQANew16230 |     200 |

  #Completed9
  #Need to add contact everytime before we run the regressions
  @NotRequiredRegressionMLP2
  Scenario Outline: Create new policy bundle for single client
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on create new policy bundle "<clientname>" "<revenue>"
    Then I verify the bundle status as "<bundlestatus>"
    # Then I verify package case
    Then I logoff from client adviser portal

    Examples:
      | ClientAdvserUname | ClientAdvserpswd | clientname        | revenue | bundlestatus       |
      | AutoClientAdviser | Welcome@1234     | CemeteryQATest261 |     200 | RESOLVED-COMPLETED |

  #Comepleted10
  #Need to map contact to organization everytime before running regression
  Scenario Outline: Create new policy bundle for multiple client and verify all policies created
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on create new policy bundle for multiple clients "<revenue>"
    # Then I verify the bundle status as "<bundlestatus>"
    Then I verify package case
    Then I verify the bundle policies created
    Then I logoff from client adviser portal

    @MLP2Regression1_Set1_not_required
    Examples:
      | ClientAdvserUname | ClientAdvserpswd | clientname | revenue | bundlestatus       |
      | AutoClientAdviser | Welcome@1234     | qet        |     200 | RESOLVED-COMPLETED |

  #Completed11
  @RegressionMLP2123786_today_New1
  Scenario Outline: Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<PortfolioManager>"
    Then Now I transfer the claim from portfolio manager to user "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>" and review the claim unit as "<clientdecision>" from pending investigation screen "<awaitreason>"
    Then I verify claimunit status "<approvestatus>"
    Then I resolve claim filede
    Then I verify claimunit status "<resolvestatus>"
    Then I logout of the application

    @MLP2Regression1_Set1_123_Not
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | resolvestatus      | awaitreason | approvestatus      | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer      | portfoliousername         | portfoliopassword | finalstatustriage        | triagecasestatus | username                       | password      | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | RESOLVED-COMPLETED | NA          | PENDING-COMPLETION |           100 | File review       |                20 | approve        | ClaimsOfficer_AT  | rules             | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 R3 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | pavan.gangone@areteanstech.com | Areteans@1981 | TriageClaimsOfficer1R3      | rules                       | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  #############################################Set2#########################################################333
  #Completed12
  @RegressionMLP228032021
  Scenario Outline: Initiate a Claim for GPA not a duplicate at TriageOff then deny at portfolio manager
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "1" from claims pages
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then Now I click its not a duplicate
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<PortfolioManager>"
    #Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then Now I transfer the claim from portfolio manager to user "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<ClaimsOfficer>"
    And I click login button
    Then I open claim unit from worklist
    #Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>" and review the claim unit as "<clientdecision>" from pending investigation screen "<awaitreason>"
    Then as claims officer select deny option "<clientdecision>"
    Then I process claim unit case "<clientdecision>" "<awaitreason>"
    Then I verify claimunit status "<denialstatus>"
    Then I logout of the application

    @Regression_MLP2_Set_2_123456 @Golive_26 @Golive_26_Today @Golive_26_Today_mlp2_0323
    Examples:
      | TriageOfficer | ClaimsOfficer | loss                    | awaitreason | denialstatus | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | usertotransfer      | portfoliousername         | portfoliopassword | finalstatustriage        | triageclaimsofficerusername | triageclaimsofficerpassword | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | statusaftertransfer   | beforetransferclaimstatus | triagecasestatus |
      | TriageOfficer | ClaimsOfficer | Group Personal Accident | NA          | PENDING-DENY |           100 | File-Review       |                20 | deny           | ClaimsOfficer_AT  | rules             | operator01  | Claims Officer 1 R3 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | TriageClaimsOfficer1R3      | rules                       | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | PENDING-INVESTIGATION | PENDING-TRIAGE            | PENDING-TRIAGE   |

  #Completed13
  @Regression1MLP2not
  Scenario Outline: Initiate a Claim for GPA with not a duplicate approve the denied claims at claims officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "1" from claims pages
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When User when logs in as "<PortfolioManager>"
    And I click login button
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I process claim unit case "<clientdecision>" "<awaitreason>"
    Then I verify claimunit status "<denialstatus>"
    Then I logout of the application
    When User when logs in as "<PortfolioManager>"
    Then I open claim unit from worklist
    Then I approve the claim unit in portmanager
    Then I verify claimunit status "<denialstatusport>"
    Then I logout of the application
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I submit denial of claim after port manager approval
    Then I verify claimunit status "<finalclaimunitstatus>"
    Then I logout of the application

    @Golive_26_Today_mlp2 @Golive_26 @Golive_26_Today1 @Golive_26_Today_mlp2_0323
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | PortfolioManager | ClaimsOfficer | finalclaimunitstatus | denialstatusport | awaitreason | denialstatus | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername         | portfoliopassword | finalstatustriage        | triagecasestatus | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | PortfolioManager | ClaimsOfficer | RESOLVED-DENIED      | PENDING-DENY     | NA          | PENDING-DENY |           100 | File review       |                20 | deny           | claimsofficer1    | rules             | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  #Completed14
  @RegressionMLP21 @Debugclaim
  Scenario Outline: Initiate a Claim for GPA with not a duplicate and select the awaiting decision at claimsofficer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<PortfolioManager>"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    #Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>" and review the claim unit as "<clientdecision>" from pending investigation screen "<awaitreason>"
    Then I process claim unit case "<clientdecision>" "<awaitreason>"
    Then I verify claimunit status "<awaitstatus>"
    Then I logout of the application

    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | awaitreason           | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer      | portfoliousername         | portfoliopassword | finalstatustriage        | triagecasestatus | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | awaitstatus            |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | Request for more info |           100 | File Review       |                20 | awaiting       | ClaimsOfficer_AT  | rules             | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 R3 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | PENDING- CLIENT ACTION |

  #Completed15
  #Scenario Outline: Initiate a Claim for GPA and verify the litigations
  #Given I have the external URL of VMIA
  #When User enter valid username  "<username>" and password "<password>" click on Login button
  #And I Select the Organization "<OrgName>"
  #Then I am logged in to the insurance portal
  #Then I want to make a claim
  #And I enter the date and time of loss
  #And I select the type of "<loss>"
  #And Click Continue button
  #When I select the business row and verify listed policies for "<loss>"
  #And Click on Continue button for "1" from claims pages
  #Then I am routed to Client Banking Account details page
  #And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
  #And Click Continue button
  #Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
  #And Click Continue button
  #Then I provide accident details "<description>" "<reportedto>" "<injury>"
  #And Click on Continue button for "4" from claims pages
  #And I check the declaration for Claims
  #Then Click Finish button
  #Then I validate the "<finalstatus>" for claims
  #And Extarct the claim number
  #Then I logout of the application
  #Given I have the URL of VMIA
  #When User when logs in as "<TriageOfficer>"
  #Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
  #Then I click its not a duplicate
  #Then verify "<finalstatustriage>"
  #Then I logout of the application
  #When I enter the "<portfoliousername>"
  #And I enter "<portfoliopassword>"
  #And I click login button
  #Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
  #Then I logout of the application
  #Given I have the URL of VMIA
  #When I enter the "<claimsofficeruser>"
  #And I enter "<claimsofficerpass>"
  #And I click login button
  #Then I open claim unit from worklist
  #Then I add a legal panel
  #Then I verify nmber of litigations
  #Then I logout of the application
  #
  #@Regression_MLP2_Set2_NotinScope @Golive_26_Today @Golive_26_Today_mlp2_Not
  #Examples:
  #| finalclaimunitstatus | name    | denialstatusport | awaitreason | denialstatus | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername       | portfoliopassword | finalstatustriage        | triagecasestatus | TriageofficerUsername  | Triageofficerpswd | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
  #| RESOLVED-DENIED      | Lowanna | PENDING-DENY     | NA          | PENDING-DENY |           100 |                20 |                20 | deny           | claimsofficer1    | rules      | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | ClaimsPortfolioManager_AT | rules      | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | TriageClaimsOfficer1R3 | rules      | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |
  #Completed16
  @RegressionMLP21
  Scenario Outline: Initiate a Claim for Property
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details "<Where>" "<What>" "<How>"
    #Then I select the property damages
    And Click Continue button
    Then I add item description "<itemdesc>" "<purprice>" "<coverage>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @Regression_MLP2_Set2 @Golive_26 @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | username                       | password      | Loss     | Where   | name     | What       | How          | purchasedescription | cost | salvagecost | category                       | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | filepath                                      |
      | pavan.gangone@areteanstech.com | Areteans@1981 | Property | Highway | Lowanna2 | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | Repair or replacement invoices | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt |

  #Completed17
  @RegressionMLP21 @mlpdebug
  Scenario Outline: Start a new policy application for Motor Vehicle without deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<Orgname>"
    Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    #Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the risk page with machinery "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<machineryflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for MV with machinery "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<machineryflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<pricemachinery>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP2_Set2_Rerun_Pass @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | product | parkingslots | multiplier | gst | stampduty | startdate   | status                   | days | paymentstatus   | lightvehicleflag | busflag | trailerflag | machineryflag | otherflag | typeofvehicle | manufactureyear | make | model | regno | currentmarketvalue | numberofvehicles | pricelightvehicle | pricebusunder50 | pricebusover50 | pricetrailer | pricemachinery |
      | MVF     |          400 |      11.98 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |  283 | PENDING-PAYMENT | false            | false   | false       | true          | false     | SUV           |            2011 | ABC  | n97   |  8798 |              40000 |                1 |               450 |             620 |         891.78 |           50 |            620 |

  #Completed18
  @RegressionMLP21
  Scenario Outline: Validate client search functionality in claims officer portal for organisation search
    Given I have the URL of VMIA
    When User when logs in as "<ClaimsOfficer>"
    Then I search for the "<orgname>" and validate with org id as "<exporgid>"
    Then I logout of the application

    @Regression_MLP2_Set2_Pass @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | ClaimsOfficer | orgname           | exporgid   |
      | ClaimsOfficer | Automation School | BUNIT-9927 |

  #Completed19
  @RegressionMLP21 @Debugcon6
  Scenario Outline: Validate client search functionality in claims officer portal for contact search
    Given I have the URL of VMIA
    When User when logs in as "<ClaimsOfficer>"
    Then I search for the "<firstname>" and validate with contact id as "<contactid>"
    Then I logout of the application

    @Regression_MLP2_Set2_Pass @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | ClaimsOfficer | firstname | contactid  |
      | ClaimsOfficer | Abhinav   | CONT-27533 |

  #Completed20
  @RegressionMLP21
  Scenario Outline: Initiate a Claim for Monument damage
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Cemetry Organization "<CemetryOrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I provide interment holder details "<holdername>" "<holderlastname>" "<contactfirstname>" "<contactlastname>" "<addressloopupinput>" "<address1>" "<state>""<contactphonenumber>"
    And Click Continue button
    Then I provide incident details "<firstname>" "<lastname>" "<incidentlocation>" "<plotnumber>" "<damagereason>" "<amountclaimed>"
    And Click Continue button
    #Then I attach an evidence file without dropdown "<filepath>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @Regression_MLP2_Set2_Pass @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | Loss            | firstname | lastname | incidentlocation | plotnumber | damagereason | amountclaimed | holdername | holderlastname | contactfirstname | contactlastname | addressloopupinput | address1       | contactphonenumber | Where   | What       | How          | purchasedescription | cost | salvagecost | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | state           |
      | Monument damage | Shane     | Watson   | Victoria         |         36 | earthquake   |          3000 | harry      | gurney         | Steve            | Smith           | Victoria           | Victoria Hotel |         8978986787 | Highway | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | New South Wales |

  #Completed21
  @RegressionMLP21
  Scenario Outline: Initiate a Claim for Cyber
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Cemetry Organization "<CemetryOrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    #Then I attach an evidence file "<filepath>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @Regression_MLP2_Set2_Rerun_PASS @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | Loss            | incident | action | damagereason | Where   | What       | How          | purchasedescription | cost | salvagecost | category | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |
      | Cyber liability | inc      | act    | earthquake   | Highway | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | Invoice  | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |

  ##################################MLP2Reg____Set3##########################################################################3
  #Completed22
  @RegressionMLP
  Scenario Outline: Initiate a Claim for Liability damage
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I add incident details for liability claims "<incident>" "<nameofclaimant>" "<incidentlocation>" "<address>"
    And Click Continue button
    #Then I attach an evidence file "<filepath>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @Regression2_MLP2_Set3 @Golive_26_Today @Golive_26_Today_mlp2_0323
    Examples:
      | incident | nameofclaimant | name     | incidentlocation | plotnumber | damagereason | amountclaimed | holdername | holderlastname | contactfirstname | contactlastname | address                                            | contactphonenumber | username                       | password      | loss                         | Where   | What       | How          | purchasedescription | cost | salvagecost | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |
      | Shane    | Watson         | Lowanna4 | Victoria         |         36 | earthquake   |          3000 | harry      | gurney         | Steve            | Smith           | Victoria Hotel, 82 Queen Street, BARRABA  NSW 2347 |         8978986787 | pavan.gangone@areteanstech.com | Areteans@1981 | Public and product liability | Highway | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |

  #Completed23
  @RegressionMLP2not
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When User when logs in as "<PortfolioManager>"
    #Then I am logged in to the insurance portal
    #Then I want to make a claim
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>" more than allowed
    Then I logout of the application
    When I enter the "<ceouser>"
    And I enter "<ceopass>"
    And I click login button
    Then I search the estimate approval file in workbasket and approve the estimate "<pendingapprstatus>" "<approvalstatus>"
    Then I logout of the application
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I process claim unit case "<clientdecision>" "<awaitreason>"
    Then I verify claimunit status "<approvestatus>"
    Then I resolve claim file
    Then I verify claimunit status "<resolvestatus>"
    Then I logout of the application

    @Regression2_MLP2_Set3_Rerun @Golive_26_Today
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | awaitreason | ceouser | ceopass | pendingapprstatus | approvalstatus    | resolvestatus      | awaitreason | approvestatus      | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername         | portfoliopassword | finalstatustriage        | triagecasestatus | username                       | password      | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | name     |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | NA          | ceo     | rules   | PENDING-APPROVAL  | RESOLVED-APPROVED | RESOLVED-COMPLETED | NA          | PENDING-COMPLETION |        300000 |                20 |                20 | approve        | claimsofficer1    | rules             | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | pavan.gangone@areteanstech.com | Areteans@1981 | TriageClaimsOfficer1R3      | rules                       | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | Lowanna4 |

  #Completed24
  @RegressionMLP2_Rerun_NeedCheck
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager make payment
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<PortfolioManager>"
    #Then I am logged in to the insurance portal
    #Then I want to make a claim
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then I add single payment option
    Then I verify claimunit status "<approvestatus>"
    Then I resolve claim filed
    #Then I verify claimunit status "<resolvestatus>"
    Then I logout of the application

    @Regression2_MLP2_Set3_Rerun @Golive_26_Today
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | resolvestatus      | awaitreason | approvestatus         | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername         | portfoliopassword | finalstatustriage        | triagecasestatus | username                       | password      | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | name     |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | RESOLVED-COMPLETED | NA          | PENDING-INVESTIGATION |           100 | File-Review       |                20 | approve        | ClaimsOfficer_AT  | rules             | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 R3| ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | pavan.gangone@areteanstech.com | Areteans@1981 | TriageClaimsOfficer1R3      | rules                       | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | Lowanna4 |

  #Completed25
  @RegressionMLP21 @Debg
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer then transfer claim from portfolio manager make payment
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<PortfolioManager>"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I prepare estimatee "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    #Then I make payment and add new payee "<payeetypedropdown>" "<payeename>" "<BSB>" "<AccountNumber>" "<EmailID>"
    Then I logout of the application

    @Regression2_MLP2_Set3_Rerun @Golive_26_Today
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | payeetypedropdown | payeename             | resolvestatus      | awaitreason | approvestatus      | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername         | portfoliopassword | finalstatustriage        | triagecasestatus | username                       | password      | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | name     |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | Contractor        | Automation_Contractor | RESOLVED-COMPLETED | NA          | PENDING-COMPLETION |           100 | File-Review       |                20 | approve        | claimsofficer1    | rules             | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 R3 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | pavan.gangone@areteanstech.com | Areteans@1981 | TriageClaimsOfficer1R3      | rules                       | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | Lowanna4 |

  #Completed26
  Scenario Outline: Initiate a Claim for DO insurance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    And I provide general details for DO insurance
    And Click Continue button
    And I provide general details for unlawful termination
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @Regression2_MLP2_Set3 @Golive_26_Today
    Examples:
      | OrgName | resolvestatus      | awaitreason | approvestatus      | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername         | portfoliopassword | finalstatustriage            | triagecasestatus | username                       | password      | triageclaimsofficerusername | triageclaimsofficerpassword | Loss                           | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | Qwanna  | RESOLVED-COMPLETED | NA          | PENDING-COMPLETION |           100 |                20 |                20 | approve        | claimsofficer1    | rules             | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIMUNIT COMPLETION | PENDING-TRIAGE   | pavan.gangone@areteanstech.com | Areteans@1981 | TriageClaimsOfficer1R3      | rules                       | Directors & Officers Liability | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  #Completed27
  @RegressionMLP2_NeedCheck
  Scenario Outline: Initiate a Claim for GPA and add recoverables third party from claims officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When User when logs in as "<PortfolioManager>"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then I prepare recoverable for third party "<recoverableamount>" "<recoverabletype>"
    Then I add single payment option
    Then I verify claimunit status "<approvestatus>"
    Then I resolve claim filee
    #Then I verify claimunit status "<resolvestatus>" (This step cannot be tested since the payment has to be done automatically)
    Then I logout of the application

    @Regression2_MLP2_Set3_Rerun @Golive_26_Today @Golive_26_Today_mlp2 @Golive_26_Today_mlp2_0323
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | recoverabletype      | name     | resolvestatus      | awaitreason | approvestatus         | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername         | portfoliopassword | finalstatustriage        | triagecasestatus | username                       | password      | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | Third party recovery | Lowanna4 | RESOLVED-COMPLETED | NA          | PENDING-INVESTIGATION |           100 | File review       |                20 | approve        | ClaimsOfficer_AT  | rules             | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | pavan.gangone@areteanstech.com | Areteans@1981 | TriageClaimsOfficer1R3      | rules                       | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  #Completed28
  @RegressionMLP21 @RegressionMLP2_Failed @RegressionMLP20407123 @RegressionMLP2_Rerun123456
  Scenario Outline: Initiate a Claim for GPA and add recoverables reinsurance from claims officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Then I close the external link
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When User when logs in as "<PortfolioManager>"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    #Then I prepare recoverable for reinsurance "<recoverableamount>" "<recoverabletype>"
    Then I add single payment option
    Then I verify claimunit status "<approvestatus>"
    #    Then I resolve claim filede
    Then I logout of the application

    @Regression2_MLP2_Set3 @Golive_26_Today
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | recoverabletype | name     | resolvestatus      | awaitreason | approvestatus         | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername         | portfoliopassword | finalstatustriage        | triagecasestatus | username                       | password      | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | Reinsurance     | Lowanna4 | RESOLVED-COMPLETED | NA          | PENDING-INVESTIGATION |           100 | File review       |                20 | approve        | claimsofficer1    | rules             | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 R3 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | pavan.gangone@areteanstech.com | Areteans@1981 | TriageClaimsOfficer1R3      | rules                       | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  #Completed29
  Scenario Outline: Create a new event in claims officer portal
    Given I have the URL of VMIA
    When User when logs in as "<ClaimsOfficer>"
    Then I want to make a claim for client adviser
    Then I add a new event category "<eventname>"
    Then I logout of the application

    @Regression2_MLP2_Set3_Pass
    Examples:
      | ClaimsOfficer | eventname          |
      | ClaimsOfficer | automation_event_1 |

  #Completed30
  @NotrequredRegressionMLP2
  Scenario Outline: Initiate a Claim for GPA and associate event from claims officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    Then I am logged in to the insurance portal
    And I Select the Organization "<name>"
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When User when logs in as "<PortfolioManager>"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then I associate event with claim "<eventname>"
    Then I verify claimunit status "<approvestatus>"
    #Then I resolve claim filed
    Then I logout of the application

    @Regression2_MLP2_Set3 @Golive_26_Today
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | eventname    | resolvestatus      | awaitreason | approvestatus         | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername         | portfoliopassword | finalstatustriage        | triagecasestatus | username                       | password      | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | name     |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | sanitytestqa | RESOLVED-COMPLETED | NA          | PENDING-INVESTIGATION |           100 | File review       |                20 | approve        | claimsofficer1    | rules             | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | pavan.gangone@areteanstech.com | Areteans@1981 | TriageClaimsOfficer1R3      | rules                       | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | Lowanna4 |

  #Completed32
  Scenario Outline: Create a contact using client portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    Then I want to click view organization profile
    Then I want to create new contact "<title>" "<firstname>" "<lastname>" "<phonenumber>" "<email>" "<acnttype>"
    Then I logout of the application

    @Regression2_MLP2_Set3_Pass @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | username                       | password      | title    | firstname | lastname | phonenumber | email    | acnttype |
      | pavan.gangone@areteanstech.com | Areteans@1981 | Engineer | Ab        | Ban      |  8987633457 | ag@g.com | Claims   |

  #Completed 33
  Scenario Outline: Update a contact using client portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    Then I want to click view organization profile
    Then I want to update new contact "<updatedtitle>"
    Then I logout of the application

    @Regression2_MLP2_Set3_Pass @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | username                       | password      | updatedtitle |
      | pavan.gangone@areteanstech.com | Areteans@1981 | SrEngineer   |

  #Completed 34
  @RegressionMLP21
  Scenario Outline: Enable/Disable a contact using client portal
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    Then I want to click view organization profile
    Then I want to enable or disable contact
    Then I logout of the application

    @Regression2_MLP2_Set3_Pass @Golive_26_Today @Golive_26_Today_mlp2
    Examples:
      | username                       | password      | updatedtitle |
      | pavan.gangone@areteanstech.com | Areteans@1981 | SrEngineer   |

  #Completed 35
  Scenario Outline: Initiate a Claim for GPA cemetry
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    Then I am logged in to the insurance portal
    And I Select the Cemetry Organization "<CemetryOrgName>"
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    Then Download claim form available
    Then I logout of the application

    @Regression2_MLP2_Set3 @Golive_26_Today @Golive_26_Today_mlp2 @Golive_26_Today_mlp2_0323
    Examples:
      | CemetryOrgName1          | Loss                                 |
      | Perth New Cemetery Trust | Group Personal Accident (Volunteers) |

  #Completed36
  @RegressionMLP2_Not
  Scenario Outline: Make a claim from client adviser portal Cemetery
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on make a claim
    Then I enter business details for claim using orgname "<orgname>"
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    And I provide general details for DO insurance
    And Click Continue button
    And I provide general details for unlawful termination
    And Click on Continue button for "1" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logoff from client adviser portal

    @Regression2_MLP2_Set3_Pass @Golive_26_Today @Golive_26_Today_mlp2 @Golive_26_Today_mlp2_0323
    Examples:
      | ClientAdvserUname | ClientAdvserpswd | orgname               | Loss                           | what     | claimantname | addline1 | addline2 | suburb    | state           | postcode | nature  | treatment | category                       | filepath                              | finalstatus        |
      | AutoClientAdviser | Welcome@1234     | Rahara Cemetery Trust | Directors & Officers Liability | Highwaay | liabClaimant | Sydney   | Central  | Chatswood | New South Wales |     9897 | naturee | plaster   | Repair or replacement invoices | C:\\Attachments\\Assertions_Added.txt | RESOLVED-COMPLETED |

  #Completed37
  @RegressionMLP21 @RegressionMLP209042021
  Scenario Outline: Start a new policy application  for property and Make Endorsement under client Adviser portal
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    And I validate the "<status>"
    Then I accept the quote
    # And I verify the payment "<paymentstatus>"
    Then I logoff from client adviser portal
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<updatedvalue>" as "<propertyvalue>"
    And I do not want to change anymore values
    And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for property "<propertyvalue>" "<minimumvalue>" "<updatedminimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logoff from client adviser portal

    @Regression2_MLP2_Set3 @Golive_26_Today
    Examples:
      | ClientAdvserUname | ClientAdvserpswd | product | value          | updatedvalue   | propertyvalue | minimumvalue | updatedminimumvalue | percentage | gst | stampduty | startdate   | client_number | status                   | pendingstatus        | days | uwusername     | uwpassword  | paymentstatus   | endpaymentstatus |
      | AutoClientAdviser | Welcome@1234     | PRO     | Up to $250,000 | Up to $500,000 |        100000 |          950 |                1500 |          1 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter_AT | Welcome@321 | PENDING-PAYMENT | PENDING-PAYMENT  |

  #Completed38
  @RegressionMLP21 @RegressionMLP209042021 @RegDebug123
  Scenario Outline: Start a new policy and then endorse it for GPA in client adviser portal and make two continuos Endorsement from client Adviser portal only
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
    And Click UW Continue button
    Then I enter number of "<updatedstudents>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
    And Click UW Continue button
    Then I enter number of "<updatedstudents>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus_1>"
    Then I logoff from client adviser portal

    @Regression2_MLP2_Set3 @Golive_26_Today
    Examples:
      | ClientAdvserUname | ClientAdvserpswd | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   | endpaymentstatus | endpaymentstatus_1 |
      | AutoClientAdviser | Welcome@1234     | GPA     |      200 |             500 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |  195 | PENDING-PAYMENT | PENDING-PAYMENT  | RESOLVED-COMPLETED |

  #Completed39
  @RegressionMLP21 @RegressionMLP20405 @RegressionMLP209042021 @RegDebug
  Scenario Outline: Start a new policy and then endorse it with negative premium for GPA in client adviser portal
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
    And Click UW Continue button
    Then I enter number of "<updatedstudents>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logoff from client adviser portal

    @Regression2_MLP2_Set3 @Golive_26_Today
    Examples:
      | ClientAdvserUname | ClientAdvserpswd | product | students | updatedstudents | multiplier | gst | stampduty | startdate   | client_number | status                   | days | paymentstatus   | endpaymentstatus |
      | AutoClientAdviser | Welcome@1234     | GPA     |      500 |             200 |        5.5 | 0.1 |       0.1 | currentdate | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |  194 | PENDING-PAYMENT | PENDING-REFUND   |

  #Completed40
  @RegressionMLP21 @RegressionMLP20405 @RegressionMLP209042021 @RegDebug
  Scenario Outline: Initiate a new application for GPA with full year deviation and complete the uw process under the client adviser portal
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<pendingstatus>"
    Then I logoff from client adviser portal
    Then I close the external link
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then Now I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    Then I logoff from client adviser portal

    @Regression2_MLP2_Set3 @Golive_26_Today
    Examples:
      | ClientAdvserUname | ClientAdvserpswd | product | students | updatedstudents | multiplier | gst | stampduty | startdate  | client_number | status                   | days | pendingstatus        | endpaymentstatus | Queue_value  | paymentstatus   |
      | AutoClientAdviser | Welcome@1234     | GPA     |      500 |             200 |        5.5 | 0.1 |       0.1 | 01/07/2020 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |  365 | PENDING-UNDERWRITING | PENDING-REFUND   | Underwriting | PENDING-PAYMENT |

  #Completed41
  @RegressionMLP21 @RegressionMLP209042021 @RegDebug
  Scenario Outline: Start a new policy application for Motor Vehicle in client adviser without deviation and make one endorsement
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the risk page "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter business details for claim
    Then I select the policy
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the endorsement risk page "<Endorsedlightvehicleflag>" "<Endorsedbusflag>" "<Endorsedtrailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for MV "<Endorsedlightvehicleflag>" "<Endorsedbusflag>" "<Endorsedtrailerflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logoff from client adviser portal

    @Regression2_MLP2_Set3 @Golive_26_Today
    Examples:
      | ClientAdvserUname | ClientAdvserpswd | product | parkingslots | multiplier | gst | stampduty | startdate   | status                   | days | endpaymentstatus | paymentstatus   | lightvehicleflag | Endorsedlightvehicleflag | busflag | Endorsedbusflag | trailerflag | Endorsedtrailerflag | otherflag | typeofvehicle | manufactureyear | make | model | regno | currentmarketvalue | numberofvehicles | pricelightvehicle | pricebusunder50 | pricebusover50 | pricetrailer |  |
      | AutoClientAdviser | Welcome@1234     | MVF     |          400 |      11.98 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |  208 | PENDING-PAYMENT  | PENDING-PAYMENT | true             | false                    | true    | false           | false       | true                | false     | SUV           |            2011 | ABC  | n97   |  8798 |              40000 |                1 |               450 |             620 |         891.78 |           50 |  |

  #Completed42
  @RegressionMLP
  Scenario Outline: Start a new policy application for MSC in client adviser with deviation and complete the underwritting process by applying uw filter under the client adviser portal
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User enter valid username  "<ClientAdvserUname>" and password "<ClientAdvserpswd>" click on Login button for internal user
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then I accept the quote
    And I verify the payment status "<paymentstatus>"
    Then I logoff from client adviser portal

    @Regression2_MLP2_Set3 @Golive_26_Today @Golive_26_Today_mlp2_Rerun2003
    Examples:
      | ClientAdvserUname | ClientAdvserpswd | product | parkingslots | updatedparkingslots | multiplier | gst | stampduty | startdate | status                   | days | endpaymentstatus | paymentstatus   | pendingstatus        | Queue_value  |
      | AutoClientAdviser | Welcome@1234     | MSC     |          400 |                 500 |         12 | 0.1 |       0.1 | 1/07/2020 | PENDING-CLIENTACCEPTANCE |  365 | PENDING-PAYMENT  | PENDING-PAYMENT | PENDING-UNDERWRITING | Underwriting |
