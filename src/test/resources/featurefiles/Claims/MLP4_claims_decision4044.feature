Feature: Claims MLP4 Sprint 4

    #@MPL4 @Indeminity
#		Scenario Outline: Creating medical Ideminity claim and make ideminity decision 
    #Given User enters the Internal URL from Browser
    #When User when logs in as "<TriageOfficer>"
    #Then I am logged in to the insurance portal
    #Then I want to make a claim from TriageOfficer
    #Then Search and select Oraganization "<Organization>" 
    #And Select contact from contact "<contact>"
    #And I enter the date and time of loss
    #And I select the type of "<loss>"
    #And Click Continue button 
    #When I select the business row and verify listed policies for "<loss>"
    #And Click Continue button
    #And Click Continue button
    #And Select Urgency level "NO" and Claim contact "Yes" of the claim
    #And Click Continue button
    #And Select the claim source dropdown "Incident report"
    #And Click Continue button
    #And Enter the patient details "Test" "Regression" "Male" "12/may/1999" "MRNO" "Public"
    #And Click Continue button
    #And Select patient whether patient is claimant "Yes"
    #And Click Continue button
    #And Enter the claims incident details "incidentDes" "Factaccount" "Dermatology" "Inpatient" "Yes"
    #And Click Continue button
    #And Click Continue button
    #And I check the declaration for Claims
    #Then Click Finish button
    #Then I validate the "<finalstatus>" for claims
    #And Extarct the claim number
    #And Open the claim from "Triage Claims" queue
   #	And review claim and check dupilicate
   #	And Select claim category "B" "Yes" "NO"
   #	And Validated the medical claim indemnity case status "PENDING-CLAIM COMPLETION"
   #	Then I logout of the application
   #	When User when logs in as "PortfolioManager"
    #Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    #Then I logout of the application
    #Given I have the URL of VMIA
    #When User when logs in as "ClaimsOfficer"
    #Then I open claim unit from worklist
    #And Select the indeminity decision "<Decision>"	
    #Then I validate the "<PENDING-INVESTIGATION>" for claims
    #
   #Examples: 
    #|Organization         | TriageOfficer | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |invaliduser  |  usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType    | PENDING-INVESTIGATION |
    #|Rahara Cemetery Trust| TriageOfficer | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Medical indemnity       | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | operator01  | Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full           | PENDING-INVESTIGATION |
   #|Rahara Cemetery Trust| TriageOfficer | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Medical indemnity       | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | operator01  | Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Partial        | PENDING-INVESTIGATION |
    
#
    #@MPL4 @Indeminity
#		Scenario Outline: Creating medical Ideminity claim and make ideminity decision 
    #Given User enters the Internal URL from Browser
    #When User when logs in as "<TriageOfficer>"
    #Then I am logged in to the insurance portal
    #Then I want to make a claim from TriageOfficer
    #Then Search and select Oraganization "<Organization>" 
    #And Select contact from contact "<contact>"
    #And I enter the date and time of loss
    #And I select the type of "<loss>"
    #And Click Continue button 
    #When I select the business row and verify listed policies for "<loss>"
    #And Click Continue button
    #And Click Continue button
    #And Select Urgency level "NO" and Claim contact "Yes" of the claim
    #And Click Continue button
    #And Select the claim source dropdown "Incident report"
    #And Click Continue button
    #And Enter the patient details "Test" "Regression" "Male" "12/may/1999" "MRNO" "Public"
    #And Click Continue button
    #And Select patient whether patient is claimant "Yes"
    #And Click Continue button
    #And Enter the claims incident details "incidentDes" "Factaccount" "Dermatology" "Inpatient" "Yes"
    #And Click Continue button
    #And Click Continue button
    #And I check the declaration for Claims
    #Then Click Finish button
    #Then I validate the "<finalstatus>" for claims
    #And Extarct the claim number
    #And Open the claim from "Triage Claims" queue
   #	And review claim and check dupilicate
   #	And Select claim category "B" "Yes" "NO"
   #	And Validated the medical claim indemnity case status "PENDING-CLAIM COMPLETION"
   #	Then I logout of the application
   #	When User when logs in as "<PortfolioManager>"  
    #Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    #Then I logout of the application
    #Given I have the URL of VMIA
    #When User when logs in as "<ClaimsOfficer>"
    #Then I open claim unit from worklist
    #And Select the indeminity decision "<Decision>""<GrantedType>"
    #Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    #When User when logs in as "<PortfolioManager>"
    #And Open the claim from "Claims Portfolio Manager 1 R3" queue
    #And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    #Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    #	
   #Examples: 
    #|Organization         | TriageOfficer | PortfolioManager | ClaimsOfficer | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |invaliduser  |  usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType    | PENDING-INVESTIGATION | PENDING - INDEMNITY DECISION | DenailDecision                  |
    #|Rahara Cemetery Trust| TriageOfficer | PortfolioManager | ClaimsOfficer | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Medical indemnity       | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | operator01  | Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Denied   | Full           | PENDING-INVESTIGATION | PENDING - INDEMNITY DECISION | Approve                         |
    #
        #@MPL4 @Indeminity
#		Scenario Outline: Creating medical Ideminity claim and make ideminity Request more information decision 
    #Given User enters the Internal URL from Browser
    #When User when logs in as "<TriageOfficer>"
    #Then I am logged in to the insurance portal
    #Then I want to make a claim from TriageOfficer
    #Then Search and select Oraganization "<Organization>" 
    #And Select contact from contact "<contact>"
    #And I enter the date and time of loss
    #And I select the type of "<loss>"
    #And Click Continue button 
    #When I select the business row and verify listed policies for "<loss>"
    #And Click Continue button
    #And Click Continue button
    #And Select Urgency level "NO" and Claim contact "Yes" of the claim
    #And Click Continue button
    #And Select the claim source dropdown "Incident report"
    #And Click Continue button
    #And Enter the patient details "Test" "Regression" "Male" "12/may/1999" "MRNO" "Public"
    #And Click Continue button
    #And Select patient whether patient is claimant "Yes"
    #And Click Continue button
    #And Enter the claims incident details "incidentDes" "Factaccount" "Dermatology" "Inpatient" "Yes"
    #And Click Continue button
    #And Click Continue button
    #And I check the declaration for Claims
    #Then Click Finish button
    #Then I validate the "<finalstatus>" for claims
    #And Extarct the claim number
    #And Open the claim from "Triage Claims" queue
   #	And review claim and check dupilicate
   #	And Select claim category "B" "Yes" "NO"
   #	And Validated the medical claim indemnity case status "PENDING-CLAIM COMPLETION"
   #	Then I logout of the application
   #	When User when logs in as "<PortfolioManager>"  
    #Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    #Then I logout of the application
    #Given I have the URL of VMIA
    #When User when logs in as "<ClaimsOfficer>"
    #Then I open claim unit from worklist
    #And Select the indeminity decision "<Decision>""<GrantedType>"	
    #Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    #When User when logs in as "<PortfolioManager>"
    #And Open the claim from "Claims Portfolio Manager 1 R3" queue
    #And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    #Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    #	
   #Examples: 
    #|Organization         | TriageOfficer | PortfolioManager | ClaimsOfficer | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |invaliduser  |  usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType    | PENDING-INVESTIGATION | PENDING - INDEMNITY DECISION | DenailDecision                  |DenailType |
    #|Rahara Cemetery Trust| TriageOfficer | PortfolioManager | ClaimsOfficer | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Medical indemnity       | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | operator01  | Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Denied   | Full           | PENDING-INVESTIGATION | PENDING - INDEMNITY DECISION | Request more information        |  Full     |
    #
    @MPL4 @Indeminity
		Scenario Outline: Creating medical Ideminity claim and make ideminity Decline denial decision 
    Given User enters the Internal URL from Browser
    #When User when logs in as "<TriageOfficer>"
    #Then I am logged in to the insurance portal
    #Then I want to make a claim from TriageOfficer
    #Then Search and select Oraganization "<Organization>" 
    #And Select contact from contact "<contact>"
    #And I enter the date and time of loss
    #And I select the type of "<loss>"
    #And Click Continue button 
    #When I select the business row and verify listed policies for "<loss>"
    #And Click Continue button
    #And Click Continue button
    #And Select Urgency level "NO" and Claim contact "Yes" of the claim
    #And Click Continue button
    #And Select the claim source dropdown "Incident report"
    #And Click Continue button
    #And Enter the patient details "Test" "Regression" "Male" "12/may/1999" "MRNO" "Public"
    #And Click Continue button
    #And Select patient whether patient is claimant "Yes"
    #And Click Continue button
    #And Enter the claims incident details "incidentDes" "Factaccount" "Dermatology" "Inpatient" "Yes"
    #And Click Continue button
    #And Click Continue button
    #And I check the declaration for Claims
    #Then Click Finish button
    #Then I validate the "<finalstatus>" for claims
    #And Extarct the claim number
    #And Open the claim from "Triage Claims" queue
   #	And review claim and check dupilicate
   #	And Select claim category "B" "Yes" "NO"
   #	And Validated the medical claim indemnity case status "PENDING-CLAIM COMPLETION"
   #	Then I logout of the application
   	When User when logs in as "<PortfolioManager>"  
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application 
    Given I have the URL of VMIA
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"	
    Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    When User when logs in as "<PortfolioManager>"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING-INVESTIGATION" for claims
    	
   Examples: 
    |Organization         | TriageOfficer | PortfolioManager | ClaimsOfficer | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |invaliduser  |  usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType       | PENDING-INVESTIGATION | PENDING - INDEMNITY DECISION | DenailDecision                  |DenailType |
    |Rahara Cemetery Trust| TriageOfficer | PortfolioManager | ClaimsOfficer | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Medical indemnity       | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | operator01  | Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Denied   | Partial           | PENDING-INVESTIGATION | PENDING - INDEMNITY DECISION | Decline denial                  |Partial    |
     
    