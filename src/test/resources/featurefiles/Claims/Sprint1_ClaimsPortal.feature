Feature: Claims MLP2

  @mlp2
  Scenario Outline: Initiate a Claim for GPA in triage claimsOfficer
    Given I have the URL of VMIA
    When I enter the "<claimsofficerusername>"
    And I enter "<claimsofficerpassword>"
    And I click login button
    Then I want to make a claim from claims officer
    Then I enter client details
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I provide an attachment "<attachfilepath>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    #Then I search the claim file and edit the claim
    Then I logout of the application

    Examples: 
      | attachfilepath                        | claimsofficerusername | claimsofficerpassword | loss                    | AccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | C:\\Attachments\\Assertions_Added.txt | TriageClaimsOfficer1  | Welcome@1234          | Group Personal Accident | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  @TriageEditClaim
  Scenario Outline: Initiate a Claim for GPA and edit the claim in triage claim officer
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and edit the claim "<triagecasestatus>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<ChangedAccountName>" is refed
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    Examples: 
      | triagecasestatus | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | PENDING-TRIAGE   | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  @TriageDupCheck
  Scenario Outline: Initiate a Claim for GPA and edit the claim in triage claim officer
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and edit the claim "<triagecasestatus>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<ChangedAccountName>" is refed
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    Examples: 
      | triagecasestatus | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | PENDING-TRIAGE   | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  @TriageDupCheckNotADuplicate
  Scenario Outline: Initiate a Claim for GPA and resolve claim not a duplicate in triage claim officer
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then I logout of the application

    Examples: 
      | finalstatustriage | triagecasestatus | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | hh                | PENDING-TRIAGE   | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |
