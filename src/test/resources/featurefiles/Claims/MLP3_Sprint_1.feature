Feature: MPL3 Sprint-1 Claims

  @MLP3_Sprint1
    Scenario Outline:  Verify that entered note at Triage officer should be displayed at claims officer
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then I open claim case from worklist
    Then User click on the view note and enter the text "<notetext>"
    Then User click on attachemt "<attachement>" select the required document
    Then User click postButton to submmit the note
    Then I logout of the application
    When User when logs in as "<ClaimsOfficer>"
    Then User click on Notifaction Bell and verify the "<comment>" from Triageofficer
    Then I logout of the application

    Examples: 
      | TriageOfficer |ClaimsOfficer| comment                    | notetext          | attachement                    |
      | TriageOfficer |ClaimsOfficer|Triage Claims Officer 1 R3 | @ClaimsOfficer1R3 | C:\\pega\\Documents\\vimia.txt |

  @MLP3_Sprint1-Today
  Scenario Outline: Verify that entered note at Triage officer should be displayed at claims officer
 
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then Now I click its not a duplicate
    Then Now verify "<finalstatustriage>"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<PortfolioManager>"
    Then User click on the view note and enter the text "<notetext>"
    Then User click postButton to submmit the note
    Then I logout of the application
    When User when logs in as "<ClaimsOfficer>"
    Then User click on Notifaction Bell and verify the "<comment>" from Triageofficer
    Then I logout of the application
    Examples: 
     | PortfolioManager | ClaimsOfficer  |usertotransfer       | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained |comment               | notetext| finalstatus        | statusaftertransfer   | beforetransferclaimstatus | finalstatustriage        | triagecasestatus| 
     | PortfolioManager | ClaimsOfficer  | Claims Officer 1 R3 | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | Triage Claims Officer|@ClaimsOfficer1R3|RESOLVED-COMPLETED |PENDING-INVESTIGATION | PENDING-TRIAGE   |PENDING-CLAIM COMPLETION | PENDING-TRIAGE|
 

   
 @MLP3_Sprint1-Today
  Scenario Outline: verify the undernote section for created claim with devination
  
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    #Then I close the external link
    Given I have the URL of VMIA
    When User when logs in as "<Underwriter>"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    And I accept the addnote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    #save
    And I save the policy number
    Then I logout of the application
    
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then Navigate to Claim files from Triage cliam Officer
    Then verify the unernote section for created claim
    Then Now I logout of the application

    Examples: 
      |TriageOfficer|Underwriter| username                       | password      | product | students | multiplier | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername     | uwpassword  | paymentstatus   | name     | loss                    | AccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | triageclaimsofficerusername | triageclaimsofficerpassword |
      |TriageOfficer|Underwriter| pavan.gangone@areteanstech.com | Areteans@1981 | GPA     |    15000 |        5.5 | 0.1 |       0.1 | 1/07/2020 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter_AT | Welcome@321 | PENDING-PAYMENT | Lowanna2 | Group Personal Accident | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | TriageClaimsOfficer1R3      | Welcome@1234                |

  @MLP3_Sprint1
  Scenario Outline: verify the undernote section for created claim with out devination (US-4021)
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then Navigate to Claim file from Triage cliam Officer
    Then verify the unernote section for created claim
    Then Now I logout of the application

    Examples: 
      |TriageOfficer | username                       | password      | product | students | multiplier | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername     | uwpassword  | paymentstatus   | name     | loss                    | AccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | triageclaimsofficerusername | triageclaimsofficerpassword |
      |TriageOfficer| pavan.gangone@areteanstech.com | Areteans@1981 | GPA     |    15000 |        5.5 | 0.1 |       0.1 | 1/07/2020 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter_AT | Welcome@321 | PENDING-PAYMENT | Lowanna2 | Group Personal Accident | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | TriageClaimsOfficer1R3      | Welcome@1234                |

  @MLP3_Sprint1_New
  Scenario Outline: Initiate a property and edit the claim in triage claim officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details "<Where>" "<What>" "<How>"
    And Click Continue button
    Then I add item description "<itemdesc>" "<purprice>" "<coverage>"
    And Click Continue button
    #Then I attach an evidence file without dropdown "<filepath>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and edit the claim "<triagecasestatus>"
    Then I logout of the application

    Examples: 
      | TriageOfficer | filepath                       | username                             | password          | Loss     | Where   | name     | What       | How          | purchasedescription | cost | salvagecost | category | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |
      | TriageOfficer | C:\\pega\\Documents\\vimia.txt | sudheer.kunapareddy@areteanstech.com | gjd*&$^%djdj@6789 | Property | Highway | Lowanna1 | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | SECS     | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |

  @MLP3_Sprint1
  Scenario Outline: Initiate a GPA and edit the claim in triage claim officer
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details "<Where>" "<What>" "<How>"
    And Click Continue button
    Then I add item description "<itemdesc>" "<purprice>" "<coverage>"
    And Click Continue button
    Then I attach an evidence file without dropdown "<filepath>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    And Click Continue button
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and edit the claim "<triagecasestatus>"
    Then I logout of the application

    Examples: 
      | TriageOfficer | filepath                       | Loss     | Where   | name     | What       | How          | purchasedescription | cost | salvagecost | category | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |
      | TriageOfficer | C:\\pega\\Documents\\vimia.txt | Property | Highway | Lowanna1 | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | SECS     | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |

  @MLP3_Sprint11234567890
  Scenario Outline: Protfoliomanager Bulk Transfer to claims handlers
    Given I have the URL of VMIA
    When User when logs in as "<PortfolioManager>"
    And User enter the Filter value"<ClaimHandlerName>" and "<usertotransfer>" click on the fileter get claimsUnit
    #And Select Requeried claim unit to transfer from the list
    #And Select Transfer claim option from Select actions dropdown
    Then Now I transfer the claim from portfolio manager to user "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then Now I logout of the application

    Examples: 
      | PortfolioManager | ClaimHandlerName    | usertotransfer      | beforetransferclaimstatus | statusaftertransfer   |
      | PortfolioManager | Claims Officer 1 R3 | Claims Officer 2 R3 | PENDING-TRIAGE            | PENDING-INVESTIGATION |

  @MLP3_Sprint1
  Scenario Outline: Initiate a Claim for GPA and Reopen the Claim status Reslove-completed
    Given I have the external URL of VMIA
   When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<description>" "<reportedto>" "<injury>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then I search the claim file and edit the claim "<triagecasestatus>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    #And "<ChangedAccountName>" is refed #this step is not working, need to check functionality
    And Click on Continue button for "6" from claims pages
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    Given I have the URL of VMIA
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    And User click on Action dropdown and seltect the reopen from the list
    Then I logout of the application

    Examples: 
      | triagecasestatus | username                             | password          | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | name     |
      | PENDING-TRIAGE   | sudheer.kunapareddy@areteanstech.com | gjd*&$^%djdj@6789 | TriageClaimsOfficer1        | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | Lowanna1 |

  @MLP3_Sprint1
  Scenario Outline: verify the Capture loss details fileds should not be editable
    Given I have the URL of VMIA
    When I enter the "<claimsofficerusername>"
    And I enter "<claimsofficerpassword>"
    And I click login button
    #Then I want to make a claim
    Then I want to make a claim from claims officer
    Then I enter client details
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    #Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<description>" "<reportedto>" "<injury>"
    And Click on Continue button for "3" from claims pages
    And I provide an attachment "<filepath>"
    And Click Continue button
    And verify the Capture loss details fileds should not be editable
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    #Then I search the claim file and edit the claim
    Then I logout of the application

    Examples: 
      | filepath                                      | claimsofficerusername  | claimsofficerpassword | loss                                 | AccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | description       | reportedto | injury     | finalstatus        |
      | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | TriageClaimsOfficer1R3 | Welcome@1234          | Group Personal Accident (Volunteers) | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     7001 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road | Headmaster | Broken Leg | RESOLVED-COMPLETED |

  @MLP3_Sprint1_
  Scenario Outline: Initiate a Repopen of Claim Files from portfolio manager
    Given I have the URL of VMIA
    When I enter the "<portfoliousername>"
    And I enter "<portfoliopassword>"
    And I click login button
    And User enter calimsid "<Claim_ID>" for which Resloved completed staus
    And User click on Action dropdown and seltect the reopen from the list

    Examples: 
      | portfoliousername         | portfoliopassword | Claim_ID | resolvestatus      |
      | ClaimsPortfolioManager1R3 | Welcome@1234      | CF-9002  | RESOLVED-COMPLETED |
