 Feature: Update the litigation case events and timetable
 
    @MPL4 @Indeminity
    Scenario Outline: Creating medical Ideminity claim and make ideminity decision
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I am logged in to the insurance portal
    Then I want to make a claim from TriageOfficer
    Then Search and select Oraganization "<Organization>" 
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button 
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Select Urgency level "NO" and Claim contact "Yes" of the claim
    And Click Continue button
    And Select the claim source dropdown "Incident report"
    And Click Continue button
    And Enter the patient details "Test" "Regression" "Male" "12/may/1999" "MRNO" "Public"
    And Click Continue button
    And Select patient whether patient is claimant "Yes"
    And Click Continue button
    And Enter the claims incident details "incidentDes" "Factaccount" "Dermatology" "Inpatient" "Yes"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    And Open the claim from "Triage Claims" queue
   	And review claim and check dupilicate
   	And Select claim category "B" "Yes" "NO"
   	And Validated the medical claim indemnity case status "PENDING-CLAIM COMPLETION"
   	Then I logout of the application
   	Given I have the URL of VMIA
   	When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    And Validated the medical claim indemnity case status "PENDING-INVESTIGATION"
    And User should Add litigation
    And Enter the Solicitor details "Defendant" "PavanAutomation" "14/May/2021" "PlaintiffSolicitr" "Test"
   	And Enter the Litigatation Status "Coronial" "Judgement" "County" "12345"  "14/May/2021" "14/May/2021" "Mani_qa" 
    And Enter the CoDefendent details "Ronaldo" "Gayle" "Rabada" 
   	And Enter the Timetable details "Inquest" "25/Jun/2021" "Sudheer"
   	And Enter the Event details "Yes" "15/Jun/2021" "Hi event note"	
   	And Verify the Timetable history "Yes" "15/02/0021" "25/06/2021" "Inquest" "Claims Officer 1 R3"
   	And Enter the Settlement details "Settlement" "14/May/2021" "comment" "Defendant pays" "No"
   	And Select settlement monetary "No" "500000" "500001" "comment" 
   	And Add Litigation attachment "<file>"
   	And complete the litigiation for "ClaimsOfficer"
   	Then I validate the "LITIGATION-SETTLED" for litigation
   	Then I logout of the application
   	
   Examples: 
    |Organization         | TriageOfficer | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |invaliduser  |  usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType    | PENDING-INVESTIGATION | file      |
    |Rahara Cemetery Trust| TriageOfficer | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Medical indemnity       | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | operator01  | Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full           | PENDING-INVESTIGATION | Black.png |
 