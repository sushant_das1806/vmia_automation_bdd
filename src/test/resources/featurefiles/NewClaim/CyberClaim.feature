Feature: Cyber Claim feature

    Scenario Outline: Initiate a Claim for Cyber and resolve claim as duplicate in triage claim officer
    ### Updated the name with above related name by replacing below
    ### Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer.
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    ### Added the below step, Its continue to proceed for the next page  - Select policy
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    And Click Continue button
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    ### Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    ### Replaced the above step with the below and made flow modifications here
    Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
    Then I resolve claim as duplicate
    Then Now verify "<finalPenstatus>"
    Then Now I logout of the application

    @CyberClaim  @ResolvedDuplicate @EnvironmentAndWaters @RegresionClaimMLP4 @RegressionMLP4Scenarios @testeri
    Examples:
    | Loss            | incident | action |    finalstatus       | AccountName | BSB    | AccountNumber | BankName          | EmailID     | triagecasestatus | finalstatustriage        | OrgName        |  finalPenstatus    |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED  | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE   | PENDING-CLAIM COMPLETION | Auto claim E&W | RESOLVED-DUPLICATE |

    Scenario Outline: Initiate a Claim for Construction and resolve claim as cancel in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then I search the claim file and cancel the claim from Actions Drop down
    And verify the Claims Files Status "RESOLVED-CANCELLED" after Cancel from Actions drop down
    Then I logout of the application

    @CyberClaim  @ResolvedDuplicate @EnvironmentAndWaters @RegresionClaimMLP4
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|OrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|    Auto claim E&W  |


    Scenario Outline: Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100"
    Then create Recovery estimate  "Client excess" "70"
    And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "20" "09/08/2021"
    And I verify recovery status "PAYMENT-RECOVERED"
    Then I logout of the application

    @CyberClaim  @ResolvedDuplicate @EnvironmentAndWaters @RegresionClaimMLP4
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|OrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|    Auto claim E&W  |5000         |300              |200          |

    Scenario Outline: Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @CyberClaim  @ResolvedDuplicate @EnvironmentAndWaters @RegresionClaimMLP4
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|OrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|    Auto claim E&W  |5000         |300              |200              |


    Scenario Outline: Initiate a Claim for Cyber
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<CemetryOrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application

#    @CyberClaim  @ResolvedDuplicate @Education
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto claim edu      |
    @CyberClaim  @ResolvedDuplicate @EnvironmentAndWaters
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|    Auto claim E&W  |
    @CyberClaim  @ResolvedDuplicate @GovernmentEconomy
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Auto claim G&E    |
    @CyberClaim  @ResolvedDuplicate @LawAndJustice
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Auto claim L&J	    |
    @CyberClaim  @ResolvedDuplicate @PropertyAndPlan
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|    Auto claim P&P   |
    @CyberClaim  @ResolvedDuplicate @BusinessAndIndustries
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Auto Claim B&I    |
    @CyberClaim  @ResolvedDuplicate @Ambulance
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|    Claim_Ambulance  |
    @CyberClaim  @ResolvedDuplicate @BushNursingHospital
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_BushnurshingHospital    |
    @CyberClaim  @ResolvedDuplicate @DepartmentalDivision
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_DepartmentalDivsion	    |
    @CyberClaim  @ResolvedDuplicate @HealthAndCommnutiy
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|Claim_HealthAndCommnutiy     |
    @CyberClaim  @ResolvedDuplicate @MedicalReasearchPP
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_MedicalReasearchPP|
    @CyberClaim  @ResolvedDuplicate @Publichospital
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_publichospital|
    @CyberClaim  @ResolvedDuplicate @MedicalReasearchPP
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_MedicalReasearchPP|
    @CyberClaim  @ResolvedDuplicate @SpecialistAgencies
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_SpecialistAgencies|
    @CyberClaim  @ResolvedDuplicate @SpecialistAgenciesPP
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_SpecialistAgenciesPP|
    @CyberClaim  @ResolvedDuplicate @TransportAndVechicle
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_TransportAndVechicle|


    Scenario Outline: Initiate a Claim for Cyber and cancel claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<CemetryOrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and cancel the claim from Actions Drop down
    And verify the Claims Files Status "RESOLVED-CANCELLED" after Cancel from Actions drop down
    Then I logout of the application

    @CyberClaim @ResolvedDuplicate @Education @CancelClaim
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |    Auto claim edu             |
#        @CyberClaim @ResolvedDuplicate @Environment_and_Water  @CancelClaim
#        Examples:
#            | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |CemetryOrgName |
#            | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |     Auto claim E&W            |
#        @CyberClaim @ResolvedDuplicate @Government_and_Economy  @CancelClaim
#        Examples:
#            | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |CemetryOrgName |
#            | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |
#        @CyberClaim @ResolvedDuplicate @Law_and_Justice  @CancelClaim
#        Examples:
#            | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |CemetryOrgName |
#            | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J  |
#        @CyberClaim @ResolvedDuplicate @Property_and_Planning  @CancelClaim
#        Examples:
#            | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |CemetryOrgName |
#            | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  Auto claim P&P |
#        @CyberClaim @ResolvedDuplicate @Business_and_Industry  @CancelClaim
#        Examples:
#            | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     |CemetryOrgName |
#            | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  Auto Claim B&I |
        @CyberClaim  @ResolvedDuplicate @Ambulance @CancelClaim
        Examples:
        | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
        | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|    Claim_Ambulance  |
        @CyberClaim  @ResolvedDuplicate @BushNursingHospital @CancelClaim
        Examples:
        | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
        | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_BushnurshingHospital    |
        @CyberClaim  @ResolvedDuplicate @DepartmentalDivision @CancelClaim
        Examples:
        | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
        | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_DepartmentalDivsion	    |
        @CyberClaim  @ResolvedDuplicate @HealthAndCommnutiy @CancelClaim
        Examples:
        | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
        | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|Claim_HealthAndCommnutiy     |
        @CyberClaim  @ResolvedDuplicate @MedicalReasearchPP @CancelClaim
        Examples:
        | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
        | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_MedicalReasearchPP|
        @CyberClaim  @ResolvedDuplicate @Publichospital @CancelClaim
        Examples:
        | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
        | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_publichospital|
        @CyberClaim  @ResolvedDuplicate @MedicalReasearchPP @CancelClaim
        Examples:
        | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
        | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_MedicalReasearchPP|
        @CyberClaim  @ResolvedDuplicate @SpecialistAgencies @CancelClaim
        Examples:
        | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
        | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_SpecialistAgencies|
        @CyberClaim  @ResolvedDuplicate @SpecialistAgenciesPP @CancelClaim
        Examples:
        | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
        | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_SpecialistAgenciesPP|
        @CyberClaim  @ResolvedDuplicate @TransportAndVechicle @CancelClaim
        Examples:
        | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |
        | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_TransportAndVechicle|


    Scenario Outline: Initiate a Claim for Cyber and make final payment and close the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<CemetryOrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    ### Added the below step, Its continue to proceed for the next page  - Select policy
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    ### added the below step here to launch the interaction portal
    Given I have the URL of VMIA
    When User when logs in as "TriageOfficer"
    ### Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    ### Replaced the above step with the below and made flow modifications here
    Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<CemetryOrgName>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    ### Have added new method in the above step def and updated multiple locators
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    ### After update there, where no locator find to proceed with pay info so added the locator in below step
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    ### Sprint 6 story CCI-5152, got added in below step - View Payment
    Then Verify the view payment button
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    ### Then I open claim unit from worklist
    ### Above step is replaced with below one
    Then I open the claim unit from Claims portal
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    @CyberClaim  @ResolvedDuplicate @Education  @Finalpayment @RegressionMLP4Scenarios @SprintScenarios
    Examples:
    | Loss            | incident | action |    finalstatus      | AccountName | BSB    | AccountNumber | BankName          | EmailID     |triagecasestatus | finalstatustriage        | CemetryOrgName | payableamount | outstandingamount | recoverableamount |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION | Auto claim edu | 5000          | 300               | 200               |
#    @CyberClaim  @ResolvedDuplicate @Environment_and_Water @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|Auto claim E&W | 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @Government_and_Economy @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto claim G&E| 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @Law_and_Justice @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto claim L&J| 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @Property_and_Planning @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto claim P&P| 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @Business_and_Industry @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto Claim B&I| 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @Ambulance @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName | payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|    Claim_Ambulance  | 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @BushNursingHospital @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_BushnurshingHospital    | 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @DepartmentalDivision @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_DepartmentalDivsion	    | 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @HealthAndCommnutiy @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|Claim_HealthAndCommnutiy     | 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @MedicalReasearchPP @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_MedicalReasearchPP| 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @Publichospital @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_publichospital| 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @MedicalReasearchPP @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_MedicalReasearchPP| 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @SpecialistAgencies @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_SpecialistAgencies| 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @SpecialistAgenciesPP @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_SpecialistAgenciesPP| 5000         |300              |200              |
#    @CyberClaim  @ResolvedDuplicate @TransportAndVechicle @Finalpayment
#    Examples:
#    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
#    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_TransportAndVechicle| 5000         |300              |200              |


    Scenario Outline: Initiate a Claim for Cyber and  create recovery and aprrove recovery in finance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<CemetryOrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100"
    Then create Recovery estimate  "Client excess" "70"
    And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "20" "09/08/2021"
    And I verify recovery status "PAYMENT-RECOVERED"
    Then I logout of the application
    @CyberClaim  @ResolvedDuplicate @Education @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto claim edu| 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @Environment_and_Water @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|Auto claim E&W | 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate  @Government_and_Economy @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto claim G&E| 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @Law_and_Justice @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto claim L&J| 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @Property_and_Planning @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto claim P&P| 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @Business_and_Industry @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto Claim B&I| 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @Ambulance @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName | payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|    Claim_Ambulance  | 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @BushNursingHospital @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_BushnurshingHospital    | 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @DepartmentalDivision @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_DepartmentalDivsion	    | 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @HealthAndCommnutiy @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|Claim_HealthAndCommnutiy     | 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @MedicalReasearchPP @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_MedicalReasearchPP| 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @Publichospital @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_publichospital| 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @MedicalReasearchPP @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_MedicalReasearchPP| 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @SpecialistAgencies @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_SpecialistAgencies| 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @SpecialistAgenciesPP @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_SpecialistAgenciesPP| 5000         |300              |200              |
    @CyberClaim  @ResolvedDuplicate @TransportAndVechicle @Recovery
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |payableamount|outstandingamount|recoverableamount|
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_TransportAndVechicle| 5000         |300              |200              |

    Scenario Outline: Initiate a Claim for Cyber and add litigation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<CemetryOrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    And Validated the medical claim indemnity case status "PENDING-INVESTIGATION"
    And User should Add litigation
    And Enter the Solicitor details "Defendant" "PavanAutomation" "14/May/2021" "PlaintiffSolicitr" "Test"
    And Enter the Litigatation Status "Coronial" "Judgement" "County" "12345"  "14/May/2021" "14/May/2021" "Mani_qa"
    And Enter the CoDefendent details "Ronaldo" "Gayle" "Rabada"
    And Enter the Timetable details "Inquest" "25/Jun/2021" "Sudheer"
    And Enter the Event details "Yes" "15/Jun/2021" "Hi event note"
    And Verify the Timetable history "Yes" "15/02/0021" "25/06/2021" "Inquest" "Claims Officer 1 R3"
    And Enter the Settlement details "Settlement" "14/May/2021" "comment" "Defendant pays" "No"
    And Select settlement monetary "No" "500000" "500001" "comment"
    And complete the litigiation for "ClaimsOfficer"
    Then I validate the "LITIGATION-SETTLED" for litigation
    Then I logout of the application

    @CyberClaim  @ResolvedDuplicate @Litigation @Education
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto claim edu| 5000         |300              |200              |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @Litigation @Environment_and_Water
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|Auto claim E&W | 5000         |300              |200              |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @Litigation @Government_and_Economy
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto claim G&E| 5000         |300              |200              |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @Litigation @Law_and_Justice
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto claim L&J| 5000         |300              |200              |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @Litigation @Property_and_Planning
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto claim P&P| 5000         |300              |200              |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @Litigation @Business_and_Industry
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        |CemetryOrgName |payableamount|outstandingamount|recoverableamount|usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Auto Claim B&I| 5000         |300              |200              |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @Ambulance @Litigation
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|    Claim_Ambulance  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @BushNursingHospital @Litigation
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_BushnurshingHospital    | Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @DepartmentalDivision @Litigation
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_DepartmentalDivsion	    |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @HealthAndCommnutiy @Litigation
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|Claim_HealthAndCommnutiy     |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @MedicalReasearchPP @Litigation
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_MedicalReasearchPP| Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @Publichospital @Litigation
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_publichospital| Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @MedicalReasearchPP @Litigation
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_MedicalReasearchPP|Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @SpecialistAgencies @Litigation
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_SpecialistAgencies|Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @SpecialistAgenciesPP @Litigation
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_SpecialistAgenciesPP|Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |
    @CyberClaim  @ResolvedDuplicate @TransportAndVechicle @Litigation
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage|CemetryOrgName |usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION|   Claim_TransportAndVechicle|Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |


    Scenario Outline: Initiate a Claim for CyberAnd Indeminity Decision granted
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<CemetryOrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    And Validated the medical claim indemnity case status "PENDING-INVESTIGATION"
    Then I logout of the application

    @CyberClaim  @ResolvedDuplicate  @ID_Granted @Education
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Granted  | Full        | Auto claim edu |
    @CyberClaim  @ResolvedDuplicate      @ID_Granted @Environment_and_Water
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Granted  | Full        | Auto claim E&W |
    @CyberClaim  @ResolvedDuplicate      @ID_Granted
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Granted  | Full        | Auto claim G&E|
    @CyberClaim  @ResolvedDuplicate      @ID_Granted @Law_and_Justice
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Granted  | Full        |  Auto claim L&J |
    @CyberClaim  @ResolvedDuplicate      @ID_Granted @Property_and_Planning
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Granted  | Full        | Auto claim P&P |
    @CyberClaim  @ResolvedDuplicate      @ID_Granted @Business_and_Industry
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Granted  | Full        | Auto Claim B&I |

    Scenario Outline: Initiate a Claim for Cyber and Indeminity Decision Denied approved
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<CemetryOrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @CyberClaim  @ResolvedDuplicate @ID_Denied_Approve  @Education
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision  |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Full        | Approve         | Auto claim edu              |
    @CyberClaim  @ResolvedDuplicate @ID_Denied_Approve  @Environment_and_Water
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision  |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Full        | Approve         |          Auto claim E&W     |
    @CyberClaim  @ResolvedDuplicate @ID_Denied_Approve @Government_and_Economy
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision  |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Full        | Approve         |   Auto claim G&E              |
    @CyberClaim  @ResolvedDuplicate @ID_Denied_Approve  @Law_and_Justice
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision  |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Full        | Approve         |       Auto claim L&J	        |
    @CyberClaim  @ResolvedDuplicate @ID_Denied_Approve @Property_and_Planning
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision  |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Full        | Approve         |Auto claim P&P |
    @CyberClaim  @ResolvedDuplicate @ID_Denied_Approve @Business_and_Industry
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision  |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Full        | Approve         | Auto Claim B&I              |

    Scenario Outline: Initiate a Claim for Cyber and Indeminity Decision Denied and Denied type full
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<CemetryOrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @CyberClaim  @ResolvedDuplicate    @ID_FullDenial @Education
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision                | DenailType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Full        | Request more information      |  Full      | Auto claim edu              |
    @CyberClaim  @ResolvedDuplicate    @ID_FullDenial @Environment_and_Water
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision                | DenailType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Full        | Request more information      |  Full      |     Auto claim E&W          |
    @CyberClaim  @ResolvedDuplicate    @ID_FullDenial @Government_and_Economy
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision                | DenailType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Full        | Request more information      |  Full      |     Auto claim G&E            |
    @CyberClaim  @ResolvedDuplicate    @ID_FullDenial @Law_and_Justice
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision                | DenailType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Full        | Request more information      |  Full      |     Auto claim L&J	          |
    @CyberClaim  @ResolvedDuplicate    @ID_FullDenial @Property_and_Planning
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision                | DenailType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Full        | Request more information      |  Full      |     Auto claim P&P          |
    @CyberClaim  @ResolvedDuplicate    @ID_FullDenial @Business_and_Industry
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision                | DenailType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Full        | Request more information      |  Full      |Auto Claim B&I |

    Scenario Outline: Initiate a Claim for Cyber and Indeminity Decision Denied and Denied type partial                       @ID_PartialDenial
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<CemetryOrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I answer DPC question
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details for cyber claims "<incident>" "<action>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @CyberClaim  @ResolvedDuplicate @ID_PartialDenial @Education
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision                | DenailType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Partial     | Decline denial                |Partial     |  Auto claim edu             |
    @CyberClaim  @ResolvedDuplicate @ID_PartialDenial @Environment_and_Water
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision                | DenailType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Partial     | Decline denial                |Partial     | Auto claim E&W              |
    @CyberClaim  @ResolvedDuplicate @ID_PartialDenial @Government_and_Economy
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision                | DenailType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Partial     | Decline denial                |Partial     |     Auto claim G&E            |
    @CyberClaim  @ResolvedDuplicate @ID_PartialDenial @Law_and_Justice
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision                | DenailType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Partial     | Decline denial                |Partial     |        Auto claim L&J	       |
    @CyberClaim  @ResolvedDuplicate @ID_PartialDenial @Property_and_Planning
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision                | DenailType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Partial     | Decline denial                |Partial     |   Auto claim P&P            |
    @CyberClaim  @ResolvedDuplicate @ID_PartialDenial @Business_and_Industry
    Examples:
    | Loss            | incident | action |    finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID    |triagecasestatus|finalstatustriage        | Decision | GrantedType | DenailDecision                | DenailType |CemetryOrgName |
    | Cyber Liability | inc      | act    |  RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | PENDING-TRIAGE  | PENDING-CLAIM COMPLETION| Denied   | Partial     | Decline denial                |Partial     | Auto Claim B&I              |
