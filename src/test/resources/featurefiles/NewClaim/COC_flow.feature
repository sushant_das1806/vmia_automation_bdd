Feature: New COC Not intersted flow

  Scenario Outline: Initiate a Claim for Combined Liability  Edit the claim data
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    And Click on the Note Intersted Party of COC in More Actions
    And Enter the CerificateDetails "<PartyName>" "<EffetiveDate>" "<From>" "<To>"
    And Click Continue button
    And Click Continue button
    And I check the declaration
    And I Click Finish button
    And I note down the case ID
    And Verify the case status is "PENDING-APPROVAL"
    Then I logout of the application
    @CombinedLiability @Property_and_Planning  @EditClaim
    Examples:
     | name           | PartyName    | EffetiveDate | From         | To  |
     | COC_Automation | Highway      | 26/10/2021   | 26/10/2021   | 26/10/2021  |