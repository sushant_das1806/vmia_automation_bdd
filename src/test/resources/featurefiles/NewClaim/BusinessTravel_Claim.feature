Feature: Business Travel claim

  Scenario Outline: Initiate a Claim for Business travel and resolve claim as duplicate in triage claim officer
  Given I have the URL of VMIA
  Given User when logs in as "TriageOfficer"
  Then I want to make a claim from claims officer
  Then Search and select Oraganization "<OrgName>"
  And Select contact from contact "<contact>"
  And I enter the date and time of loss
  And I select the type of "<loss>"
  And Select coverage "<Coverage>" for travel insurance
  And Click Continue button
  And Select Private Claims as "No"
  And Click Continue button
  When I select the business row and verify listed policies for "<loss>"
  And Click Continue button
  ### there is no use of the below step in the existing flow, so commenting!
  ### And Select "Yes" in Is traveler part of Insured company field
  And Click Continue button
#  And Check Alert and error message for not entering the check details
  And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
  And Click Continue button
  And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
  And Select the Loss Property "<LossProerty>"
  And Click Continue button
  And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
  And Click Continue button
  And Click Continue button
  And I check the declaration for Claims
  And Click Continue button
  Then Click Finish button without Action Refresh
  Then I validate the "<finalstatus>" for claims
  And I extract the claim unit number
  And I click the exit button in claim
  ### Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
  ### Replaced the above step with the below and made flow modifications here
  Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
  Then I resolve claim as duplicate
  Then Now verify "<finalPenstatus>"
  Then Now I logout of the application

  @BTVClaim @ReslovedDuplicate @BTVInternation @Property_and_Planning @RegresionClaimMLP4 @RegressionMLP4Scenarios
  Examples:
    | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | triagecasestatus  | OrgName        | LossProerty                               |RecoverStpes |Coverage  | finalPenstatus     |
    |Travel Insurance | Flinders    | 13/07/2021    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021  | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                 |   Yes              |RESOLVED-COMPLETED | PENDING-TRIAGE    | Auto claim P&P | Baggage and personal effects and/or money | test        |Domestic  | RESOLVED-DUPLICATE |


  Scenario Outline: Initiate a Claim for Business travel and resolve claim as cancellation
  Given I have the URL of VMIA
  Given User when logs in as "TriageOfficer"
  Then I want to make a claim from claims officer
  Then Search and select Oraganization "<OrgName>"
  And Select contact from contact "<contact>"
  And I enter the date and time of loss
  And I select the type of "<loss>"
  And Select coverage "<Coverage>" for travel insurance
  And Click Continue button
  ### Below 2 steps are added
  And Select Private Claims as "No"
  And Click Continue button
  When I select the business row and verify listed policies for "<loss>"
  And Click Continue button
  ### there is no use of the below step in the existing flow, so commenting!
  ### And Select "Yes" in Is traveler part of Insured company field
  And Click Continue button
#  And Check Alert and error message for not entering the check details
  And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
  And Click Continue button
  And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
  And Select the Loss Property "<LossProerty>"
  And Click Continue button
  And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
  And Click Continue button
  And Click Continue button
  And I check the declaration for Claims
  And Click Continue button
  Then Click Finish button without Action Refresh
  Then I validate the "<finalstatus>" for claims
  And I extract the claim unit number
  And I click the exit button in claim
  ### Then I search the claim file and cancel the claim from Actions Drop down
  ### added new below step by replacing above step
  Then I search the claim with orgnization "<OrgName>" and cancel the claim from Action drop down
  And verify the Claims Files Status "RESOLVED-CANCELLED" after Cancel from Actions drop down
  Then I logout of the application

    @BTVClaim @ReslovedDuplicate @BTVInternation @Property_and_Planning @RegresionClaimMLP4 @RegressionMLP4Scenarios
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus         | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              | RecoverStpes| Coverage      |
      |Travel Insurance | Flinders    | 13/07/2021    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021  | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                 |   Yes              |RESOLVED-COMPLETED  | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim P&P| Baggage and personal effects and/or money| test        | Domestic      |


  Scenario Outline: Initiate a Claim for Business travel and create Recovery
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Select coverage "<Coverage>" for travel insurance
    And Click Continue button
    ### Below 2 steps are added
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    ### there is no use of the below step in the existing flow, so commenting!
    ### And Select "Yes" in Is traveler part of Insured company field
    And Click Continue button
    # And Check Alert and error message for not entering the check details
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
    And Select the Loss Property "<LossProerty>"
    And Click Continue button
    And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    And Click Continue button
    Then Click Finish button without Action Refresh
    And I extract the claim unit number
    And I click the exit button in claim
    ### Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    ### Replaced the above step with the below and made flow modifications here
    Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "22-Statutory Repayment" "100"
    Then create Recovery estimate  "Client excess" "70"
    ## updated the locator in the below step
    And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "10" "9/8/2021"
    And I verify recovery status "PAYMENT-RECOVERED"
    Then I logout of the application

    @BTVClaim @ReslovedDuplicate @BTVInternation @Property_and_Planning @RegresionClaimMLP4 @RegressionMLP4Scenarios
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours | IsAuthBussinesTravel | finalstatus        | finalstatustriage        | triagecasestatus  | OrgName        | LossProerty                               | RecoverStpes | Coverage      | payableamount | outstandingamount | recoverableamount |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                 |   Yes                | RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE    | Auto claim P&P | Baggage and personal effects and/or money | test         | Domestic      | 5000          | 300               | 200               |

  Scenario Outline: Initiate a Claim for Business travel and create final payment
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Select coverage "<Coverage>" for travel insurance
    And Click Continue button
    ### Below 2 steps are added
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    ### there is no use of the below step in the existing flow, so commenting!
    ### And Select "Yes" in Is traveler part of Insured company field
    And Click Continue button
#    And Check Alert and error message for not entering the check details
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
    And Select the Loss Property "<LossProerty>"
    And Click Continue button
    And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    And Click Continue button
    Then Click Finish button without Action Refresh
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    ### Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    ### Replaced the above step with the below and made flow modifications here
    Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    ### Have added new method in the above step def and updated multiple locators
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    ### After update, there where no locator find to proceed with pay info so added the locator in below step
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I validate the "<finalPaystatus>" for claims
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    ### Then I open claim unit from worklist
    ### Above step is replaced with below one
    Then I open the claim unit from Claims portal
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @BTVClaim @ReslovedDuplicate @BTVInternation @Property_and_Planning @RegresionClaimMLP4 @RegressionMLP4Scenarios
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage        | triagecasestatus  | OrgName       | LossProerty                              | RecoverStpes |Coverage      |payableamount | outstandingamount| recoverableamount| finalPaystatus |
      |Travel Insurance | Flinders    | 13/07/2021    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021  | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                 |   Yes              |RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE    | Auto claim P&P| Baggage and personal effects and/or money| test         |Domestic      | 5000         | 300              | 200              | PAYMENT PAID   |


  Scenario Outline: Initiate a Claim for Business travel and resolve claim as duplicate in triage claim officer.
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>" for claim "<loss>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Select coverage "<Coverage>" for travel insurance
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Select "Yes" in Is traveler part of Insured company field
    And Click Continue button
    And Check Alert and error message for not entering the check details
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
    And Select the Loss Property "<LossProerty>"
    And Click Continue button
    And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application


    @BTVClaim @ReslovedDuplicate @BTVInternation @Property_and_Planning
    Examples:
    | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim P&P|Baggage and personal effects and/or money| test      |International|

#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim P&P|Baggage and personal effects and/or money| test      |Domestic      |
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim P&P|Baggage and personal effects and/or money| test      |International|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim P&P|Baggage and personal effects and/or money| test      |Domestic      |
    @BTVClaim @ReslovedDuplicated @BTVInternation  @Education
    Examples:

      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim edu|Baggage and personal effects and/or money| test      |International|
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim edu|Baggage and personal effects and/or money| test      |Domestic      |
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim edu|Baggage and personal effects and/or money| test      |International|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim edu|Baggage and personal effects and/or money| test      |Domestic      |
    @BTVClaim @ReslovedDuplicated @BTVInternation  @Environment_and_Water
    Examples:

      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim E&W|Baggage and personal effects and/or money| test      |International|
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim E&W|Baggage and personal effects and/or money| test      |Domestic      |
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim E&W|Baggage and personal effects and/or money| test      |International|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim E&W|Baggage and personal effects and/or money| test      |Domestic      |
    @BTVClaim @ReslovedDuplicated @BTVInternation @Government_and_Economy
    Examples:

      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim G&E|Baggage and personal effects and/or money| test      |International|
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim G&E|Baggage and personal effects and/or money| test      |Domestic      |
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim G&E|Baggage and personal effects and/or money| test      |International|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim G&E|Baggage and personal effects and/or money| test      |Domestic      |
    @BTVClaim @ReslovedDuplicated @BTVInternation @Law_and_Justice
    Examples:

      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim L&J|Baggage and personal effects and/or money| test      |International|
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim L&J|Baggage and personal effects and/or money| test      |Domestic      |
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim L&J|Baggage and personal effects and/or money| test      |International|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim L&J|Baggage and personal effects and/or money| test      |Domestic      |
    @BTVClaim @ReslovedDuplicated @BTVInternation @Business_and_Industry
    Examples:

      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto Claim B&I|Baggage and personal effects and/or money| test      |International|
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto Claim B&I|Baggage and personal effects and/or money| test      |Domestic      |
#  |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto Claim B&I|Baggage and personal effects and/or money| test      |International|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto Claim B&I|Baggage and personal effects and/or money| test      |Domestic      |




  Scenario Outline: Initiate a Claim for Business travel Edit the claim data
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Select coverage "<Coverage>" for travel insurance
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Select "Yes" in Is traveler part of Insured company field
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
    And Select the Loss Property "<LossProerty>"
    And Click Continue button
    And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and edit the claim "<triagecasestatus>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "2" from claims pages
    Then I am routed to Client Banking Account details page
    And Click on Continue button for "<NumTime2>" from claims pages
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @BTVClaim @Payment @EditClaim  @Property_and_Planning
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage   |payableamount|outstandingamount|recoverableamount|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim P&P|Baggage and personal effects and/or money| test      |International| 5000         |300              |200              |
    @BTVClaim @Payment @EditClaim @Business_and_Industry
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage   |payableamount|outstandingamount|recoverableamount|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto Claim B&I|Baggage and personal effects and/or money| test      |International| 5000         |300              |200              |
    @BTVClaim @Payment @EditClaim @Law_and_Justice
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage   |payableamount|outstandingamount|recoverableamount|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim L&J|Baggage and personal effects and/or money| test      |International| 5000         |300              |200              |
    @BTVClaim @Payment @EditClaim @Government_and_Economy
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage   |payableamount|outstandingamount|recoverableamount|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim G&E|Baggage and personal effects and/or money| test      |International| 5000         |300              |200              |
    @BTVClaim @Payment @EditClaim @Environment_and_Water
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage   |payableamount|outstandingamount|recoverableamount|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim E&W|Baggage and personal effects and/or money| test      |International| 5000         |300              |200              |
    @BTVClaim @Payment @EditClaim  @Education
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage   |payableamount|outstandingamount|recoverableamount|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim edu|Baggage and personal effects and/or money| test      |International| 5000         |300              |200              |


  Scenario Outline: Initiate a Claim for Business travel and cancel the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>" for claim "<loss>"
    Then I am logged in to the insurance portal
#    Then I want to make a claim
#    And I enter the date and time of loss
#    And I select the type of "<loss>"
#    And Select coverage "<Coverage>" for travel insurance
#    And Click Continue button
#    When I select the business row and verify listed policies for "<loss>"
#    And Click Continue button
#    And Select "Yes" in Is traveler part of Insured company field
#    And Click Continue button
#    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
#    And Click Continue button
#    And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
#    And Select the Loss Property "<LossProerty>"
#    And Click Continue button
#    And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
#    And Click Continue button
#    And Click Continue button
#    And I check the declaration for Claims
#    Then Click Finish button
#    Then I validate the "<finalstatus>" for claims
#    And I extract the claim unit number
#    Then I logout of the application
#    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and cancel the claim from Actions Drop down
    And verify the Claims Files Status "RESOLVED-CANCELLED" after Cancel from Actions drop down
    Then I logout of the application

    @BTVClaim @Payment @CancelCalim @Property_and_Planning
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | TriageCancelStatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage   |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED | RESOLVED-CANCELLED |Auto claim P&P|Baggage and personal effects and/or money| test      |International|
#    @BTVClaim @Payment @CancelCalim @Business_and_Industry
#    Examples:
#      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | TriageCancelStatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage   |
#      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED | RESOLVED-CANCELLED |Auto Claim B&I|Baggage and personal effects and/or money| test      |International|
#    @BTVClaim @Payment @CancelCalim @Law_and_Justice
#    Examples:
#      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | TriageCancelStatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage   |
#      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED | RESOLVED-CANCELLED |Auto claim L&J|Baggage and personal effects and/or money| test      |International|
#    @BTVClaim @Payment @CancelCalim @Government_and_Economy
#    Examples:
#      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | TriageCancelStatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage   |
#      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED | RESOLVED-CANCELLED |Auto claim G&E|Baggage and personal effects and/or money| test      |International|
#    @BTVClaim @Payment @CancelCalim @Environment_and_Water
#    Examples:
#      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | TriageCancelStatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage   |
#      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED | RESOLVED-CANCELLED |Auto claim E&W|Baggage and personal effects and/or money| test      |International|
#    @BTVClaim @Payment @CancelCalim @Education
#    Examples:
#      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | TriageCancelStatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage   |
#      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED | RESOLVED-CANCELLED |Auto claim edu|Baggage and personal effects and/or money| test      |International|

  Scenario Outline: Initiate a Claim for Business travel And Indeminity Decision granted  and add litigation
#    Given I have the external URL of VMIA
#    When User enter valid username  "<username>" and password "<password>" click on Login button
#    And I Select the Organization "<OrgName>"
#    Then I am logged in to the insurance portal
#    Then I want to make a claim
#    And I enter the date and time of loss
#    And I select the type of "<loss>"
#    And Select coverage "<Coverage>" for travel insurance
#    And Click Continue button
#    When I select the business row and verify listed policies for "<loss>"
#    And Click Continue button
#    And Select "Yes" in Is traveler part of Insured company field
#    And Click Continue button
#    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
#    And Click Continue button
#    And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
#    And Select the Loss Property "<LossProerty>"
#    And Click Continue button
#    And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
#    And Click Continue button
#    And Click Continue button
#    And I check the declaration for Claims
#    Then Click Finish button
#    Then I validate the "<finalstatus>" for claims
#    And I extract the claim unit number
#    Then I logout of the application
#    Given I have the URL of VMIA
#    Given User when logs in as "TriageOfficer"
#    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
#    And I click its not a duplicate
#    Then Now verify "PENDING-CLAIM COMPLETION"
#    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    And Validated the medical claim indemnity case status "PENDING-INVESTIGATION"
    And User should Add litigation
    And Enter the Solicitor details "Defendant" "PavanAutomation" "14/May/2021" "PlaintiffSolicitr" "Test"
    And Enter the Litigatation Status "Coronial" "Judgement" "County" "12345"  "14/May/2021" "14/May/2021" "Mani_qa"
    And Enter the CoDefendent details "Ronaldo" "Gayle" "Rabada"
    And Enter the Timetable details "Inquest" "25/Jun/2021" "Sudheer"
    And Enter the Event details "Yes" "15/Jun/2021" "Hi event note"
    And Verify the Timetable history "Yes" "15/02/0021" "25/06/2021" "Inquest" "Claims Officer 1 R3"
    And Enter the Settlement details "Settlement" "14/May/2021" "comment" "Defendant pays" "No"
    And Select settlement monetary "No" "500000" "500001" "comment"
    And complete the litigiation for "ClaimsOfficer"
    Then I validate the "LITIGATION-SETTLED" for litigation
    Then I logout of the application

    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_Granted  @litigation @Education
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision | GrantedType |invaliduser|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim edu |Baggage and personal effects and/or money  | test      |Domestic      | Granted | Full        |Claims Offcier|
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_Granted @litigation @Environment_and_Water
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision | GrantedType |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim E&W |Baggage and personal effects and/or money  | test      |Domestic      | Granted | Full        |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_Granted @litigation @Government_and_Economy
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision | GrantedType |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim G&E |Baggage and personal effects and/or money  | test      |Domestic      | Granted | Full        |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_Granted @litigation @Law_and_Justice
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision | GrantedType |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim L&J |Baggage and personal effects and/or money  | test      |Domestic      | Granted | Full        |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_Granted @litigation @Property_and_Planning
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision | GrantedType |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim P&P|Baggage and personal effects and/or money  | test      |Domestic      | Granted | Full        |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_Granted @litigation @Business_and_Industry
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision | GrantedType |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto Claim B&I |Baggage and personal effects and/or money  | test      |Domestic      | Granted | Full        |

  Scenario Outline: Initiate a Claim for Business travel and Indeminity Decision Denied approved
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Select coverage "<Coverage>" for travel insurance
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Select "Yes" in Is traveler part of Insured company field
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
    And Select the Loss Property "<LossProerty>"
    And Click Continue button
    And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_Denied_Approve @Education
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision | GrantedType | DenailDecision  |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim edu|Baggage and personal effects and/or money  | test       |Domestic      |Denied   | Full        | Approve         |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_Denied_Approve @Environment_and_Water
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision | GrantedType | DenailDecision  |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim E&W|Baggage and personal effects and/or money  | test       |Domestic      |Denied   | Full        | Approve         |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_Denied_Approve @Government_and_Economy
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision | GrantedType | DenailDecision  |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim G&E|Baggage and personal effects and/or money  | test       |Domestic      |Denied   | Full        | Approve         |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_Denied_Approve @Law_and_Justice
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision | GrantedType | DenailDecision  |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim L&J|Baggage and personal effects and/or money  | test       |Domestic      |Denied   | Full        | Approve         |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_Denied_Approve @Property_and_Planning
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision | GrantedType | DenailDecision  |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim P&P|Baggage and personal effects and/or money  | test       |Domestic      |Denied   | Full        | Approve         |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_Denied_Approve @Business_and_Industry
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision | GrantedType | DenailDecision  |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto Claim B&I|Baggage and personal effects and/or money  | test       |Domestic      |Denied   | Full        | Approve         |


  Scenario Outline: Initiate a Claim for Business travel and Indeminity Decision Denied and Denied type full
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Select coverage "<Coverage>" for travel insurance
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Select "Yes" in Is traveler part of Insured company field
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
    And Select the Loss Property "<LossProerty>"
    And Click Continue button
    And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_FullDenial @Education
    Examples:

      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours  |IsAuthBussinesTravel|finalstatus             | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision  | GrantedType | DenailDecision              | DenailType |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim edu|Baggage and personal effects and/or money  | test       |Domestic      | Denied   | Full        | Request more information    |  Full      |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_FullDenial @Environment_and_Water
    Examples:

      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours  |IsAuthBussinesTravel|finalstatus             | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision  | GrantedType | DenailDecision              | DenailType |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim E&W|Baggage and personal effects and/or money  | test       |Domestic      | Denied   | Full        | Request more information    |  Full      |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_FullDenial @Government_and_Economy
    Examples:

      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours  |IsAuthBussinesTravel|finalstatus             | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision  | GrantedType | DenailDecision              | DenailType |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim G&E  |Baggage and personal effects and/or money  | test       |Domestic      | Denied   | Full        | Request more information    |  Full      |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_FullDenial @Law_and_Justice
    Examples:

      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours  |IsAuthBussinesTravel|finalstatus             | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision  | GrantedType | DenailDecision              | DenailType |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim L&J|Baggage and personal effects and/or money  | test       |Domestic      | Denied   | Full        | Request more information    |  Full      |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_FullDenial @Property_and_Planning
    Examples:

      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours  |IsAuthBussinesTravel|finalstatus             | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision  | GrantedType | DenailDecision              | DenailType |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim P&P|Baggage and personal effects and/or money  | test       |Domestic      | Denied   | Full        | Request more information    |  Full      |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_FullDenial @Business_and_Industry
    Examples:

      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours  |IsAuthBussinesTravel|finalstatus             | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage      |Decision  | GrantedType | DenailDecision              | DenailType |
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto Claim B&I|Baggage and personal effects and/or money  | test       |Domestic      | Denied   | Full        | Request more information    |  Full      |


  Scenario Outline: Initiate a Claim for Business travel and Indeminity Decision Denied and Denied type partial
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Select coverage "<Coverage>" for travel insurance
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Select "Yes" in Is traveler part of Insured company field
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
    And Select the Loss Property "<LossProerty>"
    And Click Continue button
    And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_PartialDenial @Education
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate  | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours  |IsAuthBussinesTravel|finalstatus             | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes |Coverage      |Decision   | GrantedType | DenailDecision   | DenailType |
      |Travel Insurance | Flinders     | 13/07/2021   | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim edu |Baggage and personal effects and/or money | test        |Domestic      |  Denied   | Partial     | Decline denial   |Partial     |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_PartialDenial @Environment_and_Water
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate  | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours  |IsAuthBussinesTravel|finalstatus             | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes |Coverage      |Decision   | GrantedType | DenailDecision   | DenailType |
      |Travel Insurance | Flinders     | 13/07/2021   | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    | Auto claim E&W|Baggage and personal effects and/or money | test        |Domestic      |  Denied   | Partial     | Decline denial   |Partial     |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_PartialDenial @Government_and_Economy
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate  | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours  |IsAuthBussinesTravel|finalstatus             | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes |Coverage      |Decision   | GrantedType | DenailDecision   | DenailType |
      |Travel Insurance | Flinders     | 13/07/2021   | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim G&E |Baggage and personal effects and/or money | test        |Domestic      |  Denied   | Partial     | Decline denial   |Partial     |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_PartialDenial @Law_and_Justice
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate  | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours  |IsAuthBussinesTravel|finalstatus             | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes |Coverage      |Decision   | GrantedType | DenailDecision   | DenailType |
      |Travel Insurance | Flinders     | 13/07/2021   | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim L&J|Baggage and personal effects and/or money | test        |Domestic      |  Denied   | Partial     | Decline denial   |Partial     |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_PartialDenial @Property_and_Planning
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate  | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours  |IsAuthBussinesTravel|finalstatus             | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes |Coverage      |Decision   | GrantedType | DenailDecision   | DenailType |
      |Travel Insurance | Flinders     | 13/07/2021   | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    | Auto claim P&P|Baggage and personal effects and/or money | test        |Domestic      |  Denied   | Partial     | Decline denial   |Partial     |
    @BTVClaim @ReslovedDuplicated @BTVInternation @ID_PartialDenial @Business_and_Industry
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate  | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours  |IsAuthBussinesTravel|finalstatus             | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes |Coverage      |Decision   | GrantedType | DenailDecision   | DenailType |
      |Travel Insurance | Flinders     | 13/07/2021   | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                  |   Yes              |RESOLVED-COMPLETED      | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto Claim B&I |Baggage and personal effects and/or money | test        |Domestic      |  Denied   | Partial     | Decline denial   |Partial     |



  Scenario Outline: Initiate a Claim for Business travel and make final payment and close the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Select coverage "<Coverage>" for travel insurance
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
#    And Select "Yes" in Is traveler part of Insured company field
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
    And Select the Loss Property "<LossProerty>"
    And Click Continue button
    And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @BTVClaim @FinalPayment @ResolveCompleted @Property_and_Planning
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage              | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage     |payableamount |outstandingamount|recoverableamount|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                 |   Yes              |RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Auto claim P&P |Baggage and personal effects and/or money | test       |International| 5000         |300              |200              |

  Scenario Outline: Initiate a Claim for Business travel and  create recovery and aprrove recovery in finance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Select coverage "<Coverage>" for travel insurance
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Select "Yes" in Is traveler part of Insured company field
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Enter the travel infromation of incident "<IsAuthBussinesTravel>" "<DepartureDate>" "<ReturnDate>" "<DepartureCity>" "<IncidentCity>" "<IncidentCountry>" "<ExactLocation>"
    And Select the Loss Property "<LossProerty>"
    And Click Continue button
    And Enter the DamageDetails of Baggage property "<HowDamageOccur>" "<RecoverStpes>" "<isReportedPolice>" "<IslostBycarrier>" "<IscomplainedRaisedAgainstcarrier>" "<IsLossPropertyYours>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100"
    Then create Recovery estimate  "Client excess" "70"
    And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "20" "09/08/2021"
    And I verify recovery status "PAYMENT-RECOVERED"
    Then I logout of the application

    @BTVClaim @RecoveryCreation @PaymentRecovered @Property_and_Planning
    Examples:
      | loss            | AccountName | DepartureDate | BSB    | AccountNumber | BankName          | EmailID     | ReturnDate | DepartureCity|IncidentCity | IncidentCountry | ExactLocation  | HowDamageOccur    | isReportedPolice | IslostBycarrier| IscomplainedRaisedAgainstcarrier| IsLossPropertyYours |IsAuthBussinesTravel|finalstatus        | finalstatustriage                    | triagecasestatus  |OrgName       | LossProerty                              |RecoverStpes|Coverage     |payableamount |outstandingamount|recoverableamount|
      |Travel Insurance | Flinders     | 13/07/2021  | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |20/07/2021   | London       | Berlin      | Germany         | suburb1        | Victoria          |     No           | Yes            | Yes                             | yes                 |   Yes              |RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION             | PENDING-TRIAGE    |Auto claim P&P |Baggage and personal effects and/or money | test       |International| 5000         |300              |200              |

