Feature: Property claim


  Scenario Outline: Initiate a Claim for Property with macbook
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And Check Alert and error message for not entering the check details
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details "<Where>" "<What>" "<How>"
    And Select the the damage type "<Damege>" and "<isDetorAcerRepaired>"
    And Click Continue button
    Then I add item description "<itemdesc>" "<purprice>" "<coverage>" "<isDetorAcerRepaired>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number from open case
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then Now verify "PENDING-CLAIM COMPLETION"
    Then Now I logout of the application
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then Validate the policy number in CF "<Policynum>" and CU "<DETpolicynum>"
    Then I logout of the application
    
    @Regression_MLP2_Set2 @Golive_26 @Golive_26_Today @Golive_26_Today_mlp2 @RegresionClaimMLP5
    Examples:
      | username                       | password      | Loss     | Where   | OrgName         | What       | How          | purprice | itemdesc | coverage | category                       | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | Damege          |isDetorAcerRepaired|usertotransfer|DETpolicynum|
      | pavan.gangone@areteanstech.com | Areteans@1981 | Property | Highway | Auto Claim Schl | TyreBusted | Overspeeding | 1000     | test     |  SCIP    | Repair or replacement invoices | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Apple notebook|     No           |claimsofficer1   |    A11896        |
#      | pavan.gangone@areteanstech.com | Areteans@1981 | Property | Highway | Auto Claim Schl | TyreBusted | Overspeeding | 1000     | test     |  SCIP    | Repair or replacement invoices | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Apple notebook|     Yes           |claimsofficer1||
#      | pavan.gangone@areteanstech.com | Areteans@1981 | Property | Highway | Auto Claim Schl | TyreBusted | Overspeeding | 1000     | test     |  SCIP    | Repair or replacement invoices | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Non-Apple notebook|     No           |claimsofficer1||

  Scenario Outline: Initiate a Claim for Property with non-MacBook
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And Check Alert and error message for not entering the check details
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details "<Where>" "<What>" "<How>"
    And Select the the damage type "<Damege>" and "<isDetorAcerRepaired>"
    #Then I select the property damages
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number from open case
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then Now verify "RESOLVED-COMPLETED"
    Then Now I logout of the application

    @Regression_MLP2_Set2 @Golive_26 @Golive_26_Today @Golive_26_Today_mlp2 @RegresionClaimMLP5
    Examples:
      | username                       | password      | Loss     | Where   | OrgName         | What       | How          | purchasedescription | cost | salvagecost | category                       | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | Damege            |isDetorAcerRepaired|
      | pavan.gangone@areteanstech.com | Areteans@1981 | Property | Highway | Auto Claim Schl | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | Repair or replacement invoices | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Non-Apple notebook|     Yes           |


  Scenario Outline: Initiate a Claim for Property with windows and resolve complete in traiage offcier
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And Check Alert and error message for not entering the check details
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details "<Where>" "<What>" "<How>"
    And Select the the damage type "<Damege>" and "<isDetorAcerRepaired>"
    #Then I select the property damages
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number from open case
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then Now verify "RESOLVED-COMPLETED"
    Then Now I logout of the application

    @Regression_MLP2_Set2 @Golive_26 @Golive_26_Today @Golive_26_Today_mlp2 @RegresionClaimMLP5
    Examples:
      | username                       | password      | Loss     | Where   | OrgName         | What       | How          | purprice | itemdesc | coverage | category                       | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | Damege        |isDetorAcerRepaired|
      | pavan.gangone@areteanstech.com | Areteans@1981 | Property | Highway | Auto Claim Schl | TyreBusted | Overspeeding | 1000     | test     |  SCIP    | Repair or replacement invoices | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Non-Apple notebook|     No           |


  Scenario Outline: Initiate a Claim for Property and resolve claim as duplicate in triage claim officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details "<Where>" "<What>" "<How>"
    And Select the the damage type "<Damege>" and "<isDetorAcerRepaired>"
    And Click Continue button
    And Enter the list all items damaged "<description>" "<purchasePrice>" and "<assetNumber>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number from open case
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
    Then I resolve claim as duplicate
    Then Now verify "<finalPenstatus>"
    Then Now I logout of the application

    @RegressionMLP5ClaimsScenarios
    Examples:
      | username                       | password      | Loss     | Where   | OrgName         | What       | How          | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | Damege             |isDetorAcerRepaired | triagecasestatus | finalPenstatus     | description | purchasePrice | assetNumber |
      | pavan.gangone@areteanstech.com | Areteans@1981 | Property | Highway | Auto Claim Schl | TyreBusted | Overspeeding | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Non-Apple notebook |     No             | PENDING-TRIAGE   | RESOLVED-DUPLICATE | VMIA        | 400           | 45          |

  Scenario Outline: Initiate a Claim for Property  and create final payment and close the claim
#    Given I have the external URL of VMIA
#    When User enter valid username  "<username>" and password "<password>" click on Login button
#    And I Select the Organization "<OrgName>"
#    Then I am logged in to the insurance portal
#    Then I want to make a claim
    Given I have the external URL of VMIA
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    When I select the type of of "<Loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide loss details "<Where>" "<What>" "<How>"
    And Select the the damage type "<Damege>" and "<isDetorAcerRepaired>"
    And Click Continue button
    Then I add item description "<itemdesc>" "<purprice>" "<coverage>" "<isDetorAcerRepaired>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    And Click Continue button
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
    Then I click its not a duplicate
    Then Now verify "<finalstatustriage>"
    Then Now I logout of the application
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open the claim unit from Claims portal
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    Examples:
      | username                       | password      | Loss     | Where   | OrgName        | What       | How          | purprice | itemdesc | coverage | category                       | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | Damege        |isDetorAcerRepaired| usertotransfer |DETpolicynum | finalstatustriage        | triagecasestatus | payableamount |outstandingamount| recoverableamount|
      | pavan.gangone@areteanstech.com | Areteans@1981 | Property | Highway | Auto claim P&P | TyreBusted | Overspeeding | 1000     | test     |  SCIP    | Repair or replacement invoices | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Apple notebook|     No            | claimsofficer1 |    A11896   | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |  5000         |300              | 200              |
#    Auto Claim Schl