Feature: Initiate a Claim for EmRepSS Products

  Scenario Outline: Initiate a Claim for Property - EmRePSS and edit the claim data
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And I provide Incident loss for Property "Natural" "EarthQuake"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and edit the claim "<triagecasestatus>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "2" from claims pages
    Then I am routed to Client Banking Account details page
    And Click on Continue button for "<NumTime2>" from claims pages
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @PI_Emrepss @Government_and_Economy   @EditClaim @RegresionClaimMLP5
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           |usertotransfer      | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |

    @PI_Emrepss @Environment_and_Water  @EditClaim
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name            | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |

    @PI_Emrepss @Law_and_Justice  @EditClaim
    Examples:
      | username                          | password      | loss               |  finalstatus       | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name            | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |


  Scenario Outline: Initiate a Claim for Property - EmRePSS and cancel the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And I provide Incident loss for Property "Natural" "EarthQuake"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and cancel the claim from Actions Drop down
    And verify the Claims Files Status "RESOLVED-CANCELLED" after Cancel from Actions drop down
    Then I logout of the application


    @PI_Emrepss @Government_and_Economy  @CancelClaim
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           |usertotransfer      | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |

    @PI_Emrepss @Environment_and_Water  @CancelClaim
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name            | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |

    @PI_Emrepss @Law_and_Justice  @CancelClaim
    Examples:
      | username                          | password      | loss               |  finalstatus       | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name            | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |


  Scenario Outline: Initiate a Claim for Property - EmRePSS and make final payment and close the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And I provide Incident loss for Property "Natural" "EarthQuake"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    @PI_Emrepss @Government_and_Economy @Finalpayment @RegresionClaimMLP5
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           |usertotransfer      | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |payableamount |outstandingamount|recoverableamount|
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | 5000         |300              |200              |

    @PI_Emrepss @Environment_and_Water @Finalpayment
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name            | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |payableamount |outstandingamount|recoverableamount|
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | 5000         |300              |200              |

    @PI_Emrepss @Law_and_Justice @Finalpayment
    Examples:
      | username                          | password      | loss               |  finalstatus       | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name            | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |payableamount |outstandingamount|recoverableamount|
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | 5000         |300              |200              |


  Scenario Outline: Initiate a Claim for Property - EmRePSS and  create recovery and aprrove recovery in finance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And I provide Incident loss for Property "Natural" "EarthQuake"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100"
    Then create Recovery estimate  "Client excess" "70"
    And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "20" "09/08/2021"
    And I verify recovery status "PAYMENT-RECOVERED"
    Then I logout of the application
    @PI_Emrepss @Government_and_Economy	@Recovery @RegresionClaimMLP5
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           |usertotransfer      | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |payableamount |outstandingamount|recoverableamount|
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | 5000         |300              |200              |

    @PI_Emrepss @Environment_and_Water	@Recovery
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name            | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |payableamount |outstandingamount|recoverableamount|
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | 5000         |300              |200              |

    @PI_Emrepss @Law_and_Justice	@Recovery
    Examples:
      | username                          | password      | loss               |  finalstatus       | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name            | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |payableamount |outstandingamount|recoverableamount|
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | 5000         |300              |200              |


  Scenario Outline: Initiate a Claim for Property - EmRePSS And Indeminity Decision granted  and add litigation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And I provide Incident loss for Property "Natural" "EarthQuake"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    #    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    And Validated the medical claim indemnity case status "PENDING-INVESTIGATION"
    And User should Add litigation
    And Enter the Solicitor details "Defendant" "PavanAutomation" "14/May/2021" "PlaintiffSolicitr" "Test"
    And Enter the Litigatation Status "Coronial" "Judgement" "County" "12345"  "14/May/2021" "14/May/2021" "Mani_qa"
    And Enter the CoDefendent details "Ronaldo" "Gayle" "Rabada"
    And Enter the Timetable details "Inquest" "25/Jun/2021" "Sudheer"
    And Enter the Event details "Yes" "15/Jun/2021" "Hi event note"
    And Verify the Timetable history "Yes" "15/02/0021" "25/06/2021" "Inquest" "Claims Officer 1 R3"
    And Enter the Settlement details "Settlement" "14/May/2021" "comment" "Defendant pays" "No"
    And Select settlement monetary "No" "500000" "500001" "comment"
    And complete the litigiation for "ClaimsOfficer"
    Then I validate the "LITIGATION-SETTLED" for litigation
    Then I logout of the application

    @PI_Emrepss @Government_and_Economy @Litigation @ID_Granted
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           |usertotransfer      | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |

    @PI_Emrepss @Environment_and_Water @Litigation @ID_Granted
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name            | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |

    @PI_Emrepss @Law_and_Justice @Litigation @ID_Granted
    Examples:
      | username                          | password      | loss               |  finalstatus       | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name            | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |


  Scenario Outline: Initiate a Claim for Property - EmRePSS and Indeminity Decision Denied approved
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And I provide Incident loss for Property "Natural" "EarthQuake"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
#    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @PI_Emrepss @Government_and_Economy @ID_Denied_Approve
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | DenailDecision  | finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |Denied   | Full        | Approve          | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |
    @PI_Emrepss @Environment_and_Water  @ID_Denied_Approve
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | DenailDecision  | finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |Denied   | Full        | Approve          | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |
    @PI_Emrepss @Law_and_Justice   @ID_Denied_Approve
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | DenailDecision  | finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |Denied   | Full        | Approve          | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |


  Scenario Outline: Initiate a Claim for Property - EmRePSS and Indeminity Decision Denied and Denied type full
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And I provide Incident loss for Property "Natural" "EarthQuake"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
#    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @PI_Emrepss @Government_and_Economy    @ID_FullDenial
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | DenailDecision                | DenailType |finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Denied   | Full        | Request more information      |  Full      | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |
    @PI_Emrepss @Environment_and_Water    @ID_FullDenial
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | DenailDecision                | DenailType |finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Denied   | Full        | Request more information      |  Full      | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |
    @PI_Emrepss @Law_and_Justice    @ID_FullDenial
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | DenailDecision                | DenailType |finalstatustriage                   | triagecasestatus  |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Denied   | Full        | Request more information      |  Full      | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |


  Scenario Outline: Initiate a Claim for Property - EmRePSS and Indeminity Decision Denied and Denied type partial
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And I provide Incident loss for Property "Natural" "EarthQuake"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
#    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @PI_Emrepss @Government_and_Economy  @ID_PartialDenial
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | DenailDecision                | DenailType | finalstatustriage        | triagecasestatus |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |Denied    | Partial     | Decline denial                |Partial     | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |
    @PI_Emrepss @Environment_and_Water  @ID_PartialDenial
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | DenailDecision                | DenailType | finalstatustriage        | triagecasestatus |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |Auto claim E&W |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |Denied    | Partial     | Decline denial                |Partial     | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |
    @PI_Emrepss @Law_and_Justice  @ID_PartialDenial
    Examples:
      | username                          | password      | loss               | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType | DenailDecision                | DenailType | finalstatustriage        | triagecasestatus |
      | manoj.ramanathan@areteanstech.com | Test@1234567  | Property - EmRePSS | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |Auto claim L&J|Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |Denied    | Partial     | Decline denial                |Partial     | PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    |
