Feature: FineArts static claim

Scenario Outline: Initiate a Claim for Fine Arts-Static Liability and resolve claim as duplicate in triage claim officer
    ### Updated the name with above related name by replacing below
    ### Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    ### Added the below step, Its continue to proceed for the next page  - Select policy
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Add Incident details "<LossType>" "<Loss>" "<Details>"
    And Click Continue button
    And Enter the FineArts Details for claim "<Artist>" "<Description>" "<Dimension>" "<Owner>" "<Value>"
    And Select Incident report details in claim "<Damage>" "<Incident>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    And Click Continue button
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    ### Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    ### Replaced the above step with the below and made flow modifications here
    Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
    Then I resolve claim as duplicate
    Then Now verify "<finalPenstatus>"
    Then Now I logout of the application
    @FineArtsStatic_Claim @ResolvedDuplicate @RegresionClaimMLP4 @RegressionMLP4Scenarios
    Examples:
        | loss                          | AccountName |  BSB   | AccountNumber | BankName          | EmailID     |  finalstatus        | triagecasestatus |OrgName        | finalPenstatus     | LossType | Loss | Details | Artist | Description | Dimension | Owner | Value | Damage | Incident |
        | Fine Art Exhibitions - Static | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | PENDING-TRIAGE   |Auto claim edu | RESOLVED-DUPLICATE | Theft    | Test | Address | Artist | TitleofWork | Dimension | Owner | 5000  | No     | No       |

Scenario Outline: Initiate a Claim for Construction and resolve claim as cancel in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<name>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
#    And Select coverage "Static" for FineArts claim
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Add Incident details "Theft" "Test" "Address"
    And Click Continue button
    And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
    And Select Incident report details in claim "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then I search the claim file and cancel the claim from Actions Drop down
    And verify the Claims Files Status "RESOLVED-CANCELLED" after Cancel from Actions drop down
    Then I logout of the application

    @FineArtsStatic_Claim @ResolvedDuplicate  @Government_and_Economy
    Examples:
        | loss                                  | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |name|
        | Fine Art Exhibitions - Static | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim edu |

Scenario Outline: Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<name>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
#    And Select coverage "Static" for FineArts claim
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Add Incident details "Theft" "Test" "Address"
    And Click Continue button
    And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
    And Select Incident report details in claim "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100"
    Then create Recovery estimate  "Client excess" "70"
    And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "20" "09/08/2021"
    And I verify recovery status "PAYMENT-RECOVERED"
    Then I logout of the application
    @FineArtsStatic_Claim @ResolvedDuplicate  @Government_and_Economy
    Examples:
        | loss                                  | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |name|payableamount|outstandingamount|recoverableamount|
        |Fine Art Exhibitions - Static| Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim edu|5000         |300              |200              |

Scenario Outline: Initiate a Claim for Fine Arts-Static and create final payment and close the claim
    ### I have updated the Scenario name here because the  given name is improper for the flow
    ### Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
#    And Select coverage "Static" for FineArts claim
    And Click Continue button
    ### Added the below step, Its continue to proceed for the next page  - Select policy
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Add Incident details "Theft" "Test" "Address"
    And Click Continue button
    And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
    And Select Incident report details in claim "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    And Click Continue button
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    ### Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    ### Replaced the above step with the below and made flow modifications here
    Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    ### Have added new method in the above step def and updated multiple locators
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    ### After update there, where no locator find to proceed with pay info so added the locator in below step
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    ### Then I open claim unit from worklist
    ### Above step is replaced with below one
    Then I open the claim unit from Claims portal
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @FineArtsStatic_Claim @ResolvedDuplicate  @Government_and_Economy @RegressionMLP4Scenarios
    ### finalstatustriage status was RESOLVED-DUPLICATE but we updated to PENDING-CLAIM COMPLETION based on the flow
    Examples:
        | loss                          | AccountName |  BSB   | AccountNumber | BankName          | EmailID     |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName        |payableamount|outstandingamount|recoverableamount |
        | Fine Art Exhibitions - Static | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |Auto claim edu |5000         |300              |200               |


    @Claim @Regression
    Scenario Outline: Initiate a Claim for FineArts and resolve claim as a duplicate in triage claim officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Add Incident details "Theft" "Test" "Address"
    And Click Continue button
    And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
    And Select Incident report details in claim "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application

    @FineArtsStatic_Claim @ResolvedDuplicate @Education
    Examples:
        | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|
        | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim edu|
    @FineArtsStatic_Claim @ResolvedDuplicate @Environment_and_Water
    Examples:
        | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|
        | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim E&W |
    @FineArtsStatic_Claim @ResolvedDuplicate  @Government_and_Economy
    Examples:
    | loss                                  | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|
    | Fine Art Exhibitions - Domestic Transit | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim G&E |
    @FineArtsStatic_Claim @ResolvedDuplicate @Law_and_Justice
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|
    |Fine Art Exhibitions - Domestic Transit| Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim L&J|

    Scenario Outline: Initiate a Claim for FineArts Edit the claim data
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    #        And Select coverage "Static" for FineArts claim
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Add Incident details "Theft" "Test" "Address"
    And Click Continue button
    And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
    And Select Incident report details in claim "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and edit the claim "<triagecasestatus>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "2" from claims pages
    Then I am routed to Client Banking Account details page
    And Click on Continue button for "<NumTime2>" from claims pages
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    @FineArtsStatic_Claim @edit @Education
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim edu|
    @FineArtsStatic_Claim @edit @Environment_and_Water
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim E&W |
    @FineArtsStatic_Claim @edit @Government_and_Economy
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim G&E |
    @FineArtsStatic_Claim @edit @Law_and_Justice
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim L&J|


    Scenario Outline: Initiate a Claim for FineArts and cancel the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    #        And Select coverage "Static" for FineArts claim
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Add Incident details "Theft" "Test" "Address"
    And Click Continue button
    And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
    And Select Incident report details in claim "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and cancel the claim from Actions Drop down
    And verify the Claims Files Status "RESOLVED-CANCELLED" after Cancel from Actions drop down
    Then I logout of the application
    @FineArtsStatic_Claim @cancel  @Education
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim edu|
    @FineArtsStatic_Claim @cancel @Environment_and_Water
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim E&W |
    @FineArtsStatic_Claim @cancel @Government_and_Economy
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim G&E |
    @FineArtsStatic_Claim @cancel @Law_and_Justice
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim L&J|



    Scenario Outline: Initiate a Claim for FineArts and make final payment and close the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    #        And Select coverage "Static" for FineArts claim
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Add Incident details "Theft" "Test" "Address"
    And Click Continue button
    And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
    And Select Incident report details in claim "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    @FineArtsStatic_Claim @FinalPayment  @Education
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|payableamount|outstandingamount|recoverableamount|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim edu| 5000         |300              |200              |
    @FineArtsStatic_Claim @FinalPayment @Environment_and_Water
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|payableamount|outstandingamount|recoverableamount|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim E&W | 5000         |300              |200              |
    @FineArtsStatic_Claim @FinalPayment @Government_and_Economy
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|payableamount|outstandingamount|recoverableamount|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim G&E | 5000         |300              |200              |
    @FineArtsStatic_Claim @FinalPayment @Law_and_Justice
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|payableamount|outstandingamount|recoverableamount|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim L&J| 5000         |300              |200              |


    Scenario Outline: Initiate a Claim for FineArts and  create recovery and aprrove recovery in finance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    #        And Select coverage "Static" for FineArts claim
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Add Incident details "Theft" "Test" "Address"
    And Click Continue button
    And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
    And Select Incident report details in claim "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100"
    Then create Recovery estimate  "Client excess" "70"
    And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "20" "09/08/2021"
    And I verify recovery status "PAYMENT-RECOVERED"
    Then I logout of the application

    @FineArtsStatic_Claim @Recovery  @Education
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|payableamount|outstandingamount|recoverableamount|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim edu| 5000         |300              |200              |
    @FineArtsStatic_Claim @Recovery @Environment_and_Water
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|payableamount|outstandingamount|recoverableamount|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim E&W | 5000         |300              |200              |
    @FineArtsStatic_Claim @Recovery @Government_and_Economy
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|payableamount|outstandingamount|recoverableamount|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim G&E | 5000         |300              |200              |
    @FineArtsStatic_Claim @Recovery @Law_and_Justice
    Examples:
    | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|payableamount|outstandingamount|recoverableamount|
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim L&J| 5000         |300              |200              |



    Scenario Outline: Initiate a Claim for FineArts and Indeminity Decision granted  and add litigation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Select coverage "Static" for FineArts claim
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Add Incident details "Theft" "Test" "Address"
    And Click Continue button
    And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
    And Select Incident report details in claim "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    And Validated the medical claim indemnity case status "PENDING-INVESTIGATION"
    And User should Add litigation
    And Enter the Solicitor details "Defendant" "PavanAutomation" "14/May/2021" "PlaintiffSolicitr" "Test"
    And Enter the Litigatation Status "Coronial" "Judgement" "County" "12345"  "14/May/2021" "14/May/2021" "Mani_qa"
    And Enter the CoDefendent details "Ronaldo" "Gayle" "Rabada"
    And Enter the Timetable details "Inquest" "25/Jun/2021" "Sudheer"
    And Enter the Event details "Yes" "15/Jun/2021" "Hi event note"
    And Verify the Timetable history "Yes" "15/02/0021" "25/06/2021" "Inquest" "Claims Officer 1 R3"
    And Enter the Settlement details "Settlement" "14/May/2021" "comment" "Defendant pays" "No"
    And Select settlement monetary "No" "500000" "500001" "comment"
    And complete the litigiation for "ClaimsOfficer"
    Then I validate the "LITIGATION-SETTLED" for litigation
    Then I logout of the application

    @FineArtsStatic_Claim @ResolvedDuplicate @ID_Granted @Litigation  @Education
    Examples:
    | loss                    | AccountName |  BSB   | AccountNumber | BankName          | EmailID     |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName| Decision | GrantedType |usertotransfer |
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim edu|  Granted  | Full       |Claims Officer 1 R3 |
    @FineArtsStatic_Claim @ResolvedDuplicate @ID_Granted @Litigation @Environment_and_Water
    Examples:
    | loss                    | AccountName |  BSB   | AccountNumber | BankName          | EmailID     |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName| Decision | GrantedType |usertotransfer |
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim E&W |  Granted  | Full       |Claims Officer 1 R3 |
    @FineArtsStatic_Claim @ResolvedDuplicate @ID_Granted @Litigation @Government_and_Economy
    Examples:
    | loss                    | AccountName |  BSB   | AccountNumber | BankName          | EmailID     |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName| Decision | GrantedType |usertotransfer |
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim G&E |  Granted  | Full       |Claims Officer 1 R3 |
    @FineArtsStatic_Claim @ResolvedDuplicate @ID_Granted @Litigation @Law_and_Justice
    Examples:
    | loss                    | AccountName |  BSB   | AccountNumber | BankName          | EmailID     |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName| Decision | GrantedType |usertotransfer |
    | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim L&J|  Granted  | Full       |Claims Officer 1 R3 |

    Scenario Outline: Initiate a Claim for FineArts and Indeminity Decision Denied approved
        Given I have the external URL of VMIA
        When User enter valid username  "<username>" and password "<password>" click on Login button
        And I Select the Organization "<OrgName>"
        Then I am logged in to the insurance portal
        Then I want to make a claim
        And I enter the date and time of loss
        And I select the type of "<loss>"
        And Select coverage "Static" for FineArts claim
        And Click Continue button
        When I select the business row and verify listed policies for "<loss>"
        And Click Continue button
        And Click Continue button
        And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
        And Click Continue button
        And Add Incident details "Theft" "Test" "Address"
        And Click Continue button
        And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
        And Select Incident report details in claim "No" "No"
        And Click Continue button
        And Click Continue button
        And I check the declaration for Claims
        Then Click Finish button
        Then I validate the "<finalstatus>" for claims
        And I extract the claim unit number
        Then I logout of the application
        Given I have the URL of VMIA
        Given User when logs in as "TriageOfficer"
        Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
        Then I resolve claim as duplicate
        Then Now verify "RESOLVED-DUPLICATE"
        Then Now I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "PortfolioManager"
        Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
        And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "ClaimsOfficer"
        Then I open claim unit from worklist
        And Select the indeminity decision "<Decision>""<GrantedType>"
        Then I validate the "PENDING - INDEMNITY DECISION" for claims
        When User when logs in as "PortfolioManager"
        And Open the claim from "Claims Portfolio Manager 1 R3" queue
        And Select the indeminity denial action "<DenailDecision>""<DenailType>"
        Then I validate the "PENDING - INDEMNITY DECISION" for claims
        Then I logout of the application
        @FineArtsStatic_Claim @ResolvedDuplicate @ID_Denied_Approve  @Education
        Examples:
            | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName| Decision | GrantedType | DenailDecision  |usertotransfer |
            | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim edu| Denied   | Full        | Approve         |Claims Officer 1 R3 |
        @FineArtsStatic_Claim @ResolvedDuplicate @ID_Denied_Approve @Environment_and_Water
        Examples:
            | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName| Decision | GrantedType | DenailDecision  |usertotransfer |
            | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim E&W | Denied   | Full        | Approve         |Claims Officer 1 R3 |
        @FineArtsStatic_Claim @ResolvedDuplicate @ID_Denied_Approve @Government_and_Economy
        Examples:
            | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName| Decision | GrantedType | DenailDecision  |usertotransfer |
            | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim G&E | Denied   | Full        | Approve         |Claims Officer 1 R3 |
        @FineArtsStatic_Claim @ResolvedDuplicate @ID_Denied_Approve @Law_and_Justice
        Examples:
            | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName| Decision | GrantedType | DenailDecision  |usertotransfer |
            | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim L&J| Denied   | Full        | Approve         |Claims Officer 1 R3 |

    Scenario Outline: Initiate a Claim for FineArts and Indeminity Decision Denied and Denied type full
        Given I have the external URL of VMIA
        When User enter valid username  "<username>" and password "<password>" click on Login button
        And I Select the Organization "<OrgName>"
        Then I am logged in to the insurance portal
        Then I want to make a claim
        And I enter the date and time of loss
        And I select the type of "<loss>"
        And Select coverage "Static" for FineArts claim
        And Click Continue button
        When I select the business row and verify listed policies for "<loss>"
        And Click Continue button
        And Click Continue button
        And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
        And Click Continue button
        And Add Incident details "Theft" "Test" "Address"
        And Click Continue button
        And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
        And Select Incident report details in claim "No" "No"
        And Click Continue button
        And Click Continue button
        And I check the declaration for Claims
        Then Click Finish button
        Then I validate the "<finalstatus>" for claims
        And I extract the claim unit number
        Then I logout of the application
        Given I have the URL of VMIA
        Given User when logs in as "TriageOfficer"
        Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
        Then I resolve claim as duplicate
        Then Now verify "RESOLVED-DUPLICATE"
        Then Now I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "PortfolioManager"
        Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
        And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "ClaimsOfficer"
        Then I open claim unit from worklist
        And Select the indeminity decision "<Decision>""<GrantedType>"
        Then I validate the "PENDING - INDEMNITY DECISION" for claims
        When User when logs in as "PortfolioManager"
        And Open the claim from "Claims Portfolio Manager 1 R3" queue
        And Select the indeminity denial action "<DenailDecision>""<DenailType>"
        Then I validate the "PENDING - INDEMNITY DECISION" for claims
        Then I logout of the application
        @FineArtsStatic_Claim @ResolvedDuplicate  @ID_FullDenial  @Education
        Examples:
            | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName| Decision | GrantedType | DenailDecision                | DenailType |usertotransfer |
            | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim edu| Denied   | Full        | Request more information      |  Full      |Claims Officer 1 R3 |
        @FineArtsStatic_Claim @ResolvedDuplicate  @ID_FullDenial @Environment_and_Water
        Examples:
            | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName| Decision | GrantedType | DenailDecision                | DenailType |usertotransfer |
            | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim E&W | Denied   | Full        | Request more information      |  Full      |Claims Officer 1 R3 |
        @FineArtsStatic_Claim @ResolvedDuplicate  @ID_FullDenial @Government_and_Economy
        Examples:
            | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName| Decision | GrantedType | DenailDecision                | DenailType |usertotransfer |
            | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim G&E | Denied   | Full        | Request more information      |  Full      |Claims Officer 1 R3 |
        @FineArtsStatic_Claim @ResolvedDuplicate  @ID_FullDenial @Law_and_Justice
        Examples:
            | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName| Decision | GrantedType | DenailDecision                | DenailType |usertotransfer |
            | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim L&J| Denied   | Full        | Request more information      |  Full      |Claims Officer 1 R3 |

    Scenario Outline: Initiate a Claim for FineArts and Indeminity Decision Denied and Denied type partial
        Given I have the external URL of VMIA
        When User enter valid username  "<username>" and password "<password>" click on Login button
        And I Select the Organization "<OrgName>"
        Then I am logged in to the insurance portal
        Then I want to make a claim
        And I enter the date and time of loss
        And I select the type of "<loss>"
        And Select coverage "Static" for FineArts claim
        And Click Continue button
        When I select the business row and verify listed policies for "<loss>"
        And Click Continue button
        And Click Continue button
        And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
        And Click Continue button
        And Add Incident details "Theft" "Test" "Address"
        And Click Continue button
        And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
        And Select Incident report details in claim "No" "No"
        And Click Continue button
        And Click Continue button
        And I check the declaration for Claims
        Then Click Finish button
        Then I validate the "<finalstatus>" for claims
        And I extract the claim unit number
        Then I logout of the application
        Given I have the URL of VMIA
        Given User when logs in as "TriageOfficer"
        Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
        Then I resolve claim as duplicate
        Then Now verify "RESOLVED-DUPLICATE"
        Then Now I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "PortfolioManager"
        Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
        And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "ClaimsOfficer"
        Then I open claim unit from worklist
        And Select the indeminity decision "<Decision>""<GrantedType>"
        Then I validate the "PENDING - INDEMNITY DECISION" for claims
        When User when logs in as "PortfolioManager"
        And Open the claim from "Claims Portfolio Manager 1 R3" queue
        And Select the indeminity denial action "<DenailDecision>""<DenailType>"
        Then I validate the "PENDING - INDEMNITY DECISION" for claims
        Then I logout of the application
        @FineArtsStatic_Claim @ResolvedDuplicate  @ID_PartialDenial @Education
        Examples:
            | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|   Decision | GrantedType | DenailDecision                | DenailType |usertotransfer |
            | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim edu| Denied   | Partial     | Decline denial                |Partial     |Claims Officer 1 R3 |
        @FineArtsStatic_Claim @ResolvedDuplicate  @ID_PartialDenial @Environment_and_Water
        Examples:
            | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|   Decision | GrantedType | DenailDecision                | DenailType |usertotransfer |
            | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim E&W | Denied   | Partial     | Decline denial                |Partial     |Claims Officer 1 R3 |
        @FineArtsStatic_Claim @ResolvedDuplicate  @ID_PartialDenial @Government_and_Economy
        Examples:
            | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|   Decision | GrantedType | DenailDecision                | DenailType |usertotransfer |
            | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim G&E | Denied   | Partial     | Decline denial                |Partial     |Claims Officer 1 R3 |
        @FineArtsStatic_Claim @ResolvedDuplicate  @ID_PartialDenial @Law_and_Justice
        Examples:
            | loss                    | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName|   Decision | GrantedType | DenailDecision                | DenailType |usertotransfer |
            | Fine Arts Exhibitions   | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto claim L&J| Denied   | Partial     | Decline denial                |Partial     |Claims Officer 1 R3 |
