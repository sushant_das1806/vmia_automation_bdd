Feature: Directors & Officers Liability emRepss

  Scenario Outline: Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application

    @DAOClaim @ResolvedDuplicate @LawAndJustice @MLP4Sprint5
    Examples:
      |OrgName        | loss                          | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus |
      |Auto claim G&E | Directors & Officers - EmRePSS| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| WorkPressure            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |

  Scenario Outline: Initiate a Claim for Construction and resolve claim as cancel in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then I search the claim file and cancel the claim from Actions Drop down
    And verify the Claims Files Status "RESOLVED-CANCELLED" after Cancel from Actions drop down
    Then I logout of the application

    @DAOClaim @ResolvedDuplicate @LawAndJustice @MLP4Sprint5 @RegresionClaimMLP4
    Examples:
      |OrgName        | loss                          | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus |
      |Auto claim G&E | Directors & Officers - EmRePSS| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| WorkPressure            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |

  Scenario Outline: Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100"
    Then create Recovery estimate  "Client excess" "70"
    And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "20" "09/08/2021"
    And I verify recovery status "PAYMENT-RECOVERED"
    Then I logout of the application

    @DAOClaim @ResolvedDuplicate @LawAndJustice @MLP4Sprint5 @RegresionClaimMLP4
    Examples:
      |OrgName        | loss                          | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus |payableamount|outstandingamount|recoverableamount|
      |Auto claim G&E | Directors & Officers - EmRePSS| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| WorkPressure            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |5000         |300              |200              |

  Scenario Outline: Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @DAOClaim @ResolvedDuplicate @LawAndJustice @MLP4Sprint5 @RegresionClaimMLP4
    Examples:
      |OrgName        | loss                          | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus |payableamount|outstandingamount|recoverableamount|
      |Auto claim G&E | Directors & Officers - EmRePSS| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| WorkPressure            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |5000         |300              |200              |








  @Claim @Regression
  Scenario Outline: Initiate a Claim for Director and Officer Liability and resolve claim as duplicate in triage claim officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application

    @DAOClaim @ResolvedDuplicate @LawAndJustice @MLP4Sprint5
    Examples:
      |OrgName        | loss                          | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus |
      |Auto claim G&E | Directors & Officers - EmRePSS| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| WorkPressure            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
#    @DAOClaim @ResolvedDuplicate @GovernmentEconomy @MLP4Sprint5
#    Examples:
#      |OrgName        | loss                          | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus |
#      |Auto Gov & Eco | Directors & Officers - EmRePSS| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| Salary            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
#    @DAOClaim @ResolvedDuplicate @CSOEProgram @MLP4Sprint5
#    Examples:
#      |OrgName    | loss                          | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus |
#      |Csoes-1347 | Directors & Officers Liability CSOE| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| Salary            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |


  Scenario Outline: Initiate a Claim for Director and Officer Liability Edit the claim data
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and edit the claim "<triagecasestatus>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "2" from claims pages
    Then I am routed to Client Banking Account details page
    And Click on Continue button for "<NumTime2>" from claims pages
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @DAOClaim @ResolvedDuplicate @EditClaim
    Examples:
      |OrgName        | loss                          | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus |
      |Csoes-1347 | Directors & Officers Liability CSOE| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| Salary            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |


  Scenario Outline: Initiate a Claim for Director and Officer Liability and cancel the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and cancel the claim from Actions Drop down
    And verify the Claims Files Status "RESOLVED-CANCELLED" after Cancel from Actions drop down
    Then I logout of the application

    @DAOClaim @ResolvedDuplicate @cancel
    Examples:
      |OrgName    | loss                               | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus |
      |Csoes-1347 | Directors & Officers Liability CSOE| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| Salary            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |


  Scenario Outline: Initiate a Claim for Director and Officer Liability  and make final payment and close the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @DAOClaim @ResolvedDuplicate @finalPayment
    Examples:
      |OrgName    | loss                               | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus |payableamount|outstandingamount|recoverableamount|
      |Csoes-1347 | Directors & Officers Liability CSOE| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| Salary            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | 5000         |300              |200              |


  Scenario Outline: Initiate a Claim for Director and Officer Liability and create recovery and aprrove recovery in finance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100"
    Then create Recovery estimate  "Client excess" "70"
    And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "20" "09/08/2021"
    And I verify recovery status "PAYMENT-RECOVERED"
    Then I logout of the application

    @DAOClaim @ResolvedDuplicate @CSOE @recovery
    Examples:
      |OrgName        | loss                          | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus |payableamount |outstandingamount|recoverableamount|
      |Csoes-1347 | Directors & Officers Liability CSOE| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| Salary            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | 5000         |300              |200              |


  Scenario Outline: Initiate a Claim for Director and Officer Liability and Indeminity Decision granted   and add litigation                                             @ID_Granted
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    And Validated the medical claim indemnity case status "PENDING-INVESTIGATION"
    And User should Add litigation
    And Enter the Solicitor details "Defendant" "PavanAutomation" "14/May/2021" "PlaintiffSolicitr" "Test"
    And Enter the Litigatation Status "Coronial" "Judgement" "County" "12345"  "14/May/2021" "14/May/2021" "Mani_qa"
    And Enter the CoDefendent details "Ronaldo" "Gayle" "Rabada"
    And Enter the Timetable details "Inquest" "25/Jun/2021" "Sudheer"
    And Enter the Event details "Yes" "15/Jun/2021" "Hi event note"
    And Verify the Timetable history "Yes" "15/02/0021" "25/06/2021" "Inquest" "Claims Officer 1 R3"
    And Enter the Settlement details "Settlement" "14/May/2021" "comment" "Defendant pays" "No"
    And Select settlement monetary "No" "500000" "500001" "comment"
    And complete the litigiation for "ClaimsOfficer"
    Then I validate the "LITIGATION-SETTLED" for litigation
    Then I logout of the application

    @DAOClaim @ResolvedDuplicate    @ID_Granted @Litigation @CSOE
    Examples:
      |OrgName        | loss                          | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus |  Decision | GrantedType |
      |Csoes-1347 | Directors & Officers Liability CSOE| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| Salary            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | Granted  | Full    |


  Scenario Outline: Initiate a Claim for Director and Officer Liability and Indeminity Decision Denied approved                                      @ID_Denied_Approve
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @DAOClaim @ResolvedDuplicate  @ID_Denied_Approve @CSOE
    Examples:
      |OrgName        | loss                          | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus | Decision | GrantedType | DenailDecision  |
      |Csoes-1347 | Directors & Officers Liability CSOE| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| Salary            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | Denied   | Full        | Approve         |


  Scenario Outline: Initiate a Claim for Director and Officer Liability and Indeminity Decision Denied and Denied type full                          @ID_FullDenial
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @DAOClaim @ResolvedDuplicate   @ID_FullDenial @CSOE
    Examples:
      |OrgName        | loss                          | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus | Decision | GrantedType | DenailDecision                | DenailType |
      |Csoes-1347 | Directors & Officers Liability CSOE| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| Salary            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |Denied   | Full        | Request more information      |  Full      |


  Scenario Outline: Initiate a Claim for Director and Officer Liability and Indeminity Decision Denied and Denied type partial                       @ID_PartialDenial
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the employee details "<firstname>" "<LastName>" "<emloyeePosition>" "<employeeDuty>"
    And Enter the salary details "<Basesalary>" "<CTC>" "<ContractEmployment>" "No" "NO"
    And Click Continue button
    And Enter Employee terminiation details "No" "No" "<joinDate>" "23"
    And Enter the legal detail of terminiation "No" "No" "<natureOfComplaint>" "No" "terminationReason" "challangeReason" "6000" "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @DAOClaim @ResolvedDuplicate @ID_PartialDenial @CSOE
    Examples:
      |OrgName        | loss                               | firstname |LastName|emloyeePosition| employeeDuty | Basesalary | CTC  | ContractEmployment| joinDate  | natureOfComplaint |  finalstatus        | finalstatustriage        | triagecasestatus | Decision | GrantedType | DenailDecision                | DenailType |
      |Csoes-1347     | Directors & Officers Liability CSOE| John      |Cambell | Tester        | Testing      | 5000       | 15000| No               | 12/07/2017| Salary            |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | Denied   | Partial     | Decline denial                |Partial     |
