Feature: Initiate a Claim for EmRepSS-Motor Products

  Scenario Outline: Initiate a Claim for EmRepSS-Motor and resolve claim as  duplicate in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<name>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
#    And Add Incident details "Theft" "Test" "Address"
    And Add Inncident Details for EmRePss-Motor "16/12/2021" "Test Reason" "Test Name"
#    And Click Continue button
#    And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
#    And Select Incident report details in claim "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application

    Examples:
      | loss                                  | AccountName |  BSB    | AccountNumber | BankName          | EmailID    |  finalstatus        | finalstatustriage        | triagecasestatus |name|
      | Motor - EmRePSS | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | RESOLVED-DUPLICATE| PENDING-TRIAGE   |Auto Gov & Eco|