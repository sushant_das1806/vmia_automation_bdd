Feature: Initiate a Claim for Combined Liability

  Scenario Outline: Initiate a Claim for Combined Liability  Edit the claim data
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And I select the lodging as "Claim"
    And I select the coverage as "Public and products liability"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" <addline1> "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and edit the claim "<triagecasestatus>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "2" from claims pages
    Then I am routed to Client Banking Account details page
    And Click on Continue button for "<NumTime2>" from claims pages
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    @CombinedLiability @Property_and_Planning  @EditClaim
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim P&P |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
  #    @CombinedLiability @Business_and_Industry  @EditClaim
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto Claim B&I |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
#
#    @CombinedLiability @Environment_and_Water  @EditClaim
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
#
#    @CombinedLiability @Education  @EditClaim
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim edu |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
#
#    @CombinedLiability @Government_and_Economy  @EditClaim
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
#
#    @CombinedLiability @Law_and_Justice  @EditClaim
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |


  Scenario Outline: Initiate a Claim for Combined Liability and cancel the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And I select the lodging as "Claim"
    And I select the coverage as "Public and products liability"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" <addline1> "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and cancel the claim from Actions Drop down
    And verify the Claims Files Status "RESOLVED-CANCELLED" after Cancel from Actions drop down
    Then I logout of the application

    @CombinedLiability @Property_and_Planning @CancelClaim
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim P&P |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
  #    @CombinedLiability @Business_and_Industry @CancelClaim
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto Claim B&I |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
#
#    @CombinedLiability @Environment_and_Water @CancelClaim
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
#
#    @CombinedLiability @Education @CancelClaim
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim edu |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
#
#    @CombinedLiability @Government_and_Economy @CancelClaim
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
#
#    @CombinedLiability @Law_and_Justice @CancelClaim
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |



  Scenario Outline: Initiate a Claim for Combined Liability and make final payment and close the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And I select the lodging as "Claim"
    And I select the coverage as "Public and products liability"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" <addline1> "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @CombinedLiability @Property_and_Planning @Finalpayment
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |payableamount|outstandingamount|recoverableamount|
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim P&P |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   | 5000         |300              |200             |
  #    @CombinedLiability @Business_and_Industry @Finalpayment
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |payableamount|outstandingamount|recoverableamount|
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto Claim B&I |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   | 5000         |300              |200              |
#
#    @CombinedLiability @Environment_and_Water @Finalpayment
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |payableamount|outstandingamount|recoverableamount|
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   | 5000         |300              |200              |
#
#    @CombinedLiability @Education @Finalpayment
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |payableamount|outstandingamount|recoverableamount|
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim edu |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   | 5000         |300              |200              |
#
#    @CombinedLiability @Government_and_Economy @Finalpayment
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |payableamount|outstandingamount|recoverableamount|
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   | 5000         |300              |200              |
#
#    @CombinedLiability @Law_and_Justice @Finalpayment
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |payableamount|outstandingamount|recoverableamount|
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   | 5000         |300              |200              |




  Scenario Outline: Initiate a Claim for Combined Liability and  create recovery and aprrove recovery in finance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And I select the lodging as "Claim"
    And I select the coverage as "Public and products liability"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" <addline1> "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100"
    Then create Recovery estimate  "Client excess" "70"
    And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "20" "09/08/2021"
    And I verify recovery status "PAYMENT-RECOVERED"
    Then I logout of the application

    @CombinedLiability @Property_and_Planning @Recovery
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |payableamount|outstandingamount|recoverableamount|
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim P&P |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   | 5000         |300              |200             |
  #    @CombinedLiability @Business_and_Industry @Recovery
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |payableamount|outstandingamount|recoverableamount|
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto Claim B&I |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   | 5000         |300              |200              |
#
#    @CombinedLiability @Environment_and_Water @Recovery
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |payableamount|outstandingamount|recoverableamount|
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   | 5000         |300              |200              |
#
#    @CombinedLiability @Education @Recovery
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |payableamount|outstandingamount|recoverableamount|
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim edu |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   | 5000         |300              |200              |
#
#    @CombinedLiability @Government_and_Economy @Recovery
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |payableamount|outstandingamount|recoverableamount|
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   | 5000         |300              |200              |
#
#    @CombinedLiability @Law_and_Justice @Recovery
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |payableamount|outstandingamount|recoverableamount|
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   | 5000         |300              |200              |


  Scenario Outline: Initiate a Claim for Combined Liability  And Indeminity Decision granted and add litigation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And I select the lodging as "Claim"
    And I select the coverage as "Public and products liability"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" <addline1> "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    And Validated the medical claim indemnity case status "PENDING-INVESTIGATION"
    And User should Add litigation
    And Enter the Solicitor details "Defendant" "PavanAutomation" "14/May/2021" "PlaintiffSolicitr" "Test"
    And Enter the Litigatation Status "Coronial" "Judgement" "County" "12345"  "14/May/2021" "14/May/2021" "Mani_qa"
    And Enter the CoDefendent details "Ronaldo" "Gayle" "Rabada"
    And Enter the Timetable details "Inquest" "25/Jun/2021" "Sudheer"
    And Enter the Event details "Yes" "15/Jun/2021" "Hi event note"
    And Verify the Timetable history "Yes" "15/02/0021" "25/06/2021" "Inquest" "Claims Officer 1 R3"
    And Enter the Settlement details "Settlement" "14/May/2021" "comment" "Defendant pays" "No"
    And Select settlement monetary "No" "500000" "500001" "comment"
    And complete the litigiation for "ClaimsOfficer"
    Then I validate the "LITIGATION-SETTLED" for litigation
    Then I logout of the application

    @CombinedLiability @Property_and_Planning @Litigation
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim P&P |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full        |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |

#    @CombinedLiability @Business_and_Industry @Litigation
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto Claim B&I |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
#
#    @CombinedLiability @Environment_and_Water @Litigation
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
#
#    @CombinedLiability @Education @Litigation
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim edu |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
#
#    @CombinedLiability @Government_and_Economy @Litigation
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |
#
#    @CombinedLiability @Law_and_Justice @Litigation
#    Examples:
#      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus | Decision | GrantedType |finalstatustriage                   | triagecasestatus |
#      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            | Granted  | Full    |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE   |




  Scenario Outline: Initiate a Claim for Combined Liability and Indeminity Decision Denied approved

    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And I select the lodging as "Claim"
    And I select the coverage as "Public and products liability"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" <addline1> "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application

    @CombinedLiability @Property_and_Planning @ID_Denied_Approve
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision  |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim P&P |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied   | Full        | Approve         |
    @CombinedLiability @Education @ID_Denied_Approve
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision  |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |Auto claim edu |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied   | Full        | Approve         |
    @CombinedLiability @Environment_and_Water @ID_Denied_Approve
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision  |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |Auto claim E&W |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied   | Full        | Approve         |
    @CombinedLiability @Government_and_Economy @ID_Denied_Approve
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision  |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied   | Full        | Approve         |
    @CombinedLiability @Law_and_Justice @ID_Denied_Approve
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision  |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |Auto claim L&J |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied   | Full        | Approve         |
    @CombinedLiability @Business_and_Industry @ID_Denied_Approve
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision  |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |Auto Claim B&I |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied   | Full        | Approve         |

  Scenario Outline: Initiate a Claim for Combined Liability and Indeminity Decision Denied and Denied type full

    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And I select the lodging as "Claim"
    And I select the coverage as "Public and products liability"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" <addline1> "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application

    @CombinedLiability @Property_and_Planning  @ID_Denied_Full
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision                | DenailType |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim P&P |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied  | Full        | Request more information      |  Full      |
    @CombinedLiability @Environment_and_Water  @ID_Denied_Full
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision                | DenailType |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |Auto claim E&W  |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied  | Full        | Request more information      |  Full      |
    @CombinedLiability @Education  @ID_Denied_Full
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision                | DenailType |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |Auto claim edu |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied  | Full        | Request more information      |  Full      |
    @CombinedLiability @Government_and_Economy  @ID_Denied_Full
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision                | DenailType |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied  | Full        | Request more information      |  Full      |
    @CombinedLiability @Law_and_Justice  @ID_Denied_Full
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision                | DenailType |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J	 |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied  | Full        | Request more information      |  Full      |
    @CombinedLiability @Business_and_Industry  @ID_Denied_Full
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision                | DenailType |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto Claim B&I |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied  | Full        | Request more information      |  Full      |

  Scenario Outline: Initiate a Claim for Combined Liability and Indeminity Decision Denied and Denied type partial

    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And I select the lodging as "Claim"
    And I select the coverage as "Public and products liability"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" <addline1> "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Given User enters the Internal URL from Browser
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application

    @CombinedLiability @Property_and_Planning  @ID_Denied_Partial
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision                | DenailType |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim P&P |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied   | Partial     | Decline denial                |Partial     |
    @CombinedLiability @Education  @ID_Denied_Partial
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision                | DenailType |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim edu |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied   | Partial     | Decline denial                |Partial     |
    @CombinedLiability @Environment_and_Water  @ID_Denied_Partial
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision                | DenailType |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim E&W |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied   | Partial     | Decline denial                |Partial     |
    @CombinedLiability @Government_and_Economy  @ID_Denied_Partial
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision                | DenailType |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim G&E |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied   | Partial     | Decline denial                |Partial     |
    @CombinedLiability @Law_and_Justice  @ID_Denied_Partial
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision                | DenailType |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto claim L&J |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied   | Partial     | Decline denial                |Partial     |
    @CombinedLiability @Business_and_Industry  @ID_Denied_Partial
    Examples:
      | username                          | password     | loss                         | what    | claimantname | addline1    | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                                      | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | name           | usertotransfer     | statusaftertransfer   | beforetransferclaimstatus |finalstatustriage                   | triagecasestatus  | Decision | GrantedType | DenailDecision                | DenailType |
      | manoj.ramanathan@areteanstech.com | Test@1234567 | Combined Liability           | Highway | liabClaimant | "Sydney "   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Users\\pega\\Documents\\TRIMUserSetup.txt | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Auto Claim B&I |Claims Officer 1 R3 | PENDING-INVESTIGATION | PENDING-TRIAGE            |PENDING-CLAIM COMPLETION            | PENDING-TRIAGE    | Denied   | Partial     | Decline denial                |Partial     |
