Feature: Professional Indeminity feature

  @MPL4 @Indeminity @US-8003
  Scenario Outline: Initiate a Claim for Professional Indeminity  and resolve claim as duplicate in triage claim officer
  ### Updated the Scenario outline name from the below name
  ### Creating Professional Ideminity claim
  Given I have the external URL of VMIA
  When User enter valid username  "<username>" and password "<password>" click on Login button
  And I Select the Organization "<OrgName>"
  Then I am logged in to the insurance portal
  Then I want to make a claim
  And I enter the date and time of loss
  And I select the type of "<loss>"
  And Click Continue button
  And Select Private Claims as "No"
  And Click Continue button
  When I select the business row and verify listed policies for "<loss>"
  And Click Continue button
  And Click Continue button
  And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
  And Click Continue button
  And Click Continue button
  And I check the declaration for Claims
  Then Click Finish button
  Then I validate the "<finalstatus>" for claims
  And I extract the claim unit number
  Then I logout of the application
  Given I have the URL of VMIA
  Given User when logs in as "TriageOfficer"
  ### Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
  ### Replaced the above step with the below and made flow modifications here
  Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
  Then I resolve claim as duplicate
  Then Now verify "<finalPenstatus>"
  Then Now I logout of the application

    @ProfessionalIndemnity @ResolvedDuplicate @BusinessAndIndustries @RegresionClaimMLP5 @RegressionMLP5ClaimsScenarios
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalPenstatus     | triagecasestatus |
      |  Professional Indemnity |  RESOLVED-COMPLETED |  12/03/2021      |Auto Claim B&I| RESOLVED-DUPLICATE | PENDING-TRIAGE   |
    @ProfessionalIndemnity @ResolvedDuplicate @EnvioronmentAndWater
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim E&W|
    @ProfessionalIndemnity @ResolvedDuplicate @GovernmentAndEconomy
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim G&E|
    @ProfessionalIndemnity @ResolvedDuplicate @CSOEProgram
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_CSOEProgram|
    @ProfessionalIndemnity @ResolvedDuplicate @LawAndJustice
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim L&J|
    @ProfessionalIndemnity @ResolvedDuplicate @Publichospital
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_publichospital|
    @ProfessionalIndemnity @ResolvedDuplicate @TransportAndVechicle
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_TransportAndVechicle|

  @MPL4 @Indeminity @US-8003
  Scenario Outline: Creating Professional Ideminity claim and cancel the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-CANCELLED"
    Then Now I logout of the application
    @ProfessionalIndemnity @ResolvedDuplicate  @BusinessandIndustry @RegresionClaimMLP5
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto Claim B&I|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|
    @ProfessionalIndemnity @ResolvedDuplicate @EnvironmentandWater
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim E&W|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|
    @ProfessionalIndemnity @ResolvedDuplicate @GovernmentandEconomy
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim G&E|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|
    @ProfessionalIndemnity @ResolvedDuplicate @CSOEProgram
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_CSOEProgram|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|
    @ProfessionalIndemnity @ResolvedDuplicate @LawAndJustice
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim L&J|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|
    @ProfessionalIndemnity @ResolvedDuplicate @Publichospital
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_publichospital|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|
    @ProfessionalIndemnity @ResolvedDuplicate @TransportAndVechicle
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_TransportAndVechicle|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|


  @MPL4 @Indeminity @US-8003
  Scenario Outline: Creating Professional Ideminity claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    @Professional_Indemnity  @Business_and_Industry
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto Claim B&I| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity @Environment_and_Water
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim E&W| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity @Government_and_Economy
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim G&E| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @ProfessionalIndemnity @ResolvedDuplicate @CSOEProgram
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_CSOEProgram|
    @ProfessionalIndemnity @ResolvedDuplicate @LawAndJustice
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim L&J|
    @ProfessionalIndemnity @ResolvedDuplicate @Publichospital
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_publichospital|
    @ProfessionalIndemnity @ResolvedDuplicate @TransportAndVechicle
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_TransportAndVechicle|

  Scenario Outline: Creating Professional Ideminity claim and edit the claim data
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and edit the claim "<triagecasestatus>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click on Continue button for "2" from claims pages
    Then I am routed to Client Banking Account details page
    And Click on Continue button for "<NumTime2>" from claims pages
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    @Professional_Indemnity    @EditClaim  @Business_and_Industry
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto Claim B&I| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity   @EditClaim @Environment_and_Water
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim E&W| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity   @EditClaim @Government_and_Economy
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim G&E| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |

  Scenario Outline: Creating Professional Ideminity claim and cancel the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and cancel the claim from Actions Drop down
    And verify the Claims Files Status "RESOLVED-CANCELLED" after Cancel from Actions drop down
    Then I logout of the application
    @Professional_Indemnity  @CancelClaim  @Business_and_Industry
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto Claim B&I| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity  @CancelClaim @Environment_and_Water
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim E&W| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity  @CancelClaim @Government_and_Economy
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim G&E| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |


  Scenario Outline: Creating Professional Ideminity claim and make final payment and close the claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    ### added the below step here to launch the interaction portal
    Given I have the URL of VMIA
    When User when logs in as "TriageOfficer"
    ### Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    ### Replaced the above step with the below and made flow modifications here
    Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    ### Have added new method in the above step def and updated multiple locators
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    ### After update there, where no locator find to proceed with pay info so added the locator in below step
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    ### Then I open claim unit from worklist
    ### Above step is replaced with below one
    Then I open the claim unit from Claims portal
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    @Professional_Indemnity    @Finalpayment  @Business_and_Industry @RegresionClaimMLP5 @RegressionMLP5ClaimsScenarios
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate| OrgName        | finalstatustriage        | triagecasestatus |payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity |  RESOLVED-COMPLETED |  12/03/2021      | Auto Claim B&I | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | 5000         |300              |200              |
    @Professional_Indemnity    @Finalpayment @Environment_and_Water
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim E&W| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | 5000         |300              |200              |
    @Professional_Indemnity    @Finalpayment @Government_and_Economy
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim G&E| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | 5000         |300              |200              |
    @ProfessionalIndemnity @Finalpayment @CSOEProgram
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus | payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_CSOEProgram|PENDING-CLAIM COMPLETION  | PENDING-TRIAGE|5000         |300              |200              |
    @ProfessionalIndemnity @Finalpayment @LawAndJustice
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim L&J|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|5000         |300              |200              |
    @ProfessionalIndemnity @Finalpayment @Publichospital
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_publichospital|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|5000         |300              |200              |
    @ProfessionalIndemnity @Finalpayment @TransportAndVechicle
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName                    | finalstatustriage            | triagecasestatus |payableamount |outstandingamount|recoverableamount|
    |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_TransportAndVechicle|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE      |5000         |300              |200    |


  Scenario Outline: Creating Professional Ideminity claim and  create recovery and aprrove recovery in finance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100"
    Then create Recovery estimate  "Client excess" "70"
    And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "20" "09/08/2021"
    And I verify recovery status "PAYMENT-RECOVERED"
    Then I logout of the application
    @Professional_Indemnity	@Recovery @Business_and_Industry @RegresionClaimMLP5
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto Claim B&I| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | 5000         |300              |200              |
    @Professional_Indemnity	@Recovery @Environment_and_Water
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim E&W| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | 5000         |300              |200              |
    @Professional_Indemnity	@Recovery @Government_and_Economy
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim G&E| PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | 5000         |300              |200              |
    @ProfessionalIndemnity @Recovery @CSOEProgram
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus | payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_CSOEProgram|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|5000         |300              |200              |
    @ProfessionalIndemnity @Recovery @CSOEProgram
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus | payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_CSOEProgram|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|5000         |300              |200              |
    @ProfessionalIndemnity @Finalpayment @LawAndJustice
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim L&J|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|5000         |300              |200              |
    @ProfessionalIndemnity @Finalpayment @Publichospital
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_publichospital|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|5000         |300              |200              |
    @ProfessionalIndemnity @Finalpayment @TransportAndVechicle
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | finalstatustriage        | triagecasestatus |payableamount |outstandingamount|recoverableamount|
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_TransportAndVechicle|PENDING-CLAIM COMPLETION       | PENDING-TRIAGE|5000         |300              |200              |



  Scenario Outline: Creating Professional Ideminity claim  and Indeminity Decision granted  and add litigation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    And Validated the medical claim indemnity case status "PENDING-INVESTIGATION"
    And User should Add litigation
    And Enter the Solicitor details "Defendant" "PavanAutomation" "14/May/2021" "PlaintiffSolicitr" "Test"
    And Enter the Litigatation Status "Coronial" "Judgement" "County" "12345"  "14/May/2021" "14/May/2021" "Mani_qa"
    And Enter the CoDefendent details "Ronaldo" "Gayle" "Rabada"
    And Enter the Timetable details "Inquest" "25/Jun/2021" "Sudheer"
    And Enter the Event details "Yes" "15/Jun/2021" "Hi event note"
    And Verify the Timetable history "Yes" "15/02/0021" "25/06/2021" "Inquest" "Claims Officer 1 R3"
    And Enter the Settlement details "Settlement" "14/May/2021" "comment" "Defendant pays" "No"
    And Select settlement monetary "No" "500000" "500001" "comment"
    And complete the litigiation for "ClaimsOfficer"
    Then I validate the "LITIGATION-SETTLED" for litigation
    Then I logout of the application
    @Professional_Indemnity @Litigation @ID_Granted @Business_and_Industry
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |  Decision | GrantedType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto Claim B&I|  Granted  | Full        | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity @Litigation @ID_Granted @Environment_and_Water
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |  Decision | GrantedType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim E&W|  Granted  | Full        | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity @Litigation @ID_Granted @Government_and_Economy
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |  Decision | GrantedType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim G&E|  Granted  | Full        | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @ProfessionalIndemnity @Litigation @CSOEProgram
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | Decision | GrantedType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_CSOEProgram|Granted  | Full        | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @ProfessionalIndemnity @Litigation @LawAndJustice
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision | GrantedType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim L&J|Granted  | Full        | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @ProfessionalIndemnity @Litigation @Publichospital
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | Decision | GrantedType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_publichospital|Granted  | Full        | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @ProfessionalIndemnity @Litigation @TransportAndVechicle
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | Decision | GrantedType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_TransportAndVechicle|Granted  | Full        | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |

  Scenario Outline: Creating Professional Ideminity claim and Indeminity Decision Denied approved
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @Professional_Indemnity   @ID_Denied_Approve @Business_and_Industry
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision | GrantedType | DenailDecision  | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto Claim B&I|Denied   | Full        | Approve         | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity  @ID_Denied_Approve @Environment_and_Water
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision | GrantedType | DenailDecision  | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim E&W|Denied   | Full        | Approve         | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity  @ID_Denied_Approve @Government_and_Economy
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision | GrantedType | DenailDecision  | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim G&E|Denied   | Full        | Approve         | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @ProfessionalIndemnity @Litigation @CSOEProgram
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       | Decision | GrantedType | DenailDecision  | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_CSOEProgram|Denied   | Full        | Approve         | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @ProfessionalIndemnity @Litigation @LawAndJustice
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision | GrantedType | DenailDecision  | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim L&J|Denied   | Full        | Approve         | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @ProfessionalIndemnity @Litigation @Publichospital
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision | GrantedType | DenailDecision  | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_publichospital|Denied   | Full        | Approve         | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @ProfessionalIndemnity @Litigation @TransportAndVechicle
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision | GrantedType | DenailDecision  | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Claim_TransportAndVechicle|Denied   | Full        | Approve         | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |

  Scenario Outline: Creating Professional Ideminity claim and Indeminity Decision Denied and Denied type full
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @Professional_Indemnity @ID_FullDenial @Business_and_Industry
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision | GrantedType | DenailDecision                | DenailType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto Claim B&I| Denied   | Full        | Request more information      |  Full      | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity  @ID_FullDenial @Environment_and_Water
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision | GrantedType | DenailDecision                | DenailType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim E&W| Denied   | Full        | Request more information      |  Full      | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity   @ID_FullDenial @Government_and_Economy
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision | GrantedType | DenailDecision                | DenailType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim G&E| Denied   | Full        | Request more information      |  Full      | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |

  Scenario Outline: Creating Professional Ideminity claim  and Indeminity Decision Denied and Denied type partial
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    And Select the indeminity decision "<Decision>""<GrantedType>"
    Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
    When User when logs in as "PortfolioManager"
    And Open the claim from "Claims Portfolio Manager 1 R3" queue
    And Select the indeminity denial action "<DenailDecision>""<DenailType>"
    Then I validate the "PENDING - INDEMNITY DECISION" for claims
    Then I logout of the application
    @Professional_Indemnity  @ID_PartialDenial @Business_and_Industry
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision    | GrantedType | DenailDecision                | DenailType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto Claim B&I|   Denied   | Partial     | Decline denial                |Partial     | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity @ID_PartialDenial @Environment_and_Water
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision    | GrantedType | DenailDecision                | DenailType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim E&W|   Denied   | Partial     | Decline denial                |Partial     | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
    @Professional_Indemnity @ID_PartialDenial @Government_and_Economy
    Examples:
      | loss                    |  finalstatus        |IncidentReportDate|OrgName       |Decision    | GrantedType | DenailDecision                | DenailType | finalstatustriage        | triagecasestatus |
      |  Professional Indemnity  |  RESOLVED-COMPLETED |  12/03/2021     |Auto claim G&E|   Denied   | Partial     | Decline denial                |Partial     | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |
