Feature: Construction Risk claim

    Scenario Outline: Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer.
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Select construction loss subtype "<lossSubtype>"
    And Click Continue button
    ### Added the below step, Its continue to proceed for the next page  - Select policy
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
    And Click Continue button
    And Capture loss detais "Material damage;3rd party property damage;Personal injury"
    And Click Continue button
    And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
    And Click Continue button
    And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    And Click Continue button
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    ### Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    ### Replaced the above step with the below and made flow modifications here
    Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
    Then I resolve claim as duplicate
    Then Now verify "<finalPenstatus>"
    Then Now I logout of the application

    @Construction_Risk @ResolveDuplicate @Property_and_Planning @RegresionClaimMLP4 @RegressionMLP4Scenarios
    Examples:
    |OrgName        | loss               | finalstatus        | lossSubtype                                               | finalstatustriage         | triagecasestatus  | NameOfContract | ContractDate | DateCommenceWork | DueDate    | ValueOfContract | WhenIncidetReported | howDamageOccur | EstimateDamage | DescriptionDamage | IncidentFirstReported | NameOfInjury | TreatmentProvided | whatHappended | finalPenstatus     |
    |Auto claim P&P | Construction Risks | RESOLVED-COMPLETED | Construction Risks - Material Damage and Liability Annual | PENDING-CLAIM COMPLETION  | PENDING-TRIAGE    | Automation PAP | 01/08/2021   | 01/08/2021       | 02/12/2021 | 6000            | test                | test           |     600        | Tst               | test                  | JohnCena     | Yes               | test          | RESOLVED-DUPLICATE |

    Scenario Outline: Initiate a Claim for Construction and resolve claim as cancel in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    ### Added the below step to select sub type
    And Select construction loss subtype "<lossSubtype>"
    And Click Continue button
    ### Added the below step, Its continue to proceed for the next page  - Select policy
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
    And Click Continue button
    And Capture loss detais "Material damage;3rd party property damage;Personal injury"
    And Click Continue button
    And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
    And Click Continue button
    And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    And Click Continue button
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    ### Then I search the claim file and cancel the claim from Actions Drop down
    ### added new below step by replacing above step
    Then I search the claim with orgnization "<OrgName>" and cancel the claim from Action drop down
    And verify the Claims Files Status "RESOLVED-CANCELLED" after Cancel from Actions drop down
    Then I logout of the application

    @Construction_Risk @ResolveDuplicate @Property_and_Planning @RegresionClaimMLP4 @RegressionMLP4Scenarios
    Examples:
    |OrgName        | loss                 |lossSubtype  | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
    |Auto claim P&P | Construction Risks   |Construction Risks - Material Damage and Liability Annual|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |


    Scenario Outline: Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
    And Click Continue button
    And Capture loss detais "Material damage;3rd party property damage;Personal injury"
    And Click Continue button
    And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
    And Click Continue button
    And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "26-BI settlement" "100"
    Then create Recovery estimate  "Client excess" "70"
    And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
    And Extract the recovery id
    And Verify the recovery history "test" "50"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery "Test" "20" "09/08/2021"
    And I verify recovery status "PAYMENT-RECOVERED"
    Then I logout of the application

    @Construction_Risk @ResolveDuplicate @Property_and_Planning @RegresionClaimMLP4
    Examples:
    |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
    |Auto claim P&P|Construction Risks - Material Damage and Liability Annual|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |5000         |300              |200              |

    Scenario Outline: Initiate a Claim for Construction and create final payment and close the claim
    ### I have updated the Scenario name here because the  given name is improper for the flow
    ### Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Select construction loss subtype "<lossSubtype>"
    And Click Continue button
    ### Added the below step, Its continue to proceed for the next page  - Select policy
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
    And Click Continue button
    And Capture loss detais "Material damage;3rd party property damage;Personal injury"
    And Click Continue button
    And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
    And Click Continue button
    And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    And Click Continue button
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    ### Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    ### Replaced the above step with the below and made flow modifications here
    Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transafer the claim to user "Claims Officer 1 R3"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    Then I open claim unit from worklist
    ### Have added new method in the above step def and updated multiple locators
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    ### After update, there where no locator find to proceed with pay info so added the locator in below step
    Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open case from manualpayments and approve the same
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsOfficer"
    ### Then I open claim unit from worklist
    ### Above step is replaced with below one
    Then I open the claim unit from Claims portal
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    @Construction_Risk @ResolveDuplicate @Property_and_Planning @RegresionClaimMLP4 @RegressionMLP4Scenarios
    ### Updated Loss value in the example set and also they haven't passed value for lossSubtype. Previously it was(loss value) - Construction Risks - Material Damage and Liability Annual
    Examples:
    |OrgName       | loss               | lossSubtype                                               |finalstatus        | finalstatustriage         | triagecasestatus  | NameOfContract| ContractDate| DateCommenceWork| DueDate   | ValueOfContract| WhenIncidetReported| howDamageOccur| EstimateDamage| DescriptionDamage| IncidentFirstReported| NameOfInjury| TreatmentProvided| whatHappended| payableamount| outstandingamount| recoverableamount|
    |Auto claim P&P| Construction Risks | Construction Risks - Material Damage and Liability Annual |RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION  | PENDING-TRIAGE    | Automation PAP| 01/08/2021  | 01/08/2021      | 02/12/2021| 6000           | test               | test          |     600       | Tst              | test                 | JohnCena    | Yes              | test         | 5000         | 300              | 200              |


    Scenario Outline: Initiate a Claim for Construction and resolve claim as  duplicate in triage claim officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
    And Click Continue button
    And Capture loss detais "Material damage;3rd party property damage;Personal injury"
    And Click Continue button
    And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
    And Click Continue button
    And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I resolve claim as duplicate
    Then Now verify "RESOLVED-DUPLICATE"
    Then Now I logout of the application

    @Construction_Risk @ResolveDuplicate @Property_and_Planning
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
        |Auto claim P&P|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |
    @Construction_Risk @ResolveDuplicate  @Education
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
        |Auto claim edu|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |
    @Construction_Risk @ResolveDuplicate @Environment_and_Water
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
        |Auto claim E&W|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |
    @Construction_Risk @ResolveDuplicate @Government_and_Economy
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
        |Auto claim G&E|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |
    @Construction_Risk @ResolveDuplicate @Law_and_Justice
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
        |Auto claim L&J|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |
    @Construction_Risk @ResolveDuplicate @Business_and_Industry
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
        |Auto Claim B&I|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |
    @Construction_Risk @ResolveDuplicate @Cemetery
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
        |Auto Claim cementery|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |

    Scenario Outline: Initiate a Claim for Construction and cancel claim
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
    And Click Continue button
    And Capture loss detais "Material damage;3rd party property damage;Personal injury"
    And Click Continue button
    And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
    And Click Continue button
    And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    And User when logs in as "TriageOfficer"
    Then I search the claim file and cancel the claim from Actions Drop down
    And verify the Claims Files Status "<TriageCancelStatus>" after Cancel from Actions drop down
    Then I logout of the application

    @Construction_Risk @ResolvedDuplicate @Property_and_Planning  @CancelClaim
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|
        |Auto claim P&P|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|
    @Construction_Risk @ResolvedDuplicate @Education  @CancelClaim
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|
        |Auto claim edu|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|
    @Construction_Risk @ResolvedDuplicate @Environment_and_Water  @CancelClaim
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|
        |Auto claim E&W|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|
    @Construction_Risk @ResolvedDuplicate @Government_and_Economy  @CancelClaim
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|
        |Auto claim G&E|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|
    @Construction_Risk @ResolvedDuplicate @Law_and_Justice  @CancelClaim
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|
        |Auto claim L&J|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|
    @Construction_Risk @ResolvedDuplicate @Business_and_Industry  @CancelClaim
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|
        |Auto Claim B&I|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|
    @Construction_Risk @ResolvedDuplicate @Cemetery  @CancelClaim
    Examples:
        |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|
        |Auto Claim cementery|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|

    Scenario Outline: Initiate a Claim for Construction Edit the claim data
        Given I have the external URL of VMIA
        When User enter valid username  "<username>" and password "<password>" click on Login button
        And I Select the Organization "<OrgName>"
        Then I am logged in to the insurance portal
        Then I want to make a claim
        And I enter the date and time of loss
        And I select the type of "<loss>"
        And Click Continue button
        When I select the business row and verify listed policies for "<loss>"
        And Click Continue button
        And Click Continue button
        And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
        And Click Continue button
        And Capture loss detais "Material damage;3rd party property damage;Personal injury"
        And Click Continue button
        And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
        And Click Continue button
        And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
        And Click Continue button
        And Click Continue button
        And I check the declaration for Claims
        Then Click Finish button
        Then I validate the "<finalstatus>" for claims
        And I extract the claim unit number
        Then I logout of the application
        Given I have the URL of VMIA
        And User when logs in as "TriageOfficer"
        Then I search the claim file and edit the claim "<triagecasestatus>"
        And Click Continue button
        When I select the business row and verify listed policies for "<loss>"
        And Click on Continue button for "2" from claims pages
        Then I am routed to Client Banking Account details page
        And Click on Continue button for "<NumTime2>" from claims pages
        Then Click Finish button
        Then I validate the "<finalstatus>" for claims
        Then I logout of the application

        @Construction_Risk @ResolveDuplicate  @EditClaim @Property_and_Planning
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
            |Auto claim P&P|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |
        @Construction_Risk @ResolveDuplicate  @EditClaim @Education
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
            |Auto claim edu|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |
        @Construction_Risk @ResolveDuplicate  @EditClaim @Environment_and_Water
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
            |Auto claim E&W|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |
        @Construction_Risk @ResolveDuplicate  @EditClaim @Government_and_Economy
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
            |Auto claim G&E|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |
        @Construction_Risk @ResolveDuplicate  @EditClaim @Law_and_Justice
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
            |Auto claim L&J|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |
        @Construction_Risk @ResolveDuplicate  @EditClaim @Business_and_Industry
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
            |Auto Claim B&I|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |
        @Construction_Risk @ResolveDuplicate  @EditClaim @Cemetery
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|
            |Auto Claim cementery|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |


    Scenario Outline: Initiate a Claim for Construction and make final payment and close the claim
        Given I have the external URL of VMIA
        When User enter valid username  "<username>" and password "<password>" click on Login button
        And I Select the Organization "<OrgName>"
        Then I am logged in to the insurance portal
        Then I want to make a claim
        And I enter the date and time of loss
        And I select the type of "<loss>"
        And Click Continue button
        When I select the business row and verify listed policies for "<loss>"
        And Click Continue button
        And Click Continue button
        And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
        And Click Continue button
        And Capture loss detais "Material damage;3rd party property damage;Personal injury"
        And Click Continue button
        And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
        And Click Continue button
        And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
        And Click Continue button
        And Click Continue button
        And I check the declaration for Claims
        Then Click Finish button
        Then I validate the "<finalstatus>" for claims
        And I extract the claim unit number
        Then I logout of the application
        When User when logs in as "TriageOfficer"
        Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
        Then I click its not a duplicate
        Then verify "<finalstatustriage>"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "PortfolioManager"
        And Open the claim from "Unassigned Claims" queue
        And Transafer the claim to user "Claims Officer 1 R3"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "ClaimsOfficer"
        Then I open claim unit from worklist
        Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
        Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
        And I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "Finance"
        And Open case from manualpayments and approve the same
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "ClaimsOfficer"
        Then I open claim unit from worklist
        Then I validate the "<finalstatus>" for claims
        Then I logout of the application

        @Construction_Risk   @Finalpayment @ResolveDuplicate @Property_and_Planning
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto claim P&P|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         | 5000         |300              |200              |
        @Construction_Risk @ResolveDuplicate   @Finalpayment @Education
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto claim edu|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         | 5000         |300              |200              |
        @Construction_Risk @ResolveDuplicate  @Finalpayment @Environment_and_Water
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto claim E&W|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         | 5000         |300              |200              |
        @Construction_Risk @ResolveDuplicate  @Finalpayment @Government_and_Economy
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto claim G&E|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         | 5000         |300              |200              |
        @Construction_Risk @ResolveDuplicate  @Finalpayment @Law_and_Justice
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto claim L&J|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         | 5000         |300              |200              |
        @Construction_Risk @ResolveDuplicate  @Finalpayment @Business_and_Industry
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto Claim B&I|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         | 5000         |300              |200              |
        @Construction_Risk @ResolveDuplicate  @Finalpayment @Cemetery
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto Claim cementery|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test  | 5000         |300              |200              |


    Scenario Outline: Initiate a Claim for Construction and  create recovery and aprrove recovery in finance
        Given I have the external URL of VMIA
        When User enter valid username  "<username>" and password "<password>" click on Login button
        And I Select the Organization "<OrgName>"
        Then I am logged in to the insurance portal
        Then I want to make a claim
        And I enter the date and time of loss
        And I select the type of "<loss>"
        And Click Continue button
        When I select the business row and verify listed policies for "<loss>"
        And Click Continue button
        And Click Continue button
        And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
        And Click Continue button
        And Capture loss detais "Material damage;3rd party property damage;Personal injury"
        And Click Continue button
        And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
        And Click Continue button
        And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
        And Click Continue button
        And Click Continue button
        And I check the declaration for Claims
        Then Click Finish button
        Then I validate the "<finalstatus>" for claims
        And I extract the claim unit number
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "TriageOfficer"
        Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
        Then I click its not a duplicate
        Then verify "<finalstatustriage>"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "PortfolioManager"
        And Open the claim from "Unassigned Claims" queue
        And Transafer the claim to user "Claims Officer 1 R3"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "ClaimsOfficer"
        Then I open claim unit from worklist
        Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
        Then create payment with "Single" "26-BI settlement" "100"
        Then create Recovery estimate  "Client excess" "70"
        And Add create recovery "Settlement" "Individual" "First" "lastname" "50"
        And Extract the recovery id
        And Verify the recovery history "test" "50"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "Finance"
        And Open and approve the recovery "Test" "20" "9/8/2021"
        And I verify recovery status "PAYMENT-RECOVERED"
        Then I logout of the application

        @Construction_Risk @ResolveDuplicate @Recovery @Property_and_Planning
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto claim P&P|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         | 5000         |300              |200              |
        @Construction_Risk @ResolveDuplicate @Recovery @Education
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto claim edu|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         | 5000         |300              |200              |
        @Construction_Risk @ResolveDuplicate @Recovery @Environment_and_Water
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto claim E&W|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         | 5000         |300              |200              |
        @Construction_Risk @ResolveDuplicate @Recovery @Government_and_Economy
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto claim G&E|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         | 5000         |300              |200              |
        @Construction_Risk @ResolveDuplicate @Recovery @Law_and_Justice
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto claim L&J|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         | 5000         |300              |200              |
        @Construction_Risk @ResolveDuplicate  @Recovery @Business_and_Industry
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto Claim B&I|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         | 5000         |300              |200              |
        @Construction_Risk @ResolveDuplicate  @Recovery @Cemetery
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|payableamount|outstandingamount|recoverableamount|
            |Auto Claim cementery|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test  | 5000         |300              |200              |


    Scenario Outline: Initiate a Claim for Construction And Indeminity Decision granted  and add litigation
        Given I have the external URL of VMIA
        When User enter valid username  "<username>" and password "<password>" click on Login button
        And I Select the Organization "<OrgName>"
        Then I am logged in to the insurance portal
        Then I want to make a claim
        And I enter the date and time of loss
        And I select the type of "<loss>"
        And Click Continue button
        When I select the business row and verify listed policies for "<loss>"
        And Click Continue button
        And Click Continue button
        And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
        And Click Continue button
        And Capture loss detais "Material damage;3rd party property damage;Personal injury"
        And Click Continue button
        And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
        And Click Continue button
        And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
        And Click Continue button
        And Click Continue button
        And I check the declaration for Claims
        Then Click Finish button
        Then I validate the "<finalstatus>" for claims
        And I extract the claim unit number
        Then I logout of the application
        Given I have the URL of VMIA
        Given User when logs in as "TriageOfficer"
        Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
        Then I resolve claim as duplicate
        Then Now verify "RESOLVED-DUPLICATE"
        Then Now I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "PortfolioManager"
        Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
        And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "ClaimsOfficer"
        Then I open claim unit from worklist
        And Select the indeminity decision "<Decision>""<GrantedType>"
        And Validated the medical claim indemnity case status "PENDING-INVESTIGATION"
        And User should Add litigation
        And Enter the Solicitor details "Defendant" "PavanAutomation" "14/May/2021" "PlaintiffSolicitr" "Test"
        And Enter the Litigatation Status "Coronial" "Judgement" "County" "12345"  "14/May/2021" "14/May/2021" "Mani_qa"
        And Enter the CoDefendent details "Ronaldo" "Gayle" "Rabada"
        And Enter the Timetable details "Inquest" "25/Jun/2021" "Sudheer"
        And Enter the Event details "Yes" "15/Jun/2021" "Hi event note"
        And Verify the Timetable history "Yes" "15/02/0021" "25/06/2021" "Inquest" "Claims Officer 1 R3"
        And Enter the Settlement details "Settlement" "14/May/2021" "comment" "Defendant pays" "No"
        And Select settlement monetary "No" "500000" "500001" "comment"
        And complete the litigiation for "ClaimsOfficer"
        Then I validate the "LITIGATION-SETTLED" for litigation
        Then I logout of the application

        @Construction_Risk @ResolvedDuplicate  @ID_Granted @Litigation @Property_and_Planning
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType |
            |Auto claim P&P|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Granted | Full        |
        @Construction_Risk @ResolvedDuplicate  @ID_Granted @Litigation @Education
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType |
            |Auto claim edu|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Granted | Full        |
        @Construction_Risk @ResolvedDuplicate  @ID_Granted @Litigation @Environment_and_Water
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType |
            |Auto claim E&W|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Granted | Full        |
        @Construction_Risk @ResolvedDuplicate  @ID_Granted @Litigation @Government_and_Economy
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType |
            |Auto claim G&E|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Granted | Full        |
        @Construction_Risk @ResolvedDuplicate  @ID_Granted @Litigation @Law_and_Justice
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType |
            |Auto claim L&J|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Granted | Full        |
        @Construction_Risk @ResolvedDuplicate  @ID_Granted @Litigation @Business_and_Industry
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType |
            |Auto Claim B&I|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Granted | Full        |
        @Construction_Risk @ResolvedDuplicate  @ID_Granted @Litigation @cementery
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType |
            |Auto Claim cementery|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Granted | Full        |



    Scenario Outline: Initiate a Claim for Construction and Indeminity Decision Denied approved
        Given I have the external URL of VMIA
        When User enter valid username  "<username>" and password "<password>" click on Login button
        And I Select the Organization "<OrgName>"
        Then I am logged in to the insurance portal
        Then I want to make a claim
        And I enter the date and time of loss
        And I select the type of "<loss>"
        And Click Continue button
        When I select the business row and verify listed policies for "<loss>"
        And Click Continue button
        And Click Continue button
        And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
        And Click Continue button
        And Capture loss detais "Material damage;3rd party property damage;Personal injury"
        And Click Continue button
        And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
        And Click Continue button
        And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
        And Click Continue button
        And Click Continue button
        And I check the declaration for Claims
        Then Click Finish button
        Then I validate the "<finalstatus>" for claims
        And I extract the claim unit number
        Then I logout of the application
        Given I have the URL of VMIA
        Given User when logs in as "TriageOfficer"
        Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
        Then I resolve claim as duplicate
        Then Now verify "RESOLVED-DUPLICATE"
        Then Now I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "PortfolioManager"
        Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
        And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "ClaimsOfficer"
        Then I open claim unit from worklist
        And Select the indeminity decision "<Decision>""<GrantedType>"
        Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
        When User when logs in as "PortfolioManager"
        And Open the claim from "Claims Portfolio Manager 1 R3" queue
        And Select the indeminity denial action "<DenailDecision>""<DenailType>"
        Then I validate the "PENDING - INDEMNITY DECISION" for claims
        Then I logout of the application
        @Construction_Risk @ResolvedDuplicate @ID_Denied_Approve @Property_and_Planning
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision  |
            |Auto claim P&P|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|Denied   | Full        | Approve         |
        @Construction_Risk @ResolvedDuplicate @ID_Denied_Approve @Education
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision  |
            |Auto claim edu|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|Denied   | Full        | Approve         |
        @Construction_Risk @ResolvedDuplicate @ID_Denied_Approve @Environment_and_Water
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision  |
            |Auto claim E&W|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|Denied   | Full        | Approve         |
        @Construction_Risk @ResolvedDuplicate @ID_Denied_Approve @Government_and_Economy
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision  |
            |Auto claim G&E|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|Denied   | Full        | Approve         |
        @Construction_Risk @ResolvedDuplicate @ID_Denied_Approve @Law_and_Justice
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision  |
            |Auto claim L&J|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|Denied   | Full        | Approve         |
        @Construction_Risk @ResolvedDuplicate @ID_Denied_Approve  @Business_and_Industry
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision  |
            |Auto Claim B&I|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|Denied   | Full        | Approve         |
        @Construction_Risk @ResolvedDuplicate @ID_Denied_Approve @Cemetery
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision  |
            |Auto Claim cementery|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|Denied   | Full        | Approve         |



    Scenario Outline: Initiate a Claim for Construction and Indeminity Decision Denied and Denied type full                          @ID_Denied_Full
        Given I have the external URL of VMIA
        When User enter valid username  "<username>" and password "<password>" click on Login button
        And I Select the Organization "<OrgName>"
        Then I am logged in to the insurance portal
        Then I want to make a claim
        And I enter the date and time of loss
        And I select the type of "<loss>"
        And Click Continue button
        When I select the business row and verify listed policies for "<loss>"
        And Click Continue button
        And Click Continue button
        And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
        And Click Continue button
        And Capture loss detais "Material damage;3rd party property damage;Personal injury"
        And Click Continue button
        And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
        And Click Continue button
        And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
        And Click Continue button
        And Click Continue button
        And I check the declaration for Claims
        Then Click Finish button
        Then I validate the "<finalstatus>" for claims
        And I extract the claim unit number
        Then I logout of the application
        Given I have the URL of VMIA
        Given User when logs in as "TriageOfficer"
        Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
        Then I resolve claim as duplicate
        Then Now verify "RESOLVED-DUPLICATE"
        Then Now I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "PortfolioManager"
        Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
        And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "ClaimsOfficer"
        Then I open claim unit from worklist
        And Select the indeminity decision "<Decision>""<GrantedType>"
        Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
        When User when logs in as "PortfolioManager"
        And Open the claim from "Claims Portfolio Manager 1 R3" queue
        And Select the indeminity denial action "<DenailDecision>""<DenailType>"
        Then I validate the "PENDING - INDEMNITY DECISION" for claims
        Then I logout of the application
        @Construction_Risk @ResolvedDuplicate   @ID_Denied_Full @Property_and_Planning
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto claim P&P|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Denied   | Full        | Request more information      |  Full      |
        @Construction_Risk @ResolvedDuplicate   @ID_Denied_Full @Education
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto claim edu|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Denied   | Full        | Request more information      |  Full      |
        @Construction_Risk @ResolvedDuplicate   @ID_Denied_Full @Environment_and_Water
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto claim E&W|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Denied   | Full        | Request more information      |  Full      |
        @Construction_Risk @ResolvedDuplicate   @ID_Denied_Full @Government_and_Economy
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto claim G&E|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Denied   | Full        | Request more information      |  Full      |
        @Construction_Risk @ResolvedDuplicate   @ID_Denied_Full @Law_and_Justice
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto claim L&J|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Denied   | Full        | Request more information      |  Full      |
        @Construction_Risk @ResolvedDuplicate   @ID_Denied_Full  @Business_and_Industry
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto Claim B&I|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Denied   | Full        | Request more information      |  Full      |
        @Construction_Risk @ResolvedDuplicate   @ID_Denied_Full @Cemetery
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto Claim cementery|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED| Denied   | Full        | Request more information      |  Full      |


    Scenario Outline: Initiate a Claim for Construction and Indeminity Decision Denied and Denied type partial                       @ID_Denied_Partial
        Given I have the external URL of VMIA
        When User enter valid username  "<username>" and password "<password>" click on Login button
        And I Select the Organization "<OrgName>"
        Then I am logged in to the insurance portal
        Then I want to make a claim
        And I enter the date and time of loss
        And I select the type of "<loss>"
        And Click Continue button
        When I select the business row and verify listed policies for "<loss>"
        And Click Continue button
        And Click Continue button
        And Enter the contract details "<NameOfContract>" "<ContractDate>" "<DateCommenceWork>" "<DueDate>" "<ValueOfContract>" "PartyClaim"
        And Click Continue button
        And Capture loss detais "Material damage;3rd party property damage;Personal injury"
        And Click Continue button
        And Enter the Proerty damage details "<WhenIncidetReported>" "<howDamageOccur>" "No" "No" "<EstimateDamage>" "<DescriptionDamage>"
        And Click Continue button
        And Enter Personal Injury Details "<IncidentFirstReported>" "<NameOfInjury>" "<NameOfInjury>" "<TreatmentProvided>" "<whatHappended>"
        And Click Continue button
        And Click Continue button
        And I check the declaration for Claims
        Then Click Finish button
        Then I validate the "<finalstatus>" for claims
        And I extract the claim unit number
        Then I logout of the application
        Given I have the URL of VMIA
        Given User when logs in as "TriageOfficer"
        Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
        Then I resolve claim as duplicate
        Then Now verify "RESOLVED-DUPLICATE"
        Then Now I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "PortfolioManager"
        Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
        And Validated the medical claim indemnity case status "PENDING - INDEMNITY DECISION"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "ClaimsOfficer"
        Then I open claim unit from worklist
        And Select the indeminity decision "<Decision>""<GrantedType>"
        Then I validate the "<PENDING - INDEMNITY DECISION>" for claims
        When User when logs in as "PortfolioManager"
        And Open the claim from "Claims Portfolio Manager 1 R3" queue
        And Select the indeminity denial action "<DenailDecision>""<DenailType>"
        Then I validate the "PENDING - INDEMNITY DECISION" for claims
        Then I logout of the application
        @Construction_Risk @ResolvedDuplicate     @ID_Denied_Partial @Property_and_Planning
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto claim P&P|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|   Denied   | Partial     | Decline denial                |Partial     |
        @Construction_Risk @ResolvedDuplicate     @ID_Denied_Partial @Education
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto claim edu|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|   Denied   | Partial     | Decline denial                |Partial     |
        @Construction_Risk @ResolvedDuplicate     @ID_Denied_Partial @Environment_and_Water
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto claim E&W|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|   Denied   | Partial     | Decline denial                |Partial     |
        @Construction_Risk @ResolvedDuplicate     @ID_Denied_Partial  @Government_and_Economy
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto claim G&E|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|   Denied   | Partial     | Decline denial                |Partial     |
        @Construction_Risk @ResolvedDuplicate     @ID_Denied_Partial @Law_and_Justice
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto claim L&J|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|   Denied   | Partial     | Decline denial                |Partial     |
        @Construction_Risk @ResolvedDuplicate     @ID_Denied_Partial  @Business_and_Industry
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto Claim B&I|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|   Denied   | Partial     | Decline denial                |Partial     |
        @Construction_Risk @ResolvedDuplicate     @ID_Denied_Partial @Cemetery
        Examples:
            |OrgName      | loss                                             | finalstatus        | finalstatustriage            | triagecasestatus  |NameOfContract|ContractDate|DateCommenceWork|DueDate   |ValueOfContract|WhenIncidetReported|howDamageOccur|EstimateDamage|DescriptionDamage|IncidentFirstReported|NameOfInjury|TreatmentProvided|whatHappended|TriageCancelStatus|Decision | GrantedType | DenailDecision                | DenailType |
            |Auto Claim cementery|Construction Risks – Material Damage and Liability|RESOLVED-COMPLETED| PENDING-CLAIM COMPLETION       | PENDING-TRIAGE    |Automation PAP|01/08/2021  |01/08/2021      |02/12/2021|6000           | test              |test          |     600      |Tst              | test                |JohnCena    |Yes              |test         |RESOLVED-CANCELLED|   Denied   | Partial     | Decline denial                |Partial     |

