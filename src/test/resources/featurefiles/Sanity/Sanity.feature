Feature: Sanity suite for VMIA

  Scenario Outline: Start a new policy application for Cyber product and select client acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And I enter the revenue details "100" "100"
    And Click UW Continue button
    And I enter details in the Data protection "VMIA Test" "Test" Screen
    And Click UW Continue button
    And I enter the details in Data access and recovery Screen
    And Click UW Continue button
    And I enter the details in  Outsourcing activities Screen
    And Click UW Continue button
    And I enter the details Claims information Screen
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I logout of the application

    @Sanity
    Examples:
      | decsion |  Permium   | UnderwritingNotes  |  orgname          |product                    | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|
      | No      |    10      | Hello VMIA         |  RegressionSet2_LawAndJustice  |Cyber| PENDING-UNDERWRITING | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |


  Scenario Outline: Initiate a Claim for GPA and resolve claim as duplicate in triage claim officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Select Private Claims as "No"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
#    And I validate the "<finalstatus>"
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logout of the application
    Given I have the URL of VMIA
    When I enter the "<triageclaimsofficerusername>"
    And I enter "<triageclaimsofficerpassword>"
    And I click login button
    Then Now I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then Now I resolve claim as duplicate
    Then Now verify duplicate status "<finalstatustriage>"
    Then Now I logout of the application

    @Sanity
    Examples:
      | finalstatustriage  | triagecasestatus |  triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        | name     |
      | RESOLVED-DUPLICATE | PENDING-TRIAGE   | TriageClaimsOfficer1R3      | Welcome@1234                | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED | Lowanna4 |
