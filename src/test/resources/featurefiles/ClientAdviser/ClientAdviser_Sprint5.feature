Feature: Client Adviser suite for VMIA MLP1

  Scenario Outline: Raise a general service request in client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on new interaction
    And I select the organization for new interaction "<organization>"
    Then I add task and do a general service request
    Then I enter the service request query parameters "<enquiryarea>" "<enquirycategory>" and I assert "<thankyoumessage>" "<gsrstatus>"
    Then I logoff from client adviser portal

    @ghgg @CS @ClientAdviser
    Examples: 
      | username          | password     | organization      | enquiryarea | enquirycategory | thankyoumessage                                            | gsrstatus          |
      | AutoClientAdviser | Welcome@1234 | Automation School | Insurance   | Policy advice   | General enquiry case successfully created, please confirm! | Resolved-Completed |

  Scenario Outline: Raise a general service request in client adviser portal pending investigation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on new interaction
    And I select the organization for new interaction "<organization>"
    Then I add task and do a general service request
    Then I enter the service request query parameters which cannot be handled via phone "<enquiryarea>" "<enquirycategory>" and I assert "<thankyoumessage>" "<gsrstatus>"
    Then I logoff from client adviser portal

    @ghgsgz @CS @ClientAdviser
    Examples: 
      | username          | password     | organization      | enquiryarea | enquirycategory | thankyoumessage                                            | gsrstatus             |
      | AutoClientAdviser | Welcome@1234 | Automation School | Insurance   | Policy advice   | General enquiry case successfully created, please confirm! | Pending-Investigation |

  Scenario Outline: Make a claim from client adviser portal liability
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a claim
    Then I enter business details for claim
    Then I want to make a claim for client adviser
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And Click Continue button
    Then I provide liability loss details "<what>" "<claimantname>" "<addline1>" "<addline2>" "<suburb>" "<state>" "<postcode>" "<nature>" "<treatment>"
    And Click Continue button
    Then I attach an evidence file "<filepath>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logoff from client adviser portal

    @ghgsg @CS @ClientAdviserClaim
    Examples: 
      | username          | password     | loss                         | what    | claimantname | addline1 | addline2 | suburb    | state           | postcode | nature | treatment | category                       | filepath                              | finalstatus        |
      | AutoClientAdviser | Welcome@1234 | Public and Product Liability | Highway | liabClaimant | Sydney   | Central  | Chatswood | New South Wales |     9897 | nature | plaster   | Repair or replacement invoices | C:\\Attachments\\Assertions_Added.txt | RESOLVED-COMPLETED |

  Scenario Outline: Make a claim from client adviser portal for GPA
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a claim
    Then I enter business details for claim
    Then I want to make a claim for client adviser
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    Then I logoff from client adviser portal
    When I enter the "<claimsofficerusername>"
    And I enter "<claimsofficerpassword>"
    And I click login button
    Then I search the claim unit from workbasket
    Then I logoff from client adviser portal

    @ClientAdviserGPA
    Examples: 
      | username          | password     | claimsofficerusername | claimsofficerpassword | loss                    | AccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | AutoClientAdviser | Welcome@1234 | ClaimsOfficer         | Welcome@321           | Group Personal Accident | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

  Scenario Outline: Initiate a Claim for Property in Client adviser
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a claim
    Then I enter business details for claim
    Then I want to make a claim for client adviser
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And Click Continue button
    Then I provide loss details "<Where>" "<What>" "<How>"
    And Click Continue button
    Then I provide SCIP details "<purchasedescription>" "<cost>" "<salvagecost>"
    And Click Continue button
    Then I attach an evidence file "<filepath>" "<category>"
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logoff from client adviser portal

@ClientAdviserPRO
    Examples: 
      | username          | password     | loss     | Where   | What       | How          | purchasedescription | cost | salvagecost | category                       | filepath                              | finalstatus        |
      | AutoClientAdviser | Welcome@1234 | Property | Highway | TyreBusted | Overspeeding | Tyre                | 1000 |         500 | Repair or replacement invoices | C:\\Attachments\\Assertions_Added.txt | RESOLVED-COMPLETED |

  Scenario Outline: Start a new policy application for GPA with no deviation in client adviser portal
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter number of "<students>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<students>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    # And I provide some "<action>"
    Then I validate the premium for "<students>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logoff from client adviser portal

    @ClientAdviserGPAUW
    Examples: 
      | username          | password     | product | students | multiplier | gst | stampduty | startdate | client_number | status                   | days | paymentstatus   |
      | AutoClientAdviser | Welcome@1234 | GPA     |      400 |        5.5 | 0.1 |       0.1 | 1/09/2020 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |  333 | PENDING-PAYMENT |

  @Sprint2 @UW @PRO @PRONoDeviation @Regression @reg3 @clientadviserPROIns
  Scenario Outline: Start a new policy application from client adviser for property with STP
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logoff from client adviser portal

    Examples: 
      | username          | password     | product | value          | propertyvalue | minimumvalue | percentage | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername  | uwpassword  | paymentstatus   |
      | AutoClientAdviser | Welcome@1234 | PRO     | Up to $250,000 |        100000 |          950 |          1 | 0.1 |       0.1 | 1/09/2020 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  303 | underwriter | Welcome@321 | PENDING-PAYMENT |

  Scenario Outline: Start a new policy application for Liability from client adviser with no deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    #And Validate "<client_number>"
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logoff from client adviser portal

    @ClientAdviserPPLUW
    Examples: 
      | username          | password     | product | basepremium | paymentstatus   | hiredays          | gst | stampduty | startdate | client_number | status                   |
      | AutoClientAdviser | Welcome@1234 | PPL     |         320 | PENDING-PAYMENT | Less than 50 days | 0.1 |       0.1 | 1/09/2020 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE |

  Scenario Outline: Start a new policy application for MSC in client adviser with no deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for "<parkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @ClientAdviserMSCUW
    Examples: 
      | username          | password     | product | parkingslots | multiplier | gst | stampduty | startdate | status                   | days | paymentstatus   |
      | AutoClientAdviser | Welcome@1234 | MSC     |          400 |         12 | 0.1 |       0.1 | 1/09/2020 | PENDING-CLIENTACCEPTANCE |  303 | PENDING-PAYMENT |

  Scenario Outline: Start a new policy application for Motor Vehicle in client adviser without deviation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter business details for claim
    Then I want to start a new application for client adviser
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the start date of insurance "<startdate>"
    Then I fill up the MV details in the risk page "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<typeofvehicle>" "<manufactureyear>" "<make>" "<model>" "<regno>" "<currentmarketvalue>" "<numberofvehicles>"
    And Click UW Continue button
    #And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for MV "<lightvehicleflag>" "<busflag>" "<trailerflag>" "<otherflag>" "<pricelightvehicle>" "<pricebusunder50>" "<pricebusover50>" "<pricetrailer>" "<numberofvehicles>" "<gst>" "<stampduty>" "<currentmarketvalue>" "<days>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @ClientAdviserMWUW
    Examples: 
      | username          | password     | product | parkingslots | multiplier | gst | stampduty | startdate | status                   | days | paymentstatus   | lightvehicleflag | busflag | trailerflag | otherflag | typeofvehicle | manufactureyear | make | model | regno | currentmarketvalue | numberofvehicles | pricelightvehicle | pricebusunder50 | pricebusover50 | pricetrailer |
      | AutoClientAdviser | Welcome@1234 | MVF     |          400 |      11.98 | 0.1 |       0.1 | 1/09/2020 | PENDING-CLIENTACCEPTANCE |  303 | PENDING-PAYMENT | true             | true    | true        | false     | SUV           |            2011 | ABC  | n97   |  8798 |              40000 |                1 |               450 |             620 |         891.78 |           50 |
