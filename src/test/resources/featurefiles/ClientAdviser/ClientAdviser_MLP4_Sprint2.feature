Feature: Client Adviser suite for VMIA MLP1

  Scenario Outline: Verify Risk adviosr is able to add asset and underwriter approve the same asset
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Add asset" in AssetManagement
    And Select the organization "<organization>" for Adding Asset
    And Select the contact associate with request "No"
    And Click Continue Button
    And Add Asset Details "<Location>" "<AssetType>" "<AssetName>" "<AssetBand>"
    And Click Continue Button
    And Click Continue Button
    And Click Finish Button
    And Capture the Asset Details and Click submit asset
    Then Verify the case status "PENDING- APPROVAL" and capture the case id
    Then I logoff from client adviser portal
    And I have the URL of VMIA
    When User when logs in as "UWPortfoliaManger"
    And Open Asset from "Asset Management" Underwriter Queue
    Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
    Then I check the case status to be "<FinalStatus>"
    And I logout of the application

    Examples:
      #| username          | password     | organization | Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStatus|
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED|
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|No  |RESOLVED-COMPLETED|
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | MELBOURNE AIRPORT  | Contents     | Test 		| 2				|Reject |No   |RESOLVED-REJECTED|
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Request more information |No   |RESOLVED-RFI|
      
   
		
   #Scenario Outline: Verify Risk adviosr is able to Update
    #Given I have the URL of VMIA
    #When I enter the "<username>"
    #And I enter "<password>"
    #And I click login button
    #Then I am logged in to the client adviser portal
    #And Click New and Select "Update asset" in AssetManagement
    #And Select the organization "<organization>" for Adding Asset
    #And Select the contact associate with request "No"
#		And Click Continue Button
#		And Search the Assetby "Asset name" using value "<AssetName>"
#		And Select the Asset result by name "<AssetName>"
#		And Update the selected asset by "<AssetName>"
#		And Click Continue Button
#		And Click Continue Button
#		And Click Finish Button
#		And Capture the Asset Details and Click submit asset
#		Then Verify the case status "PENDING- APPROVAL" and capture the case id
#		Then I logoff from client adviser portal
#		And I have the URL of VMIA
#		When User when logs in as "UWPortfoliaManger"
#		And Open Asset from "Asset Management" Underwriter Queue
#		Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
#		Then I check the case status to be "<FinalStatus>"
#		And I logout of the application
#		
    #@ghgg @CS @ClientAdviser
    #Examples: 
      #| username          | password     | organization | Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStaus|
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED |
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|No  |RESOLVED-COMPLETED |
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Reject|Yes   |RESOLVED-REJECTED |
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Request more information|Yes   |RESOLVED-RFI |
         
    #Scenario Outline: Verify Risk adviosr is able to Dispose the asset
    #Given I have the URL of VMIA
    #When I enter the "<username>"
    #And I enter "<password>"
    #And I click login button
    #Then I am logged in to the client adviser portal
    #And Click New and Select "Dispose asset" in AssetManagement
    #And Select the organization "<organization>" for Adding Asset
    #And Select the contact associate with request "No"
#		And Click Continue Button
#		And Search the Assetby "Asset name" using value "test"
#		And Select the Asset result by name "test"
#		And Click Continue Button
#		And Click Continue Button
#		And Click Finish Button
#		And Capture the Asset Details and Click submit asset
#		Then Verify the case status "PENDING- APPROVAL" and capture the case id
#		Then I logoff from client adviser portal
#		And I have the URL of VMIA
#		When User when logs in as "UWPortfoliaManger"
#		And Open Asset from "Asset Management" Underwriter Queue
#		Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
#		Then I check the case status to be "<FinalStaus>"
#		And I logout of the application
#		
    #@ghgg @CS @ClientAdviser
    #Examples: 
    #	 | username          | password     | organization | Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStaus|
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED |
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|No  |RESOLVED-COMPLETED |
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Reject|Yes   |RESOLVED-REJECTED |
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Request more information|Yes   |RESOLVED-RFI |
      
      
      
    #Scenario Outline: Verify Risk adviosr is able to Transfer the asset
    #Given I have the URL of VMIA
    #When I enter the "<username>"
    #And I enter "<password>"
    #And I click login button
    #Then I am logged in to the client adviser portal
    #And Click New and Select "Transfer asset" in AssetManagement
    #And Select the organization "<organization>" for Adding Asset
    #And Select the contact associate with request "No"
    #And Click Continue Button
    #And Select the organization "<DestinationOrg>" for Adding Asset
#		And Click Continue Button
#		And Search the Assetby "Asset name" using value "Test"
#		And Select the Asset result by name "Test"
#		And Click Continue Button
#		And Click Continue Button
#		And Click Finish Button
#		And Capture the Asset Details and Click submit asset
#		And Link the policy "V010874" to the asset
#		Then Verify the case status "PENDING- APPROVAL" and capture the case id
#		Then I logoff from client adviser portal
#		And I have the URL of VMIA
#		When User when logs in as "UWPortfoliaManger"
#		And Open Asset from "Asset Management" Underwriter Queue
#		Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
#		Then I check the case status to be "<FinalStatus>"
#		And I logout of the application
		

    #Examples: 
      #| username          | password     | organization |DestinationOrg| Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStatus|
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP |Lowanna27      |Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED|
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP |Lowanna27      |Sydney cot  | Bore     | Test 		| 2				|Approve|No   |RESOLVED-COMPLETED|
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Reject|Yes   |RESOLVED-REJECTED |
      #| AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Request more information|Yes   |RESOLVED-RFI |
      
      
      
    Scenario Outline: Verify Risk adviosr is able to upload the bulk upload template 
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Bulk add/update assets" in AssetManagement
    And Attach the bulk upload file "<file>" and click submit
    Then I logoff from client adviser portal
    And I have the URL of VMIA
		When User when logs in as "UWPortfoliaManger"
		And Open Asset from "Asset Management" Underwriter Queue
		Then Perform "<Action>" action on the asset and Select "<IsEndorseNeed>" in Endorsement descion for Asset
		Then I check the case status to be "<FinalStatus>"
		And I logout of the application
		
		Examples: 
	  | username          | password     | file														  | Action |FinalStatus|  IsEndorseNeed|
#		| AutoClientAdviser | Welcome@1234 |BulkUploadTemplateTest_Updated.csv|Approve|RESOLVED-COMPLETED|No|
    | AutoClientAdviser | Welcome@1234 |BulkUploadTemplateTest_Updated.csv|Approve|RESOLVED-COMPLETED|Yes|
    | AutoClientAdviser | Welcome@1234 |BulkUploadTemplateTest_Updated.csv|Reject|RESOLVED-REJECTED|Yes|
    | AutoClientAdviser | Welcome@1234 |BulkUploadTemplateTest_Updated.csv|Request more information|RESOLVED-RFI|Yes|