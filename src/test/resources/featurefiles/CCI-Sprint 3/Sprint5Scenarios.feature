Feature: It covers, all the automatable scenarios from Sprint 5

  Scenario Outline: CR-PI-A(A)-LXRP,  Project Professional Indemnity - Alliance Annual Policy (LXRP)
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Enter the Policy Effective date "27/08/2021" and Expiration date "8/08/2085"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And I Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click the exit button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<cancelPremium>" and "<UnderwritingNotes>" in cancellation
    Then I submit the case to get routed to the user
    And I check the case status to be "PENDING-REFUND"
    Then I logout of the application

    @SprintScenarios
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname               | product                                                        | status         | pendingstatus        | uwstatus                 | finalstatus     | EnableOrDisableProrate| cancelPremium | IsCancelFullTerm | cancellationdate | Cancelreason |
      | No      |  200      | Hello VMIA         | Automation_Transaport | Project Professional Indemnity - Alliance Annual Policy (LXRP) | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                   | -150          | yes              | 22/07/2021       | Property sold|

  Scenario Outline: CR-PI-ITC(A)-MRPV,   Project Professional Indemnity - Annual Insurance Policy (MRPV)
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Enter the Policy Effective date "27/08/2021" and Expiration date "8/08/2085"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And I Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click the exit button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<cancelPremium>" and "<UnderwritingNotes>" in cancellation
    Then I submit the case to get routed to the user
    And I check the case status to be "PENDING-REFUND"
    Then I logout of the application

    @SprintScenarios
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname               | product                                                         | status         | pendingstatus        | uwstatus                 | finalstatus     | EnableOrDisableProrate| cancelPremium | IsCancelFullTerm | cancellationdate | Cancelreason |
      | No      |  200      | Hello VMIA         | Automation_Transaport | Project Professional Indemnity - Annual Insurance Policy (MRPV) | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                   | -150          | yes              | 22/07/2021       | Property sold|
