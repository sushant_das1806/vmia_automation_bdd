Feature: It covers, all the automatable scenarios from Sprint 8

  Scenario Outline: CCI-5874 - Ability to make Endorsements Inactive
  #Start a new policy application and then make Endorsement for MSC
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    Then I enter the number of "<updatedparkingslots>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    When I Click new button to deactivate endorsement
    Then Search and select Oraganization "<name>"
    And Select contact from contact "<contact>"
    When select the policy and click finish
    And select the endorsementID with deactivate reason "<dereason>" and click continue
    When I verify the payment status "<pendingstatus>"
    And I capture the policy ID
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    And I search the deactivate endorsementId from "<queue>"
    When select the deactivate EndorsementID
    And reject the request and submit with "<rejComments>"
    #    And I validate the status of the case to be "<pendingreview>"
#    And I check the status to be "<pendingreview>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    When select deactivate endorsementID to proceed deactivation of the endorsement
    Then click on finish button
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    And I search the deactivate endorsementId from "<queue>"
    When select the deactivate EndorsementID
    And approve the request and submit
    #    And I validate the status of the case to be "<finalStatus>"
#    And I check the status to be "<finalStatus>" Regression_PAP
    Then I logout of the application

    Examples:
      | product                     | parkingslots | updatedparkingslots | startdate   | status                   | endpaymentstatus | paymentstatus   | name            | pendingstatus    | queue                        | rejComments      | dereason |
      | Motor – Special Contingency | 400          | 500                 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT  | PENDING-PAYMENT | Auto Claim Schl | PENDING-APPROVAL | Endorsement Management Queue | verify rejection | deactive |
#    Examples:
#      | OrgName         | pendingstatus    | queue                        | rejComments      | pendingreview  | finalStatus        | dereason |
#      | Auto Claim Schl | PENDING-APPROVAL | Endorsement Management Queue | verify rejection | PENDING-REVIEW | RESOLVED-COMPLETED | deactive |

  Scenario Outline: CCI-6078 - New CSOE Bundle
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click CreateNew button
    And Enter the Orgdetails "<clientCategory>" "<goveDepartment>" "tester" "8508356502" "Sydney " and "Department of Health"
    And I note down the PayWay number ID
    And I logoff from client adviser portal
    Given I have the URL of VMIA
    And User when logs in as "Finance"
    Then I open the "Payway Account" workbasket in the finance portal
    And I open the Payway Case and enter the Payway Number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    And Click New and Select "Manage contact"
    Then Search the contact using contactid "CONT-27655"
    And Add the contact to Organzation to the contact "" "Insurance;Finanace"
    And Click New and Select "Create policy bundle"
    And Select orgnazation by searching clientsegment "<clientCategory>" bundle type "<bundleType>" and orgname
    Then validate the product list "<bundleType>"
    Then Add the unincorproted orgnization "<UnincorprateOrg>" and policyEffectiveDate "<PolicyeffecveDate>" and finish the policy
    Then Verify case status "<finalstatus>" and capture the case id
    When Click New and Select "Cancel bundle" from Cancel Policy option
    And  search the organization using clientSegment "<clientCategory>" and Orgnazation
    And  Select the contact and Product "CSO" click finish
    Then Cancel the bundle policy using "<IsFullTermCancel>" and "<Cancelreason>"
    Then I validate the "<finalstatus>" after bundle policy cancellation
    And I logoff from client adviser portal
    @SprintScenarios
    Examples:
      | UnincorprateOrg | IsFullTermCancel | PolicyeffecveDate  | Cancelreason  | finalstatus        | bundleType | clientCategory                              | goveDepartment                       |
      | Scotland PNP    | Yes              | current            | Property sold | RESOLVED-COMPLETED | CSOE (CLS) | Community Service Organisation (Education)  | Department of Education and Training |


  Scenario Outline: CCI-6626 - Search function for Historical Policy (Pre 1 July 2013)
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I validate warning message "<warMsg>" when I select only 1 field
    When I select  min 2 fields "<year>" from historical policy tab
    Then I verify the search function
    And I logoff from client adviser portal
    @SprintScenarios
    Examples:
      | year  | warMsg                                  |
      | 2012  | Please provide one more search criteria |

  Scenario Outline: CCI-5456 - Initiate Bulk Add Endorsement Text and CCI-6884 Approve Bulk Add Endorsement Text
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    When I click new button to bulk add endorsement text
    And select the details "<startDate>" "<productID>" "<clientCategory>" proceed to add endorsement text
    And add endorsement text "<endorsementText>" and comments "<comments>"
    And I capture the policy ID
    And I validate the "<pendingStatus>"
    And I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    And I search the deactivate endorsementId from "<queue>"
    When select the deactivate EndorsementID
    And reject the request and submit with "<rejComments>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    When select deactivate endorsementID to proceed deactivation of the endorsement
    And Click UW Continue button
    And Click UW Continue button
    Then enter comments "<comments>" and click finish
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    And I search the deactivate endorsementId from "<queue>"
    When select the deactivate EndorsementID
    And approve the request and submit
    Then I logout of the application
    @SprintScenarios
    Examples:
      | startDate   | productID | clientCategory | endorsementText | comments | pendingStatus    | queue                        | rejComments |
      | currentdate | BTV-001   | Ambulance      | hello           |  comm    | PENDING-APPROVAL | Endorsement Management Queue | test        |


  @SprintScenarios
  Scenario: CCI-1158 - Delegation of Client360 business rules
    Given I have the URL of VMIA
    When User when logs in as "Adminstartors"
    Then I logout Pegadevstudio application
#    Given I have the URL of VMIA
#    When User when logs in as "BusinessManager"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "InsightAndAnalytics"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "InsuranceAdmin"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "PortFolioManager"
#    Then I logout of the application