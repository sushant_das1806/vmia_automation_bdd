Feature: It covers, all the automatable scenarios from Sprint 3

  Scenario Outline: VAMP motor vehicle claims and create final payment and close the claim
  Given I have the URL of VMIA
  Given User when logs in as "TriageOfficer"
  Then I want to make a claim from claims officer
  Then Search and select Oraganization "<OrgName>"
  And Select contact from contact "<contact>"
  And I enter the date and time of loss
  And I select the type of "<loss>"
  And Click Continue button continuously for 3 times
  And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
  And Click Continue button
  When I provide loss details "<lossdetails>"
  And Click Continue button
  And Click Continue button
  And I check the declaration for Claims
  Then Click Finish button
  Then I validate the "<finalstatus>" for claims
  And Extarct the claim number
  And I click the exit button in claim
  Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
  Then verify "<finalstatustriage>"
  Then I logout of the application
  Given I have the URL of VMIA
  When User when logs in as "PortfolioManager"
  And Open the claim from "Unassigned Claims" queue
  And Transafer the claim to user "Claims Officer 1 R3"
  Then I logout of the application
  Given I have the URL of VMIA
  When User when logs in as "ClaimsOfficer"
  Then I open claim unit from worklist
  Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
  Then create payment with "Single" "26-BI settlement" "100" "10/08/2021" and mark it as final
  And I logout of the application
  Given I have the URL of VMIA
  When User when logs in as "Finance"
  And Open case from manualpayments and approve the same
  Then I logout of the application
  Given I have the URL of VMIA
  When User when logs in as "ClaimsOfficer"
  Then I open the claim unit from Claims portal
  Then I validate the "<finalstatus>" for claims
  Then I logout of the application

  @SprintScenarios03
  Examples:
  | loss              | lossdetails | finalstatus        | AccountName | BSB    | AccountNumber | BankName          | EmailID     | OrgName        | finalstatustriage         | triagecasestatus | payableamount| outstandingamount| recoverableamount|
  | Motor outsourced  | outsourced  | RESOLVED-COMPLETED | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | Katia_Accaputo | PENDING-CLAIM COMPLETION  | PENDING-TRIAGE   | 5000         | 300              | 200              |


  Scenario Outline: Adhoc invoice payment and refund process
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I add a new Adhoc Invoice "<OrgName>"
    And Proceed by selecting Organization and contact
    And Click Continue button
    And Add details "<policyNumber>" "<Product>" "<Premium>" "<GST>" and "<StampDuty>"
    And Click Continue button
    And I check the declaration for Adhoc Invoice
    Then Click Finish button
    And I validate the "<status>"
    And Extract the case ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "HeadOfInsurance"
    When I search the case id in "Adhoc Invoice Queue" and process it
    When Approve the request and submit
    And I validate the "<headstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    Then I open the "Pending finance" workbasket in the finance portal
    When Filter the caseID and click on the record
    And I enter Manual processing reason "<reason>" and received date and then click on submit
    And I validate the "<finalstatus>"
    Then I logout of the application

    @SprintScenarios03
    Examples:
      | policyNumber | Product | Premium | GST | StampDuty | status          | headstatus     | OrgName    | finalstatus      |
      | V02595r      | Nexna   | 100.4   | 100 |   3475648 | PENDING-APPROVAL| PENDING-PAYMENT| Regression | RESOLVED-MANUALLY|


  @SprintScenarios03
  Scenario: Accessibility of options for Insurance Team user profiles
#    Given I have the URL of VMIA
#    When User when logs in as "ClientAdvisoryManager"
#    Then I open the "Underwriting" workbasket in the finance portal
#    When verify the VMIA job role in "ClientAdvisoryManager"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "Underwriter"
#    Then I open the "Underwriting" workbasket in the finance portal
#    When verify the VMIA job role in "Underwriter"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "SeniorUnderwriterConstruction"
#    Then I open the "Underwriting" workbasket in the finance portal
#    When verify the VMIA job role in "SeniorUnderwriterConstruction"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "MIPortfolioManager"
#    Then I open the "Underwriting" workbasket in the finance portal
#    When verify the VMIA job role in "MIPortfolioManager"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "CasualityPortfolioManager"
#    Then I open the "Underwriting" workbasket in the finance portal
#    When verify the VMIA job role in "CasualityPortfolioManager"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "PropertyPortfolioManager"
#    Then I open the "Underwriting" workbasket in the finance portal
#    When verify the VMIA job role in "PropertyPortfolioManager"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "PortfolioManagerConstruction"
#    Then I open the "Underwriting" workbasket in the finance portal
#    When verify the VMIA job role in "PortfolioManagerConstruction"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "SeniorTechnicalSpecialist"
#    Then I open the "Underwriting" workbasket in the finance portal
#    When verify the VMIA job role in "SeniorTechnicalSpecialist"
#    Then I logout of the application
#    Given I have the URL of VMIA
#    When User when logs in as "HeadOfInsurance"
#    Then I open the "Underwriting" workbasket in the finance portal
#    When verify the VMIA job role in "HeadOfInsurance"
#    Then I logout of the application
    ### Below user has no records in UnderwriterQueue
    Given I have the URL of VMIA
    When User when logs in as "ChiefInsuranceOfficer"
    Then I open the "Underwriting" workbasket in the finance portal
    When verify the VMIA job role in "ChiefInsuranceOfficer"
    Then I logout of the application
    ## Client Advisor missing here


