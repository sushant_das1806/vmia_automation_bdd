Feature: It covers, all the automatable scenarios from Sprint 6

  Scenario Outline: CCI-3168 - Public & Products Liability product and perform an Endorsement and then Cancel the single policy with Ability to cancel/void previous year policies
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    When I Click new button to cancel "<single>" policy
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    When select the policy and click finish
    And Click UW Continue button
    And Enter cancel policy details "<reason>"
    Then I accept the quote and click finish
    And I capture the policy ID
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Approve the quote and submit
    And I validate the "<finalstatus>"
    And I logout of the application
    @CCI-3168
    Examples:
      | single        | bundle        | reason        |  OrgName         | pendingstatus        | finalstatus    |
      | Cancel policy | Cancel bundle | Property sold |  E&W Regression  | PENDING-UNDERWRITING | PENDING-REFUND |

  Scenario Outline: CCI-3168 - Public & Products Liability product and perform an Endorsement and then Cancel the bundle policy with Ability to cancel/void previous year policies
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click CreateNew button
    And Enter the Orgdetails "Community Service Organisation" "Department of Health" "tester" "8508356502" "Sydney " and "Department of Health"
    And I note down the PayWay number ID
    And I logoff from client adviser portal
    Given I have the URL of VMIA
    And User when logs in as "Finance"
    Then I open the "Payway Account" workbasket in the finance portal
    And I open the Payway Case and enter the Payway Number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    And Click New and Select "Manage contact"
    Then Search the contact using contactid "CONT-27655"
    And Add the contact to Organzation to the contact "" "Insurance;Finanace"
    And Click New and Select "Create policy bundle"
    And Select orgnazation by searching clientsegment "<clientCategory>" bundle type "<bundleType>" and orgname
#    And Select orgnazation by searching clientsegment "Community Service Organisation" and orgname
    Then validate the product list "<productList>"
    Then Add the unincorproted orgnization "<UnincorprateOrg>" and policyEffectiveDate "<PolicyeffecveDate>" and finish the policy
    Then Verify case status "<finalstatus>" and capture the case id
    When Click New and Select "Cancel bundle" from Cancel Policy option
    And  search the organization using clientSegment "<clientCategory>" and Orgnazation
    And  Select the contact and Product "CSO" click finish
    Then Cancel the bundle policy using "<IsFullTermCancel>" and "<Cancelreason>"
    Then I validate the "<finalstatus>" after bundle policy cancellation
    And I logoff from client adviser portal

    @SprintScenarios
    Examples:
      | UnincorprateOrg | IsFullTermCancel | PolicyeffecveDate  | Cancelreason  | finalstatus        | bundleType | clientCategory                  | productList  |
      | Scotland PNP    | Yes              | current            | Property sold | RESOLVED-COMPLETED | CSO        | Community Service Organisation  | CSOEducation |

  Scenario Outline: CCI-5065 - Legal Panel - Solicitor's list
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    Then I am logged in to the client adviser portal
    When Select manage components and go to VMIA solicitor
    And Add VMIA solicitor details "<Name>" "<Email>" "<PhoneNumber>" "<Personnel>" and "<Address>"
    And Select the Solicitor record and change the status "Inactive" and Empanelment "Off-panel"
    Then Verify and validate the same record in Inactive tab with status "No"
    Then I logout of the application

    @SprintScenarios
    Examples:
      | Name    | Email            | PhoneNumber  | Personnel       | Address  |
      | Scoti   | Scoti.nz@cig.com | 8735652453   | Special Counsel | Victoria |

  @SprintScenarios
  Scenario Outline: CCI-3940 - Expand claims transfer feature claims admin role
    Given I have the URL of VMIA
    Given User when logs in as "TriageOfficer"
    Then I want to make a claim from claims officer
    Then Search and select Oraganization "<OrgName>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    And Add Incident details "Theft" "Test" "Address"
    And Click Continue button
    And Enter the FineArts Details for claim "Artist" "TitleofWork" "Dimension" "Owner" "5000"
    And Select Incident report details in claim "No" "No"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    And Click Continue button
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And I extract the claim unit number
    And I click the exit button in claim
    Then I am logged in to the claims officer portal
    Then I search claim file and proceed for duplicate check "<triagecasestatus>" "<OrgName>"
    And I extract claim file number
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "PortfolioManager"
    And Open the claim from "Unassigned Claims" queue
    And Transfer the claim to user "Claims Admin"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClaimsAdmin"
    And I am logged in to the claims admin portal
    And I select the Bulk transfer to transfer the claims
    And Transfer the claims both the ways "Claims Admin" "Claims Officer 1 R3"
    Then Verify and Validate the Claim file record after transfer
    Then I logout of the application
    Examples:
      | loss                                    | AccountName |  BSB   | AccountNumber | BankName          | EmailID     |  finalstatus        | finalstatustriage        | triagecasestatus |OrgName        |payableamount|outstandingamount|recoverableamount|
      | Fine Art Exhibitions - Domestic Transit | Flinders    | 123236 |       3475648 | Commonwealth Bank | comm@gm.com |  RESOLVED-COMPLETED | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   |Auto claim G&E |5000         |300              |200              |

  Scenario Outline: CR-PPL(P) - Construction Risks – Liability Project Insurance Policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Enter the Policy Effective date "27/08/2021" and Expiration date "8/08/2085"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
#    And Enter the mandatoryInformation for Construnction Liablity metro tunnel finaliseQuoate and Submit
    And I Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click the exit button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<cancelPremium>" and "<UnderwritingNotes>" in cancellation
    Then I submit the case to get routed to the user
    And I check the case status to be "PENDING-REFUND"
    Then I logout of the application
    @SprintScenarios
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname               | product                                                 | status         | pendingstatus        | uwstatus                 | finalstatus     | EnableOrDisableProrate| cancelPremium | IsCancelFullTerm | cancellationdate | Cancelreason |
      | No      |  200      | Hello VMIA         | Automation_Transaport | Construction Risks - Liability Project Insurance Policy | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                   | -150          | yes              | 22/07/2021       | Property sold|

  Scenario Outline: CR-MD(P) - Construction Risks – Material Damage Project Insurance Policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Enter the Policy Effective date "27/08/2021" and Expiration date "8/08/2085"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
#    And Enter the mandatoryInformation for Construnction Liablity metro tunnel finaliseQuoate and Submit
    And I Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click the exit button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<cancelPremium>" and "<UnderwritingNotes>" in cancellation
    Then I submit the case to get routed to the user
    And I check the case status to be "PENDING-REFUND"
    Then I logout of the application
    @SprintScenarios
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname               | product                                                       | status         | pendingstatus        | uwstatus                 | finalstatus     | EnableOrDisableProrate| cancelPremium | IsCancelFullTerm | cancellationdate | Cancelreason |
      | No      |  200      | Hello VMIA         | Automation_Transaport | Construction Risks - Material Damage Project Insurance Policy | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                   | -150          | yes              | 22/07/2021       | Property sold|