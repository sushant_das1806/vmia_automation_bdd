Feature: It covers, all the automatable scenarios from Sprint 7

  Scenario Outline: MI-004, Surge Support Medical Indemnity insurance policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    Then I Add Additional Deductibles "<name>" "<amount>" "<description>"
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<cancelPremium>" and "<UnderwritingNotes>" in cancellation
    Then I submit the case to get routed to the user
    And I check the case status to be "PENDING-REFUND"
    Then I logout of the application

    @SprintScenarios
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname               | product                         | status         | pendingstatus        | uwstatus                 | finalstatus     | name | amount | description | cancelPremium | IsCancelFullTerm| cancellationdate| Cancelreason | EnableOrDisableProrate|
      | No      |     2000  | Hello VMIA        | Automation_Transaport | Surge Support Medical Indemnity | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Test |  345   |  VMIA       | -150          | No              | 17/09/2023      | Property sold| Yes                   |


  Scenario Outline: BTV-001-SCH, STP product, Business Travel - Cancel policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click "Yes" to continue
    And Click UW Continue button
    And Add travel records with the details "<destination>", number of students and number of staff
    And Click UW Continue button
    Then I enter the reuired details for Summary and review page
    Then I Click Finish button
    And I validate the "<uwstatus>"
    Then I accept the quote
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    When Approve the quote and submit
    And I check the case status to be "PENDING-REFUND"
    Then I logout of the application

    @SprintScenarios
    Examples:
      | orgname               | product         | destination| uwstatus                 | IsCancelFullTerm | cancellationdate| Cancelreason |
      | Automation_Transaport | Business travel | India      | PENDING-CLIENTACCEPTANCE |  No              | 25/05/2023      | Property sold|


  Scenario Outline: BTV-001-SCH, Non-STP product, Business Travel - Endorsement and Then cancel policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click "Yes" to continue
    And Click UW Continue button
    And Add travel records with the details "<destination>", number of students and number of staff
    And Click UW Continue button
    Then I enter the reuired details for Summary and review page
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<pendingstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    When I click on Case ID after filter
    When Now click on the approve quote
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    Then Select effectiveDate And Enter information about the policy
    And I capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    When Approve the quote and submit
    And I check the case status to be "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    When Approve the quote and submit
    And I check the case status to be "PENDING-REFUND"
    Then I logout of the application

    @SprintScenarios
    Examples:
      | orgname               | product         | destination| pendingstatus        | uwstatus                 | finalstatus     | IsCancelFullTerm | cancellationdate| Cancelreason |
      | Automation_Transaport | Business travel | India      | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No               | 25/05/2023      | Property sold|