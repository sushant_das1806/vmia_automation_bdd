Feature: Show endorsement text to internal/external user


  Scenario Outline: Start a new policy application for GPA with deviation and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I open the case for Review
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Add Dedctible for the professional Indeminity
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I open the case for Review
    And Enter the endorsement text "Endorsement"
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Add Dedctible for the professional Indeminity
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then I accept the quote
    And I verify the endorsement text
    And I validate the "<finalstatus>"
    Then I logout of the application

    @MPL4 @Auto_Law_Jus @Professional_Indemnity @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Professional Indemnity              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |


#
#
#  Scenario Outline: Start a new policy application for GPA with deviation and endorsement with client Acceptance
#Given I have the external URL of VMIA
#When I enter the "<username>" for azure
#And I enter "<password>" for azure
#And I click login button for azure
#And I Select the Organization "<orgname>"
#Then I want to start a new application for VMIA insurance
#And click on the select button of the "<product>"
#And Click UW Continue button
#And Click UW Continue button
#Then I Click Finish button
#And I capture the policy ID
#And I validate the "<status>"
#Then I logout of the application
#Given I have the URL of VMIA
#When User when logs in as "RiskAdvisor"
#Then I am logged in to the client adviser portal
#    And I open the case for Review
#And Select the Policy decision "<decsion>" and Submit the Application
#And I validate the "<pendingstatus>"
#Then I logoff from client adviser portal
#Given I have the URL of VMIA
#When User when logs in as "Underwriter"
#When I search the case id that is routed and I begin the case
#And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
#And Add Dedctible for the professional Indeminity
#And Submit the Quoate
#And I validate the "<uwstatus>"
#Then I logout of the application
#Given I have the external URL of VMIA
#When I enter the "<username>" for azure
#And I enter "<password>" for azure
#And I click login button for azure
#And I Select the Organization "<orgname>"
#Then I open the approved case
#Then I accept the quote
#And I validate the "<finalstatus>"
#And I save the policy number
#Then I logout of the application
#Given I have the external URL of VMIA
#When I enter the "<username>" for azure
#And I enter "<password>" for azure
#And I click login button for azure
#And I Select the Organization "<orgname>"
#And Select policy and Click on Update Policy
#And Click UW Continue button
#And Click UW Continue button
#Then I Click Finish button
#And I capture the policy ID
#And I validate the "<status>"
#Then I logout of the application
#Given I have the URL of VMIA
#When User when logs in as "RiskAdvisor"
#Then I am logged in to the client adviser portal
#    And I open the case for Review
#And Select the Policy decision "<decsion>" and Submit the Application
#And I validate the "<pendingstatus>"
#Then I logoff from client adviser portal
#Given I have the URL of VMIA
#When User when logs in as "Underwriter"
#When I search the case id that is routed and I begin the case
#And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
#    And Enter the endorsement text "Endorsement"
#And Add Dedctible for the professional Indeminity
#And Submit the Quoate
#And I validate the "<uwstatus>"
#Then I logout of the application
#Given I have the external URL of VMIA
#When I enter the "<username>" for azure
#And I enter "<password>" for azure
#And I click login button for azure
#And I Select the Organization "<orgname>"
#Then I open the approved case
#Then I accept the quote
#    And I verify the endorsement text
#And I validate the "<finalstatus>"
#Then I logout of the application
#
#@MPL4 @Auto_Law_Jus @Professional_Indemnity @PolicyCreation @clientacceptance
#Examples:
#| decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#| No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Professional Indemnity              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |


