Feature:EmRePs Bundle Policies

  Scenario Outline: Create a new EmRePs Bundle policy and then cancel it
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Click New and Select "Manage organisation"
    Then I provide organisation name "<Neworgname>"
    Then I want to create a new customer
    And Enter the Orgdetails "<ClientSegment>" "<OrgType>" "<GovenrmentDepartment>" "<Profession>" "<PhoneNumber>" "Sydney "
    And I note down the PayWay number ID
    And I close the current tab
    And Click New and Select "Manage contact"
#   Then Search the contact using contactid "CONT-27682"
    Then Search contact using name "<Firstname>" "<Lastname>"
    And Add the contact to Organzation to the contact "<Neworgname>" "Insurance;Finanace"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    And User when logs in as "Finance"
    Then I open the "Payway Account" workbasket in the finance portal
    And I open the Payway Case and enter the Payway Number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Click New and Select "Create policy bundle"
    And Select organization by searching "<ClientSegment>" and "<BundleType>" and "<Neworgname>"
    Then Enter the Policy Effective Date
    And I Click Finish button
    Then Verify case status for bundle policy "RESOLVED-COMPLETED"
    Then I note down the Package Case ID
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I validate the "PENDING-UNDERWRITING"
    And Now click on the approve quote
    And click submit button in UpdateOrganization Screen
    And I validate the "PENDING-CLIENTACCEPTANCE"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "PENDING-PAYMENT"
#    Then Verify case status for bundle policy "PENDING-PAYMENT"
    Then I logoff from client adviser portal
    And I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<Neworgname>"
    And Verify all the policies are created and more action is disabled for "EmRePsValidate"
    And I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    When Click New and Select "Cancel bundle" from Cancel Policy option
    And search the organization using clientSegment "<ClientSegment>" and "<Neworgname>"
    And  Select the contact and Product "EmRePs" click finish
    And Click Continue button
    Then click more actions and cancel the policy using the following information
      |<IsFullTermCancel>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    And I validate the "PENDING-REFUND"
    Then I logout of the application

@CCI @CCISprint2
    Examples:
      | Neworgname                      | ClientSegment      |OrgType    | GovenrmentDepartment                              | Profession | PhoneNumber |  BundleType |IsFullTermCancel|Cancelreason |
      |TestAuto_Environment&Water_Bundle|Environment & Water |Core Client|Department of Environment, Land, Water and Planning| Test PRof  | 9876577323  |EmRePs       |Yes             |Property sold|