Feature: Professional And Medical Indeminity claims test

  @MPL4 @Indeminity @US-8003
  Scenario Outline: Creating Professional Ideminity claim
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then I am logged in to the insurance portal
    Then I want to make a claim from TriageOfficer
    Then Search and select Oraganization "<Organization>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Enter the loss details "<IncidentReportDate>" "Test" "ThirdPary"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    Examples:
      |Organization  | TriageOfficer | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      |QAcemetery  | TriageOfficer   | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Professional indemnity  | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |



    @MPL4 @Indeminity @US-4024
    Scenario Outline: Creating medical Ideminity claim
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then I am logged in to the insurance portal
    Then I want to make a claim from TriageOfficer
    Then Search and select Oraganization "<Organization>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Select Urgency level "NO" and Claim contact "Yes" of the claim
    And Click Continue button
    And Select the claim source dropdown "Incident report"
    And Click Continue button
    And Enter the patient details "Test" "Regression" "Male" "12/may/1999" "MRNO" "Public"
    And Click Continue button
    And Select patient whether patient is claimant "Yes"
    And Click Continue button
    And Enter the claims incident details "incidentDes" "Factaccount" "Dermatology" "Inpatient" "Yes"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    Then I logout of the application

    Examples:
    |Organization         | TriageOfficer | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
    |Rahara Cemetery Trust| TriageOfficer   | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Medical indemnity| Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |


    @MPL4 @Indeminity @US-6103 @US-6105
    Scenario Outline: Creating medical Ideminity claim and add solicitor for litigation
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then I am logged in to the insurance portal
    Then I want to make a claim from TriageOfficer
    Then Search and select Oraganization "<Organization>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Select Urgency level "NO" and Claim contact "Yes" of the claim
    And Click Continue button
    And Select the claim source dropdown "Incident report"
    And Click Continue button
    And Enter the patient details "Test" "Regression" "Male" "12/may/1999" "MRNO" "Public"
    And Click Continue button
    And Select patient whether patient is claimant "Yes"
    And Click Continue button
    And Enter the claims incident details "incidentDes" "Factaccount" "Dermatology" "Inpatient" "Yes"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    And Open the claim from "Triage Claims" queue
   	And review claim and check dupilicate
   	And Select claim category "C" "Yes" "NO"
   	And Enter the Solicitor details "Defendant" "PavanAutomation" "14/May/2021" "PlaintiffSolicitr" "Test"
   	And Enter the Litigatation Status "Judgement" "County" "12345"  "14/May/2021" "14/May/2021"
   	And Enter the Defendent details "Test"
   	And Enter the Event details "Inquest" "14/May/2021" "resoultioncoments"
   	And Enter the Resoultion details "Dismissal" "14/May/2021" "1500" "Defendant pays" "resoultionMonetery"
   	Then I logout of the application
   	When User when logs in as "PortfolioManager"
   	And Open the claim from "Unassigned Claims" queue
   	And Validated the medical claim indemnity case status "PENDING-TRIAGE"
   	Then Transafer the claim to user "Claims Officer 1 R3"
   	And Validated the medical claim indemnity case status "PENDING-INVESTIGATION"
   	Then I logout of the application

    Examples:
    |Organization         | TriageOfficer | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
    |Rahara Cemetery Trust| TriageOfficer   | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Medical indemnity| Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

    @MPL4 @Indeminity @claims @US-6103
    Scenario Outline: Creating medical Ideminity claim and while traige the claim and select claim category c
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then I am logged in to the insurance portal
    Then I want to make a claim from TriageOfficer
    Then Search and select Oraganization "<Organization>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Select Urgency level "NO" and Claim contact "Yes" of the claim
    And Click Continue button
    And Select the claim source dropdown "Incident report"
    And Click Continue button
    And Enter the patient details "Test" "Regression" "Male" "12/may/1999" "MRNO" "Public"
    And Click Continue button
    And Select patient whether patient is claimant "Yes"
    And Click Continue button
    And Enter the claims incident details "incidentDes" "Factaccount" "Dermatology" "Inpatient" "Yes"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    And Open the claim from "Triage Claims" queue
   	And review claim and check dupilicate
   	And Select claim category "C" "No" "NO"
   	And Validated the medical claim indemnity case status "PENDING-CLAIM COMPLETION"
   	Then I logout of the application

    Examples:
    |Organization         | TriageOfficer | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
    |Rahara Cemetery Trust| TriageOfficer   | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Medical indemnity| Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |



    @MPL4 @Indeminity @US-6103
    Scenario Outline: Creating medical Ideminity claim with Claim category As A
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then I am logged in to the insurance portal
    Then I want to make a claim from TriageOfficer
    Then Search and select Oraganization "<Organization>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Select Urgency level "NO" and Claim contact "Yes" of the claim
    And Click Continue button
    And Select the claim source dropdown "Incident report"
    And Click Continue button
    And Enter the patient details "Test" "Regression" "Male" "12/may/1999" "MRNO" "Public"
    And Click Continue button
    And Select patient whether patient is claimant "Yes"
    And Click Continue button
    And Enter the claims incident details "incidentDes" "Factaccount" "Dermatology" "Inpatient" "Yes"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    And Open the claim from "Triage Claims" queue
   	And review claim and check dupilicate
   	And Select claim category "A" "Yes" "NO"
   	And Validated the medical claim indemnity case status "PENDING-CLAIM COMPLETION"
   	Then I logout of the application

   Examples:
    |Organization         | TriageOfficer | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
    |Rahara Cemetery Trust| TriageOfficer   | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Medical indemnity| Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |



    @MPL4 @Indeminity  @US-6103
	Scenario Outline: Creating medical Ideminity claim with Claim category As B
    Given User enters the Internal URL from Browser
    When User when logs in as "<TriageOfficer>"
    Then I am logged in to the insurance portal
    Then I want to make a claim from TriageOfficer
    Then Search and select Oraganization "<Organization>"
    And Select contact from contact "<contact>"
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    And Click Continue button
    And Select Urgency level "NO" and Claim contact "Yes" of the claim
    And Click Continue button
    And Select the claim source dropdown "Incident report"
    And Click Continue button
    And Enter the patient details "Test" "Regression" "Male" "12/may/1999" "MRNO" "Public"
    And Click Continue button
    And Select patient whether patient is claimant "Yes"
    And Click Continue button
    And Enter the claims incident details "incidentDes" "Factaccount" "Dermatology" "Inpatient" "Yes"
    And Click Continue button
    And Click Continue button
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    And Open the claim from "Triage Claims" queue
   	And review claim and check dupilicate
   	And Select claim category "B" "Yes" "NO"
   	And Validated the medical claim indemnity case status "PENDING-CLAIM COMPLETION"
   	Then I logout of the application

   Examples:
    |Organization         | TriageOfficer | username   | password    | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
    |Rahara Cemetery Trust| TriageOfficer   | Operator02 | abhinav@123 | TriageClaimsOfficer1        | Welcome@1234                | Medical indemnity| Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | VIC   |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | VIC         |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |

    @Recoveries @RecoveryhistoryChange @US-14006
    Scenario Outline: Initiate a Claim for GPA and add recoverables reinsurance from claims officer
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>"
    Then I am logged in to the insurance portal
    Then I want to make a claim
    And I enter the date and time of loss
    And I select the type of "<loss>"
    And Click Continue button
    When I select the business row and verify listed policies for "<loss>"
    And Click Continue button
    Then I am routed to Insured Client Details page and validate the fields
    And Click Continue button
    Then I am routed to Client Banking Account details page
    And "<AccountName>" "<BSB>" "<AccountNumber>" "<BankName>" "<EmailID>" is fed
    And Click Continue button
    Then I provide claimant details "<studentname>" "<dateofbirth>" "<addressline1>" "<addressline2>" "<suburb>" "<state>" "<postcode>" "<parentname>" "<relationship>" "<parentaddressline1>" "<parentaddressline2>" "<parentsuburb>" "<parentstate>" "<parentpostcode>" "<telephone>" "<parentemail>"
    And Click Continue button
    Then I provide accident details "<accidentdescription>" "<accidentreportedto>" "<injuriessustained>"
    And Click on Continue button for "4" from claims pages
    And I check the declaration for Claims
    Then Click Finish button
    Then I validate the "<finalstatus>" for claims
    And Extarct the claim number
    Then I logout of the application
    Then I close the external link
    Given I have the URL of VMIA
    When User when logs in as "<TriageOfficer>"
    Then I search the claim file and proceed for duplicate check "<triagecasestatus>" "<finalstatustriage>"
    Then I click its not a duplicate
    Then verify "<finalstatustriage>"
    Then I logout of the application
    When User when logs in as "<PortfolioManager>"
    Then I transfer the claim from portfolio manager and check skill based transfer "<invaliduser>" "<usertotransfer>" "<beforetransferclaimstatus>" "<statusaftertransfer>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    Then I prepare estimate "<payableamount>" "<outstandingamount>" "<recoverableamount>"
    Then create payment with "Single" "27-Independent medical exam" "100"
    Then create Recovery estimate  "RecoveryType" "Recoveryestimate"
    And Add create recovery "estimiate" "PartyType" "firstname" "lastname" "recoveryamt"
    And Extract the recovery id
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Finance"
    And Open and approve the recovery
    Then I logout of the application
    When User when logs in as "<ClaimsOfficer>"
    Then I open claim unit from worklist
    And Verify the recovery history

   @Regression2_MLP2_Set3 @Golive_26_Today
    Examples:
      | TriageOfficer | PortfolioManager | ClaimsOfficer | recoverabletype | name     | resolvestatus      | awaitreason | approvestatus         | payableamount | outstandingamount | recoverableamount | clientdecision | claimsofficeruser | claimsofficerpass | invaliduser | statusaftertransfer   | beforetransferclaimstatus | usertotransfer   | portfoliousername         | portfoliopassword | finalstatustriage        | triagecasestatus | username                       | password      | triageclaimsofficerusername | triageclaimsofficerpassword | loss                    | AccountName | ChangedAccountName | BSB    | AccountNumber | BankName          | EmailID     | studentname | dateofbirth | addressline1 | addressline2 | suburb  | state    | postcode | parentname        | relationship | parentaddressline1 | parentaddressline2 | parentsuburb | parentstate | parentpostcode | telephone  | parentemail | accidentdescription | accidentreportedto | injuriessustained | finalstatus        |
      | TriageOfficer | PortfolioManager | ClaimsOfficer | Reinsurance     | Lowanna4 | RESOLVED-COMPLETED | NA          | PENDING-INVESTIGATION |           100 | File review       |                20 | approve        | claimsofficer1    | rules             | operator01  | PENDING-INVESTIGATION | PENDING-TRIAGE            | Claims Officer 1 R3 | ClaimsPortfolioManager_AT | rules             | PENDING-CLAIM COMPLETION | PENDING-TRIAGE   | pavan.gangone@areteanstech.com | Areteans@1981 | TriageClaimsOfficer1R3      | rules                       | Group Personal Accident | Flinders    | Steve              | 123236 |       3475648 | Commonwealth Bank | comm@gm.com | automation  | 6/05/2008   | Street1      | locality1    | suburb1 | Victoria |     2654 | parent automation | Father       | Street1            | locality1          | suburb1      | Victoria    |           2654 | 0978765467 | a@b.com     | Crossing the road   | Headmaster         | Broken Leg        | RESOLVED-COMPLETED |



