Feature:Property

  Scenario Outline: Start a new policy application for property with deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>" for "<product>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the "<startdate>" of the insurance application
    And I select the property "<value>" as "<propertyvalue>"
    And Click UW Continue button
    And I validate the property value "<value>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Then I close the external link
    Given I have the URL of VMIA
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I validate the premium for property "<propertyvalue>" "<minimumvalue>" "<percentage>" "<gst>" "<stampduty>" "<days>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set1_golive_26
    Examples:
      | username                       | password      | product | value          | propertyvalue | minimumvalue | percentage | gst | stampduty | startdate | client_number | status                   | pendingstatus        | days | uwusername     | uwpassword  | paymentstatus   | name     |
      | pavan.gangone@areteanstech.com | Areteans@1981 | PRO     | Up to $250,000 |        100000 |          950 |          1 | 0.1 |       0.1 | 1/07/2020 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |  365 | underwriter_AT | Welcome@321 | PENDING-PAYMENT | Automation PAP |