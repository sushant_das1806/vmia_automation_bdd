Feature: Journey Auto Pricing feature

  Scenario Outline: Start a new Journey policy with no deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @Journey @STPNewBusiness @NoDeviation @EnvironmentAndWaters @InsuranceRegression
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Auto E&w      | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPNewBusiness @NoDeviation @LawAndJustice
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      |   Auto Law & Jus | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPNewBusiness @NoDeviation @Education
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | AutoEdu       | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPNewBusiness @NoDeviation @PropertyAndPlan
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | AutoProp&plan| JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPNewBusiness @NoDeviation @Cemetry
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Autom_Cemetry | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPNewBusiness @NoDeviation @DepartmentalDivision
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                                     | paymentstatus   |
      | Automation_DepartmentalDivision | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPNewBusiness @NoDeviation @CommunityHeathcare
    Examples:
      | orgname       | product | fte    | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Automation_CS_CommunityHeathcare | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPNewBusiness @NoDeviation @MedicalResearchPP
    Examples:
      | orgname                          | product | fte    | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Automation_MedicalResearchPP | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPNewBusiness @NoDeviation @PublicHospital
    Examples:
      | orgname                         | product | fte    | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      |Automation_PublicHospital | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPNewBusiness @NoDeviation @SpecialistAgencies
    Examples:
      | orgname                          | product | fte    | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Automation_Specialist_Agencies | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPNewBusiness @NoDeviation @SpecialistAgenciesPP
    Examples:
      | orgname                          | product | fte    | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Automation_Specialist_AgenciesPP | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  Scenario Outline: Start a new Journey policy with deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    And I save the policy number
    Then I logout of the application
        #### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
        ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
#  @Journey @NewBusiness @Deviation @EnvironmentAndWaters
#  Examples:
#  | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
#  |Auto E&w          | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
#  @Journey @NewBusiness @Deviation @LawAndJustice
#  Examples:
#  | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
#  | Auto Law & Jus   | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
#  @Journey @NewBusiness @Deviation @Education
#  Examples:
#  | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
#  | AutoEdu         | JRN    | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
#  @Journey @NewBusiness @Deviation
#  Examples:
#  | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
#  | Automation PAP | JRN   | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
#  @Journey @NewBusiness @Deviation @Cemetry
#  Examples:
#  | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
#  | Autom_Cemetry   | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
#  @Journey @NewBusiness @Deviation @DepartmentalDivision
#  Examples:
#  | orgname                        | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
#  | Automation_DepartmentalDivision| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
#  @Journey @NewBusiness @Deviation @CommunityHeathcare
#  Examples:
#  | orgname                         | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
#  | Automation_CS_CommunityHeathcare| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @NewBusiness @Deviation @MedicalResearchPP  @All
    Examples:
      | orgname                       | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_MedicalResearchPP  | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @NewBusiness @Deviation @PublicHospital
    Examples:
      | orgname                         | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_PublicHospital| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @NewBusiness @Deviation @SpecialistAgencies
    Examples:
      | orgname                         | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_Specialist_Agencies| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @NewBusiness @Deviation @SpecialistAgenciesPP
    Examples:
      | orgname                         | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_Specialist_AgenciesPP| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |

  @MLP3Sprint1 @UW @JRN @PropAndPlan @NewApplication @RegMLP312345
  Scenario Outline: Start a new Journey policy with no deviation and then endorse it - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Organization "<orgname>" for "<product>"
    Then I select update policy from more actions
    Then I enter updated values of "<updatedfte>" and "<startdate>"
    And I select no previous journey insurance
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium of Journey for "<fte>" "<updatedfte>" for "<startdate>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

    @Journey @STPEndorsement @NoDeviation @EnvironmentAndWaters
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Auto E&w      | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPEndorsement @NoDeviation @LawAndJustice
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Auto Law & Jus | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPEndorsement @NoDeviation @Education
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | AutoEdu       | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPEndorsement @NoDeviation @PropertyAndPlan
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | AutoProp&plan| JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPEndorsement @NoDeviation @Cemetry
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Autom_Cemetry | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPEndorsement @NoDeviation @DepartmentalDivision
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Automation_DepartmentalDivision | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @STPEndorsement @NoDeviation @CommunityHeathcare
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Automation_CS_CommunityHeathcare | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @NonSTPEndorsement @NoDeviation @MedicalResearchPP
    Examples:
      | orgname                     | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Automation_MedicalResearchPP| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @NonSTPEndorsement @NoDeviation @PublicHospital
    Examples:
      | orgname                  | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Automation_PublicHospital| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @NonSTPEndorsement @NoDeviation @SpecialistAgencies
    Examples:
      | orgname                         | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Automation_Specialist_Agencies| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Journey @NonSTPEndorsement @NoDeviation @SpecialistAgenciesPP
    Examples:
      | orgname                         | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
      | Automation_Specialist_AgenciesPP| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  @MLP3Sprint1 @UW @JRN @PropAndPlan @NewApplication @Test2 @RegMLP312345  @newwwww
  Scenario Outline: Start a new Journey policy with deviation through Client portal and then endorse it through Client Adviser Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    And I save the policy number
    Then I logout of the application
        #### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
        ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
        ##### Client Adviser - Endorsement
    Given I have the URL of VMIA
    When I enter the "<clientadviserusername>"
    And I enter "<clientadviserpassword>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I search and select org using "<orgname>" and "<zipcode>"
    Then I select the policy
    And Click UW Continue button
    Then I enter updated values of "<updatedfte>" and "<startdate>"
    And I select no previous journey insurance
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<UWStatus>"
    And I note down the case ID
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then I validate the endorsement premium of Journey for "<fte>" "<updatedfte>" for "<startdate>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logoff from client adviser portal

    @Journey @Endorsement @Deviation @EnvironmentAndWaters
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      |Auto E&w          | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Endorsement @Deviation @LawAndJustice
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Auto Law & Jus  | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Endorsement @Deviation @Education
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | AutoEdu         | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Endorsement @Deviation @PropertyAndPlan
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation PAP   | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Endorsement @Deviation @Cemetry
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Autom_Cemetry   | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Endorsement @Deviation @DepartmentalDivision
    Examples:
      | orgname                         | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_DepartmentalDivision | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Endorsement @Deviation @CommunityHeathcare
    Examples:
      | orgname                         | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_CS_CommunityHeathcare| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Endorsement @Deviation @MedicalResearchPP
    Examples:
      | orgname                         | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_MedicalResearchPP| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Endorsement @Deviation @PublicHospital
    Examples:
      | orgname                         | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_PublicHospital| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Endorsement @Deviation @SpecialistAgencies
    Examples:
      | orgname                         | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_Specialist_Agencies| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Endorsement @Deviation @SpecialistAgenciesPP
    Examples:
      | orgname                         | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_Specialist_AgenciesPP| JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |



  @MLP3Sprint1 @UW @JRN @PropAndPlan @NewApplication @Cancellation
  Scenario Outline: Start a new Journey policy with no deviation and then cancel it - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Organization "<orgname>" for "<product>"
    Then I cancel a policy from more actions
    Then I check the case status to be "<UWStatus>"
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile
    Then I logout of the application

    @Journey @Cancellation @NoDeviation @EnvironmentAndWaters
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      | Auto E&w      | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |
    @Journey @Cancellation @NoDeviation @LawAndJustice
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      | Auto Law & Jus| JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |
    @Journey @Cancellation @NoDeviation @Education
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      | AutoEdu       | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |
    @Journey @Cancellation @NoDeviation  @PropertyAndPlan
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      | Automation PAP | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |
    @Journey @Cancellation @NoDeviation @Cemetry
    Examples:
      | orgname       | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      | Autom_Cemetry | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |
    @Journey @Cancellation @NoDeviation @DepartmentalDivision
    Examples:
      | orgname                         | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      | Automation_DepartmentalDivision | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |
    @Journey @Cancellation @NoDeviation @CommunityHeathcare
    Examples:
      | orgname                          | product | fte | updatedfte | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      | Automation_CS_CommunityHeathcare | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |



  Scenario Outline: Start a new Journey policy with deviation through Client portal and then cancel it
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and total number of "<fte>"
    And I select no previous journey insurance
    And Click UW Continue button
    And I validate the total number of "<fte>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    Then I logout of the application
        #### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
        ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of Journey for "<fte>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I click the exit button
    And I Select the Organization "<orgname>" for "<product>"
    Then I cancel a policy from more actions
    Then I check the case status to be "<UWStatus>"
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile
    Then I logout of the application
    @Journey @Cancellation @Deviation @EnvironmentAndWaters
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      |Auto E&w          | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Cancellation @Deviation @LawAndJustice
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Auto Law & Jus  | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Cancellation @Deviation @Education
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | AutoEdu         | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Cancellation @Deviation @PropertyAndPlan
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation PAP   | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Cancellation @Deviation @Cemetry
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Autom_Cemetry   | JRN     | 5000 |       6500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Cancellation @Deviation @DepartmentalDivision
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_DepartmentalDivision | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Cancellation @Deviation @CommunityHeathcare
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_CS_CommunityHeathcare | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Cancellation @Deviation @MedicalResearchPP
    Examples:
      | orgname                     | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_MedicalResearchPP| JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Cancellation @Deviation @PublicHospital
    Examples:
      | orgname                   | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_PublicHospital | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Cancellation @Deviation @SpecialistAgencies
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_Specialist_Agencies | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @Journey @Cancellation @Deviation @SpecialistAgenciesPP
    Examples:
      | orgname          | product | fte  | updatedfte | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_Specialist_AgenciesPP | JRN     | 200 |        500 |          7 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter_1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
