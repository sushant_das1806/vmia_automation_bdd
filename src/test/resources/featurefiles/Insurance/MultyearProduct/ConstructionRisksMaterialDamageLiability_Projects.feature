Feature: Construction Risks - Material Damage and Liability (Projects)

  Scenario Outline: Construction multiyear New product
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Enter the Policy Effective date "27/08/2021" and Expiration date "8/08/2085"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the mandatoryInformation of finalise Quote and Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    ### Added the below step
    And I logout of the application

    @MPL5_Sprint1 @Business&Industry @Construction_multiyear @PolicyCreation @clientacceptance @RegressionMLP5Set2
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname|product                                                      | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|
      | No      |  200      | Hello VMIA        |AutoB&I | Construction Risks - Material Damage and Liability (Projects)| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |

  Scenario Outline: Validating the Effective Date of multiyear product
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Enter the Policy Effective date "01/06/2013" and Expiration date "30/07/2099"
    And Click UW Continue button
    And Verify the error message "Your selected policy dates cannot be covered by this policy type. Please contact VMIA for more details." in information screen
    And Enter the Policy Effective date "01/07/2013" and Expiration date "30/08/2099"
    And Click UW Continue button
    And Verify the error message "Your selected policy dates cannot be covered by this policy type. Please contact VMIA for more details." in information screen
    And Enter the Policy Effective date "01/07/2013" and Expiration date "30/06/2099"
    And Add Testdocument attachment "Policy_attachment.txt"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the mandatoryInformation of finalise Quote and Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"

    @MPL5_Sprint1 @Business&Industry @Construction_multiyear @PolicyCreation @clientacceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname|product                                                      | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|
      | No      |  500      | Hello VMIA        |AutoB&I | Construction Risks - Material Damage and Liability (Projects)| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |


  Scenario Outline: Create construction mutiyear product and do cancellation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Enter the Policy Effective date "27/06/2021" and Expiration date "08/06/2022"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the mandatoryInformation of finalise Quote and Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click the exit button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<cancelPremium>" and "<UnderwritingNotes>" in cancellation
    Then I submit the case to get routed to the user
    And I check the case status to be "PENDING-REFUND"
    Then I logout of the application

    @MPL5_Sprint1 @Business&Industry @Construction_multiyear @PolicyCreation @clientacceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname|product                                                      | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason| cancelPremium|EnableOrDisableProrate|
      | No      |  200      | Hello VMIA        |AutoB&I | Construction Risks - Material Damage and Liability (Projects)| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|-150         | Yes                  |
#      | No      |  200      | Hello VMIA        |AutoB&I | Construction Risks - Material Damage and Liability (Projects)| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|-500         |Yes|


  Scenario Outline: Create construction mutiyear product and do Endrosement
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Enter the Policy Effective date "27/08/2021" and Expiration date "8/08/2085"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And Enter the mandatoryInformation of finalise Quote and Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click the exit button
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Select the effective date "16/09/2021" for endorsment in Multiyear product
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"

    @MPL5_Sprint1 @Business&Industry @Construction_multiyear @PolicyCreation @clientacceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname     |product                                                      | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  200      | Hello VMIA        |Automation_B&I | Construction Risks - Material Damage and Liability (Projects)| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  Scenario Outline: Construction multiyear New product with RiskAdviosr acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Enter the Policy Effective date "27/08/2021" and Expiration date "8/08/2085"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the mandatoryInformation of finalise Quote and Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal


    @MPL5_Sprint1 @Business&Industry @Construction_multiyear @PolicyCreation @clientacceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname|product                                                      | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|
      | Yes     |  200      | Hello VMIA         |AutoB&I | Construction Risks - Material Damage and Liability (Projects)| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |
