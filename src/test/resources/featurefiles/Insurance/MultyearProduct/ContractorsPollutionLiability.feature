Feature: Contractor Pollution Liability product

  Scenario Outline: Contractors Pollution Liability New product
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    And click on the select button of the "<product>" in Risk Advisor
    And Click UW Continue button
    And Enter the Policy Effective date "27/08/2021" and Expiration date "8/08/2035"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<pendingstatus>"
    And I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the mandatoryInformation for contractor liability product and Click Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below two steps
    And Click on Casenumber and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Mycases
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MLP5_Sprint2 @Transport&Vechicle @MarineCargoImportExport @PolicyCreation @clientacceptance @Multiyear @RegressionMLP5Set2 @RegressionMLP5Set2Scenarios11
    Examples:
      | Permium   | UnderwritingNotes  | orgname   |product                       | pendingstatus        | uwstatus                 | finalstatus     | EnableOrDisableProrate|
      |  200      | Hello VMIA         |Tran&Vechi |Contractor Pollution Liability| PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |Yes                   |

  Scenario Outline: Validating the Effective Date of  Contractors Pollution Liability product
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    And click on the select button of the "<product>" in Risk Advisor
    And Enter the Policy Effective date "01/06/2013" and Expiration date "30/07/2099"
    And Click UW Continue button
    And Verify the error message "Your selected policy dates cannot be covered by this policy type. Please contact VMIA for more details." in information screen
    And Enter the Policy Effective date "01/07/2013" and Expiration date "30/08/2099"
    And Click UW Continue button
    And Verify the error message "Your selected policy dates cannot be covered by this policy type. Please contact VMIA for more details." in information screen
    And Enter the Policy Effective date "01/01/2017" and Expiration date "30/06/2099"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the mandatoryInformation for contractor liability product and Click Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MLP5_Sprint2 @Transport&Vechicle @MarineCargoImportExport @PolicyCreation @clientacceptance @Multiyear@Validation
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname   |product                            | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|
      | No      |  200      | Hello VMIA         |Tran&Vechi | Contractor Pollution Liability| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                 |

  Scenario Outline: Create Contractors Pollution Liability multi year product and do cancellation
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    And click on the select button of the "<product>" in Risk Advisor
    And Click UW Continue button
    And Enter the Policy Effective date "27/08/2021" and Expiration date "8/08/2085"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the mandatoryInformation for contractor liability product and Click Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number from RiskAdvisor
    And Click New and Select "Cancel policy" from Cancel Policy option
    Then I enter "<orgname>" and Click find button from Newapplication page
    Then I select the policy in Risk advisor portal
    Then cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<cancelPremium>" and "<UnderwritingNotes>" in cancellation
    Then I submit the case to get routed to the user
    And I check the case status to be "PENDING-REFUND"
    Then I logout of the application

    @MLP5_Sprint2 @Transport&Vechicle @MarineCargoImportExport @PolicyCreation @clientacceptance @Multiyear@Validation
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname|product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason| cancelPremium|EnableOrDisableProrate|
      | No      |  200      | Hello VMIA        |Tran&Vech| Contractor Pollution Liability| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|-150         | Yes                  |
#      | No      |  200      | Hello VMIA        |AutoB&I | Contractors Pollution Liability| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|-500         |


  Scenario Outline: Create Contractors Pollution Liability mutiyear product and do Endrosement
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    And click on the select button of the "<product>" in Risk Advisor
    And Click UW Continue button
    And Enter the Policy Effective date "27/08/2021" and Expiration date "8/08/2085"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the mandatoryInformation for contractor liability product and Click Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    And I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number from RiskAdvisor
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I enter "<orgname>" and Click find button from Newapplication page
    Then I select the policy in Risk advisor portal
    And Click UW Continue button
    And Select the effective date "16/09/2021" for endorsment in Multiyear product
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MLP5_Sprint1 @Transport&Vechicle @MarineCargoImportExport @PolicyCreation @clientacceptance @Endorsement
    Examples:
    | Permium   | UnderwritingNotes  | orgname     |product                        | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|
    |  200      | Hello VMIA         |Tran&Vechi   | Contractor Pollution Liability| PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                   |

  Scenario Outline:  Construction risk-metro tunnel New product with RiskAdviosr acceptance
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    And click on the select button of the "<product>" in Risk Advisor
    And Click UW Continue button
    And Enter the Policy Effective date "27/08/2021" and Expiration date "8/08/2085"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the mandatoryInformation for contractor liability product and Click Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MLP5_Sprint2 @Transport&Vechicle @MarineCargoImportExport @PolicyCreation @RiskAdvisorAcceptance @Multiyear
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname   |product                             | status         | pendingstatus        | uwstatus                 | finalstatus     | EnableOrDisableProrate|
      | Yes      |  200      | Hello VMIA        |Tran&Vechi| Contractor Pollution Liability| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |