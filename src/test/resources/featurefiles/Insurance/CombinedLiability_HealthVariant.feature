Feature: Combined Liablity Health variant

  Scenario Outline: Start a new policy application for Combined Libality
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    And Enter the Annual renveue "<OperationalRevenue>" "<VicrorianRevenue>" "<OtherFund>"
    And Enter the Incident, OverStateWork, AffilationWorkDetails
    And Enter the Storage Hazard details "<activityCasuedDamege>" "<presenceWater>"
    And Enter the Product, Airfield Liablity and Contract info "<Contractorinfo>"
    And Click UW Continue button
    And Add visitor info "<NoOfVisitor>" "<StreetName>" "<Subrubtown>" "<State>" "<postcode>"
    And Enter the Professional Indeminity info
    And Enter the insurance history details
    And Enter the Professional ActivityInfo
    And Enter the Join ventures and overseaswork
    And Click UW Continue button
    And Enter the contract info for liablilty
    And Enter the Risk management details
    And Enter the claim circumstances details
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    When I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application


#    @CombinedLiability @PropertyAndPlan @NewBusiness
#    Examples:
#    | product           |OrgName        |OperationalRevenue|VicrorianRevenue|OtherFund|Subrubtown|NoOfVisitor|State   |postcode|activityCasuedDamege|StreetName|presenceWater |Contractorinfo                                        |Permium|UnderwritingNotes|finalstatus    |uwstatus                |
#    | Combined liability|PAPautomation  |500               |600             |700      |Sydney    |3          |Victoria|6421    |Not applicable      |Main Road|Not applicable|We do not engage Contractors, Sub Contractors or Agents|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|
#    @CombinedLiability  @DepartmentalDivision @NewBusiness
#    Examples:
#      | product           |OrgName                        |OperationalRevenue|VicrorianRevenue|OtherFund|Subrubtown|NoOfVisitor|State   |postcode|activityCasuedDamege|StreetName|presenceWater |Contractorinfo                                        |Permium|UnderwritingNotes|finalstatus    |uwstatus                |
#      | Combined liability|Automation_DepartmentalDivision|500               |600             |700      |Sydney    |3          |Victoria|6421    |Not applicable      |Main Road|Not applicable|We do not engage Contractors, Sub Contractors or Agents|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|
    @CombinedLiability  @Ambulance @NewBusiness @InsuranceRegression
    Examples:
      | product           |OrgName                |OperationalRevenue|VicrorianRevenue|OtherFund|Subrubtown|NoOfVisitor|State   |postcode|activityCasuedDamege|StreetName|presenceWater |Contractorinfo                                        |Permium|UnderwritingNotes|finalstatus    |uwstatus                |EnableOrDisableProrate|
      | Combined liability|Automation_Ambulance|500               |600             |700      |Sydney    |3          |Victoria|6421    |Not applicable      |Main Road|Not applicable|We do not engage Contractors, Sub Contractors or Agents|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|Default Yes                     |

  Scenario Outline: Start a new policy application for Combined Liability and do cancellation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    And Enter the Annual renveue "<OperationalRevenue>" "<VicrorianRevenue>" "<OtherFund>"
    And Enter the Incident, OverStateWork, AffilationWorkDetails
    And Enter the Storage Hazard details "<activityCasuedDamege>" "<presenceWater>"
    And Enter the Product, Airfield Liablity and Contract info "<Contractorinfo>"
    And Click UW Continue button
    And Add visitor info "<NoOfVisitor>" "<StreetName>" "<Subrubtown>" "<State>" "<postcode>"
    And Enter the Professional Indeminity info
    And Enter the insurance history details
    And Enter the Professional ActivityInfo
    And Enter the Join ventures and overseaswork
    And Enter the contract info for liablilty
    And Enter the Risk management details
    And Enter the claim circumstances details
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    When I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @CombinedLiability @PAP @Cancellation
    Examples:
      | product           |OrgName        |OperationalRevenue|VicrorianRevenue|OtherFund|Subrubtown|NoOfVisitor|State   |postcode|activityCasuedDamege|StreetName|presenceWater |Contractorinfo                                        |Permium|UnderwritingNotes|finalstatus    |uwstatus                |IsCancelFullTerm|cancellationdate|Cancelreason|
      | Combined liability|PAPautomation  |500               |600             |700      |Sydney    |3          |Victoria|6421    |Not applicable      |Main Road|Not applicable|We do not engage Contractors, Sub Contractors or Agents|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|yes             | 22/07/2021     |Property sold|
    @CombinedLiability  @DepartmentalDivision @Cancellation
    Examples:
      | product           |OrgName                        |OperationalRevenue|VicrorianRevenue|OtherFund|Subrubtown|NoOfVisitor|State   |postcode|activityCasuedDamege|StreetName|presenceWater |Contractorinfo                                        |Permium|UnderwritingNotes|finalstatus    |uwstatus                |IsCancelFullTerm|cancellationdate|Cancelreason|
      | Combined liability|Automation_DepartmentalDivision|500               |600             |700      |Sydney    |3          |Victoria|6421    |Not applicable      |Main Road|Not applicable|We do not engage Contractors, Sub Contractors or Agents|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|yes             | 22/07/2021     |Property sold|
    @CombinedLiability  @CSCommunityHealthCare @Cancellation
    Examples:
      | product           |OrgName                        |OperationalRevenue|VicrorianRevenue|OtherFund|Subrubtown|NoOfVisitor|State   |postcode|activityCasuedDamege|StreetName|presenceWater |Contractorinfo                                        |Permium|UnderwritingNotes|finalstatus    |uwstatus                |IsCancelFullTerm|cancellationdate|Cancelreason|
      | Combined liability|Automation_CS_CommunityHeathcare|500               |600             |700      |Sydney    |3          |Victoria|6421    |Not applicable      |Main Road|Not applicable|We do not engage Contractors, Sub Contractors or Agents|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|yes             | 22/07/2021     |Property sold|


  Scenario Outline: Start a new policy application for Combined Liability and do Endorsment
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    And Enter the Annual renveue "<OperationalRevenue>" "<VicrorianRevenue>" "<OtherFund>"
    And Enter the Incident, OverStateWork, AffilationWorkDetails
    And Enter the Storage Hazard details "<activityCasuedDamege>" "<presenceWater>"
    And Enter the Product, Airfield Liablity and Contract info "<Contractorinfo>"
    And Click UW Continue button
    And Add visitor info "<NoOfVisitor>" "<StreetName>" "<Subrubtown>" "<State>" "<postcode>"
    And Enter the Professional Indeminity info
    And Enter the insurance history details
    And Enter the Professional ActivityInfo
    And Enter the Join ventures and overseaswork
    And Enter the contract info for liablilty
    And Enter the Risk management details
    And Enter the claim circumstances details
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    Then Select EffectiveDate "24/08/2021" And Enter UpdateInfo "Test"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I logout of the application

    @CombinedLiability @PropertyAndPlan @Endrosement
    Examples:
      | product           |OrgName        |OperationalRevenue|VicrorianRevenue|OtherFund|Subrubtown|NoOfVisitor|State   |postcode|activityCasuedDamege|StreetName|presenceWater |Contractorinfo                                        |Permium|UnderwritingNotes|finalstatus    |uwstatus                |
      | Combined liability|PAPautomation  |500               |600             |700      |Sydney    |3          |Victoria|6421    |Not applicable      |Main Road|Not applicable|We do not engage Contractors, Sub Contractors or Agents|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|
    @CombinedLiability  @DepartmentalDivision @Endrosement
    Examples:
      | product           |OrgName                        |OperationalRevenue|VicrorianRevenue|OtherFund|Subrubtown|NoOfVisitor|State   |postcode|activityCasuedDamege|StreetName|presenceWater |Contractorinfo                                        |Permium|UnderwritingNotes|finalstatus    |uwstatus                |
      | Combined liability|Automation_DepartmentalDivision|500               |600             |700      |Sydney    |3          |Victoria|6421    |Not applicable      |Main Road|Not applicable|We do not engage Contractors, Sub Contractors or Agents|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|
    @CombinedLiability  @CSCommunityHealthCare @Endrosement
    Examples:
      | product           |OrgName                        |OperationalRevenue|VicrorianRevenue|OtherFund|Subrubtown|NoOfVisitor|State   |postcode|activityCasuedDamege|StreetName|presenceWater |Contractorinfo                                        |Permium|UnderwritingNotes|finalstatus    |uwstatus                |
      | Combined liability|Automation_CS_CommunityHeathcare|500               |600             |700      |Sydney    |3          |Victoria|6421    |Not applicable      |Main Road|Not applicable|We do not engage Contractors, Sub Contractors or Agents|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|