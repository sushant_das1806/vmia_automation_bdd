Feature: Bundle policy creation Community Service Organzation

  Scenario Outline: Create the auswide type Bundle Policy
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click CreateNew button
    And Enter the Orgdetails "<ClientSegment>" "<GovenrmentDepartment>" "<Profession>" "<PhoneNumber>" "Sydney "
    And Click New and Select "Manage contact"
    Then Search the contact using contactid "CONT-27682"
    And Add the contact to Organzation to the contact "" "Insurance;Finanace"
    And Click New and Select "Create policy bundle"
    And Select orgnazation by searching clientsegment "<ClientSegment>" and "<BundleType>" and orgname
    Then Add the unincorproted orgnization "<UnincorprartOrg>" and policyEffectiveDate "<EffectiveDate>" and finish the policy
    Then Verify case status "RESOLVED-COMPLETED" and capture the case id
    And Click New and Select "Create policy bundle"
    And Select orgnazation by searching clientsegment "<ClientSegment>" and "<AusBundleType>" and orgname
    And Enter the Revenue breakdown
    And Enter the Revenue "<Revenue>" and PolicyEffectiveDate "<EffectiveDate>"
    Then Verify case status "RESOLVED-COMPLETED" and capture the case id
    Then I logoff from client adviser portal
    And I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And Select the Bundle Policy Orgnization
    And Verify all the policies are created and more action is disabled for "AuswideValidate"
    And I logout of the application

    @AusWideBundle @CSOBundle @NewBusiness @BundlePolicy @MLP5_Sprint3 @RegressionMLP5Set2
    Examples:
      | username          | password     | ClientSegment                 | GovenrmentDepartment| Profession|PhoneNumber|AusBundleType | Revenue|EffectiveDate|BundleType|UnincorprartOrg|
      | AutoClientAdviser | Welcome@1234 | Community Service Organisation| Department of Health| Test      | 1234567890|CSO (Auswide)| 5000   |29/09/2021   |CSO        | test          |

  Scenario Outline: Create the CSO Bundle Policy and verify the mail is exist
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click CreateNew button
    And Enter the Orgdetails "<ClientSegment>" "<GovenrmentDepartment>" "<Profession>" "<PhoneNumber>" "Sydney "
    And Click New and Select "Manage contact"
    Then Search the contact using contactid "CONT-27682"
    And Add the contact to Organzation to the contact "" "Insurance;Finanace"
    And Click New and Select "Create policy bundle"
    And Select orgnazation by searching clientsegment "<ClientSegment>" and "<BundleType>" and orgname
    Then Add the unincorproted orgnization "<UnincorprartOrg>" and policyEffectiveDate "<EffectiveDate>" and finish the policy
    Then Verify case status "RESOLVED-COMPLETED" and capture the case id
    Then I logoff from client adviser portal
    And I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And Select the Bundle Policy Orgnization
    And Verify all the policies are created and more action is disabled
    And I logoff from client adviser portal

    @CSOPBundle @CSOBundle  @BundlePolicy  @NewBusiness @MLP5_Sprint3
    Examples:
      | username          | password     | ClientSegment                 | GovenrmentDepartment| Profession|PhoneNumber|BundleType| UnincorprartOrg|EffectiveDate|
      | AutoClientAdviser | Welcome@1234 | Community Service Organisation| Department of Health| Test      | 1234567890|CSO	      | 5000   |29/09/2021   |


  Scenario Outline: Create the Bundle Policy and Cancel the policy bundle
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click CreateNew button
    And Enter the Orgdetails "<ClientSegment>" "<GovenrmentDepartment>" "<Profession>" "<PhoneNumber>" "Sydney "
    And Click New and Select "Manage contact"
    Then Search the contact using contactid "CONT-27682"
    And Add the contact to Organzation to the contact "" "Insurance;Finanace"
    And Click New and Select "Create policy bundle"
    And Select orgnazation by searching clientsegment "<ClientSegment>" and "<BundleType>" and orgname
    And Enter the Revenue "<Revenue>" and PolicyEffectiveDate "<EffectiveDate>"
    Then Verify case status "RESOLVED-COMPLETED" and capture the case id
    When Click New and Select "Cancel bundle" from Cancel Policy option
    And  search the organization using clientSegment "Community Service Organisation" and Orgnazation
    And  Select the contact and Product "CSO" click finish
    Then click more actions and cancel the policy using the following information
      |<IsFullTermCancel>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And I logoff from client adviser portal

    @AusWideBundle @CSOBundle @AusWideBundleMidTermCancellation @BundlePolicy @MLP5_Sprint3
    Examples:
      | ClientSegment | GovenrmentDepartment| Profession|PhoneNumber|BundleType| Revenue|EffectiveDate|IsFullTermCancel|cancellationdate|Cancelreason|
     | CSO Program   | Department of Health| Test      | 1234567890|Auswide	  | 5000   |29/09/2021   |  Yes           | 2/06/2021       |Property sold |
    @CSOPBundle @CSOBundle  @CSOBundleMidTermCancellation @BundlePolicy @MLP5_Sprint3
    Examples:
    | ClientSegment | GovenrmentDepartment| Profession|PhoneNumber|BundleType| Revenue|EffectiveDate|IsFullTermCancel|cancellationdate|Cancelreason|
     | CSO Program   | Department of Health| Test      | 1234567890|CSOP bundle| 5000   |29/09/2021  |   No             | 2/06/2021       |Property sold |

  Scenario Outline: Create the Bundle Policy and verify the unicorprated orgnization is added properly to the Organzation
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click CreateNew button
    And Enter the Orgdetails "Community Service Organisation" "Department of Education and Training" "tester" "8508356502" "Sydney "
    And Click New and Select "Manage contact"
    Then Search the contact using contactid "CONT-27587"
    And Add the contact to Organzation to the contact "" "Insurance;Finanace"
    And Click New and Select "Create policy bundle"
    And Select orgnazation by searching clientsegment "Community Service Organisation" and orgname
    Then Add the unincorproted orgnization "<UnincorprartOrg>" and policyEffectiveDate "<EffectiveDate>" and finish the policy
    Then Verify case status "RESOLVED-COMPLETED" and capture the case id
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click UpdateButton
    And Verify the UnincorpatedOrgnization added "Test" in bundle policy is displayed in Orgnization
    And I logoff from client adviser portal

    Examples:
      | username          | password     | organization | Location    | Action |FinalStaus|
      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Approve|RESOLVED-COMPLETED |

  Scenario Outline: Add unicorprated orgnization to Organzation and check the same is displayed while creating bundle policy
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click CreateNew button
    And Enter the Orgdetails "Community Service Organisation" "Department of Education and Training" "tester" "8508356502" "Sydney "
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click UpdateButton
    And Update the following orgnization details "Yes" "No" "14/06/2021" "Updateorg" "Orgdetail"
    And Add Unincorprate org "Test" in the orgnization
    And Click New and Select "Manage contact"
    Then Search the contact using contactid "CONT-27587"
    And Add the contact to Organzation to the contact "" "Insurance;Finanace"
    And Click New and Select "Create policy bundle"
    And Select orgnazation by searching clientsegment "Community Service Organisation" and orgname
    Then Verify the unincorproted orgnization "Test" is displaying in policy bundle and finish the policy
    Then Verify case status "RESOLVED-COMPLETED" and capture the case id
    And I logoff from client adviser portal

    Examples:
      | username          | password     | organization | Location    | AssetType|AssetName |AssetBand|Action |IsEndorseNeed|FinalStaus|
      | AutoClientAdviser | Welcome@1234 | Scotland PNP | Sydney cot  | Bore     | Test 		| 2				|Approve|Yes   |RESOLVED-COMPLETED |


  Scenario Outline: Verify the Split and merge organization details can be updated in manage Organization
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click CreateNew button
    And Enter the Orgdetails "Community Service Organisation" "Department of Education and Training" "tester" "8508356502" "Sydney "
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click UpdateButton
    And Update the following orgnization details "<isSubjectToGovernChange>" "<IsOrgMerged>" "<EffecitveDate>" "<comments>" "<MergeOrgDetail>"
    And click submit button in UpdateOrganization Screen
#    And Click New and Select "Manage contact"
#    Then Search the contact using contactid "CONT-27587"
#    And Add the contact to Organzation to the contact "" "Insurance;Finanace"
#    Then Verify case status "RESOLVED-COMPLETED" and capture the case id

    Examples:
      | username          | password     | isSubjectToGovernChange | IsOrgMerged    | EffecitveDate |MergeOrgDetail|comments|
#      | AutoClientAdviser | Welcome@1234 | Yes                     | Merged         | 14/06/2021    |Scotland PNP  |Updateorg|
      | AutoClientAdviser | Welcome@1234 | Yes                     | Splited          | 14/06/2021    |Scotland PNP;CSEO 5|Updateorg|
#      | AutoClientAdviser | Welcome@1234 | Yes                     | No             | 14/06/2021    |Scotland PNP;CSEO 5 |Updateorg|