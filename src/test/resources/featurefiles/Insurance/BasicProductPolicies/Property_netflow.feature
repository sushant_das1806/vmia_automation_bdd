Feature:Property - NetFlow feature

  Scenario Outline: Start a new policy application for Property - Netflow client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

    @TransportAndVehicle @PropertyNetflow @NewBusiness @ClientAcceptance @RegressionMLP5Set1 @RegressionMLP5Set1Scenarios
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname              | product            | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|
      | No      |     5000  | Hello VMIA        | Automation_Transaport| Property - Netflow | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |Yes                   |

  Scenario Outline:Start a new policy application for Property - Netflow with RiskAdvisor client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal


    @TransportAndVehicle @PropertyNetflow @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product             | status         | pendingstatus        | uwstatus                 | finalstatus    |EnableOrDisableProrate|
      | Yes     |      10  | Hello VMIA          |Automation_Transaport|Property - Netflow    | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                    |

  Scenario Outline: Cancelling the Property - Netflow policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
    When I click on Case ID after filter
    And validate the premium calculation for the "<cancellation>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @TransportAndVehicle @PropertyNetflow @Cancellation @ClientAcceptance @MidTermCancellation @RegressionMLP5Set2
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname              |product                         | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate| Cancelreason | EnableOrDisableProrate|
      | No      |     2000  | Hello VMIA        |Automation_Transaport | Property - Netflow             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 20/06/2022     | Property sold| Yes                   |
      ### cancellation date 22/07/2021 replaced with 20/06/2022

  Scenario Outline: Start a new policy application for Property - Netflow with deviation and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

    @TransportAndVehicle @PropertyNetflow @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |EnableOrDisableProrate|
      | No      |      10  | Hello VMIA          |Automation_Transaport| Property - Netflow             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                                 |


  Scenario Outline: Start a new policy Property - Netflow application and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @TransportAndVehicle @PropertyNetflow @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname           |product                  | status         | pendingstatus        | uwstatus                 | finalstatus    |EnableOrDisableProrate|
      | Yes      |      10  | Hello VMIA          |Automation_Transaport| Property - Netflow| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                             |
