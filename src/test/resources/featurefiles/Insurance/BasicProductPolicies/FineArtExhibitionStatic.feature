Feature: Fine Arts-Domestic feature

  Scenario Outline: Start a new policy application for Fine Arts-Domestic client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    @InsuranceRegression
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Gov & Eco  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoEdu         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Law & Jus  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto E&w        | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoB&I         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoProp&plan   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Autom_Cemetry   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline:Start a new policy application for Fine Arts-Domestic with RiskAdvisor client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Gov & Eco  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoEdu         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Law & Jus  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto E&w        | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoB&I         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoProp&plan   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Autom_Cemetry   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline: Cancelling the Fine Arts-Domestic policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus      |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT| No             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Gov & Eco  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoEdu         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Law & Jus  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto E&w        | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoB&I         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoProp&plan   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Autom_Cemetry   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus      |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT| Yes             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Gov & Eco  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoEdu         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Law & Jus  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto E&w        | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoB&I         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoProp&plan   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/07/2021     |Property sold|
    Examples:
      | decsion | Permium   | UnderwritingYestes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Autom_Cemetry   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/07/2021     |Property sold|


  Scenario Outline: Start a new policy application for GPA with deviation and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Gov & Eco  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoEdu         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Law & Jus  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto E&w        | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoB&I         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoProp&plan   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Autom_Cemetry   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |



  Scenario Outline: Start a new policy application and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal


    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Gov & Eco  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoEdu         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Law & Jus  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto E&w        | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoB&I         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |AutoProp&plan   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Autom_Cemetry   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline: Start a new Fine Art Exhibitions - Static Domestic policy application and endorse and then Cancel Policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<UpPermium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I click on Exit Button
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And validate the premium calculation for the "<cancellation>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @RegressionMLP4Scenarios
    Examples:
      | decsion | Permium  | UnderwritingNotes | orgname           | product                         | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm | cancellationdate | Cancelreason  | UpPermium |
      | No      |    10    | Hello VMIA        | Auto trans & vech | Fine Art Exhibitions - Static   | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No              | 25/05/2023       | Property sold | 100       |
