Feature: Comprehensive Motor Vehicle feature

  Scenario Outline: Start a new policy application for Comprehensive motor vehicle product
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"

    @MLP4 @LawAndJustice @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle
#    Examples:
#      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
#      | No      |  2000      | Hello VMIA         |Auto Law & Jus  | Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP4 @CommunityHeathcare @PolicyCreation @clientacceptance @NewBusiness  @InsuranceRegression @ComprehensiveMotorVehicle @InsuranceRegression
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname                          |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000      | Hello VMIA         |Automation_CS_CommunityHeathcare  | Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @MLP4 @BushNursingHospital @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle
#    Examples:
#      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
#      | No      |  2000      | Hello VMIA         |BushNursingHospital_Automation  | Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @MLP4 @DepartmentalDivision @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle
#    Examples:
#      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
#      | No      |  2000      | Hello VMIA         |Automation_DepartmentalDivision | Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @MLP4 @Ambulance @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle
#    Examples:
#      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
#      | No      |  2000      | Hello VMIA         |Automation_Ambulanc  | Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @MedicalResearchPP @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname                   |product                      | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000      | Hello VMIA        |Automation_MedicalResearchPP| Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @PublicHospital @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname               |product                       | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000      | Hello VMIA        |Automation_PublicHospital| Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @Ambulance @PolicyCreation @SpecialistAgencies  @clientacceptance @ComprehensiveMotorVehicle
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname                     |product                       | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000     | Hello VMIA        |Automation_Specialist_Agencies  | Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @SpecialistAgenciesPP @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname                      |product                      | status         | pendingstatus        | uwstatus   | finalstatus     |
      | No      |  2000     | Hello VMIA        |Automation_Specialist_AgenciesPP| Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  Scenario Outline:Start a new policy application for Comprehensive motor vehicle product with risk advisor acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MPL4 @PropertyAndPlan @ComprehensiveMotorVehicle @PolicyCreation @RiskAdvisorAcceptance @ComprehensiveMotorVehicle
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | Yes      |  1000     | Hello VMIA         | Auto Law & Jus   |  Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @PublicHospital @PolicyCreation @RiskAdvisorAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname               |product                       | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | Yes      |  2000      | Hello VMIA        |Automation_PublicHospital| Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @Ambulance @PolicyCreation @SpecialistAgencies @RiskAdvisorAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname                     |product                       | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | Yes      |  2000     | Hello VMIA        |Automation_Specialist_Agencies  | Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @SpecialistAgenciesPP @PolicyCreation @RiskAdvisorAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname                      |product                      | status         | pendingstatus        | uwstatus   | finalstatus     |
      | Yes     |  2000     | Hello VMIA        |Automation_Specialist_AgenciesPP| Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline: Start a new Comprehensivemotorvehicle application and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MPL4 @PropertyAndPlan @Group_Personal_Accident_Heritage_Divers @PolicyCreation @RiskAdvisorAcceptance @Endrosement  @All
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname         |product                       | status         | pendingstatus        | uwstatus                 |finalstatus|
      | Yes      |  2000      | Hello VMIA        | Auto Law & Jus   |  Comprehensive Motor Vehicle| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @PublicHospital @PolicyCreation @RiskAdvisorAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname               |product                       | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | Yes      |  2000      | Hello VMIA        |Automation_PublicHospital| Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @Ambulance @PolicyCreation @SpecialistAgencies @RiskAdvisorAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname                     |product                       | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | Yes      |  2000     | Hello VMIA        |Automation_Specialist_Agencies  | Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @SpecialistAgenciesPP @PolicyCreation @RiskAdvisorAcceptance
    Examples:
      | decsion| Permium   | UnderwritingNotes |  orgname                      |product                      | status         | pendingstatus        | uwstatus   | finalstatus     |
      | Yes     |  2000     | Hello VMIA        |Automation_Specialist_AgenciesPP| Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |



  Scenario Outline: Start a new policy Comprehensive Motorvehicle application and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

    @MPL4 @PropertyAndPlan @ComprehensiveMotorVehicle @PolicyCreation @clientacceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname         |product                       | status         | pendingstatus        | uwstatus                 |finalstatus|
      | No      |  2000      | Hello VMIA        | Auto Law & Jus   |  Comprehensive Motor Vehicle| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @PublicHospital @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname               |product                       | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000      | Hello VMIA        |Automation_PublicHospital| Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @Ambulance @PolicyCreation @SpecialistAgencies @clientacceptance @ComprehensiveMotorVehicle
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname                     |product                       | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000     | Hello VMIA        |Automation_Specialist_Agencies  | Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @SpecialistAgenciesPP @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle
    Examples:
      | decsion| Permium   | UnderwritingNotes |  orgname                      |product                      | status         | pendingstatus        | uwstatus   | finalstatus     |
      | No     |  2000     | Hello VMIA        |Automation_Specialist_AgenciesPP| Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline: Cancelling the product policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @PropertyAndPlan @ComprehensiveMotorVehicle @clientacceptance @Cancellation  @All @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname        |product                                    | status         | pendingstatus        | uwstatus             | finalstatus |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Auto Law & Jus |  Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
    @PropertyAndPlan @ComprehensiveMotorVehicle @PolicyCreation @clientacceptance @Cancellation  @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                 |product                                    | status         | pendingstatus        | uwstatus             | finalstatus |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Auto Law & Jus |  Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|
    @MedicalResearchPP @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle @Cancellation @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                     |product                                    | status         | pendingstatus        | uwstatus             | finalstatus |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Automation_MedicalResearchPP |  Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
    @MedicalResearchPP @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle @Cancellation @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                 |product                                    | status         | pendingstatus        | uwstatus             | finalstatus |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Automation_MedicalResearchPP|  Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|
    @PublicHospital @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle @Cancellation @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                 |product                                    | status         | pendingstatus        | uwstatus             | finalstatus |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Automation_PublicHospital|  Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
    @PublicHospital @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle @Cancellation @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                 |product                                    | status         | pendingstatus        | uwstatus             | finalstatus |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Automation_PublicHospital|  Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|
    @Specialist_Agencies @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle @Cancellation @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname        |product                                    | status         | pendingstatus        | uwstatus             | finalstatus |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Auto Law & Jus |  Automation_Specialist_Agencies| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
    @Specialist_Agencies @PolicyCreation @clientacceptance @ComprehensiveMotorVehicle @Cancellation @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                 |product                                    | status         | pendingstatus        | uwstatus             | finalstatus |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Auto Law & Jus |  Automation_Specialist_Agencies | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|
    @Specialist_AgenciesPP  @clientacceptance @ComprehensiveMotorVehicle @Cancellation @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                        |product                                    | status         | pendingstatus        | uwstatus             | finalstatus |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Automation_Specialist_AgenciesPP|  Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
    @Specialist_AgenciesPP @clientacceptance @ComprehensiveMotorVehicle @Cancellation @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                        |product                                    | status         | pendingstatus        | uwstatus             | finalstatus |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Automation_Specialist_AgenciesPP|  Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|

  Scenario Outline: Start a new policy application for Comprehensive motor vehicle product and perform an Endorsement and then Cancel the policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    ### There is no chance of getting display - policy number in below step
    ### And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<isCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @MLP4 @CommunityHeathcare @PolicyCreation @clientacceptance @NewBusiness  @InsuranceRegression @ComprehensiveMotorVehicle @InsuranceRegression @RegressionMLP5Set1 @RegressionMLP5Set1Scenarios
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname                          | product                     | status         | pendingstatus        | uwstatus                 | finalstatus     |isCancelFullTerm| cancellationdate| Cancelreason|
      | No      |  2000     | Hello VMIA         | Automation_CS_CommunityHeathcare | Comprehensive Motor Vehicle | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021      | Property sold|