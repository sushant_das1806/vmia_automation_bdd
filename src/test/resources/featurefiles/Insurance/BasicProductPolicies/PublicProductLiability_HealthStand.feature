Feature: Public product liability Product

  Scenario Outline: Start a new policy application for Public & Products Liability product and select client acceptance in client portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the Business value and Submit Quoate
    Given Enter the deductible value and Submit Quote
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I logout of the application

    @MLP5_Sprint2 @Ambulance @PublicProductLiabilityHealth @NewBusiness @clientacceptance @InsuranceRegression
    Examples:
      | decsion |  Permium   | UnderwritingNotes  |  orgname          |product                    | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|
      | No      |    5000      | Hello VMIA       |  Automation_Ambulance     |Public & Products Liability| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |
#

  Scenario Outline: Start a new policy application for Public & Products Liability product and select client acceptance in risk advisor portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MPL4 @Ambulance @PublicProductLiabilityHealth @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|
      | yes      |    5000   | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Automation_Ambulance   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                      |

  Scenario Outline: Start a new Public & Products Liability policy application and endorsement with riskAdvisor Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    ### And Submit the Quoate
    ### Added below step intead of above step because the mandetory fields are not entering but directly submitting
    Given Enter the deductible value and Submit Quote
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I logout of the application

    @MLP5_Sprint2 @Ambulance @PublicProductLiabilityHealth @PolicyEndorsemet @clientacceptance @Endorsement @RegressionMLP5Set2
    Examples:
      | decsion |  Permium   | UnderwritingNotes  |  orgname         |product                                              | status      | pendingstatus| uwstatus | finalstatus     | EnableOrDisableProrate|
      |No      |   5000     | Hello VMIA         |  Automation_Ambulance   |Public & Products Liability| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |

  Scenario Outline: Start a new Public & Products Liability policy application and do endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

    @MLP5_Sprint2 @Ambulance @PublicProductLiabilityHealth @PolicyEndorsemet @clientacceptance @Endorsement
    Examples:
      | decsion |  Permium   | UnderwritingNotes  |  orgname         |product                                              | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|
      | No      |   5000     | Hello VMIA         |  Automation_Ambulance   |Public & Products Liability| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |


  Scenario Outline: Start a new policy application for Public & Products Liability Annual product and cancel it
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And validate the premium calculation for the "<cancellation>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @MLP5_Sprint2 @Ambulance @PublicProductLiabilityHealth @PolicyCancellation @clientacceptance  @Cancellation @FullTermCancellation
    Examples:
      | decsion |  Permium   | UnderwritingNotes  |  orgname             |product          | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |    10      | Hello VMIA         |  Automation_Ambulance|Public & Products Liability| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |yes             | 22/07/2021     |Property sold|
    @MLP5_Sprint2 @Ambulance @PublicProductLiabilityHealth @PolicyCancellation @clientacceptance  @Cancellation @MidTermCancellation
    Examples:
      | decsion |  Permium   | UnderwritingNotes  |  orgname             |product          | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |    10      | Hello VMIA         |  Automation_Ambulance|Public & Products Liability| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |yes             | 22/07/2021     |Property sold|

  Scenario Outline: Start a new policy application for Public & Products Liability product and perform an Endorsement and then Cancel the policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the Business value and Submit Quoate
    Given Enter the deductible value and Submit Quote
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    ### There is no chance of getting display - policy number in below step
    ### And I save the policy number
    And I click on Exit Button
    ### Below steps are duplicate in the existing flow
    ### And I Select the Organization "<orgname>" for "<product>"
    ### Then I open the approved case
    ### Then I accept the quote
    ### And I validate the "<finalstatus>"
    ### And I save the policy number
    ### And I click on Exit Button
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And validate the premium calculation for the "<cancellation>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    And I logout of the application

    @MLP5_Sprint2 @Ambulance @PublicProductLiabilityHealth @NewBusiness @clientacceptance @InsuranceRegression @RegressionMLP5Set1 @RegressionMLP5Set1Scenarios
    Examples:
      | decsion |  Permium   | UnderwritingNotes |  orgname               | product                     | status         | pendingstatus        | uwstatus                 | finalstatus     | EnableOrDisableProrate| IsCancelFullTerm| cancellationdate| Cancelreason |
      | No      |    5000    | Hello VMIA        |  Automation_Ambulance  | Public & Products Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                   | Yes             | 22/07/2021      | Property sold|


  Scenario Outline: CCI-4870 - Public & Products Liability product and perform an Endorsement and then Cancel the policy with exclude endorsement
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the Business value and Submit Quoate
    Given Enter the deductible value and Submit Quote
    And I validate the "<uwstatus>"
    Then I logout of the application
    ### 1st cycle done here
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    ### 2nd cycle is done here
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I click on Exit Button
    #### added new step from here
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    ### step added here-new required step
    And I give endorsement text "<EndorseText>" and check exclude check box
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I click on Exit Button
    ### ended new step here
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And validate the premium calculation for the "<cancellation>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    And I logout of the application

    @CCI-4870
    Examples:
      | decsion |  Permium   | UnderwritingNotes |  orgname               | product                     | status         | pendingstatus        | uwstatus                 | finalstatus     | EnableOrDisableProrate| IsCancelFullTerm| cancellationdate| Cancelreason |
      | No      |    5000    | Hello VMIA        |  Automation_Ambulance  | Public & Products Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                   | Yes             | 22/07/2021      | Property sold|


  Scenario Outline: CCI-4171 - Public & Products Liability product and perform an Endorsement and then Cancel the policy with edit endorsement text
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Enter the Business value and Submit Quoate
    Given Enter the deductible value and Submit Quote
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Add endorsement text "<EndorseText>" and submit
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application
    ### 2nd story flow getting implemented from here
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I Click new button to edit endorsement text
    Then search with the selected organization
    And Select contact from contact "<contact>"
    And I select the policy and then endorsement
    And Edit the Endorsement text "<UpdateEndorseText>" and click continue
    And Select the reviewer "<name>" and provide comments "<comments>" and then Click Finish button
    And I capture the endorse ID
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the endorsement ID and select the record
    And Endorsement text request has been "Reject"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I Click new button to edit endorsement text
    Then search with the selected organization
    And Select contact from contact "<contact>"
    And I select the policy and then endorsement
    And Edit the Endorsement text "<UpdateEndorseText>" and click continue
    And Select the reviewer "<name>" and provide comments "<comments>" and then Click Finish button
    And I capture the endorse ID
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the endorsement ID and select the record
    And Endorsement text request has been "Approve"
    Then I logout of the application
    ### 2nd story flow getting Ended here
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And validate the premium calculation for the "<cancellation>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    And I logout of the application

    @CCI-4171
    Examples:
      | decsion |  Permium   | UnderwritingNotes |  orgname               | product                     | status         | pendingstatus        | uwstatus                 | finalstatus     | EnableOrDisableProrate| IsCancelFullTerm| cancellationdate| Cancelreason | EndorseText      | UpdateEndorseText        | comments | name         |
      | No      |    5000    | Hello VMIA        |  Automation_Ambulance  | Public & Products Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                   | Yes             | 22/07/2021      | Property sold| endorsement Text | updated endorsement text | VMIA     | Underwriter3 |

