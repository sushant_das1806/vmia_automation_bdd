Feature: Combined Medical and Professional Indeminity

  Scenario Outline: Start new Combined Medical and Professional Indeminity basic product
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    Given Enter the mandatoryInformation for MIPI product and Click Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"

    @MLP4 @PropertyAndPlan @ @PolicyCreation @clientacceptance @NewBusiness @InsuranceRegression @CombinedMedicalProfessionIdeminity @InsuranceRegression
    Examples:
      | decsion | Permium   | UnderwritingNotes  |   orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000      | Hello VMIA         | Automation PAP   |  Combined Medical & Professional Indemnity | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @CSCommunityHealthCare  @PolicyCreation @clientacceptance @NewBusiness @CombinedMedicalProfessionIdeminity
    Examples:
      | decsion | Permium   | UnderwritingNotes |   orgname                        |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000     | Hello VMIA        | Automation_CS_CommunityHeathcare| Combined Medical & Professional Indemnity | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  Scenario Outline: Start a new policy application for Combined Medical and Professional Indeminity
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    Given Enter the mandatoryInformation for MIPI product and Click Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MLP4 @PropertyAndPlan @CombinedMedicalAndProfessionIndeminity @clientacceptance @CombinedMedicalProfessionIdeminity
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | Yes      |  1000     | Hello VMIA        | Auto E&w        |  Combined Medical & Professional Indemnity | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @CSCommunityHealthCare @CombinedMedicalAndProfessionIndeminity @PolicyCreation @clientacceptance @CombinedMedicalProfessionIdeminity
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname                    |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | Yes      |  1000     | Hello VMIA        |Automation_CS_CommunityHeathcare        |  Combined Medical & Professional Indemnity | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  Scenario Outline: Start a new Combined Medical and Professional Indeminity application and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MLP4 @PropertyAndPlan @CombinedMedicalAndProfessionIndeminity @PolicyCreation @clientacceptance @Endrosement @InsuranceRegression @CombinedMedicalProfessionIdeminity
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         | Auto E&w        |  Combined Medical & Professional Indemnity| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
    @MLP5_Sprint2 @CSCommunityHealthCare @CombinedMedicalAndProfessionIndeminity @PolicyCreation @clientacceptance @Endrosement @CombinedMedicalProfessionIdeminity
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA        |Automation_CS_CommunityHeathcare        |  Combined Medical & Professional Indemnity| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|


  Scenario Outline: Start a new Combined Medical and Professional Indeminity policy application and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

    @MLP4 @PropertyAndPlan @CombinedMedicalAndProfessionIndeminity @PolicyCreation @clientacceptance  @All @CombinedMedicalProfessionIdeminity
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus             | finalstatus     |
      | Yes      |  2000      | Hello VMIA        | Auto E&w       |  Combined Medical & Professional Indemnity| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP4 @CSCommunityHealthCare @CombinedMedicalAndProfessionIndeminity @PolicyCreation @clientacceptance  @All @CombinedMedicalProfessionIdeminity
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus             | finalstatus     |
      | Yes      |  2000      | Hello VMIA       | Automation_CS_CommunityHeathcare       |  Combined Medical & Professional Indemnity| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  Scenario Outline: Cancelling the Combined Medical and Professional Indeminity product policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    Given Enter the mandatoryInformation for MIPI product and Click Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @MLP4 @PropertyAndPlan  @PolicyCreation @clientacceptance @Cancellation  @All @InsuranceRegression @CombinedMedicalProfessionIdeminity @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus             | finalstatus         |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Auto E&w        |  Combined Medical & Professional Indemnity | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
    @MLP5 @PropertyAndPlan @CombinedMedicalAndProfessionIndeminity @PolicyCreation @clientacceptance @Cancellation  @All @InsuranceRegression @CombinedMedicalProfessionIdeminity @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus             | finalstatus         |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Auto E&w        |  Combined Medical & Professional Indemnity | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|
    @MPL5 @CSCommunityHealthCare @CombinedMedicalAndProfessionIndeminity @PolicyCreation @clientacceptance @Cancellation @CombinedMedicalProfessionIdeminity @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname         |product                                    | status         | pendingstatus        | uwstatus             | finalstatus         |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Automation_CS_CommunityHeathcare        |  Combined Medical & Professional Indemnity | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
    @MLP5 @CSCommunityHealthCare @CombinedMedicalAndProfessionIndeminity @PolicyCreation @clientacceptance @Cancellation @CombinedMedicalProfessionIdeminity @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes  |  orgname                               |product                                    | status         | pendingstatus        | uwstatus             | finalstatus         |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         |Automation_CS_CommunityHeathcare        |  Combined Medical & Professional Indemnity | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|


  Scenario Outline: Start new Combined Medical and Professional Indeminity basic product
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    Given Enter the mandatoryInformation for MIPI product and Click Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"

    @MLP4 @PropertyAndPlan @ @PolicyCreation @clientacceptance @NewBusiness @InsuranceRegression @CombinedMedicalProfessionIdeminity @InsuranceRegression
    Examples:
      | decsion | Permium   | UnderwritingNotes  |   orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000      | Hello VMIA         | Automation PAP   |  Combined Medical & Professional Indemnity | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @MLP5 @CSCommunityHealthCare  @PolicyCreation @clientacceptance @NewBusiness @CombinedMedicalProfessionIdeminity
    Examples:
      | decsion | Permium   | UnderwritingNotes |   orgname                        |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000     | Hello VMIA        | Automation_CS_CommunityHeathcare| Combined Medical & Professional Indemnity | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  Scenario Outline: Start a new policy application for Combined Medical and Professional Indeminity perform and Endorsement and then Cancel the policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    Given Enter the mandatoryInformation for MIPI product and Click Submit
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number from RiskAdvisor
    And I click on Exit Button
    Then I logoff from client adviser portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And Enter the "<EndorsePermium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I click on Exit Button
    Then I logoff from client adviser portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    ### Below step removed here because there wont be immediate logoff once logout the application
    ### Then I logoff from client adviser portal

    @MLP4 @PropertyAndPlan @CombinedMedicalAndProfessionIndeminity @clientacceptance @CombinedMedicalProfessionIdeminity @RegressionMLP5Set1 @RegressionMLP5Set1Scenarios
    ###cancellationdate was 22/12/2021  but here we updated  to 2/05/2023
    Examples:
      | decsion | Permium |EndorsePermium  | UnderwritingNotes  | orgname         | product                                   | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
      | Yes      |  1000  |2000            | Hello VMIA         | Auto E&w        | Combined Medical & Professional Indemnity | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 2/05/2023     |Property sold|
