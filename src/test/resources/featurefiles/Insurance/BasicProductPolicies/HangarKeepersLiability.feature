Feature: Hangar Keepers Liability product

Scenario Outline: Start a new policy application for Hangar Keepers Liability from Cient Advisor portal
  Given I have the URL of VMIA
  Given  User when logs in as "ClientAdviser"
  Then I am logged in to the client adviser portal
  Then Click New and Select "New application"
  Then I enter "<orgname>" and Click find button from Newapplication page
  And click on the select button of the "<product>"
  And Click UW Continue button
  And Click UW Continue button
  Then I Click Finish button
  And I capture the policy ID
  And I validate the "<status>"
  Then I logoff from client adviser portal
  Given I have the URL of VMIA
  When User when logs in as "Underwriter"
  When I search the case id that is routed and I begin the case
  ### Below step is newly added
#  When I click on Case ID after filter
  And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
  Then I Add Additional Deductibles "Deductible1" "1000" "Deductible description"
  And Submit the Quoate
  And I validate the "<uwstatus>"
  Then I logout of the application
  Given I have the URL of VMIA
  When User when logs in as "ClientAdviser"
  ### And Search the policy and begin the case
  ### Above step is replaced with the below two steps
  And Click on Casenumber and then enter ID in Search text field and click Apply button
  And After filter I select first Case ID from the table in Mycases
  Then I accept the quote
  And I validate the "<finalstatus>"
  Then I logoff from client adviser portal

  @Ambulance @HangarKeepersLiability @NewBusiness @RegressionMLP5Set2 @RegressionMLP5Set2Scenarios
  Examples:
  |orgname                  |product                 |status              |Permium |UnderwritingNotes |EnableOrDisableProrate|uwstatus                |finalstatus    |
  |RegressionSet1_Ambulance |Hangar Keepers Liability|PENDING-UNDERWRITING|2000    |Hello VMIA        |No                    |PENDING-CLIENTACCEPTANCE|PENDING-PAYMENT|

#  @BusinessAndIndustries @HangarKeepersLiability @NewBusiness @RegressionMLP5Set2
#  Examples:
#  |orgname                              |product                 |status              |Permium|UnderwritingNotes|EnableOrDisableProrate|uwstatus                |finalstatus    |
#  |RegressionSet1_BusinessAndIndustries |Hangar Keepers Liability|PENDING-UNDERWRITING|2000   |Hello VMIA       |No                    |PENDING-CLIENTACCEPTANCE|PENDING-PAYMENT|
#
#  @Educational @HangarKeepersLiability @NewBusiness @RegressionMLP5Set2
#  Examples:
#  |orgname                 |product                 |status              |Permium |UnderwritingNotes|EnableOrDisableProrate|uwstatus                |finalstatus    |
#  |RegressionSet1_Education|Hangar Keepers Liability|PENDING-UNDERWRITING|2000    |Hello VMIA       |No                    |PENDING-CLIENTACCEPTANCE|PENDING-PAYMENT|
#
#  @GovernmentEconomy @HangarKeepersLiability @NewBusiness @RegressionMLP5Set2
#  Examples:
#  |orgname                     |product                 |status              |Permium |UnderwritingNotes|EnableOrDisableProrate|uwstatus                |finalstatus    |
#  |RegressionSet1_GovAndEconomy|Hangar Keepers Liability|PENDING-UNDERWRITING|2000    |Hello VMIA       |No                    |PENDING-CLIENTACCEPTANCE|PENDING-PAYMENT|
#
#  @LawAndJustice @HangarKeepersLiability @NewBusiness @RegressionMLP5Set2
#  Examples:
#  |orgname|product|status|Permium|UnderwritingNotes|EnableOrDisableProrate|uwstatus|finalstatus|
#  |RegressionSet1_LawAndJustice|Hangar Keepers Liability|PENDING-UNDERWRITING|2000|Hello VMIA|No|PENDING-CLIENTACCEPTANCE|PENDING-PAYMENT|
#
#  @PropertyAndPlan @HangarKeepersLiability @NewBusiness @RegressionMLP5Set2 @RegressionSet2_Ambulance
#  Examples:
#  |orgname                        |product                 |status              |Permium|UnderwritingNotes|EnableOrDisableProrate|uwstatus                |finalstatus    |
#  |RegressionSet1_PropertyPlanning|Hangar Keepers Liability|PENDING-UNDERWRITING|2000   |Hello VMIA       |No                    |PENDING-CLIENTACCEPTANCE|PENDING-PAYMENT|
#
#  @TransportAndVehicle @HangarKeepersLiability @NewBusiness @RegressionMLP5Set2
#  Examples:
#  |orgname                           |product                 |status              |Permium        |UnderwritingNotes|EnableOrDisableProrate  |uwstatus                |finalstatus    |
#  |RegressionSet1_TransportAndVehicle|Hangar Keepers Liability|PENDING-UNDERWRITING|2000           |Hello VMIA       |No                      |PENDING-CLIENTACCEPTANCE|PENDING-PAYMENT|
#
#  @HealthCommunity @HangarKeepersLiability @NewBusiness @RegressionMLP5Set2
#  Examples:
#  |orgname                          |product                 |status              |Permium | UnderwritingNotes|EnableOrDisableProrate| uwstatus                | finalstatus    |
#  |RegressionSet1_HealthAndCommunity|Hangar Keepers Liability|PENDING-UNDERWRITING|2000    | Hello VMIA       |No                    | PENDING-CLIENTACCEPTANCE| PENDING-PAYMENT|


  Scenario Outline: Start a new policy application for Hangar Keepers Liability from Cient Advisor portal and then Cancel the policy
    Given I have the URL of VMIA
    Given  User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then Click New and Select "New application"
    Then I enter "<orgname>" and Click find button from Newapplication page
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    Then I Add Additional Deductibles "Deductible1" "1000" "Deductible description"
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    ### And Search the policy and begin the case
    ### Above step is replaced with the below two steps
    And Click on Casenumber and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Mycases
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number from RiskAdvisor
    Then I logoff from client adviser portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    And I validate the "<cancelstatus>"
    Then I logout of the application

#  @Ambulance @HangarKeepersLiability @MidTermCancellation @RegressionMLP5Set2 @mlp5set00
#  Examples:
#  |orgname                 |product                 |status              |Permium |UnderwritingNotes|EnableOrDisableProrate|uwstatus                |finalstatus    |IsCancelFullTerm |cancellationdate |Cancelreason |cancelstatus   |
#  |RegressionSet1_Ambulance|Hangar Keepers Liability|PENDING-UNDERWRITING|2000    |Hello VMIA       |No                    |PENDING-CLIENTACCEPTANCE|PENDING-PAYMENT|No               | 18/06/2022      |Property sold|PENDING-REFUND |
#  ### Cancelation date updated with 18/06/2022. Previous date was 22/12/2021

  @BusinessAndIndustries @HangarKeepersLiability @MidTermCancellation @RegressionMLP5Set2
  Examples:
  |orgname                              |product                  |status               |Permium| UnderwritingNotes |EnableOrDisableProrate |uwstatus                 |finalstatus    | IsCancelFullTerm |cancellationdate |Cancelreason |cancelstatus   |
  |RegressionSet1_BusinessAndIndustries |Hangar Keepers Liability |PENDING-UNDERWRITING |2000   | Hello VMIA        |No                     |PENDING-CLIENTACCEPTANCE |PENDING-PAYMENT| No               | 18/06/2022      |Property sold|PENDING-REFUND |

#  @Educational @HangarKeepersLiability @MidTermCancellation @RegressionMLP5Set2
#  Examples:
#  |orgname                  |product                  |status               |Permium| UnderwritingNotes |EnableOrDisableProrate |uwstatus                 |finalstatus    |
#  |RegressionSet1_Education |Hangar Keepers Liability |PENDING-UNDERWRITING |2000   | Hello VMIA        |No                     |PENDING-CLIENTACCEPTANCE |PENDING-PAYMENT|
#
#  @GovernmentEconomy @HangarKeepersLiability @MidTermCancellation @RegressionMLP5Set2
#  Examples:
#  |orgname                      |product                  | status              |Permium  |UnderwritingNotes|EnableOrDisableProrate |uwstatus                 |finalstatus    |
#  |RegressionSet1_GovAndEconomy |Hangar Keepers Liability | PENDING-UNDERWRITING|2000     |Hello VMIA       |No                     |PENDING-CLIENTACCEPTANCE |PENDING-PAYMENT|
#
#  @LawAndJustice @HangarKeepersLiability @MidTermCancellation @RegressionMLP5Set2
#  Examples:
#  |orgname|product|status|Permium|UnderwritingNotes|EnableOrDisableProrate|uwstatus|finalstatus|
#  |RegressionSet1_LawAndJustice|Hangar Keepers Liability|PENDING-UNDERWRITING|2000|Hello VMIA|No|PENDING-CLIENTACCEPTANCE|PENDING-PAYMENT|
#
#  @PropertyAndPlan @HangarKeepersLiability @MidTermCancellation @RegressionMLP5Set2
#  Examples:
#  |orgname                        |product                  |status               |Permium|UnderwritingNotes|EnableOrDisableProrate |uwstatus                 |finalstatus    |
#  |RegressionSet1_PropertyPlanning|Hangar Keepers Liability |PENDING-UNDERWRITING |2000   |Hello VMIA       |No                     |PENDING-CLIENTACCEPTANCE |PENDING-PAYMENT|
#
#  @TransportAndVehicle @HangarKeepersLiability @MidTermCancellation @RegressionMLP5Set2
#  Examples:
#  |orgname                            |product                  |status               |Permium|UnderwritingNotes|EnableOrDisableProrate |uwstatus                 |finalstatus    |
#  |RegressionSet1_TransportAndVehicle |Hangar Keepers Liability |PENDING-UNDERWRITING |2000   |Hello VMIA       |No                     |PENDING-CLIENTACCEPTANCE |PENDING-PAYMENT|
#
#  @HealthCommunity @HangarKeepersLiability @MidTermCancellation @RegressionMLP5Set2
#  Examples:
#  |orgname                          |product                  |status               |Permium|UnderwritingNotes|EnableOrDisableProrate |uwstatus                 |finalstatus    | IsCancelFullTerm |cancellationdate |Cancelreason |cancelstatus   |
#  |RegressionSet1_HealthAndCommunity|Hangar Keepers Liability |PENDING-UNDERWRITING |2000   |Hello VMIA       |No                     |PENDING-CLIENTACCEPTANCE |PENDING-PAYMENT| No               | 18/06/2022      |Property sold|PENDING-REFUND |

