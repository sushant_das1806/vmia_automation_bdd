Feature: Initiate Endorserment

    Scenario Outline: Start a new policy application for GPA with deviation and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

    @MPL4 @Auto_Law_Jus @Public_Products_Liability @PolicyCreation @clientacceptance @Endorsement
    Examples:
        | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
        | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @AutoB&I @Public_Products_Liability @PolicyCreation @clientacceptance @Endorsement
    Examples:
        | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
        | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoB&I   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @AutoEdu @Public_Products_Liability @PolicyCreation @clientacceptance @Endorsement
    Examples:
        | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
        | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoEdu   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @Auto_pro_plan @Public_Products_Liability @PolicyCreation @clientacceptance @Endorsement
    Examples:
        | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
        | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @Auto_E_w @Public_Products_Liability @PolicyCreation @clientacceptance @Endorsement
    Examples:
        | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
        | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w    |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |

#    @MPL4 @Auto_Law_Jus @Group_Personal_Accident_CFA @PolicyCreation @clientacceptance
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#        | RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Group Personal Accident CFA               | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#     @MPL4 @AutoB&I @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#        | RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoB&I          | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#    @MPL4 @AutoEdu @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#    	  | RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoEdu          | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#     @MPL4 @Auto_Gov_Eco @Fine_Art_Exhibitions_Static @PolicyCreation	@clientacceptance @Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#      	| RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Gov & Eco   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#      @MPL4 @Auto_Law_Jus @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#       | RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#       @MPL4 @Auto_pro_plan @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#       | RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#      @MPL4 @Auto_cem_trust @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#       | RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto cem trust   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#       @MPL4 @Auto_trans_vech @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#       | RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#       @MPL4 @Auto_E_w @Fine_Art_Exhibitions_Static @PolicyCreation	@clientacceptance @Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#       | RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#      @MPL4 @Auto_E_w @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#    		| RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | Auto E&w        | Group Personal Accident - Flight Risks    | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#    		   @MPL4 @Auto_pro_plan @Group_Personal_Accident_Heritage_Divers @PolicyCreation @clientacceptance @Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#       | RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan  | Group Personal Accident - Heritage Divers | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#       @MPL4 @Auto_E_w @Group_Personal_Accident @PolicyCreation @clientacceptance	@Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#    		| RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w         | Group Personal Accident                   | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#    	@MPL4 @Auto_Law_Jus @Public_Products_Liability_Covid19 @PolicyCreation @clientacceptance @Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#    	 | RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   | Public & Products Liability - Covid19     | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#    	@MPL4 @Auto_Law_Jus @Group_Personal_Accident @PolicyCreation @clientacceptance @Endorsement
#    Examples:
#        | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#     	  | RiskAdvisor | rules      | No      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |  Group Personal Accident	                 | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |

    Scenario Outline: Start a new policy application and endorsement with client Acceptance
        Given I have the external URL of VMIA
        When I enter the "<username>" for azure
        And I enter "<password>" for azure
        And I click login button for azure
        And I Select the Organization "<orgname>"
        Then I want to start a new application for VMIA insurance
        And click on the select button of the "<product>"
        And Click UW Continue button
        And Click UW Continue button
        Then I Click Finish button
        And I capture the policy ID
        And I validate the "<status>"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "RiskAdvisor"
        Then I am logged in to the client adviser portal
        And Search the policy and begin the case
        And Select the Policy decision "<decsion>" and Submit the Application
        And I validate the "<pendingstatus>"
        Then I logoff from client adviser portal
        Given I have the URL of VMIA
        When User when logs in as "Underwriter"
        When I search the case id that is routed and I begin the case
        And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
        And I validate the "<uwstatus>"
        Then I logout of the application
        Given I have the external URL of VMIA
        When I enter the "<username>" for azure
        And I enter "<password>" for azure
        And I click login button for azure
        And I Select the Organization "<orgname>"
        Then I open the approved case
        Then I accept the quote
        And I validate the "<finalstatus>"
        And I save the policy number
        Then I logout of the application
        Given I have the external URL of VMIA
        When I enter the "<username>" for azure
        And I enter "<password>" for azure
        And I click login button for azure
        And I Select the Organization "<orgname>"
        And Select policy and Click on Update Policy
        And Click UW Continue button
        And Click UW Continue button
        Then I Click Finish button
        And I capture the policy ID
        And I validate the "<status>"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "RiskAdvisor"
        Then I am logged in to the client adviser portal
        And Search the policy and begin the case
        And Select the Policy decision "<decsion>" and Submit the Application
        And I validate the "<pendingstatus>"
        Then I logoff from client adviser portal
        Given I have the URL of VMIA
        When User when logs in as "Underwriter"
        When I search the case id that is routed and I begin the case
        And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
        And I validate the "<uwstatus>"
        Then I logout of the application
        Given I have the URL of VMIA
        When User when logs in as "RiskAdvisor"
        Then I am logged in to the client adviser portal
        And Search the policy and begin the case
        Then I accept the quote
        And I validate the "<finalstatus>"
        Then I logoff from client adviser portal

        @MPL4 @Auto_Law_Jus @Public_Products_Liability @PolicyCreation @clientacceptance @Endorsement
        Examples:
        | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
        | yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @AutoB&I @Public_Products_Liability @PolicyCreation @clientacceptance @Endorsement
        Examples:
            | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoB&I   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @AutoEdu @Public_Products_Liability @PolicyCreation @clientacceptance @Endorsement
        Examples:
        | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
        | yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoEdu   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_pro_plan @Public_Products_Liability @PolicyCreation @clientacceptance @Endorsement
        Examples:
        | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
        | yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_E_w @Public_Products_Liability @PolicyCreation @clientacceptance @Endorsement
        Examples:
        | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
        | yes    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w    |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |  ABCD124   |

        @MPL4 @Auto_Law_Jus @Group_Personal_Accident_CFA @PolicyCreation @clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Group Personal Accident CFA               | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @AutoB&I @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoB&I          | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @AutoEdu @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoEdu          | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_Gov_Eco @Fine_Art_Exhibitions_Static @PolicyCreation	@clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Gov & Eco   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_Law_Jus @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_pro_plan @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_cem_trust @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto cem trust   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_trans_vech @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_E_w @Fine_Art_Exhibitions_Static @PolicyCreation	@clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_E_w @Fine_Art_Exhibitions_Static @PolicyCreation @clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 | Auto E&w        | Group Personal Accident - Flight Risks    | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_pro_plan @Group_Personal_Accident_Heritage_Divers @PolicyCreation @clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan  | Group Personal Accident - Heritage Divers | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_E_w @Group_Personal_Accident @PolicyCreation @clientacceptance	@Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w         | Group Personal Accident                   | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_Law_Jus @Public_Products_Liability_Covid19 @PolicyCreation @clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   | Public & Products Liability - Covid19     | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
        @MPL4 @Auto_Law_Jus @Group_Personal_Accident @PolicyCreation @clientacceptance @Endorsement
        Examples:
            | RiskAdvisor | RKpassword | decsion | uwID        | uwpassword  | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
            | RiskAdvisor | rules      | Yes      | Underwriter | Welcome@321  |      10  | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |  Group Personal Accident	                 | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |

   