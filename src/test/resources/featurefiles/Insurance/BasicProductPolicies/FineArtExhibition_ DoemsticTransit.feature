Feature: Fine Arts-Domestic transit feature

  Scenario Outline: Start a new policy application for Fine Arts-Domestic client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

    @TransportAndVehicle @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance  @InsuranceRegression
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - Domestic Transit| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @GovernmentEconomy @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @Educational @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @LawAndJustice @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @EnvironmentAndWaters @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @BusinessAndIndustries @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @PropertyAndPlan @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @Cemetry @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |Autom_Cemetry   | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline:Start a new policy application for Fine Arts-Domestic with RiskAdvisor client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @TransportVehicle @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @GovernmentEconomy @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Educational @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      |Yes      |      10  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @LawAndJustice @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @EnvironmentAndWaters @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @BusinessAndIndustries @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @PropertyAndPlan @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Cemetry @FineArtsStaticDomesticTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Autom_Cemetry   | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline: Cancelling the Fine Arts-Domestic policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And validate the premium calculation for the "<cancellation>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @TransportVehicle @FineArtsStaticDomesticTransit @Cancellation @ClientAcceptance @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus      |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |     2000  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT| No             | 22/07/2021     |Property sold|
#    @GovernmentEconomy @FineArtsStaticDomesticTransit @Cancellation @ClientAcceptance @FullTermCancellation
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
#      | No      |      2000 | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes           | 22/07/2021     |Property sold|
#    @Educational @FineArtsStaticDomesticTransit @Cancellation @ClientAcceptance @MidTermCancellation
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
#      | No      |    2000  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
#    @LawAndJustice @FineArtsStaticDomesticTransit @Cancellation @ClientAcceptance @FullTermCancellation
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
#      | No      |  2000 | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |Yes           | 22/07/2021     |Property sold|
#    @EnvironmentAndWaters @FineArtsStaticDomesticTransit @Cancellation @ClientAcceptance @MidTermCancellation
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
#      | No      |    2000 | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
#    @BusinessAndIndustries @FineArtsStaticDomesticTransit @Cancellation @ClientAcceptance @FullTermCancellation
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
#      | No      |      2000  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/07/2021     |Property sold|
#    @PropertyAndPlan @FineArtsStaticDomesticTransit @Cancellation @ClientAcceptance @MidTermCancellation
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
#      | No      |      2000  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
#    @Cemetry @FineArtsStaticDomesticTransit @Cancellation @ClientAcceptance @FullTermCancellation
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
#      | No      |    2000   | Hello VMIA          |Autom_Cemetry   | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/07/2021     |Property sold|


  Scenario Outline: Start a new policy application for GPA with deviation and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application
    @TransportVehicle @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @GovernmentEconomy @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Educational @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @LawAndJustice @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @EnvironmentAndWaters @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @BusinessAndIndustries @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @PropertyAndPlan @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Cemetry @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Autom_Cemetry   | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  Scenario Outline: Start a new policy application and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @TransportVehicle @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @GovernmentEconomy @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Educational @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @LawAndJustice @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @EnvironmentAndWaters @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @BusinessAndIndustries @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @PropertyAndPlan @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Cemetry @FineArtsStaticDomesticTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA        |Autom_Cemetry   | Fine Art Exhibitions - Domestic Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline: Start a new Fine Art Exhibitions - Domestic Transit application and endorse and then Cancel Policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I click on Exit Button
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And validate the premium calculation for the "<cancellation>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @Developed
    Examples:
      | decsion | Permium  | UnderwritingNotes | orgname          | product                                   | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm | cancellationdate | Cancelreason  |
      | No      |      10  | Hello VMIA        |Auto trans & vech | Fine Art Exhibitions - Domestic Transit   | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No              | 25/05/2023       | Property sold |