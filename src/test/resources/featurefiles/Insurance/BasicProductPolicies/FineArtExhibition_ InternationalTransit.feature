Feature: Fine Arts-International transit feature

  Scenario Outline: Start a new policy application for Fine Arts-Domestic client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

    @TransportAndVehicle @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance  @InsuranceRegression
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @GovernmentEconomy @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @Educational @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @LawAndJustice @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @EnvironmentAndWaters @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @BusinessAndIndustries @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @PropertyAndPlan @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @Cemetry @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
#    Examples:
#      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
#      | No      |      10  | Hello VMIA          |Autom_Cemetry   | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline:Start a new policy application for Fine Arts-Domestic with RiskAdvisor client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @TransportVehicle @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @GovernmentEconomy @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Educational @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      |Yes      |      10  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @LawAndJustice @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @EnvironmentAndWaters @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @BusinessAndIndustries @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @PropertyAndPlan @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Cemetry @FineArtsStaticInternationalTransit @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Autom_Cemetry   | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline: Cancelling the Fine Arts-Domestic policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And validate the premium calculation for the "<cancellation>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @TransportVehicle @FineArtsStaticInternationalTransit @Cancellation @ClientAcceptance @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus      |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |     2000  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT| No             | 22/07/2021     |Property sold|
    @GovernmentEconomy @FineArtsStaticInternationalTransit @Cancellation @ClientAcceptance @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      2000 | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes           | 22/07/2021     |Property sold|
    @Educational @FineArtsStaticInternationalTransit @Cancellation @ClientAcceptance @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |    2000  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
    @LawAndJustice @FineArtsStaticInternationalTransit @Cancellation @ClientAcceptance @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000 | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |Yes           | 22/07/2021     |Property sold|
    @EnvironmentAndWaters @FineArtsStaticInternationalTransit @Cancellation @ClientAcceptance @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |    2000 | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
    @BusinessAndIndustries @FineArtsStaticInternationalTransit @Cancellation @ClientAcceptance @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      2000  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/07/2021     |Property sold|
    @PropertyAndPlan @FineArtsStaticInternationalTransit @Cancellation @ClientAcceptance @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      2000  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/07/2021     |Property sold|
    @Cemetry @FineArtsStaticInternationalTransit @Cancellation @ClientAcceptance @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |    2000   | Hello VMIA          |Autom_Cemetry   | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/07/2021     |Property sold|


  Scenario Outline: Start a new policy application for GPA with deviation and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application
    @TransportVehicle @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @GovernmentEconomy @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Educational @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @LawAndJustice @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @EnvironmentAndWaters @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @BusinessAndIndustries @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @PropertyAndPlan @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Cemetry @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Autom_Cemetry   | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  Scenario Outline: Start a new policy application and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @TransportVehicle @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @GovernmentEconomy @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Educational @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @LawAndJustice @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @EnvironmentAndWaters @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @BusinessAndIndustries @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @PropertyAndPlan @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Cemetry @FineArtsStaticInternationalTransit @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA        |Autom_Cemetry   | Fine Art Exhibitions - International Transit             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |