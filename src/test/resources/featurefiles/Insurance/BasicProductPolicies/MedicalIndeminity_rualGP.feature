Feature: Medical Indemnity - Rural GP Product

  Scenario Outline: Start a new policy application for Medical Indemnity - Rural GP product and select client acceptance in client portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Add Testdocument attachment "Policy_attachment.txt"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I enter risk info "<revenue>" "<isGPRegister>" "<Aopplypolicy>" "<Retroactivecover>" "<EnableOrDisableProrate>"
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I logout of the application

    @MLP5_Sprint2 @Ambulance @MedicalIndemnity @NewBusiness @clientacceptance @InsuranceRegression
    Examples:
      | decsion |  Permium   | UnderwritingNotes  |  orgname         |product                    | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|Retroactivecover|Aopplypolicy|isGPRegister|revenue|
      | No      |    5000      | Hello VMIA        |  Auto_rualgp  |Medical Indemnity - Rural GP| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  | Yes             |Section1a   |Yes         |75,000 more|
#

  Scenario Outline: Start a new policy application for Medical Indemnity - Rural GP product and select client acceptance in risk advisor portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MPL4 @Ambulance @Professional_Indemnity @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|
      | yes      |    5000   | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567|Auto_rualgp   |Professional Indemnity              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                      |

  Scenario Outline: Start a new Medical Indemnity - Rural GP policy application and endorsement with riskAdvisor Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I logout of the application

    @MLP5_Sprint2 @Ambulance @MedicalIndemnity @PolicyEndorsemet @clientacceptance @Endorsement
    Examples:
      | decsion |  Permium   | UnderwritingNotes  |  orgname         |product                                              | status      | pendingstatus| uwstatus | finalstatus     | EnableOrDisableProrate|
      |No      |   5000     | Hello VMIA         |  Auto_rualgp   |Medical Indemnity - Rural GP| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |

  Scenario Outline: Start a new Medical Indemnity - Rural GP policy application and do endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

    @MLP5_Sprint2 @Ambulance @MedicalIndemnity @PolicyEndorsemet @clientacceptance @Endorsement
    Examples:
      | decsion |  Permium   | UnderwritingNotes  |  orgname         |product                                              | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|
      | No      |   5000     | Hello VMIA         |  Auto_rualgp   |Medical Indemnity - Rural GP| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |


  Scenario Outline: Start a new policy application for Medical Indemnity - Rural GP Annual product and cancel it
#    Given I have the external URL of VMIA
#    When User enter valid username  "<username>" and password "<password>" click on Login button
#    And I Select the Organization "<orgname>" for "<product>"
#    Then I want to start a new application for VMIA insurance
#    And click on the select button of the "<product>"
#    And Click UW Continue button
#    And Click UW Continue button
#    Then I Click Finish button
#    And I capture the policy ID
#    And I validate the "<status>"
#    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And validate the premium calculation for the "<cancellation>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @MLP5_Sprint2 @Ambulance @MedicalIndemnity @PolicyCancellation @clientacceptance  @Cancellation @FullTermCancellation
    Examples:
      | decsion |  Permium   | UnderwritingNotes  |  orgname    |product          | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |    5000    | Hello VMIA         |  Auto_rualgp|Medical Indemnity - Rural GP| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |yes             | 22/07/2021     |Property sold|
    @MLP5_Sprint2 @Ambulance @MedicalIndemnity @PolicyCancellation @clientacceptance  @Cancellation @MidTermCancellation
    Examples:
      | decsion |  Permium   | UnderwritingNotes  |  orgname             |product          | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |   5000      | Hello VMIA         |  Auto_rualgp|Medical Indemnity - Rural GP| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes                  |yes             | 22/07/2021     |Property sold|

  Scenario Outline: Start a new policy application for Medical Indemnity - Rural GP product and perform an Endorsement and then Cancel the policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Add Testdocument attachment "Policy_attachment.txt"
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And I enter risk info "<revenue>" "<isGPRegister>" "<Aopplypolicy>" "<Retroactivecover>" "<EnableOrDisableProrate>"
    And Enter the mandatoryInformation for medicalIndeminityrularGP and Click Submit
    When Now click on the approve quote
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    ### And Search the policy and begin the case
    ### Above step is replaced with the below three steps
    And I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue
    And Click on CaseID and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Myworkbaskets
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
#    And Enter the "<upPermium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And I enter risk info "<revenue>" "<isGPRegister>" "<Aopplypolicy>" "<isGPRegister>" "<EnableOrDisableProrate>"
    When Now click on the approve quote
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    ### There is no chance of getting display - policy number in below step(Displaying Pending-Payment)
    ### And I save the policy number
    And I click on Exit Button
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    ### Below step is newly added
#    When I click on Case ID after filter
    And validate the premium calculation for the "<cancellation>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    ### Duplicate steps found so below step got commented
    ### And I logout of the application (Pending-refund)

    @MLP5_Sprint2 @Ambulance @MedicalIndemnity @NewBusiness @clientacceptance @InsuranceRegression @RegressionMLP5Set1 @RegressionMLP5Set1Scenarios
    Examples:
      | decsion |  Permium   | UnderwritingNotes |  orgname      |product                     | status         | pendingstatus        | uwstatus                 | finalstatus     |EnableOrDisableProrate|Retroactivecover |Aopplypolicy|isGPRegister| revenue    | revenueUpdate            | IsCancelFullTerm|cancellationdate|Cancelreason | upPermium |
      | No      |    5000    | Hello VMIA        |  Auto_rualgp  |Medical Indemnity - Rural GP| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No                   | No              |Section1a   |Yes         | 75,000 more| Between 40,000 and 75,000| Yes             | 22/07/2021     |Property sold| 100       |