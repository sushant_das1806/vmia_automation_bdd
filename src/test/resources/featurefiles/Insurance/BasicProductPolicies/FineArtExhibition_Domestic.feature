Feature: Fine Arts-Domestic feature

  ### there is no product by the this name(based on the feature). Product name used for the below scenario's is the other feature product
  Scenario Outline: Start a new policy application for Fine Arts-Domestic client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application

    @TransportAndVehicle @FineArtsStaticDomestic @NewBusiness @ClientAcceptance  @InsuranceRegression @RegressionMLP4
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @GovernmentEconomy @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Educational @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @LawAndJustice @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @EnvironmentAndWaters @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @BusinessAndIndustries @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @PropertyAndPlan @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Cemetry @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | No      |      10  | Hello VMIA          |Autom_Cemetry   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline:Start a new policy application for Fine Arts-Domestic with RiskAdvisor client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @TransportVehicle @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @GovernmentEconomy @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Educational @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      |Yes      |      10  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @LawAndJustice @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @EnvironmentAndWaters @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @BusinessAndIndustries @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @PropertyAndPlan @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @Cemetry @FineArtsStaticDomestic @NewBusiness @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |
      | Yes      |      10  | Hello VMIA          |Autom_Cemetry   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |


  Scenario Outline: Cancelling the Fine Arts-Domestic policy
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @TransportVehicle @FineArtsStaticDomestic @Cancellation @ClientAcceptance @MidTermCancellation @RegressionMLP4
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus      |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |     2000  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT| No             | 22/12/2021     |Property sold|
    @GovernmentEconomy @FineArtsStaticDomestic @Cancellation @ClientAcceptance @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      2000 | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes           | 22/12/2021     |Property sold|
    @Educational @FineArtsStaticDomestic @Cancellation @ClientAcceptance @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |    2000  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/12/2021     |Property sold|
    @LawAndJustice @FineArtsStaticDomestic @Cancellation @ClientAcceptance @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000 | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |Yes           | 22/12/2021     |Property sold|
    @EnvironmentAndWaters @FineArtsStaticDomestic @Cancellation @ClientAcceptance @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |    2000 | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/12/2021     |Property sold|
    @BusinessAndIndustries @FineArtsStaticDomestic @Cancellation @ClientAcceptance @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      2000  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/12/2021     |Property sold|
    @PropertyAndPlan @FineArtsStaticDomestic @Cancellation @ClientAcceptance @MidTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |      2000  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | No             | 22/12/2021     |Property sold|
    @Cemetry @FineArtsStaticDomestic @Cancellation @ClientAcceptance @FullTermCancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |    2000   | Hello VMIA          |Autom_Cemetry   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes             | 22/12/2021     |Property sold|


  Scenario Outline: Start a new policy application for GPA with deviation and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<endorsementpremium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application
    @TransportVehicle @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | No      |      10  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13              |
    @GovernmentEconomy @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | No      |      10  | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13              |
    @Educational @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | No      |      10  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13              |
    @LawAndJustice @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | No      |      10  | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13              |
    @EnvironmentAndWaters @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | No      |      10  | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13              |
    @BusinessAndIndustries @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | No      |      10  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13              |
    @PropertyAndPlan @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | No      |      10  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13              |
    @Cemetry @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | No      |      10  | Hello VMIA          |Autom_Cemetry   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13              |


  Scenario Outline: Start a new policy application and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<endorsementpremium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @TransportVehicle @FineArtsStaticDomestic @Endrosement @ClientAcceptance @RegressionMLP4
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | Yes      |      10  | Hello VMIA          |Auto trans & vech| Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13             |
    @GovernmentEconomy @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | Yes      |      10  | Hello VMIA          |Auto Gov & Eco  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13             |
    @Educational @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | Yes      |      10  | Hello VMIA          |AutoEdu         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13             |
    @LawAndJustice @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | Yes      |      10  | Hello VMIA          |Auto Law & Jus  | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13             |
    @EnvironmentAndWaters @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | Yes      |      10  | Hello VMIA          |Auto E&w        | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13             |
    @BusinessAndIndustries @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | Yes      |      10  | Hello VMIA          |AutoB&I         | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13             |
    @PropertyAndPlan @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | Yes      |      10  | Hello VMIA          |AutoProp&plan   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13             |
    @Cemetry @FineArtsStaticDomestic @Endrosement @ClientAcceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus    |endorsementpremium|
      | Yes      |      10  | Hello VMIA        |Autom_Cemetry   | Fine Art Exhibitions - Static             | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |13             |





