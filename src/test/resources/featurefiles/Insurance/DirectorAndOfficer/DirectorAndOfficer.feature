Feature: Combined Liability feature

  Scenario Outline: Start a new Directors & Officers Liability
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And User submit the cover details
    And Click UW Continue button
    And I enter the Public Body details "Public Body" "Statutory Authority" "10" "20" "15" "18" "19"
    And Click UW Continue button
    And I enter the Director Details "David" "MBA" "24"
    And Click UW Continue button
    And I enter the Accounting and internal control practices
    And Click UW Continue button
    And I enter the employment practise details
    And Click UW Continue button
    And I enter POllution practices Details
    And Click UW Continue button
    And I enter Insurance and claims details
    And Click UW Continue button
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application

    @NewPolicy @Property_and_Planning @InsuranceRegression
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname        | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |
#      | No      |  2000      | Hello VMIA         | Auto claim P&P | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#      | No      |  2000      | Hello VMIA         |Claim_DepartmentalDivsion| Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#      | No      |  2000      | Hello VMIA         | Claim_PCP| Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#      | No      |  2000      | Hello VMIA         | Claim_HealthAndCommnutiy | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#      | No      |  2000      | Hello VMIA         | Claim_SpecialistAgenciesPP | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#      | No      |  2000      | Hello VMIA         | Claim_SpecialistAgencies | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
      | No      |  2000      | Hello VMIA         | Claim_PCP | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#      | No      |  2000      | Hello VMIA         | Claim_publichospital | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |






    @NewPolicy @DepartmentalDivision  @All
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                          | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000      | Hello VMIA         | Automation_DepartmentalDivision  | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @NewPolicy @DepartmentalDivision
    Examples:
      | decsion | Permium    | UnderwritingNotes  | orgname                           | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000      | Hello VMIA         | Automation_CS_CommunityHeathcare  | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

  Scenario Outline: Start a new Directors & Officers Liability and do cancellation

    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And User submit the cover details
    And Click UW Continue button
    And I enter the Public Body details "Public Body" "Statutory Authority" "10" "20" "15" "18" "19"
    And Click UW Continue button
    And I enter the Director Details "David" "MBA" "24"
    And Click UW Continue button
    And I enter the Accounting and internal control practices
    And Click UW Continue button
    And I enter the employment practise details
    And Click UW Continue button
    And I enter POllution practices Details
    And Click UW Continue button
    And I enter Insurance and claims details
    And Click UW Continue button
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application


    @NewPolicy @Property_and_Planning @Cancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname        | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         | Auto claim P&P | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
      | No      |  2000      | Hello VMIA         | Automation_CS_CommunityHeathcare |  Group Personal Accident CFA  | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|


    @NewPolicy @DepartmentalDivision @Cancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                          | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         | Automation_DepartmentalDivision  | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
      | No      |  2000      | Hello VMIA         | Automation_CS_CommunityHeathcare |  Group Personal Accident CFA  | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|

    @NewPolicy @CommunityHeathcareCenter @Cancellation
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                          | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
      | No      |  2000      | Hello VMIA         | Automation_CS_CommunityHeathcare | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
      | No      |  2000      | Hello VMIA         | Automation_CS_CommunityHeathcare |  Group Personal Accident CFA  | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|

#    @NewPolicy @BushNursingHospital @Cancellation
#    Examples:
#      | decsion | Permium   | UnderwritingNotes   | orgname                          | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
#      | No      |  2000      | Hello VMIA         | BushNursingHospital_Automation   | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
#      | No      |  2000      | Hello VMIA         | BushNursingHospital_Automation |  Group Personal Accident CFA  | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|

#    @NewPolicy @Ambulance @Cancellation
#    Examples:
#      | decsion | Permium   | UnderwritingNotes   | orgname              | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |IsCancelFullTerm|cancellationdate|Cancelreason|
#      | No      |  2000      | Hello VMIA         | Automation_Ambulance | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |yes             | 22/07/2021     |Property sold|
#      | No      |  2000      | Hello VMIA         | Automation_Ambulance |  Group Personal Accident CFA  | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |No             | 22/07/2021     |Property sold|



  Scenario Outline: Start a new Directors & Officers Liability and do Endorsment

    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And User submit the cover details
    And Click UW Continue button
    And I enter the Public Body details "Public Body" "Statutory Authority" "10" "20" "15" "18" "19"
    And Click UW Continue button
    And I enter the Director Details "David" "MBA" "24"
    And Click UW Continue button
    And I enter the Accounting and internal control practices
    And Click UW Continue button
    And I enter the employment practise details
    And Click UW Continue button
    And I enter POllution practices Details
    And Click UW Continue button
    And I enter Insurance and claims details
    And Click UW Continue button
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    Then Select EffectiveDate "24/08/2021" And Enter UpdateInfo "Test"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I logout of the application


    @NewPolicy @Property_and_Planning @Endrosement
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname        | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000      | Hello VMIA         | Auto claim P&P | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |

    @NewPolicy @DepartmentalDivision @Endrosement
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                          | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000      | Hello VMIA         | Automation_DepartmentalDivision  | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @NewPolicy @CommunityHeathcareCenter @Endrosement
    Examples:
      | decsion | Permium   | UnderwritingNotes   | orgname                          | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  2000      | Hello VMIA         | Automation_CS_CommunityHeathcare | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @NewPolicy @BushNursingHospital @Endrosement
#    Examples:
#      | decsion | Permium   | UnderwritingNotes   | orgname                          | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |
#      | No      |  2000      | Hello VMIA         | BushNursingHospital_Automation   | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
#    @NewPolicy @Ambulance @Endrosement
#    Examples:
#      | decsion | Permium   | UnderwritingNotes   | orgname              | product                        | status         | pendingstatus        | uwstatus                 | finalstatus     |
#      | No      |  2000      | Hello VMIA         | Automation_Ambulance | Directors & Officers Liability | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |




