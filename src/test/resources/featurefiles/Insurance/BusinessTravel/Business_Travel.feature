Feature: Business Travel feature

      Scenario Outline: Start a new BTV policy for Property and Planning client with no deviation and then endorse it - Client Portal
      Given I have the external URL of VMIA
      When User enter valid username  "<username>" and password "<password>" click on Login button
      And I Select the Organization "<orgname>" for "<product>"
      Then I want to start a new application for VMIA insurance
      When I select "<product>" and submit
      Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
      And Click UW Continue button
      Then I enter the start date of insurance "<startdate>"
      Then I enter International travel "<InternationalTrips>" details with NO travel to listed countries
      Then I enter Domestic travel "<DomesticTrips>" details
      Then I select No to travel exceeding 180 days
      And Click UW Continue button
      And I check the declaration
      Then I Click Finish button
      And I validate the "<status>"
      Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
      Then I accept the quote
      And I save the policy number
      Then I click the exit button
      And I Select the Organization "<orgname>" for "<product>"
      Then I select update policy from more actions
      Then I enter the start date of insurance "<startdate>"
      Then I enter International travel "<UpdatedInternationalTrips>" details with NO travel to listed countries
      Then I enter Domestic travel "<UpdatedDomesticTrips>" details
      Then I select No to travel exceeding 180 days
      And I do not want to change anymore values
      And Click UW Continue button
      And I check the declaration
      Then I Click Finish button
      And I validate the "<status>"
      Then I validate the endorsement premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<UpdatedInternationalTrips>" "<UpdatedDomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
      Then I accept the quote
      And I verify the payment for endorsement "<paymentstatus>"
      Then I logout of the application

#      @STPEndorsement @BTV @PropertynPlan
#      Examples:
#      | orgname         | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
#      |AutoProp&plan    | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |
#      @STPEndorsement @BTV @Cemetry
#      Examples:
#      | orgname         | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
#      |Autom_Cemetry    | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |
#      @STPEndorsement @BTV @Environment&water
#      Examples:
#      | orgname         | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
#      |Auto E&w         | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |
#      @STPEndorsement @BTV @Law&Justice
#      Examples:
#      | orgname         | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
#      |Auto Law & Jus  | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |
#      @STPEndorsement @BTV @DepartmentalDivision
#      Examples:
#      | orgname                       | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
#      |Automation_DepartmentalDivision| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |
#      @STPEndorsement @BTV @Automation_Ambulance
#      Examples:
#      | orgname            | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
#      |Automation_Ambulance| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |
#      @STPEndorsement @BTV @BushNursingHospital
#      Examples:
#      | orgname                      | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
#      |BushNursingHospital_Automation| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |
#      @STPEndorsement @BTV @MedicalResearchPP  @All
#      Examples:
#      | orgname                      | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
#      |Automation_Medical ResearchPP| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |
#      @STPEndorsement @BTV @PublicHospital
#      Examples:
#      | orgname                 | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
#      |Automation_PublicHospital| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |
#      @STPEndorsement @BTV @SpecialistAgencies
#      Examples:
#      | orgname                      | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
#      |Automation_Specialist_Agencies| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |
#      @STPEndorsement @BTV @SpecialistAgenciesPP
#      Examples:
#      | orgname                        | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
#      |Automation_Specialist_AgenciesPP| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |
            @STPEndorsement @BTV @CSCommunityHealthCare  @All @InsuranceRegression
            Examples:
                  | orgname                        | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |
                  |Automation_CS_CommunityHeathcare| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |



      Scenario Outline: Start a new BTV policy for Property and Planning client with deviation and then endorse it through Client Adviser portal - Client Portal
      Given I have the external URL of VMIA
      When User enter valid username  "<username>" and password "<password>" click on Login button
      And I Select the Organization "<orgname>" for "<product>"
      Then I want to start a new application for VMIA insurance
      When I select "<product>" and submit
      Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
      And Click UW Continue button
      Then I enter the start date of insurance "<startdate>"
      Then I enter International travel "<InternationalTrips>" details with NO travel to listed countries
      Then I enter Domestic travel "<DomesticTrips>" details
      Then I select Yes to travel exceeding 180 days and enter "<EstimateNumber>"
      And Click UW Continue button
      And I check the declaration
      Then I Click Finish button
      Then I check the status to be "<UWStatus>"
      Then I logout of the application
      Given I have the URL of VMIA
      When User when logs in as "Underwriter"
      When I search the case id that is routed and I begin the case
      Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
      And I accept the quote for uw
      Then I submit the case to get routed to the user
      Then I logout of the application
      Given I have the external URL of VMIA
      When User enter valid username  "<username>" and password "<password>" click on Login button
      And I Select the Organization "<orgname>" for "<product>"
      Then I open the approved case
      And I validate the "<status>"
      Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
      Then I accept the quote
      And I save the policy number
      Then I click the exit button
      Then I logout of the application
      Given I have the URL of VMIA
      When User when logs in as "ClientAdviser"
      Then I am logged in to the client adviser portal
      Then I click on update a policy
      Then I search and select org using "<orgname>" and "<zipcode>"
      Then I select the policy
      And Click UW Continue button
      Then I enter International travel "<UpdatedInternationalTrips>" details with NO travel to listed countries
      Then I enter Domestic travel "<UpdatedDomesticTrips>" details
      Then I select Yes to travel exceeding 180 days and enter "<EstimateNumber>"
      Then I enter the start date of insurance "<startdate>"
      And I do not want to change anymore values
      Then I note the case ID
      And Click UW Continue button
      And I check the declaration
      Then I Click Finish button
      And I verify the payment status "<UWStatus>"
      And I note down the case ID
      Then I logoff from client adviser portal
      Given I have the URL of VMIA
      When User when logs in as "ClientAdviser"
      Then I am logged in to the client adviser portal
      Then I select the case from Client Adviser UW Queue "<Queue_value>"
      And I Approve and submit the case
      Then I validate the endorsement premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<UpdatedInternationalTrips>" "<UpdatedDomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
      Then I accept the quote
      And I verify the payment for endorsement "<paymentstatus>"
      Then I logoff from client adviser portal


      @NonSTPEndorsement @PropertyAndPlanning @BTV
      Examples:
      | orgname          | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips | EstimateNumber |    UWStatus             | Queue_value  |
      | PAPautomation    | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |            200 |    PENDING-UNDERWRITING | Underwriting |
      @NonSTPEndorsement @Cemetery @BTV
      Examples:
      | orgname          | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips | EstimateNumber | UWStatus             | Queue_value  |
      | Autom_Cemetry    | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |            200 |  PENDING-UNDERWRITING  | Underwriting |
      @NonSTPEndorsement @Environment&Water @BTV
      Examples:
      | orgname          | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips | EstimateNumber |  UWStatus             | Queue_value  |
      | Auto E&w         | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |              30 |            200 |      PENDING-UNDERWRITING | Underwriting |
      @NonSTPEndorsement @Law&Justice @BTV
      Examples:
      | orgname          | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips | EstimateNumber |   UWStatus             | Queue_value  |
      | Auto Law & Jus   | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |              30 |            200 |     PENDING-UNDERWRITING | Underwriting |
      @NonSTPEndorsement @Education @BTV
      Examples:
      | orgname          | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips | EstimateNumber |   UWStatus             | Queue_value  |
      | AutoEdu          | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |              30 |            200 |    PENDING-UNDERWRITING | Underwriting |
      @NonSTPEndorsement @DepartmentalDivision @BTV
      Examples:
      | orgname                        | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips | EstimateNumber |    UWStatus             | Queue_value  |
      | Automation_DepartmentalDivision| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |              30 |            200 |      PENDING-UNDERWRITING | Underwriting |
      @NonSTPEndorsement @Ambulance @BTV
      Examples:
      | orgname            | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips | EstimateNumber |    UWStatus             | Queue_value  |
      |Automation_Ambulance| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |              30 |            200 |     PENDING-UNDERWRITING | Underwriting |
      @NonSTPEndorsement @BushNursingHospital @BTV
      Examples:
      | orgname                       | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips | EstimateNumber |  UWStatus             | Queue_value  |
      | BushNursingHospital_Automation| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |              30 |            200 |    PENDING-UNDERWRITING | Underwriting |
      @NonSTPEndorsement @BTV @MedicalResearchPP
      Examples:
      | orgname                      | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |EstimateNumber |  UWStatus             | Queue_value  |
      |Automation_MedicalResearchPP| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |    200 |     PENDING-UNDERWRITING | Underwriting |
      @NonSTPEndorsement @BTV @PublicHospital
      Examples:
      | orgname                 | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |EstimateNumber |  UWStatus             | Queue_value  |
      |Automation_PublicHospital| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |    200 |     PENDING-UNDERWRITING | Underwriting |
      @NonSTPEndorsement @BTV @SpecialistAgencies
      Examples:
      | orgname                      | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |EstimateNumber |   UWStatus             | Queue_value  |
      |Automation_Specialist_Agencies| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |    200 |    PENDING-UNDERWRITING | Underwriting |
      @NonSTPEndorsement@BTV @SpecialistAgenciesPP
      Examples:
      | orgname                        | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |EstimateNumber |   UWStatus             | Queue_value  |
      |Automation_Specialist_AgenciesPP| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |    200 |      PENDING-UNDERWRITING | Underwriting |
      @NonSTPEndorsement@BTV @CSCommunityHealthCare
      Examples:
      | orgname                        | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |EstimateNumber |   UWStatus             | Queue_value  |
      |Automation_CS_CommunityHeathcare| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |    200 |      PENDING-UNDERWRITING | Underwriting |



      Scenario Outline: Start a new BTV policy for Property and Planning client with no deviation and then cancel it
      Given I have the external URL of VMIA
      When User enter valid username  "<username>" and password "<password>" click on Login button
      And I Select the Organization "<orgname>" for "<product>"
      Then I want to start a new application for VMIA insurance
      When I select "<product>" and submit
      Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
      And Click UW Continue button
      Then I enter the start date of insurance "<startdate>"
      Then I enter International travel "<InternationalTrips>" details with NO travel to listed countries
      Then I enter Domestic travel "<DomesticTrips>" details
      Then I select No to travel exceeding 180 days
      And Click UW Continue button
      And I check the declaration
      Then I Click Finish button
      And I validate the "<status>"
      Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
      Then I accept the quote
      And I save the policy number
      Then I click the exit button
      And I Select the Organization "<orgname>" for "<product>"
      Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
      And Now capture the policy ID
      Then I logout of the application
      When I have the URL of VMIA
      When User when logs in as "Underwriter"
      When I search the case id that is routed and I begin the case
      And I accept the quote for uw
      Then I submit the case to get routed to the user
      Then I logout of the application

#      @STPCancellation @BTV @PropertynPlan
#      Examples:
#      | orgname         | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |AutoProp&plan    | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
#      @STPCancellation @BTV @Cemetry
#      Examples:
#      | orgname         | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Autom_Cemetry    | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
#      @STPCancellation @BTV @Environment&water
#      Examples:
#      | orgname         | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Auto E&w         | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
#      @STPCancellation @BTV @Law&Justice
#      Examples:
#      | orgname         | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Auto Law & Jus  | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
#      @STPCancellation @BTV @DepartmentalDivision
#      Examples:
#      | orgname                       | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Automation_DepartmentalDivision| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
#      @STPCancellation @BTV @Ambulance
#      Examples:
#      | orgname            | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Automation_Ambulance| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
#      @STPCancellation @BTV @BushNursingHospital
#      Examples:
#      | orgname                      | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |BushNursingHospital_Automation| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
            @STPCancellation @BTV @MedicalResearchPP
            Examples:
                  | orgname                      | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                 |IsCancelFullTerm|cancellationdate|Cancelreason|
                  |Automation_MedicalResearchPP| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE |yes             | 22/07/2021     |Property sold|
            @STPCancellation @BTV @PublicHospital
            Examples:
                  | orgname                 | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   |IsCancelFullTerm|cancellationdate|Cancelreason|
                  |Automation_PublicHospital| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE |yes             | 22/07/2021     |Property sold|
            @STPCancellation @BTV @SpecialistAgencies
            Examples:
                  | orgname                      | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | IsCancelFullTerm|cancellationdate|Cancelreason|
                  |Automation_Specialist_Agencies| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE |yes             | 22/07/2021     |Property sold|
            @STPCancellation @BTV @SpecialistAgenciesPP
            Examples:
                  | orgname                        | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | IsCancelFullTerm|cancellationdate|Cancelreason|
                  |Automation_Specialist_AgenciesPP| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | yes             | 22/07/2021     |Property sold|
            @STPCancellation @BTV @CSCommunityHealthCare
            Examples:
                  | orgname                        | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | IsCancelFullTerm|cancellationdate|Cancelreason|
                  |Automation_CS_CommunityHeathcare| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | yes             | 22/07/2021     |Property sold|

      Scenario Outline: Start a new BTV client with deviation and then cancel the application
            Given I have the external URL of VMIA
            When User enter valid username  "<username>" and password "<password>" click on Login button
            And I Select the Organization "<orgname>" for "<product>"
            Then I want to start a new application for VMIA insurance
            When I select "<product>" and submit
            Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
            And Click UW Continue button
            Then I enter the start date of insurance "<startdate>"
            Then I enter International travel "<InternationalTrips>" details with NO travel to listed countries
            Then I enter Domestic travel "<DomesticTrips>" details
            Then I select Yes to travel exceeding 180 days and enter "<EstimateNumber>"
            And Click UW Continue button
            And I check the declaration
            Then I Click Finish button
            Then I check the status to be "<UWStatus>"
            Then I logout of the application
            Given I have the URL of VMIA
            When User when logs in as "Underwriter"
            When I search the case id that is routed and I begin the case
            Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
            And I accept the quote for uw
            Then I submit the case to get routed to the user
            Then I logout of the application
            Given I have the external URL of VMIA
            When User enter valid username  "<username>" and password "<password>" click on Login button
            And I Select the Organization "<orgname>" for "<product>"
            Then I open the approved case
            And I validate the "<status>"
            Then I validate the premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
            Then I accept the quote
            And I save the policy number
            Then I click the exit button
            And I Select the Organization "<orgname>" for "<product>"
            Then click more actions and cancel the policy using the following information
                  |<IsCancelFullTerm>|
                  |<cancellationdate>|
                  |<Cancelreason>    |
            And Now capture the policy ID
            Then I logout of the application
            When I have the URL of VMIA
            When User when logs in as "Underwriter"
            When I search the case id that is routed and I begin the case
            And I accept the quote for uw
            Then I submit the case to get routed to the user
            Then I logout of the application

#      @NonSTPCancelation @BTV @AutoProp&plan
#      Examples:
#      | orgname         | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |AutoProp&plan    | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
#      @NonSTPCancelation @BTV @Cemetry
#      Examples:
#      | orgname         | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Autom_Cemetry    | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
#      @NonSTPCancelation @BTV @Environment&water
#      Examples:
#      | orgname         | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Auto E&w         | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
#      @NonSTPCancelation @BTV @Law&Justice
#      Examples:
#      | orgname         | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Auto Law & Jus  | BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
#      @NonSTPCancelation @BTV @DepartmentalDivision
#      Examples:
#      | orgname                       | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Automation_DepartmentalDivision| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
#      @NonSTPCancelation @BTV @Automation_Ambulance
#      Examples:
#      | orgname            | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Automation_Ambulance| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
#      @NonSTPCancelation @BTV @BushNursingHospital
#      Examples:
#      | orgname                      | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | paymentstatus   | UpdatedInternationalTrips | UpdatedDomesticTrips |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |BushNursingHospital_Automation| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |                        10 |                   30 |yes             | 22/07/2021     |Property sold|
            @NonSTPCancelation @BTV @MedicalResearchPP
            Examples:
                  | orgname                      | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                 |IsCancelFullTerm|cancellationdate|Cancelreason|
                  |Automation_MedicalResearchPP| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE |yes             | 22/07/2021     |Property sold|
            @NonSTPCancelation @BTV @PublicHospital
            Examples:
                  | orgname                 | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   |IsCancelFullTerm|cancellationdate|Cancelreason|
                  |Automation_PublicHospital| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE |yes             | 22/07/2021     |Property sold|
            @NonSTPCancelation @BTV @SpecialistAgencies
            Examples:
                  | orgname                      | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | IsCancelFullTerm|cancellationdate|Cancelreason|
                  |Automation_Specialist_Agencies| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE |yes             | 22/07/2021     |Property sold|
            @NonSTPCancelation @BTV @SpecialistAgenciesPP
            Examples:
                  | orgname                        | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | IsCancelFullTerm|cancellationdate|Cancelreason|
                  |Automation_Specialist_AgenciesPP| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | yes             | 22/07/2021     |Property sold|
            @NonSTPCancelation @BTV @CSCommunityHealthCare
            Examples:
                  | orgname                        | product | startdate   | InternationalTrips | DomesticTrips | InternationalMultiplier | DomesticMultiplier | gst | InternationalStampduty | DomesticStampduty | status                   | IsCancelFullTerm|cancellationdate|Cancelreason|
                  |Automation_CS_CommunityHeathcare| BTV     | currentdate |                  3 |            10 |                   38.00 |                1.6 | 0.1 |                   0.01 |               0.1 | PENDING-CLIENTACCEPTANCE | yes             | 22/07/2021     |Property sold|