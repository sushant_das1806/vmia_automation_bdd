Feature: GPA Volunteer

  @MLP3Sprint1 @UW @GPA @PropAndPlan @NewApplication
  Scenario Outline: Start a new GPA policy with no deviation and then endorse it - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
#    And I Select the Organization "<orgname>" for "<product>"
    And I Select the Organization "<orgname>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and details of persons "<volunteers>"
    And I select No to previous GPA policy details
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
#    And I validate the "<status>"
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Organization "<orgname>" for "<product>"
    Then I select update policy from more actions
    Then I enter updated values of PnP GPA "<startdate>" and details of persons "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>"
    And I select No to previous GPA policy details
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I validate the endorsement premium of PnP GPA for "<volunteers>" "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>" from "<startdate>" using values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logout of the application

#    @GPAEndorsement @PropertyAndPlan @STPEndorsement @GPAVolunteer
#    Examples:
#  | orgname          | product | volunteers | updatedvolunteers | boardMembers | workExperienceStudents | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   |
#  | Automation PAP   | GPA     |       2000 |              6000 |           50 |                   5000 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |
    @GPAEndorsement @DepartmentalDivision @STPEndorsement @GPAVolunteer @RegressionMLP5Set2 @RegressionMLP5Set2Scenarios
    Examples:
      | orgname                         | product | volunteers | updatedvolunteers | boardMembers | workExperienceStudents | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | UWStatus |
      | Automation_DepartmentalDivision | GPA     |       2000 |              6000 |           50 |                   5000 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | PENDING-UNDERWRITING |

  @MLP3Sprint1 @UW @GPA @PropAndPlan @NewApplication @Test2
  Scenario Outline: Start a new GPA policy with deviation and then endorse it through Client Adviser portal - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and details of persons "<volunteers>"
    And I select No to previous GPA policy details
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    Then I logout of the application
    ##### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    And I validate the "<status>"
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application
    ##### Client Adviser - Apply Endorsement and accept quote
    Given I have the URL of VMIA
    When I enter the "<clientadviserusername>"
    And I enter "<clientadviserpassword>"
    And I click login button
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I search and select org using "<orgname>" and "<zipcode>"
    Then I select the policy
    And Click UW Continue button
    Then I enter updated values of PnP GPA "<startdate>" and details of persons "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>"
    And I select No to previous GPA policy details
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<UWStatus>"
    And I note down the case ID
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
    Then I validate the endorsement premium of PnP GPA for "<volunteers>" "<updatedvolunteers>" "<boardMembers>" "<workExperienceStudents>" from "<startdate>" using values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logoff from client adviser portal

    @GPAEndorsement @PropertyAndPlan @Endorsement @GPAVolunteer @InsuranceRegression
    Examples:
      | orgname          | product | volunteers | updatedvolunteers | boardMembers | workExperienceStudents | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation PAP | GPA     |     101000 |            106000 |           50 |                   5000 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |
    @GPAEndorsement @DepartmentalDivision @Endorsement @GPAVolunteer
    Examples:
      | orgname          | product | volunteers | updatedvolunteers | boardMembers | workExperienceStudents | multiplier | gst | stampduty | startdate   | status                   | paymentstatus   | uwusername     | uwpassword  | zipcode | clientadviserusername | clientadviserpassword | UWStatus             | Queue_value  |
      | Automation_DepartmentalDivision | GPA     |     101000 |            106000 |           50 |                   5000 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | underwriter1 | Welcome@321 |    3000 | AutoClientAdviser     | Welcome@1234          | PENDING-UNDERWRITING | Underwriting |

  @MLP3Sprint1 @UW @GPA @PropAndPlan @NewApplication @Cancellation @MLP4Story
  Scenario Outline: Start a new GPA policy with no deviation and then Cancel it - Client Portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Prop and Planning Organisation
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter "<startdate>" and details of persons "<volunteers>"
    And I select No to previous GPA policy details
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium of PnP GPA for "<volunteers>" using values "<startdate>" "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I save the policy number
    Then I click the exit button
    And I Select the Organization "<orgname>" for "<product>"
    Then I cancel a policy from more actions
    Then I check the case status to be "<UWStatus>"
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile
    Then I logout of the application

    @GPACancellation @Property_and_Planning @GPAVolunteer
    Examples:
      | orgname                   | product | volunteers | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      | Automation Cemetery Trust | GPA     |       2000 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |
    @GPACancellation @DepartmentalDivision @GPAVolunteer
    Examples:
      | orgname                   | product | volunteers | multiplier | gst | stampduty | startdate   | status                   | UWStatus             |
      | Automation_DepartmentalDivision | GPA     |       2000 |          1 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-UNDERWRITING |
