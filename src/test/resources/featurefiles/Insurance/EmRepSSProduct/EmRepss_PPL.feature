Feature: EmRepss - PPL policy creation of from client adviosr portal

  Scenario Outline: Start a new policy application for EmRepss policy
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    And click on the select button of the "<product>" in Risk Advisor
    And Click UW Continue button
    And Enter the policy effective date "<PolicyStartDate>" and capture the Expirydate
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And validate the pro rata premium for the emRepss policy "Creation"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MPL4 @EmRepSSPPL @NewBusiness @clientacceptance @GovernmentEconomy @RegressionMLP4
    Examples:
      |orgname        |product            | status               | uwstatus                 | finalstatus     |PolicyStartDate |
      |Auto Gov & Eco |Public & Products Liability - EmRePSS   | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | 10/11/2021     |



  Scenario Outline: Start a new policy application for EmRepss policy with deviation and cancel the policy
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    Then I click on make a new application
    Then I enter "<orgname>" and Click find button from Newapplication page
    And click on the select button of the "<product>" in Risk Advisor
    And Click UW Continue button
    And Enter the policy effective date "<PolicyStartDate>" and capture the Expirydate
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And validate the pro rata premium for the emRepss policy "Creation"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    ### And Search the policy and begin the case
    ### Above step is replaced with the below two steps
    And Click on Casenumber and then enter ID in Search text field and click Apply button
    And After filter I select first Case ID from the table in Mycases
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number from RiskAdvisor
    Then I logoff from client adviser portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
#    And validate the pro rata premium for the emRepss policy "Cancellation"
    And validate the return premium for the emRepss policy "Cancellation"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @MPL4 @GovernmentEconomy @EmRepSSPPL @MidTermCancellation @clientacceptance @RegressionMLP4
    ### Created new Organization here. The organization exist was - Auto Gov & Eco
    Examples:
      |orgname           |product                                | status               | uwstatus                 | finalstatus     |PolicyStartDate |IsCancelFullTerm|cancellationdate|Cancelreason    |
      |Auto Health & Com |Public & Products Liability - EmRePSS  | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | 21/11/2021     | No             | 22/12/2021     |Property sold   |
    @MPL4 @GovernmentEconomy @EmRepSSPPL @FullTermCancellation @clientacceptance
    Examples:
      |orgname        |product                       | status               | uwstatus                 | finalstatus     |PolicyStartDate |IsCancelFullTerm|cancellationdate|Cancelreason    |
      |Auto Gov & Eco |Public & Products Liability - EmRePSS| PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | 21/11/2021     | Yes            | 22/12/2021     |Property sold           |
