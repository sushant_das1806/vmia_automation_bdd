Feature: Cyber Product

  Scenario Outline: Start a new policy application for CYBER
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the revenue details "<priviousrevenue>" "<cuurentrevenue>"
    And Click Continue button
    Then I enter details in the Data protection "<ByWhom>" "<provide_details>" Screen
    And Click Continue button
    And I enter the details in Data access and recovery Screen
    And Click Continue button
    And I enter the details in  Outsourcing activities Screen
    And Click Continue button
    And I enter the details Claims information Screen
    And Click Continue button
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application

#    @Cyber @PropertyAndPlan @NewBusiness
#    Examples:
#      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      | Permium|UnderwritingNotes|finalstatus    |uwstatus                |EnableOrDisableProrate|
#      | Cyber   |             100 |            200 | ect    | testing         |PAPautomation|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|Default Yes|
#    @Cyber @DepartmentalDivision @NewBusiness  @InsuranceRegression
#    Examples:
#      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      				  | Permium|UnderwritingNotes|finalstatus    |uwstatus                |EnableOrDisableProrate|
#      | Cyber   |             100 |            200 | ect    | testing         |Automation_DepartmentalDivision|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|Default Yes|
#    @Cyber @CommunityHeathcare @NewBusiness
#    Examples:
#      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      				   | Permium|UnderwritingNotes|finalstatus    |uwstatus                |EnableOrDisableProrate|
#      | Cyber   |             100 |            200 | ect    | testing         |Automation_CS_CommunityHeathcare|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|Default Yes|
#    @Cyber @Ambulance @NewBusiness
    Examples:
      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      	   | Permium|UnderwritingNotes|finalstatus    |uwstatus                |EnableOrDisableProrate|
      | Cyber   |             100 |            200 | ect    | testing         |Automation_Ambulance|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|Default Yes|
#    @Cyber @BushNursingHospital @NewBusiness
#    Examples:
#      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      				| Permium|UnderwritingNotes|finalstatus    |uwstatus                |EnableOrDisableProrate|
#      | Cyber   |             100 |            200 | ect    | testing         |BushNursingHospital_Automation|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|Default Yes|




  Scenario Outline: Start a new policy application for CYBR and do cancellation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the revenue details "<priviousrevenue>" "<cuurentrevenue>"
    And Click Continue button
    Then I enter details in the Data protection "<ByWhom>" "<provide_details>" Screen
    And Click Continue button
    And I enter the details in Data access and recovery Screen
    And Click Continue button
    And I enter the details in  Outsourcing activities Screen
    And Click Continue button
    And I enter the details Claims information Screen
    And Click Continue button
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    Then I close the external link
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and "<EnableOrDisableProrate>" prorata
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<OrgName>" for "<product>"
    Then click more actions and cancel the policy using the following information
      |<IsCancelFullTerm>|
      |<cancellationdate>|
      |<Cancelreason>    |
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
#    @Cyber @PropertyAndPlan @Cancellation
#    Examples:
#      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      | Permium|UnderwritingNotes|finalstatus    |uwstatus                |IsCancelFullTerm|cancellationdate|Cancelreason|EnableOrDisableProrate|
#      | Cyber   |             100 |            200 | ect    | testing         |PAPautomation|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|yes             | 22/07/2021     |Property sold|Default Yes|
#    @Cyber @DepartmentalDivision @Cancellation
#    Examples:
#      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      				  | Permium|UnderwritingNotes|finalstatus    |uwstatus                |IsCancelFullTerm|cancellationdate|Cancelreason|EnableOrDisableProrate|
#      | Cyber   |             100 |            200 | ect    | testing         |Automation_DepartmentalDivision|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|yes             | 22/07/2021     |Property sold|Default Yes|
#    @Cyber @CommunityHeathcare @Cancellation
#    Examples:
#      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      				   | Permium|UnderwritingNotes|finalstatus    |uwstatus                |IsCancelFullTerm|cancellationdate|Cancelreason|EnableOrDisableProrate|
#      | Cyber   |             100 |            200 | ect    | testing         |Automation_CS_CommunityHeathcare|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|yes             | 22/07/2021     |Property sold|Default Yes|
#    @Cyber @Ambulance @Cancellation
#    Examples:
#      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      	   | Permium|UnderwritingNotes|finalstatus    |uwstatus                |IsCancelFullTerm|cancellationdate|Cancelreason|EnableOrDisableProrate|
#      | Cyber   |             100 |            200 | ect    | testing         |Automation_Ambulance|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|yes             | 22/07/2021     |Property sold|Default Yes|
    @Cyber @BushNursingHospital @Cancellation
    Examples:
      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      				| Permium|UnderwritingNotes|finalstatus    |uwstatus                |IsCancelFullTerm|cancellationdate|Cancelreason|EnableOrDisableProrate|
      | Cyber   |             100 |            200 | ect    | testing         |BushNursingHospital_Automation|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|yes             | 22/07/2021     |Property sold| Default Yes|



  Scenario Outline: Start a new policy application for CYBR and do endrosement
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the revenue details "<priviousrevenue>" "<cuurentrevenue>"
    And Click Continue button
    Then I enter details in the Data protection "<ByWhom>" "<provide_details>" Screen
    And Click Continue button
    And I enter the details in Data access and recovery Screen
    And Click Continue button
    And I enter the details in  Outsourcing activities Screen
    And Click Continue button
    And I enter the details Claims information Screen
    And Click Continue button
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    Then I close the external link
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    And I click on Exit Button
    And I Select the Organization "<OrgName>" for "<product>"
    And Select policy and Click on Update Policy
    Then Select EffectiveDate "24/08/2021" And Enter UpdateInfo "Test"
    Then I logout of the application
    Given I have the URL of VMIA
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I logout of the application

#    @Cyber @PropertyAndPlan @Endrosement
#    Examples:
#      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      | Permium|UnderwritingNotes|finalstatus    |uwstatus                |
#      | Cyber   |             100 |            200 | ect    | testing         |PAPautomation|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|
    @Cyber @DepartmentalDivision @Endrosement
    Examples:
      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      				  | Permium|UnderwritingNotes|finalstatus    |uwstatus                |
      | Cyber   |             100 |            200 | ect    | testing         |Automation_DepartmentalDivision|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|
    @Cyber @CommunityHeathcare @Endrosement
    Examples:
      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      				   | Permium|UnderwritingNotes|finalstatus    |uwstatus                |
      | Cyber   |             100 |            200 | ect    | testing         |Automation_CS_CommunityHeathcare|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|
    @Cyber @Ambulance @Endrosement
    Examples:
      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      	   | Permium|UnderwritingNotes|finalstatus    |uwstatus                |
      | Cyber   |             100 |            200 | ect    | testing         |Automation_Ambulance|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|
    @Cyber @BushNursingHospital @Endrosement
    Examples:
      | product | priviousrevenue | cuurentrevenue | ByWhom | provide_details |OrgName      				| Permium|UnderwritingNotes|finalstatus    |uwstatus                |
      | Cyber   |             100 |            200 | ect    | testing         |BushNursingHospital_Automation|500     |test             |PENDING-PAYMENT|PENDING-CLIENTACCEPTANCE|
