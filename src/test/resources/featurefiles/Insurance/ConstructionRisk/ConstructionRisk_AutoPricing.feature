Feature: Contruction Risks Auto Pricing feature

      Scenario Outline: Start a new Construction Risks policy for Property and Planning client and then endorse it using Client Adviser Portal
      Given I have the external URL of VMIA
      When User enter valid username  "<username>" and password "<password>" click on Login button
      And I Select the Organization "<OrgClientSegment>"
      Then I want to start a new application for VMIA insurance
      And click on the select button of the "Construction Risks - Material Damage and Liability Annual"
      ### And click on the select button of the "Construction Risks – Material Damage and Liability Annual"
      Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
      And Click UW Continue button
      Then I fill the Constrution Risk information questions
      Then I click ViewEdit all contracts button
      Then I click on Add item button
      Then I fill the contract details "<ContractType>" "<EstimatedProjectValue>" and click on submit
      ### Then I click Add button to add a contract
      ### Then I fill contract details "<ContractType>" "<EstimatedProjectValue>"
      ### Then I fill the contract location details
      ### Then I fill the Optional Extensions details "<LargestSingleContractVal>"
      And Click UW Continue button
      And I check the declaration
      Then I Click Finish button
      And I validate the status of the case to be "PENDING-UNDERWRITING"
      And I click on Exit Button
      Then I logout of the application
      Given I have the URL of VMIA
      When User when logs in as "Underwriter"
      When I search the case id that is routed and I begin the case
      When I click on Case ID after filter
      Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
      And I accept the quote for uw
      Then I submit the case to get routed to the user
      Then I logout of the application
      ##### Client - Accept the quote
      Given I have the external URL of VMIA
      When User enter valid username  "<username>" and password "<password>" click on Login button
      And I Select the Organization "<OrgClientSegment>"
      Then I open the approved case
      And I validate the status of the case to be "PENDING-CLIENTACCEPTANCE"
      Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
      Then I accept the quote
      And I validate the status of the case to be "PENDING-PAYMENT"
      And I save the policy number
      Then I logout of the application

#      @PropertyAndPlanning @Construction @NewApplication
#      Examples:
#      |OrgClientSegment | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      | AutoProp&plan   | CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#      @Cemetry @Construction @NewApplication
#      Examples:
#      |OrgClientSegment | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      |Autom_Cemetry    | CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#
#      @Environment&Water @Construction @NewApplication
#      Examples:
#      |OrgClientSegment | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      |Auto E&w         | CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#
#      @Law&justice @Construction @NewApplication
#      Examples:
#      |OrgClientSegment | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      | Auto Law & Jus  | CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#
#      @Education @Construction @NewApplication
#      Examples:
#      |OrgClientSegment | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      | AutoEdu         | CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#      @CommunityHeathcare @Construction @NewApplication
#      Examples:
#      |OrgClientSegment                 | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      | Automation_CS_CommunityHeathcare| CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#      @Automation_Ambulance @Construction @NewApplication
#      Examples:
#      |OrgClientSegment     | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      | Automation_Ambulance| CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
      @SpecialistAgencies @Construction @NewBusiness @InsuranceRegression @Verified
      Examples:
      |OrgClientSegment                 | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
      | Automation_Specialist_Agencies  | CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#      @PublicHospital @Construction @NewBusiness
#      Examples:
#      |OrgClientSegment         | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      |Automation_PublicHospital| CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |

#      Scenario Outline: Start a new Construction Risks policy for Property and Planning client and then endorse it using Client Adviser Portal
#      Given I have the external URL of VMIA
#      When User enter valid username  "<username>" and password "<password>" click on Login button
#      And I Select the Organization "<OrgClientSegment>"
#      Then I want to start a new application for VMIA insurance
#      And click on the select button of the "Construction Risks – Material Damage and Liability Annual"
#      Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
#      And Click UW Continue button
#      Then I fill the Constrution Risk information questions
#      Then I click Add button to add a contract
#      Then I fill contract details "<ContractType>" "<EstimatedProjectValue>"
#      Then I fill the contract location details
#      Then I fill the Optional Extensions details "<LargestSingleContractVal>"
#      And Click UW Continue button
#      And I check the declaration
#      Then I Click Finish button
#      And I validate the status of the case to be "PENDING-UNDERWRITING"
#      And I click on Exit Button
#      Then I logout of the application
#      Given I have the URL of VMIA
#      When User when logs in as "Underwriter"
#      When I search the case id that is routed and I begin the case
#      Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
#      And I accept the quote for uw
#      Then I submit the case to get routed to the user
#      Then I logout of the application
#      ##### Client - Accept the quote
#      Given I have the external URL of VMIA
#      When User enter valid username  "<username>" and password "<password>" click on Login button
#      And I Select the Organization "<OrgClientSegment>"
#      Then I open the approved case
#      And I validate the status of the case to be "PENDING-CLIENTACCEPTANCE"
#      Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
#      Then I accept the quote
#      And I validate the status of the case to be "PENDING-PAYMENT"
#      And I save the policy number
#      Then I logout of the application
#      ##### Client Adviser - Apply Endorsement
#      Given I have the URL of VMIA
#      When User when logs in as "ClientAdviser"
#      Then I am logged in to the client adviser portal
#      Then I click on update a policy
#      Then I search and select org using client segment "<OrgClientSegment>"
#      Then I select the policy
#      And Click UW Continue button
#      Then I fill updated contract details "<NewContractType>" "<NewEstimatedProjectValue>"
#      And Click UW Continue button
#      And I check the declaration
#      Then I Click Finish button
#      And I validate the status of the case to be "PENDING-UNDERWRITING"
#      And I note down the case ID
#      Then I logoff from client adviser portal
#      ##### Underwriter - Submit the quote
#      Given I have the URL of VMIA
#      When User when logs in as "Underwriter"
#      When I search the case id that is routed and I begin the case
#      Then I validate the endorsement premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<NewEstimatedProjectValue>" "<NewMaterialDamageMul>" "<NewLiabilityMul>" "<gst>" "<stampduty>"
#      And I accept the quote for uw
#      Then I submit the case to get routed to the user
#      Then I logout of the application
#      #### Client Adviser - Accept quote
#      Given I have the URL of VMIA
#      When User when logs in as "ClientAdviser"
#      Then I am logged in to the client adviser portal
#      Then I select the case from Client Adviser My cases tab
#      Then I validate the endorsement premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<NewEstimatedProjectValue>" "<NewMaterialDamageMul>" "<NewLiabilityMul>" "<gst>" "<stampduty>"
#      Then I accept the quote
#      And I validate the status of the case to be "PENDING-PAYMENT"
#      Then I logoff from client adviser portal
#      @PropertyAndPlanning @Construction @Endorsement
#      Examples:
#      |OrgClientSegment | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      | AutoProp&plan   | CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#
#      @Cemetry @Construction @Endorsement
#      Examples:
#      |OrgClientSegment | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      |Autom_Cemetry    | CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#
#      @Environment&Water @Construction @Endorsement
#      Examples:
#      |OrgClientSegment | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      |Auto E&w         | CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#
#      @Law&justice @Construction @Endorsement
#      Examples:
#      |OrgClientSegment | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      | Auto Law & Jus  | CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#      @Education @Construction @Endorsement
#      Examples:
#      |OrgClientSegment | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      | AutoEdu         | CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#
#      @CommunityHeathcare @Construction @Endorsement
#      Examples:
#      |OrgClientSegment                 | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      | Automation_CS_CommunityHeathcare| CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#
#      @Ambulance @Construction @Endorsement
#      Examples:
#      |OrgClientSegment     | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      | Automation_Ambulance| CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#      @SpecialistAgencies @Construction @Endorsement
#      Examples:
#      |OrgClientSegment                 | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      | Automation_Specialist_Agencies  | CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#      @PublicHospital @Construction @Endorsement
#      Examples:
#      |OrgClientSegment         | product| ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty | startdate | NewContractType                     | NewEstimatedProjectValue | NewMaterialDamageMul | NewLiabilityMul | LargestSingleContractVal |
#      |Automation_PublicHospital| CON    | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 | pastdate  | Level crossing removal - rail under |                   786542 |                 0.14 |            0.15 |                   156000 |
#
#
#      @MLP4Sprint5 @UW @CON @PropAndPlan @NewApplication1234332 @RegMLP3
#      Scenario Outline: Start a new Construction Risks policy for Property and Planning client and then cancel from client portal
#      Given I have the external URL of VMIA
#      When User enter valid username  "<username>" and password "<password>" click on Login button
#      And I Select the Organization "<orgname>" for "<product>"
#      Then I want to start a new application for VMIA insurance
#      And click on the select button of the "Construction Risks – Material Damage and Liability Annual"
#      Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
#      And Click UW Continue button
#      Then I fill the Constrution Risk information questions
#      Then I click Add button to add a contract
#      Then I fill contract details "<ContractType>" "<EstimatedProjectValue>"
#      Then I fill the contract location details
#      Then I fill the Optional Extensions details "<LargestSingleContractVal>"
#      And Click UW Continue button
#      And I check the declaration
#      Then I Click Finish button
#      And I validate the status of the case to be "PENDING-UNDERWRITING"
#      And I click on Exit Button
#      Then I logout of the application
#      ##### Underwriter - Submit the quote
#      Given I have the URL of VMIA
#      When User when logs in as "Underwriter"
#      When I search the case id that is routed and I begin the case
#      Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
#      And I accept the quote for uw
#      Then I submit the case to get routed to the user
#      Then I logout of the application
#      Given I have the external URL of VMIA
#      When User enter valid username  "<username>" and password "<password>" click on Login button
#      And I Select the Prop and Planning Organisation
#      Then I open the approved case
#      And I validate the status of the case to be "PENDING-CLIENTACCEPTANCE"
#      Then I validate the premium of Construction Risks using "<EstimatedProjectValue>" "<materialDamageMul>" "<LiabilityMul>" "<gst>" "<stampduty>"
#      Then I accept the quote
#      And I validate the status of the case to be "PENDING-PAYMENT"
#      And I save the policy number
#      Then I logout of the application
#      Given I have the external URL of VMIA
#      When User enter valid username  "<username>" and password "<password>" click on Login button
#      And I Select the Organization "<orgname>" for "<product>"
#      Then click more actions and cancel the policy using the following information
#      |<IsCancelFullTerm>|
#      |<cancellationdate>|
#      |<Cancelreason>    |
#      And Now capture the policy ID
#      Then I logout of the application
#      Given I have the URL of VMIA
#      When User when logs in as "Underwriter"
#      When I search the case id that is routed and I begin the case
#      And I accept the quote for uw
#      Then I submit the case to get routed to the user
#      Then I logout of the application
#
##      @Construction @Cancellation @Property&Planning
#      @test
#      Examples:
#      |orgname       |  product | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty |  LargestSingleContractVal |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |AutoProp&plan |  CON     | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 |156000                     |yes             | 22/07/2021     |Property sold|
#
#      @Construction @Cancellation @Cemetry
#      Examples:
#      |orgname       |  product | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty |  LargestSingleContractVal |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Autom_Cemetry |  CON     | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 |156000                     |yes             | 22/07/2021     |Property sold|
#
#      @Construction @Cancellation  @Environment&Water
#      Examples:
#      |orgname       |  product | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty |  LargestSingleContractVal |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Auto E&w   |  CON     | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 |156000                     |yes             | 22/07/2021     |Property sold|
#
#      @Construction @Cancellation @Law&justice
#      Examples:
#      |orgname       |  product | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty |  LargestSingleContractVal |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Auto Law & Jus |  CON     | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 |156000                     |yes             | 22/07/2021     |Property sold|
#
#      @Construction @Cancellation @Education
#      Examples:
#      |orgname       |  product | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty |  LargestSingleContractVal |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |AutoEdu|  CON     | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 |156000                     |yes             | 22/07/2021     |Property sold|
#
#      @Construction @Cancellation @CommunityHeathcare
#      Examples:
#      |orgname       |  product | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty |  LargestSingleContractVal |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Automation_CS_CommunityHeathcare|  CON     | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 |156000                     |yes             | 22/07/2021     |Property sold|
#
#      @Construction @Cancellation @Ambulance
#      Examples:
#      |orgname       |  product | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty |  LargestSingleContractVal |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Automation_Ambulance|  CON     | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 |156000                     |yes             | 22/07/2021     |Property sold|
#
#      @Construction @Cancellation @CommunityHeathcare
#      Examples:
#      |orgname                       |  product | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty |  LargestSingleContractVal |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Automation_Specialist_Agencies|  CON     | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 |156000                     |yes             | 22/07/2021     |Property sold|
#
#      @Construction @Cancellation @Ambulance
#      Examples:
#      |orgname                  |  product | ContractType             | EstimatedProjectValue | materialDamageMul | LiabilityMul | gst | stampduty |  LargestSingleContractVal |IsCancelFullTerm|cancellationdate|Cancelreason|
#      |Automation_PublicHospital|  CON     | Electrical - substations |                556700 |              0.15 |          0.1 | 0.1 |       0.1 |156000                     |yes             | 22/07/2021     |Property sold|
#
