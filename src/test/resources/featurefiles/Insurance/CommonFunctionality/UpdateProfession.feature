Feature: Update Profession in Profession Indmitnity

  Scenario Outline: Start a new policy application for professional Indeminity  product and select client acceptance in client portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
#    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Update Profession "Tests" and select "Yes" whether same value need to be updated in the organization
    And Add Dedctible for the professional Indeminity
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then Verify the updated profession "tetsTests" in client Acceptance
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

    @MPL4 @Auto_Law_Jus @Professional_Indemnity @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | manoj.ramanathan@areteanstech.com | Test@1234567 |Auto Law & Jus   |Professional Indemnity              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |



  Scenario Outline: Start a new policy application for CYBR with deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    And Enter the Annual renveue "<OperationalRevenue>" "<VicrorianRevenue>" "<OtherFund>"
    And Enter the Incident, OverStateWork, AffilationWorkDetails
    And Enter the Storage Hazard details "<activityCasuedDamege>" "<presenceWater>"
    And Enter the Product, Airfield Liablity and Contract info "<Contractorinfo>"
    And Click UW Continue button
    And Add visitor info "<NoOfVisitor>" "<StreetName>" "<Subrubtown>" "<State>" "<postcode>"
    And Enter the Professional Indeminity info
    And Enter the insurance history details
    And Enter the Professional ActivityInfo
    And Enter the Join ventures and overseaswork
    And Enter the contract info for liablilty
    And Enter the Risk management details
    And Enter the claim circumstances details
    And I check the declaration
    Then I Click Finish button
    Then I logout of the application
    And I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
#    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Update Profession "Tests" and select "Yes" whether same value need to be updated in the organization
    And Add Dedctible for the professional Indeminity
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then Verify the updated profession "tetsTests" in client Acceptance
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application


    Examples:
      | product           |OrgName        |OperationalRevenue|VicrorianRevenue|OtherFund|Subrubtown|NoOfVisitor|State   |postcode|activityCasuedDamege|StreetName|presenceWater |Contractorinfo                                         |
      | Combined liability|PAPautomation  |500               |600             |700      |Sydney    |3          |Victoria|6421    |Not applicable      |Main Road|Not applicable|We do not engage Contractors, Sub Contractors or Agents|