Feature:  Policy history table


  Scenario Outline: Validate policy history details are displayed properly in the pop-up
    Given I have the URL of VMIA
    And User when logs in as "Underwriter"
    And click "Search" after clicking the Hamburger
    And Search the policy number "<Policynumber>"
    And Open the policy and click on the history table
    When Clicking on the dollor icon
    Then Pop up should display the Premimum details

    Examples:
    |Policynumber|
    |V018161     |
