Feature: zero-Premimum feature
  @zeroPremium @MLP4_Patch @ResolveCompleted
  Scenario Outline: Start a new policy application for basic product
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then I accept the quote by selecting no in the invoice no
    And I validate the "<finalstatus>"
    @MPL4 @Auto_pro_plan @Group_Personal_Accident_Heritage_Divers @PolicyCreation @clientacceptance
    Examples:
      | decsion | Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |
      | No      |  0      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w        |  Group Personal Accident - Alpine Reserve | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | RESOLVED-COMPLETED |

  @zeroPremium @MLP4_Patch @ResolveCompleted @endorsement
  Scenario Outline: Start a new policy application and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<EndorsePremium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<EndrosementStatus>"
    Then I logout of the application

    @MPL4 @Auto_pro_plan @Group_Personal_Accident_Heritage_Divers @PolicyCreation @clientacceptance
    Examples:
      | decsion | Permium   | EndorsePremium|UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus             | finalstatus     |EndrosementStatus |
      | Yes      |  2000    |     0         | Hello VMIA         | pavan.gangone@areteanstech.com|Areteans@1981| Auto E&w       |  Group Personal Accident - Alpine Reserve| PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT |RESOLVED-COMPLETED|