Feature: New Orgnization feature


  Scenario Outline: Add unicorprated orgnization to Organzation and check the same is displayed while creating bundle policy
    Given I have the URL of VMIA
    When I enter the "<username>"
    And I enter "<password>"
    And I click login button
    Then I am logged in to the client adviser portal
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click CreateNew button
    And Verify the dropdown values of orgtype for "<ClientSegment>" and Select orgnizationType "<orgnizationType>"
    And Enter the Orgdetails "<ClientSegment>" "<GovernmentDepartment>" "<Profession>" "1234567890" "Sydney "
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click UpdateButton
    And Update the following orgnization details "Yes" "No" "14/06/2021" "Updateorg" "Orgdetail"
    And Add Unincorprate org "Test" in the orgnization
    And Click New and Select "Manage contact"
    Then Search the contact using contactid "CONT-27587"
    And Add the contact to Organzation to the contact "" "Insurance;Finanace"
    Then Verify case status "RESOLVED-COMPLETED" and capture the case id
    And I logoff from client adviser portal

    Examples:
      | username          | password     |ClientSegment          |GovernmentDepartment                |Profession|orgnizationType|
      | AutoClientAdviser | Welcome@1234 |Community Health Centre|Department of Education and Training|health    |Section 25A    |



  Scenario Outline: Create Orgnization from examples
    Given I have the URL of VMIA
    And User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Click New and Select "Manage organisation"
    And Search the new Orgname and click CreateNew button
    And Verify the dropdown values of orgtype for "<ClientSegment>" and Select orgnizationType "<orgnizationType>"
    And Enter the Orgdetails "<ClientSegment>" "<GovernmentDepartment>" "<Profession>" "1234567890" "Sydney "
    And Click New and Select "Manage contact"
    Then Search contact using name "<FistName>" "<LastName>"
    And Add the contact to Organzation to the contact "" "Insurance;Finanace"
    Then Verify case status "RESOLVED-COMPLETED" and capture the case id
    And I logoff from client adviser portal

    Examples:
      | username          | password     |ClientSegment          |GovernmentDepartment                |Profession|orgnizationType|FistName|LastName|
      | AutoClientAdviser | Welcome@1234 |Community Health Centre|Department of Education and Training|health    |Section 25A    |Tester  |Auto    |

  @OrgCreation
  Scenario: Create Orgnization from data sheet
    Given I have the URL of VMIA
    And User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Extract the data from the sheet "Sheetname"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    And User when logs in as "Finance"
    Then I open the "Payway Account" workbasket in the finance portal
    And I open the Payway Case and enter the Payway Number
