Feature: Motor Vehicle - Victoria Police

  Scenario Outline: Create Motor vehicle from client portal VicFleet
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<OrgName>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the required details "<ExecutiveVehicles1>""<ExecutiveVehicles1>" "<carrying vehicles>" for Vehicle details page
    And Click UW Continue button
    Then I enter the required details for Drivers page
    And Click UW Continue button
    Then I enter the required details for Insurance history page
    And Click UW Continue button
    Then I enter the requred details for Accumulation page
    And Click UW Continue button
    Then I enter the reuired details for Summary and review page
    Then I Click Finish button
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "<Underwriter>"
    When I search the case id that is routed and I begin the case
    Then I individually provide the "<basepremium>" and "<notes>"
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "PENDING-PAYMENT"
    Then I logout of the application
    @MLP5_Sprint2 @CSCommunityHealthCare
    Examples:
      |OrgName                 | Underwriter | product                         | ExecutiveVehicles1 | ExecutiveVehicles1 | carrying vehicles | basepremium | notes   |status                  |
      |LawAndjustinceRegression| Underwriter | Motor Vehicle - Victoria Police |                  1 |                  2 | testing           |         200 | Testing |PENDING-CLIENTACCEPTANCE|