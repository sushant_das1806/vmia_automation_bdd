Feature: Motor Vehicle

  @MLP3Sprint1 @UW @Motor @NewApplication
  Scenario Outline: Start a new Motor policy with no deviation and then endorse it - Client Portal

    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And I note down the case ID
    And Click UW Continue button
    And I enter the Number of Vehicle "<number>" and submit
    And Click UW Continue button
    And Enter the new employement drivers
    And Click UW Continue button
    And I submit the Insurance history
    And Click UW Continue button
    And I submit the Accumulation
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    And I note the case ID
    Then I logout of the application
        #### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<status>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I select update policy from more actions
    And I update the endorsement for Motor Vehicle
    Then I Click Finish button
    Then I validate the status of the case to be "<status>"
    And I save the policy number
    Then I logout of the application
  #### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<status>"
    Then I logout of the application
   ##### Client - Accept the quote
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
#  Then I validate the endorsement premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<UpdatedInternationalTrips>" "<UpdatedDomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    And I save the policy number
    Then I logout of the application


    @Endorsement @Motor @BushNursingHospital  @All @InsuranceRegression
    Examples:
      | orgname                         | product           | startdate   | status                   | paymentstatus   | Permium|UnderwritingNotes|finalstatus      | UWStatus             |number| uwusername     | uwpassword|
      |  BushNursingHospital_Automation | Motor Vehicle     | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | 500     |test             |PENDING-PAYMENT | PENDING-UNDERWRITING |100   | underwriter1 | Welcome@321 |
    @Endorsement @Motor @DepartmentalDivision
    Examples:
      | orgname                         | product           | startdate   | status                   | paymentstatus   | Permium|UnderwritingNotes|finalstatus      | UWStatus             |number| uwusername     | uwpassword|
      |  Automation_CS_CommunityHeathcare | Motor Vehicle     | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | 500     |test             |PENDING-PAYMENT | PENDING-UNDERWRITING |100   | underwriter1 | Welcome@321 |


  Scenario Outline: Start a new Motor policy with deviation and then endorse it through Client Adviser portal - Client Portal

    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And I enter the Number of Vehicle "<number>" and submit
    And Click UW Continue button
    And Enter the new employement drivers
    And Click UW Continue button
    And I submit the Insurance history
    And Click UW Continue button
    And I submit the Accumulation
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    And I save the policy number
    Then I logout of the application
  #### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<UWStatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I click on update a policy
    Then I search and select org using "<orgname>" and "<zipcode>"
    Then I select the policy
    And Click UW Continue button
    And I update the endorsement for Motor Vehicle
    And I do not want to change anymore values
    Then I note the case ID
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I verify the payment status "<UWStatus>"
    And I note down the case ID
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "ClientAdviser"
    Then I am logged in to the client adviser portal
    Then I select the case from Client Adviser UW Queue "<Queue_value>"
    And I Approve and submit the case
  #    Then I validate the endorsement premium of BTV policy using values "<startdate>" "<InternationalTrips>" "<DomesticTrips>" "<UpdatedInternationalTrips>" "<UpdatedDomesticTrips>" "<InternationalMultiplier>" "<DomesticMultiplier>" "<gst>" "<InternationalStampduty>" and "<DomesticStampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<paymentstatus>"
    Then I logoff from client adviser portal

    @GPAEndorsement @DepartmentalDivision
    Examples:
      | orgname                         | product           | startdate   | status                   | paymentstatus   | Permium|UnderwritingNotes|finalstatus      | UWStatus             |number| uwusername     | uwpassword|
      |  BushNursingHospital_Automation | Motor Vehicle     | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | 500     |test             |PENDING-PAYMENT | PENDING-UNDERWRITING |100   | underwriter1 | Welcome@321 |
    @GPAEndorsement @DepartmentalDivision
    Examples:
      | orgname                         | product           | startdate   | status                   | paymentstatus   | Permium|UnderwritingNotes|finalstatus      | UWStatus             |number| uwusername     | uwpassword|
      |  Automation_CS_CommunityHeathcare | Motor Vehicle     | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | 500     |test             |PENDING-PAYMENT | PENDING-UNDERWRITING |100   | underwriter1 | Welcome@321 |

  Scenario Outline: Start a new BTV policy for Property and Planning client with no deviation and then cancel it - Client Portal

    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And I enter the Number of Vehicle "<number>" and submit
    And Click UW Continue button
    And Enter the new employement drivers
    And Click UW Continue button
    And I submit the Insurance history
    And Click UW Continue button
    And I submit the Accumulation
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<UWStatus>"
    And I save the policy number
    Then I logout of the application
        #### Underwriter - Submit the quote
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the "<Permium>" and "<UnderwritingNotes>" and Submit the Policy
    And I validate the "<UWStatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I click the exit button
    And I Select the Organization "<orgname>" for "<product>"
    Then I cancel a policy from more actions
    Then I check the case status to be "<UWStatus>"
    And I note down the case ID
    Then I click the exit button
    Then I verify that More actions is disabled for the policy tile
    Then I logout of the application

    @Motor @Cancellation @Deviation @DepartmentalDivision
    Examples:
      | orgname                         | product           | startdate   | status                   | paymentstatus   | Permium|UnderwritingNotes|finalstatus      | UWStatus             |number| uwusername     | uwpassword|
      |  BushNursingHospital_Automation | Motor Vehicle     | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | 500     |test             |PENDING-PAYMENT | PENDING-UNDERWRITING |100   | underwriter1 | Welcome@321 |
    @Motor @Cancellation @Deviation @DepartmentalDivision
    Examples:
      | orgname                         | product           | startdate   | status                   | paymentstatus   | Permium|UnderwritingNotes|finalstatus      | UWStatus             |number| uwusername     | uwpassword|
      |  Automation_CS_CommunityHeathcare | Motor Vehicle     | currentdate | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | 500     |test             |PENDING-PAYMENT | PENDING-UNDERWRITING |100   | underwriter1 | Welcome@321 |
