Feature: Public product liabitlity product

  Scenario Outline: Start a new policy application for basic product and select client acceptance in client portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Add Dedctible for the PPL policy
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"

    @MPL4 @Auto_Law_Jus @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#  @MPL4 @AutoB&I @Public_Products_Liability @PolicyCreation @clientacceptance
#  Examples:
#    | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#    | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoB&I   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#  @MPL4 @AutoEdu @Public_Products_Liability @PolicyCreation @clientacceptance
#  Examples:
#    | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#    | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoEdu   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#  @MPL4 @Auto_pro_plan @Public_Products_Liability @PolicyCreation @clientacceptance
#  Examples:
#    | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#    | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
#  @MPL4 @Auto_E_w @Public_Products_Liability @PolicyCreation @clientacceptance
#  Examples:
#    | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
#    | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w    |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |


  Scenario Outline: Start a new policy application for basic product and select client acceptance in risk advisor portal
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Add Dedctible for the PPL policy
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal


    #
    @MPL4 @Auto_Law_Jus @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @AutoB&I @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoB&I   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @AutoEdu @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoEdu   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @Auto_pro_plan @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @Auto_E_w @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | yes    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w    |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |  ABCD124   |


  Scenario Outline: Start a new policy application for new Products with deviation and do full term cancellation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Add Dedctible for the PPL policy
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    Then I cancel a policy from more actions
    And Now capture the policy ID
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application

    @MPL4 @Auto_Law_Jus @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @AutoB&I @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoB&I   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @AutoEdu @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoEdu   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @Auto_pro_plan @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @Auto_E_w @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w    |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |

  Scenario Outline: Start a new policy application and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Add Dedctible for the PPL policy
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Add Dedctible for the PPL policy
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logoff from client adviser portal

    @MPL4 @Auto_Law_Jus @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @AutoB&I @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoB&I   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @AutoEdu @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoEdu   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @Auto_pro_plan @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | yes      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @Auto_E_w @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | yes    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w    |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |  ABCD124   |

  Scenario Outline: Start a new policy application for GPA with deviation and endorsement with client Acceptance
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>" for "<product>"
    Then I want to start a new application for VMIA insurance
    And click on the select button of the "<product>"
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Add Dedctible for the PPL policy
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>" for "<product>"
    And Select policy and Click on Update Policy
    And Click UW Continue button
    And Click UW Continue button
    Then I Click Finish button
    And I capture the policy ID
    And I validate the "<status>"
    Then I logout of the application
    Given I have the URL of VMIA
    When User when logs in as "RiskAdvisor"
    Then I am logged in to the client adviser portal
    And Search the policy and begin the case
    And Select the Policy decision "<decsion>" and Submit the Application
    And I validate the "<pendingstatus>"
    Then I logoff from client adviser portal
    Given I have the URL of VMIA
    When User when logs in as "Underwriter"
    When I search the case id that is routed and I begin the case
    And Enter the premium "<Permium>" and uwnotes "<UnderwritingNotes>"
    And Add Dedctible for the PPL policy
    And Submit the Quoate
    And I validate the "<uwstatus>"
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>" for "<product>"
    Then I open the approved case
    Then I accept the quote
    And I validate the "<finalstatus>"
    Then I logout of the application

    @MPL4 @Auto_Law_Jus @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto Law & Jus   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @AutoB&I @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoB&I   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @AutoEdu @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |AutoEdu   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @Auto_pro_plan @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto pro & plan   |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |
    @MPL4 @Auto_E_w @Public_Products_Liability @PolicyCreation @clientacceptance
    Examples:
      | decsion |  Permium   | UnderwritingNotes  | username                       | password      | orgname         |product                                    | status         | pendingstatus        | uwstatus                 | finalstatus     |Yes/No | purchaseno |
      | No      |    10      | Hello VMIA         | pavan.gangone@areteanstech.com | Areteans@1981 |Auto E&w    |Public & Products Liability              | PENDING-REVIEW | PENDING-UNDERWRITING | PENDING-CLIENTACCEPTANCE | PENDING-PAYMENT | Yes   | ABCD124    |