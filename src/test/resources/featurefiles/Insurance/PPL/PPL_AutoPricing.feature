Feature: PPL Autopricing

  Scenario Outline: Start a new policy application for Liability with no deviation and risk question as yes and endorse it
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<orgname>" for "<product>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    #Then I enter the "<startdate>" of the insurance application
    Then I enter the Start Date  of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    #And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>"
    Then I want to update a policy for Liability
    And Click UW Continue button
    # Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<updatedhiredays>"
    And I do not want to change anymore values
    #And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the endorsement premium for liability for "<basepremium>" "<updatedbasepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set3  @InsuranceRegression
    Examples:
      | username                       | password      | product | basepremium | updatedbasepremium | paymentstatus   | endpaymentstatus | hiredays          | updatedhiredays | gst | stampduty | startdate | client_number | status                   | OrgName     |
      | pavan.gangone@areteanstech.com | Areteans@1981 | PPL     |         320 |                560 | PENDING-PAYMENT | PENDING-PAYMENT  | Less than 50 days | 50 to 100 days  | 0.1 |       0.1 | 1/07/2020 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | Lowanna7 |


  Scenario Outline: Start a new policy application for Liability with no deviation and risk question as yes and endorse it negative premium
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>" for "<product>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    #Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    #Then I enter the "<startdate>" of the insurance application
    Then I enter the Start Date  of the insurance application
    Then I answer the risk questions as yes with "<hiredays>"
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for liability for "<basepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    #And I verify the payment "<paymentstatus>"
    #And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<orgname>" for "<product>"
    #Then I want to update a policy
    Then I want to update a policy for Liability
    # Then I select the policy to be updated
    And Click UW Continue button
    # Then I enter the "<startdate>" of the insurance application
    Then I answer the risk questions as yes with "<updatedhiredays>"
    And I do not want to change anymore values
    #And I do not need cancellation cover
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    # Then I validate the endorsement premium for liability for "<basepremium>" "<updatedbasepremium>" "<gst>" "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set3 @Regression_MLP1_Set1_golive_26_Rerun1234 @Regression_MLP1_Set1_golive_26
    Examples:
      | username                       | password      | product | basepremium | updatedbasepremium | paymentstatus   | endpaymentstatus | hiredays       | updatedhiredays   | gst | stampduty | startdate | client_number | status                   | name      |
      | pavan.gangone@areteanstech.com | Areteans@1981 | PPL     |         560 |                320 | PENDING-PAYMENT | PENDING-REFUND   | 50 to 100 days | Less than 50 days | 0.1 |       0.1 | 1/07/2020 | BUNIT-9514    | PENDING-CLIENTACCEPTANCE | Lowanna10 |
