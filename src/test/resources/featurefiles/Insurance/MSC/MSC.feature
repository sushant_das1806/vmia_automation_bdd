Feature: MSC Product

  Scenario Outline: Start a new policy application for MSC with no deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>" for "<product>"
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then I validate the premium for "<parkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set2  @All
    Examples:
      | username                       | password      | product | parkingslots | multiplier | gst | stampduty | startdate   | status                   | days | paymentstatus   | name     |
      | pavan.gangone@areteanstech.com | Areteans@1981 | MSC     |          400 |         12 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |  98 | PENDING-PAYMENT | Lowanna7 |


  Scenario Outline: Start a new policy application for MSC with deviation
    Given I have the external URL of VMIA
    When User enter valid username  "<username>" and password "<password>" click on Login button
    And I Select the Organization "<name>" for "<product>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    Then I check the status to be "<pendingstatus>"
    Then I logout of the application
    Then I close the external link
    Given I have the URL of VMIA
    When I enter the "<uwusername>"
    And I enter "<uwpassword>"
    And I click login button
    When I search the case id that is routed and I begin the case
    And I accept the quote for uw
    Then I submit the case to get routed to the user
    Then I logout of the application
    Then I close the external link
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>" for "<product>"
    Then I open the approved case
    And I validate the "<status>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set1_golive_26
    Examples:
      | username                       | password      | uwusername     | uwpassword  | product | parkingslots | multiplier | gst | stampduty | startdate | status                   | days | paymentstatus   | pendingstatus        | name     |
      | pavan.gangone@areteanstech.com | Areteans@1981 | underwriter_AT | Welcome@321 | MSC     |          400 |         12 | 0.1 |       0.1 | 1/07/2020 | PENDING-CLIENTACCEPTANCE |  365 | PENDING-PAYMENT | PENDING-UNDERWRITING | Lowanna7 |


  Scenario Outline: Start a new policy application and then make Endorsement for MSC
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>" for "<product>"
    #Then I am logged in to the insurance portal
    Then I want to start a new application for VMIA insurance
    When I select "<product>" and submit
    # Then I am routed to UW Client Details page and validate the fields for "<product>" type loss
    And Click UW Continue button
    Then I enter the number of "<parkingslots>" and "<startdate>"
    And Click UW Continue button
    And I validate the number of "<parkingslots>"
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    Then Now I validate the premium for "<parkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    #Then I validate the premium for "<parkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment "<paymentstatus>"
    # And I save the policy number
    Then I logout of the application
    Given I have the external URL of VMIA
    When I enter the "<username>" for azure
    And I enter "<password>" for azure
    And I click login button for azure
    And I Select the Organization "<name>" for "<product>"
    #Then I want to update a policy
    Then I want to update a policy for MSC
    # Then I select the policy to be updated
    And Click UW Continue button
    Then I enter the number of "<updatedparkingslots>" and "<startdate>"
    And I do not want to change anymore values
    And Click UW Continue button
    And I check the declaration
    Then I Click Finish button
    And I validate the "<status>"
    # Then I validate the endorsement premium for "<parkingslots>" "<updatedparkingslots>" for "<days>" values "<multiplier>" "<gst>" and "<stampduty>"
    Then I accept the quote
    And I verify the payment for endorsement "<endpaymentstatus>"
    Then I logout of the application

    @Regression_MLP1_Set2 @Regression_MLP1_Set1_golive_26
    Examples:
      | username                       | password      | product | parkingslots | updatedparkingslots | multiplier | gst | stampduty | startdate   | status                   | days | endpaymentstatus | paymentstatus   | name     |
      | pavan.gangone@areteanstech.com | Areteans@1981 | MSC     |          400 |                 500 |         12 | 0.1 |       0.1 | currentdate | PENDING-CLIENTACCEPTANCE |  203 | PENDING-PAYMENT  | PENDING-PAYMENT | Lowanna7 |