
package com.cucumber.framework.stepDef;
import java.io.FileInputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cucumber.framework.GeneralHelperSel.CSVFileReader;
import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import com.cucumber.framework.PageObjects.HomePageLoc;
import com.cucumber.framework.configreader.ObjectRepo;
import com.cucumber.framework.configreader.PropertyFileReader;
import com.cucumber.framework.context.TestContext;
import com.cucumber.framework.utility.ResourceHelper;
import io.cucumber.java.en.*;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;
import org.testng.asserts.SoftAssert;

public class UWStepDefinition {

	TestContext context;
	public UWStepDefinition(){

	}
	public UWStepDefinition(TestContext cont){
		context=cont;
	}
	public boolean orgCreated=false;
	public String strGST = "0.1";
	public String strStampDuty = "0.1";
	public String strGPAMultiplier = "5.5";
	public static String organizaionname;

	public int strSchoolPropertyLessThan250KMinPremium = 950;


	@Then("I want to start a new application for VMIA insurance")
	public void i_want_to_start_a_new_application_for_VMIA_insurance() throws Exception {
		SeleniumFunc.waitFor(10);
		context.getHomePage().clickNewApplication();
		SeleniumFunc.waitFor(10);
		context.getData().setData("MultiYear","No");
	}

	@Then("I want to start a new application for client adviser")
	public void i_want_to_make_a_claim_client_adviser() throws Exception {
		context.getHomePage().clickMakeClaim();
		// SeleniumFunc.waitFor(3);
		SeleniumFunc.waitFor(3);
	}

	@Then("I want to click on show my details")
	public void i_want_to_click_on_show_my_details() throws Exception {

		context.getHomePage().clickMakeClaim();

		// SeleniumFunc.waitFor(5);
		SeleniumFunc.waitFor(3);
	}

	@Then("I want to update a policy")
	public void i_want_to_update_a_policy() throws Exception {

		context.getHomePage().clickUpdatePolicy();


		//SeleniumFunc.waitFor(5);
		SeleniumFunc.waitFor(5);
	}

	@Then("I want to update a policy for Liability")

	public void i_want_to_update_a_policy_for_Liability() throws Exception {

		context.getHomePage().clickUpdatePolicyforLiability();

	}

	@Then("I want to update a policy for Property")

	public void i_want_to_update_a_policy_for_Property() throws Exception {

		context.getHomePage().clickUpdatePolicyforPro();

	}

	@Then("I want to update a policy for MVF")

	public void i_want_to_update_a_policy_for_MVF() throws Exception {

		context.getHomePage().clickUpdatePolicyforMVF();

	}

	@Then("I want to update a policy for GPA")

	public void i_want_to_update_a_policy_for_GPA() throws Exception {

		context.getHomePage().clickUpdatePolicyforGPA();

	}

	@Then("I want to update a policy for MSC")

	public void i_want_to_update_a_policy_for_MSC() throws Exception {

		context.getHomePage().clickUpdatePolicyforMSC();

	}

	@Then("I select the policy to be updated")
	public void i_select_the_policy_to_be_updated() throws Exception {

		context.getUnderWritingPage().searchAndOpenPolicyForEndorsement();


		SeleniumFunc.waitFor(10);
	}

	@When("I select {string} and submit")
	public void i_want_to_select_loss_for_UW(String loss) throws Exception {
		//ObjectRepo.reader.getProduct();
		context.getData().setData("Product","Product");
		if (loss.equals("GPA")) {
			context.getUnderWritingPage().selectGPA();
		}else if (loss.equals("PRO")) {
			context.getUnderWritingPage().selectPRO();
		}else if (loss.equals("PPL")) {
			context.getUnderWritingPage().selectLiability();
		}else if (loss.equals("MSC")) {
			context.getUnderWritingPage().selectMotorContingency();
		}else if (loss.equals("MVF")) {
			context.getUnderWritingPage().selectMotorVehicle();
		}else if (loss.equals("JRN")) {
			context.getUnderWritingPage().selectJourney();
		}else if (loss.equals("BTV")) {
			context.getUnderWritingPage().selectBusinessTravel();
		}else if (loss.equals("CON")) {
			context.getUnderWritingPage().selectConstructionRisks();
		}else if(loss.equals("RnD"))
			context.getUnderWritingPage().selectRnDProduct();
		else if(loss.equals("Fine Art Exhibitions - Static"))
			context.getUnderWritingPage().selectProduct(loss);
		else {
			System.out.println("The case '"+ loss +"' is not added to i_want_to_select_loss_for_UW function");
		}
		if(loss.equals("MLNCB"))
		{
			context.getUnderWritingPage().selectMotorLossofNoClaimBonus();
		}
		//context.getUnderWritingPage().clickSubmitUWLossType(); submit button discontinued from MLP2 start as part of ui/uxchanges
		if(loss.equals("CBL"))
		{
			context.getUnderWritingPage().SelectCBL();
		}

	}

	@Then("I am routed to UW Client Details page and validate the fields for {string} type loss")
	public void I_am_routed_to_uw_client(String loss) throws Exception {

		context.getUnderWritingPage().fetchCaseID();
		SeleniumFunc.waitFor(5);
	}

	@And("I save the policy number")
	public void i_extract_policynumber() throws Exception {
		String casestatusscreen = SeleniumFunc.xpath_Genericmethod_getElementText("//span[@data-test-id='202005302009370459202']/following-sibling::p");
			Pattern p = Pattern.compile("V([0-9]+)");
			Matcher m = p.matcher(casestatusscreen);
			m.matches();
			if (m.find()) {
				context.getData().setPolicynumber( "V" + m.group(1));
			}
	}

	@And("I save the policy number from RiskAdvisor")
	public void i_extract_policynumberFromRiskAdvisor() throws Exception {
		context.getUnderWritingPage().savePolicyNumberRiskAdviosr();
	}

	@Given("Open Asset from {string} Underwriter Queue")
	public void open_Asset_from_Underwriter_Queue(String queue) throws Exception {
		context.getUnderWritingPage().openCaseFromUnderwriterQueue(queue);
	}

	@Then("Perform {string} action on the asset and Select {string} in Endorsement descion for Asset")
	public void perform_action_on_the_asset(String action,String endorseneed) throws Exception {
		context.getUnderWritingPage().ActionOnAsset(action,endorseneed);
	}




	@And("I do not want to change anymore values")
	public void i_dont_change_any_other_values() throws Exception {
		context.getUnderWritingPage().moreEndorsementsQues();
	}

	@Then("I click the exit button")
	public void click_Exitbutton() throws Exception {
		context.getUnderWritingPage().clickExitButton();
	}

	@Then("I click the exit button in claim")
	public void click_Exitbutton_inclaim() throws Exception {
		context.getUnderWritingPage().clickExitButtonInClaims();
	}

	@And("I do not need cancellation cover")
	public void i_dont_need_canc_cover() throws Exception {
		context.getUnderWritingPage().cancellationQuesProEndorse();
	}

	@And("Validate {string}")
	public void validate_clientnumber(String clientnumber) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().verifyClientNumber(clientnumber);
	}

	@Then("I enter number of {string} and {string}")
	public void i_enter_number_of_students(String students, String date) throws Exception {

		context.getUnderWritingPage().enterStartDateOfInsurance(date);
		SeleniumFunc.waitFor(1);
		context.getUnderWritingPage().enterNumberOfStudents(students);
	}

	@Then("value of students given {string}")
	public void i_enter_number_of_string(String students) throws Exception {

		context.getUnderWritingPage().enterStartDateOfInsurance1();
		SeleniumFunc.waitFor(1);
		context.getUnderWritingPage().enterNumberOfStudents(students);
	}

	@Then("I enter the number of {string} and {string}")
	public void i_enter_number_of_parking(String parking, String date) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().enterStartDateOfInsurance(date);
		SeleniumFunc.waitFor(1);
		context.getUnderWritingPage().enterNumberOfParkingSlots(parking);
	}
	/*
	 * @Then("I enter the number of {string}") public void
	 * i_enter_number_of_parking(String parking) throws Exception {
	 * SeleniumFunc.switchToDefaultContent(); context.getUnderWritingPage().enterStartDateOfInsurance(date);
	 * context.getUnderWritingPage().enterStartDateOfInsurance1(); SeleniumFunc.waitFor(1);
	 * context.getUnderWritingPage().enterNumberOfParkingSlots(parking); }
	 */

	@Then("I enter the start date of insurance {string}")
	public void i_enter_startdateofinsurance(String date) throws Exception {

		context.getUnderWritingPage().enterStartDateOfInsurance(date);
		SeleniumFunc.waitFor(2);
	}

	@Then("I enter the {string} of the insurance application")
	public void i_enter_startdate_of_insurance_application(String date) throws Exception {
		context.getUnderWritingPage().enterStartDateOfInsurance(date);
		SeleniumFunc.waitFor(2);
	}

	@Then("I enter the Start Date  of the insurance application")
	public void i_enter_startdate() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().enterStartDateOfInsurance1();

	}

	@Then("I select the property {string} as {string}")
	public void i_select_property_value(String value, String propertyval) throws Exception {
		context.getUnderWritingPage().enterValueForPropertyTypeInsurance(value, propertyval);
	}

	@And("I provide additional details for property above ten million {string} {string} {string} {string} {string} {string}")
	public void i_provide_additional_det(String propertyval, String singlelocation, String locationsecurity,
										 String construction, String additionaldetails, String fireprotection) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().enterValueForPropertyMoreThanTenMillion(propertyval, singlelocation, locationsecurity, construction,
				additionaldetails, fireprotection);
	}

	@And("I check the declaration")
	public void i_check_the_declaration() throws Exception {
		context.getUnderWritingPage().checkDeclaration();
	}

	@Then("I accept the quote")
	public void i_accept_the_quote() throws Exception {
		context.getUnderWritingPage().acceptQuote();
	}

	@Then("Verify the updated profession {string} in client Acceptance")
	public void verify_the_updated_profession_in_client_acceptance(String profession) throws Exception {
		Assert.assertEquals(profession,context.getUnderWritingPage().verifyUpdatedProfessionInAcceptQuoate());
	}


	@Given("click {string} after clicking the Hamburger")
	public void click_after_clicking_the_hamburger(String Option) throws Exception {
		context.getUnderWritingPage().clickHamburger(Option);
	}

	@Given("Search the policy number {string}")
	public void search_the_policy_number(String policyNumber) throws Exception {
		context.getUnderWritingPage().SearchPolicyUsingPolicyNumber(policyNumber);
	}

	@Given("Open the policy and click on the history table")
	public void open_the_policy_and_click_on_the_history_table() throws Exception {
		context.getUnderWritingPage().clickPolicyHisotryAndDollorIcon();
	}

	@When("Clicking on the dollor icon")
	public void clicking_on_the_dollor_icon() {

	}

	@Then("Pop up should display the Premimum details")
	public void pop_up_should_display_the_premimum_details() {

	}




	@Then("I accept the quote by selecting no in the invoice no")
	public void i_accept_the_quote_zero_premium() throws Exception {

		context.getUnderWritingPage().acceptQuotewithNo();
	}
	@Given("Select No in Purchase order popup and click Submit")
	public void select_No_in_Purchase_order_popup_and_click_Submit() throws Exception {
		context.getUnderWritingPage().selectNoAndClickSubmitInPurchaseOrderPopup();

	}

	@Given("Select Yes and enter the purchaseorderno {string} and click submit")
	public void select_Yes_and_enter_the_purchaseorderno_and_click_submit(String PurchaseOrderNo) throws Exception {
		context.getUnderWritingPage().selectYesAndEnterPurchaseOrderNoInSubmitInPurchaseOrderPopup(PurchaseOrderNo);
	}

	@Then("I write test line in scenario outline with parameter as {string}")
	public void testCode(String args) throws Exception {
		javax.swing.JOptionPane.showMessageDialog(null, "Inside test step");
	}

	@Then("I Click Finish button")
	public void i_click_finish_button() throws Exception {
		context.getUnderWritingPage().clickFinish();
		SeleniumFunc.waitFor(2);
	}

	@And("I capture the policy ID")
	public void i_capture_policyid() throws Exception {
		context.getUnderWritingPage().fetchCaseID();
	}

	@Given("Enter the mandatoryInformation for AircraftInsurance and Click Submit")
	public void enter_the_mandatory_information_for_aircraft_insurance_and_click_submit() throws Exception {
		context.getFinalisequotePage().enterMandatoryInformationAircraftInsurance();
	}

	@When("Enter the mandatoryInformation for medicalIndeminityrularGP and Click Submit")
	public void enter_the_mandatory_information_for_medical_indeminityrular_gp_and_click_submit() throws Exception {
		context.getFinalisequotePage().enterMandatoryInformationMedicaIndeminity();
	}

	@Given("enter the Self Insured Options and click submit")
	public void enter_the_self_insured_options_and_click_submit() throws Exception {
		context.getFinalisequotePage().enterSelfInsuredRententionAndSubmit();
	}

	@And("I validate the {string}")
	public void i_validate_final_status(String status) throws Exception {
//		context.getUnderWritingPage().verifyUWFinalCaseStatus(ObjectRepo.reader.getstatus().toUpperCase());
//		SeleniumFunc.waitFor(6);
		context.getUnderWritingPage().verifyUWFinalCaseStatus(status);
	}

	@When("Approve the quote and submit")
	public void approve_the_quote_and_submit() throws Exception  {
		context.getUnderWritingPage().approveQuoteAndSubmit();
	}

	@Given("Enter the mandatoryInformation for PPL Grip and Click Submit")
	public void enter_the_mandatory_information_for_ppl_grip_and_click_submit() throws Exception {
		context.getFinalisequotePage().enterMandatoryInfoPPLGrip();
	}

	@Given("Enter the mandatoryInformation for PID Alliance in finalise Quote and Submit")
	public void enter_the_mandatory_information_for_pid_alliance_in_finalise_quote_and_submit() throws Exception {
		context.getFinalisequotePage().enterMandatoryInformationForProfessionalIndeminityAlliance();
	}

	@Given("Enter the mandatoryInformation for MIPI product and Click Submit")
	public void enter_the_mandatory_information_for_mipi_product_and_click_submit() throws Exception {
		context.getFinalisequotePage().enterMandatoryInfoForMIPIProduct();
	}

	@Given("Enter the mandatoryInformation for contractor liability product and Click Submit")
	public void enter_the_mandatory_information_for_contractor_liability_product_and_click_submit() throws Exception {
		context.getFinalisequotePage().enterMandatoryInformationContractorPollutionLiability();
	}

	@Given("Enter the mandatoryInformation for Investment Management Insurance and Click Submit")
	public void enter_the_mandatory_information_for_investment_management_insurance_and_click_submit() throws Exception {
		context.getFinalisequotePage().enterMandatoryIMI();
	}

	@Given("Enter the mandatoryInformation of finalise Quote and Submit")
	public void enter_the_mandatory_information_of_finalise_quote_and_submit() throws Exception {
		context.getFinalisequotePage().enterMandatoryInformationConstructionMultiyearPolicy();
	}


	@And("Enter the {string} and {string} and Submit the Policy")
	public void EnterPremiumAndSubmiQuoate(String Premium,String notes) throws Exception {
		context.getFinalisequotePage().enterPremiumForNewAppplication(Premium,notes,"No");
	}

	@And("Enter the {string} and {string} and {string} prorata")
	public void EnterUWPremiumNotes(String Premium,String notes,String enableDisableProrata) throws Exception {
		context.getFinalisequotePage().enterPremiumAndNotes(Premium,notes,enableDisableProrata);
	}

	@Given("I give endorsement text {string} and check exclude check box")
	public void giveEndorsementTextAndCheckExcludeCheckBox(String updatedText) throws Exception  {
		context.getFinalisequotePage().addEndorsementTextAndCheckExcludeBtn(updatedText);
	}

	@Given("Add endorsement text {string} and submit")
	public void addEndorsementTextAndSubmit(String updatedText) throws Exception {
		context.getFinalisequotePage().addEndorsementText(updatedText);
	}

	@Given("I validate the IBPS message {string}")
	public void i_validate_the_ibps_message(String ibpsmsg) throws Exception {
		context.getUnderWritingPage().validateTheMessages(ibpsmsg);
	}

	@When("Enter the mandatory Insured details and Click Submit")
	public void enter_the_mandatory_insured_details_and_click_submit() throws Exception {
		context.getFinalisequotePage().enterInsuredDetails("Test");
	}

	@Given("Enter the mandatoryInformation for Exparital Medical expense and Click Submit")
	public void enter_the_mandatory_information_for_exparital_medical_expense_and_click_submit() throws Exception {
		context.getFinalisequotePage().enterMandatoryInformationExpatriateMedicalExpenses();
	}

	@Given("Enter the Business value and Submit Quoate")
	public void enter_the_business_value_and_submit_quoate() throws Exception {
		context.getFinalisequotePage().enterBusinessAndSubmitQuoate();
	}

	@Given("Enter the deductible value and Submit Quote")
	public void enter_the_deductible_value_and_submit_quoate() throws Exception {
		context.getFinalisequotePage().enterDeductiblesForPPLandSubmitQuote();
	}


	@Given("Enter the mandatory information for ChildProtection Indiminity and Click Submit")
	public void enter_the_mandatory_information_for_child_protection_indiminity_and_click_submit() throws Exception {
		context.getFinalisequotePage().enterMandatoryInformationForChildIndeminity("All Claims","5000");
	}

	@Given("Enter the mandatoryInformation for Construnction Liablity metro tunnel finaliseQuoate and Submit")
	public void enter_the_mandatory_information_for_construnction_finalise_quoate_and_submit() throws Exception {
		context.getFinalisequotePage().enterMandatoryInfoForConstructionMertoTunnel("PolicyScheduleDocument.pdf","COC.pdf");
	}

	@When("I Submit the Policy")
	public void i_submit_the_policy() throws Exception {
		context.getFinalisequotePage().submitPolicy();
	}

	@Given("Upload COC and PolicySchedule Document and click Submit")
	public void enter_the_mandatory_information_finalise_quoate_and_submit() throws Exception {
		context.getFinalisequotePage().UploadCOCAndPolicyScheduleClickSubmit("PolicyScheduleDocument.pdf","COC.pdf");
	}

	@Given("Click Add endorsement text and validate short term endorsement")
	public void click_add_endorsement_text_and_validate_short_term_endorsement() throws Exception {
		context.getFinalisequotePage().enterEndorsementTextAndValidate("21/09/2100");
	}


	@Then("I Add Additional Deductibles {string} {string} {string}")
	public void Add_Deductibles(String name,String amount,String description)throws Exception{
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().addDedctibleForPolicies(name,amount,description);

	}
	@And("Enter the premium {string} and uwnotes {string}")
	public void EnterPremiumAndNotes(String Premium,String notes) throws Exception {
		context.getUnderWritingPage().enterThePremiumAndSubmitQuoate(Premium,notes);
	}



	@And("Add Dedctible for the PPL policy")
	public void AddDeductibleForPPL() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().addDedctibleForPPL("50","100","20");
	}

	@And("Add Dedctible for the professional Indeminity")
	public void AddDeductibleForProfessionalIndeminity() throws Exception {
		context.getUnderWritingPage().addDedcutibleForProfessionalIndeminity();
	}

	@When("Add Dedctible and submit the Quoate")
	public void add_dedctible_and_submit_the_quoate() throws Exception {
		try{
			context.getUnderWritingPage().addDedcutibleForProfessionalIndeminity();
			SeleniumFunc.switchToDefaultContent();
			context.getUnderWritingPage().SubmitQuote();
		}catch(Exception e){}
	}

	@Given("Update Profession {string} and select {string} whether same value need to be updated in the organization")
	public void update_profession_and_select_whether_same_value_need_to_be_updated_in_the_organization(String Profession, String IsitNeedUpdatedInOrg) throws Exception {
		context.getUnderWritingPage().updateProfessionOnFinalizeQuoate(Profession,IsitNeedUpdatedInOrg);
	}

	@Given("enter limits of liablity for Non owned aircraft liabilty")
	public void enter_limits_of_liablity_for_non_owned_aircraft_liabilty() throws Exception {
		context.getFinalisequotePage().enterLimitsOfLibilityforNOL();
	}

	@And("Submit the Quoate")
	public void SubmitTheQuoate() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().SubmitQuote();
	}

	@Given("I enter risk info {string} {string} {string} {string} {string}")
	public void i_enter_risk_info(String Revenue,String isGPRegister,String Aopplypolicy,String Retroactivecover,String prorata) throws Exception {
		context.getFinalisequotePage().enterRiskInfoForMedicalIndiminityRuralGP(Revenue,isGPRegister,Aopplypolicy,Retroactivecover,prorata);
	}

	@And("Search the policy and begin the case")
	public void SearchPolicyBeginCase() throws Exception {
//		context.getData().setCaseId("AI-52");
		context.getUnderWritingPage().SearchAndBeginTheCaseInRiskAdivisor();
	}

	@Then("I click on My workbaskets tab and Select Risk Adviser Queue from the View Queue")
	public void click_on_my_workbaskets_tab() throws Exception {
		context.getUnderWritingPage().SearchAndReviewTheCaseInRiskAdivisor();
	}

	@Given("Click on CaseID and then enter ID in Search text field and click Apply button")
	public void click_on_case_id_and_then_enter_id_in_search_text_field_and_click_apply_button() throws Exception {
		context.getUnderWritingPage().clickOnCaseID();
		context.getUnderWritingPage().enterIDAndClickApply();
	}

	@Given("After filter I select first Case ID from the table in Myworkbaskets")
	public void click_on_first_case_id_frommyworkbaskets() throws Exception {
		context.getUnderWritingPage().selectFirstCaseIDFromTable();
	}

	@Given("Select {string} on IBPS options")
	public void select_on_ibps_options(String option) throws Exception  {
		context.getUnderWritingPage().selectIBPSFromRiskInformation(option);
	}

	@When("Click on Casenumber and then enter ID in Search text field and click Apply button")
	public void click_on_casenumber_and_then_enter_id_in_search_text_field_and_click_apply_button() throws Exception {
		context.getUnderWritingPage().clickOnCaseNumber();
		context.getUnderWritingPage().entercaseNumberIDAndClickApply();
	}

	@When("After filter I select first Case ID from the table in Mycases")
	public void after_filter_i_select_first_case_id_from_the_table_in_mycases() throws Exception {
		context.getUnderWritingPage().selectFirstCaseIDFromMycaseTable();
	}

	@And("Select the Policy decision {string} and Submit the Application")
	public void SelectPolicyDescionAndSubmit(String decision) throws Exception {
		context.getUnderWritingPage().selectPolicyDecisionAndSubmitTheCase(decision);
	}

	@And("I verify the endorsement text")
	public void VerifyEndorsemntText() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().verifyendorsementText();
	}
	@And("Enter the endorsement text {string}")
	public void EndorsemntText(String Endo) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().endorsementText(Endo);
	}

	@And("I validate the status of the case to be {string}")
	public void iValidateCaseStatus(String status) throws Exception {
		context.getUnderWritingPage().verifyUWFinalCaseStatus(status.toUpperCase());
	}

	@And("I click on Refresh policy")
	public void i_click_on_refresh_policy() throws Exception {
		SeleniumFunc.waitFor(5);
		context.getUnderWritingPage().ClickonRefresh();
		SeleniumFunc.waitFor(5);

	}

	@And("I click on Exit Button")
	public void i_click_on_Exit_Button() throws Exception {
		SeleniumFunc.waitFor(5);
		context.getUnderWritingPage().clickOnExit();
	}

	@And("I verify the payment status {string}")
	public void i_verify_payment_status(String status) throws Exception {
		SeleniumFunc.waitFor(3);
		context.getUnderWritingPage().verifyUWCasePaymentStatus1(status);
	}
	@And("I verify the message {string}")
	public void i_verify_message_status(String status) throws Exception {
		Thread.sleep(3000);
		context.getUnderWritingPage().verifyUWCasePaymentStatus2(status);
	}

	@And("I provide some {string} with value to be updated if required as {string}")
	public void i_provide_action(String action, String updatedStudents) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().changeSTPConditionAsPerSelectedAction(action, updatedStudents);
	}

	@And("I check the status to be {string}")
	public void i_check_pending_status(String status) throws Exception {
		context.getUnderWritingPage().verifyUWPendingCaseStatus(status);
	}

	@Then("I route to my open actions")
	public void I_am_routed_to_openactions() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().routeToMyOpenActions();
	}

	@And("I verify the payment {string}")
	public void i_check_payment_status(String status) throws Exception {
		// SeleniumFunc.waitFor(3);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().verifyUWCasePaymentStatus(ObjectRepo.reader.getpaymentstatus());
		SeleniumFunc.waitFor(3);

	}

	@And("I verify manual resolve {string}")
	public void i_check_manual_status(String status) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().verifyUWCaseManualResolveStatus(status);
	}

	@And("I verify the payment for endorsement {string}")
	public void i_check_payment_end_status(String status) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().verifyUWCaseEndPaymentStatus(status);
	}

	@And("I verify the final status for updation {string}")
	public void i_check_final_end_status(String status) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().verifyUWCaseEndPaymentStatus(status);
	}

	@And("I verify the cancelled {string}")
	public void i_check_cancelled_status(String status) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().verifyUWCaseCancelledStatus(status);
	}

	@Then("Click UW Continue button")
	public void click_continue_button() throws Exception {
		context.getUnderWritingPage().clickContinueButton();
		SeleniumFunc.waitInMilliSeconds(5);
	}

	@When("Add travel records with the details {string}, number of students and number of staff")
	public void add_travel_records(String destination) throws Exception {
		context.getUnderWritingPage().addDetailsForTravelRecord(destination);
	}

	@Given("Select Public product liability net flow product")
	public void select_public_product_liability_net_flow_product() throws Exception {
		context.getUnderWritingPage().SelectProdctPPLNetFlow();
		context.getData().setData("Product","Public & Products Liability");
	}


	@Then("I select the policy in Risk advisor portal")
	public void i_select_the_policy_in_risk_advisor_portal() throws Exception {

		context.getClientAdviserPage().SelectPolicyUsingPolicyNumber(context.getData().getPolicynumber());
	}

	@Given("Select the effective date {string} for endorsment in Multiyear product")
	public void select_the_effective_date_for_endorsment_in_multiyear_product(String value) throws Exception {
		SeleniumFunc.SelectDateWithDropDown("//img[@name='CalendarImg-647b5417']",value);
	}

	@Given("Enter the Professional Indeminity info")
	public void enter_the_professional_indeminity_info() throws Exception {
		context.getUnderWritingPage().enterTheProfessionalIndiminityInfo();
	}

	@Given("Enter the insurance history details")
	public void enter_the_insurance_history_details() throws Exception {
		context.getUnderWritingPage().enterInsuranceHistoryDetails();
	}

	@Given("Enter the Professional ActivityInfo")
	public void enter_the_professional_activity_info() throws Exception {
		context.getUnderWritingPage().enterProfessionalActivities();
	}

	@Given("Enter the Join ventures and overseaswork")
	public void enter_the_join_ventures_and_overseaswork() throws Exception {
		context.getUnderWritingPage().enterJointVenturesOverseaInfo();
	}

	@Given("Enter the contract info for liablilty")
	public void enter_the_contract_info_for_liablilty() throws Exception {
		context.getUnderWritingPage().EnterContractorAndSubContractorInfor();
	}

	@Given("Enter the Fee income info and contract info for liability")
	public void enter_the_fee_income_info_and_contract_info_for_liability() throws Exception {
		context.getUnderWritingPage().EnterFeeIncomeInfo();
		context.getUnderWritingPage().EnterContractorAndSubContractorInfor();
	}

	@Given("Enter the Risk management details")
	public void enter_the_risk_management_details() throws Exception {
		context.getUnderWritingPage().enterRiskInformation();
	}

	@Given("Enter the claim circumstances details")
	public void enter_the_claim_circumstances_details() throws Exception {
		context.getUnderWritingPage().enterClaimsAndCircucentancesInfo();
	}



	@Given("Enter the Product, Airfield Liablity and Contract info {string}")
	public void enter_the_product_airfield_liablity_and_contract_info(String contractinfo) throws Exception {
		context.getUnderWritingPage().enterAirfieldAndProductLiablity_ContractDetails(contractinfo);
	}

	@Given("Add visitor info {string} {string} {string} {string} {string}")
	public void add_visitor_info(String NoOfVisitor, String StreetName, String Subrubtown, String State,String postcode) throws Exception {
		context.getUnderWritingPage().enterTheVisitorDetails(NoOfVisitor,StreetName,Subrubtown,State,postcode);
	}

	@Given("Enter the Storage Hazard details {string} {string}")
	public void enter_the_storage_hazard_details(String activityCasuedDamege,String presenceWater) throws Exception {
		context.getUnderWritingPage().enterStoargeOfHazardsDetails(activityCasuedDamege,presenceWater);
	}

	@Given("Enter the Annual renveue {string} {string} {string}")
	public void enter_the_annual_renveue(String OperationalRevenue,String VicrorianRevenue,String OtherFund) throws Exception {
		context.getUnderWritingPage().enterTotalAnnualRevenue(OperationalRevenue,VicrorianRevenue,OtherFund);
	}

	@Given("Enter the Incident, OverStateWork, AffilationWorkDetails")
	public void enter_the_incident_over_state_work_affilation_work_details() throws Exception {
		context.getUnderWritingPage().enterTheIncident_OverStateWork_AffilationWorkDetails();

		SeleniumFunc.waitFor(5);
//		SeleniumFunc.waitFor(80);

	}



	@Then("Enter the policy effective date {string} and capture the Expirydate")
	public void enterThePolicystartDate(String effectiveDate) throws Exception {
		SeleniumFunc.waitFor(2);
		context.getUnderWritingPage().SelectTheEffecttiveDateAndCaptureTheExpiryDate(effectiveDate);
	}

	@Then("click on the select button of the {string}")
	public void click_continue_button(String product) throws Exception {
		SeleniumFunc.waitFor(2);
		context.getUnderWritingPage().selectProduct(product);
		context.getData().setData("Product",product);
		SeleniumFunc.waitFor(3);
	}

	@When("click on the select button of the {string} by verifying {string}")
	public void click_on_the_select_button_of_the_by_verifying(String product, String description) throws Exception {
		SeleniumFunc.waitFor(2);
		context.getUnderWritingPage().selectProductByVerifyingDescription(product,description);
		context.getData().setData("Product",product);
		SeleniumFunc.waitFor(3);
	}

	@Given("I select the policy start date in the same financial year")
	public void i_select_the_policy_start_date_in_the_same_financial_year() throws Exception {
		context.getUnderWritingPage().enterStartDateInSameFinancialYear();

	}

	@Then("click on the select button of the {string} in Risk Advisor")
	public void selectProductInRiskAdvisor(String product) throws Exception {
		SeleniumFunc.waitFor(4);
		context.getUnderWritingPage().selectProductInRiskAdvisor(product);
		context.getData().setData("Product",product);
		SeleniumFunc.waitFor(3);
	}

	@Given("Enter the Policy Effective date {string} and Expiration date {string}")
	public void enter_the_policy_effective_date_and_expiration_date(String effectiveDate, String ExpiratitionDate) throws Exception {
		context.getUnderWritingPage().enterTheEffectiveAndExpirationdate(effectiveDate,ExpiratitionDate);
		Thread.sleep(5000);
		context.getData().setData("MultiYear","Yes");
	}

	@Given("Verify the error message {string} in information screen")
	public void verify_the_error_message_in_information_screen(String erromsg) throws Exception {

		Assert.assertEquals(context.getUnderWritingPage().getPolicyEffectivedateErrorMessageConstructionpolicy(),erromsg);
	}

	@When("Search the policy and click on the moreactions and click on the cancel policy")
	public void search_the_policy_and_click_on_the_moreactions_and_click_on_the_cancel_policy() throws Exception
	{
		context.getUnderWritingPage().clickonmoreactionsandcancelpolicy();
	}

	@Given("Click on the Note Intersted Party of COC in More Actions")
	public void click_on_the_note_intersted_party_of_coc_in_more_actions() throws Exception {
		context.getUnderWritingPage().clickOnNoteInterstedPolicy();
	}

	@Given("Enter the CerificateDetails {string} {string} {string} {string}")
	public void enter_the_cerificate_details(String nameOfThePart,String effectiveDate,String fromDate,String toDate) throws Exception {
		context.getUnderWritingPage().enterNoteInterstedPartydetails( nameOfThePart, effectiveDate, fromDate, toDate);
	}



	@When("Now capture the policy ID")
	public void now_capture_the_policy_ID() throws Exception {
		context.getUnderWritingPage().capturecanceIDno();
	}

	@When("Now search with the policy ID in the UW portal")
	public void now_search_with_the_policy_ID_in_the_UW_portal() throws Exception {
		context.getUnderWritingPage().selectsearchandenterpolicyID();
	}

	@When("Now click on the begin button")
	public void now_click_on_the_begin_button() throws Exception
	{
		//context.getUnderWritingPage().clickthebeginbtn();
		Thread.sleep(5);
		context.getUnderWritingPage().beginTheCase();
	}
	@When("Now click on the approve quote")
	public void now_click_on_the_approve_quote() throws Exception
	{
		context.getUnderWritingPage().clickapprovebtn();
	}


	@When("Click on the new button")
	public void click_on_the_new_button() throws Exception
	{

		context.getUnderWritingPage().clicknew();

	}
	@When("Click on the new button in humburger")
	public void click_on_the_new_button_in_humburger() throws Exception
	{

		context.getUnderWritingPage().clicknewhum();

	}
	@When("Now click on the Manage purchase order button")
	public void now_click_on_the_Manage_purchase_order_button() throws Exception
	{
		context.getUnderWritingPage().clickonmanagebtn();


	}
	@When("click on the Manage purchase order button")
	public void click_on_the_Manage_purchase_order_button() throws Exception
	{
		context.getUnderWritingPage().clickOnManageBtn();


	}
	@When("Now enter the policy no {string}")
	public void now_enter_the_policy_no(String policyno) throws Exception
	{
		Thread.sleep(2000);

		context.getUnderWritingPage().enterthepolicy(policyno);
	}
	@When("Now click on the search button")
	public void now_click_on_the_search_button() throws Exception {
		context.getUnderWritingPage().clicksearchButton();

		SeleniumFunc.waitFor(2);
	}

	@When("Now click on the radio button")
	public void now_click_on_the_radio_button() throws Exception
	{
		context.getUnderWritingPage().clickradbtnn();

	}
	@When("Clear the predefined purchase order and enter new purchase order{string}")
	public void clear_the_predefined_purchase_order_and_enter_new_purchase_order(String purchaseorder) throws Exception {

		context.getUnderWritingPage().enterthepurchaseorderr(purchaseorder);
	}

	@Then("Add Testdocument attachment {string}")
	public void Add_attachment_Rnd_Product(String file) throws Exception {
		context.getUnderWritingPage().addAttachmentToRnDPRodcut(file);
		SeleniumFunc.waitFor(40);
	}

	@And("I validate the number of {string}")
	public void validate_students(String students) throws Exception {

		context.getUnderWritingPage().validateInfo(students);
		SeleniumFunc.waitFor(10);
	}

	@And("I validate the property value {string}")
	public void validate_property_value(String propvalue) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().validatePropValue(propvalue);
		SeleniumFunc.waitFor(5);
	}

	@And("I provide an attachment having filepath as {string}")
	public void attach_file(String filepath) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().attachFileInRiskPage(filepath);
		SeleniumFunc.waitFor(5);
	}

	@When("I search the case id that is routed and I begin the case")
	public void enter_search() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
		SeleniumFunc.waitFor(9);
//		context.getData().setCaseId("P-12821");
//		context.getData().setData("MultiYear","Yes");
//		context.getUnderWritingPage().enterIdInSearchBox(context.getData().getCaseId());
//		SeleniumFunc.acceptTheAlert();
//		SeleniumFunc.waitFor(2);
//		SeleniumFunc.getRefresh();
		context.getUnderWritingPage().beginTheCase();
		SeleniumFunc.acceptTheAlert();
	}

	@When("I click on Case ID after filter")
	public void i_click_on_case_id_after_filter() throws Exception {
		context.getUnderWritingPage().clickCaseID();
	}

	@When("I search the deactivate endorsementId from {string}")
	public void i_search_the_deactivate_endorsement_id_from(String queue) throws Exception {
		context.getUnderWritingPage().searchDeactivateID(queue);
	}

	@When("select the deactivate EndorsementID")
	public void select_the_deactivate_endorsement_id() throws Exception {
		context.getUnderWritingPage().selectDeactivateEndorsementID();
	}

	@Then("click on finish button")
	public void validate_the_rejected_comments_verify_rejection_and_click_finish() throws Exception {
//		context.getUnderWritingPage().validateRejectedComments(rejComments);
		context.getUnderWritingPage().clickFinish();
	}

	@Then("enter comments {string} and click finish")
	public void enter_comments_and_click_finish(String comments) throws Exception {
		context.getUnderWritingPage().clickCommentsAndFinish(comments);
	}

	@Given("reject the request and submit with {string}")
	public void the_request_and_submit(String comments) throws Exception {
		context.getUnderWritingPage().rejectRequest(comments);
	}

	@Then("approve the request and submit")
	public void the_request_and_submit() throws Exception {
		context.getUnderWritingPage().approveRequest();
	}

	@When("select deactivate endorsementID to proceed deactivation of the endorsement")
	public void select_deactivate_endorsement_id_to_proceed_deactivation_of_the_endorsement() throws Exception {
		context.getUnderWritingPage().clickOnCaseNumber();
		context.getUnderWritingPage().entercaseNumberIDAndClickApply();
		context.getUnderWritingPage().selectFirstCaseIDFromMycaseTable();
	}

	@When("validate the pro rata premium for the emRepss policy {string}")
	public void ValidatePremiumForEmRepss(String action) throws Exception {
		//SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
		SeleniumFunc.waitFor(5);
//		context.getUnderWritingPage().validatePremium(action,"834.58","Yes");
		context.getUnderWritingPage().validateTotalPremium(action,"834.58","Yes");
		SeleniumFunc.robotEnter();
	}

	@Given("validate the return premium for the emRepss policy {string}")
	public void validate_the_return_premium_for_the_em_repss_policy(String action) throws Exception {
		//SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
		SeleniumFunc.waitFor(5);
		context.getUnderWritingPage().validatePremium(action,"834.58","Yes");
//		context.getUnderWritingPage().validateTotalPremium(action,"834.58","Yes");
		SeleniumFunc.robotEnter();
	}

	@When("I update base premium")
	public void up_base_prem() throws Exception {
		//SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
		SeleniumFunc.waitFor(5);
		context.getUnderWritingPage().clickUpdateBasePremButtonFinalizeQuote();
		SeleniumFunc.robotEnter();
		context.getUnderWritingPage().UpdateBasePremiumFromUWPortal("3000","Base premium change test");

	}
	@When("I update base premium for verify uper limit")
	public void update_base_prem_morelimit() throws Exception {
		//SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().clickUpdateBasePremButtonFinalizeQuote();
		SeleniumFunc.robotEnter();
		context.getUnderWritingPage().UpdateBasePremiumFromUWPortal("950000","baseepremmq");

	}
	@When("I update custom premium")
	public void up_custom_prem() throws Exception {
		//SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
		SeleniumFunc.waitFor(5);
		context.getUnderWritingPage().clickUpdateBasePremButtonFinalizeQuote();
		SeleniumFunc.robotEnter();
		context.getUnderWritingPage().UpdateCustomPremiumFromUWPortal("3000","baseepremmq");

	}
	@Then("I validate the premium for {string} for {string} values {string} {string} and {string}")
	public void validate_premium(String students, String days, String multiplier, String gst, String stampduty)
			throws Exception {
		int intstudents = Integer.parseInt(students);
		int intdays = Integer.parseInt(days);
		float floatmul = Float.parseFloat(multiplier);
		float floatgst = Float.parseFloat(gst);
		float floatstamp = Float.parseFloat(stampduty);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidatePremiumDetails(intstudents, intdays, floatmul, floatgst, floatstamp);

	}

	@Then("Now I validate the premium for {string} for {string}")
	public void validate_premium1(String students, String startDate)
			throws Exception {
		int intStudents = Integer.parseInt(students);//11000

		String fiscalYearEndDate = SeleniumFunc.getPolicyYearEndDate();
		if(startDate.equalsIgnoreCase("current date") || startDate.equalsIgnoreCase("currentdate")){
			startDate = SeleniumFunc.getCurrentDate().replaceAll("[-]","/");
		}else if(startDate.contains("1/07/")){
			int fiscalEndYear = SeleniumFunc.getFiscalEndYear() - 1;
			startDate = "01/07/" + fiscalEndYear;
		}

		int intDays = Integer.parseInt(String.valueOf(SeleniumFunc.findTheDateDifference(startDate, fiscalYearEndDate)));
//		float floatmul = Float.parseFloat(multiplier);
//		float floatgst = Float.parseFloat(gst);
//		float floatstamp = Float.parseFloat(stampduty);
		float floatMul = Float.parseFloat(strGPAMultiplier);
		float floatGST = Float.parseFloat(strGST);
		float floatStamp = Float.parseFloat(strStampDuty);

		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidatePremiumDetails1(intStudents, intDays, floatMul, floatGST, floatStamp);

	}

	@Then("Now I validate the premium for updated {string} for {string}")
	public void validate_premium2(String students, String startDate)
			throws Exception {
		int intStudents = Integer.parseInt(students);

		String fiscalYearEndDate = SeleniumFunc.getPolicyYearEndDate();
		if(startDate.equalsIgnoreCase("current date") || startDate.equalsIgnoreCase("currentdate")){
			startDate = SeleniumFunc.getCurrentDate().replaceAll("[-]","/");
		}else if(startDate.contains("1/07/")){
			int fiscalEndYear = SeleniumFunc.getFiscalEndYear() - 1;
			startDate = "01/07/" + fiscalEndYear;
		}

		int intDays = Integer.parseInt(String.valueOf(SeleniumFunc.findTheDateDifference(startDate, fiscalYearEndDate)));

		// int intdays = Integer.parseInt(days);
//		float floatmul = Float.parseFloat(multiplier);
//		float floatgst = Float.parseFloat(gst);
//		float floatstamp = Float.parseFloat(stampduty);
		float floatMul = Float.parseFloat(strGPAMultiplier);
		float floatGST = Float.parseFloat(strGST);
		float floatStamp = Float.parseFloat(strStampDuty);
//		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
//
//		Date requestStartDate = sdf.parse("18/02/2021");
//		Date requestEndDate = sdf.parse(days);
//		long diffInMillies0 = Math.abs(requestEndDate.getTime() - requestStartDate.getTime());
//		System.out.println("remaining days-------" + diffInMillies0);
//		long intDays = TimeUnit.DAYS.convert(diffInMillies0, TimeUnit.MILLISECONDS);
//		System.out.println("remaining days-------" + intDays);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidatePremiumDetails1(intStudents, intDays, floatMul, floatGST, floatStamp);

	}

	@Then("I validate the endorsement premium for {string} {string} for {string} values {string} {string} and {string}")
	public void validate_end_premium(String students, String updatedStud, String days, String multiplier, String gst, String stampDuty) throws Exception {
		int intstudents = Integer.parseInt(students);
		int intupstudents = Integer.parseInt(updatedStud);
		int intdays = Integer.parseInt(days);
//		float floatmul = Float.parseFloat(multiplier);
//		float floatgst = Float.parseFloat(gst);
//		float floatstamp = Float.parseFloat(stampDuty);
		float floatMul = Float.parseFloat(strGPAMultiplier);
		float floatGST = Float.parseFloat(strGST);
		float floatStamp = Float.parseFloat(strStampDuty);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidateEndorsementPremiumDetails(intstudents, intupstudents, intdays, floatMul, floatGST, floatStamp);

	}

	@Then("I validate the endorsement premium for {string} {string} with endorsement effective date {string}")
	public void validate_end_premium_with_end_date(String students, String updatedStud, String endEffectDate) throws Exception {
		int intStudents = Integer.parseInt(students);
		int intUpdStudents = Integer.parseInt(updatedStud);

		String fiscalYearEndDate = SeleniumFunc.getPolicyYearEndDate();
		if(endEffectDate.equalsIgnoreCase("current date") || endEffectDate.equalsIgnoreCase("currentdate")){
			endEffectDate = SeleniumFunc.getCurrentDate().replace("-","/").replace("-","/");
		}
		int intDays = Integer.parseInt(String.valueOf(SeleniumFunc.findTheDateDifference(endEffectDate, fiscalYearEndDate)));
//		float floatMul = Float.parseFloat(multiplier);
//		float floatGST = Float.parseFloat(gst);
//		float floatStamp = Float.parseFloat(stampDuty);
		float floatMul = Float.parseFloat(strGPAMultiplier);
		float floatGST = Float.parseFloat(strGST);
		float floatStamp = Float.parseFloat(strStampDuty);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidateEndorsementPremiumDetails(intStudents, intUpdStudents, intDays, floatMul, floatGST, floatStamp);
	}

	@Then("I validate the premium for property {string} {string} {string} {string} {string} {string}")
	public void validate_premium_property(String propertyValue, String minimumValue, String percentage, String gst,
										  String stampDuty, String startDate) throws Exception {
		int propValue = Integer.parseInt(propertyValue);
		int minimumVal = Integer.parseInt(minimumValue);
		float floatPercent = Float.parseFloat(percentage);
		float floatGST = Float.parseFloat(gst);
		float floatStamp = Float.parseFloat(stampDuty);

		String fiscalYearEndDate = SeleniumFunc.getPolicyYearEndDate();
		if(startDate.equalsIgnoreCase("current date") || startDate.equalsIgnoreCase("currentdate")){
			startDate = SeleniumFunc.getCurrentDate().replaceAll("[-]","/");
		}else if(startDate.contains("1/07/")){
			int fiscalEndYear = SeleniumFunc.getFiscalEndYear() - 1;
			startDate = "01/07/" + fiscalEndYear;
		}
		int intDays = Integer.parseInt(String.valueOf(SeleniumFunc.findTheDateDifference(startDate, fiscalYearEndDate)));

		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidatePremiumDetailsForProperty(propValue, minimumVal, floatPercent / 100, floatGST,
				floatStamp, intDays);

	}

	@Then("I validate the endorsement premium for property {string} {string} {string} {string} {string} {string} {string}")
	public void validate_end_premium_property(String propertyvalue, String minimumvalue, String updatedminimumvalue,
											  String percentage, String gst, String stampduty, String days) throws Exception {
		int propvalue = Integer.parseInt(propertyvalue);
		int minimumval = Integer.parseInt(minimumvalue);
		int upminimumval = Integer.parseInt(updatedminimumvalue);
		float floatpercent = Float.parseFloat(percentage);
		float floatgst = Float.parseFloat(gst);
		float floatstamp = Float.parseFloat(stampduty);
		int day = Integer.parseInt(days);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidateEndorsementPremiumDetailsForProperty(propvalue, minimumval, upminimumval,
				floatpercent / 100, floatgst, floatstamp, day);

	}

	@Then("I validate the premium for liability for {string} {string} {string}")
	public void validate_premium_liab(String basepremium, String gst, String stampduty) throws Exception {
		int baseprem = Integer.parseInt(basepremium);
		float floatgst = Float.parseFloat(gst);
		float floatstamp = Float.parseFloat(stampduty);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidatePremiumDetailsForLiability(baseprem, floatgst, floatstamp);

	}

	@Then("I validate the endorsement premium for liability for {string} {string} {string} {string}")
	public void validate_premium_liab_endorsement(String basepremium, String updatedbasepremium, String gst,
												  String stampduty) throws Exception {
		int baseprem = Integer.parseInt(basepremium);
		int updbaseprem = Integer.parseInt(updatedbasepremium);
		float floatgst = Float.parseFloat(gst);
		float floatstamp = Float.parseFloat(stampduty);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidateEndorsementPremiumDetailsForLiability(baseprem, updbaseprem, floatgst, floatstamp);

	}

	@Then("I validate the premium for MV for {string} {string} {string} {string}")
	public void validate_premium_mv(String basepremium, String gst, String stampduty, String days) throws Exception {
		float baseprem = Float.parseFloat(basepremium);
		int intdays = Integer.parseInt(days);
		float floatgst = Float.parseFloat(gst);
		float floatstamp = Float.parseFloat(stampduty);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidatePremiumDetailsForMVUW(baseprem, floatgst, floatstamp, intdays);

	}

	@And("I accept the quote for uw")
	public void accept_quote_uw() throws Exception {
		SeleniumFunc.waitFor(1);
		context.getUnderWritingPage().acceptQuoteUW();
		SeleniumFunc.waitFor(5);
	}

	@Given("validate the premium calculation for the {string}")
	public void validate_the_premium_calculation_for_the(String policyType) throws Exception {
		context.getFinalisequotePage().validatePremiumforCancellation();
	}


	@Then("I submit the case to get routed to the user")
	public void submit_quote_uw() throws Exception {

		context.getUnderWritingPage().submitQuoteUW();
		SeleniumFunc.waitFor(5);
	}

	@Given("Enter the {string} and {string} in cancellation")
	public void enter_the_and_in_cancellation(String premium, String notes) throws Exception {
		context.getFinalisequotePage().enterPremiumForCancellation(premium,notes);
	}

	@Then("I open the approved case")
	public void open_approved_case() throws Exception {
//		context.getData().setCaseId("PRO-24622");
		SeleniumFunc.waitFor(30);
		context.getUnderWritingPage().searchAndOpenUWApprovedCase();
		SeleniumFunc.waitFor(5);
	}

	@Then("I answer the risk questions as no")
	public void ans_risk_ques_case() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().answerRiskquestionNo();
		SeleniumFunc.waitFor(10);
	}

	@Then("I answer the risk questions as yes with {string}")
	public void ans_risk_ques_yes(String hiredays) throws Exception {
		context.getUnderWritingPage().answerRiskquestionYes(hiredays);
		SeleniumFunc.waitFor(10);
	}

	@Then("I individually provide the {string} and {string}")
	public void uw_createQuote(String basepremium, String notes) throws Exception {


		context.getUnderWritingPage().feedBasePremiumFromUWPortal(basepremium, notes);
		// SeleniumFunc.waitFor(10);
		SeleniumFunc.waitFor(3);
	}

	@Then("I individually provide for Liability {string} and {string}")
	public void uw_createQuote1(String basepremium, String notes) throws Exception {
		context.getUnderWritingPage().feedBasePremiumFromUWPortal1(basepremium, notes);
		SeleniumFunc.waitFor(10);
	}

	@Then("I provide individually {string} and {string}")
	public void uw_createQuote2(String basepremium, String notes) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().feedBasePremiumFromUWPortal1(basepremium, notes);
		SeleniumFunc.waitFor(10);
	}

	@Then("I manually process payment from finance portal")
	public void manually_process_finance() throws Exception {
//		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
		context.getUnderWritingPage().manualProcessCase();
		SeleniumFunc.waitFor(5);
	}


	@Then("I add {string} and {string} as market stall events")
	public void uw_marketStallEvens(String marketstallholders, String numberofdates) throws Exception {
		context.getUnderWritingPage().doMarketStallOperationsForLiability(marketstallholders, numberofdates);
	}

	@Then("I fill up the MV details in the risk page {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void uw_mv_risk_page(String lightvehicleflag, String busflag, String trailerflag, String otherflag,
								String typeofvehicle, String manufactureyear, String make, String model, String regno,
								String currentmarketvalue, String numberofvehicles) throws Exception {
		int integernumberofvehicles = Integer.parseInt(numberofvehicles);
		context.getUnderWritingPage().enterValueInMVRiskPage(lightvehicleflag, busflag, trailerflag, otherflag, typeofvehicle, manufactureyear,
				make, model, regno, currentmarketvalue, integernumberofvehicles);
	}

	@Then("I fill up the MV details in the risk page with machinery {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void uw_mv_risk_page_machinery(String lightvehicleflag, String busflag, String trailerflag,
										  String machineryflag, String otherflag, String typeofvehicle, String manufactureyear, String make,
										  String model, String regno, String currentmarketvalue, String numberofvehicles) throws Exception {
		int integernumberofvehicles = Integer.parseInt(numberofvehicles);
		context.getUnderWritingPage().enterValueInMVRiskPageMLP2(lightvehicleflag, busflag, trailerflag, machineryflag, otherflag, typeofvehicle,
				manufactureyear, make, model, regno, currentmarketvalue, integernumberofvehicles);
	}

	@Then("I fill up the MV details in the endorsement risk page {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void uw_mv_end_risk_page(String lightvehicleflag, String busflag, String trailerflag, String otherflag,
									String typeofvehicle, String manufactureyear, String make, String model, String regno,
									String currentmarketvalue, String numberofvehicles) throws Exception {
		int integernumberofvehicles = Integer.parseInt(numberofvehicles);
		context.getUnderWritingPage().enterValueInMVRiskPageEndorsement(lightvehicleflag, busflag, trailerflag, otherflag, typeofvehicle,
				manufactureyear, make, model, regno, currentmarketvalue, integernumberofvehicles);
	}

	@Then("I validate the premium for MV {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void uw_mv_pricing_calculation(String lightvehicleflag, String busflag, String trailerflag, String otherflag,
										  String pricelightvehicle, String pricebusunder50, String pricebusover50, String pricetrailer,
										  String numberofvehicles, String gst, String stampduty, String currentmarketvalue, String days)
			throws Exception {

		int integernumberofvehicles = Integer.parseInt(numberofvehicles);
		context.getUnderWritingPage().validateMVPricing(lightvehicleflag, busflag, trailerflag, otherflag, Double.parseDouble(pricelightvehicle),
				Double.parseDouble(pricebusunder50), Double.parseDouble(pricebusover50),
				Double.parseDouble(pricetrailer), integernumberofvehicles, Double.parseDouble(gst),
				Double.parseDouble(stampduty), Double.parseDouble(currentmarketvalue), Integer.parseInt(days));
	}

	@Then("I validate the premium for MV with machinery {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void uw_mv_pricing_calculation_with_machinery(String lightvehicleflag, String busflag, String trailerflag,
														 String machineryflag, String otherflag, String pricelightvehicle, String pricebusunder50,
														 String pricebusover50, String pricetrailer, String pricemachinery, String numberofvehicles, String gst,
														 String stampduty, String currentmarketvalue, String days) throws Exception {

		int integernumberofvehicles = Integer.parseInt(numberofvehicles);
		context.getUnderWritingPage().validateMVPricingMachinery(lightvehicleflag, busflag, trailerflag, machineryflag, otherflag,
				Double.parseDouble(pricelightvehicle), Double.parseDouble(pricebusunder50),
				Double.parseDouble(pricebusover50), Double.parseDouble(pricetrailer),
				Double.parseDouble(pricemachinery), integernumberofvehicles, Double.parseDouble(gst),
				Double.parseDouble(stampduty), Double.parseDouble(currentmarketvalue), Integer.parseInt(days));
	}

	@Then("I cancel a policy from more actions")
	public void cancelPolicy() throws Exception {
		context.getUnderWritingPage().cancelPolicyMoreActions();
		SeleniumFunc.waitFor(5);
	}

	@Then("click more actions and cancel the policy using the following information")
	public void select_the_following_values_in_the_cancellation(DataTable dataTable) throws Exception {
		List<List<String>> cancellation= dataTable.asLists(String.class);
		context.getUnderWritingPage().cancelPolicyByClickingMoreActions(cancellation.get(0).get(0),cancellation.get(2).get(0),cancellation.get(1).get(0));
		context.getData().setPolicyAction("Cancallation");
	}

	@Then("Cancel the bundle policy using {string} and {string}")
	public void cancel_the_bundle_policy_using_and(String iscancelfullterm,String cancelReason) throws Exception {
		context.getUnderWritingPage().cancelBundlePolicy(iscancelfullterm,cancelReason);
	}




	@Then("cancel the policy using the following information")
	public void cancel_the_policy_using_the_following_information(DataTable dataTable) throws Exception {
		List<List<String>> cancellation= dataTable.asLists(String.class);
		context.getUnderWritingPage().cancelPolicy(cancellation.get(0).get(0),cancellation.get(2).get(0),cancellation.get(1).get(0));
	}

	@Then("Click {string} and Select {string} in policy cancellation and click finish")
	public void cancelPolicyfromClientAdvisor() throws Exception {
		context.getUnderWritingPage().cancelPolicyMoreActions();
		SeleniumFunc.waitFor(5);
	}

	@Then("Select policy and Click on Update Policy")
	public void clickUpdatePolicy() throws Exception {
		SeleniumFunc.waitFor(2);
		context.getUnderWritingPage().clickUpdatePolicy() ;
		SeleniumFunc.waitFor(3);
		context.getData().setPolicyAction("Endorsment");
	}

	@Then("Select EffectiveDate {string} And Enter UpdateInfo {string}")
	public void SelectEffectiveDateAndEnterUpdateInfo(String effectiveDate,String updateinfo) throws Exception {
		context.getUnderWritingPage().enterEndorsementDetails(effectiveDate,updateinfo);
		SeleniumFunc.waitFor(3);
	}

	@Then("Select effectiveDate And Enter information about the policy")
	public void select_effective_date_and_enter_information_about_the_policy() throws Exception {
		context.getUnderWritingPage().enterTheEndorsementDetails();
	}

	@And("I Select the Organization {string}")
	public void I_Select_the_Organisation(String name) throws Exception {
//		TestBase.currentCaseID="CON-30064";
		if(orgCreated==false) {
			System.out.println("Its if statement"+name);
			context.getData().setOrgnization(name);
			context.getUnderWritingPage().iSelectTheOrganisation(name);
			orgCreated=true;
		}
		else{
			System.out.println("Its else statement"+name);
			context.getUnderWritingPage().iSelectTheOrganisation(context.getData().getOrgnization());
		}

//		if (Objects.equals("null",String.valueOf(System.getProperty("Regression"))))
//		else CSVFileReader.getOrgFromCSV("","");
//		SeleniumFunc.waitFor(15);
//		context.getData().setPolicyAction("New Business");
	}

	@Given("I Select the Organization {string} for {string}")
	public void i_select_the_organization_for(String orgName, String productName) throws Exception {
		System.setProperty("Orgnization","Random");

		if(orgCreated==false) {
			if (Objects.equals("null", String.valueOf(System.getProperty("Orgnization")))){
				context.getData().setOrgnization(orgName);
				context.getUnderWritingPage().iSelectTheOrganisation(context.getData().getOrgnization());}
			else if (String.valueOf(System.getProperty("Orgnization")).equalsIgnoreCase("Random")) {
				context.getData().setOrgnization(CSVFileReader.getOrgFromCSV(productName, "Insurance"));
				context.getUnderWritingPage().iSelectTheOrganisation(context.getData().getOrgnization());
			} else if (String.valueOf(System.getProperty("Orgnization")).equalsIgnoreCase("FromFeature")) {
				context.getData().setOrgnization(orgName);
				context.getUnderWritingPage().iSelectTheOrganisation(context.getData().getOrgnization());
			} else{
				context.getData().setOrgnization(CSVFileReader.getOrgvalue((System.getProperty("Orgnization")), "Insurance"));
				context.getUnderWritingPage().iSelectTheOrganisation(context.getData().getOrgnization());
			}
			orgCreated=true;
		}
		else{
			context.getUnderWritingPage().iSelectTheOrganisation(context.getData().getOrgnization());
		}
		organizaionname = context.getData().getOrgnization();
		SeleniumFunc.waitFor(15);
		context.getData().setData("Orgnizantion","");
		context.getData().setPolicyAction("New Business");
	}

	public String select_the_organization() throws Exception {
		return organizaionname;
	}

	@Given("I Select the Organization {string} for claim {string}")
	public void i_select_the_organization_for_claim(String orgname, String claimType) throws Exception {
		if (Objects.equals("null",String.valueOf(System.getProperty("Orgnization")))) context.getUnderWritingPage().iSelectTheOrganisation(orgname);
		else if(String.valueOf(System.getProperty("Orgnization")).equalsIgnoreCase("Random")){ System.out.println(CSVFileReader.getOrgFromCSV(claimType,"Claim")); context.getUnderWritingPage().iSelectTheOrganisation(CSVFileReader.getOrgFromCSV(claimType,"Claim")); }
		else if(String.valueOf(System.getProperty("Orgnization")).equalsIgnoreCase("FromFeature")){ context.getUnderWritingPage().iSelectTheOrganisation(orgname); }
		else  context.getUnderWritingPage().iSelectTheOrganisation(CSVFileReader.getOrgvalue((System.getProperty("Orgnization")),"Claim"));
		SeleniumFunc.waitFor(15);
	}


	@And("Select the Bundle Policy Orgnization")
	public void Select_the_Bundle_Organisation() throws Exception {
		context.getUnderWritingPage().selectTheOrganizationBundle();
	}

	@Given("Verify all the policies are created and more action is disabled for {string}")
	public void verify_all_the_policies_are_created_and_more_action_is_disabled_for(String productType) throws Exception {
		ArrayList<String> policyDetails=context.getUnderWritingPage().getAllThePolicyElement();
		SoftAssert sassert=new SoftAssert();
		List<String> expectedresult=context.getData().getCSOBundleProductsPolicies(productType);
		System.out.println("Expected Result:"+policyDetails);
		for(String policyName:policyDetails){ sassert.assertTrue(expectedresult.contains(policyName)); }
		sassert.assertAll();
		Assert.assertTrue(context.getUnderWritingPage().getDisabledButtons().size()==policyDetails.size());
	}



	@And("Check Apply for policy button is not displayed")
	public void ApplyforPoicyNotDisplayed() throws Exception {
		Assert.assertFalse(context.getUnderWritingPage().verifyApplyForPolicyNotDisplayed());
	}


	@And("I Select the Organization1 {string}")
	public void I_Select_the_Organisation1(String name) throws Exception {
		context.getUnderWritingPage().iSelectTheOrganisation(ObjectRepo.reader.getOrganisation1());
	}

	@And("I Select the Cemetry Organization {string}")
	public void I_Select_the_CemeteryOrganisation(String name) throws Exception {
		context.getUnderWritingPage().iSelectTheOrganisation(ObjectRepo.reader.getCemeteryOrganisation());
	}

	@And("I Select the Prop and Planning Organisation")
	public void I_Select_the_PropAndPlanning_Organisation() throws Exception {
		SeleniumFunc.getRefresh();
		context.getUnderWritingPage().iSelectTheOrganisation(ObjectRepo.reader.getPropAndPlanningOrganisation());
	}

	@Then("I enter {string} and total number of {string}")
	public void i_enter_number_of_FTE(String date, String FTE) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().enterStartDateOfInsurance(date);
		SeleniumFunc.waitFor(1);
		context.getUnderWritingPage().enterNumberOfFTE(FTE);
	}

	@And("I select no previous journey insurance")
	public void select_prev_insurance_details_no() throws Exception {
		context.getUnderWritingPage().enterPrevInsuranceDetails();
	}

	@And("I validate the total number of {string}")
	public void validate_FTE(String fte) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().validateJourneyFTEInfo(fte);
		//SeleniumFunc.waitFor(10);
	}

	@Then("I validate the premium of Journey for {string} using values {string} {string} {string} and {string}")
	public void validate_premium_for_Journey(String fte, String startDate, String multiplier, String gst, String stampduty) throws Exception {
		int intfte = Integer.parseInt(fte);
		int intdays = context.getUnderWritingPage().findDaysRemaining(startDate);
		float floatmul = Float.parseFloat(multiplier);
		float floatgst = Float.parseFloat(gst);
		float floatstamp = Float.parseFloat(stampduty);
		SeleniumFunc.switchToDefaultContent();

		//context.getUnderWritingPage().calculateAndValidatePremiumDetailsForJourney(intfte,intdays,floatmul,floatgst,floatstamp);
		context.getUnderWritingPage().calculateAndValidatePremiumDetails(intfte,intdays,floatmul,floatgst,floatstamp);

	}

	@Then("I validate the endorsement premium of Journey for {string} {string} for {string} values {string} {string} and {string}")
	public void validate_journey_endorsement_premium(String fte, String updatedfte, String effectiveDate, String multiplier, String gst, String stampduty) throws Exception {
//
//
		int intfte = Integer.parseInt(fte);
		int intupdatedfte = Integer.parseInt(updatedfte);
		int intdays = context.getUnderWritingPage().findDaysRemaining(effectiveDate);
		float floatmul = Float.parseFloat(multiplier);
		float floatgst = Float.parseFloat(gst);
		float floatstamp = Float.parseFloat(stampduty);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidateEndorsementPremiumDetails(intfte, intupdatedfte, intdays, floatmul, floatgst, floatstamp);
	}

	@Then("I search for organisation {string} and select contact")
	public void enter_claim_details() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().claimEnterBusiness();
	}
	@Then("I validate the premium for Motor Loss of No Claims Bonus Risk Information {string} {string} {string} {string}")
	public void uw_mlb_pricing_calculation( String baseprem, String floatgst, String floatstamp, String days) throws Exception {

		//int integernumberofvehicles = Integer.parseInt(numberofvehicles);
		context.getUnderWritingPage().calculateAndValidatePremiumDetailsForMLBN(baseprem,floatgst,floatstamp, days);
	}
	@Then("enter number of Vehicles {string}and Time period for using Vehicles")
	public void enternumberofVehiclesandtimeperiod(String vehicles ) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().enterNumberofvechil(vehicles);
		SeleniumFunc.waitFor(2);
	}
	@Then("I enter the revenue details {string} {string}")
	public void i_enter_the_revenue_details(String previousval, String currentValue) throws Exception {

		context.getUnderWritingPage().enterRevenuevalues(previousval,currentValue);
	}
	@Then("User submit the cover details")
	public void coverDetails() throws Exception{
		context.getUnderWritingPage().coverDetails();
	}
	@Then("I enter the Public Body details {string} {string} {string} {string} {string} {string} {string}")
	public void publicBodyDetails(String description, String LegalStatus, String currentAssets, String currentLiabilities, String annualExpens, String govtFunds, String otherActivities) throws Exception {
		context.getUnderWritingPage().publicBodyDetails(description,LegalStatus,currentAssets,currentLiabilities, annualExpens,govtFunds,otherActivities);
	}
	@Then("I enter the Director Details {string} {string} {string}")
	public void directorDetails(String name,String qualification,String experience) throws Exception{
		context.getUnderWritingPage().directorDetails(name,qualification,experience);
	}

	@Then("I enter the Accounting and internal control practices")
	public void ControlPractices() throws Exception{
		context.getUnderWritingPage().ControlPractices();
	}

	@Then("I enter the employment practise details")
	public void employementDetails() throws Exception{
		context.getUnderWritingPage().employementDetails();
	}

	@Then("I enter POllution practices Details")
	public void pollutionDetails() throws Exception{
		context.getUnderWritingPage().pollutionDetails();
	}

	@Then("I enter Insurance and claims details")
	public void InsuranceAandClaimDetails() throws Exception{
		context.getUnderWritingPage().InsuranceAandClaimDetails();
	}

	@Then("I enter details in the Data protection {string} {string} Screen")
	public void i_enter_details_in_the_Data_protection_Screen(String byWhom, String provideDetails) throws Exception {
		context.getUnderWritingPage().enterrequiredfilledforDataProtectionScreen(byWhom,provideDetails);
	}

	@Then("I enter the details in Data access and recovery Screen")
	public void i_enter_the_details_in_Data_access_and_recovery_Screen() throws Exception {
		context.getUnderWritingPage().enterRequiredDetaisinDataaccessandrecovery();
	}

	@Then("I enter the details in  Outsourcing activities Screen")
	public void i_enter_the_details_in_Outsourcing_activities_Screen() throws Exception {
		// Write code here that turns the phrase above into concrete actions
		context.getUnderWritingPage().enterSelectRequiredvalueforOutsourcing();
	}

	@Then("I enter the details Claims information Screen")
	public void i_enter_the_details_Claims_information_Screen() throws Exception {
		context.getUnderWritingPage().enterdetailstheClaimsinformation();
	}



	@Then("I select update policy from more actions")
	public void updatePolicyFromMoreActions() throws Exception {
		context.getUnderWritingPage().selectUpdatePolicyFromMoreActions();

	}

	@Then("I enter updated values of {string} and {string}")
	public void enterUpdatedFTEandEffectiveDate(String FTE, String effectiveDate) throws Exception {
		try {
			context.getUnderWritingPage().fetchCaseID();
			context.getUnderWritingPage().enterStartDateOfInsurance(effectiveDate);
			context.getUnderWritingPage().enterNumberOfFTE(FTE);

		}catch(Exception e) {
			SeleniumFunc.waitFor(5);
		}
	}

	@Then("I search and select org using {string} and {string}")
	public void searchOrgAndSelectContact(String OrgName, String ZIPCode) throws Exception{
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().searchOrgAndSelectContact(OrgName, ZIPCode);
	}


	@Then("I search and select org using client segment {string}")
	public void searchOrgAndSelectContactUsingClientSegment(String clientSegment) throws Exception{
		SeleniumFunc.switchToDefaultContent();
		String orgName = "";
		if(clientSegment.equalsIgnoreCase("SCH")) {
			orgName = ObjectRepo.reader.getOrganisation();
		}else if(clientSegment.equalsIgnoreCase("CEM")) {
			orgName = ObjectRepo.reader.getCemeteryOrganisation();
		}else if(clientSegment.equalsIgnoreCase("PAP")) {
			orgName = ObjectRepo.reader.getPropAndPlanningOrganisation();
		}else if(clientSegment.equalsIgnoreCase("VCF")) {
			orgName = ObjectRepo.reader.getVicFleetOrganisation();
		}
		context.getUnderWritingPage().searchOrgAndSelectContact(orgName, "");
	}

	@Then("I verify that More actions is disabled for the policy tile")
	public void verifyMoreActionsDisabled() throws Exception{
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().verifyMoreActionsIsDisabled();
	}

	@Then("I check the case status to be {string}")
	public void i_check_case_status(String expectedStatus) throws Exception {
		context.getUnderWritingPage().validateCaseStatus(expectedStatus);
	}

	@And("I note down the case ID")
	public void note_down_case_id() throws Exception {

		context.getUnderWritingPage().fetchCaseID();
	}

	@Then("I enter {string} and details of persons {string}")
	public void i_enter_volunteers_details_for_PropandPlan(String date, String volunteers) throws Exception {

		context.getUnderWritingPage().enterStartDateOfInsurance(date);
		SeleniumFunc.waitFor(1);
		context.getUnderWritingPage().enterPersonsDetailsForCategories(volunteers, "", "", "", "", "", "");
	}

	@And("I select No to previous GPA policy details")
	public void selectNoToPrevPolicy() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().selectNoForPrevGPAPolicy();
	}

	@Then("I enter updated values of PnP GPA {string} and details of persons {string} {string} {string}")
	public void i_enter_volunteers_details_for_PropandPlan(String date, String volunteers, String boardMembers, String workExperienceStudents) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().enterStartDateOfInsurance(date);
		SeleniumFunc.waitFor(1);
		context.getUnderWritingPage().enterPersonsDetailsForCategories(volunteers, boardMembers, workExperienceStudents, "", "", "", "");
	}

	@Then("I validate the premium of PnP GPA for {string} using values {string} {string} {string} and {string}")
	public void validate_PnP_GPA_premium(String volunteers, String date, String multiplier, String gst, String stampduty) throws Exception {
//
//
		long lngPersons = Integer.parseInt(volunteers);
		int intdays = context.getUnderWritingPage().findDaysRemaining(date);
		float floatmul = Float.parseFloat(multiplier);
		float floatgst = Float.parseFloat(gst);
		float floatstamp = Float.parseFloat(stampduty);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidatePnPGPAPremiumDetails(lngPersons, intdays, floatmul, floatgst, floatstamp);
	}

	@Then("I validate the endorsement premium of PnP GPA for {string} {string} {string} {string} from {string} using values {string} {string} and {string}")
	public void validate_PnP_GPA_endorsement_premium(String volunteers, String updatedvolunteers, String boardMembers, String workExperienceStudents, String date, String multiplier, String gst, String stampduty) throws Exception {
//
//
		long lngCurrentPersonsCount = Integer.parseInt(volunteers);
		long lngEndorsedPersonsCount = Integer.parseInt(updatedvolunteers) + Integer.parseInt(boardMembers) + Integer.parseInt(workExperienceStudents);
		int intdays = context.getUnderWritingPage().findDaysRemaining(date);
		float floatmul = Float.parseFloat(multiplier);
		float floatgst = Float.parseFloat(gst);
		float floatstamp = Float.parseFloat(stampduty);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidatePnPGPAEndorsementPremiumDetails(lngCurrentPersonsCount, lngEndorsedPersonsCount, intdays, floatmul, floatgst, floatstamp);
	}

	@Then("I enter International travel {string} details with NO travel to listed countries")
	public void enter_international_trips_with_no_travel_to_listed_countries(String internationalTrips) throws Exception {

		context.getUnderWritingPage().enterInternationalTripsWithNoTravelToListedCountries(internationalTrips);
		SeleniumFunc.waitFor(1);
	}

	@Then("I enter Domestic travel {string} details")
	public void enterDomesticTripsDetails(String domesticTrips) throws Exception {
		SeleniumFunc.waitFor(2);
		context.getUnderWritingPage().enterDomesticTrips(domesticTrips);
		SeleniumFunc.waitFor(1);
		context.getUnderWritingPage().travelNotExceed180Days();
		SeleniumFunc.waitFor(1);
	}

	@Then("I select No to travel exceeding 180 days")
	public void selectTravelNotExceeding180Days() throws Exception {
		context.getUnderWritingPage().travelNotExceed180Days();
		SeleniumFunc.waitFor(1);
	}

	@Then("I select Yes to travel exceeding 180 days and enter {string}")
	public void selectTravelExceeding180Days(String estimateNumber) throws Exception {

		context.getUnderWritingPage().travelExceed180Days(estimateNumber);
		SeleniumFunc.waitFor(1);
	}

	@Then("I note the case ID")
	public void getCaseID() throws Exception {
		try {
			SeleniumFunc.switchToDefaultContent();
			context.getUnderWritingPage().fetchCaseID();
		}catch(Exception e) {
			SeleniumFunc.waitFor(1);
			System.out.println(e.getMessage());
		}
	}
	@Then("I validate the premium of BTV policy using values {string} {string} {string} {string} {string} {string} {string} and {string}")
	public void validateBTVPremium(String startdate, String internationalTrips, String domesticTrips,
								   String internationalMultiplier, String domesticMultiplier, String gst, String internationalStampduty,
								   String domesticStampduty) throws Exception {
		int intdays = context.getUnderWritingPage().findDaysRemaining(startdate);
		int intInternationalTrips = Integer.parseInt(internationalTrips);
		int intDomesticTrips = Integer.parseInt(domesticTrips);
		float floatinternationalMul = Float.parseFloat(internationalMultiplier);
		float floatDomesticMul = Float.parseFloat(domesticMultiplier);
		float floatgst = Float.parseFloat(gst);
		float floatInternationalStamp = Float.parseFloat(internationalStampduty);
		float floatDomesticStamp = Float.parseFloat(domesticStampduty);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidateBTVPremiumDetails(intdays, intInternationalTrips, intDomesticTrips, floatinternationalMul, floatDomesticMul, floatgst, floatInternationalStamp, floatDomesticStamp);
	}

	@Then("I validate the endorsement premium of BTV policy using values {string} {string} {string} {string} {string} {string} {string} {string} {string} and {string}")
	public void 	validateBTVEndorsementPremium(String startdate, String internationalTrips, String domesticTrips,
												 String updatedInternationalTrips, String updatedDomesticTrips, String internationalMultiplier, String domesticMultiplier, String gst, String internationalStampduty,
												 String domesticStampduty) throws Exception {
		int intdays = context.getUnderWritingPage().findDaysRemaining(startdate);
		int intInternationalTrips = Integer.parseInt(internationalTrips);
		int intDomesticTrips = Integer.parseInt(domesticTrips);
		int intUpdatedInternationalTrips = Integer.parseInt(updatedInternationalTrips);
		int intUpdatedDomesticTrips = Integer.parseInt(updatedDomesticTrips);
		float floatinternationalMul = Float.parseFloat(internationalMultiplier);
		float floatDomesticMul = Float.parseFloat(domesticMultiplier);
		float floatgst = Float.parseFloat(gst);
		float floatInternationalStamp = Float.parseFloat(internationalStampduty);
		float floatDomesticStamp = Float.parseFloat(domesticStampduty);
		context.getUnderWritingPage().calculateAndValidateBTVEndorsementPremiumDetails(intdays, intInternationalTrips, intDomesticTrips, intUpdatedInternationalTrips, intUpdatedDomesticTrips, floatinternationalMul, floatDomesticMul, floatgst, floatInternationalStamp, floatDomesticStamp);
	}

	@Then("I provide a schedule of {string} {string}{string} {string}{string}{string}assets the organisation")
	public void i_provide_a_schedule_of_assets_the_organisation(String StreetNumber,String StreetName,String suburb,String pindcode,String State,String country) throws Exception {
		context.getUnderWritingPage().assetsOrganization(StreetNumber,StreetName,suburb,pindcode,State,country);
	}

	@Then("I provide details facility {string} {string} {string} {string} {string} {string}")
	public void i_provide_details_facility(String string, String string2, String string3, String string4, String string5, String string6) {
		// Write code here that turns the phrase above into concrete actions
		throw new cucumber.api.PendingException();
	}

	@Then("I provide owns watercraft")
	public void i_provide_owns_watercraft() {
		// Write code here that turns the phrase above into concrete actions
		throw new cucumber.api.PendingException();
	}

	@Then("I provide details consequential financial loss cover{string} {string}")
	public void i_provide_details_consequential_financial_loss_cover(String string, String string2) {
		// Write code here that turns the phrase above into concrete actions
		throw new cucumber.api.PendingException();
	}

	@Then("I provide preferred deductible to apply in the event of a claim {string}")
	public void i_provide_preferred_deductible_to_apply_in_the_event_of_a_claim(String string) {
		// Write code here that turns the phrase above into concrete actions
		throw new cucumber.api.PendingException();
	}

	@Then("I enter the required details {string}{string} {string} for Vehicle details page")
	public void i_enter_the_required_details_for_Vehicle_details_page(String vehicle1, String vehicle2, String vehicle3) throws Exception {
		context.getUnderWritingPage().details_for_Vehicle_details_pagdetails_for_Vehicle_details_page(vehicle1,vehicle2,vehicle3);
	}
	@Then("I enter the required details {string}{string} {string} for Vehicle details page for cemetry trust")
	public void i_enter_the_required_details_for_Vehicle_details_page1(String vehicle1, String vehicle2, String vehicle3) throws Exception {
		context.getUnderWritingPage().details_for_Vehicle_details_pagdetails_for_Vehicle_details_page_CT(vehicle1,vehicle2,vehicle3);
	}

	@Then("I enter the required details for Drivers page")
	public void i_enter_the_required_details_for_Drivers_page() throws Exception {
		context.getUnderWritingPage().enter_the_required_details_for_Drivers_page();
	}

	@Then("I enter the required details for Insurance history page")
	public void i_enter_the_required_details_for_Insurance_history_page() throws Exception {
		context.getUnderWritingPage().enter_the_required_details_for_Insurance_history_page();
	}

	@Then("I enter the requred details for Accumulation page")
	public void i_enter_the_requred_details_for_Accumulation_page() throws Exception {
		context.getUnderWritingPage().enter_the_requred_details_for_Accumulation_page();
	}

	@Then("I enter the reuired details for Summary and review page")
	public void i_enter_the_reuired_details_for_Summary_and_review_page() throws Exception {
		context.getUnderWritingPage().checkDeclaration();
	}

	@Then("I fill the Constrution Risk information questions")
	public void fillConstrucitonRisksInformationQuestions() throws Exception {
//		context.getUnderWritingPage().fillConsRiskQuestions();
		context.getUnderWritingPage().fillConstructionRiskQuestions();
		SeleniumFunc.waitFor(1);
	}

	@Then("I click Add button to add a contract")
	public void clickAddButton() throws Exception {
		context.getUnderWritingPage().clickAddButton();
		SeleniumFunc.waitFor(1);
	}

	@Then("I click ViewEdit all contracts button")
	public void i_click_view_edit_all_contracts_button() throws Exception {
		context.getUnderWritingPage().clickViewEditButton();
	}

	@Then("I click on Add item button")
	public void i_click_on_add_item_button() throws Exception {
		context.getUnderWritingPage().clickAddItemButton();
	}

	@Then("I fill the contract details {string} {string} and click on submit")
	public void i_fill_the_contract_details_and_click_on_submit(String contractType, String estimatedProjectValue) {
		context.getUnderWritingPage().FillContractDetailsAndClickSubmit(contractType, estimatedProjectValue);
	}

	@Then("I fill contract details {string} {string}")
	public void iFillContractDetails(String contractType, String estimatedProjectValue) throws Exception {
//		context.getUnderWritingPage().expandContractDetails();
		context.getUnderWritingPage().fillContractDetails(contractType, estimatedProjectValue);
	}

	@Then("I fill the contract location details")
	public void iFillContractLocationDetails() throws Exception {
		context.getUnderWritingPage().clickCantFindAddress();
		context.getUnderWritingPage().fillContractAddress();
	}

	@Then("I update base premium for verify uper limit {string}")
	public void i_update_base_premium_for_verify_uper_limit(String basePrimium) throws Exception {
		SeleniumFunc.waitFor(5);

		context.getUnderWritingPage().clickUpdateBasePremButtonFinalizeQuote();
		SeleniumFunc.robotEnter();
		context.getUnderWritingPage().UpdateBasePremiumFromUWPortal(basePrimium,"baseepremmq");
	}


	@Then("User enters the required details {string}	{string} {string} for Public and products liability page")
	public void user_enters_the_required_details_for_Public_and_products_liability_page(String Operationalrevenue,String VictorGovfunding,String Otherfunding ) throws Exception {
		context.getUnderWritingPage().enterPublic_and_products_liability_page(Operationalrevenue,VictorGovfunding, Otherfunding);

	}

	@Then("User enters the required details for Incidents and potential claims")
	public void user_enters_the_required_details_for_Incidents_and_potential_claims() throws Exception {
		context.getUnderWritingPage().enters_the_required_details_for_Incidents_and_potential_claims();
	}

	@Then("User enters the required details for Interstate or overseas work")
	public void user_enters_the_required_details_for_Interstate_or_overseas_work() throws Exception {
		context.getUnderWritingPage().enters_the_required_details_for_Interstate_or_overseas_work();
	}

	@Then("User enters the required details for Affliation")
	public void user_enters_the_required_details_for_Affliation() throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Affliation();
	}

	@Then("User enters the required details {string} {string}for Storage of hazardous substances or dangerous goods")
	public void user_enters_the_required_details_for_Storage_of_hazardous_substances_or_dangerous_goods(String ProtectionLicense,String yourpremises) throws Exception {
		context.getUnderWritingPage().Storage_of_hazardous_substances_or_dangerous_goods(ProtectionLicense,yourpremises);
	}

	@Then("User enters the required details for Airfield liability")
	public void user_enters_the_required_details_for_Airfield_liability() throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Airfield_liability();
	}

	@Then("User enters the required details for Product liability")
	public void user_enters_the_required_details_for_Product_liability() throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Product_liability();
	}

	@Then("User enters the required details for Contractors Sub contractors")
	public void user_enters_the_required_details_for_Contractors_Sub_contractors() throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Contractors_Sub_contractors();
	}

	@Then("User enters the required details {string} {string} {string} {string} {string} {string} {string} {string} {string}for Visitors")
	public void user_enters_the_required_details_for_Visitors(String nofvisitors,String UnitLevel,String SouthWales,String StreetNumber,String StreetName,String Suburb,String State,String Postcode,String Country) throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Visitors(nofvisitors, UnitLevel, SouthWales, StreetNumber, StreetName, Suburb, State, Postcode, Country);
	}

	@Then("User enters the required details {string} for General information")
	public void user_enters_the_required_details_for_General_information(String Qualifiedstaff) throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_General_information(Qualifiedstaff);
	}

	@Then("User enters the required details for Insurance history")
	public void user_enters_the_required_details_for_Insurance_history() throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Insurance_history();
	}

	@Then("User enters the required details {string}{string}{string}for Professional activities")
	public void user_enters_the_required_details_for_Professional_activities(String proact,String Natureofwork,String percentage) throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Professional_activities(proact,Natureofwork,percentage);
	}

	@Then("User enters the required details for Joint ventures")
	public void user_enters_the_required_details_for_Joint_ventures() throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Joint_ventures();
	}

	@Then("User enters the required details for Overseas work")
	public void user_enters_the_required_details_for_Overseas_work() throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Overseas_work();
	}

	@Then("User enters the required details for Fee income")
	public void user_enters_the_required_details_for_Fee_income() throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Fee_income();
	}

	@Then("User enters the required details {string}{string} for Contractors and sub contractors")
	public void user_enters_the_required_details_for_Contractors_and_sub_contractors(String potentialindemnities, String grossprofessionalfees) throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Contractors_and_sub_contractors(potentialindemnities,grossprofessionalfees);
	}

	@Then("User enters the required details for Risk management")
	public void user_enters_the_required_details_for_Risk_management() throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Risk_management();
	}

	@Then("User enters the required details for Claims and circumstances")
	public void user_enters_the_required_details_for_Claims_and_circumstances() throws Exception {
		context.getUnderWritingPage().user_enters_the_required_details_for_Claims_and_circumstances();
	}

	@Then("User enters the required details for Summary and review")
	public void user_enters_the_required_details_for_Summary_and_review() throws Exception {
		context.getUnderWritingPage().checkDeclaration();
	}
	@And("I accept the addnote for uw")
	public void enter_notes_uw() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().enterNoteasUW();
		SeleniumFunc.waitFor(2);
	}
	@Then("I fill the Optional Extensions details {string}")
	public void iFillOptionalExtensionDetails(String largestSingleContractVal) throws Exception {
		context.getUnderWritingPage().fillLargestSingleContractValue(largestSingleContractVal);
		context.getUnderWritingPage().fillOptionalExtensionDetails();
	}

	@Then("I validate the premium of Construction Risks using {string} {string} {string} {string} {string}")
	public void validateConstructionRisksPremium(String EstimatedProjectValue, String materialDamageMul, String LiabilityMul, String gst, String stampduty) throws Exception {
		float floatEstimatedProjectValue = Float.parseFloat(EstimatedProjectValue);
		float floatMaterialDamageMul = Float.parseFloat(materialDamageMul);
		float floatLiabilityMul = Float.parseFloat(LiabilityMul);
		float floatgst = Float.parseFloat(gst);
		float floatstamp = Float.parseFloat(stampduty);
		//		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidateConstructionRisksPremiumDetails(floatEstimatedProjectValue, floatMaterialDamageMul, floatLiabilityMul, floatgst, floatstamp);
	}

	@Then("I fill updated contract details {string} {string}")
	public void iFillUpdatedContractDetails(String newContractType, String newEstimatedProjectValue) throws Exception {
		//		context.getUnderWritingPage().expandContractDetails();
		context.getUnderWritingPage().fillUpdateContractDetails(newContractType, newEstimatedProjectValue);
	}

	@Then("I validate the endorsement premium of Construction Risks using {string} {string} {string} {string} {string} {string} {string} {string}")
	public void validateConstructionRisksPremium(String EstimatedProjectValue, String materialDamageMul, String LiabilityMul, String newEstimatedProjectValue, String newMaterialDamageMul, String newLiabilityMul, String gst, String stampduty) throws Exception {
		float floatEstimatedProjectValue = Float.parseFloat(EstimatedProjectValue);
		float floatMaterialDamageMul = Float.parseFloat(materialDamageMul);
		float floatLiabilityMul = Float.parseFloat(LiabilityMul);
		float floatNewEstimatedProjectValue = Float.parseFloat(newEstimatedProjectValue);
		float floatNewMaterialDamageMul = Float.parseFloat(newMaterialDamageMul);
		float floatNewLiabilityMul = Float.parseFloat(newLiabilityMul);
		float floatgst = Float.parseFloat(gst);
		float floatstamp = Float.parseFloat(stampduty);
		SeleniumFunc.switchToDefaultContent();
		context.getUnderWritingPage().calculateAndValidateConstructionRisksEndorsementPremiumDetails(floatEstimatedProjectValue, floatMaterialDamageMul, floatLiabilityMul, floatNewEstimatedProjectValue, floatNewMaterialDamageMul, floatNewLiabilityMul, floatgst, floatstamp);
	}
	@Then("I enter the Number of Vehicle {string} and submit")
	public void NumberOfVehicles(String NumberOfVehicles) throws Exception {
		context.getUnderWritingPage().NumberOfVehicles(NumberOfVehicles);

	}

	@Then("Enter the new employement drivers")
	public void employementDrivers() throws Exception {
		context.getUnderWritingPage().employementDrivers();

	}

	@Then("I submit the Insurance history")
	public void InsuranceHistory() throws Exception {
		context.getUnderWritingPage().InsuranceHistory();

	}

	@Then("I submit the Accumulation")
	public void Accumulation() throws Exception {
		context.getUnderWritingPage().Accumulation();

	}

	@Then("I update the endorsement for Motor Vehicle")
	public void EndorsementMotor() throws Exception {
		context.getUnderWritingPage().EndorsementMotor();

	}

}
