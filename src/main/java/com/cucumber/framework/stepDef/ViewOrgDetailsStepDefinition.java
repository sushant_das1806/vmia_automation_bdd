package com.cucumber.framework.stepDef;

import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import com.cucumber.framework.PageObjects.HomePage;
import com.cucumber.framework.PageObjects.ViewOrgDetails;
import com.cucumber.framework.TestBase.TestBase;
//import cucumber.api.java.en.Then;

import com.cucumber.framework.context.TestContext;
import io.cucumber.java.en.*;

public class ViewOrgDetailsStepDefinition {


	TestContext context;
	public ViewOrgDetailsStepDefinition(TestContext cont){
		context=cont;
	}
	
	@Then("I want to click show my details")
	public void i_want_to_start_a_new_application_for_VMIA_insurance() throws Exception {
		context.getHomePage().clickViewOrgDetails();
	}
	
	@Then("I want to click view organization profile")
	public void i_want_to_click_view_org_profile() throws Exception {
		context.getHomePage().clickViewOrgDetails();
	   //Thread.sleep(10000);
	}
	
	@Then("I verify policy details from show my details page")
	public void verify_policydetails() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		context.getViewOrgDetailsPage().verifyPolicyDetails();
	}

	@Then("Switch to Asset tab and Open the Asset {string}")
	public void OpenAssetFromClientPortal(String assetId) throws Exception {
		context.getViewOrgDetailsPage().switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		context.getViewOrgDetailsPage().openAssetFromOrgnization(assetId);
	}

	
	@Then("I want to create new contact {string} {string} {string} {string} {string} {string}")
	public void create_new_contact(String title, String firstname, String lastname, String phonenumber, String emailid, String acnttype) throws Exception {
		context.getViewOrgDetailsPage().switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		context.getViewOrgDetailsPage().createNewContact(title, firstname, lastname, phonenumber, emailid, acnttype);
	}
	
	@Then("I want to update new contact {string}")
	public void update_new_contact(String updatedtitle) throws Exception {
		context.getViewOrgDetailsPage().switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		context.getViewOrgDetailsPage().updateNewContact(updatedtitle);
	}
	
	@Then("I want to enable or disable contact")
	public void enable_disable() throws Exception {
		context.getViewOrgDetailsPage().switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		context.getViewOrgDetailsPage().enabledisableContact();
	}
}
