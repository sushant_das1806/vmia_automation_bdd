package com.cucumber.framework.stepDef;

import java.util.concurrent.TimeUnit;
//import cucumber.api.java.en.*;
//import cucumber.api.java.en.Then;
import com.cucumber.framework.context.TestContext;
import io.cucumber.java.eo.Se;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

//import org.openqa.selenium.JavascriptExecutor;

import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import com.cucumber.framework.PageObjects.HomePageLoc;
import com.cucumber.framework.TestBase.TestBase;
import com.cucumber.framework.configreader.ObjectRepo;

import io.cucumber.java.en.*;

public class GeneralStepDefinition {

	TestContext context;

	public GeneralStepDefinition(TestContext cont){
		context=cont;
	}


	@Given("I have the URL of VMIA")
	public void i_have_the_of_VMIA() {
		if (ObjectRepo.reader.getRun().equalsIgnoreCase("jenkins")) {
			TestBase.getDriver().get(System.getProperty("URL"));
			TestBase.getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		} else {
			TestBase.getDriver().get(ObjectRepo.reader.getWebsite());
		}
		SeleniumFunc.waitFor(3);
		TestBase.getDriver().manage().timeouts().pageLoadTimeout(10,TimeUnit.SECONDS);
	}

	@Given("User enters the Internal URL from Browser")
	public void i_have_the_of_VMIA_Internal() {
		if (ObjectRepo.reader.getRun().equalsIgnoreCase("jenkins")) {
			TestBase.getDriver().get(System.getProperty("URL"));
			TestBase.getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		} else {
			TestBase.getDriver().get(ObjectRepo.reader.getWebsite());
		}

	}

	@Given("I have the external URL of VMIA")
	public void i_have_the_of_VMIA_external() {
		TestBase.getDriver().manage().deleteAllCookies();
		TestBase.getDriver().get(ObjectRepo.reader.getWebsiteExternal());
		TestBase.getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@When("User enter valid username  {string} and password {string} click on Login button")
	public void user_enter_valid_username_and_click_on_Login_button(String username, String pswd) throws Exception {
		SeleniumFunc.waitFor(10);
		context.getLoginPage().setUserNameAzure(ObjectRepo.reader.getUserName());
		context.getLoginPage().setPasswordAzure(ObjectRepo.reader.getPassword());
		context.getLoginPage().clickOnLoginBtnAzure();
		SeleniumFunc.switchToDefaultContent();
//		Thread.sleep(60000);
//		SeleniumFunc.goToFrameByName(HomePageLoc.strFrame1);
//		SeleniumFunc.waitForElement(By.xpath(HomePageLoc.view_org_profile), 120);

	}

	@When("User enter valid username  {string} and password {string} click on Login button for internal user")
	public void user_enter_valid_username_and_click_on_Login_buttonforinternaluser(String username, String pswd)
			throws Exception {
		Thread.sleep(1000);
		context.getLoginPage().setUserName(username);
		context.getLoginPage().setPassword(pswd);
//		context.getLoginPage().setUserName(ObjectRepo.reader.getTriageUserName());
//		context.getLoginPage().setPassword(ObjectRepo.reader.getTriagePassword());
		context.getLoginPage().clickOnLoginbtn();
//			

	}

	// @When("User enter valid username {string} and password {string} click on
	// Login button for internal user")
	@When("User when logs in as {string}")
	public void user_enter_valid_username_and_click_on_Login_buttonforinternaluser(String userProfile)
			throws Exception {
		SeleniumFunc.waitFor(3);

		if (userProfile.equalsIgnoreCase("TriageOfficer")) {
			context.getLoginPage().setUserName(ObjectRepo.reader.getTriageUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getTriagePassword());
		} else if (userProfile.equalsIgnoreCase("ClaimsOfficer")) {
			context.getLoginPage().setUserName(ObjectRepo.reader.getClaimsOfficerUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getCliamsOfficerPassword());
		} else if (userProfile.equalsIgnoreCase("ClaimsPortfolioManager")) {
			//PortfolioManager was updated with ClaimsPortfolioManager
			context.getLoginPage().setUserName(ObjectRepo.reader.getClaimsProtfolioMngrUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getClaimsProtfolioMngrPassword());
		} else if (userProfile.equalsIgnoreCase("RiskAdmin")) {
			context.getLoginPage().setUserName(ObjectRepo.reader.getRiskAdminUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getRiskAdminPassword());
		}else if (userProfile.equalsIgnoreCase("Underwriter")) {
			context.getLoginPage().setUserName(ObjectRepo.reader.getUnderwriterName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getUnderwriterPassword());
		}else if (userProfile.equalsIgnoreCase("ClientAdviser")) {
			context.getLoginPage().setUserName(ObjectRepo.reader.getClientAdviserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getClientAdviserPassword());
		}else if (userProfile.equalsIgnoreCase("MIPortfolioManager")) {
			context.getLoginPage().setUserName(ObjectRepo.reader.getMIPortfolioManagerName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getMIPortfolioManagerPassword());
		}else if (userProfile.equalsIgnoreCase("Finance")) {
			context.getLoginPage().setUserName(ObjectRepo.reader.getfinanceName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getfinanceNPassword());
		}else if(userProfile.equalsIgnoreCase("UWPortfoliaManger")) {
			context.getLoginPage().setUserName(ObjectRepo.reader.getUWPortfolioMangerUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getUWPortfolioMangerPassword());
		}
		else if(userProfile.equalsIgnoreCase("RiskAdvisor")) {
			context.getLoginPage().setUserName(ObjectRepo.reader.getRiskAdvisorUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getRiskAdvisorUserPassword());
		}else if(userProfile.equalsIgnoreCase("GeospatialCoordinator")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getGeospatialCoordinatorUsername());
			context.getLoginPage().setPassword(ObjectRepo.reader.getGeospatialCoordinatorPassword());
		}else if(userProfile.equalsIgnoreCase("HeadOfInsurance")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getHeadOfInsuranceUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getHeadOfInsurancePassword());
		}else if(userProfile.equalsIgnoreCase("ClientAdvisoryManager")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getClientAdvisorManagerUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getClientAdvisorManagerPassword());
		}else if(userProfile.equalsIgnoreCase("SeniorUnderwriterConstruction")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getSeniorUnderwriterConstructionUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getSeniorUnderwriterConstructionPassword());
		}else if(userProfile.equalsIgnoreCase("CasualityPortfolioManager")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getCasualityPortfolioManagerUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getCasualityPortfolioManagerPassword());
		}else if(userProfile.equalsIgnoreCase("PropertyPortfolioManager")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getPropertyPortfolioManagerUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getPropertyPortfolioManagerPassword());
		}else if(userProfile.equalsIgnoreCase("PortfolioManagerConstruction")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getPortfolioManagerConstructionUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getPortfolioManagerConstructionPassword());
		}else if(userProfile.equalsIgnoreCase("SeniorTechnicalSpecialist")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getSeniorTechnicalSpecialistUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getSeniorTechnicalSpecialistPassword());
		}else if(userProfile.equalsIgnoreCase("ChiefInsuranceOfficer")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getChiefInsuranceOfficerUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getChiefInsuranceOfficerPassword());
		}else if(userProfile.equalsIgnoreCase("ClaimsAdmin")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getClaimsAdminUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getClaimsAdminPassword());
		}else if(userProfile.equalsIgnoreCase("Adminstartors")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getAdminstartorsUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getAdminstartorsPswd());
		}else if(userProfile.equalsIgnoreCase("BusinessManager")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getBusinessmanagerUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getBusinessmanagerPswd());
		}else if(userProfile.equalsIgnoreCase("InsightAndAnalytics")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getInsightAnalyticsUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getInsightAnalyticsPswd());
		}else if(userProfile.equalsIgnoreCase("InsuranceAdmin")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getInsuranceAdminUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getInsuranceAdminPswd());
		}else if(userProfile.equalsIgnoreCase("PortFolioManager")){
			context.getLoginPage().setUserName(ObjectRepo.reader.getProtfolioManagerUserName());
			context.getLoginPage().setPassword(ObjectRepo.reader.getProtfolioManagerPswd());
		}
		context.getLoginPage().clickOnLoginbtn();
		SeleniumFunc.waitFor(6);
	}

	@When("I enter the {string}")
	public void i_enter_the(String user) throws Exception {
		context.getLoginPage().setUserName(user);
	}

	@When("I enter the {string} for azure")
	public void i_enter_the_user(String user) throws Exception {
		context.getLoginPage().setUserNameAzure(user);
	}



	@When("I enter {string}")
	public void i_enter(String pass) throws Exception {
		context.getLoginPage().setPassword(pass);
	}

	@When("I enter {string} for azure")
	public void i_enter_pass(String pass) throws Exception {
		context.getLoginPage().setPasswordAzure(pass);
	}

	@When("I click login button")
	public void i_Click_login_button() throws Exception {

		context.getLoginPage().clickOnLoginbtn();
		// context.getLoginPage().getRefresh();

	}

	@When("I click login button for azure")
	public void i_Click_login_button_azure() throws Exception {
		context.getLoginPage().clickOnLoginBtnAzure();
		// context.getLoginPage().getRefresh();
//	    SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);

		// SeleniumFunc.waitFor(3);
	}

	@When("Click New and Select {string} in AssetManagement")
	public void clickNewButtonAndOptionSelect(String option) throws Exception {

		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.clientAdvisor_New);
		SeleniumFunc.waitFor(2);
		Actions action=new Actions(TestBase.getDriver());
		WebElement ele=TestBase.getDriver().findElement(By.xpath(HomePageLoc.ClientAdviosr_New_AssetManagement));
		action.moveToElement(ele).build().perform();
//		SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.ClientAdviosr_New_AssetManagement);
		SeleniumFunc.waitFor(4);
//		WebElement ele1=TestBase.getDriver().findElement(By.xpath("//li[@class='menu-item menu-item-enabled menu-item-active']//ul//li[@class='menu-item menu-item-enabled']/a[1]"));
//		action.moveToElement(ele1).build().perform();
		SeleniumFunc.xpath_GenericMethod_Click("//li[@class='menu-item menu-item-enabled menu-item-active']//ul//li/a/span/span[text()='"+option+"']/ancestor::a");
	}





	@Then("I am logged in to the portal")
	public void i_am_logged_in_to_the_portal() throws Exception {
		/*
		 * Thread.sleep(20000); SeleniumFunc.
		 * xpath_GenericMethod_Click("//div[text()='Click here to start a new policy application']"
		 * ); Thread.sleep(20000);
		 */
	}

	@Then("I logout of the application")
	public void i_am_logged_out() throws Exception {
		context.getLoginPage().performLogout();
	}

	@Then("I logout Pegadevstudio application")
	public void i_logout_pegadevstudio_application() throws Exception {
		context.getLoginPage().devStudioLogout();
	}

	@Given("Verify the case status is {string}")
	public void verify_the_case_status_is(String stats) throws Exception {
		context.getUnderWritingPage().verifyTheCaseStatus(stats);
	}

	@Then("User logout of the application")
	public void User_am_logged_out() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getLoginPage().performLogout();
		// Thread.sleep(2);
	}

	@Then("Now I logout of the application")
	public void i_am_logged_out_1() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getLoginPage().performLogout_1();
		Thread.sleep(2000);
	}

	@Then("I close the external link")
	public void i_close_browser() throws Exception {
		context.getLoginPage().openInternalLink();
		Thread.sleep(2000);
	}

    @When("Print Pass Status")
    public void printPassStatus() {
		System.out.println("Test Passed");
    }

	@Then("I open the {string} workbasket in the finance portal")
	public void iOpenTheWorkbasketInTheFinancePortal(String workBasket) throws Exception {
		try{
			SeleniumFunc.switchToDefaultContent();
			context.getClientAdviserPage().closeAllInteractions();
		}catch(Exception e){
			SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
		}
		context.getFinance().switchToWorkbasket(workBasket);
	}

	@When("verify the VMIA job role in {string}")
	public void verify_the_vmia_job_role_in(String portalName) throws Exception {
		if(portalName.equalsIgnoreCase("ClientAdvisoryManager")){
			context.getUnderWritingPage().verifyNewApplication();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			SeleniumFunc.xpath_GenericMethod_Click("//span[@id='close']");
			SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
			context.getFinance().switchToWorkbasket("Underwriting");
			context.getUnderWritingPage().verifyEndorseApplication();
			context.getUnderWritingPage().verifyAddEndorsementText();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			context.getUnderWritingPage().verifyUpdateRiskInfo();
		}else if(portalName.equalsIgnoreCase("Underwriter")){
			context.getUnderWritingPage().verifyEndorseApplication();
			context.getUnderWritingPage().verifyUpdateRiskInfo();
		}else if(portalName.equalsIgnoreCase("SeniorUnderwriterConstruction")){
			context.getUnderWritingPage().verifyNewApplication();
			context.getUnderWritingPage().verifyUpdateRiskInfo();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
			context.getFinance().switchToWorkbasket("Underwriting");
			context.getUnderWritingPage().verifyEndorseApplication();
			context.getUnderWritingPage().verifyAddEndorsementText();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			context.getUnderWritingPage().verifyUpdateRiskInfo();
		}else if(portalName.equalsIgnoreCase("MIPortfolioManager")){
			context.getUnderWritingPage().verifyNewApplication();
			context.getUnderWritingPage().verifyUpdateRiskInfo();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
			context.getFinance().switchToWorkbasket("Underwriting");
			context.getUnderWritingPage().verifyEndorseApplication();
			context.getUnderWritingPage().verifyAddEndorsementText();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			context.getUnderWritingPage().verifyUpdateRiskInfo();
		}else if(portalName.equalsIgnoreCase("CasualityPortfolioManager")){
			context.getUnderWritingPage().verifyNewApplication();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
			context.getFinance().switchToWorkbasket("Underwriting");
			context.getUnderWritingPage().verifyEndorseApplication();
			context.getUnderWritingPage().verifyAddEndorsementText();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			context.getUnderWritingPage().verifyUpdateRiskInfo();
		}else if(portalName.equalsIgnoreCase("PropertyPortfolioManager")){
			context.getUnderWritingPage().verifyNewApplication();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
			context.getFinance().switchToWorkbasket("Underwriting");
			context.getUnderWritingPage().verifyEndorseApplication();
			context.getUnderWritingPage().verifyAddEndorsementText();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			context.getUnderWritingPage().verifyUpdateRiskInfo();
		}else if(portalName.equalsIgnoreCase("PortfolioManagerConstruction")){
			context.getUnderWritingPage().verifyNewApplication();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
			context.getFinance().switchToWorkbasket("Underwriting");
			context.getUnderWritingPage().verifyEndorseApplication();
			context.getUnderWritingPage().verifyAddEndorsementText();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			context.getUnderWritingPage().verifyUpdateRiskInfo();
			//Portal access - Access to be added, is missing
		}else if(portalName.equalsIgnoreCase("SeniorTechnicalSpecialist")){
			context.getUnderWritingPage().verifyNewApplication();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
			context.getFinance().switchToWorkbasket("Underwriting");
			context.getUnderWritingPage().verifyEndorseApplication();
			context.getUnderWritingPage().verifyAddEndorsementText();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			context.getUnderWritingPage().verifyUpdateRiskInfo();
		}else if(portalName.equalsIgnoreCase("HeadOfInsurance")){
			context.getUnderWritingPage().verifyNewApplication();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
			context.getFinance().switchToWorkbasket("Underwriting");
			context.getUnderWritingPage().verifyEndorseApplication();
			context.getUnderWritingPage().verifyAddEndorsementText();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			//Portal access - Access to be added, is missing
		}else if(portalName.equalsIgnoreCase("ChiefInsuranceOfficer")){
			context.getUnderWritingPage().verifyNewApplication();
			context.getUnderWritingPage().verifyUpdateRiskInfo();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
			context.getFinance().switchToWorkbasket("Underwriting");
			context.getUnderWritingPage().verifyEndorseApplication();
			context.getUnderWritingPage().verifyAddEndorsementText();
			context.getUnderWritingPage().verifyUpdatePolicyDetails();
			context.getUnderWritingPage().verifyUpdateRiskInfo();
			//Portal access - Access to be added, is missing
		}

	}


	@And("I open the Payway Case and enter the Payway Number")
	public void iOpenThePaywayCaseAndEnterThePaywayNumber() throws Exception {
		context.getFinance().openPaywayCaseAndAddPaywayNumber();
	}

}
