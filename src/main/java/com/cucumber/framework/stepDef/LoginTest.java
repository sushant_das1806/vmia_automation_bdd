package com.cucumber.framework.stepDef;


import com.cucumber.framework.TestBase.TestBase;
import com.cucumber.framework.configreader.ObjectRepo;
import com.cucumber.framework.context.TestContext;
import io.cucumber.java.en.*;


public class LoginTest {
	// private final Logger log = LoggerHelper.getLogger(LoginTest.class);
	

	TestContext context;
	public LoginTest(TestContext cont){
		context=cont;
	}

	@Given("^Steps for login in case of failure$")
	public void steps_for_login_in_case_of_failure() throws Throwable {
	  System.out.println("Steps to reproduce:");
	  System.out.println("Open the browser and navigate to the url");
	  System.out.println("I enter username as 'CACSR'");
	  System.out.println("I enter password as 'Rules@1234'");
	  System.out.println("I click on Login button");
	  System.out.println("Login should be successful");
	}

	@Given("^Open the browser and navigate to the url$")
	public void open_the_browser_and_navigate_to_the_url() throws Throwable {
		//System.out.println("Before driver instantiation: "+TestBase.getDriver());
	//	TestBase.getDriver().get(ObjectRepo.reader.getWebsite()); //Run from local
	//	TestBase.getDriver().get(System.getProperty("URL"));  //Run from jenkins
		if(ObjectRepo.reader.getRun().equalsIgnoreCase("jenkins")) {
			TestBase.getDriver().get(System.getProperty("URL"));
		}else {
			TestBase.getDriver().get(ObjectRepo.reader.getWebsite());
		}

		//System.out.println("After driver instantiation: " +TestBase.getDriver());
	}

	@When("^I enter username as \"([^\"]*)\"$")
	public void i_enter_username_as(String username) throws Throwable {
		context.getLoginPage().setUserName(username);
	}

	@When("^I enter password as \"([^\"]*)\"$")
	public void i_enter_password_as(String password) throws Throwable {
		context.getLoginPage().setPassword(password);
		//System.out.println("Enter password " +password);
	}

	@When("^I click on Login button$")
	public void i_click_on_Login_button() throws Throwable {
		context.getLoginPage().clickOnLoginbtn();
	//System.out.println("click on login " +TestBase.getDriver());
	
	}

	@Then("^Login should be successful$")
	public void login_should_be_successful() throws Throwable {
		context.getLoginPage().verifyMsg();
	
	}
	
	@Given("^Steps for login with invalid credentials \"([^\"]*)\" and \"([^\"]*)\" in case of failure$")
	public void steps_for_login_with_invalid_credentials_and_in_case_of_failure(String username, String password) throws Throwable {
		  System.out.println("Steps to reproduce:");
		  System.out.println("Open the browser and navigate to the url");
		  System.out.println("I enter username as" +username);
		  System.out.println("I enter password as" +password);
		  System.out.println("I click on Login button");
		//  System.out.println("Login should not be successful");
	}

	@Then("^User should not be logged in$")
	public void user_should_not_be_logged_in() throws Throwable {
		context.getLoginPage().verifyLoginFail();
	}
	


}
