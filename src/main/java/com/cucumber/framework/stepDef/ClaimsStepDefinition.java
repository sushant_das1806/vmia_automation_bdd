package com.cucumber.framework.stepDef;

import com.cucumber.framework.PageObjects.Claims;
import com.cucumber.framework.TestBase.TestBase;
import com.cucumber.framework.context.TestContext;
import org.junit.Assert;
import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import io.cucumber.java.en.*;
import com.cucumber.framework.PageObjects.Claims;
import org.openqa.selenium.By;

import java.util.Random;


public class ClaimsStepDefinition {

	TestContext context;

	public ClaimsStepDefinition(TestContext cont){
		context=cont;
	}




	@Then("I am logged in to the insurance portal")
	public void i_am_logged_in_to_the_insurance_portal() {

	}

	@Then("I want to make a claim")
	public void i_want_to_make_a_claim() throws Exception {
		context.getHomePage().clickMakeClaim();
	}

	@Then("Search and select Oraganization {string}")
	public void search_and_select_Oraganization(String org) throws Exception {
		context.getClaimsPage().searchAndSelectOrgazation(org);
	}

	@Then("search with the selected organization")
	public void search_with_the_selected_organization() throws Exception {
		context.getClaimsPage().searchAndGetOrganization();
	}

	@Then("I want to make a claim from TriageOfficer")
	public void i_want_to_make_a_claim_from_TriageOfficer() throws Exception {
		context.getHomePage().clickMakeNewClaimsAsTraiageOfficer();
	}
	@Then("I want to make a claim from Claims Officer")
	public void i_want_to_make_a_claimClaimsOfficer() throws Exception {

		context.getHomePage().clickMakeClaim();
		// Thread.sleep(10000);
	}

	@Then("I want to make a claim for client adviser")
	public void i_want_to_make_a_claim_client_adviser() throws Exception {

		// homepage.clickMakeClaim();
		// Thread.sleep(10000);
	}



	@Then("I enter the date and time of loss")
	public void I_enter_the_date_and_time_of_loss() throws Exception {
		SeleniumFunc.waitFor(2);
		context.getClaimsPage().clickDate();
		context.getClaimsPage().clickToday();
		SeleniumFunc.waitFor(1);
	}


	@Given("Select contact from contact {string}")
	public void select_contact_from_contact(String contact) throws Exception {
		context.getClaimsPage().selectConatact(contact);
	}

	@Then("I enter the {string} and time of loss")
	public void I_enter_the_text_date_and_time_of_loss(String dateofloss) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().enterDateandTimeOfLoss(dateofloss);
		SeleniumFunc.waitFor(2);
	}

	@Then("I select the type of {string}")
	public void i_select_the_type_of_loss(String loss) throws Exception {

		context.getClaimsPage().selectLossType(loss);

	}
	@Given("Select construction loss subtype {string}")
	public void select_construction_loss_subtype(String subtype) throws Exception {
	       context.getClaimsPage().selectSubTypeOfLossForConstruction(subtype);
	}

	@Then("I select the lodging as {string}")
	public void selectLodge(String Lodge) throws Exception {
		context.getClaimsPage().selectLodge(Lodge);
	}

	@Then("I select the coverage as {string}")
	public void selectCoverage(String coverage) throws Exception {
		context.getClaimsPage().selectCoverageForClaim(coverage);
	}
	@Then("I Select the Suborg check box and SubOrg type {string}")
	public void i_Select_the_Suborg_check_box_and_SubOrg_type(String subOrgType) throws Exception {
		context.getClaimsPage().selectSubOrg(subOrgType);
	}

	@Then("I select the loss type of {string}")
	public void i_select_the_type_of_loss_req(String loss) throws Exception {
		context.getClaimsPage().selectLossTypeAdv(loss);
	}

	@When("I select the type of of {string}")
	public void i_select_the_type_of_of_Loss(String Loss) throws Exception {
		context.getClaimsPage().selectLossType1(Loss);
	}


	@Given("Enter the loss details {string} {string} {string}")
	public void enter_the_loss_details(String date, String lossdetails, String thridpary) throws Exception {
		context.getClaimsPage().enterLossDetails(date, lossdetails, thridpary);
	}


	@Then("Click Continue button")
	public void click_continue_button() throws Exception {
		context.getClaimsPage().clickContinueButton();

	}

	@Given("Click Continue button continuously for {int} times")
	public void click_continue_button_continuously_for_times(int numb) throws Exception {
		context.getClaimsPage().clickContinueButtonContinuosly(numb);
	}

	@Given("Click Continue button continuously")
	public void click_continue_button_continuously() throws Exception {

	}

	@Given("new step")
	public void new_step() {
		System.out.print("Hello");
	}

	@Then("Check Alert and error message for not entering the check details")
	public void checkPaymentMandatoryErrorMessage() throws Exception {
		Thread.sleep(1000);
		context.getClaimsPage().clickContinueButton();
//		context.getClaimsPage().validateErrorMessageOfPayment();
		Thread.sleep(30000);
	}

	@Then("I provide Incident loss for Property {string} {string}")
	public void propertyLossDetials(String how, String damage) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().propertyEmRepSSLossDetials(how,damage);
	}
	@Then("I provide the property loss details {string} {string} {string}")
	public void propertyLossDetials(String where, String what, String how) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().propertyLossDetials(where,what,how);
	}

	@Then("I provide the damaged items {string} {string}")
	public void propertyDamagedItems(String Description, String Amount) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().propertyDamagedItems(Description,Amount);
	}

	@Given("Enter the incident details {string} {string} {string}")
	public void enter_the_incident_details(String incidentDate,String IncidentDes,String claimantName) throws Exception {
		context.getClaimsPage().enterTheIncidentDetailsGPAVolunteersClaim(incidentDate, IncidentDes, claimantName);
	}

	@Given("Add medical item {string} {string}")
	public void add_medical_item(String item,String Amount) throws Exception {
		context.getClaimsPage().addMedicalExpenseItem(item, Amount);
	}

	@Given("Enter the loss details of motor claim {string} {string} {string} {string} {string}")
	public void enter_the_loss_details_of_motor_claim(String whereDamageOccur,String whatDamaheOccur,String releventdetails,String howdamageoccur,String wasIncidentReportToPolice) throws Exception {
		context.getClaimsPage().enterTheLossdetails_vehicleClaim( whereDamageOccur, whatDamaheOccur, releventdetails, howdamageoccur, wasIncidentReportToPolice);
	}

	@Given("Enter the vehicle detail {string} {string} {string}")
	public void enter_the_vehicle_detail(String vehicleType,String RegistrationNumber,String owner) throws Exception {
		context.getClaimsPage().enterVechileDetails_motorClaim(vehicleType, RegistrationNumber, owner);
	}

	@Given("Enter the claimant details {string} {string} {string} {string} {string} {string} {string} {string}")
	public void enter_the_claimant_details(String studentName,String DoB,String StudentAddress,String ParentsName,String RelationShiptoStudent,String ParentsAddress,String phonenumber,String email) throws Exception {
		context.getClaimsPage().EnterTheClaimantDetailsFlighrRiskGPAClaim(studentName, DoB, StudentAddress, ParentsName, RelationShiptoStudent, ParentsAddress, phonenumber, email);
	}

	@Given("Incident details {string} {string} {string}")
	public void incident_details(String IncidentDes,String IsIncidentReportToShool,String InjuriesSustained) throws Exception {
		context.getClaimsPage().EnterTheIncidentDetailsFlighrRiskGPAClaim(IncidentDes, IsIncidentReportToShool, InjuriesSustained);
	}

	@Given("Medical details {string} {string} {string}")
	public void medical_details(String IsSeekeTreatment,String IsPrivateHealthInsurance,String IsAmbulanceCoverd) throws Exception {
		context.getClaimsPage().MediCalDetails_FlightRisk_GPA(IsSeekeTreatment, IsPrivateHealthInsurance, IsAmbulanceCoverd);
	}

	@Then("Select coverage {string} for FineArts claim")
	public void SelectCoveregeInFineArtsClaim(String coverage) throws Exception {
		context.getClaimsPage().selectCoverage(coverage);
	}


	@Then("Select coverage {string} for travel insurance")
	public void SelectCoveregeInTravelInsurance(String coverage) throws Exception {
		context.getClaimsPage().SelectCoveragefortravelClaim(coverage);
	}

	@Given("Select Urgency level {string} and Claim contact {string} of the claim")
	public void select_Urgency_level_and_Claim_contact_of_the_claim(String urgencylevel, String contact) throws Exception {
		System.out.println("Control is here");
		context.getClaimsPage().selectUrgencyLevel(urgencylevel, contact);
	}

	@Given("Add Incident details {string} {string} {string}")
	public void add_incident_details(String TypeOfIncident, String IncidentDes, String Address) throws Exception {
		context.getClaimsPage().AddIncidentDetailsForFineArtsClaim(TypeOfIncident,IncidentDes,Address);
	}


	@Given("Enter the FineArts Details for claim {string} {string} {string} {string} {string}")
	public void enter_the_fine_arts_details_for_claim(String artist,String titleofwork,String dimension,String owner,String worth ) throws Exception {
		context.getClaimsPage().addArtWorkDetails(artist,titleofwork,dimension,owner,worth);
	}

	@Given("Select Incident report details in claim {string} {string}")
	public void select_incident_report_details_in_claim(String isSomeoneResponsibleFordamage,String isIncidentReported) throws Exception {
		context.getClaimsPage().enterResponsibleOfDamageAndPoliciReport(isSomeoneResponsibleFordamage,isIncidentReported);
	}

	@Given("Select patient whether patient is claimant {string}")
	public void select_patient_whether_patient_is_claimant(String options) throws Exception {
		context.getClaimsPage().selectPatientClaiment(options);
	}

	@Given("Enter the claims incident details {string} {string} {string} {string} {string}")
	public void enter_the_claims_incident_details(String incidentdes, String factaccount, String clinicSpc, String area, String disclousre) throws Exception {
		context.getClaimsPage().enterTheIncidentDetail(incidentdes, factaccount, clinicSpc, area, disclousre);
	}


	@Given("Select the claim source dropdown {string}")
	public void select_the_claim_source_dropdown(String claimsource) throws Exception {
		context.getClaimsPage().selectClaimsSource(claimsource);
	}

	@Given("Enter the patient details {string} {string} {string} {string} {string} {string}")
	public void enter_the_patient_details(String Name, String fanilyName, String gender, String dob, String medicalreocrdno, String admitstatus) throws Exception {
		context.getClaimsPage().enterThePatienDetails(Name, fanilyName, gender, dob, medicalreocrdno, admitstatus);
	}

	@Then("Validated the medical claim indemnity case status {string}")
	public void validated_the_case_status(String expecStatus) throws Exception {
		Assert.assertEquals(expecStatus,context.getClaimsPage().getClaimStausOfMIclaim());
	}

	@Then("Transafer the claim to user {string}")
	public void transafer_the_claim_to_user(String user) throws Exception {
		context.getClaimsPage().transferClaim(user);
	}

	@Given("Transfer the claim to user {string}")
	public void transfer_the_claim_to_user(String user) throws Exception {
		context.getClaimsPage().transferClaimToAdmin(user);
	}


	@Then("Click Save and Exit button")
	public void click_save_exit_button() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().clickSaveAndExitButton();
		// Thread.sleep(10000);
	}

	@Then("I select the property damages")
	public void select_property_damages() throws Exception {
		// context.getClaimsPage().switchToDefaultContent();
		context.getClaimsPage().propertyDamagesSECS();
		// Thread.sleep(10000);
	}

	@Then("I add item description {string} {string} {string} {string}")
	public void item_description(String itemdesc, String purprice, String coverage,String IsDetYesorNo) throws Exception {

		context.getClaimsPage().SECSScreen(itemdesc, purprice, coverage,IsDetYesorNo);
		// Thread.sleep(10000);
	}

	@When("I select the business row and verify listed policies for {string}")
	public void I_select_the_business_row(String losstype) throws Exception {
		context.getClaimsPage().selectBusinessRow();
	}

	@When("I provide loss details {string}")
	public void i_provide_loss_details(String lossdetails) throws Exception {
		context.getClaimsPage().enterLossDetails(lossdetails);
	}

	@Given("Click on mandatory fields notification or claim and coverage")
	public void click_on_mandatory_fields_notificationOrclaim_and_coverage() throws Exception {
		context.getClaimsPage().clickMandatoryOptions();
	}

	@Given("Enter the contract details {string} {string} {string} {string} {string} {string}")
	public void enter_the_contract_details(String contractionContract,String contractSinged,String commencementOfWork,String dueDate,String valueOfContract,String PartyMakingClaim) throws Exception {
		context.getClaimsPage().enterTheContractDetailsConstructionClaim(contractionContract, contractSinged, commencementOfWork, dueDate, valueOfContract, PartyMakingClaim);
	}

	@Given("Capture loss detais {string}")
	public void capture_loss_detais(String losstype) throws Exception {
		context.getClaimsPage().CaptureLossDetailsOfConstructionClaim(losstype);
	}

	@Given("Enter the Proerty damage details {string} {string} {string} {string} {string} {string}")
	public void enter_the_proerty_damage_details(String IncidentfirstReport,String howDamageOccur,String IsWitness,String RepotedPolicy,String estimateValueOfDamage,String descrptionOfDamage) throws Exception {
		context.getClaimsPage().enterThePropertDamageConstructionClaim(IncidentfirstReport, howDamageOccur, IsWitness,RepotedPolicy , estimateValueOfDamage, descrptionOfDamage);
	}

	@Given("Enter Personal Injury Details {string} {string} {string} {string} {string}")
	public void enter_personal_injury_details(String IncidentfirstReportPI,String InjuryPersons,String NatureOfInjury,String TreatmentProvided,String incidentWhatHappended) throws Exception {
		context.getClaimsPage().enterThePersonalInjuryDetailsConstructionClaim(IncidentfirstReportPI, InjuryPersons, NatureOfInjury, TreatmentProvided, incidentWhatHappended);
	}


	@Then("I am routed to Insured Client Details page and validate the fields")
	public void I_am_routed_to_insured_client() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().verifyFNOLCaseformat("FNOL");
		// context.getClaimsPage().verifyCaseStatus("NEW");
	}

	@Then("Click Finish button")
	public void I_click_finish_button() throws Exception {
		context.getClaimsPage().clickFinishButton();
	}

	@Then("Click Finish button without Action Refresh")
	public void I_click_finish_button_withoutActionRefresh() throws Exception {
		context.getClaimsPage().clickFinishButton();
	}



	@Then("I am routed to Client Banking Account details page")
	public void I_am_routed_to_client_banking_details() throws Exception {
	}

	@Then("{string} {string} {string} {string} {string} is fed")
	public void details_are_fed(String acntname, String bsb, String accntno, String bankname, String email)
			throws Exception {
//		Thread.sleep(30000);
		context.getClaimsPage().enterClientBankingAccountName(acntname);
		Random rnd = new Random();
		int number = rnd.nextInt(899999);
		int bsb1 = 100000 +number;
		context.getClaimsPage().enterClientBankingBSB(String.valueOf(bsb1));
		number = rnd.nextInt(8999999);
		int accntno1 = 1000000 + number;
		context.getClaimsPage().enterClientBankingAccountNumber(String.valueOf(accntno1));
		// context.getClaimsPage().enterClientBankingBankName(bankname);
		context.getClaimsPage().enterClientBankingEmail(email);

	}

	@Then("{string} is refed")
	public void details_are_refed(String acntname) throws Exception {
		context.getClaimsPage().enterClientBankingAccountNameAfterClearingOriginal(acntname);
	}

	@Given("Select {string} in Is traveler part of Insured company field")
	public void select_in_is_traveler_part_of_insured_company_field(String travelInfo) throws Exception {
		context.getClaimsPage().SelectIsTravelerPartOfInsuredComapany(travelInfo);
	}

	@Given("Enter the travel infromation of incident {string} {string} {string} {string} {string} {string} {string}")
	public void enter_the_travel_infromation_of_incident(String isBusinessTravel,String DepartureDate,String ReturnDate,String DepartureCity,String Incidetcity,String IncidetCountry,String exactlocation) throws Exception {
		context.getClaimsPage().enterTheTravelClaimIncidentDeatil(isBusinessTravel,DepartureDate,ReturnDate,DepartureCity,Incidetcity,IncidetCountry,exactlocation);
	}

	@Given("Select the Loss Property {string}")
	public void SelectLossProertyInTravelClaim(String lossdetail) throws Exception {
		context.getClaimsPage().selectThePropertyMissedInTravelClaim(lossdetail);
	}

	@Given("Enter the DamageDetails of Baggage property {string} {string} {string} {string} {string} {string}")
	public void enter_the_damage_details_of_baggage_property(String Damegedes,String lossdetails,String IsReportToPolice,String isLostByCarrier,String IsComplainRaisedAgainstCarrier,String IsAllMissiedProertyYours) throws Exception {
		context.getClaimsPage().enterTheIncidentReportDetailsForBaggaeProperyInTravelClaim(Damegedes, lossdetails, IsReportToPolice, isLostByCarrier, IsComplainRaisedAgainstCarrier, "test", IsAllMissiedProertyYours);
	}

	@Then("I provide interment holder details {string} {string} {string} {string} {string} {string} {string}{string}")
	public void interment_details(String holdername, String holderlastname, String contactfirstname,
								  String contactlastname, String addressloopupinput, String address, String state, String contactphonenumber)
			throws Exception {
		context.getClaimsPage().enterIntermentHolderDetails(holdername, holderlastname, contactfirstname, contactlastname,
				addressloopupinput, address, state, contactphonenumber);
	}



	@Then("I provide incident details {string} {string} {string} {string} {string} {string}")
	public void incident_details(String firstname, String lastname, String incidentlocation, String plotnumber,
								 String damagereason, String amountclaimed) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().monumentIncidentDetails(firstname, lastname, incidentlocation, plotnumber, damagereason, amountclaimed);

	}

	@And("I validate the {string} for claims")
	public void i_validate_final_status_claims(String status) throws Exception {
		Thread.sleep(4000);
		context.getClaimsPage().verifyFinalCaseStatus(status);
	}

	@Then("Extarct the claim number")
	public void extarct_the_claim_number() throws Exception {
//		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().extractClaimNumber();

	}

	@Given("Extract the case ID")
	public void extractTheCaseID() throws Exception {
		context.getClaimsPage().fetchClaimsID();
	}

	@Then("I extract the claim unit number from open case")
	public void extarct_the_claim_number_myopencase() throws Exception {
//		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().extractClaimNumberFromMyOpenCase();
	}

	@Then("Open the claim from {string} queue")
	public void open_the_claim_from_queue(String Queue) throws Exception {
		context.getClaimsPage().closeAllTabs();
//		TestBase.currentClaimNumber="CF-36242";
		context.getClaimsPage().openClaimUnassingedQueueFromSearch();
//		context.getClaimsPage().OpenClaimFromQueue(Queue);
	}

	@Then("review claim and check dupilicate")
	public void skip_review_claim_and_check_dupilicate() throws Exception {
		context.getClaimsPage().skip_reviewclaimDuplicate();
	}

	@Then("Select claim category {string} {string} {string}")
	public void select_claim_category(String claimcatory, String solicterneed, String Isclaimsenstive) throws Exception {
		context.getClaimsPage().selectClaimcategory(claimcatory,solicterneed,Isclaimsenstive);
	}


	@And("I answer DPC question")
	public void DPCQuestion() throws Exception {
		// Thread.sleep(4000);
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().enterCyberGstQuestion();
	}

	@And("I enter employee insured or not")
	public void insuredQuestion() throws Exception {
		// Thread.sleep(4000);
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().enterInsuredEmployee();
	}

	@And("I provide general details for travel insurance")
	public void generalClaimTravelInsurance() throws Exception {
		// Thread.sleep(4000);
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().travelInsuranceGeneralDetails();
	}

	@And("I provide medical details for travel insurance")
	public void medicalTravelInsurance() throws Exception {
		// Thread.sleep(4000);
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().travelInsuranceMedicalDetails();
	}

	@And("I provide general details for DO insurance")
	public void generalDetailsDO() throws Exception {
		// Thread.sleep(4000);
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().DOGeneralDetails();
	}

	@And("I provide general details for unlawful termination")
	public void unlawfulTermination() throws Exception {
		// Thread.sleep(4000);
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().DOUnlawfulTermination();
	}

	@And("I provide loss details for property cyber {string}")
	public void incidentLossDetailsProperty(String howdamage) throws Exception {
		// Thread.sleep(4000);
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().provideLossDetailsPropertyCyber(howdamage);
	}

	@And("I provide loss details for cyber claims {string} {string}")
	public void incidentLossDetailsCyber(String incident, String action) throws Exception {
		// Thread.sleep(4000);

		context.getClaimsPage().cyberIncidentScreen(incident, action);
	}

	@And("I extract the claim unit number")
	public void i_extract_claimunit() throws Exception {
		context.getClaimsPage().extractClaimNumber();
	}

	@Then("No eligible policy validation message is displayed")
	public void no_eligible_policy_displayed() throws Exception {
		context.getClaimsPage().verifyErrorMessageSelectLossScreen("No eligible policies found");
	}

	@Then("Download claim form available")
	public void download_form_link_available() throws Exception {
		context.getClaimsPage().downloadClaimLink();
	}

	@Then("future date not allowed message is displayed")
	public void future_date_not_allowed_displayed() throws Exception {
		context.getClaimsPage().verifyErrorMessageFutureDate("Cannot be a future date.");
		SeleniumFunc.waitFor(2);
	}


	@Then("I provide loss details {string} {string} {string}")
	public void i_provide_loss_details(String where, String what, String how) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().fillInDataIntoQuestions(where, what, how);
		SeleniumFunc.waitFor(2);
	}

	@Given("Select the the damage type {string} and {string}")
	public void select_the_the_damage_type_damege_and(String losstype,String isdet) throws Exception {
		context.getClaimsPage().selectTheLossOrDamage(losstype,isdet);
	}

	@Given("Enter the list all items damaged {string} {string} and {string}")
	public void enter_the_list_all_items_damaged_and(String decription, String price, String asset) throws Exception {
		context.getClaimsPage().enterDamagedItems(decription,price,asset);
	}

	@Then("I provide SCIP details {string} {string} {string}")
	public void i_provide_SCIP_details(String description, String purchasecost, String salvagevalue) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().fillInScipDetails(description, purchasecost, salvagevalue);
	}

	@Then("I attach an evidence file {string} {string}")
	public void i_attach_file(String filepath, String category) throws Exception {

		context.getClaimsPage().attachFileInAttachmentsPage(filepath, category);
	}

	@Then("I attach an evidence file for recovery {string} {string}")
	public void i_attach_file2(String filepath, String category) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().attachFileInAttachmentsPage3(filepath, category);
	}
	@Then("I attach one evidence file {string} {string}")
	public void i_attach_one_file(String filepath, String category) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().attachFileInAttachmentsPage1(filepath, category);
	}

	@Then("I provide liability loss details for formal investigation coverage {string} {string} {string}")
	public void i_provide_liability_loss_details_for_formal_investigation_coverage(String InvestigationName, String what, String claimantname) throws Exception {
		context.getClaimsPage().fillInvestigationCoverageDetails(InvestigationName,what,claimantname);
	}

	@Then("I provide liability loss details for Professional liability coverage {string} {string}")
	public void i_provide_liability_loss_details_for_professional_liability_coverage(String Particulars, String ThirdParty) throws Exception {
		context.getClaimsPage().fillInvestigationLiabilityCoverageDetails(Particulars,ThirdParty);
	}

	@Then("I provide liability loss details {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void i_attach_file(String what, String claimantname, String addline1, String addline2, String suburb,
							  String state, String postcode, String nature, String treatment) throws Exception {

		context.getClaimsPage().fillLiabilityLossDetails(what, claimantname, addline1, addline2, suburb, state, postcode, nature,
				treatment);
	}

	@Then("I provide claimant details {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void i_provide_claimant_details(String studentname, String dateofbirth, String addressline1,
										   String addressline2, String suburb, String state, String postcode, String parentname, String relationship,
										   String parentaddressline1, String parentaddressline2, String parentsuburb, String parentstate,
										   String parentpostcode, String telephone, String parentemail) throws Exception {


		context.getClaimsPage().fillInClaimantDetails(studentname, dateofbirth, addressline1, addressline2, suburb, state, postcode,
				parentname, relationship, parentaddressline1, parentaddressline2, parentsuburb, parentstate,
				parentpostcode, telephone, parentemail);
		SeleniumFunc.waitFor(5);
	}

	@Then("I provide accident details {string} {string} {string}")
	public void i_provide_accident_details(String description, String reportedto, String injury) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().fillInDataIntoAccidentDetails(description, reportedto, injury);
	}

	@Then("I approve the claim unit in portmanager")
	public void approve_port() throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().clickApprove();
	}

	@Then("I reject the claim unit in portmanager")
	public void reject_port() throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().clickReject();
	}

	@Then("I submit denial of claim after port manager approval")
	public void deny_claim() throws Exception {

		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().denyClaimAfterPortManagerAppr();
	}

	@Then("I search for the {string} and validate with org id as {string}")
	public void orgSearch(String orgname, String orgid) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().clientSearchForOrganisation(orgname, orgid);
	}

	@Then("I search for the {string} and validate with contact id as {string}")
	public void contactSearch(String contactname, String contactid) throws Exception {

		context.getClaimsPage().clientSearchForContact(contactname, contactid);
	}

	@And("I check the declaration for Claims")
	public void i_check_the_declaration() throws Exception {

		context.getClaimsPage().checkDeclaration();
	}

	@Given("I check the declaration for Adhoc Invoice")
	public void checkTheDeclarationForAdhocinvoice() throws Exception {
		context.getClaimsPage().checkDeclarationForAdhocinvoice();
	}

	@When("I search the case id in {string} and process it")
	public void searchTheCaseIDAndProcess(String queueValue) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click("//img[@title='vmia logo']");
		SeleniumFunc.waitFor(9);
		context.getClaimsPage().beginTheCase(queueValue);
		SeleniumFunc.acceptTheAlert();
	}



	@When("Approve the request and submit")
	public void approveRequestAndSubmit() throws Exception {
		context.getClaimsPage().approveRequest();
	}

	@When("Filter the caseID and click on the record")
	public void filterCaseIDAndClickOnRecord() throws Exception {
		context.getClaimsPage().filterTheRecordAndSubmit();
	}

	@When("I enter Manual processing reason {string} and received date and then click on submit")
	public void enterManualProcessingReasonAndclickOnSubmit(String reason) throws Exception {
		context.getClaimsPage().manualProcessingReasonAndSubmit(reason);
	}

	@Given("Enter the employee details {string} {string} {string} {string}")
	public void enter_the_employee_details(String FirstName,String Lastname, String Position,String empolyeDuties) throws Exception {
		context.getClaimsPage().enterEmployeePersonalDetailDOAClaim(FirstName, Lastname,  Position, empolyeDuties);
	}

	@Given("Enter the employee details {string} {string} {string} {string} {string}")
	public void enter_the_employee_details(String insuredOfficer, String FirstName,String Lastname, String Position,String empolyeDuties) throws Exception {
		context.getClaimsPage().enterEmployeePersonalDetailNewDOAClaim(insuredOfficer, FirstName, Lastname,  Position, empolyeDuties);
	}

	@Given("Enter the salary details {string} {string} {string} {string} {string}")
	public void enter_the_salary_details(String baseSalary,String totalRumenarion,String isEmpHaveContract,String IsEmpCoveredAgreement,String IsEmpHavePersonaFile) throws Exception {
		context.getClaimsPage().enterEmployeeSalaryDetailsDOAClaim(  baseSalary, totalRumenarion, isEmpHaveContract, IsEmpCoveredAgreement, IsEmpHavePersonaFile);
	}

	@Given("Enter Employee terminiation details {string} {string} {string} {string}")
	public void enter_employee_terminiation_details(String isEmployeeTerminated,String isEmpResinged,String joinDate,String AgeAtJoining) throws Exception {
		context.getClaimsPage().enterEmployeeTerminationDetailDoAclaim( isEmployeeTerminated, isEmpResinged, joinDate, AgeAtJoining);
	}

	@Given("Enter the legal detail of terminiation {string} {string} {string} {string} {string} {string} {string} {string} {string}")
	public void enter_the_legal_detail_of_terminiation(String isEmplyeeDisrupts,String isEmpProcedding,String natureOfComplaint,String wasEmployeeAtProbation,String terminationReason,String challangeReason,String salaryAtTerminiation,String isOrgempCount,String isLegalRepresativePresent) throws Exception {
		context.getClaimsPage().enterTheLeagalActionDetaisDOAClaim( isEmplyeeDisrupts, isEmpProcedding, natureOfComplaint, wasEmployeeAtProbation, terminationReason, challangeReason, salaryAtTerminiation, isOrgempCount, isLegalRepresativePresent);
	}

	@Then("I search the claim unit from workbasket")
	public void open_approved_case() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		// claims.openMyWork();
		// claims.searchAndOpenClaimUnit();
		context.getClaimsPage().claimsOfficerTriageClaims();
		// Thread.sleep(10000);
	}

	@Then("I search the claim file and edit the claim {string}")
	public void openClaimFileTriage(String expectedstatus) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().triageClaimsOfficerEditClaimFile(expectedstatus);
		Thread.sleep(2000);
	}

	@Then("I search the claim with orgnization {string} and edit the claim {string}")
	public void i_search_the_claim_with_orgnization_and_edit_the_claim(String org, String expectedstatus) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().closeAllTabs();
		context.getClaimsPage().editClaimFileFromtriageClaimsOfficer(org,expectedstatus);
		Thread.sleep(2000);
	}

	@Then("I search the claim file and cancel the claim from Actions Drop down")
	public void openClaimFileTriage1() throws Exception {
//		TestBase.currentClaimNumber="CF-31774";
		context.getClaimsPage().triageClaimsOfficerCancelClaimFile();
	}

	@Then("I search the claim with orgnization {string} and cancel the claim from Action drop down")
	public void searchClaimWithOrgnizationAndCancelClaimFromActionDropdown(String org) throws Exception {
		context.getClaimsPage().cancelClaimFileFromtriageClaimsOfficer(org);
	}

//	@Then("I search the claim and cancel the claim from Action drop down")
//	public void i_search_the_claim_and_cancel_the_claim_from_action_drop_down() throws Exception {
//		context.getClaimsPage().cancelClaimFileFromtriageClaimsOfficer();
//	}

	@Then("I search the claim file and WithDrawn the claim from Actions Drop down")
	public void i_search_the_claim_file_and_WithDrawn_the_claim_from_Actions_Drop_down() throws Exception {
		context.getClaimsPage().triageClaimsOfficerWithDrawnClaimFile();
	}

	@Then("verify the Claims Files Status {string} after Cancel from Actions drop down")
	public void verify_the_Claims_Files_Status_after_Cancel_from_Actions_drop_down(String pentingStatus) throws Exception {
		context.getClaimsPage().verifytheCancelStatusfromTriageofficer(pentingStatus);

	}


	@Then("verify the Claims Files Status {string} after Select Withdraw from Actions drop down")
	public void verify_the_Claims_Files_Status_after_Select_Withdraw_from_Actions_drop_down(String pentingStatus) throws Exception {
		context.getClaimsPage().verifytheCancelStatusfromTriageofficer(pentingStatus);
	}

	@Then("I transfer the claim from portfolio manager to user {string} {string} {string}")
	public void portfolioManagerTransfer(String usertotransfer, String expectedstatus, String statusaftertransfer)
			throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().claimsPortfolioManagerTriageClaims(usertotransfer, expectedstatus, statusaftertransfer);

	}

	@Then("Now I transfer the claim from portfolio manager to user {string} {string} {string}")
	public void portfolioManagerTransfer_1(String usertotransfer, String expectedstatus, String statusaftertransfer)
			throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().claimsPortfolioManagerTriageClaims_1(usertotransfer, expectedstatus, statusaftertransfer);
		context.getClaimsPage().OpenClaimFromQueue("Unassigned Claims");
		Assert.assertEquals(expectedstatus,context.getClaimsPage().getClaimStausOfMIclaim());
		context.getClaimsPage().transferClaim(usertotransfer);
		// Thread.sleep(10000);
	}

	@Then("I transfer the claim from portfolio manager and check skill based transfer {string} {string} {string} {string}")
	public void portfolioManagerTransferSkillBased(String invaliduser, String usertotransfer, String expectedstatus,
												   String statusaftertransfer) throws Exception {
//		TestBase.currentClaimNumber="CF-30388";
		try {
			SeleniumFunc.switchToDefaultContent();
			context.getClaimsPage().closeAllTabs();
		} catch (Exception e) {
		}
		// claims.openMyWork();
		// claims.searchAndOpenClaimUnit();
		context.getClaimsPage().claimsPortfolioManagerTriageClaimsSkillBasedTest(invaliduser, usertotransfer, expectedstatus,
				statusaftertransfer);
		// Thread.sleep(10000);
	}

	@Then("I transfer claim from portfolio manager and check still based transfer {string} {string} {string} {string}")
	public void i_transfer_claim_from_portfolio_manager_and_check_still_based_transfer(String invaliduser, String usertotransfer, String expectedstatus,
																					   String statusaftertransfer) throws Exception {
		try {
			SeleniumFunc.switchToDefaultContent();
			context.getClaimsPage().closeAllTabs();
		} catch (Exception e) {
		}
		context.getClaimsPage().claimsPortfolioManagerTriageClaimsSkillsBased(invaliduser, usertotransfer, expectedstatus,
				statusaftertransfer);
	}

	@Then("I search the claim file and proceed for duplicate check {string} {string}")
	public void openClaimFileTriageDupCheck(String expectedstatus, String finalstatus) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		//context.getClaimsPage().closeAllTabs();
//		TestBase.currentClaimNumber="CF-31362";
		context.getClaimsPage().triageClaimsOfficerDupCheck_1(expectedstatus, finalstatus);
		// Thread.sleep(10000);

	}

	@Then("Now I search the claim file and proceed for duplicate check {string} {string}")
	public void openClaimFileTriageDupCheck_1(String expectedstatus, String finalstatus) throws Exception {
		context.getClaimsPage().closeAllTabs();

		context.getClaimsPage().openMyWork();
//		TestBase.currentClaimNumber="CF-44146";
		context.getClaimsPage().searchAndOpenClaimUnit();
		context.getClaimsPage().triageClaimsOfficerDupCheck_1(expectedstatus, finalstatus);
		// Thread.sleep(10000);

	}

	@Then("I search claim file and proceed for duplicate check {string} {string}")
	public void i_search_claim_file_and_proceed_for_duplicate_check(String expectedstatus, String orgname) throws Exception {
		context.getClaimsPage().triageClaimsOfficerDuplicateCheck_1(expectedstatus, orgname);
	}

	@Then("click on first record as we already filtered and proceed for duplicate check {string}")
	public void clickFirstRecordAndProceedForDuplicateCheck(String expectedstatus) throws Exception {
		context.getClaimsPage().clickOnFirstRecord(expectedstatus);
	}

	@Then("validate the error message {string}")
	public void validateErrorMessage(String errorMsg) throws Exception {
		context.getClaimsPage().validateErrorMessage(errorMsg);
	}

	@Then("click on Edit claim from Actions dropdown")
	public void clickEditClaimFromActionsDropdown() throws Exception {
		context.getClaimsPage().clickEditFromActions();
	}

	@Then("provide the Reason {string} for editing claim data and click on Submit button")
	public void provideReasonForEditingClaimDataAndClickSubmitButton(String reason) throws Exception {
		context.getClaimsPage().provideReason(reason);
	}

	@Then("I search the estimate approval file in workbasket and approve the estimate {string} {string}")
	public void searchEstimateAndApprove(String expectedstatus, String finalstatus) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		// context.getClaimsPage().closeAllTabs();
		SeleniumFunc.switchToDefaultContent();
		// context.getClaimsPage().openMyWork();
		// context.getClaimsPage().searchAndOpenClaimUnit();
		context.getClaimsPage().estimateWorkbasketApprove(expectedstatus, finalstatus);
		// Thread.sleep(10000);
	}

	@Then("I check the reports in hamburger {string}")
	public void hamburgerReports(String claimstatusfromfeature) throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().reportInHamburger(claimstatusfromfeature);
	}

	@Then("I check the reports for status {string}")
	public void hamburgerReports_1(String claimstatusfromfeature) throws Exception {
		// claims = new Claims(TestBase.getDriver());
		// context.getClaimsPage().sendAppObject(claims);
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().reportInHamburger1(claimstatusfromfeature);
		SeleniumFunc.switchToDefaultContent();
		// context.getClaimsPage().reportInHamburger2();
		context.getClaimsPage().reportInHamburger2(claimstatusfromfeature);

	}

	@Then("I extract claim file number")
	public void i_extract_claim_file_number() throws Exception  {
		context.getClaimsPage().saveClaimFileNumber();
	}

	@Then("I click its not a duplicate")
	public void stepNotADuplicate() throws Exception {
		context.getClaimsPage().clickNotADuplicate();
	}

	@Then("Now I click its not a duplicate")
	public void stepNotADuplicate_1() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().clickNotADuplicate_1();
	}

	@Then("I resolve claim as duplicate")
	public void stepResolveAsDuplicate() throws Exception {
		context.getClaimsPage().clickResolveAsDuplicate();
	}

	@Then("Now I resolve claim as duplicate")
	public void stepResolveAsDuplicate_1() throws Exception {
		context.getClaimsPage().clickResolveAsDuplicate_1();
	}

	@Then("I want to make a claim from claims officer")
	public void claimsOfficerClaimRaise() throws Exception {
		context.getClaimsPage().clickNewClaimFromClaimsOfficer();

	}

	@When("I am logged in to the claims admin portal")
	public void i_am_logged_in_to_the_claims_admin_portal() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().closeAllInteractions();
	}

	@When("I select the Bulk transfer to transfer the claims")
	public void i_select_the_bulk_transfer_to_transfer_the_claims() throws Exception {
		context.getClaimsPage().clickOnBulkTransfer();
		context.getClaimsPage().selectAllClaims();
	}

	@Then("Transfer the claims both the ways {string} {string}")
	public void transfer_the_claims_both_the_ways(String admin, String user) throws Exception {
		context.getClaimsPage().transferTheClaim(user);
		context.getClaimsPage().closeTransferClaimWindow(user);
		context.getClaimsPage().selectClaimsOfficerAndFilter(user);
//		context.getClaimsPage().selectAllClaims();
//		context.getClaimsPage().transferTheClaim(admin);
	}

	@Then("Verify and Validate the Claim file record after transfer")
	public void verify_the_claim_file_record_after_transfer() throws Exception {
		context.getClaimsPage().enterClaimFileIDAndClickApply();
		context.getClaimsPage().validateTheClaimFileRecord();
	}

	@When("Click on Continue button for {string} from claims pages")

	public void ClickeditClaimsContinue(String contNum) throws Exception {

		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().clickContinueButtonTillEnabled(contNum);

	}

	@Then("I enter client details {string}")
	public void claimsOfficerClientDetails(String claims_zipcode) throws Exception {
		context.getClaimsPage().claimEnterBusiness(claims_zipcode);

	}

	@And("I provide an attachment {string}")
	public void claimsOfficerattachment(String filepath) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().attachFileInAttachmentsPageProofOfClaim(filepath);

	}

	@Then("verify {string}")
	public void verifyPendingClaimunitComp(String claimstatusfromfeature) throws Exception {
		context.getClaimsPage().validateClaimunitCompletion(claimstatusfromfeature);
//		Thread.sleep(2000);
	}

	@Then("Now verify {string}")
	public void verifyPendingClaimunitComp_1(String claimstatusfromfeature) throws Exception {
		context.getClaimsPage().validateClaimunitCompletion_1(claimstatusfromfeature);
	}

	@Then("I verify claimunit status {string}")
	public void verifyClaimunitCompStatus(String claimstatusfromfeature) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().verifyStatusClaimsOfficer(claimstatusfromfeature);
	}

	@Then("I verify status in portfolio manager {string}")
	public void verifyPortManStatus(String claimstatusfromfeature) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().verifyStatusPortMan(claimstatusfromfeature);
	}

	@Then("I resolve claim filee")
	public void resolveClaimFilee() throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().resolveClaimFilee();
	}

	@Then("I resolve claim file")
	public void resolveClaimFile() throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().resolveClaimFile();
	}

	@Then("I resolve claim filed")
	public void resolveClaimFiled() throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().resolveClaimFiled();
	}

	@Then("I resolve claim filede")
	public void resolveClaimFilede() throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().resolveClaimFilede();
	}

	@Then("verify duplicate status {string}")
	public void verifyDuplicateStatus(String claimstatusfromfeature) throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().validateResolvedDuplicate(claimstatusfromfeature);
	}

	@Then("Now verify duplicate status {string}")
	public void verifyDuplicateStatus_1(String claimstatusfromfeature) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().validateResolvedDuplicate_1(claimstatusfromfeature);
	}

	@Then("I open claim unit from worklist")
	public void openUnitFromWorklist() throws Exception {
		context.getClaimsPage().closeAllTabs();
		context.getClaimsPage().openMyWork();
//		TestBase.currentClaimNumber="CF-36633";
//		context.getClaimsPage().searchAndOpenClaimUnit();
		context.getClaimsPage().searchAndOpenTheClaimUnit();
	}

	@Then("I open the claim unit from Claims portal")
	public void i_open_the_claim_unit_from_claims_portal() throws Exception {
		context.getClaimsPage().closeAllTabs();
		context.getClaimsPage().openClaimFromSearch();

	}

	@Then("Validate the policy number in CF {string} and CU {string}")
	public void validate_the_policy_number_in_cf_and_cu(String policynumber, String cfpolicynum) throws Exception {
		context.getClaimsPage().validatePolicyNumberinCFandCUFiles(policynumber,cfpolicynum);
	}


	@Then("Select the indeminity decision {string}")
	public void indeminityDecisionOnClaim(String des) throws Exception {
		context.getClaimsPage().SelectClaimIndeminityDecision(des);
	}

	@Then("Select the mandatory Classification dropdowns")
	public void select_the_mandatory_classification_dropdowns() throws Exception {
		context.getClaimsPage().SelectCauseAndOutcome();
	}

	@Then("I open claim from my review tab")
	public void openClaimfromMyreview() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().closeAllTabs();
		context.getClaimsPage().openMyReviewCases();
		context.getClaimsPage().searchAndOpenClaimfromMyreviewTab();
	}

	@Then("Validate the SLA time {string} in My review section")
	public void validateTheSLAOfClaim(String sla) throws Exception {
		context.getClaimsPage().validateClaimSLA(sla);
	}


	@Then("Complete the claim Review")
	public void CompleteClaimReview() throws Exception {
		context.getClaimsPage().CompleteClaimReview();
	}


	@Then("User open required claims unit \"<claimsFile>\" from Workbasket")
	public void SelectClaimsFilefromWorkBasket(String claimsUnits ) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().closeAllTabs();
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().openMyWork();
		context.getClaimsPage().OpenClaimUnitRequired(claimsUnits);
	}

	@Then("I prepare estimate {string} {string} {string} and review the claim unit as {string} from pending investigation screen {string}")
	public void addEstimateAndReviewClaimUnit(String payableamount, String outstandingamount, String recoverableamount,
											  String clientdecision, String awaitreason) throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().addEstimateAndReviewClaimUnit(payableamount, outstandingamount, recoverableamount, clientdecision,
				awaitreason);


	}
	@Then("as claims officer select deny option {string}")

	public void selectoptions(String denyOption) throws Exception {
		context.getClaimsPage().makeDecisionClaimUnitdeny(denyOption);
	}
	@Then("I prepare estimate {string} {string} {string} more than allowed")
	public void addEstimateMoreThanAllowed(String payableamount, String outstandingamount, String recoverableamount)
			throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().addEstimateHigherLimit(payableamount, outstandingamount, recoverableamount);

	}

	@Then("I prepare estimate {string} {string} {string}")
	public void addEstimate(String payableamount, String outstandingamount, String recoverableamount) throws Exception {
		context.getClaimsPage().addEstimatee(payableamount, outstandingamount, recoverableamount);

	}

	@Then("create payment with {string} {string} {string} {string} and mark it as final")
	public void create_final_Payment(String Paymentmode, String paymentcode, String Paymentval,String setlementDate) throws Exception {
		context.getClaimsPage().makeFinalPayment(Paymentmode, paymentcode, Paymentval,setlementDate);
	}

	@Then("Verify the view payment button")
	public void verifyViewPaymentButton() throws Exception {
		context.getClaimsPage().verifyViewPaymentButton();
	}

	@Then("create payment with {string} {string} {string} {string} with out final payment")
	public void create_payment_with_with_out_final_payment(String Paymentmode, String paymentcode, String Paymentval,String setlementDate) throws Exception {
		context.getClaimsPage().makeFinalPaymentWithoutFinalcheckpoint(Paymentmode, paymentcode, Paymentval,setlementDate);
	}

	@Then("create payment with {string} {string} {string} {string} and upload a file from attach from claim file button")
	public void createPaymentAnduploadFileFromAttachFromClaimFileButton(String Paymentmode, String paymentcode, String Paymentval, String setlementDate) throws Exception {
		context.getClaimsPage().makeFinalPaymentAndUploadFileFromAttachFromClaimFileBtn(Paymentmode, paymentcode, Paymentval,setlementDate);
	}

	@When("Open case from manualpayments and approve the same")
	public void open_and_approve_the_manualPayments() throws Exception {
		context.getClaimsPage().approvemanualPayments();
	}

	@Then("create payment with {string} {string} {string}")
	public void create_payment_with(String Paymentmode, String paymentcode, String Paymentval) throws Exception {
		context.getClaimsPage().makePayment(Paymentmode, paymentcode, Paymentval);
	}

	@Then("create Recovery estimate  {string} {string}")
	public void create_Recovery_estimate(String RecoveryType, String recoveryestimate) throws Exception {
		context.getClaimsPage().createrecoveryEstimate(RecoveryType, recoveryestimate);
	}

	@Then("Add create recovery {string} {string} {string} {string} {string}")
	public void add_create_recovery(String estimiate, String PartyType, String firstname, String lastname, String recoveryamt) throws Exception {
		context.getClaimsPage().createrecovery(estimiate, PartyType, firstname, lastname, recoveryamt);
	}

	@Then("Extract the recovery id")
	public void extract_the_recovery_id() throws Exception {
		context.getClaimsPage().extractRecoveryID();
	}

	@When("Open and approve the recovery {string} {string} {string}")
	public void open_and_approve_the_recovery(String reason, String recoverrecived,String recoverydate ) throws Exception {
		context.getClaimsPage().approveTheRecovery(reason, recoverrecived,recoverydate);
	}
	@Then("I verify recovery status {string}")
	public void verifyRecoveryStatus(String status) throws Exception {
		context.getClaimsPage().verifyRecoveryStatus(status);
	}

	@Then("Verify the recovery history {string} {string}")
	public void verify_the_recovery_history(String expecrecovery,String exprecoverdes) {

		context.getClaimsPage().validateRecoveryHistory(expecrecovery, exprecoverdes);
	}


	@Then("I prepare estimatee {string} {string} {string}")
	public void addEstimatee(String payableamount, String outstandingamount, String recoverableamount)
			throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().addEstimatee(payableamount, outstandingamount, recoverableamount);

	}

	@Then("I prepare recoverable for third party {string} {string}")
	public void addRecoverableThirdParty(String recoveryamount, String recoverabletype) throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().addRecoverableThirdParty(recoveryamount, recoverabletype);

	}

	@Then("I prepare recoverable for new third party {string} {string}")
	public void addRecoverableThirdPartyNew(String recoveryamount, String recoverabletype) throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().addRecoverableThirdPartyNew(recoveryamount, recoverabletype);

	}

	@When("I add a new Adhoc Invoice {string}")
	public void addNewAdhocInvoice(String orgName) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().createAdhocInvoice(orgName);
	}

	@When("Proceed by selecting Organization and contact")
	public void proceedBySelectingOrganizationAndContact() throws Exception {
		context.getClaimsPage().selectOrgAndContact();
	}

	@Given("Add details {string} {string} {string} {string} and {string}")
	public void addDetails(String policyNumber, String Product, String Premium, String GST, String Stamp) throws Exception {
		context.getClaimsPage().addDetailsByClickingOnAddNewLineLink(policyNumber, Product, Premium,GST,Stamp);
	}

	@Then("I add a new event category {string}")
	public void addNewEventCategory(String eventname) throws Exception {

		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().createEventCategory(eventname);

	}
	@Then("User select final payment option")
	public void user_select_final_payment_option() throws Exception {
		context.getClaimsPage().SelectfinalPayment();
	}

	@Then("I prepare recoverable for reinsurance {string} {string}")
	public void addRecoverableReinsurance(String recoveryamount, String recoverabletype) throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().addRecoverableReinsurance(recoveryamount, recoverabletype);

	}

	@Then("I add single payment option")
	public void addPaymentSingle() throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().addPaymentSingle();

	}

	@Then("I associate event with claim {string}")
	public void associateClaim(String eventname) throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().associteClaimWithEvent(eventname);

	}

	@Then("I make payment and add new payee {string} {string} {string} {string} {string}")
	public void add_payment_single(String payeetypedropdown, String payeename, String bsb, String acntnumber,
								   String mail) throws Exception {
		SeleniumFunc.switchToDefaultContent();

		context.getClaimsPage().addPayeeNewDuringMakeSinglePayment(payeetypedropdown, payeename, bsb, acntnumber, mail);

	}

	@Then("I add a legal panel")
	public void addLegalPanel() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().addLegalPanel();
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().closeAllTabs();
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().openMyWork();
		context.getClaimsPage().searchAndOpenClaimUnit();
	}


	@Then("I verify nmber of litigations")
	public void verifyLitigations() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().verifyNumberOfLitigations();

	}

	@Then("I process claim unit case {string} {string}")
	public void processclaimUnitCase(String clientdecision, String awaitreason) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().makeDecisionClaimUnit(clientdecision,awaitreason);

	}

	@Then("I attach an evidence file without dropdown {string}")
	public void i_attach_1_file(String filepath) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().attachFileInAttachmentsPageWithoutDropdown(filepath);
	}

	@Then("I Click on claim log")
	public void clickClaimLog() throws Exception {

		// claims.switchToDefaultContent();
		context.getClaimsPage().clickClaimLog();
	}

	@Then("I add incident details for liability claims {string} {string} {string} {string}")
	public void incident_Liab(String incident, String nameofclaimant, String addresslookupinput, String address)
			throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().enterIncidentDetailsLiability(incident, nameofclaimant, addresslookupinput, address);

	}

	@Then("I withdraw application due to reason {string}")
	public void with_app_client(String withdrawreason) throws Exception {

		// claims.switchToDefaultContent();
		context.getClaimsPage().withdrawCase(withdrawreason);
	}

	@Then("I cancel claim from org details and verify status {string}")
	public void cancel_claim_org(String claimstatus) throws Exception {

		context.getClaimsPage().cancelClaimOrgAndVerifyStatus(claimstatus);
	}

	@Then("I select Profile for portfoliomanager")
	public void Select_profile_portfolio() throws Exception {
		// claims.switchToDefaultContent();
		context.getClaimsPage().SelectProfile();
	}

	@Then("I select reportee{string}")
	public void Select_profile_portfolio_new_1(String reportee) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().Selectreportee(reportee);

	}

	@Then("Now I deselect the same reportee")
	public void Now_I_deSelect_the_same_reportee() throws Exception
	{

		context.getClaimsPage().Deselectreportee();

	}

	@Then("I am logged in to Traige officer portal")
	public void Select_Triage() throws Exception {
		context.getClaimsPage().closeAllTabs();
	}

	@Then("I click on make a claim From Traige officer")
	public void Select_Triage1() throws Exception {
		context.getClaimsPage().Selectclaim();

	}

	@Then("I provide loss details in Triage  {string} {string} {string}")
	public void i_provide_loss_details1(String where, String what, String how) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().fillInDataIntoQuestions1(where, what, how);
		SeleniumFunc.waitFor(2);
	}

	@Then("I provide SCIP details for traige{string}{string}{string}")
	public void i_provide_SCIP_details1(String description, String purchasecost, String salvagevalue) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().fillInScipDetails1(description, purchasecost, salvagevalue);
	}

	@Then("I attach proof evidence file {string} {string}")
	public void i_attach_file1(String filepath, String category) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().attachFileInAttachmentsPage2(filepath, category);
	}

	@When("Verify the landed to Manage components page {string}")
	public void verify_the_landed_to_Manage_components_page(String pageName) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().verifytheLandedPage(pageName);
	}

	@Then("Click on Manage Components link after clicking on hamburger menu")
	public void click_on_Manage_Components_link_after_clicking_on_hamburger_menu() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().clickOnManageComponent();

	}

	@Then("Click on Add Payee from Manage Componet page")
	public void click_on_Add_Payee_from_Manage_Componet_page() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().clickAddPayeeButton();
	}

	//@Then("User enter the following required details {string} {string} {string}{string}{string}{string}{string}for Adding a payee")
	@Then("User enter the following required details {string} {string} {string}{string} {string}{string}{string}for Adding a payee")
	public void user_enter_the_required_details_for_Adding_a_payee(String payee, String payeeName, String payeeAccname,
																   String payeebsb, String payeeAccNum, String payeeBankname, String payeeEmail) throws Exception {
		context.getClaimsPage().entertherequireddetailsatAddpayeedetailspage(payee, payeeName, payeeAccname, payeebsb, payeeAccNum,
				payeeBankname, payeeEmail);
	}

	@Then("Click on the submit and Verify the status {string}")
	public void click_on_the_submit_and_Verify_the_status(String status) throws Exception {
		// Write code here that turns the phrase above into concrete actions
		context.getClaimsPage().SubmitandVerifytheStatus(status);
	}

	@Then("Now earch the claim file and proceed for duplicate check {string} {string}")
	public void openClaimFileriskadmin(String expectedstatus, String finalstatus) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().closeAllTabs();
		SeleniumFunc.switchToDefaultContent();
		// context.getClaimsPage().openMyWork();
		// context.getClaimsPage().searchAndOpenClaimUnit();
		context.getClaimsPage().riskAdimclaimsselection(expectedstatus, finalstatus);
		// Thread.sleep(10000);
	}

	@Then("User search the claim file and procede with aprrovals as a risk admin  {string} {string}")
	public void user_search_the_claim_file_and_procede_with_aprrovals_as_a_risk_admin(String expectedstatus,
																					  String finalstatus) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().closeAllTabs();
		context.getClaimsPage().riskAdimclaimsselection(expectedstatus, finalstatus);
	}

	@Then("User search the claim file and procede with reject as a risk admin  {string} {string}")
	public void user_search_the_claim_file_and_procede_with_reject_as_a_risk_admin(String expectedstatus,
																				   String finalstatus) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().closeAllTabs();
		context.getClaimsPage().riskAdimRejectAddedPayee(expectedstatus, finalstatus);
	}

	@Then("User extract the caseID after submit the payee")
	public void user_extract_the_caseID_after_submit_the_payee() throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().fetchCaseID();
	}

	@Then("User search for the payee with and verify the name of the payee")
	public void user_search_for_the_payee_with_and_verify_the_name_of_the_payee() throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().verifytheaddedpayeeatClaimsHandler();
	}

	@Then("User select the payee which added and click Modify option from Action list")
	public void user_select_the_payee_which_added_and_click_Modify_option_from_Action_list() throws Exception {
		context.getClaimsPage().updatethepayeeandsubmit();
	}

	@Then("User update the payeename after click modify from Actions list  {string}")
	public void user_update_the_payeename_after_click_modify_from_Actions_list(String payeeName) throws Exception {
		context.getClaimsPage().enterthePayeeNameforupdate(payeeName);
	}

	@Then("Click on Leagal Payee from Manage Componet page")
	public void click_on_Leagal_Payee_from_Manage_Componet_page() throws Exception {

		context.getClaimsPage().ClickonLegalpanelandAddalegalpanel();
	}

	@Then("User enters the required fileds {string} {string} {string} {string} {string} {string} {string} {string} {string}for AddLegalPanel")
	public void user_enters_the_required_fileds_for_AddLegalPanel(String leagalName, String SelectClaimsType,String Address1, String Address2,String suburb,String postcode,String SolicitoName,String SolicitoEmail,String SolicitoPhone) throws Exception {

		context.getClaimsPage().entertheDetailsatLeagalPanelpage(leagalName,SelectClaimsType,Address1,Address2,suburb,postcode,SolicitoName,SolicitoEmail,SolicitoPhone);

	}

	@Then("Click Submit from Add legal panel and search added leagal panel from the list {string}")
	public void click_Submit_from_Add_legal_panel_and_search_added_leagal_panel_from_the_list(String legalName) throws Exception {
		context.getClaimsPage().clickonSubmitandsearchAddedleagalPanel(legalName);
	}

	@Then("Verify the details of added leagal peanel {string}")
	public void verify_the_details_of_added_leagal_peanel(String ClaimsType) throws Exception {
		// Write code here that turns the phrase above into concrete actions
		context.getClaimsPage().verifyclaimstype(ClaimsType);
	}


	@Then("User selects the update option from Action list for added leagal panel")
	public void user_selects_the_update_option_from_Action_list_for_added_leagal_panel() throws Exception {
		context.getClaimsPage().selectUpdateoptionfromAction();
	}

	@Then("User the updates the leagal panel details {string}")
	public void user_the_updates_the_leagal_panel_details(String updateName) throws Exception {
		context.getClaimsPage().updateLegalPanelDetails(updateName);
	}
	@Then("verify the update Legal name {string}")
	public void verify_the_update_Legal_name(String updateLegalPanelName) throws Exception {
		context.getClaimsPage().verifyUpdatelegalName(updateLegalPanelName);
	}

	@Then("User selects the Remove option from Action list for added leagal panel")
	public void user_selects_the_Remove_option_from_Action_list_for_added_leagal_panel() throws Exception {
		context.getClaimsPage().selectRemoveOptionfromAction();
	}

	@Then("User the Removes the leagal panel details from displayed list")
	public void user_the_Removes_the_leagal_panel_details_from_displayed_list() throws Exception {
		context.getClaimsPage().clickSubmitforremovalofleagalpanel();
	}

	@Then("User adding solicitor to a claim")
	public void user_adding_solicitor_to_a_claim() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().verifyThesolicitortoaclaim();
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().closeAllTabs();
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().openMyWork();
		context.getClaimsPage().searchAndOpenClaimUnit();
	}

	@Then("Verify the error message for after adding same solicitor")
	public void verify_the_error_message_for_after_adding_same_solicitor() throws Exception {
		context.getClaimsPage().verifyThesolicitortoaclaim();
	}

	@Then("verify system generating the next legal panel atuomatically")
	public void verify_system_generating_the_next_legal_panel_atuomatically() throws Exception {
		context.getClaimsPage().verifysystemAssignedLegalPanel();

	}
	@Then("Select Managepayments from Actions")
	public void select_Managepayments_from_Actions() throws Exception {
		context.getClaimsPage().addLegalPanel();
	}

	@Then("Select adjust From Actions of Managepayments")
	public void select_adjust_From_Actions_of_Managepayments() {
		// Write code here that turns the phrase above into concrete actions
		throw new cucumber.api.PendingException();
	}

	@Then("Update the adjustment")
	public void update_the_adjustment() {
		// Write code here that turns the phrase above into concrete actions
		throw new cucumber.api.PendingException();
	}

	@Then("I select the Show subsidiary claims check box")
	public void i_select_the_Show_subsidiary_claims_check_box() throws Exception {
		context.getClaimsPage().select_the_Show_subsidiary_claims_check_box();
		SeleniumFunc.switchToDefaultContent();
	}



	@Then("Verify the Claiming organisation should be subsidiary org {string}")
	public void verify_the_Claiming_organisation_should_be_subsidiary_org(String subOrg) throws Exception {
		context.getClaimsPage().verify_the_Claiming_organisation_should_be_subsidiary_org(subOrg);
	}

	@Then("I manually process recovery from recovery portal")
	public void manually_process_recovery() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().intiateRecoveries();
		Thread.sleep(5000);
	}

	@Then("I provide claimant details for journy {string} {string} {string}{string}{string}{string}{string}{string}{string}{string}{string}")
	public void i_provide_claimant_details_for_journy(String fristName,String lastName,String Addressline1,String Suburb,String State,String Postcode,String Country,String Emailaddress,String contactnumber,String Occupation,String Dateofbirth) throws Exception {
		context.getClaimsPage().claimant_details_for_journy(fristName,lastName,Addressline1,Suburb,State,Postcode,Country,Emailaddress,contactnumber,Occupation,Dateofbirth) ;
	}

	@Then("I Provide Claim for injury or death {string} {string} {string} {string}")
	public void i_Provide_Claim_for_injury_or_death(String injury,String injuryoccur,String Dateinjuryoccured,String testWhenn) throws Exception {
		// Write code here that turns the phrase above into concrete actions
		context.getClaimsPage().Claim_for_injury_or_death(injury,injuryoccur,Dateinjuryoccured,testWhenn);
	}

	@Then("I provide Medical details {string} {string} {string} {string} {string} {string} {string} {string}")
	public void i_provide_Medical_details(String MedicalPractitioner, String Periodfrom, String Periodto, String Doctorsname, String Address, String condition, String Doctorsname1, String Address3) throws Exception {

		context.getClaimsPage().Medical_details(MedicalPractitioner, Periodfrom, Periodto, Doctorsname, Address, condition, Doctorsname1, Address3);

	}

	@Then("I open claim case from worklist")
	public void openCasefromWorklist() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().closeAllTabs();
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().openMyWork();
		context.getClaimsPage().clickonfristClaimcase();

	}


	@Then("User click on the view note and enter the text {string}")
	public void user_click_on_the_view_note_and_enter_the_text(String noteText) throws Exception {
		SeleniumFunc.waitFor(2);
		context.getClaimsPage().clickOnViewnoteandentertextinpost(noteText+"\n"+"Testing VIMIA");
	}

	@Then("User click on attachemt {string} select the required document")
	public void user_click_on_attachemt_select_the_required_document(String filepath) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		//context.getClaimsPage().attachFileInAttachmentsPageProofOfClaim(filepath);
		context.getClaimsPage().uploadattachmentinviewnote(filepath);
	}
	@Then("User click on product attachemt {string} select the required document")
	public void user_click_on_attachemt_select_the_required_document1(String filepath) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().attachFileInAttachmentsPageProofOfClaim(filepath);
		//claims.uploadattachmentinviewnote(filepath);
	}
	@Then("User click postButton to submmit the note")
	public void user_click_postButton_to_submmit_the_note() throws Exception {
		context.getClaimsPage().clickpostButtontosubmmitthenote();

	}



	@Then("User click on Notifaction Bell and verify the {string} from Triageofficer")
	public void user_click_on_Notifaction_Bell_and_verify_the_from_Triageofficer(String notification) throws Exception {

		context.getClaimsPage().clickbellNotificationImage();
		SeleniumFunc.waitFor(2);
		context.getClaimsPage().verifytheNotification(notification);

	}

	@When("User click on Action dropdown and seltect the reopen from the list")
	public void user_click_on_Action_dropdown_and_seltect_the_reopen_from_the_list() throws Exception {

//    context.getClaimsPage().clientSearchForOrganisationclaims();
//    context.getClaimsPage().openMyWork();
//    context.getClaimsPage().searchAndOpenClaimUnit();
		context.getClaimsPage().selectClaimFilefromListofcliamsTableprofileManger();
		context.getClaimsPage().submitReopenclaimfromMywork();
	}



	@When("User enter calimsid {string} for which Resloved completed staus")
	public void user_enter_calimsid_for_which_Resloved_completed_staus(String ClaimsID) throws Exception {

		//claims.clientSearchForOrganisationclaims();
		context.getClaimsPage().openMyWork();
		context.getClaimsPage().searchAndOpenClaimUnit1(ClaimsID);
		//context.getClaimsPage().selectClaimFilefromListofcliamsTableprofileMangerClaimsID(ClaimsID);
	}

	@Then("verify the unernote section for created claim")
	public void Then_verify_the_unernote_section_for_created_claim () throws Exception{

		context.getClaimsPage().VerifyUnderWrttingtext();
	}

	@When("User enter the Filter value{string} and {string} click on the fileter get claimsUnit")
	public void user_enter_the_Filter_value_and_click_on_the_fileter_get_claimsUnit(String SerachClaim, String usertotransfer) throws Exception {

		context.getClaimsPage().closeAllTabs();
		context.getClaimsPage().transfertheClaimsUnittoClaimsHandlers(SerachClaim, usertotransfer);
	}


	@Then("User SelectProfile for Edit and enterUnAviabilityofHandler")
	public void SelectProfileforEditandenterUnAviabilityofHandler() throws Exception{

		context.getClaimsPage().editProfileUnavailability();
	}
	@When("User submit the unUnavailability dates from {string} to {string} dates")
	public void user_submit_the_unUnavailability_dates_from_to_dates(String startDate, String endDate) throws Exception {
		context.getClaimsPage().enterUnavailabilitydateforclaimHandler( startDate, endDate);
	}

	@Then("Navigate to Claim file from Triage cliam Officer")
	public void openClaimFilefromTR_Officer(String expectedstatus,String finalstatus) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().closeAllTabs();
		SeleniumFunc.switchToDefaultContent();
		//context.getClaimsPage().openMyWork();
		//context.getClaimsPage().searchAndOpenClaimUnit();
		context.getClaimsPage().getClaimFile_triageClaimsOfficer();
		//Thread.sleep(10000);
	}
	@Then("Navigate to Claim files from Triage cliam Officer")
	public void openClaimFilefromTR_Officers() throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().closeAllTabs();
		SeleniumFunc.switchToDefaultContent();
		//claims.openMyWork();
		//context.getClaimsPage().searchAndOpenClaimUnit();
		context.getClaimsPage().getClaimFile_triageClaimsOfficer();
		//Thread.sleep(10000);
	}
	@Then("I enter client details")
	public void claimsOfficerClientDetails() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().claimEnterBusiness();

	}
	@Then("verify the Capture loss details fileds should not be editable")
	public void verify_the_Capture_loss_details_fileds_should_not_be_editable() throws Exception {
		// Write code here that turns the phrase above into concrete actions
		context.getClaimsPage().clickoncapturelossdetailsandverifythefileds();
	}
	@Then("Select the {string}")
	public void Add_Action(String action) throws Exception {
		context.getClaimsPage().AddAction(action);

	}
	@Then("Search for claim and link the claim by {string}")
	public void Link_the_claim(String claimant) throws Exception {

		context.getClaimsPage().LinkClaim(claimant);

	}
	@Then("Unlink the claims")
	public void Unlink_Claims() throws Exception {
		context.getClaimsPage().UnLinkClaim();

	}
	@Then("open the {string} and verify the linked claims")
	public void verify_Linked_Claim(String Claiment) throws Exception {
		context.getClaimsPage().VerifyLinkClaims(Claiment);
	}
	//		@Then("review claim and check dupilicate")
//	public void skip_review_claim_and_check_dupilicate() throws Exception {
//	   claims.skip_reviewclaimDuplicate();
//	}
	@Then("Select the indeminity denial action {string}{string}")
	public void indeminity_denial_action(String denialAction,String DenailType) throws Exception {

		context.getClaimsPage().SelectClaimIndeminityDenialDecision(denialAction,DenailType);

	}
	@Then("Verify the Claim Adminstration fields")
	public void VerifyClaimsAdmin() throws Exception {
		context.getClaimsPage().VerifyClaimsAdmin();

	}
//@When("Select Requeried claim unit to transfer from the list")
//public void select_Requeried_claim_unit_to_transfer_from_the_list() {
//  // Write code here that turns the phrase above into concrete actions
//  throw new cucumber.api.PendingException();
//}
//
//@When("Select Transfer claim option from Select actions dropdown")
//public void select_Transfer_claim_option_from_Select_actions_dropdown() {
//  // Write code here that turns the phrase above into concrete actions
//  throw new cucumber.api.PendingException();
//}
@Then("Select the indeminity decision {string}{string}")
public void indeminityDecisionOnClaim(String des,String decisiontype) throws Exception {
	context.getClaimsPage().SelectClaimIndeminityDecision(des,decisiontype);

}
	@Then("User should Add litigation")
	public void User_should_Add_litigation() throws Exception {
//		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().AddLitigation();

	}
	@Given("Enter the Litigatation Status {string} {string} {string} {string}  {string} {string} {string}")
	public void enter_the_Litigatation_Status(String LegalStatus, String Status, String Jurisdiction, String Courtnumber, String DateIssue,String dateservered, String Plaintiff) throws Exception {
		context.getClaimsPage().Litigation_enterTheLitigationDetails(LegalStatus, Status, Jurisdiction, Courtnumber, DateIssue, dateservered, Plaintiff);
	}

	@Given("Enter the CoDefendent details {string} {string} {string}")
	public void enter_the_CoDefendent_details(String Defendent, String Soli, String Insurer) throws Exception {
		context.getClaimsPage().Litigation_SelectDefendentdetails(Defendent, Soli, Insurer);
	}

	@Given("Enter the Event details {string} {string} {string}")
	public void Enter_the_Event_details(String concluded, String actualdate, String notes) throws Exception {
		context.getClaimsPage().Litigation_enterConcludedAndNotesDetails(concluded, actualdate, notes);
	}
	@Then("Verify the Timetable history {string} {string} {string} {string} {string}")
	public void Verify_Timetable_History(String concluded, String eventdate, String actualdate, String eventname, String responsible) throws Exception {
//		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().AssertTimetableHistory(concluded,eventdate, actualdate, eventname, responsible);

	}
	@Given("Enter the Settlement details {string} {string} {string} {string} {string}")
	public void enter_the_Settlement_details_resoultionMony(String Resolution, String ResoultionDate, String comment, String cost,String settlementMonetery) throws Exception {
		context.getClaimsPage().Litigation_EnterTheLitigationSettlement(Resolution, ResoultionDate, comment, cost, settlementMonetery);
	}

	@Given("Select settlement monetary {string} {string} {string} {string}")
	public void select_settlement_monetary(String monetary, String vmia_contribution, String General_damages, String settlementMonetery) throws Exception {
		context.getClaimsPage().Select_Settlement_Monetary(monetary, vmia_contribution, General_damages, settlementMonetery);
	}
	@Then("Add Litigation attachment {string}")
	public void Add_attachment_Litigation(String file) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().addAttachmentToLitigation(file);
		SeleniumFunc.waitFor(5);
	}
	@Given("complete the litigiation for {string}")
	public void complete_the_litigiation_for(String user) throws Exception{
		context.getClaimsPage().CompleteLitigation(user);
	}
	@And("I validate the {string} for litigation")
	public void i_validate_the_for_litigation(String expecStatus) throws Exception {
		Thread.sleep(4000);
		SeleniumFunc.switchToDefaultContent();
		Assert.assertEquals(expecStatus,context.getClaimsPage().getClaimStausOfMIclaim());
		Thread.sleep(2000);
	}
	@Then("I check the {string} for status {string}")
	public void LitigatReport(String report, String claimstatus ) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClaimsPage().OpenReportBrowser(report);
		context.getClaimsPage().openLitigationClaimInReportTable(claimstatus);
	}

	@Then("Verify the Litigation Report browser")
	public void LitigationReportBrowser() throws Exception {
		context.getClaimsPage().LitigationReportBrowser();
	}
	@Given("Enter the Solicitor details {string} {string} {string} {string} {string}")
	public void enter_the_Solicitor_details(String partytype, String VMIASolicitor, String Appoinmentdate, String PlaintiffSolicitr, String ReasonforSolcitor) throws Exception {
		context.getClaimsPage().Litigation_enterTheSolicitordetails(partytype, VMIASolicitor, Appoinmentdate, PlaintiffSolicitr, ReasonforSolcitor);
	}
	@Given("Enter the Timetable details {string} {string} {string}")
	public void enter_the_Timetable_details(String eventName, String eventdate, String resoultioncoments) throws Exception {
		context.getClaimsPage().Litigation_enterTheEventDetails(eventName, eventdate, resoultioncoments);
	}

	@And("Add Inncident Details for EmRePss-Motor {string} {string} {string}")
	public void addInncidentDetailsForEmRePssMotor(String incidentDate, String incidentReason, String Name) throws Exception{
		context.getClaimsPage().AddIncidentDetailsForEmrepssMotor(incidentDate,incidentReason,Name);
	}

	@Then("Select Private Claims as {string}")
	public void select_private_claims_as(String string) throws Exception {
		context.getClaimsPage().selectPrivateClaims(string);
	}


}




