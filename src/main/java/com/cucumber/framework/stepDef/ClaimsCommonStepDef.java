package com.cucumber.framework.stepDef;

import com.cucumber.framework.PageObjects.ClaimReviewHomePage;
import com.cucumber.framework.TestBase.TestBase;
import com.cucumber.framework.context.TestContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class ClaimsCommonStepDef {

    TestContext context;
   public ClaimsCommonStepDef(TestContext cont){
        context=cont;
    }


    ClaimReviewHomePage claimReviewHomePage;
    @Given("Add attachment with details following details {string} {string} {string} {string}")
    public void add_attachment_with_details_following_details(String filename,String attachmentCategory,String author,String attachdocate) throws Exception {
        context.getClaimReviewHomePage().AddAttachmentInTheclaim(filename,attachmentCategory,author,attachdocate);
    }

    @Given("Updated the attachment data  {string} {string} {string} {string}")
    public void updated_the_attachment_data(String filename,String author,String attachmentCategory,String attachdocate) throws Exception {
        context.getClaimReviewHomePage().clickUpdateFileMetadata(filename,author,attachmentCategory,attachdocate);
    }

    @Given("Verify the history of the document {string} {string} {string}")
    public void verify_the_history_of_the_document(String filename,String Category,String Documentdate) throws Exception {
        context.getClaimReviewHomePage().getAttachmentDocHistory(filename,Category,Documentdate);
    }

    @Given("Select {string} tab in search")
    public void select_documents_tab_in_search(String Option) throws Exception {

        context.getClaimReviewHomePage().selectTabinSearch(Option);
    }

    @Given("Enter the documentname {string} and click search")
    public void enter_the_documentname_and_click_search(String document) throws Exception {
        context.getClaimReviewHomePage().enterTheDocumentNameAndClickSearch(document);
    }

    @Given("click edit from the document search result and update the author")
    public void click_edit_from_the_document_search_result_and_update_the_following_details() throws Exception {
        context.getClaimReviewHomePage().editTheDocumentFromSearchResult();
    }

    @Given("Verify the history is added for the update")
    public void verifyHistoryDocUpdate() throws Exception {
        context.getClaimReviewHomePage().VerifyTheDocumentHistoryInSearchResult();

    }


}
