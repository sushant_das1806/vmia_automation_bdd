package com.cucumber.framework.stepDef;

import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
//import cucumber.api.java.en.*;
import com.cucumber.framework.PageObjects.HomePageLoc;
import com.cucumber.framework.TestBase.TestBase;
import com.cucumber.framework.configreader.ObjectRepo;
import com.cucumber.framework.context.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.*;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ClientAdviserStepDefinition {

	TestContext context;
	public ClientAdviserStepDefinition(TestContext cont){
		context=cont;
	}

	@Then("I am logged in to the client adviser portal")
	public void i_am_logged_in_to_the_advisor_portal() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().closeAllInteractions();
//		TestBase.currentCaseID="GEN-316";
	}

	@Then("I validate warning message {string} when I select only {int} field")
	public void i_validate_warning_message_when_i_select_only_field(String warMsg, Integer index) throws Exception {
		context.getClientAdviserPage().goToSearchAndSelectHistoricpolicy();
		context.getClientAdviserPage().validateWarningMessageBySelectingOnlyOneField(warMsg,index);
	}

	@When("I select  min {int} fields {string} from historical policy tab")
	public void i_select_min_fields_from_historical_policy_tab(Integer index, String year) throws Exception {
		context.getClientAdviserPage().selectMinTwoFields(index,year);
	}

	@Then("I verify the search function")
	public void i_verify_the_search_function() throws Exception {
		context.getClientAdviserPage().verifySearchFunction();
	}

	@Then("I am logged in to the claims officer portal")
	public void i_am_logged_in_to_the_claims_officer_portal() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().closeAllInteractions();
	}

	@When("Select manage components and go to VMIA solicitor")
	public void selectManageComponentsAndGoToVMIASolicitor() throws Exception {
		context.getClientAdviserPage().selectManageComponentsAndGoToSolicitor();
	}

//	@When("Add VMIA solicitor details")
//	public void add_vmia_solicitor_details() throws Exception {
//		context.getClientAdviserPage().addSolicitorDetails();
//		context.getClientAdviserPage().solicitorsList();
//	}

	@When("Add VMIA solicitor details {string} {string} {string} {string} and {string}")
	public void add_vmia_solicitor_details_and(String Name, String Email, String PhoneNumber, String Personnel,String Address) throws Exception {
		context.getClientAdviserPage().addSolicitorDetails(Address);
		context.getClientAdviserPage().solicitorsList(Name,Email,PhoneNumber,Personnel);
	}

//	@When("Select the Solicitor record and change the status to {string}")
//	public void select_the_solicitor_record_and_change_the_status_to(String string) {
////		context.getClientAdviserPage().updateTheStatus();
//	}

	@When("Select the Solicitor record and change the status {string} and Empanelment {string}")
	public void select_the_solicitor_record_and_change_the_status_and_on_pancel(String status, String option) throws Exception {
		context.getClientAdviserPage().updateTheStatus(status,option);
	}

	@Then("Verify and validate the same record in Inactive tab with status {string}")
	public void verifyAndValidateRecordInInactiveTab(String status) throws Exception {
		context.getClientAdviserPage().verifyAndValidateRecordInInactiveTab(status);
	}

	@Then("I Click new button to edit endorsement text")
	public void clickNewButtonToEditEndorsementText() throws Exception {
		context.getClientAdviserPage().clickNewButtonForEndorsemanrText();
	}

	@When("I Click new button to deactivate endorsement")
	public void i_click_new_button_to_deactivate_endorsement() throws Exception {
		context.getClientAdviserPage().clickNewButtonForDeactivateEndorsement();
	}

	@When("I click new button to bulk add endorsement text")
	public void i_click_new_button_to_bulk_add_endorsement_text() throws Exception {
		context.getClientAdviserPage().clickNewButtonForBulkAddEndorsementText();
	}

	@When("select the details {string} {string} {string} proceed to add endorsement text")
	public void select_the_details_proceed_to_add_endorsement_text(String startDate, String productID, String clientCategory) throws Exception {
		context.getClientAdviserPage().selectAllTheDetails(startDate,productID,clientCategory);
		context.getClientAdviserPage().clickSearchAndContinue();
	}

	@When("add endorsement text {string} and comments {string}")
	public void add_endorsement_text_and_continue(String endtext, String comments) throws Exception {
		context.getClientAdviserPage().addendorsementTextAndContinue(endtext);
		context.getClientAdviserPage().addCommentsAndClickFinish(comments);
	}

	@Then("I select the policy and then endorsement")
	public void selectThePolicyThenEndorsement() throws Exception {
		context.getClientAdviserPage().selectPolicyAndEndorsement();
	}

	@Then("Edit the Endorsement text {string} and click continue")
	public void editTheEndorsementTextAndClickContinue(String updatedText) throws Exception {
		context.getClientAdviserPage().editEndorsementtext(updatedText);
	}

	@Then("Select the reviewer {string} and provide comments {string} and then Click Finish button")
	public void selectReviewerAndProvideCommentsAndClickFinishButton(String reviewer, String comments) throws Exception {
		context.getClientAdviserPage().selectReviewer(reviewer,comments);
	}

	@Then("I capture the endorse ID")
	public void captureTheEndorseId() throws Exception {
		context.getClientAdviserPage().fetchEndorseID();
	}

	@When("I search the endorsement ID and select the record")
	public void searchEndorsementIDAndSelectTheRecord() throws Exception {
		context.getClientAdviserPage().searchEndorsementID();
	}

	@Given("Endorsement text request has been {string}")
	public void endorsement_text_request_has_been_rejected(String status) throws Exception {
		context.getClientAdviserPage().requestRegejected(status);
	}

	@Given("Extract the data from the sheet {string}")
	public void extract_the_data_from_the_sheet(String Sheet) throws Exception {
		context.getClientAdviserPage().createOrgFromDataSheet("randomOrg");
	}

	@Then("Click New and Select {string}")
	public void clickeNewAndSelectOptions(String options) throws Exception {
		context.getClientAdviserPage().clickNewAndSelectOptions(options);
	}

	@When("I Click new button to cancel {string} policy")
	public void clickNewButtonToCancelPolicy(String type) throws Exception {
		context.getClientAdviserPage().clickNewButtonForCancelPolicy(type);
	}

	@When("select the policy and click finish")
	public void selectPolicyAndClickFinish() throws Exception {
		context.getClientAdviserPage().selectPolicy();
	}

	@When("select the endorsementID with deactivate reason {string} and click continue")
	public void select_the_endorsement_id_and_click_continue(String dereason) throws Exception {
		context.getClientAdviserPage().selectEndorsementID();
		context.getClientAdviserPage().deactivatingReason(dereason);
	}

	@Given("Enter cancel policy details {string}")
	public void enter_cancel_policy_details(String reason) throws Exception {
		context.getClientAdviserPage().cancelPolicyDetails(reason);
	}

	@Then("I accept the quote and click finish")
	public void acceptQuoteAndClickFinish() throws Exception {
		context.getClientAdviserPage().acceptQuote();
	}

	@And("I open the case for Review")
	public void SearchPolicyReviewCase() throws Exception {
		context.getUnderWritingPage().SearchAndReviewTheCaseInRiskAdivisor();
		context.getUnderWritingPage().scearchCaseNumberInRiskAdvisor();
		context.getUnderWritingPage().clickSearchResultOfRiskAdvisorQueue();

	}

	@Given("I filter the case from Risk advisor queue")
	public void i_filter_the_case_from_risk_advisor_queue() throws Exception {
		context.getUnderWritingPage().SearchAndReviewTheCaseInRiskAdivisor();
		context.getUnderWritingPage().scearchCaseNumberInRiskAdvisor();

	}

	@Given("search the policy in following work and begin the work")
	public void i_filter_the_case_from_followingwork_queue() throws Exception {
		context.getUnderWritingPage().openCaseFromFollowingWorkQueue();
	}

	@Given("Check the due date {string} and Open the case")
	public void check_the_due_date_and_open_the_case(String duedate) throws Exception {
		context.getUnderWritingPage().verifyDueDateOfCaseInRiskAdvisorQueue(duedate);
		context.getUnderWritingPage().clickSearchResultOfRiskAdvisorQueue();
	}


	@Given("Update the following orgnization details {string} {string} {string} {string} {string}")
	public void update_the_orgnization_details(String governchnage, String merged, String effdate, String comments,String mergeOrgDetails) throws Exception {
		context.getClientAdviserPage().updateOrg(governchnage,merged,effdate,comments,mergeOrgDetails);
	}

	@Given("Add Unincorprate org {string} in the orgnization")
	public void add_Unincorprate_org_in_the_orgnization(String orgname) throws Exception {
		context.getClientAdviserPage().AddUnincorpraorgInOrg(orgname);
	}


	@Then("Click New and Select {string} from Cancel Policy option")
	public void clickeCancelPolicyAndSelectOptions(String options) throws Exception {
		context.getClientAdviserPage().clickCancelPolicyAndSelectOptions(options);
	}

	@Then("search the organization using clientSegment {string} and Orgnazation")
	public void searchOrgOptionsCancel(String options) throws Exception {

		context.getClientAdviserPage().searchOrgForBundleCancallation(options);
	}

	@Then("Select orgnazation by searching clientsegment {string} bundle type {string} and orgname")
	public void searchOrgforCreateBundle(String options, String bundleType) throws Exception {

		context.getClientAdviserPage().searchOrgByClientSegmentBundleTypeAndOrgName(options,bundleType);
	}

	@Then("validate the product list {string}")
	public void validate_the_product_list_products(String productType) throws Exception {
		context.getClientAdviserPage().validateProductList(productType);
	}

	@Then("Select the contact and Product {string} click finish")
	public void SelectOrgInNeWBundlePolicy(String clientSegment) throws Exception {

		context.getClientAdviserPage().selectContactAndProduct(clientSegment);
	}

	@Then("Add the unincorproted orgnization {string} and policyEffectiveDate {string} and finish the policy")
	public void addUnIncorprotePolicyAndFinishThePolicy(String UnincorpOrgName,String policyEffectiveDate) throws Exception {
		context.getClientAdviserPage().AddUnicorpOrgAndSubmitPolicy(UnincorpOrgName,policyEffectiveDate);
	}

	@Given("Select orgnazation by searching clientsegment {string} and {string} and orgname")
	public void select_orgnazation_by_searching_clientsegment_and_and_orgname(String clientSegment, String BundleType) throws Exception {
		context.getClientAdviserPage().searchOrgByClientSegmentAndOrgName(clientSegment,BundleType);

	}


	@Then("Verify the unincorproted orgnization {string} is displaying in policy bundle and finish the policy")
	public void VerifyUnIncorprotePolicyAndFinishThePolicy(String UnincorpOrgName) throws Exception {
		context.getClientAdviserPage().VerifyTheUnicorpOrgAndSubmitPolicy(UnincorpOrgName);
	}

	@Given("Enter the Revenue {string} and PolicyEffectiveDate {string}")
	public void enter_the_revenue_and_policy_effective_date(String Revneue,String Effectivedate) throws Exception {
		context.getClientAdviserPage().enterRevenueAndEffetiveDate(Revneue,Effectivedate);
	}

	@Given("Enter the Revenue breakdown")
	public void enter_the_revenue_breakdown() throws Exception {
		context.getClientAdviserPage().enterRevenueBreakdown();
	}

	@Then("Verify case status {string} and capture the case id")
	public void verifyStatusAndCapturID(String expstatus) throws Exception {
		context.getClientAdviserPage().verifyTheStatusAndCaptureCaseIDForBundlePolicy(expstatus);
		context.getClientAdviserPage().closeCurrentInteraction();
	}

	@Then("Verify case status for bundle policy {string}")
	public void verifyStatusforBundlePolicy(String expstatus) throws Exception {
		context.getClientAdviserPage().verifyTheStatusForBundlePolicy(expstatus);

	}



	@Then("Click on the package case")
	public void clickOnPackageCase() throws Exception {
		context.getClientAdviserPage().clickPolicyBundlePackage();
	}

	@Then("Verify the policy document mail is present in attahcment tab")
	public void verifyThePolicyAttachment() throws Exception {
		WebElement doc=context.getClientAdviserPage().getAttachMailDocumentElement();
		Assert.assertEquals(doc.getText(),"Your new insurance with V...");
	}

	@Then("I select the case from Client Adviser UW Queue {string}")
	public void i_select_the_case_from_Client_Adviser_UW_Queue(String Q_value) throws Exception {
		context.getClientAdviserPage().selectFromUWQueue(Q_value);
	}

	@Then("Search the new Orgname and click CreateNew button")
	public void searchOrgNameandCreateNew() throws Exception {

		context.getClientAdviserPage().enterTheOrgnameForSearch();
	}

	@Given("Verify the dropdown values of orgtype for {string} and Select orgnizationType {string}")
	public void verify_the_dropdown_values_of_orgtype_for(String orgname,String orgtype) throws Exception {
		context.getClientAdviserPage().verifyOrgTypeDropdownforClientSegment(orgname,orgtype);
	}

	@Then("Search the new Orgname and click UpdateButton")
	public void searchOrgNameandUpdateExisitingOrg() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().enterTheOrgnameAndClickUpdate();
	}


	@Then("click submit button in UpdateOrganization Screen")
	public void ClickSubmitInUpdateOrganization() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().clickSubmitInOrgDetails();
	}

	@Then("Verify the UnincorpatedOrgnization added {string} in bundle policy is displayed in Orgnization")
	public void verifyTheUnincorpratedOrg(String orgname) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		Assert.assertEquals(context.getClientAdviserPage().getUnincorpratedOrgFromOrgnization(),orgname);
	}

	@Then("Enter the Orgdetails {string} {string} {string} {string} {string}")
	public void enterTheOrgdetails(String clientSegment,String goverdep, String profession, String phonenumber,String address) throws Exception {

		context.getClientAdviserPage().clickEnterTheNewOrgDetails(clientSegment,goverdep,profession,phonenumber,address);
	}

	@When("Enter the Orgdetails {string} {string} {string} {string} {string} and {string}")
	public void enter_the_orgdetails_and(String clientSegment,String goverdep, String profession, String phonenumber,String address, String govfundep) throws Exception {
		context.getClientAdviserPage().enterTheNewOrgDetails(clientSegment,goverdep,profession,phonenumber,address,govfundep);
	}

	@Then("I validate the {string} after bundle policy cancellation")
	public void i_validate_the_after_bundle_policy_cancellation(String status) throws Exception  {
		Thread.sleep(4000);
		context.getClaimsPage().verifyFinalCaseStatus(status);
	}

	@Then("Search the contact using contactid {string}")
	public void searchContactUsingContactID(String contactid) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().searchContactByContactId(contactid);

	}

	@Then("Add the contact to Organzation to the contact {string} {string}")
	public void addorgtoContact(String orgname,String accounttype) throws Exception {
		context.getClientAdviserPage().updateOrgDetailsInContact(orgname,accounttype);
	}

	@Then("Search contact using name {string} {string}")
	public void search_contact_using_name(String firstName, String LastName) throws Exception {
		context.getClientAdviserPage().searchContactUsingName(ObjectRepo.reader.getFirstName(),ObjectRepo.reader.getLastname());
	}


	@Then("I select the case from Client Adviser My cases tab")
	public void i_select_the_case_from_Client_Adviser_MyCases() throws Exception {

		context.getClientAdviserPage().selectCaseFromMyTasksTab();
	}

	@And("I Approve and submit the case")
	public void i_Approve_and_submit_the_case() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().selectFromClAdvUW();
	}

	@Then("I click on new interaction")
	public void i_click_on_interaction() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().clickNewInteraction();
		// homepage.verifyPageTitle("Client Portal");
	}
	@Then("I add a VGRMF {string}")
	public void addVgrmfFlag(String flag) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().addFlag(flag);

	}
	@Then("verify the VGRMF {string}")
	public void verifyVgrmfFlag(String flag) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().verifyFlag(flag);

	}

	@Then("I click on make a claim")
	public void i_click_on_makeaclaim() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().clickMakeClaim();
		// homepage.verifyPageTitle("Client Portal");
	}

	@Then("I click on make a new application")
	public void i_click_on_makeanewapplication() throws Exception {

		context.getClientAdviserPage().clickNewApp();
		// homepage.verifyPageTitle("Client Portal");
	}

	@Then("I click on update a policy")
	public void i_click_on_updatepolicy() throws Exception {
		context.getClientAdviserPage().clickUpdatePolicy();
		// homepage.verifyPageTitle("Client Portal");
	}

	@Then("I click on create new organisation")
	public void i_click_on_neworganisation() throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().clickCreateNewOrganisation();

		// homepage.verifyPageTitle("Client Portal");
	}

	@Then("I click on create new policy bundle {string} {string}")
	public void i_click_on_policy_bundle(String clientname, String revenue) throws Exception {
		context.getClientAdviserPage().clickPolicyBundle();
		context.getClientAdviserPage().policybundledetails(clientname, revenue);
		// homepage.verifyPageTitle("Client Portal");
	}

	@Then("I click on create new policy bundle for multiple clients {string}")
	public void i_click_on_policy_bundle(String revenue) throws Exception {
		context.getClientAdviserPage().clickPolicyBundle();
		context.getClientAdviserPage().policybundledetailsMultipleClients(revenue);
		// homepage.verifyPageTitle("Client Portal");
	}

	@Then("I verify package case")
	public void verifyPackageCase() throws Exception {

		// context.getClientAdviserPage().clickPolicyBundle();
		context.getClientAdviserPage().verfyPackageCase();
		// homepage.verifyPageTitle("Client Portal");
	}

	@Then("I verify the bundle policies created")
	public void verifyPolicyBundleCreated() throws Exception {

		// context.getClientAdviserPage().clickPolicyBundle();
		context.getClientAdviserPage().verifyPolicyBundleDetails();
		// homepage.verifyPageTitle("Client Portal");
	}

	@Then("I select the organization for new interaction {string}")
	public void i_select_org_interaction(String orgname) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().selectOrgToStartInteraction(orgname);
		context.getClientAdviserPage().SelectOrgName();
		Thread.sleep(2000);
		// homepage.verifyPageTitle("Client Portal");
	}

	@Then("Select the organization {string} for Adding Asset")
	public void selectTheOrgForAddedAsset(String orgname) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().SelectOrgNameForAddingAsset(orgname);

	}

	@Then("Select option in asset {string}")
	public void selectOptionInAsset(String options) throws Exception {
		context.getClientAdviserPage().selectOptionInAsset(options);
	}

	@Given("Click on Add Location and verify the added crestazone {string} is displayed for the Postcode {string}")
	public void click_on_Add_Location_and_verify_the_added_crestazone_is_working_properly(String crestazone,String postalcode) throws Exception {
		context.getClientAdviserPage().AddLocationAndVerifyTheCrestazone(postalcode,crestazone);
	}


	@Then("Search the asset by id {string} and open the asset {string}")
	public void searchAssetIDAndOpenHistory(String AssetID,String option) throws Exception {
		context.getClientAdviserPage().searchAssetAndOpenHistory(AssetID,option);
	}

	@Given("verify the following values are displayed in asset name section in Asset details in internalportal")
	public void verify_the_following_values_are_displayed_in_asset_name_section_in_Asset_details(DataTable dataTable) throws Exception {
		SoftAssert assetAssert=new SoftAssert();
		List<List<String>> assetdetails= dataTable.asLists(String.class);
		for(List asset:assetdetails ){
			assetAssert.assertTrue(context.getClientAdviserPage().verifyAssetDetailsFieldDisplayedInAssetNameSection(asset.get(0).toString()),asset.get(0).toString()+" is not displayed in Asset detials");
			assetAssert.assertEquals(context.getClientAdviserPage().getAssetDetailsFieldValuesFromAssetNameSection(asset.get(0).toString()),asset.get(1).toString());
			System.out.println("Asset Field Name :"+asset.get(0)+" Asset details:"+asset.get(1) );}
		assetAssert.assertAll();
	}

	@Given("verify the following values are displayed in asset name section in Asset details in clientPortal")
	public void verify_the_following_values_are_displayed_in_asset_name_Asset_details_clientporal(DataTable dataTable) throws Exception {
		SeleniumFunc.waitFor(5);
		SoftAssert assetAssert=new SoftAssert();
		List<List<String>> assetdetails= dataTable.asLists(String.class);
		for(List asset:assetdetails ){
//			assetAssert.assertTrue(context.getClientAdviserPage().verifyAssetDetailsFieldDisplayedInAssetNameSection(asset.get(0).toString()),asset.get(0).toString()+" is not displayed in Asset detials");
			assetAssert.assertEquals(context.getClientAdviserPage().getAssetDetailsFieldValuesFromAssetNameSection(asset.get(0).toString()),asset.get(1).toString());
			System.out.println("Asset Field Name :"+asset.get(0)+" Asset details:"+asset.get(1) );}
		assetAssert.assertAll();
	}

	@Then("Validate the following info in assethistory section of assettype {string} AssetName {string} and AssetID")
	public void ValidateInfoAssetInfo(String AsssetName,String AssetType) throws Exception {
		context.getClientAdviserPage().validateAssetInfoHistory(AsssetName,AssetType);
	}

	@Then("Select the contact associate with request {string}")
	public void selectTheOrgNameContactAssociation(String association) throws Exception {
		context.getClientAdviserPage().selectContactAssociationWithProperty(association);
	}


	@Then("Click Continue Button")
	public void ClickContinueButton() throws Exception {
		context.getClientAdviserPage().clickcontiune();
	}

	@Given("Add Asset primary Details {string} {string} {string} {string}")
	public void add_Asset_Details(String Location, String Assetype, String assetname, String Band) throws Exception {
		context.getClientAdviserPage().addAssetLocationAndType(Location, Assetype, assetname, Band);
	}

	@Given("Click Add details {string} {string} {string} {string}")
	public void click_add_details(String string, String string2, String string3, String string4) {

	}

	@Given("Attach the bulk upload file {string} and click submit")
	public void attachTheBulkUploadFile(String filepath) throws Exception {
		context.getClientAdviserPage().AttachTheBulkUploadFile(filepath);
	}
	@Given("click hamburger and Select the {string}")
	public void click_hamburger_and_Select_the(String options) throws Exception {
		context.getClientAdviserPage().clickHamBurgerAndSelectOptions(options);
	}

	@Given("click edit button and Add Cresta zone")
	public void click_edit_button_and_Add_Cresta_zone() throws Exception {
		context.getClientAdviserPage().editCrestazoneAndAddRecord();
	}

	@Then("Enter the crestazone {string} {string} {string} {string} {string}")
	public void enter_the_crestazone(String crestaid,String postalcode,String lowcresid,String highzone,String lowzone ) throws Exception {
		context.getClientAdviserPage().enterTheCrestaZone("AUS_9999","9999","Aus_99","AutoHigh","AutoLow");
	}



	@Given("Click Finish Button")
	public void click_Finish_Button() throws Exception {

		context.getClientAdviserPage().clickFinishButton();
	}

	@Given("Search the Assetby {string} using value {string}")
	public void search_the_Assetby_using_value(String AssetSearchBy, String SearchString) throws Exception {
		context.getClientAdviserPage().SearchAsset(AssetSearchBy, SearchString);
	}

	@Given("Select the Asset result by name {string}")
	public void select_the_Asset_result_by_name(String AssetName) throws Exception {
		context.getClientAdviserPage().selectAsset(AssetName);
	}

	@Given("Update the selected asset by {string}")
	public void update_the_selected_asset_by(String AssetName) throws Exception {
		context.getClientAdviserPage().switchToSelectecAsset();
		context.getClientAdviserPage().UpdateAsset(AssetName);
	}

	@Given("Capture the Asset Details and Click submit asset")
	public void capture_the_Asset_Details_and_Click_submit_asset() throws Exception {
		context.getClientAdviserPage().extractAssetIdAndSSubmitTheAsset();
	}
	@Then("Verify the case status {string} and capture the case id")
	public void verify_the_case_status_and_capture_the_case_id(String expstatus) throws Exception {
		context.getClientAdviserPage().verifyTheStatusAndCaptureCaseID(expstatus);
	}

	@Then("Link the policy {string} to the asset")
	public void linkPolicyToAsset(String policyNo) throws Exception {
		context.getClientAdviserPage().linkPolicy(policyNo);
	}

	@Then("I verify the bundle status as {string}")
	public void bundleStatus(String expsta) throws Exception {
		context.getClientAdviserPage().verifyBundleStatus(expsta);
		// homepage.verifyPageTitle("Client Portal");
	}

	@Then("I provide organisation name {string}")
	public void i_select_org_name(String orgname) throws Exception {
		context.getClientAdviserPage().enterOrgName(orgname);
	}

	@Then("Verify the IBPS option for the organization")
	public void verify_the_ibps_option_for_the_organization() throws Exception {
		context.getClientAdviserPage().addOrverifyIBPSOption();
	}

	@Then("I add task and do a general service request")
	public void click_generalservicerequest() throws Exception {

		context.getClientAdviserPage().addGeneralServiceRequest();
	}

	@Then("I want to create a new customer")
	public void createNewCustomer() throws Exception {

		context.getClientAdviserPage().createNewCustomer();
	}

	@Then("I fill the form")
	public void fillCustomerForm() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().fillUpNewCustomerForm();
	}

	@Then("I enter the service request query parameters {string} {string} and I assert {string} {string}")
	public void edit_generalservicerequest(String enquiryarea, String enquirycat, String thankyoumsg,
										   String statustobechecked) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().createGeneralServiceRequest(enquiryarea, enquirycat, thankyoumsg, statustobechecked);
	}

	@Then("I enter the service request query parameters which cannot be handled via phone {string} {string} and I assert {string} {string}")
	public void edit_generalservicerequest_no_phone(String enquiryarea, String enquirycat, String thankyoumsg,
													String statustobechecked) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().createGeneralServiceRequestNoPhone(enquiryarea, enquirycat, thankyoumsg, statustobechecked);
	}

	@Then("I logoff from client adviser portal")
	public void logout_clientadviser() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().closeAllInteractions();
		context.getClientAdviserPage().logoff();
	}

	@Then("I enter business details for claim")
	public void enter_claim_details() throws Exception {

		context.getClientAdviserPage().claimEnterBusiness();
	}

	@Then("I select the policy")
	public void enter_claim_details_orgname1() throws Exception {
		context.getClientAdviserPage().Selectpolicy();
	}

	@Then("I enter business details for claim using orgname {string}")
	public void enter_claim_details_orgname(String orgname) throws Exception {

		SeleniumFunc.switchToDefaultContent();
		context.getClientAdviserPage().claimEnterBusinessUsingOrgname(orgname);
	}

	@Then("I enter business details for claim For Triage")
	public void enter_claim_details_1() throws Exception {

		context.getClientAdviserPage().claimEnterBusiness_Traige();
	}
	@Then("I enter {string} and Click find button from Newapplication page")
	public void i_enter_and_Click_find_button_from_Newapplication_page(String Orgname) throws Exception {

		context.getClientAdviserPage().OrganisationnameFromCleintAdviser(Orgname);
		context.getData().setData("MultiYear","No");
	}
	@Then("I update the organization name {string} {string}")
	public void i_update_the_organization_name(String updateOrag ,String changeReason) throws Exception {
		context.getClientAdviserPage().UpdatetheOrganizationname(updateOrag, changeReason);
	}
	@Then("I verify the succesful msg for Organization")
	public void i_verify_the_succesful_msg_for_Organization() throws Exception {
		context.getClientAdviserPage().verify_the_succesful_msg_for_Organization();
	}

	@Then("I search for updated organization and verify the updated org name {string}")
	public void i_search_for_updated_organization_and_verify_the_updated_org_name(String UpdateOrgName) throws Exception {
		context.getClientAdviserPage().updated_organization_and_verify_the_updated_org_name(UpdateOrgName);
	}

	@Then("I update the organization name and slect the primary contact {string} {string} {string}")
	public void i_update_the_organization_name_and_slect_the_primary_contact(String updateOrag, String changeReason, String primarycontact) throws Exception {
		context.getClientAdviserPage().UpdatetheOrganizationnameandprimarycontact(updateOrag, changeReason,primarycontact);
	}
	@Then("I click on Bell nofication and select update Oraganization notification")
	public void i_click_on_Bell_nofication_and_select_update_Oraganization_notification() throws Exception {
		context.getClientAdviserPage().Bell_nofication_and_select_update_Oraganization_notification();
	}

	@Then("I provide required information before approval from client officer {string} {string}")
	public void i_provide_required_information_before_approval_from_client_officer(String AprrovalNote,String updateOrag) throws Exception {
		context.getClientAdviserPage().provide_required_information_before_approval_from_client_officer(AprrovalNote,updateOrag);
	}
	@Then("Click on all products in segments in client advisor")
	public void All_products_in_segment() throws Exception {
		context.getClientAdviserPage().AllProductInSegment();
	}

	@When("Click {string} to continue")
	public void click_to_continue(String decision) throws Exception {
		context.getClientAdviserPage().clickAnyOption(decision);
	}

	@Then("Click {string} to reapply the policy")
	public void Reapply_Policy(String decision) throws Exception {
		context.getClientAdviserPage().ReapplyPolicy(decision);
	}

	@And("I note down the PayWay number ID")
	public void iNoteDownThePayWayNumberID() throws Exception {
		context.getClientAdviserPage().notePaywayCaseID();

	}

	@And("Select organization by searching {string} and {string} and {string}")
	public void selectOrganizationBySearchingAndAnd(String clientSegment, String bundleType, String orgName) throws Exception
	{
		context.getClientAdviserPage().searchOrgByClientSegmentAndOrgName(clientSegment,bundleType,orgName);
	}

	@Then("Enter the Policy Effective Date")
	public void enterThePolicyEffectiveDate() throws Exception {
	context.getClientAdviserPage().enterEffectiveDate();
	}

	@Then("I note down the Package Case ID")
	public void iNoteDownThePackageCaseID() throws Exception {
	context.getClientAdviserPage().notePackageCaseID();
	}

	@And("Enter the Orgdetails {string} {string} {string} {string} {string} {string}")
	public void enterTheOrgdetails(String ClientSegment, String OrgType, String GovenrmentDepartment, String Profession, String PhoneNumber, String City) throws Exception {
	context.getClientAdviserPage().clickEnterTheNewOrgDetails(ClientSegment, OrgType,GovenrmentDepartment,Profession,PhoneNumber,City);
	}

	@And("I close the current tab")
	public void iCloseTheCurrentTab() throws Exception{

	context.getClientAdviserPage().closeCurrentTab();

	}

	@And("search the organization using clientSegment {string} and {string}")
	public void searchTheOrganizationUsingClientSegmentAnd(String clientSegment, String orgName) throws Exception {
	 context.getClientAdviserPage().searchOrgForBundleCancallation(clientSegment,orgName);
	}

}
