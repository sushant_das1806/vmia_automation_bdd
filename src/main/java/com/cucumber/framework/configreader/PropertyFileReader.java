
package com.cucumber.framework.configreader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;



import com.cucumber.framework.configuration.browser.BrowserType;
import com.cucumber.framework.utility.ResourceHelper;

public class PropertyFileReader implements ConfigReader {

	private Properties prop = null;
	public PropertyFileReader() throws Exception {
		prop = new Properties();
//		InputStream is = getClass().getClassLoader()
//				.getResourceAsStream("pom.xml");
//		this.prop = new Properties();
//		this.prop.load(is);

		try {
			if(Objects.equals("null",String.valueOf(System.getProperty("UAT"))))
				prop.load(ResourceHelper.getResourcePathInputStream("/src/main/resources/configfile/UAT_config.properties"));
			else
				prop.load(ResourceHelper.getResourcePathInputStream("/src/main/resources/configfile/"+System.getProperty("Env")+"_config.properties"));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public PropertyFileReader(String filepath) {
		prop = new Properties();
		try {
//			prop.load(ResourceHelper.getResourcePathInputStream("/src/main/resources/configfile/config.properties"));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getUserName() {
		return prop.getProperty("Username");
	}

	public String getReportConfigPath() {
		String reportConfigPath = prop.getProperty("reportConfigPath");
		if (reportConfigPath != null)
			return reportConfigPath;
		else
			throw new RuntimeException(
					"Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath");
	}

	public String getPassword() {
		return prop.getProperty("Password");
	}

	public String getOrganisation() {
		return prop.getProperty("OrgName");
	}
	public String getOrganisation1() {
		return prop.getProperty("OrgName1");
	}
	public String getCemeteryOrganisation() {
		return prop.getProperty("CemetryOrgName");
	}
	
	public String getPropAndPlanningOrganisation() {
		return prop.getProperty("PropAndPlanningOrgName");
	}
	
	public String getVicFleetOrganisation() {
		return prop.getProperty("VicFleetOrgName");
	}
	
	public String getTriageUserName() {
		return prop.getProperty("TriageofficerUsername");
	}
	public String getTriagePassword() {
		return prop.getProperty("Triageofficerpswd");
	}

	public String getHeadOfInsuranceUserName() { return prop.getProperty("headOfInsuranceUserName"); }
	public String getHeadOfInsurancePassword() { return prop.getProperty("headOfInsurancePswd"); }

	public String getClientAdvisorManagerUserName(){ return prop.getProperty("clientAdvisorManagerUserName"); }
	public String getClientAdvisorManagerPassword(){ return prop.getProperty("clientAdvisorManagerPswd"); }

	public String getSeniorUnderwriterConstructionUserName(){ return prop.getProperty("seniorUnderwriterConstructionUserName"); }
	public String getSeniorUnderwriterConstructionPassword(){ return prop.getProperty("seniorUnderwriterConstructionPswd"); }

	public String getCasualityPortfolioManagerUserName(){ return prop.getProperty("casualityPortfolioManagerUserName"); }
	public String getCasualityPortfolioManagerPassword(){ return prop.getProperty("casualityPortfolioManagerPswd"); }

	public String getPropertyPortfolioManagerUserName(){ return prop.getProperty("propertyPortfolioManagerUserName"); }
	public String getPropertyPortfolioManagerPassword(){ return prop.getProperty("propertyPortfolioManagerPswd"); }

	public String getPortfolioManagerConstructionUserName(){ return prop.getProperty("portfolioManagerConstructionUserName"); }
	public String getPortfolioManagerConstructionPassword(){ return prop.getProperty("portfolioManagerConstructionPswd"); }

	public String getSeniorTechnicalSpecialistUserName(){ return prop.getProperty("seniorTechnicalSpecialistUserName"); }
	public String getSeniorTechnicalSpecialistPassword(){ return prop.getProperty("seniorTechnicalSpecialistPswd"); }

	public String getChiefInsuranceOfficerUserName(){ return prop.getProperty("chiefInsuranceOfficerUserName"); }
	public String getChiefInsuranceOfficerPassword(){ return prop.getProperty("chiefInsuranceOfficerPswd"); }

	public String getClaimsAdminUserName(){ return prop.getProperty("claimsAdminUserName"); }
	public String getClaimsAdminPassword(){ return prop.getProperty("claimsAdminPswd"); }

	public String getWebsite() {
		return prop.getProperty("Website");
	}

	public String getWebsiteExternal() {
		return prop.getProperty("WebsiteExternal");
	}

	public int getPageLoadTimeOut() {
		return Integer.parseInt(prop.getProperty("PageLoadTimeOut"));
	}

	public int getImplicitWait() {
		return Integer.parseInt(prop.getProperty("ImplicitWait"));
	}

	public int getExplicitWait() {
		return Integer.parseInt(prop.getProperty("ExplicitWait"));
	}

	public String getGeospatialCoordinatorUsername(){return prop.getProperty("GeospatialCoordinatorUsername");}
	public String getGeospatialCoordinatorPassword(){return prop.getProperty("GeospatialCoordinatorPassword");}

	public String getDbType() {
		return prop.getProperty("DataBase.Type");
	}

	public String getDbConnStr() {
		return prop.getProperty("DataBase.ConnectionStr");
	}

	public BrowserType getBrowser() {
		return BrowserType.valueOf(prop.getProperty("Browser"));
	}

	public String getRun() {
		return prop.getProperty("Run");
	}

	@Override
	public String getClaimsOfficerUserName() {
		return prop.getProperty("ClaimsOfficerUserName");
	}

	@Override
	public String getCliamsOfficerPassword() {
		return prop.getProperty("CliamsOfficerPassword");
	}

	@Override
	public String getClaimsProtfolioMngrUserName() { return prop.getProperty("ClaimsProtfolioMngrUserName"); }
	@Override
	public String getClaimsProtfolioMngrPassword() {
		return prop.getProperty("ClaimsProtfolioMngrPswd");
	}

	@Override
	public String getRiskAdminUserName() {
		return prop.getProperty("RiskAdminusername");
	}
	@Override
	public String getRiskAdminPassword(){
		return prop.getProperty("RiskAdminpswd");
	}

	public String getUnderwriterName() {
		return prop.getProperty("underWriterName");
	}
	public String getUnderwriterPassword() {
		return prop.getProperty("underWriterPSWD");
	}
	
	public String getstatus() {
		return prop.getProperty("status");
	}
	public String getpendingstatus() {
		return prop.getProperty("pendingstatus");
	}
	public String getfinalstatus() {
		return prop.getProperty("finalstatus");
	}
	public String getpaymentstatus() {
		return prop.getProperty("paymentstatus");
	}

	public String getClientAdviserName() {
		return prop.getProperty("ClientAdvserUname");
	}
	public String getClientAdviserPassword() {
		return prop.getProperty("ClientAdvserpswd");
	}

	public String getMIPortfolioManagerName() {
		return prop.getProperty("MIPortfolioManagerName");
	}
    public String getMIPortfolioManagerPassword() {
    	 return prop.getProperty("MIPortfolioManagerPassword");
     }

	public String getAdminstartorsUserName() {
		return prop.getProperty("AdminstartorsUserName");
	}
	public String getAdminstartorsPswd() {
		return prop.getProperty("AdminstartorsPswd");
	}

	public String getBusinessmanagerUserName() {
		return prop.getProperty("BusinessmanagerUserName");
	}
	public String getBusinessmanagerPswd() {
		return prop.getProperty("BusinessmanagerPswd");
	}

	public String getInsightAnalyticsUserName() {
		return prop.getProperty("InsightAnalyticsUserName");
	}
	public String getInsightAnalyticsPswd() {
		return prop.getProperty("InsightAnalyticsPswd");
	}

	public String getInsuranceAdminUserName() {
		return prop.getProperty("InsuranceAdminUserName");
	}
	public String getInsuranceAdminPswd() {
		return prop.getProperty("InsuranceAdminPswd");
	}

	public String getProtfolioManagerUserName() {
		return prop.getProperty("ProtfolioManagerUserName");
	}
	public String getProtfolioManagerPswd() {
		return prop.getProperty("ProtfolioManagerPswd");
	}


	@Override
	public String getProduct() {
		// TODO Auto-generated method stub
		return null;
	}
	public String getfinanceName() {
		return prop.getProperty("financeName");
	}
    public String getfinanceNPassword() {
    	 return prop.getProperty("financeNPassword");
     }

	@Override
	public String getFirstName() { return prop.getProperty("Firstname");}

	@Override
	public String getLastname() { return prop.getProperty("Lastname");}

	@Override
	public String getUWPortfolioMangerUserName() {
	
		return prop.getProperty("UWProtfolioMngrUserName");
	}

	@Override
	public String getUWPortfolioMangerPassword() {
		
		return prop.getProperty("UWProtfolioMngrPswd");
	}

	@Override
	public String getClientUserName() {
		
		return prop.getProperty("ClientUsername");
	}

	@Override
	public String getClientPassword() {


		return prop.getProperty("ClientPassword");
	}

	@Override
	public String getRiskAdvisorUserName() {
		return prop.getProperty("RiskAdviosrUsername");
	}

	@Override
	public String getRiskAdvisorUserPassword() {
		return prop.getProperty("RiskAdviosrPassword");
	}
	
}
