package com.cucumber.framework.configreader;

import com.cucumber.framework.configuration.browser.BrowserType;

public interface ConfigReader {
	
	String getUserName();
	String getFirstName();
	String getLastname();
	String getPassword();

	String getTriageUserName();
	String getTriagePassword();

	String getClaimsOfficerUserName();
	String getCliamsOfficerPassword();

	String getClaimsProtfolioMngrUserName();
	String getClaimsProtfolioMngrPassword();

	String getAdminstartorsUserName();
	String getAdminstartorsPswd();

	String getBusinessmanagerUserName();
	String getBusinessmanagerPswd();

	String getInsightAnalyticsUserName();
	String getInsightAnalyticsPswd();

	String getInsuranceAdminUserName();
	String getInsuranceAdminPswd();

	String getProtfolioManagerUserName();
	String getProtfolioManagerPswd();

    String getRiskAdminUserName();
    String getRiskAdminPassword();
    String getUnderwriterName();
	String getUnderwriterPassword();
	String getOrganisation();
	String getOrganisation1();
	String getGeospatialCoordinatorUsername();
	String getGeospatialCoordinatorPassword();
	String getHeadOfInsuranceUserName();
	String getHeadOfInsurancePassword();
	String getClientAdvisorManagerUserName();
	String getClientAdvisorManagerPassword();
	String getSeniorUnderwriterConstructionUserName();
	String getSeniorUnderwriterConstructionPassword();
	String getCasualityPortfolioManagerUserName();
	String getCasualityPortfolioManagerPassword();
	String getPropertyPortfolioManagerUserName();
	String getPropertyPortfolioManagerPassword();
	String getPortfolioManagerConstructionUserName();
	String getPortfolioManagerConstructionPassword();
	String getSeniorTechnicalSpecialistUserName();
	String getSeniorTechnicalSpecialistPassword();
	String getChiefInsuranceOfficerUserName();
	String getChiefInsuranceOfficerPassword();
	String getClaimsAdminUserName();
	String getClaimsAdminPassword();
	String getClientAdviserName();
	String getRiskAdvisorUserName();
	String getRiskAdvisorUserPassword();
	String getClientAdviserPassword();
	
	String getCemeteryOrganisation();
	
	String getPropAndPlanningOrganisation();
	
	String getVicFleetOrganisation();
	
	String getWebsite();

	String getWebsiteExternal();

	int getPageLoadTimeOut();

	int getImplicitWait();

	int getExplicitWait();

	BrowserType getBrowser();

	String getRun();

	String getReportConfigPath();
	String getstatus();
	String getpendingstatus();
	String getfinalstatus();
	String getpaymentstatus();

	String getProduct();

	String getMIPortfolioManagerName();

	String getMIPortfolioManagerPassword();

	String getfinanceName();

	String getfinanceNPassword();

	String getUWPortfolioMangerUserName();
	String getUWPortfolioMangerPassword();
	
	String getClientUserName();
	String getClientPassword();
	
	

}
