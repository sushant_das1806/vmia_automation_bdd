
package com.cucumber.framework.configuration.browser;

import java.net.MalformedURLException;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.PageLoadStrategy;
import com.cucumber.framework.utility.ResourceHelper;



public class ChromeBrowser {

	public Capabilities getChromeCapabilities() {
		ChromeOptions option = new ChromeOptions();
		option.addArguments("start-maximized");
		option.addArguments("window-size=1936,1056");
		/* Added the below line to handle the 'timeout: Timed out receiving message from renderer: -0.001' error */ 
		option.setPageLoadStrategy(PageLoadStrategy.EAGER);
		option.addArguments("enable-features=NetworkServiceInProcess");
		option.addArguments("disable-features=NetworkService");
		option.addArguments("enable-features=NetworkServiceInProcess");
		option.addArguments("disable-features=NetworkService");
		DesiredCapabilities chrome = DesiredCapabilities.chrome();
		chrome.setJavascriptEnabled(true);
		chrome.setCapability(ChromeOptions.CAPABILITY, option);
		System.out.println("inside cap : "+chrome.getCapability(ChromeOptions.CAPABILITY));
		return chrome;
	}

	public WebDriver getChromeDriver(Capabilities cap) throws MalformedURLException {
		if (System.getProperty("os.name").contains("Mac")){
			System.setProperty("webdriver.chrome.driver", ResourceHelper.getResourcePath("/src/main/resources/drivers/chromedriver"));
			//WebDriverManager.chromedriver().setup();
			return new ChromeDriver(cap);
		}
		
		  else if(System.getProperty("os.name").contains("Window")){
		  System.setProperty("webdriver.chrome.driver",
		  ResourceHelper.getResourcePath("/src/main/resources/drivers/chromedriver.exe"
		  )); //WebDriverManager.chromedriver().setup(); 
		  return new ChromeDriver(cap);
		  }
		 
		
			/*
			 * else if(System.getProperty("os.name").contains("Window")){
			 * //System.setProperty("webdriver.gecko.driver",
			 * ResourceHelper.getResourcePath("/src/main/resources/drivers/geckodriver.exe")
			 * ); //WebDriverManager.firefoxdriver().setup(); return new RemoteWebDriver(new
			 * URL("http://10.0.0.240:5566/wd/hub"),cap); }
			 */
		return null;
	}
}
