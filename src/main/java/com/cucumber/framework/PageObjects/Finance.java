package com.cucumber.framework.PageObjects;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cucumber.framework.GeneralHelperSel.CSVFileReader;
import com.cucumber.framework.context.TestData;
import org.apache.poi.util.SystemOutLogger;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cucumber.framework.CS.CustomerServ;
import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import com.cucumber.framework.TestBase.TestBase;
//import com.cucumber.framework.helper.Logger.LoggerHelper;
import org.testng.asserts.SoftAssert;

public class Finance extends CustomerServ{
    TestData testData;
    Finance fin;
    public Finance(WebDriver driver, TestData data) {
        super(driver);
        testData=data;
    }

    By workbasketDropdown = By.xpath("//span/i[@title='Select work owner']");
    By caseIdMenu = By.xpath("//div[text()='Case ID']/child::div/a[@title='Open Menu']");
    By applyFilter = By.xpath("//span[text()='Apply filter']");
    By filterTextBox = By.xpath("//label[text()='Search text']/following::input");
    By applyButton = By.xpath("//button[text()='Apply']");
    By paywayNumbarTextbox = By.xpath("//label[text()='Payway number']/following::input");
    By submitButton = By.xpath("//button[text()='Submit']");
    By closeIcon = By.xpath("//i[@aria-label='Close']");


    public void switchToWorkbasket(String basket) throws Exception{
        SeleniumFunc.click(workbasketDropdown);
        SeleniumFunc.waitFor(2);
        SeleniumFunc.click(By.xpath("//span[text()='"+basket+"']"));
        SeleniumFunc.waitFor(3);

    }

    public void openPaywayCaseAndAddPaywayNumber() throws Exception{
        System.out.println("payway number size is.............."+TestBase.paywayNumber.size());
        for(int i =0; i<TestBase.paywayNumber.size();i++) {
            System.out.println("Proceeding with the for statement....");
            SeleniumFunc.waitFor(2);
            SeleniumFunc.click(caseIdMenu);
            SeleniumFunc.waitFor(2);
            SeleniumFunc.click(applyFilter);
            SeleniumFunc.waitFor(2);
            SeleniumFunc.click(filterTextBox);
            SeleniumFunc.clearTextBox(filterTextBox);
            System.out.println("The payway number: "+TestBase.paywayNumber.get(i));
            SeleniumFunc.enterValue(filterTextBox, TestBase.paywayNumber.get(i));
            SeleniumFunc.click(applyButton);
            SeleniumFunc.waitFor(2);
            SeleniumFunc.click(By.xpath("//a[text()='"+TestBase.paywayNumber.get(i)+"']"));
            SeleniumFunc.waitFor(2);
            Random rnd = new Random();
            int number = rnd.nextInt(89999999);
            int paywayNum = 10000000 +number;
            SeleniumFunc.clearTextBox(paywayNumbarTextbox);
            SeleniumFunc.enterValue(paywayNumbarTextbox,String.valueOf(paywayNum));
            SeleniumFunc.waitFor(2);
            SeleniumFunc.click(submitButton);
            SeleniumFunc.waitFor(2);
            SeleniumFunc.click(closeIcon);
        }
        System.out.println("Away from the for statement...");
        TestBase.paywayNumber.clear();

    }

}
