package com.cucumber.framework.PageObjects;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.datatransfer.StringSelection;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.InputMismatchException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cucumber.framework.context.TestContext;
import com.cucumber.framework.context.TestData;
import com.cucumber.framework.stepDef.UWStepDefinition;
import io.cucumber.java.en.Then;
import io.cucumber.java.eo.Se;
import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cucumber.framework.CS.CustomerServ;
import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import com.cucumber.framework.TestBase.TestBase;
//import com.cucumber.framework.helper.Logger.LoggerHelper;
import org.testng.asserts.Assertion;
import org.testng.asserts.SoftAssert;

public class Claims extends CustomerServ implements ClaimsLoc, HomePageLoc, CemetryClaimsLoc {
	//private final Logger log = LoggerHelper.getLogger(Claims.class);
	UWStepDefinition obj = new UWStepDefinition();
	Claims claims;
	String reviewdate;
	String claimFileNumber;
	List<String> linkedClaim=new ArrayList<>();
	TestData testData;
	public Claims(WebDriver driver, TestData data) {
		super(driver);
		testData=data;
	}


	public void sendAppObject(Claims claims) {
		this.claims = claims;
	}

	public void clickDate() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(date_time_of_loss);
	}

	public void clickToday() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);
	}

	public void selectConatact(String conatct) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(contact);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		
	}
	public void selectLossType(String losstype) throws Exception {
		// SeleniumFunc.waitFor(5);
//		Select Drptypeofloss = new Select(driver.findElement(By.xpath(Losstypedropdown)));
//		 Drptypeofloss.selectByVisibleText(losstype);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(Losstypedropdown,
				losstype);

	}

	public void selectSubTypeOfLossForConstruction(String sutbtype) throws Exception {
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText("$PpyWorkPage$pDamageSubType",sutbtype);
	}

	public void selectLodge(String lodge) throws Exception {
		if(lodge.equalsIgnoreCase("Claim")){
			SeleniumFunc.xpath_GenericMethod_Click(claim_lodge+""+lodge+"']");
		}
		if(lodge.equalsIgnoreCase("Notification")){
			SeleniumFunc.xpath_GenericMethod_Click(claim_lodge+""+lodge+"']");
		}

	}

	public void selectCoverageForClaim(String coverage) throws Exception {
		if(coverage.equalsIgnoreCase("Public and products liability")){
			SeleniumFunc.xpath_GenericMethod_Click(claim_lodge+""+coverage+"']");
		}
		if(coverage.equalsIgnoreCase("Professional liability")){
			SeleniumFunc.xpath_GenericMethod_Click(claim_lodge+""+coverage+"']");
		}
		if(coverage.equalsIgnoreCase("Formal investigation and representation expenses")){
			SeleniumFunc.xpath_GenericMethod_Click(claim_lodge+""+coverage+"']");
		}
		if(coverage.equalsIgnoreCase("Unknown")){
			SeleniumFunc.xpath_GenericMethod_Click(claim_lodge+""+coverage+"']");
		}

	}

	public void selectSubOrg(String subOrgType) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(chksubOrg);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtSubsidiaryorganisation, subOrgType);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(optnSubOrg);

	}

	public void selectLossTypeAdv(String losstype) throws Exception {

		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(Loss_type_New,
				losstype);

	}

	public void selectLossType1(String losstype1) throws Exception {
		// SeleniumFunc.waitFor(5);

		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(Loss_type, losstype1);
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(loss_type, losstype);
	}

	public void EnterTheClaimantDetailsFlighrRiskGPAClaim(String studentName,String DoB,String StudentAddress,String ParentsName,String RelationShiptoStudent,String ParentsAddress,String phonenumber,String email) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.name_GenericMethod_Sendkeys(Gpa_Claim_FlightRisk_StudentName,studentName);
		SeleniumFunc.name_GenericMethod_Sendkeys(Gpa_Claim_FlightRisk_DateOfBirth,DoB);
		SeleniumFunc.enterTheValueInComboBox(Gpa_Claim_FlightRisk_Student_Address,StudentAddress);
		SeleniumFunc.name_GenericMethod_Sendkeys(Gpa_Claim_FlightRisk_Parent_Gaurdian_Name,ParentsName);
		SeleniumFunc.name_GenericMethod_Sendkeys(Gpa_Claim_FlightRisk_RelationshipToStudent,RelationShiptoStudent);
		SeleniumFunc.enterTheValueInComboBox(Gpa_Claim_FlightRisk_Parent_Adress,ParentsAddress);
		SeleniumFunc.name_GenericMethod_Sendkeys(Gpa_Claim_FlightRisk_emailAdress,email);
		Thread.sleep(3);
		SeleniumFunc.name_GenericMethod_Sendkeys(Gpa_Claim_FlightRisk_PhoneNumber,phonenumber);



	}

	public void EnterTheIncidentDetailsFlighrRiskGPAClaim(String IncidentDes,String IsIncidentReportToShool,String InjuriesSustained) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.name_GenericMethod_Sendkeys(Gpa_Claim_FlightRisk_AccidentDetails,IncidentDes);
		SeleniumFunc.name_GenericMethod_Sendkeys(Gpa_Claim_FlightRisk_IsReportedSchool,IsIncidentReportToShool);
		SeleniumFunc.name_GenericMethod_Sendkeys(Gpa_Claim_FlightRisk_InjuryDetails,InjuriesSustained);
	}

	public void MediCalDetails_FlightRisk_GPA(String IsSeekeTreatment,String IsPrivateHealthInsurance,String IsAmbulanceCoverd) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		if(IsSeekeTreatment.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(Gpa_Claim_FlightRisk_MedicalDetails_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(Gpa_Claim_FlightRisk_MedicalDetails_No);
		clickContinueButton();
		if(IsPrivateHealthInsurance.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(Gpa_Claim_FlightRisk_MedicalDetails_IsPrivateInsurace_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(Gpa_Claim_FlightRisk_MedicalDetails_IsPrivateInsurace_No);
		if(IsAmbulanceCoverd.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(Gpa_Claim_FlightRisk_MedicalDetails_IsAmbulanceCover_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(Gpa_Claim_FlightRisk_MedicalDetails_IsAmbulanceCover_No);
	}

	public void addMedicalExpenseItem(String item,String Amount) throws Exception {

		SeleniumFunc.name_GenericMethod_Click(Gpa_Claim_FlightRisk_Add);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Gpa_Claim_FlightRisk_item,item);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Gpa_Claim_FlightRisk_Amount,Amount);
	}

	public void enterTheIncidentDetailsGPAVolunteersClaim(String incidentDate,String IncidentDes,String claimantName) throws Exception {

		SeleniumFunc.name_GenericMethod_Sendkeys(GPA_Volunteers_IncidentDate,incidentDate);
		SeleniumFunc.name_GenericMethod_Sendkeys(GPA_Volunteers_IncidentDes,IncidentDes);
		SeleniumFunc.name_GenericMethod_Sendkeys(GPA_Volunteers_IncidentClaimantName,claimantName);

	}

	
	public void enterLossDetails(String lossdate,String lossdetails,String thirdpary) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(loss_Calender);
		SeleniumFunc.xpath_GenericMethod_Click(lossdetails_date);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(lossdetails_detail, lossdetails);	
		SeleniumFunc.xpath_GenericMethod_Sendkeys(lossdetails_thirdparty, thirdpary);		
	}

	public void clickContinueButton() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
	}

	public void clickContinueButtonContinuosly(int numb) throws Exception {
		for(int i=0; i<numb; i++){
			SeleniumFunc.waitFor(1);
			SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		}

	}

	public void validateErrorMessageOfPayment() throws Exception {

		SeleniumFunc.acceptTheAlert();
		System.out.print("Size is.......: "+SeleniumFunc.getElements(checkDetailErrorMessge).size());
        Assert.assertTrue(SeleniumFunc.getElements(checkDetailErrorMessge).size()==3);

	}

	public void SelectIsTravelerPartOfInsuredComapany(String TravelInfo) throws Exception {
		if(TravelInfo.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(travelClaim_IsEmployeeOfInsuranceComapny_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(travelClaim_IsEmployeeOfInsuranceComapny_No);
	}

	public void enterTheTravelClaimIncidentDeatil(String isBusinessTravel,String DepartureDate,String ReturnDate,String DepartureCity,String Incidetcity,String IncidetCountry,String exactlocation) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(Claimant_Name,"test");
		if(isBusinessTravel.equalsIgnoreCase("Yes"))
		SeleniumFunc.xpath_GenericMethod_Click(travelClaim_IsBusinessAuthorisedTravel_Yes);
		else
		SeleniumFunc.xpath_GenericMethod_Click(travelClaim_IsBusinessAuthorisedTravel_No);
		SeleniumFunc.name_GenericMethod_Sendkeys(travelClaim_DepartureDate, DepartureDate);
		SeleniumFunc.name_GenericMethod_Sendkeys(travelClaim_ReturnDate, ReturnDate);
		SeleniumFunc.name_GenericMethod_Sendkeys(travelClaim_DepartureCity, DepartureCity);
		SeleniumFunc.name_GenericMethod_Sendkeys(travelClaim_IncidentCity, Incidetcity);
		SeleniumFunc.name_GenericMethod_Sendkeys(travelClaim_IncidentCountry, IncidetCountry);
		SeleniumFunc.name_GenericMethod_Sendkeys(travelClaim_IncidentExactLocatin, exactlocation);

	}

	public void selectThePropertyMissedInTravelClaim(String Proerties) throws Exception {
		String[] propery =Proerties.split(";");
		for(int i=0;i<propery.length;i++)
			SeleniumFunc.xpath_GenericMethod_Click("//label[text()='"+propery[i]+"']//preceding-sibling::input[1]");
	}

	public void enterTheIncidentReportDetailsForBaggaeProperyInTravelClaim(String Damegedes,String lossdetails,String IsReportToPolice,String isLostByCarrier,String IsComplainRaisedAgainstCarrier,String Complaindes,String IsAllMissiedProertyYours) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(travelClaim_HowDamageOccur, Damegedes);
		Thread.sleep(300);
		SeleniumFunc.name_GenericMethod_Sendkeys(travelClaim_StepsToRecoverArticle, lossdetails);
		Thread.sleep(300);
		if(IsReportToPolice.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(travelClaim_WasReportMadeTothePolice_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(travelClaim_WasReportMadeTothePolice_No);
		Thread.sleep(300);
		if(isLostByCarrier.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(travelClaim_IsArticleLostByCarrier_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(travelClaim_IsArticleLostByCarrier_No);
		Thread.sleep(300);
		if(IsComplainRaisedAgainstCarrier.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(travelClaim_IsComplainLodgetAgainstCarrier_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(travelClaim_IsComplainLodgetAgainstCarrier_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_GenericMethod_Sendkeys(travelClaim_ComplainDesctiptionOfCarrier, Complaindes);
		if(IsAllMissiedProertyYours.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(travelClaim_IsAllMissingArticlesYourProerpty_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(travelClaim_IsAllMissingArticlesYourProerpty_No);

	}



	public void selectCoverage(String coverage) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_SelectValueByClickingOptions(claims_Coverage,coverage);
	}

	public void SelectCoveragefortravelClaim(String coverage) throws Exception {
		SeleniumFunc.waitFor(2);
		if(coverage.equalsIgnoreCase("International"))
			SeleniumFunc.xpath_GenericMethod_Click(TravelInsurance_Coverage_Internantional);
		else if(coverage.equalsIgnoreCase("Domestic"))
			SeleniumFunc.xpath_GenericMethod_Click(TravelInsurance_Coverage_Domestic);
	}

	public void AddIncidentDetailsForFineArtsClaim(String typeOfIncident,String incidentDetails,String Address) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(fineArts_claims_typeincident,typeOfIncident);
		SeleniumFunc.name_GenericMethod_Sendkeys(fineArts_claims_incidentDetails,incidentDetails);
		SeleniumFunc.name_GenericMethod_Sendkeys(fineArts_claims_incidentAddress,Address);
	}

	public void AddIncidentDetailsForEmrepssMotor(String reportedDate,String incidentDetails,String Name) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.enterValue(By.xpath(EmRePSSMotor_IncidentReportDate),reportedDate);
		SeleniumFunc.enterValue(By.id(EmRePSSMotor_IncidentDetails),incidentDetails);
		SeleniumFunc.enterValue(By.id(EmRePSSMotor_name),Name);
//
	}

	public void selectPrivateClaims(String string) throws Exception{
		SeleniumFunc.click(By.xpath("//input[@type='radio']/following::label[text()='"+string+"']"));

	}
	public void addArtWorkDetails(String artist,String titleofwork,String dimension,String owner,String worth ) throws Exception {
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(fineArts_claims_Artist,artist);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(fineArts_claims_titleOfWork,titleofwork);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(fineArts_claims_Dimensions,dimension);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(fineArts_claims_Owner,owner);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(fineArts_claims_Worth,worth);
	}


	public void enterResponsibleOfDamageAndPoliciReport(String isSomeoneResponsibleFordamage,String isIncidentReported) throws Exception {
		SeleniumFunc.waitFor(2);
		if(isSomeoneResponsibleFordamage.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(fineArts_claims_thirdPartyResponsible_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(fineArts_claims_thirdPartyResponsible_No);

		if(isIncidentReported.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(fineArts_claims_ReportedPolice_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(fineArts_claims_ReportedPolice_No);

	}

	public void selectBusinessRow() throws Exception {
		SeleniumFunc.waitFor(3);

		SeleniumFunc.xpath_GenericMethod_Click(policyeffectivdate);
		SeleniumFunc.waitFor(3);
		String polciynum=SeleniumFunc.xpath_Genericmethod_getElementText(Policynumber);
		
		if(polciynum.equalsIgnoreCase(TestBase.currentPolicyNumber)) {
		SeleniumFunc.xpath_GenericMethod_Click(business_select_row);
		}else
		{
			SeleniumFunc.xpath_GenericMethod_Click(business_select_row);
		}

	}

	public void enterLossDetails(String lossdetails) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(claimDetails,lossdetails);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimDetails, lossdetails);
	}

	public void clickMandatoryOptions() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(ClaimDOL);
		SeleniumFunc.xpath_GenericMethod_Click(EPLDOL);
	}

	public void enterTheContractDetailsConstructionClaim(String contractionContract,String contractSinged,String commencementOfWork,String dueDate,String valueOfContract,String PartyMakingClaim) throws Exception {
		SeleniumFunc.name_GenericMethod_Sendkeys(construction_contract,contractionContract);
		SeleniumFunc.name_GenericMethod_Sendkeys(construction_contractDate,contractSinged);
		SeleniumFunc.name_GenericMethod_Sendkeys(construction_commenceofWorkdate,commencementOfWork);
		SeleniumFunc.name_GenericMethod_Sendkeys(construction_Valuecontract,valueOfContract);
		SeleniumFunc.name_GenericMethod_Sendkeys(Construction_dueDate,dueDate);
		SeleniumFunc.name_GenericMethod_Sendkeys(Construction_nameOfThePartymakingClaim,PartyMakingClaim);
	}

	public void CaptureLossDetailsOfConstructionClaim(String lossdetail) throws Exception {
		String[] loss =lossdetail.split(";");
		for(int i=0;i<loss.length;i++){

			SeleniumFunc.xpath_GenericMethod_Click("//label[text()='"+loss[i]+"']/preceding-sibling::input[1]");
		}
	}


	public void enterThePropertDamageConstructionClaim(String IncidentfirstReport, String howDamageOccur, String IsWitness, String IsItReportedPolicy, String estimateValueOfDamage, String descrptionOfDamage) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(construction_WhenIncidentIsReported,IncidentfirstReport);
		SeleniumFunc.name_GenericMethod_Sendkeys(construction_HowDamageOccur,howDamageOccur);
		SeleniumFunc.name_GenericMethod_Sendkeys(construction_ValueOfDamage,estimateValueOfDamage);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(construction_DescriptionDamage,descrptionOfDamage);
		if(IsWitness.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(Construction_AnyWitness_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(Construction_AnyWitness_No);

		if(IsItReportedPolicy.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(Construction_ReportedPolicy_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(Construction_ReportedPolicy_No);
	}

	public void enterThePersonalInjuryDetailsConstructionClaim(String IncidentfirstReportPI,String InjuryPersons,String NatureOfInjury,String TreatmentProvided,String incidentWhatHappended) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(construction_WhenFirstIncidentRepored,IncidentfirstReportPI);
		SeleniumFunc.name_GenericMethod_Sendkeys(construction_names_InjuredPersons,InjuryPersons);
		SeleniumFunc.name_GenericMethod_Sendkeys(construction_NatureOfInjury,NatureOfInjury);
		SeleniumFunc.name_GenericMethod_Sendkeys(construction_TreatmentProvided,TreatmentProvided);
		SeleniumFunc.name_GenericMethod_Sendkeys(construction_IncidentWhatHappended,incidentWhatHappended);
	}


	public void selectUrgencyLevel(String urgency, String contact) throws Exception {
		SeleniumFunc.waitFor(7);
		System.out.println("Control is here");
		if(urgency.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(UrgencyYes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(UrgencyNo);
		
		if(contact.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(mi_claim_contact_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(mi_claim_contact_No);
		
	}
	
	public void selectClaimsSource(String claimssource)throws Exception  {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(source_claims_dropdown);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(source_claims_option+"span[text()='"+claimssource+"']");
	}
	
	
	public void enterThePatienDetails(String name,String familname,String gender,String dob, String medcalrecord, String admitstatus) throws Exception{
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_GenericMethod_Sendkeys(mi_claim_patientname, name);
		SeleniumFunc.name_GenericMethod_Sendkeys(mi_claim_patintFamilname, familname);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(mi_claim_patientGenderDropdwon, gender);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(mi_claim_admissonstaus, admitstatus);
		SeleniumFunc.name_GenericMethod_Sendkeys(mi_claim_medicalreocrdno, medcalrecord);
		SeleniumFunc.name_GenericMethod_Click(mi_claim_patientDOB);
		SeleniumFunc.waitFor(1);
		String[] date =dob.split("/");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(mi_claim_DOB_month, date[1]);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(mi_claim_DOB_year, date[2]);
		SeleniumFunc.xpath_GenericMethod_Click("//table[@class='calendar']//tbody[@id='controlCalBody']//tr/td/a[text()='"+date[0]+"']");
		//table[@class='calendar']//tbody[@id="controlCalBody"]//tr/td/a[text()='3']
		
	}
	
	public void selectDate(String dateval) throws Exception {
		SeleniumFunc.waitFor(1);
		String[] date =dateval.split("/");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(mi_claim_DOB_month, date[1]);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(mi_claim_DOB_year, date[2]);
		SeleniumFunc.xpath_GenericMethod_Click("//table[@class='calendar']//tbody[@id='controlCalBody']//tr/td/a[text()='"+date[0]+"']");
	}
	
	public String getClaimStausOfMIclaim() throws Exception {
		SeleniumFunc.waitFor(3);
//		return driver.findElement(By.xpath(mi_claim_status)).getText();
		return SeleniumFunc.xpath_Genericmethod_getElementText(mi_claim_status);	
	}
	
	public void transferClaim(String user) throws Exception {
		SeleniumFunc.name_GenericMethod_Sendkeys(claim_transfer_User,user);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Click(claim_transfer_button);
		SeleniumFunc.waitFor(3);
	}

	public void transferClaimToAdmin(String user) throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(userFromSystem);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Click(claim_transfer_User);
		SeleniumFunc.name_GenericMethod_Sendkeys(claim_transfer_User,user);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(claimsAdmin);
		SeleniumFunc.name_GenericMethod_Click(claim_transfer_button);
	}

	public void transferTheClaim(String access) throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(userFromSystem);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Click(claim_transfer_User);
		SeleniumFunc.name_GenericMethod_Sendkeys(claim_transfer_User,access);
//		if(access.equalsIgnoreCase("Claims Admin")){
//			SeleniumFunc.waitFor(1);
////			SeleniumFunc.xpath_GenericMethod_Click(claimsAdmin);
//			SeleniumFunc.clickElementUsingActions(By.xpath(claimsAdmin));
//		}else if(access.equalsIgnoreCase("Claims Officer 1 R3")){
			SeleniumFunc.waitFor(1);
//			SeleniumFunc.xpath_GenericMethod_Click(claimsUser);
			SeleniumFunc.clickElementUsingActions(By.xpath(claimsUser));
//		}
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(transferBtn);

	}

	public void closeTransferClaimWindow(String user) throws Exception {
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(closeBtn);
	}

	public void selectClaimsOfficerAndFilter(String user) throws Exception {
		SeleniumFunc.name_GenericMethod_Click(roleTF);
		SeleniumFunc.name_GenericMethod_Clear(roleTF);
		SeleniumFunc.name_GenericMethod_Sendkeys(roleTF,user);
			for(int i=0;i<=5;i++){
				try{
				SeleniumFunc.clickElementUsingActions(By.xpath(claimsUser));
				break;
				}catch(Exception e){

				}
			}
		SeleniumFunc.name_GenericMethod_Click(filterBtn);
		SeleniumFunc.waitFor(3);
	}

	public void saveClaimFileNumber() throws Exception {
		SeleniumFunc.waitFor(3);
//		claimFileNumber = driver.findElement(By.cssSelector(claimFileNum)).getText();
		SeleniumFunc.switchToDefaultContent();
		claimFileNumber = driver.findElement(By.xpath(claimFileNum)).getText();
		System.out.println("The Claim File Number is: "+claimFileNumber);
	}

	public void enterClaimFileIDAndClickApply() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(claimFileFilter);
		SeleniumFunc.waitFor(5);
		WebElement searchField = SeleniumFunc.getElement(By.xpath(claimsFileTF));
		System.out.println("The Claim File ID is: "+claimFileNumber);
		searchField.sendKeys(claimFileNumber);
		SeleniumFunc.xpath_GenericMethod_Click(adminApplyBtn);
	}

	public void validateTheClaimFileRecord() throws Exception {
		SeleniumFunc.waitFor(6);
		WebElement ClaimFileNumb = driver.findElement(By.xpath("//span[text()='"+claimFileNumber+"']"));
		System.out.println("Expected record is: "+ClaimFileNumb.getText());
		Assert.assertTrue(ClaimFileNumb.isDisplayed());
	}

	public void selectPatientClaiment(String option) throws Exception {
		if(option.equalsIgnoreCase("No"))
			SeleniumFunc.xpath_GenericMethod_Click(mi_claim_claiment_No);
		else 
			SeleniumFunc.xpath_GenericMethod_Click(mi_claim_claiment_Yes);
	}
	
	public void enterTheIncidentDetail(String incidentdes,String factaccount,String clinicSpc,String area,String disclousre) throws Exception {
		SeleniumFunc.waitFor(4);
		SeleniumFunc.name_GenericMethod_Click(mi_claim_doIncident);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);
		SeleniumFunc.name_GenericMethod_Click(mi_claim_datefirstaware);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);	
		SeleniumFunc.xpath_GenericMethod_Sendkeys(mi_incidentDetail, incidentdes);
		SeleniumFunc.name_GenericMethod_Sendkeys(mi_IncidentFact, factaccount);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(mi_claim_clincalspc, clinicSpc);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(mi_claim_areaincident, area);
		SelectDisclousreOccured(disclousre);
	}
	
	public void  SelectDisclousreOccured(String disclosure) throws Exception {
		if(disclosure.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(mi_claim_disclosureOccure_Yes);
		else if(disclosure.equalsIgnoreCase("No"))
			SeleniumFunc.xpath_GenericMethod_Click(mi_claim_disclosureOccure_No);
		else
			SeleniumFunc.xpath_GenericMethod_Click(mi_claim_disclosureOccure_Unknown);
	}
	
	public void searchAndSelectOrgazation(String org) throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(Serch_orgname, org);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(Search_findButton);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Search_searchrestult);	
		SeleniumFunc.xpath_GenericMethod_Click(btnContinue);	
	}

	public void searchAndGetOrganization() throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(Serch_orgname, obj.select_the_organization());
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(Search_findButton);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Search_searchrestult);
		SeleniumFunc.xpath_GenericMethod_Click(btnContinue);
	}

	public void verifyPolicyYear() throws Exception {
		Boolean policyyearflag = true;
		SeleniumFunc.waitFor(2);
		List<WebElement> rows = driver.findElements(By.xpath(policy_table));
		int s = rows.size();
		System.out.println("Size----------" + s);
		for (int i = 1; i < s; i++) {
			String text = SeleniumFunc
					.xpath_Genericmethod_getElementText(policy_table + "[" + (i + 1) + "]/td[7]/div/span");
			System.out.println(text);
			String year = text.substring(6, 10);
			System.out.println(year);
			if (!(year.equals("2020"))) {
				policyyearflag = false;
			}
			Assert.assertTrue(policyyearflag);
			System.out.println(policyyearflag + " iiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		}
		
	}
	public void uploadattachmentinviewnote(String filepath) throws Exception {

		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(click_onattachment);
		waitFor(5);
		SeleniumFunc.uploadFile(filepath);
		waitFor(2);

	}
	public void verifyFNOLCaseformat(String fnolformat) throws Exception {
		SeleniumFunc.waitFor(3);
		String fnolcaseid = xpath_Genericmethod_getElementText(fnol_case_id);
		// SeleniumFunc.xpath_GenericMethod_Click(fnol_case_id);
		// String fnolcaseid = SeleniumFunc.getElementText(fnol_case_id);
		String format = fnolcaseid.substring(4, 8);
		// System.out.println(fnolcaseid+"============");
		SeleniumFunc.writeToAssertionFile("Expected case format : " + fnolformat + " , Actual format : " + format,
				(fnolformat.equals(format) ? "PASS" : "FAIL"));
		Assert.assertTrue(format.equals(fnolformat));
	}


	public void enterEmployeePersonalDetailDOAClaim(String FirstName,String Lastname, String Position,String empolyeDuties) throws Exception {
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_InsuredOfficer,"");
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_Firstname,FirstName);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_Lastname,Lastname);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_EmployeePosition,Position);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_EmployeeDuty,empolyeDuties);
	}

	public void enterEmployeePersonalDetailNewDOAClaim(String insuredOfficer, String FirstName,String Lastname, String Position,String empolyeDuties) throws Exception {
		SeleniumFunc.waitInMilliSeconds(2);
//		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_InsuredOfficer,insuredOfficer);
		SeleniumFunc.xpath_GenericMethod_Click(DOAReporteddateImg);
		SeleniumFunc.waitInMilliSeconds(2);
		SeleniumFunc.xpath_GenericMethod_Click(DOAcurrent_date);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(DOATextField,insuredOfficer);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_Firstname,FirstName);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_Lastname,Lastname);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_EmployeePosition,Position);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_EmployeeDuty,empolyeDuties);
	}

	public void enterEmployeeSalaryDetailsDOAClaim(String baseSalary,String totalRumenarion,String isEmpHaveContract,String IsEmpCoveredAgreement,String IsEmpHavePersonaFile) throws Exception {
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_Salary,baseSalary);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_TotalRumenearation,totalRumenarion);
		if(isEmpHaveContract.equalsIgnoreCase("yes"))
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_DoesEmployeeHaveContract_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_DoesEmployeeHaveContract_No);
		if(IsEmpCoveredAgreement.equalsIgnoreCase("yes"))
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_CoveredByEnterpriseAgreement_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_CoveredByEnterpriseAgreement_No);
		if(IsEmpHavePersonaFile.equalsIgnoreCase("yes"))
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_DoesEmployeeHavePersonalFile_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_DoesEmployeeHavePersonalFile_No);
	}

	public void enterEmployeeTerminationDetailDoAclaim(String isEmployeeTerminated,String isEmpResinged,String joinDate,String AgeAtJoining) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_EmployeementJoindate,joinDate);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_EmployeeAgeAtTermination,AgeAtJoining);
		if(isEmployeeTerminated.equalsIgnoreCase("yes"))
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_IsTerminiationOccured_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_IsTerminiationOccured_No);
		if(isEmpResinged.equalsIgnoreCase("yes"))
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_DidEmployeeResing_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_DidEmployeeResing_No);
	}


	public void enterTheLossdetails_vehicleClaim(String whereDamageOccur,String whatDamaheOccur,String releventdetails,String howdamageoccur,String wasIncidentReportToPolice) throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(motorvechicle_WhereDamageOccured,whereDamageOccur);
		SeleniumFunc.name_GenericMethod_Sendkeys(motorvechicle_WhatDamageOccured,whatDamaheOccur);
		SeleniumFunc.name_GenericMethod_Sendkeys(motorvechicle_OtherReleventDetail,releventdetails);
		SeleniumFunc.name_GenericMethod_Sendkeys(motorvechicle_HowDamageOccured,howdamageoccur);
		if(wasIncidentReportToPolice.equalsIgnoreCase("yes"))
			SeleniumFunc.xpath_GenericMethod_Click(motorvechicle_wasIncidentReportToPolice_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(motorvechicle_wasIncidentReportToPolice_No);
	}

	public void enterVechileDetails_motorClaim(String vehicleType,String RegistrationNumber,String owner) throws Exception {
		SeleniumFunc.name_GenericMethod_Click(motorvechicle_AddVehicleButton);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(motorvechicle_VehicleType,vehicleType);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(motorvechicle_RegistrationNumber,RegistrationNumber);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(motorvechicle_OwnerLease,owner);
	}

	public void enterTheLeagalActionDetaisDOAClaim(String isEmplyeeDisrupts,String isEmpProcedding,String natureOfComplaint,String wasEmployeeAtProbation,String terminationReason,String challangeReason,String salaryAtTerminiation,String isOrgempCount,String isLegalRepresativePresent ) throws Exception {

		if(isEmplyeeDisrupts.equalsIgnoreCase("yes"))
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_IsEmployeeActionDisrupts_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_IsEmployeeActionDisrupts_No);
		if(isEmpProcedding.equalsIgnoreCase("yes"))
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_HasEmployeeComencedProceding_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_HasEmployeeComencedProceding_NO);
		SeleniumFunc.waitFor(3);
//		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_EmployeeComplain,natureOfComplaint);
		SeleniumFunc.enterValue(By.xpath(DOAClaim_EmployeeComplainXpath),natureOfComplaint);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_TerminationReason,terminationReason);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_ChallengeReason,challangeReason);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(DOAClaim_TerminationAmount,salaryAtTerminiation);
		Thread.sleep(200);
		SeleniumFunc.getElement(By.name(DOAClaim_TerminationAmount)).sendKeys(Keys.TAB);

		if(wasEmployeeAtProbation.equalsIgnoreCase("yes"))
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_HasEmployeeInProbation_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_HasEmployeeInProbation_No);
		if(isOrgempCount.equalsIgnoreCase("yes"))
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_IsYourEmplyeeConuntLessThan15_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_IsYourEmplyeeConuntLessThan15_No);
		if(isLegalRepresativePresent.equalsIgnoreCase("yes"))
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_IsLegalRepresentationAdded_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(DOAClaim_IsLegalRepresentationAdded_No);


		SeleniumFunc.waitFor(2);

	}




	public void verifyCaseStatus(String casestatus) throws Exception {
		String casestatusscreen = xpath_Genericmethod_getElementText(case_status);
		System.out.println(casestatusscreen + "***********************8");
		System.out.println(casestatus + "***********************");
		SeleniumFunc.writeToAssertionFile(
				"Expected case format : " + casestatus + " , Actual format : " + casestatusscreen,
				(casestatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatus.equals(casestatusscreen));

	}

	public void verifyFinalCaseStatus(String casestatus) throws Exception {
		String casestatusscreen = xpath_Genericmethod_getElementText(final_case_statuss);
		System.out.println(casestatusscreen + "***********************8");
		System.out.println(casestatus + "***********************");
//		SeleniumFunc.writeToAssertionFile(
//				"Expected case format : " + casestatus + " , Actual format : " + casestatusscreen,
//				(casestatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		 Assert.assertTrue(casestatus.equals(casestatusscreen));

	}

	public void verifyPendingTriageStatus(String actualstatus, String expectedstatus) throws Exception{
		SeleniumFunc.waitFor(2);
		System.out.println("Expected Status --------- "+expectedstatus);
		System.out.println("Actual Status --------- "+actualstatus);
		Assert.assertEquals(actualstatus, expectedstatus);
	}
	public void OpenReportBrowser(String claimstatus) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(left_toggle);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(reports)));
		SeleniumFunc.waitFor(1);
		// SeleniumFunc.xpath_GenericMethod_Click(reports_claim_cases);
		if(claimstatus.equalsIgnoreCase("All claims")){
			SeleniumFunc.xpath_GenericMethod_Click(All_Claims);}
		if(claimstatus.equalsIgnoreCase(("Litigation reports"))){
			SeleniumFunc.xpath_GenericMethod_Click(Litigation_Reports);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(All_Litigated_Claims);}
		SeleniumFunc.waitFor(3);
	}

	public void openLitigationClaimInReportTable(String claimstatus) throws Exception {
		SeleniumFunc.waitFor(2);
		String Test = SeleniumFunc.getElementText(header1);
		System.out.println(Test);
		SeleniumFunc.waitFor(2);
		WebElement claimNumberIntable = SeleniumFunc.getElement(By.xpath(Filter_Lit_File));
		claimNumberIntable.click();
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(Filter_lit_icon);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(Apply_Filter);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(reports_search_text, TestBase.currentClaimFileID);
		SeleniumFunc.xpath_GenericMethod_Click(claims_apply);
		SeleniumFunc.waitFor(4);
		String claimNumberInReport = xpath_Genericmethod_getElementText(report_claim_number);
		System.out.println(claimNumberInReport);
		// System.out.println(claimstatusfromscreen);
		// System.out.println(claimstatus);
		SeleniumFunc.writeToAssertionFile("Expected claim status in reports : " + TestBase.currentClaimFileID
						+ " , Actual claim status in reports : " + claimNumberInReport,
				(claimstatus.equals(claimNumberInReport) ? "PASS" : "FAIL"));
		Assert.assertEquals(TestBase.currentClaimFileID, claimNumberInReport);
//		Assert.assertTrue(claimNumberInReport.equals(claimstatus));

	}
	public void LitigationReportBrowser() throws Exception{
		List<WebElement> ReportResults = driver.findElements(By.xpath(LitigationTableResult));
		for(WebElement LitReportResult : ReportResults)
		{
			System.out.println("Litigation Report Results : " + LitReportResult.getText());
		}
	}

	public void verifytheLandedPage(String PageName) throws Exception {

		String ManageComponetScreen = xpath_Genericmethod_getElementText(AddPayee_Details_Page);
		Assert.assertTrue(PageName.equals(ManageComponetScreen));
	}

	public void verifyErrorMessageSelectLossScreen(String message) throws Exception {
		String messagescreen = xpath_Genericmethod_getElementText(error_message_identify_loss_screen);
		System.out.println(messagescreen + "***********************8");
		System.out.println(message + "***********************");
		//log.info("this is a test log message");
		Assert.assertTrue(messagescreen.equals(message));
	}

	public void downloadClaimLink() throws Exception {
		try {
			String messagescreen = xpath_Genericmethod_getElementText(downloadClaimFormLink);
		} catch (Exception e) {
			Assert.assertTrue(false);
		}

	}

	public void verifyErrorMessageFutureDate(String message) throws Exception {
		String messagescreen = xpath_Genericmethod_getElementText(error_message_cannot_be_future_date);
		System.out.println(messagescreen + "***********************8");
		System.out.println(message + "***********************");
		//log.info("this is a test log message");
		Assert.assertTrue(messagescreen.equals(message));

	}

	public void verifyLossTypeInPolicyList(String losstype) throws Exception {
		Boolean policyyearflag = true;
		// SeleniumFunc.waitFor(5);
		List<WebElement> rows = driver.findElements(By.xpath(policy_table));
		int s = rows.size();
		System.out.println("Size----------" + s);
		for (int i = 1; i < s; i++) {
			String text = SeleniumFunc
					.xpath_Genericmethod_getElementText(policy_table + "[" + (i + 1) + "]/td[3]/div/span");
			System.out.println(text);

			if (losstype.equals("Property")) {
				Assert.assertTrue(text.equals("Property"));
			}
			if (losstype.equals("Public and Product Liability")) {
				Assert.assertTrue(text.equals("Liability for hall hirers and other users of school facilities"));
			}
			if (losstype.equals("Group Personal Accident")) {
				Assert.assertTrue(text.equals("Group Personal Accident"));
			}
		}
	}

	public void fillInDataIntoQuestions(String where, String what, String how) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(where_did_loss_occur, where);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(what_is_the_loss, what);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(how_loss_occur, how);
		SeleniumFunc.waitFor(1);

		// SeleniumFunc.xpath_GenericMethod_Click(general_damages_question_answer_no);
	}
	public void selectTheLossOrDamage(String loss,String isDetOrisRepairedByAcer) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click("//label[text()='"+loss+"']/preceding-sibling::input[1]");
		SeleniumFunc.waitFor(2);
		if(loss.equalsIgnoreCase("Apple notebook")){
			if(isDetOrisRepairedByAcer.equalsIgnoreCase("Yes"))
				SeleniumFunc.xpath_GenericMethod_Click(isDet_Yes);
			else
				SeleniumFunc.xpath_GenericMethod_Click(isDet_No);

		}else if(loss.equalsIgnoreCase("Non-Apple notebook")){
			if(isDetOrisRepairedByAcer.equalsIgnoreCase("Yes"))
				SeleniumFunc.xpath_GenericMethod_Click(isRepairedByAcer_Yes);
			else
				SeleniumFunc.xpath_GenericMethod_Click(isRepairedByAcer_No);
		}
	}

	public void enterDamagedItems(String decription, String price, String asset) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.clickElementUsingActions(By.cssSelector(addBtnCssClass));
		SeleniumFunc.name_GenericMethod_Sendkeys(desriptionNameLoc, decription);
		SeleniumFunc.name_GenericMethod_Sendkeys(originalPurchaseNameLoc, price);
		SeleniumFunc.name_GenericMethod_Sendkeys(assetNumberNameLoc, asset);

	}
	public void fillInScipDetails(String description, String purchasecost, String salvagevalue) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(scip_add_button);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(scip_purchase_desc, description);
		SeleniumFunc.waitFor(1);
//		SeleniumFunc.xpath_GenericMethod_Click(scip_datepicker);
//		SeleniumFunc.xpath_GenericMethod_Click(today_link);

		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(scip_purchase_cost, purchasecost);
//		SeleniumFunc.waitFor(1);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(scip_salvage_value, salvagevalue);

	}

	public void attachFileInAttachmentsPage(String filepath, String category) throws Exception {

		SeleniumFunc.xpath_GenericMethod_scrollIntoView(attach_button_claims);
		SeleniumFunc.xpath_GenericMethod_Click(attach_button_claims);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(attach_button_claimss);

		SeleniumFunc.waitFor(1);

		SeleniumFunc.uploadFile(filepath);

		// SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(attachment_popup_category_dropdown,
		// "category");
		try {
			SeleniumFunc.xpath_GenericMethod_Sendkeys(attachment_popup_category_dropdown,
					"Repair or replacement invoices");
		} catch (Exception e) {
		}
		try {
			SeleniumFunc.xpath_GenericMethod_Sendkeys(attachment_popup_category_dropdown, "Invoice");
		} catch (Exception e) {
		}
		SeleniumFunc.waitFor(1);

		SeleniumFunc.xpath_GenericMethod_Click(attach_button);
		SeleniumFunc.waitFor(1);

	}

	public void attachFileInAttachmentsPage1(String filepath, String category) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(attachment_button);

		SeleniumFunc.waitFor(1);

		SeleniumFunc.uploadFile(filepath);

		// SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(attachment_popup_category_dropdown,
		// "category");

		// SeleniumFunc.xpath_GenericMethod_Sendkeys(attachment_popup_category_dropdown,
		// "RepairOrReplacementInvoices");
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(categorydropdown, 5);
		SeleniumFunc.xpath_GenericMethod_Click(attach_button);

	}

	public void attachFileInAttachmentsPage3(String filepath, String category) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(btnAttachment);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(btnSelectFile);
		SeleniumFunc.waitFor(3);

		SeleniumFunc.uploadFile(filepath);

		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(categorydropdown, 3);
		SeleniumFunc.xpath_GenericMethod_Click(attach_button);

	}

	public void attachFileInAttachmentsPageWithoutDropdown(String filepath) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(attachment_button_1);

		SeleniumFunc.waitFor(1);

		SeleniumFunc.uploadFile(filepath);
		try {
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(
					attachment_popup_category_dropdown, "category");
		} catch (Exception e) {
		}

		SeleniumFunc.xpath_GenericMethod_Click(attach_button);

	}

	public void attachFileInAttachmentsPageProofOfClaim(String filepath) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(btnProductAttach);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(attach_button_claimss);

		SeleniumFunc.waitFor(2);

		SeleniumFunc.uploadFile(filepath);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(drpCategory, 2);
		
		SeleniumFunc.xpath_GenericMethod_Click(attach_button);
		SeleniumFunc.waitFor(15);

	}

	public void reportInHamburger(String claimstatus) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(left_toggle);

		SeleniumFunc.xpath_GenericMethod_Click(reports);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(reports_claim_cases);

		SeleniumFunc.xpath_GenericMethod_Click(reports_claimfile_id);
		// SeleniumFunc.xpath_GenericMethod_HoverOnDemoScreenPops(finance_caseid_filter);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(reports_claimfileid_filter);
		SeleniumFunc.xpath_GenericMethod_Click(reports_claims_applyfilter);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(reports_search_text, TestBase.currentClaimFileID);
		SeleniumFunc.xpath_GenericMethod_Click(claims_apply);

		String claimstatusfromscreen = xpath_Genericmethod_getElementText(report_claim_status);
		SeleniumFunc.writeToAssertionFile("Expected claim status in reports : " + claimstatus
				+ " , Actual claim status in reports : " + claimstatusfromscreen,
				(claimstatus.equals(claimstatusfromscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(claimstatusfromscreen.equals(claimstatus));

	}

	public void reportInHamburger1(String claimstatus) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(left_toggle);

		// SeleniumFunc.xpath_GenericMethod_Click(reports);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(reports)));
		SeleniumFunc.waitFor(1);
		// SeleniumFunc.xpath_GenericMethod_Click(reports_claim_cases);
		SeleniumFunc.xpath_GenericMethod_Click(All_Claims);

		// SeleniumFunc.xpath_GenericMethod_Click(reports_claimfile_id);
		// SeleniumFunc.xpath_GenericMethod_HoverOnDemoScreenPops(finance_caseid_filter);
		SeleniumFunc.waitFor(1);
		// SeleniumFunc.xpath_GenericMethod_Click(reports_claimfileid_filter);

	}

	public void reportInHamburger2(String claimstatus) throws Exception {
		SeleniumFunc.waitFor(2);
		driver.switchTo().frame(2);
		SeleniumFunc.waitFor(4);
		String Test = SeleniumFunc.getElementText(header);
		System.out.println(Test);
		SeleniumFunc.xpath_GenericMethod_Click(Filter_claim_File);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Filter_icon);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Apply_Filter);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Apply_Filter);
		SeleniumFunc.xpath_GenericMethod_Click(reports_claims_applyfilter);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(reports_search_text, TestBase.currentClaimFileID);
		SeleniumFunc.xpath_GenericMethod_Click(claims_apply);
		String claimstatusfromscreen = xpath_Genericmethod_getElementText(report_claim_status_1);
		// System.out.println(claimstatusfromscreen);
		// System.out.println(claimstatus);
		SeleniumFunc.writeToAssertionFile("Expected claim status in reports : " + claimstatus
						+ " , Actual claim status in reports : " + claimstatusfromscreen,
				(claimstatus.equals(claimstatusfromscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(claimstatusfromscreen.equals(claimstatus));

	}

	public void clickNewClaimFromClaimsOfficer() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(left_toggle);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(new_ham);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.jsClick(ham_new_claim);
		SeleniumFunc.waitFor(2);
	}

	public void clickOnBulkTransfer() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(logout_icon);
		SeleniumFunc.xpath_GenericMethod_Click(bulkTransferLink);
	}

	public void selectAllClaims() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(selectAllCheckBox);
		SeleniumFunc.xpath_GenericMethod_Click(selectActionBtn);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(selectTransfer);
	}

	public void claimEnterBusiness(String claims_zipcode) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(zipcode);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(zipcode, claims_zipcode);
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(find_button);
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(select_business_checkbox);
		waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		waitFor(2);

		SeleniumFunc.xpath_GenericMethod_Click(select_contact_checkbox);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);

	}
	public void propertyEmRepSSLossDetials(String how, String damage)throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(loss_proEmprss_how, how);
		SeleniumFunc.name_GenericMethod_Sendkeys(loss_proEmprss_damage, how);
		SeleniumFunc.waitFor(1);
	}
	public void propertyLossDetials(String where, String what, String how)throws Exception{
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(loss_teacher_where,where);
		SeleniumFunc.name_GenericMethod_Sendkeys(loss_teacher_what,what);
		SeleniumFunc.name_GenericMethod_Sendkeys(loss_teacher_how,how);
		SeleniumFunc.xpath_GenericMethod_Click(loss_teacher_schedule);
		SeleniumFunc.waitFor(1);
	}

	public void propertyDamagedItems(String Description, String Amount)throws Exception{
		SeleniumFunc.waitFor(2);
		WebElement buttonAdd = getElement(By.xpath(loss_Add));
		Actions add = new Actions(driver);
		add.moveToElement(buttonAdd).click().build().perform();
//		SeleniumFunc.jsClick(loss_Add);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(loss_damage_ItemDecirp,Description);
		SeleniumFunc.name_GenericMethod_Sendkeys(loss_damage_Amount,Amount);
		SeleniumFunc.waitFor(1);
	}

	public void fillInvestigationCoverageDetails(String InvestigationName, String what, String claimantname) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_investigation_name,InvestigationName);
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(today_linkk);
		SeleniumFunc.xpath_GenericMethod_Click(today_linkk);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(today_linkk, "17/02/2021");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_what_happened, what);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_claimant_name, claimantname);
	}

	public void fillInvestigationLiabilityCoverageDetails(String Particulars, String ThirdParty) throws Exception {
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(today_linkk);
		SeleniumFunc.xpath_GenericMethod_Click(today_linkk);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(today_linkk, "17/02/2021");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_what_happened, Particulars);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_claimant_name, ThirdParty);
	}

	public void fillLiabilityLossDetails(String what, String claimantname, String addline1, String addline2,
			String suburb, String state, String postcode, String nature, String treatment) throws Exception {
		try{
			SeleniumFunc.waitFor(1);
			SeleniumFunc.xpath_GenericMethod_Click(propertyDamage);
		}catch(Exception e){

		}
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(today_linkk);
		SeleniumFunc.xpath_GenericMethod_Click(today_linkk);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(today_linkk, "17/02/2021");
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_what_happened, what);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_claimant_name, claimantname);
		// SeleniumFunc.xpath_GenericMethod_Click(Liab_Loss_details_ClickonLink);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.enterTheValueInComboBox(liab_loss_details_claimant_address_line3,addline1);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_claimant_address_line1, addline1);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_claimant_address_line2, addline2);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_claimant_suburb, suburb);
//		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(liab_loss_details_claimant_state, state);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_claimant_postcode, postcode);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_nature_of_injury, nature);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_loss_details_treatment_provided, treatment);

	}

	public void clickSaveAndExitButton() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(save_and_exit_button);

	}

	public void enterDateandTimeOfLoss(String dateofloss) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(date_time_of_loss_text, dateofloss);
	}

	public void enterClientBankingAccountName(String acntname) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(clientbanking_account_name, acntname);

	}

	public void enterIntermentHolderDetails(String holdername, String holderlastname, String contactfirstname,
			String contactlastname, String suburb, String addressline1, String state, String contactphonenumber)
			throws Exception {

		SeleniumFunc.xpath_GenericMethod_Sendkeys(interment_holder_name, holdername);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(interment_last_name, holderlastname);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(contact_first_name, contactfirstname);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(contact_last_name, contactlastname);
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(address_lookup,
		// addressloopupinput);
		SeleniumFunc.waitFor(2);
		// SeleniumFunc.xpath_GenericMethod_Click("//span[text()='" + address + "']");
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(interment_hyperlnk);
		SeleniumFunc.xpath_GenericMethod_Click(interment_hyperlnk);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(interment_Address1, addressline1);
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_address_line2,
		// addressline2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(interment_subrb, suburb);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(interment_postcode, "2001");
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(interment_State, state);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(interment_contactphone, contactphonenumber);

	}

	public void monumentIncidentDetails(String firstname, String lastname, String incidentlocation, String plotnumber,
			String damagereason, String amountclaimed) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Sendkeys(incident_first_name, firstname);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(incident_last_name, lastname);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(incident_location, incidentlocation);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(plot_number, plotnumber);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(how_damage, damagereason);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(amount_claimed, amountclaimed);

	}

	public void enterClientBankingAccountNameAfterClearingOriginal(String acntname) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Clear(clientbanking_account_name);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(clientbanking_account_name, acntname);

	}

	public void provideLossDetailsPropertyCyber(String howdamage) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(how_damage_property, howdamage);
		SeleniumFunc.xpath_GenericMethod_Click(DOID);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(DOID, "Test");

	}

	public void enterClientBankingBSB(String bsb) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(clientbanking_bsb, bsb);

	}

	public void enterClientBankingAccountNumber(String acntnumber) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(clientbanking_account_number, acntnumber);

	}

	public void enterCyberGstQuestion() throws Exception {
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(cyber_incident_response_no);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(cyber_incident_response_no);

	}

	public void enterInsuredEmployee() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(insured_employee_no);

	}

	public void enterClientBankingBankName(String bankname) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(clientbanking_bank_name, bankname);

	}
	
	public void extractClaimNumber() throws Exception {
		String claimtext=null;
		SeleniumFunc.waitFor(2);
		String fnolcaseid = null;
		try {
			List<WebElement> claimtexts= SeleniumFunc.getElements(claimnumber_ele);
			for(WebElement caseidtext:claimtexts){
				if(caseidtext.getText().length()>0) { claimtext=caseidtext.getText();
					break;}
			}
		}catch(StaleElementReferenceException e) {
			SeleniumFunc.waitFor(2);
			TestBase.getDriver().navigate().refresh();
			fetchCaseID();
		}
		Pattern p = Pattern.compile("[A-Z]{2}-([0-9]{5})");
        Matcher m = p.matcher(claimtext);
        m.matches();
        if(m.find()) {
        	TestBase.currentClaimNumber=m.group(0);
        	TestBase.currentClaimFileID=m.group(0);
        }
	}

	public void extractClaimNumberFromMyOpenCase() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(vima_logo);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(myOpencase);
		TestBase.currentClaimNumber=SeleniumFunc.getElement(By.xpath(ClaimNumber_MyOpenCase)).getText();
		System.out.println("Claim number"+TestBase.currentClaimNumber);
	}

	public void skip_reviewclaimDuplicate() throws Exception {
		SeleniumFunc.name_GenericMethod_Click(Mi_claim_submit);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.name_GenericMethod_Click("pyCaseActionAreaButtons_pyWorkPage_13");
	}
	
	public void selectClaimcategory(String claimcategory,String solictorneed,String claimsensitive) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(mi_claim_claimcatogery+claimcategory+"']");
		if(claimsensitive.equalsIgnoreCase("Yes")) SeleniumFunc.xpath_GenericMethod_Click(mi_claim_isclaimsentive_Yes);
		else  SeleniumFunc.xpath_GenericMethod_Click(mi_claim_isclaimsentive_No);	
		if(claimcategory.equalsIgnoreCase("C"))
		{if(solictorneed.equalsIgnoreCase("Yes")) SeleniumFunc.xpath_GenericMethod_Click(mi_claim_solictorneed_Yes);
		else  SeleniumFunc.name_GenericMethod_Click(mi_claim_solictorneed_No);}
		SeleniumFunc.name_GenericMethod_Click(Mi_claim_submit);
	}


	public void openClaimUnassingedQueueFromSearch() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(left_toggle);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(client_search);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(claimsOption);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(Search_ClaimNumberTextBox,TestBase.currentClaimNumber);
		System.out.println("The Claim Number is: ------>"+TestBase.currentClaimNumber);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(search_SearchButtonClaim);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click("(//a[@title='Expand to show child rows'])[1]");
		waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click("//a[contains(@name,'PortalSearchDisplay') and contains(text(),'CU')]");
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(triage_actions_button);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(transferclaim);
	}

	public void openClaimFromSearch() throws Exception{
		SeleniumFunc.xpath_GenericMethod_Click(left_toggle);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(client_search);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(claimsOption);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(Search_ClaimNumberTextBox,TestBase.currentClaimNumber);
		System.out.println("The Claim Number is: ------>"+TestBase.currentClaimNumber);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(search_SearchButtonClaim);
		SeleniumFunc.waitFor(6);
//		List<WebElement> claimsList = driver.findElements(By.xpath("//td[@aria-describedby='Case ID']/following-sibling::td[@aria-describedby='Claim number']/span"));
//		System.out.println("The size is------------>: "+claimsList.size());
//		if(claimsList.size()==1){
//			System.out.println("List not filtered and going to select the only record!");
//		WebElement caseid = driver.findElement(By.xpath("//tr[@data-test-id='201905310627430956121-R1']/td[3]"));
		SeleniumFunc.xpath_GenericMethod_Click(caseidRec);
//		}else{
//			System.out.println("List got not filtered");
//		}
	}

	public void OpenClaimFromQueue(String queue) throws Exception {
		SeleniumFunc.waitFor(6);
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(queueClaimsDropDown);
		SeleniumFunc.waitForElementToClikable(By.xpath("//span[text()='"+queue+"']"),200);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+queue+"']");
		if(queue.equalsIgnoreCase("Unassigned Claims")){
			System.out.println("Its if statement------>");
//			SeleniumFunc.waitFor(10);
			SeleniumFunc.waitForElementToClikable(By.xpath(ClaimnumerTH1),40);
			SeleniumFunc.executeScript("arguments[0].click()", SeleniumFunc.getElement(By.xpath(ClaimnumerTH1)));}
		else{
			System.out.println("Its else statement------>");
			SeleniumFunc.waitFor(15);
			SeleniumFunc.executeScript("arguments[0].click()", SeleniumFunc.getElement(By.xpath(ClaimnumerTH))); }
		SeleniumFunc.waitFor(2);
		SeleniumFunc.executeScript("arguments[0].click()", SeleniumFunc.getElement(By.xpath(claimnumerFilterIcon)));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text_1,TestBase.currentClaimNumber );
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text_1,"21-1067" );
		SeleniumFunc.xpath_GenericMethod_Click(claims_apply);
		WebDriverWait wait=new WebDriverWait(TestBase.getDriver(),300);
		System.out.println("The current claim number issss------>"+TestBase.currentClaimNumber);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='"+TestBase.currentClaimNumber+"']/ancestor::td/preceding-sibling::td[1]//a")));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+TestBase.currentClaimNumber+"']/ancestor::td/preceding-sibling::td[1]//a");
		SeleniumFunc.waitFor(2);
	}

	public void OpenClaimNUmberFromQueue(String queue) throws Exception {
		SeleniumFunc.waitFor(6);
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(queueClaimsDropDown);
		SeleniumFunc.waitForElementToClikable(By.xpath("//span[text()='"+queue+"']"),200);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+queue+"']");
		if(queue.equalsIgnoreCase("Unassigned Claims")){
			System.out.println("Its if statement------>");
			SeleniumFunc.waitFor(20);
			SeleniumFunc.executeScript("arguments[0].click()", SeleniumFunc.getElement(By.xpath(claimNumberFilterIcon)));
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
			} else{
			System.out.println("Its else statement------>");
		}
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text_1,TestBase.currentClaimNumber );
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(claims_apply);
		WebDriverWait wait=new WebDriverWait(TestBase.getDriver(),300);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='"+TestBase.currentClaimNumber+"']/ancestor::td/preceding-sibling::td[1]//a")));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+TestBase.currentClaimNumber+"']/ancestor::td/preceding-sibling::td[1]//a");
		SeleniumFunc.waitFor(2);
	}

	public void OpenClaimFromTheQueue(String queue, String orgname) throws Exception {
//		SeleniumFunc.waitFor(6);
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(queueClaimsDropDown);
//		SeleniumFunc.waitForElementToClikable(By.xpath("//span[text()='"+queue+"']"),200);
//		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+queue+"']");
		SeleniumFunc.waitForElementToClikable(By.xpath("//*[@node_name='pyWorkBasketDetailAssigneeRowDropDown']//span[text()='"+queue+"']"),200);
		SeleniumFunc.xpath_GenericMethod_Click("//*[@node_name='pyWorkBasketDetailAssigneeRowDropDown']//span[text()='"+queue+"']");
		if(queue.equalsIgnoreCase("Triage Claims")) {
			SeleniumFunc.waitFor(20);
			SeleniumFunc.executeScript("arguments[0].click()", SeleniumFunc.getElement(By.xpath(claimnameFilterIcon)));
//			SeleniumFunc.waitForElement(By.xpath("//span[text()='Apply filter']"),20);
//			SeleniumFunc.waitForElementToClikable(By.xpath("//span[text()='Apply filter']"),80);
			SeleniumFunc.waitFor(20);
			SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
//			SeleniumFunc.waitForElement(By.xpath("//input[@class=\"leftJustifyStyle\"]"),80);
			SeleniumFunc.waitForElementToClikable(By.xpath("//input[@class='leftJustifyStyle']"),80);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text_1,orgname);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(claims_apply);
			SeleniumFunc.waitFor(20);
			SeleniumFunc.xpath_GenericMethod_Click(firstCaseNumber);
		} else {
			System.out.println("Its selected Unassigned claims");
		}

	}

	public void enterClientBankingEmail(String email) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(clientbanking_email, email);

	}

	public void fillInClaimantDetails(String studentname, String dateofbirth, String addressline1, String addressline2,
			String suburb, String state, String postcode, String parentname, String relationship,
			String parentaddressline1, String parentaddressline2, String parentsuburb, String parentstate,
			String parentpostcode, String telephone, String parentemail) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_student_name, studentname);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_date_of_birth, dateofbirth);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(student_address_hyperlink);
		SeleniumFunc.xpath_GenericMethod_Click(student_address_hyperlink);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_address_line1, addressline1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_address_line2, addressline2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_suburb, suburb);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(
				loss_details_claimant_student_state, state);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_postcode, postcode);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_parent_name, parentname);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_relationship, relationship);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(parent_address_hyperlink);
		SeleniumFunc.xpath_GenericMethod_Click(parent_address_hyperlink);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_parent_address_line1, parentaddressline1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_parent_address_line2, parentaddressline2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_parent_suburb, parentsuburb);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(
				loss_details_claimant_parent_state, parentstate);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_parent_postcode, parentpostcode);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_parent_email, parentemail);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claimantdetails_parent_telephone, telephone);
		SeleniumFunc.waitFor(1);

	}

	public void fillInDataIntoAccidentDetails(String description, String reportedto, String injury) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(accidentdetails_description, description);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(accidentdetails_accident_reportedto, reportedto);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(accidentdetails_injury_details, injury);
	}

	public void checkDeclaration() throws Exception {
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(checkbox_declaration);
		SeleniumFunc.xpath_GenericMethod_Click(checkbox_declaration);
	}

	public void checkDeclarationForAdhocinvoice() throws Exception {
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(checkboxDeclaration);
		SeleniumFunc.xpath_GenericMethod_Click(checkboxDeclaration);
	}

	public void beginTheCase(String queueValue) throws Exception {
		SeleniumFunc.waitFor(10);
		if(SeleniumFunc.getElements(beginCase).size()>0)
			SeleniumFunc.xpath_GenericMethod_Click(beginCase);
		else
			openCaseFromUnderwriterQueue(queueValue);
	}

	public void approveRequest() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(approveRequest);
		SeleniumFunc.xpath_GenericMethod_Click(reOpen_Submit_btn);
	}

	public void filterTheRecordAndSubmit() throws Exception {
		SeleniumFunc.waitFor(12);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(6);
		try{
			SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath("//table[@id='gridLayoutTable']/tbody[@id='gridTableBody']/tr/th[8]//div/a[@title='Open Menu']")));
		}catch(Exception e){
			SeleniumFunc.xpath_GenericMethod_Click("//table[@id='gridLayoutTable']/tbody[@id='gridTableBody']/tr/th[8]//div/a[@title='Open Menu']");
		}
		SeleniumFunc.waitForElement(By.xpath(uw_applyfilter),60);
		SeleniumFunc.xpath_GenericMethod_Click(uw_applyfilter);
		SeleniumFunc.waitForElement(By.xpath(UW_search_text),60);
		System.out.println("The case id is:----"+TestBase.currentClaimNumber+"------------------");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(UW_search_text, TestBase.currentClaimNumber);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(UW_search_text,"MAI-94");
		SeleniumFunc.xpath_GenericMethod_Click(UW_search_applyfilter);

		Thread.sleep(30000);
		SeleniumFunc.clickElementUsingActions(By.xpath(uw_clickcase));

}

	public void manualProcessingReasonAndSubmit(String reason) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click("//textarea[@name='$PpyWorkPage$pProcessingReason']");
		SeleniumFunc.xpath_GenericMethod_Sendkeys("//textarea[@name='$PpyWorkPage$pProcessingReason']","test");
		SeleniumFunc.xpath_GenericMethod_Click(loss_Calender);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
	}

	public void clickApprove() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(approve_button);
	}

	public void clickReject() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(reject_button);
	}

	public void clickFinishButton() throws Exception {
		SeleniumFunc.waitFor(2);
//		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
		SeleniumFunc.clickElementUsingActions(By.xpath(finish_button));
		SeleniumFunc.waitFor(6);
//		SeleniumFunc.waitFor(60);
//		String re = xpath_Genericmethod_getElementText(final_case_statuss);
//		WebElement finalStatus = driver.findElement(By.xpath("//div[2]/span[@class='badge_text'][last()]"));
//		SeleniumFunc.waitForElement(finalStatus,200);
//		while(!xpath_Genericmethod_getElementText(final_case_statuss).equalsIgnoreCase("RESOLVED-COMPLETED")) {
//			clickRefereshInClaimCompleteion("");
//		}
	}
	public void clickFinishButtonWithOutRefresh() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
		SeleniumFunc.waitFor(8);
		while(!xpath_Genericmethod_getElementText(final_case_statuss).equalsIgnoreCase("RESOLVED-COMPLETED")) {
			clickRefereshInClaimCompleteion();
			SeleniumFunc.waitFor(2);
		}
	}

	public void clickRefereshInClaimCompleteion() throws Exception {
		SeleniumFunc.jsClick(ActionButtonInClaimCompletion);
		SeleniumFunc.jsClick(RefershButton);

	}

	public void clickRefereshInClaimCompleteion(String str) throws Exception {

		SeleniumFunc.getRefresh();
		SeleniumFunc.waitFor(10);
	}

	public void validatePolicyNumberinCFandCUFiles(String policy,String CFpolicy) throws Exception {
		String policynumber=SeleniumFunc.getElement(By.xpath(policynumer)).getText();
		SeleniumFunc.xpath_GenericMethod_Click(claimnumber_link);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click("//li[contains(@aria-label,'CU')]//span[@id='close']");
		String CFpolicynumber=SeleniumFunc.getElement(By.xpath(Cf_policynumer)).getText();
		System.out.println("String: "+policynumber+" CFpolicynumeer:"+ CFpolicynumber  );
		Assert.assertEquals(CFpolicynumber,CFpolicy);
	}
	public void searchAndOpenClaimUnit() throws Exception {
//		TestBase.currentClaimNumber="21-2056";
		SeleniumFunc.waitFor(5);
		SeleniumFunc.click(By.xpath(claims_triage_workbasket_filter));
//		SeleniumFunc.waitFor(2);
//		SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
		SeleniumFunc.waitFor(10);
		SeleniumFunc.click(By.xpath(search_filter_textbox_worklist));
//		SeleniumFunc.waitForElementToClikable(By.xpath(search_filter_textbox_worklist),5);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(search_filter_textbox_worklist,TestBase.currentClaimNumber);
//		 SeleniumFunc.xpath_GenericMethod_Sendkeys(search_filter_textbox_worklist,"20-0054");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.click(By.xpath(apply_button));
		SeleniumFunc.waitFor(6);
		SeleniumFunc.xpath_GenericMethod_Click(first_case_worklist);
		SeleniumFunc.waitFor(2);

	}

	public void searchAndOpenTheClaimUnit() throws Exception {
		SeleniumFunc.waitFor(5);
		By claimNumbr = By.cssSelector(".workarea-view-scroll-wrapper:nth-child(2) table th:nth-child(3) div[name='BASE_REF']>a");
		SeleniumFunc.clickElementUsingActions(claimNumbr);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.click(By.xpath(search_filter_textbox_worklist));
//		SeleniumFunc.waitForElementToClikable(By.xpath(search_filter_textbox_worklist),5);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(search_filter_textbox_worklist,TestBase.currentClaimNumber);
//		 SeleniumFunc.xpath_GenericMethod_Sendkeys(search_filter_textbox_worklist,"20-0054");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.click(By.xpath(apply_button));
		SeleniumFunc.waitFor(10);
		List<WebElement> claimsList = driver.findElements(By.xpath("//td[@aria-describedby='Case ID']/following-sibling::td[@aria-describedby='Claim number']/span"));
		System.out.println("The size is------------>: "+claimsList.size());
		if(claimsList.size()==1){
//		WebElement exp = driver.findElement(By.xpath("//td[@aria-describedby='Case ID']/following-sibling::td[@aria-describedby='Claim number']/span"));
//		String claimNumb = TestBase.currentClaimNumber;
//		if(exp.isDisplayed()){
			System.out.println("List not filtered and going to select the only record!");
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(first_Claim_Number );
		}else{
			System.out.println("List got not filtered");
		}
		SeleniumFunc.waitFor(2);

	}
	
	public void validateClaimSLA(String expSLA) throws Exception {
			Assert.assertEquals(reviewdate, expSLA);
	}
	
	public void CompleteClaimReview() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(ClaimReview_reserverEnsure_Yes);
		SeleniumFunc.xpath_GenericMethod_Click(ClaimReview_dataAccurate_Yes);
		SeleniumFunc.xpath_GenericMethod_Click(ClaimReview_clientComnuicaation_date);
		selectDate("24/May/2021");
		SeleniumFunc.xpath_GenericMethod_Click(ClaimReview_claimReadyToClose_Yes);
		SeleniumFunc.name_GenericMethod_Sendkeys(ClaimReview_comments,"Claim review is done");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(ClaimReview_resolvebtn);
	}
	public void searchAndOpenClaimfromMyreviewTab() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(ClaimNumberTh_myreviewtab);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(filterIcon_myreviewtab);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
		SeleniumFunc.waitFor(12);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(search_filter_textbox_worklist, TestBase.currentClaimNumber);
//		 SeleniumFunc.xpath_GenericMethod_Sendkeys(search_fileter_textbox_myreview,"21-0546");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(apply_button_worklist);
		SeleniumFunc.waitFor(4);
		reviewdate=SeleniumFunc.getElementText(claimreview_due_date);
		System.out.println("Expected Review"+reviewdate);
		SeleniumFunc.xpath_GenericMethod_Click(searchresult_case_reviewtab);
		SeleniumFunc.waitFor(2);

	}

	public void OpenClaimUnitRequired(String claimsUnits) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(caseID_sort);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(filter_worklist);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
		SeleniumFunc.waitFor(2);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(search_filter_textbox_worklist, claimsUnits);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(apply_button_worklist);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(first_case_worklist);
		SeleniumFunc.waitFor(2);

	}

	public void openMyWork() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(left_toggle);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(my_work);
	}
	
	public void openMyReviewCases() throws Exception {
		
		SeleniumFunc.xpath_GenericMethod_Click(vima_logo);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(my_review_tab);
	}
	
	public void createAdhocInvoice(String orgName) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(left_toggle);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(new_ham);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(adhocInvoice);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(orgNameField, orgName);
		SeleniumFunc.xpath_GenericMethod_Click(Search_findButton);
	}

	public void selectOrgAndContact() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(Search_searchrestult);
		SeleniumFunc.xpath_GenericMethod_Click(btnContinue);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(contact);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
	}

	public void addDetailsByClickingOnAddNewLineLink(String policyNumber, String Product, String Premium, String GST, String Stamp) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(addNewLinkXpath);
		SeleniumFunc.waitForElementToClikable(By.name(policyNumbrs),4);
		SeleniumFunc.name_GenericMethod_Sendkeys(policyNumbrs, policyNumber);
		SeleniumFunc.name_GenericMethod_Sendkeys(productNames, Product);
		SeleniumFunc.name_GenericMethod_Sendkeys(BasePremium, Premium);
		SeleniumFunc.name_GenericMethod_Click(GSTValue);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(GSTValue, GST);
		SeleniumFunc.name_GenericMethod_Click(StampDuty);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(StampDuty, Stamp);
		SeleniumFunc.name_GenericMethod_Click(justificationComments);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(justificationComments, Product);
		SeleniumFunc.xpath_GenericMethod_Click(btnContinue);
	}

	public void createEventCategory(String eventname) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(left_toggle);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(new_ham);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(create_event_cat);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(event_name, eventname);
		SeleniumFunc.xpath_GenericMethod_Click(submit_event_cat);
	}

	public void clientSearchForOrganisation(String orgname, String expectedorgunit) throws Exception {

		// claims = new Claims(TestBase.getDriver());
		// claims.sendAppObject(claims);
		SeleniumFunc.xpath_GenericMethod_Click(left_toggle);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(client_search);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Sendkeys(org_name, orgname);
		SeleniumFunc.xpath_GenericMethod_Click(org_search_button);

		String org_bunit_value = xpath_Genericmethod_getElementText(org_link);

		SeleniumFunc.writeToAssertionFile(
				"Expected bunit : " + expectedorgunit + " , Actual bunit : " + org_bunit_value,
				(expectedorgunit.equals(org_bunit_value) ? "PASS" : "FAIL"));
		Assert.assertTrue(org_bunit_value.equals(expectedorgunit));

	}

	public void clientSearchForContact(String firstname, String expectedcontactid) throws Exception {

		// claims = new Claims(TestBase.getDriver());
		// claims.sendAppObject(claims);

		SeleniumFunc.xpath_GenericMethod_Click(client_search);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(contact_tab);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Sendkeys(client_firstname, firstname);
		SeleniumFunc.xpath_GenericMethod_Click(contact_search_button);

		String contact_ID_value = xpath_Genericmethod_getElementText(contact_id_link);

		SeleniumFunc.writeToAssertionFile(
				"Expected contact id : " + expectedcontactid + " , Actual contact id : " + contact_ID_value,
				(expectedcontactid.equals(contact_ID_value) ? "PASS" : "FAIL"));
		Assert.assertTrue(contact_ID_value.equals(expectedcontactid));

	}

	public void claimsOfficerTriageClaims() throws Exception {

		// SeleniumFunc.waitFor(5);
		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		OpenClaimFromQueue("Triage Claims");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(triage_closing_reason, "Closed");
		Thread.sleep(1000);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

	}

	public void triageClaimsOfficerEditClaimFile(String expectedstatus) throws Exception {

		// SeleniumFunc.waitFor(5);
		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		OpenClaimFromQueue("Triage Claims");
//		SeleniumFunc.xpath_GenericMethod_Click(triage_actions_button);
		SeleniumFunc.click(By.xpath(triage_actions_button));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.click(By.xpath(edit_claim_data));
		SeleniumFunc.waitFor(2);
		// claims.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(edit_claim_reason);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_claim_reason, "Need to update");
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

	}

	public void editClaimFileFromtriageClaimsOfficer(String org, String exStatus) throws Exception {
		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		OpenClaimFromTheQueue("Triage Claims",org);
//		SeleniumFunc.xpath_GenericMethod_Click(triage_actions_button);
		SeleniumFunc.waitFor(2);
//		SeleniumFunc.click(By.xpath(triage_actions_button));
		SeleniumFunc.xpath_GenericMethod_Click(triage_actions_new_button);
//		SeleniumFunc.waitFor(2);
		SeleniumFunc.click(By.xpath(edit_claim_data));
		SeleniumFunc.waitFor(2);
		// claims.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(edit_claim_reason);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_claim_reason, "Need to update");
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
		SeleniumFunc.waitFor(8);
	}

	public void triageClaimsOfficerCancelClaimFile() throws Exception {

		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		OpenClaimFromQueue("Triage Claims");

		SeleniumFunc.xpath_GenericMethod_Click(triage_actions_button);

		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(lnkCanclefromAction);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingValuebyclickingOnDropdown(drpdwnRecardShecudle,
				"Long Tail Claims");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtCancelReason, "Testing");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

	}

	public void cancelClaimFileFromtriageClaimsOfficer(String org) throws Exception {
		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		OpenClaimFromTheQueue("Triage Claims",org);
		SeleniumFunc.xpath_GenericMethod_Click(triage_actions_button);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(lnkCanclefromAction);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingValuebyclickingOnDropdown(drpdwnRecardShecudle,
				"Long Tail Claims");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtCancelReason, "Testing");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
	}

	public void triageClaimsOfficerWithDrawnClaimFile() throws Exception {

		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(btnWithDraw);
		SeleniumFunc.xpath_GenericMethod_Click(btnWithDraw);

		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(lnkWithdraw);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(btnContinue);

	}

	public void triageClaimsOfficerDupCheck(String expectedstatus, String finalstatus) throws Exception {

		// SeleniumFunc.waitFor(5);

		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		OpenClaimFromQueue("Triage Claims");
//		String casestatusscreen = xpath_Genericmethod_getElementText(pending_triage_status);
//		Assert.assertEquals(casestatusscreen,expectedstatus);
//		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

	}

	public void triageClaimsOfficerDupCheck_1(String expectedstatus, String finalstatus) throws Exception {

		// SeleniumFunc.waitFor(5);
		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		OpenClaimFromQueue("Triage Claims");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(claimClasification_Cause1,"Theft / Lost");
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(claimClasification_outcome1,"Theft / Misplaced");
//		SeleniumFunc.waitFor(1);
//		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(claimClasification_Cause2,"Theft");
//		SeleniumFunc.waitFor(1);
//		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(claimClasification_outcome2,"Cash");
		String casestatusscreen = xpath_Genericmethod_getElementText(pending_triage_status_1);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit_1);
	}


	public void triageClaimsOfficerDuplicateCheck_1(String expectedstatus, String orgname) throws Exception {

		SeleniumFunc.waitFor(6);
		//The below names are different in QA & UAT. Qa Triage Claims and UAT Triage claims
		// When I changed to UAT env, Updated the below name. previously, It was 'Triage Claims'. C was capital!
		// When I changed to UAT env, Updated the below name. previously, It was 'Triage claims'. C was Small!
		OpenClaimFromTheQueue("Triage Claims",orgname);
		SeleniumFunc.waitFor(2);
		try{
			SeleniumFunc.name_SelectFromDropdownUsingVisibleText(claimClasification_Cause1,"Theft / Lost");
			SeleniumFunc.waitFor(1);
			SeleniumFunc.name_SelectFromDropdownUsingVisibleText(claimClasification_outcome1,"Theft / Misplaced");

		}catch(Exception e){
			e.getMessage();
		}
		fetchClaimsID();
		String casestatusscreen = xpath_Genericmethod_getElementText(pending_triage_status_1);
		verifyPendingTriageStatus(casestatusscreen,expectedstatus);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit_1);
	}

	public void clickOnFirstRecord(String expectedstatus) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(firstCaseNumber);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(claimClasification_Cause1,"Theft / Lost");
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(claimClasification_outcome1,"Theft / Misplaced");
		fetchClaimsID();
		String casestatusscreen = xpath_Genericmethod_getElementText(pending_triage_status_1);
		verifyPendingTriageStatus(casestatusscreen,expectedstatus);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit_1);
	}

	public void validateErrorMessage(String actualMsg) throws Exception {
		String expectedMsg = SeleniumFunc.getElementTextWithXpath("//div[@aria-label='Error message']//div[2]/div");
		System.out.println("Actual Msg******** "+actualMsg);
		System.out.println("Expected Msg******** "+expectedMsg);
		if(!expectedMsg.contains(actualMsg)){
			Assert.assertFalse(true);
		}
	}

	public void clickEditFromActions() throws Exception {
		SeleniumFunc.click(By.xpath(triage_actions_button));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.click(By.xpath(edit_claim_data));
	}

	public void provideReason(String reason) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(edit_claim_reason);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_claim_reason, reason);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
	}

	public void estimateWorkbasketApprove(String expectedstatus, String finalstatus) throws Exception {

		// SeleniumFunc.waitFor(5);
		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(queue_dropdown);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(estimate_approval_workbasket);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(ceo_claimfile_id);
		// SeleniumFunc.xpath_GenericMethod_HoverOnDemoScreenPops(finance_caseid_filter);
		SeleniumFunc.waitFor(4);
		// SeleniumFunc.xpath_GenericMethod_Click(claimfileid_filter);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(claimfileid_filter_2)));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text, TestBase.currentClaimFileID);
		SeleniumFunc.xpath_GenericMethod_Click(claims_apply);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(claims_case_link);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		String casestatusscreen = xpath_Genericmethod_getElementText(pending_triage_statusse);

		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + expectedstatus + " , Actual status : " + casestatusscreen,
				(expectedstatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreen.equals(expectedstatus));

		SeleniumFunc.xpath_GenericMethod_Sendkeys(review_comments_estimate, "Approved");
		SeleniumFunc.xpath_GenericMethod_Click(estimate_approve_button);

		SeleniumFunc.switchToDefaultContent();
		String casestatusscreenfinal = xpath_Genericmethod_getElementText(portfolio_manager_pending_investigation);

		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + finalstatus + " , Actual status : " + casestatusscreenfinal,
				(finalstatus.equals(casestatusscreenfinal) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreenfinal.equals(finalstatus));

		// claims.switchToDefaultContent();

		/*
		 * SeleniumFunc.xpath_GenericMethod_Sendkeys(triage_closing_reason, "Closed");
		 * Thread.sleep(1000);
		 * SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
		 */

	}

	public void clickNotADuplicate() throws Exception {
		SeleniumFunc.waitFor(10);
//		SeleniumFunc.waitForElementToClikable(By.xpath(not_a_duplicate),60);
		SeleniumFunc.xpath_GenericMethod_Click(not_a_duplicate);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

	}

	public void clickNotADuplicate_1() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(not_a_duplicate);
		// SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit_1);

	}

	public void clickResolveAsDuplicate() throws Exception {
//		SeleniumFunc.waitFor(6);
		SeleniumFunc.waitForElement(By.xpath(resolve_claim_as_duplicate),40);
		SeleniumFunc.xpath_GenericMethod_Click(resolve_claim_as_duplicate);

		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(duplicateWith_dropdown, 1);

		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

	}

	public void clickResolveAsDuplicate_1() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(resolve_claim_as_duplicate);

		// SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(duplicateWith_dropdown,1);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(duplicateWith_dropdown_1, 1);

		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit_1);

	}

	public void validateClaimunitCompletion(String expectedstatus) throws Exception {

//		String casestatusscreen = xpath_Genericmethod_getElementText(status_pending_claimunit_comp);
		try{
			SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
			SeleniumFunc.waitInMilliSeconds(2);
		}catch(Exception e){
		}

		int count=0;
		while(!xpath_Genericmethod_getElementText(status_pending_claimunit_comp).equalsIgnoreCase(expectedstatus)) {
			if(count>160) {
			break;}
			System.out.println(count);
			SeleniumFunc.getRefresh();
			count=count+6;
			SeleniumFunc.waitInMilliSeconds(2);
		}

	}

	public void validateClaimunitCompletion_1(String expectedstatus) throws Exception {

		String casestatusscreen = xpath_Genericmethod_getElementText(status_pending_claimunit_comp_1);
		System.out.println("Expected Status --------- "+expectedstatus);
		System.out.println("Actual Status --------- "+casestatusscreen);
		Assert.assertEquals(casestatusscreen,expectedstatus);

	}

	public void validateResolvedDuplicate(String expectedstatus) throws Exception {

		String casestatusscreen = xpath_Genericmethod_getElementText(status_resolved_duplicate);

		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + expectedstatus + " , Actual status : " + casestatusscreen,
				(expectedstatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreen.equals(expectedstatus));

	}

	public void validateResolvedDuplicate_1(String expectedstatus) throws Exception {

		// String casestatusscreen =
		// xpath_Genericmethod_getElementText(status_resolved_duplicate);
		String casestatusscreen = xpath_Genericmethod_getElementText(status_resolved_duplicate_1);

		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + expectedstatus + " , Actual status : " + casestatusscreen,
				(expectedstatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreen.equals(expectedstatus));

	}

	public void claimsPortfolioManagerTriageClaims(String usertotransfer, String expectedstatus,
			String statusaftertransfer) throws Exception {

		// SeleniumFunc.waitFor(5);
		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(queue_dropdown);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(triage_claims);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(ClaimnumerTH1);
		// SeleniumFunc.xpath_GenericMethod_HoverOnDemoScreenPops(finance_caseid_filter);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(claimnumerFilterIcon)));
		SeleniumFunc.waitFor(2);		
		SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text, TestBase.currentClaimNumber);
		SeleniumFunc.xpath_GenericMethod_Click(claims_apply);
		SeleniumFunc.xpath_GenericMethod_Click(claims_case_link);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		String casestatusscreen = xpath_Genericmethod_getElementText(pending_triage_status);

		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + expectedstatus + " , Actual format : " + casestatusscreen,
				(expectedstatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreen.equals(expectedstatus));

		SeleniumFunc.xpath_GenericMethod_Sendkeys(user_to_transfer, usertotransfer);
		SeleniumFunc.xpath_GenericMethod_Click(transfer_button);

		String casestatusscreenaftertransfer = xpath_Genericmethod_getElementText(
				portfolio_manager_pending_investigation);

		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + statusaftertransfer + " , Actual format : " + casestatusscreenaftertransfer,
				(expectedstatus.equals(casestatusscreenaftertransfer) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreenaftertransfer.equals(statusaftertransfer));

	}

	public void claimsPortfolioManagerTriageClaims_1(String usertotransfer, String expectedstatus,
			String statusaftertransfer) throws Exception {

		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		OpenClaimFromQueue("Unassigned Claims");
		String casestatusscreen = SeleniumFunc.xpath_Genericmethod_getElementText(pending_triage_status_1_port);
		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + expectedstatus + " , Actual format : " + casestatusscreen,
				(expectedstatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		// Assert.assertTrue(casestatusscreen.equals(expectedstatus));
		SeleniumFunc.waitFor(10);
		SeleniumFunc.xpath_GenericMethod_Click(user_to_transfer);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(user_to_transfer, usertotransfer);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(Click_On_Transfer);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(transfer_button);

		String casestatusscreenaftertransfer = SeleniumFunc
				.xpath_Genericmethod_getElementText(portfolio_manager_pending_investigation_1);
		System.out.println("Case status screen value " + casestatusscreenaftertransfer);
		System.out.println("Expected case status:" + statusaftertransfer);
		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + statusaftertransfer + " , Actual format : " + casestatusscreenaftertransfer,
				(expectedstatus.equals(casestatusscreenaftertransfer) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreenaftertransfer.equals(statusaftertransfer));

	}
	public void claimsPortfolioManagerTriageClaims_2(String usertotransfer, String expectedstatus,
			String statusaftertransfer) throws Exception {

		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		OpenClaimFromQueue("Unassigned Claims");
		String casestatusscreen = SeleniumFunc.xpath_Genericmethod_getElementText(pending_triage_status_1_port);
		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + expectedstatus + " , Actual format : " + casestatusscreen,
				(expectedstatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		// Assert.assertTrue(casestatusscreen.equals(expectedstatus));
		SeleniumFunc.waitFor(10);
		SeleniumFunc.xpath_GenericMethod_Click(user_to_transfer);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(user_to_transfer, usertotransfer);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(Click_On_Transfer);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(transfer_button);

		String casestatusscreenaftertransfer = SeleniumFunc
				.xpath_Genericmethod_getElementText(portfolio_manager_pending_investigation_1);
		System.out.println("Case status screen value " + casestatusscreenaftertransfer);
		System.out.println("Expected case status:" + statusaftertransfer);
		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + statusaftertransfer + " , Actual format : " + casestatusscreenaftertransfer,
				(expectedstatus.equals(casestatusscreenaftertransfer) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreenaftertransfer.equals(statusaftertransfer));

	}
	public void verifyStatusClaimsOfficer(String expstatus) throws Exception {
		String casestatusscreenaftertransfer = xpath_Genericmethod_getElementText(
				portfolio_manager_pending_investigation);
		System.out.println("Expected status: "+expstatus);
		System.out.println("Recieved status: "+casestatusscreenaftertransfer);
		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + expstatus + " , Actual status : " + casestatusscreenaftertransfer,
				(expstatus.equals(casestatusscreenaftertransfer) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreenaftertransfer.equals(expstatus));
		try {
			SeleniumFunc.xpath_GenericMethod_Click(back_from_estimatde);
			SeleniumFunc.waitFor(3);
			SeleniumFunc.xpath_GenericMethod_Click(back_from_estimate);
		} catch (Exception e) {
		}

	}

	public void verifyStatusPortMan(String expstatus) throws Exception {
		String casestatusscreenaftertransfer = xpath_Genericmethod_getElementText(pending_triage_status);

		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + expstatus + " , Actual status : " + casestatusscreenaftertransfer,
				(expstatus.equals(casestatusscreenaftertransfer) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreenaftertransfer.equals(expstatus));

	}

	public void claimsPortfolioManagerTriageClaimsSkillBasedTest(String invaliduser, String usertotransfer,
			String expectedstatus, String statusaftertransfer) throws Exception {

		// SeleniumFunc.waitFor(5);
		SeleniumFunc.waitFor(2);
		OpenClaimFromQueue("Unassigned Claims");

		SeleniumFunc.waitFor(2);
		String casestatusscreen = xpath_Genericmethod_getElementText(pending_triage_statuss);

//		SeleniumFunc.writeToAssertionFile(
//				"Expected case status : " + expectedstatus + " , Actual format : " + casestatusscreen,
//				(expectedstatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
//		Assert.assertTrue(casestatusscreen.equals(expectedstatus));
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(user_to_transfer);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(user_to_transfer, invaliduser);
		SeleniumFunc.xpath_GenericMethod_Click(transfer_button);
		String errormessage = StringUtils.EMPTY;
		try {
			errormessage = xpath_Genericmethod_getElementText(triage_claims_officer_error_message);
		} catch (Exception e) {
			Assert.fail("Error message not found");
		}

		System.out.println(errormessage);
		SeleniumFunc.xpath_GenericMethod_Clear(user_to_transfer);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(user_to_transfer, usertotransfer);
		SeleniumFunc.xpath_GenericMethod_Click(transfer_button);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();

		String casestatusscreenaftertransfer = xpath_Genericmethod_getElementText(
				portfolio_manager_pending_investigation);
		System.out.println(casestatusscreenaftertransfer);
//		SeleniumFunc.writeToAssertionFile(
//				"Expected case status : " + statusaftertransfer + " , Actual status : " + casestatusscreenaftertransfer,
//				(expectedstatus.equals(casestatusscreenaftertransfer) ? "PASS" : "FAIL"));
//		Assert.assertTrue(casestatusscreenaftertransfer.equals(statusaftertransfer));

	}

	public void claimsPortfolioManagerTriageClaimsSkillsBased(String invaliduser, String usertotransfer,
																 String expectedstatus, String statusaftertransfer) throws Exception {

		SeleniumFunc.waitFor(2);
//		OpenClaimFromQueue("Unassigned Claims");
		OpenClaimNUmberFromQueue("Unassigned Claims");

		SeleniumFunc.waitFor(2);//In below line, Assert with expected status (Pending Triage)
		String casestatusscreen = xpath_Genericmethod_getElementText(pending_triage_statuss);
		Assert.assertEquals(casestatusscreen,expectedstatus);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(user_to_transfer);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(user_to_transfer, invaliduser);
		SeleniumFunc.xpath_GenericMethod_Click(transfer_button);
		SeleniumFunc.waitFor(2);
		String errormessage = StringUtils.EMPTY;
		try {
			errormessage = xpath_Genericmethod_getElementText(triage_claims_officer_error_message);
		} catch (Exception e) {
			Assert.fail("Error message not found");
		}
		SeleniumFunc.waitFor(2);
		System.out.println(errormessage);
		SeleniumFunc.xpath_GenericMethod_Clear(user_to_transfer);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(user_to_transfer, usertotransfer);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(transfer_button);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();

//		String casestatusscreenaftertransfer = xpath_Genericmethod_getElementText(
//				portfolio_manager_pending_investigation);
//		System.out.println(casestatusscreenaftertransfer);
	}
	public void addEstimateHigherLimit(String payableamount, String outstandingamount, String recoverableamount)
			throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(edit_estimate);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(update_estimate);
		SeleniumFunc.switchToDefaultContent();
//		SeleniumFunc.xpath_GenericMethod_Clear(edit_estimate_payable);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_estimate_payable, payableamount);
		SeleniumFunc.xpath_GenericMethod_Clear(edit_estimate_outstanding);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_estimate_outstanding, payableamount);
//		SeleniumFunc.xpath_GenericMethod_Clear(edit_estimate_recoverable);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_estimate_recoverable, recoverableamount);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingValuebyclickingOnDropdown(
				estimate_change_reason_dropdown, "File review");
		SeleniumFunc.xpath_GenericMethod_Click(submit_estimate);
		SeleniumFunc.switchToDefaultContent();

		String estimate_message = xpath_Genericmethod_getElementText(estimate_exceed_message);

		SeleniumFunc.writeToAssertionFile(
				"Expected message : "
						+ "Estimate amount entered exceeded your delegation. The estimate has been sent for approval."
						+ " , Actual message : " + estimate_message,
				("Estimate amount entered exceeded your delegation. The estimate has been sent for approval."
						.equals(estimate_message) ? "PASS" : "FAIL"));
		Assert.assertTrue(estimate_message
				.equals("Estimate amount entered exceeded your delegation. The estimate has been sent for approval."));
		SeleniumFunc.xpath_GenericMethod_Click(back_from_estimate);

	}

	public void addEstimate(String payableamount, String outstandingamount, String recoverableamount) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(edit_estimate);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(update_estimate);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Clear(edit_estimate_payable);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_estimate_payable, payableamount);

		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingValuebyclickingOnDropdown(
				estimate_change_reason_dropdown, "File review");
		SeleniumFunc.xpath_GenericMethod_Click(submit_estimate);
		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(back_from_estimate);
		SeleniumFunc.waitFor(2);
		// SeleniumFunc.xpath_GenericMethod_Click(back_from_estimate);

	}

	public void addEstimatee(String payableamount, String outstandingamount, String recoverableamount)
			throws Exception {

		// SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

		// claims.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(edit_estimate);
		SeleniumFunc.xpath_GenericMethod_Click(update_estimate);
		SeleniumFunc.xpath_GenericMethod_Clear(edit_estimate_payable);
//		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_estimate_payable, payableamount);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(estimate_change_reason_dropdown);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_estimate_outstanding, outstandingamount);
//		SeleniumFunc.xpath_GenericMethod_Clear(edit_estimate_recoverable);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_estimate_recoverable, recoverableamount);
		SeleniumFunc.waitFor(10);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingValuebyclickingOnDropdown(
				estimate_change_reason_dropdown, "File review");
		SeleniumFunc.xpath_GenericMethod_Click(submit_estimate);

		try{
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(settlement_back_btn);
		}catch(Exception e){
			e.getMessage();
		}

		try{
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(close_btn);
		}catch(Exception e) {
			e.getMessage();
		}

//		The below step impacting the flow - Initiate a Claim for GPA and add recoverables reinsurance from claims officer(MLP2)
//		try{
//			SeleniumFunc.waitFor(2);
//			SeleniumFunc.xpath_GenericMethod_Click(rec_finance_open_btn);
//		}catch(Exception e) {
//			e.getMessage();
//		}
	}

	public void associteClaimWithEvent(String eventname) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(actions_button);
		SeleniumFunc.xpath_GenericMethod_Click(event_management_option);
		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Sendkeys(event_name_text, eventname);
		SeleniumFunc.xpath_GenericMethod_Click(event_search);
		SeleniumFunc.xpath_GenericMethod_Click(event_select);
		SeleniumFunc.xpath_GenericMethod_Click(link_event);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(actions_button);
		SeleniumFunc.xpath_GenericMethod_Click(event_management_option);
		SeleniumFunc.switchToDefaultContent();

		String casestatusscreenaftertransfer = xpath_Genericmethod_getElementText(verify_Event_link);

		SeleniumFunc.writeToAssertionFile(
				"Expected status : " + eventname + " , Actual status : " + casestatusscreenaftertransfer,
				(casestatusscreenaftertransfer.contains("Pending-Approval") ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreenaftertransfer.contains(eventname));

	}

	public void addRecoverableThirdParty(String recoveryamount, String recoverabletype) throws Exception {

		// SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(edit_recoverable);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(add_recoverable);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(select_recovery_type,
				recoverabletype);
		SeleniumFunc.xpath_GenericMethod_Clear(recovery_amount);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(recovery_amount, recoveryamount);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.executeScript("arguments[0].click();", driver.findElement(By.xpath(party_type_individual)));

		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(first_name_recov, "first");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(last_name_recov, "last");
		SeleniumFunc.xpath_GenericMethod_Click(submit_recovery);
		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(back_from_estimate);

	}

	public void addRecoverableThirdPartyNew(String recoveryamount, String recoverabletype) throws Exception {

		// SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(edit_recoverable);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(add_recoverable);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(select_recovery_type,
				recoverabletype);
		SeleniumFunc.xpath_GenericMethod_Clear(recovery_amount);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(recovery_amount, recoveryamount);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtrecovery_comments, "Test");
		SeleniumFunc.xpath_GenericMethod_Click(SBT);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(lnkInitiate);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtrecoveryDesc, "Test");
		SeleniumFunc.xpath_GenericMethod_Click(radpartyType);
		// SeleniumFunc.executeScript("arguments[0].click();",
		// driver.findElement(By.xpath(party_type_individual)));

		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(first_name_recov, "first");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(last_name_recov, "last");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtrecoveryEmail, "a@b.com");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtrecoveryAmt, "200");
		SeleniumFunc.xpath_GenericMethod_Click(btnintiateRecovery);
		SeleniumFunc.switchToDefaultContent();

	}

	public void addRecoverableReinsurance(String recoveryamount, String recoverabletype) throws Exception {

		// SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(edit_recoverable);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(add_recoverable);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(select_recovery_type,
				recoverabletype);
		SeleniumFunc.xpath_GenericMethod_Clear(recovery_amount);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(recovery_amount, recoveryamount);
//		SeleniumFunc.xpath_GenericMethod_Click(reinsurance_apply);
		try{
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Reinsurer_tfield,"test");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(comments_tfield,"VMIA");
		}catch(Exception e){
			e.getMessage();
		}

		SeleniumFunc.xpath_GenericMethod_Click(submit_recovery);
		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(back_from_estimate);

	}

	public void addEstimateAndReviewClaimUnit(String payableamount, String outstandingamount, String recoverableamount,
			String clientdecision, String awaitdecision) throws Exception {

		// SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(edit_estimate);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(update_estimate);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Clear(edit_estimate_payable);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_estimate_payable, payableamount);
//		SeleniumFunc.xpath_GenericMethod_Click(edit_estimate_outstanding);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_estimate_outstanding, outstandingamount);
//		SeleniumFunc.xpath_GenericMethod_Clear(edit_estimate_recoverable);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_estimate_recoverable, recoverableamount);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingValuebyclickingOnDropdown(
				estimate_change_reason_dropdown, "File review");
		SeleniumFunc.xpath_GenericMethod_Click(submit_estimate);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(back_from_estimatde);

		SeleniumFunc.xpath_GenericMethod_Click(SBT);

		if (clientdecision.equalsIgnoreCase("approve"))

			SeleniumFunc.xpath_GenericMethod_Click(approve_claim_button);

		else if (clientdecision.equalsIgnoreCase("deny")) {

			SeleniumFunc.xpath_GenericMethod_Click(deny_claim_button);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(denial_reasom,
					"Damage not covered");
			SeleniumFunc.xpath_GenericMethod_Click(settlement_date);
			SeleniumFunc.xpath_GenericMethod_Click(today_link);
		}

		else if (clientdecision.equalsIgnoreCase("awaiting")) {

			SeleniumFunc.xpath_GenericMethod_Click(awaiting_action_claim_button);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(await_reason,
					awaitdecision);
			SeleniumFunc.xpath_GenericMethod_Click(settlement_date);
			SeleniumFunc.xpath_GenericMethod_Click(today_link);
		}

		else
			System.out.println("Valid client decision not provided");

		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(App);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(dateed);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Cue);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

		// SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
	}

	public void makeDecisionClaimUnit(String clientdecision, String awaitdecision) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(SBT);
		try{
			SeleniumFunc.waitFor(2);
			SeleniumFunc.name_SelectFromDropdownUsingVisibleText(claimClasification_Cause2,"Misplaced / lost");
			SeleniumFunc.name_SelectFromDropdownUsingVisibleText(claimClasification_outcome2,"Organisational asset");
		}catch(Exception e){

		}
		if (clientdecision.equalsIgnoreCase("approve"))

			SeleniumFunc.xpath_GenericMethod_Click(approve_claim_button);

		else if (clientdecision.equalsIgnoreCase("deny")) {
			SeleniumFunc.xpath_GenericMethod_Click(deny_claim_button);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(denial_reasom,
					"Damage not covered");
			SeleniumFunc.xpath_GenericMethod_Click(settlement_date);
			SeleniumFunc.xpath_GenericMethod_Click(today_link);
		}

		else if (clientdecision.equalsIgnoreCase("awaiting")) {

			SeleniumFunc.xpath_GenericMethod_Click(awaiting_action_claim_button);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(await_reason,
					awaitdecision);
			SeleniumFunc.xpath_GenericMethod_Click(settlement_date);
			SeleniumFunc.xpath_GenericMethod_Click(today_link);
		}

		else
			System.out.println("Valid client decision not provided");

		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
	}

	public void makeDecisionClaimUnitdeny(String clientdecision) throws Exception {
		SeleniumFunc.waitFor(10);
		SeleniumFunc.xpath_GenericMethod_Click(SBT);

		if (clientdecision.equalsIgnoreCase("approve"))

			SeleniumFunc.xpath_GenericMethod_Click(approve_claim_button);

		else if (clientdecision.equalsIgnoreCase("deny")) {
			SeleniumFunc.xpath_GenericMethod_Click(deny_claim_button);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(denial_reasom,
					"Damage not covered");
			SeleniumFunc.xpath_GenericMethod_Click(settlement_date);
			SeleniumFunc.xpath_GenericMethod_Click(today_link);
		}

		else if (clientdecision.equalsIgnoreCase("awaiting")) {

			SeleniumFunc.xpath_GenericMethod_Click(awaiting_action_claim_button);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(await_reason,
					"Test");
			SeleniumFunc.xpath_GenericMethod_Click(settlement_date);
			SeleniumFunc.xpath_GenericMethod_Click(today_link);
		}

		else
			System.out.println("Valid client decision not provided");

		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
	}

	public void resolveClaimFilee() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(claim_file_link);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(begin_analyse_claim_file);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
		SeleniumFunc.waitFor(2);
	}

	public void resolveClaimFile() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(claim_file_link);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(begin_analyse_claim_file);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(dateed);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Cue);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Per, "Long Tail Claims");
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
	}

	public void resolveClaimFiled() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(claim_file_link);
		SeleniumFunc.switchToDefaultContent();
		// SeleniumFunc.xpath_GenericMethod_Click(begin_analyse_claim_file);
		// claims.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(App);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(dateed);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Cue);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

	}

	public void resolveClaimFilede() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Sendkeys(Per, "Long Tail Claims");
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
	}

	public void closeAllTabs() throws Exception {
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(close_all_anchor);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(close_all_text);

	}

	public void denyClaimAfterPortManagerAppr() throws Exception {

		// claims.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingValuebyclickingOnDropdown(records_schedule,
				"Claims listed in state of Victoria Litigation Register");
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

	}
	
	public void attachFile() {
		
	}

	public void OpenCaseFromFinance(String queue,String claimnumber) throws Exception {
		System.out.println("The claim number is-------->"+claimnumber);
		String caseLink="";
		SeleniumFunc.waitForElementToClikable(By.xpath(vima_logo),20);
		SeleniumFunc.xpath_GenericMethod_Click(vima_logo);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(queue_dropdown);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click("//span[contains(text(),'"+queue+"')]");
		SeleniumFunc.waitFor(2);
		if(queue.equalsIgnoreCase("Recoveries")) {
			SeleniumFunc.xpath_GenericMethod_Click(finance_recoveryid);
			SeleniumFunc.waitFor(3);
			SeleniumFunc.xpath_GenericMethod_Click(finance_recovery_caseid_filter);
			caseLink="//td//a[text()='"+claimnumber+"']";
		}else if(queue.equalsIgnoreCase("Manual payments")) {
			SeleniumFunc.xpath_GenericMethod_Click(finance_manualpayment_casenumber);
			SeleniumFunc.waitFor(3);
			SeleniumFunc.xpath_GenericMethod_Click(finance_manualpayment_fitericon);
			caseLink="//span[text()='"+claimnumber+"']/parent::td/preceding-sibling::td[1]/span/a";

		}
		SeleniumFunc.xpath_GenericMethod_Click(finance_applyfilter);
		SeleniumFunc.waitForElementToClikable(By.xpath(finance_search_text),6);
//		TestBase.recoveryid="REC-133";
		SeleniumFunc.xpath_GenericMethod_Sendkeys(finance_search_text,claimnumber );
		SeleniumFunc.waitFor(6);
		SeleniumFunc.xpath_GenericMethod_Click(finance_apply);
		SeleniumFunc.waitFor(6);
		SeleniumFunc.xpath_GenericMethod_Click(caseLink);
		SeleniumFunc.waitFor(2);
	}

	public void enterclaimPaymentValues(String Paymentmode,String paymentcode,String Paymentval) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Make_payment_pay_btn);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(payment_mode_dropdown,Paymentmode);
		SeleniumFunc.waitFor(2);
		Select select = new Select(driver.findElement(By.xpath("//label[text()='Payment code']/following-sibling::div/select")));
		select.selectByIndex(1);
//		SeleniumFunc.xpath_GenericMethod_Click(payment_code);
		SeleniumFunc.waitFor(2);
//		SeleniumFunc.xpath_selectValueFromDropDownUsingVisibleTextUsingJS(payment_code, "22-Statutory Repayment");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(paymentdescription, "description");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(Make_Payment_invoceno, "description");
		SeleniumFunc.enterTheValueInComboBox(Make_Payment_Payee, "Sibam Paul chowdhury ");
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(payment_method,"Cheque");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payment_amount, Paymentval);
		SeleniumFunc.xpath_GenericMethod_Click(gst_exempt);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Click(make_payment_attachfile);
		SeleniumFunc.clickSelectFileButtonAndUploadTheFile("Claim_check_attachment.txt");
		SeleniumFunc.xpath_SelectFromDropdownUsingVisibleText(Make_Payment_Attach_categoery,"Claim");
		SeleniumFunc.xpath_GenericMethod_Click(Make_payment_attachmentpop_attachbtn);
		SeleniumFunc.waitFor(7);
	}

	public void enterclaimPaymentValuesWithFileAttachment(String Paymentmode,String paymentcode,String Paymentval) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Make_payment_pay_btn);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(payment_mode_dropdown,Paymentmode);
		SeleniumFunc.waitFor(2);
		Select select = new Select(driver.findElement(By.xpath("//label[text()='Payment code']/following-sibling::div/select")));
		select.selectByIndex(1);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(paymentdescription, "description");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(Make_Payment_invoceno, "description");
		SeleniumFunc.enterTheValueInComboBox(Make_Payment_Payee, "Sibam Paul chowdhury ");
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(payment_method,"Cheque");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payment_amount, Paymentval);
		SeleniumFunc.xpath_GenericMethod_Click(gst_exempt);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Click(make_payment_attachfromclaimfile);
		SeleniumFunc.xpath_GenericMethod_Click(checkboxInPopup);
		SeleniumFunc.xpath_GenericMethod_Click(Make_payment_attachmentpop_attachbtn);
		SeleniumFunc.waitFor(7);
	}

	public void makeFinalPayment(String Paymentmode,String paymentcode,String Paymentval,String settlementDate) throws Exception {
		try{
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(rec_finance_open_btn);
		}catch(Exception e) {
			e.getMessage();
		}
		enterclaimPaymentValues(Paymentmode,"22-Statutory Repayment",Paymentval);
		SeleniumFunc.xpath_GenericMethod_Click(chkFinalpayment);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(records_Schedule,"Long Tail Claims");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(paymentSettleDate,settlementDate);
		SeleniumFunc.xpath_GenericMethod_Click(make_payment_button);
		SeleniumFunc.waitFor(4);
//		SeleniumFunc.name_GenericMethod_Click(completed_back_btn);
		SeleniumFunc.xpath_GenericMethod_Click(completed_back_btn);
//		SeleniumFunc.name_GenericMethod_Click(settlement_back_btn);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(finance_close_btn);
	}

	public void makeFinalPaymentWithoutFinalcheckpoint(String Paymentmode,String paymentcode,String Paymentval,String settlementDate) throws Exception {
		enterclaimPaymentValues(Paymentmode,"22-Statutory Repayment",Paymentval);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(make_payment_button);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(completed_back_btn);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(finance_close_btn);
	}

	public void makeFinalPaymentAndUploadFileFromAttachFromClaimFileBtn(String Paymentmode, String paymentcode, String Paymentval,String settlementDate) throws Exception {
		try{
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(rec_finance_open_btn);
		}catch(Exception e) {
			e.getMessage();
		}
		enterclaimPaymentValuesWithFileAttachment(Paymentmode,"22-Statutory Repayment",Paymentval);
		SeleniumFunc.xpath_GenericMethod_Click(chkFinalpayment);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(records_Schedule,"Long Tail Claims");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(paymentSettleDate,settlementDate);
		SeleniumFunc.xpath_GenericMethod_Click(make_payment_button);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(completed_back_btn);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(finance_close_btn);
	}

	public void verifyViewPaymentButton() throws Exception {
		Assert.assertTrue(SeleniumFunc.isDisplayed(driver.findElement(By.xpath(viewPaymentsBtn))));
		SeleniumFunc.xpath_GenericMethod_Click(viewPaymentsBtn);
		try{
			driver.findElement(By.xpath(noPayments));
			System.out.println("There are no payment cases exist");
		}catch(Exception e){
			driver.findElement(By.xpath(withPayments));
			System.out.println("There are payment cases exist");
		}
	}

	public void approvemanualPayments() throws Exception {
//		OpenCaseFromFinance("Manual payments",TestBase.currentClaimNumber);
		OpenCaseFromFinance("Manual payments",TestBase.currentClaimNumber);
//	   OpenCaseFromFinance("Manual payments",Test);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(close_Btn);
//		SeleniumFunc.waitFor(20);
	}

	
	public void makePayment(String Paymentmode,String paymentcode,String Paymentval) throws Exception {
		enterclaimPaymentValues(Paymentmode,paymentcode,Paymentval);

		SeleniumFunc.xpath_GenericMethod_Click(make_payment_button);
		SeleniumFunc.waitFor(8);
//		SeleniumFunc.name_GenericMethod_Click(completed_back_butn);
		SeleniumFunc.xpath_GenericMethod_Click(completed_back_btn);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(finance_close_btn);
	}
	
     public void validateRecoveryHistory(String excrecovery,String exprecoverdes) {
    	 Boolean histeoryexist=false;
         String Recoverid=SeleniumFunc.getElementText(recovery_id);
    	//div[@title='Disclose History']/i[@class='icon icon-openclose']
    	 List<WebElement> recoveryamt=driver.findElements(By.xpath(recovery_table_recamount));
    	 List<WebElement> recoverydescription=driver.findElements(By.xpath(recovery_table_recamount));	
    
    	 for(int i=0;i<recoveryamt.size();i++) {
			 histeoryexist=true;
    		if(recoveryamt.get(i).getText().equalsIgnoreCase(excrecovery)&&recoverydescription.get(i).getText().equalsIgnoreCase(exprecoverdes)) {
    			System.out.println("Recovery history is created as expected");
    			break;
    		}
    	 }
    	 if(histeoryexist==false) throw new InputMismatchException("Recovery history is not created");
     }
     
     public List<WebElement> getRevoeryHistory_recovery_description() {
      
    	 List<WebElement> recoveryamt=driver.findElements(By.xpath(recovery_table_recamount));	
    	 return recoveryamt;
     }
	
	
	public void createrecoveryEstimate(String recoveryEstimationtype,String recoveryestimate) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(recovery_open_btn);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Click(recoverry_add_btn);
		SeleniumFunc.waitFor(2);
////		SeleniumFunc.name_SelectValueByClickingOptions(recoverry_estimatetype, "Reinsurance");
////		SeleniumFunc.waitFor(6);
		SeleniumFunc.name_SelectValueByClickingOptions(recoverry_estimatetype, recoveryEstimationtype);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(recovery_estimated_amt, recoveryestimate);
		SeleniumFunc.name_GenericMethod_Sendkeys(recovery_estimated_comments, "Recovery estimate");
		SeleniumFunc.waitFor(4);
		SeleniumFunc.id_GenericMethod_Click(recovery_estimate_submit_id);
	}
	public void createrecovery(String recesttype,String Parttype,String firstname,String lastname,String recoveramt ) throws Exception {
		SeleniumFunc.waitFor(4);
		SeleniumFunc.name_GenericMethod_Click(recovery_estimate_initiate);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(recovery_estimate_estimatetype, recesttype);
		SeleniumFunc.name_GenericMethod_Sendkeys(recovery_estimate_estimatdes, "test");
//		SeleniumFunc.name_GenericMethod_Click(recovery_estimate_submit_id);
//		if(Parttype.equalsIgnoreCase("business")) SeleniumFunc.name_GenericMethod_Click(recovery_estimatepartytype_business);
//		else SeleniumFunc.name_GenericMethod_Click(recovery_estimatepartytype_Individual);
//		SeleniumFunc.waitFor(1);
//		SeleniumFunc.name_GenericMethod_Sendkeys(recovery_estimate_emailaddress, "abc@c.com");
		//input[@name='$PRecoveryTempPage$pSendEmail']/following-sibling::label[text()='No']
		SeleniumFunc.xpath_GenericMethod_Click(recovery_sendEmail_No);
//		SeleniumFunc.name_GenericMethod_Sendkeys(recovery_estimate_firstname, firstname);
//		SeleniumFunc.name_GenericMethod_Sendkeys(recovery_estimate_lastname, lastname);
		SeleniumFunc.name_GenericMethod_Sendkeys(recovery_estimate_recoveryamount, recoveramt);
		SeleniumFunc.name_GenericMethod_Click(recovery_initate_recovery_btn);
		SeleniumFunc.waitFor(20);
//		SeleniumFunc.name_GenericMethod_Click(recovery_completed_back_btn);
		SeleniumFunc.xpath_GenericMethod_Click(recovery_completed_back_btns);
	}
	
	public void extractRecoveryID() throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(recovery_historyExpand);
		SeleniumFunc.waitFor(2);
		TestBase.recoveryid=SeleniumFunc.getElementText(recovery_id);
		System.out.println(TestBase.recoveryid);
	}
	
	public void approveTheRecovery(String reason, String recoverrecived,String recoverydate) throws Exception {

		OpenRecoveryFromFinance();
		SeleniumFunc.name_GenericMethod_Sendkeys(rec_finance_manualproreason,reason );
		SeleniumFunc.xpath_GenericMethod_Click(rec_finance_confr_mail);
		SeleniumFunc.name_GenericMethod_Sendkeys(rec_finance_amountRecived,recoverrecived);
		SeleniumFunc.name_GenericMethod_Click(rec_finance_amountreciveddate);
		selectDate(recoverydate);
		SeleniumFunc.xpath_GenericMethod_Click("(//label[text()='Mark recovery as final']/preceding-sibling::input)[2]");
		SeleniumFunc.xpath_GenericMethod_Click(reOpen_Submit_btn);
	}


	public void verifyRecoveryStatus(String status) throws Exception {
		String recoveryStatus=SeleniumFunc.getElement(By.xpath("(//span[@class='badge_text' and contains(text(),'Recov')])[2]")).getText();
		Assert.assertEquals(recoveryStatus,status);
	}

	public void OpenRecoveryFromFinance() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(vima_logo);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(queue_dropdown);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(finance_Recoveries);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(finance_recoveryid);
		// SeleniumFunc.xpath_GenericMethod_HoverOnDemoScreenPops(finance_caseid_filter);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(finance_recovery_caseid_filter);
		SeleniumFunc.xpath_GenericMethod_Click(finance_applyfilter);
		SeleniumFunc.waitFor(15);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(finance_search_text,TestBase.recoveryid );
		SeleniumFunc.xpath_GenericMethod_Click(finance_apply);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click("//td//a[text()='"+TestBase.recoveryid+"']");
		SeleniumFunc.waitFor(2);
	}

	public void addPaymentSingle() throws Exception {

		// claims.switchToDefaultContent();
		// SeleniumFunc.xpath_GenericMethod_Click(edit_estimate);
		try{
			SeleniumFunc.xpath_GenericMethod_Click(rec_finance_open_btn);
		}catch(Exception e){
			e.getMessage();
		}
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(10);
//		SeleniumFunc.xpath_GenericMethod_Click(Make_payment_options);
		SeleniumFunc.xpath_GenericMethod_Click(Make_payment_pay_btn);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(3);
//		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(payment_mode_dropdown,
//				"Single");
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(payment_mode_dropdownXpath,
				"Single");
		SeleniumFunc.waitFor(3);
//		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(transaction_type,
//				"Settlement");
//		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(payment_code,
//				"20-Auto repair");
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(payment_code,
				"21-Settlement");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Descdf, "description");
		try{
			SeleniumFunc.xpath_GenericMethod_Sendkeys(invoiceNumber_tfield,"654");
		}catch(Exception e){
			e.getMessage();
		}
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_type, "Broker");
//		Robot robot = new Robot();
//		robot.keyPress(KeyEvent.VK_DOWN);
//		robot.keyRelease(KeyEvent.VK_DOWN);
//		robot.keyPress(KeyEvent.VK_ENTER);
//		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(payee_name);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_name, "AutoTester");
		SeleniumFunc.enterTheValueInComboBox(Make_Payment_Payee, "Sibam Paul chowdhury ");
		Robot robort = new Robot();
		robort.keyPress(KeyEvent.VK_DOWN);
		robort.keyRelease(KeyEvent.VK_ENTER);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payment_amount, "10");
		SeleniumFunc.xpath_GenericMethod_Click(gst_exempt);
		SeleniumFunc.waitFor(2);
		try{
//			SeleniumFunc.xpath_GenericMethod_Click(attachfileXpath);
//			SeleniumFunc.xpath_GenericMethod_Click(attachCheckbox);
//			SeleniumFunc.xpath_GenericMethod_Click(submit_withdraw);
//			SeleniumFunc.waitFor(2);
			SeleniumFunc.name_GenericMethod_Click(make_payment_attachfile);
			SeleniumFunc.clickSelectFileButtonAndUploadTheFile("Claim_check_attachment.txt");
			SeleniumFunc.xpath_SelectFromDropdownUsingVisibleText(Make_Payment_Attach_categoery,"Claim");
			SeleniumFunc.xpath_GenericMethod_Click(Make_payment_attachmentpop_attachbtn);
			SeleniumFunc.waitFor(4);
		}catch(Exception e){
			e.getMessage();
		}
		SeleniumFunc.xpath_GenericMethod_Click(make_payment_button);

	}

	public void SelectfinalPayment() throws Exception {

		// claims.switchToDefaultContent();
		// SeleniumFunc.xpath_GenericMethod_Click(edit_estimate);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(Make_payment_options);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(payment_mode_dropdown,
				"Single");
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(transaction_type,
				"Settlement");
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(payment_code,
				"20-Auto repair");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Descdf, "description");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_type, "Broker");
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(payee_name);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_name, "Broker");
		Robot robort = new Robot();
		robort.keyPress(KeyEvent.VK_DOWN);
		robort.keyRelease(KeyEvent.VK_ENTER);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payment_amount, "10");
		SeleniumFunc.xpath_GenericMethod_Click(gst_exempt);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(make_payment_button);

	}

	public void addPayeeNewDuringMakeSinglePayment(String payeetypedropdown, String payeename, String bsb,
			String acntnumber, String mail) throws Exception {
		try{
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(rec_finance_open_btn);
		}catch(Exception e) {
			e.getMessage();
		}
//		SeleniumFunc.xpath_GenericMethod_Click(Make_payment_options);
		SeleniumFunc.xpath_GenericMethod_Click(Make_payment_pay_btn);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(payment_mode_dropdownXpath,
				"Single");
//		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(transaction_type,
//				"Settlement");
//		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(payment_code,
//				"20-Auto repair");
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(payment_code,
				"21-Settlement");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Descdf, "description");
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_type, "Broker");
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(payee_name);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_name, "Broker");
		Robot robort = new Robot();
		robort.keyPress(KeyEvent.VK_DOWN);
		robort.keyRelease(KeyEvent.VK_ENTER);
		SeleniumFunc.waitFor(5);

		// SeleniumFunc.xpath_GenericMethod_Click(actions_button);
		// claims.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(add_payee_button);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(add_payee_details_option);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(payee_type_dropdown,
				payeetypedropdown);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_name_new, payeename);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(account_name_new, payeename);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(bsb_new, bsb);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(account_number_new, acntnumber);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(email_new, mail);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(submit_new);

		SeleniumFunc.switchToDefaultContent();
		String casestatusscreenaftertransfer = xpath_Genericmethod_getElementText(payee_pending_approval);

		SeleniumFunc.writeToAssertionFile(
				"Expected status : Pending-Approval" + " , Actual status : " + casestatusscreenaftertransfer,
				(casestatusscreenaftertransfer.contains("Pending-Approval") ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreenaftertransfer.contains("PENDING-APPROVAL"));

	}

	public void addLegalPanel() throws Exception {

		// claims.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(actions_button);
		SeleniumFunc.xpath_GenericMethod_Click(legal_panel_option);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(add_legal_panel_button);

		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(legal_Update);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(select_plaintiff);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(vmia_solicitor, "Shibam QA");

		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(litigation_status,
				"Appeal");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(jurisdiction, "County");
		// Pavan
		SeleniumFunc.xpath_GenericMethod_Sendkeys(legal_reason, "Tesing");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(court_number, "3465");

		SeleniumFunc.xpath_GenericMethod_Click(date_issued);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);
		SeleniumFunc.xpath_GenericMethod_Click(date_served);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);

		SeleniumFunc.xpath_GenericMethod_Click(add_defendant);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(defendant_name, "Legal");
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(legal_save_btn);
		SeleniumFunc.xpath_GenericMethod_Click(legal_save_btn);
		// SeleniumFunc.xpath_GenericMethod_Click(add_legal_panel_button);
		SeleniumFunc.switchToDefaultContent();

	}

	public void adjustThepayment() throws Exception {

		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(actions_button);
		SeleniumFunc.xpath_GenericMethod_Click(lnkManagePayment);
		SeleniumFunc.xpath_GenericMethod_Click(drpdwnAdjustAction);
		SeleniumFunc.xpath_GenericMethod_Click(lstAdjust);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingValuebyclickingOnDropdown(drpdwnAdjustmentType, "");

	}

	public void verifyThesolicitortoaclaim() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(actions_button);
		SeleniumFunc.xpath_GenericMethod_Click(legal_panel_option);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(add_legal_panel_button);

		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(legal_Update);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(select_plaintiff);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(vmia_solicitor, "Shibam QA");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtPlaintiffsolicitor, "Shibam QA");

		// String txtErrormsg1 =
		// SeleniumFunc.xpath_Genericmethod_getElementText(txtErrormsg);

	}

	public void verifyNumberOfLitigations() throws Exception {

		String casestatusscreenaftertransfer = xpath_Genericmethod_getElementText(number_of_litigations);

		SeleniumFunc.writeToAssertionFile(
				"Expected number of LITIGATIONs : 1" + " , Actual number : " + casestatusscreenaftertransfer,
				("1".equals(casestatusscreenaftertransfer) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreenaftertransfer.equals("1"));

	}

	public void propertyDamagesSECS() throws Exception {

		// claims.switchToDefaultContent();
		SeleniumFunc.executeScript("arguments[0].click();", driver.findElement(By.xpath(property_damages_checkbox)));
		// SeleniumFunc.xpath_GenericMethod_Click(property_damages_checkbox);
		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click(property_covered_SECS);
		} catch (Exception e) {
		}

	}


	public void SECSScreen(String itemdesc, String purprice, String coverage,String IsDetYesorNo) throws Exception {

		// claims.switchToDefaultContent();
		SeleniumFunc.waitFor(3);
		if(IsDetYesorNo.equalsIgnoreCase("Yes")){
			SeleniumFunc.xpath_GenericMethod_Click(add_button_Det_Yes);
			Thread.sleep(400);
			SeleniumFunc.name_GenericMethod_Sendkeys(NotebookmodelNumber, itemdesc);
			Thread.sleep(200);
			SeleniumFunc.name_GenericMethod_Sendkeys(NotebookserialNumber, purprice);
		}else {
			SeleniumFunc.xpath_GenericMethod_Click(add_button_SECS);
			Thread.sleep(500);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(item_desc_1, itemdesc);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(orig_purchase_price_1, purprice);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(coverage_type_1, coverage);
		}
	}

	public void cyberIncidentScreen(String incident, String action) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(date_time_of_incident);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);
		SeleniumFunc.name_GenericMethod_Sendkeys(what_incident, incident);
		SeleniumFunc.name_GenericMethod_Sendkeys(action_incident, action);

	}

	public void clickClaimLog() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(claim_log);
	}

	public void enterIncidentDetailsLiability(String incident, String nameofclaimant, String addresslookupinput,
			String address) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(date_combined_liabiliy);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(incident_liab, incident);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(name_of_claimant, nameofclaimant);
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(addrees_lookup_liab,
		// addresslookupinput);
		SeleniumFunc.waitFor(2);
		// SeleniumFunc.xpath_GenericMethod_Click("//span[text()='" + address + "']");
		SeleniumFunc.xpath_GenericMethod_Click(addrees_lookup_liab);
		SeleniumFunc.waitFor(2);
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(contact_phone_preferred,
		// contactphonenumber);

	}

	public void withdrawCase(String withdrawreason) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(first_withdraw_button);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(withdraw_reason,
				withdrawreason);
		SeleniumFunc.xpath_GenericMethod_Click(submit_withdraw);
	}

	public void cancelClaimOrgAndVerifyStatus(String claimstatus) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(organization_details_button);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(org_claims_tab);
		SeleniumFunc.waitFor(3);
		// SeleniumFunc.executeScript("arguments[0].click()",driver.findElement(By.xpath(claim_cancel_button)));
		SeleniumFunc.xpath_GenericMethod_Click(claim_cancel_button);
		SeleniumFunc.xpath_GenericMethod_Click(dess);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(dess, "Desss");
		SeleniumFunc.xpath_GenericMethod_Click(submit_cancel);
		SeleniumFunc.waitFor(2);

		String cancelstatus = xpath_Genericmethod_getElementText(org_claim_status);

		// SeleniumFunc.writeToAssertionFile("Expected LITIGATION status :
		// RESOLVED-COMPLETED"+" , Actual status :
		// "+casestatusscreenaftertransfer,("RESOLVED-COMPLETED".equals(casestatusscreenaftertransfer)?"PASS":"FAIL"));
		Assert.assertTrue(cancelstatus.equals(claimstatus));

	}

	public void travelInsuranceGeneralDetails() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(authorized_business_travel);
		SeleniumFunc.xpath_GenericMethod_Click(date_of_departure);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);
		SeleniumFunc.xpath_GenericMethod_Click(date_of_return);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(departure_city, "city1");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(incident_city, "city2");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(incident_country, "country1");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(exact_location, "location");
		SeleniumFunc.xpath_GenericMethod_Click(medical_checkbox);
	}

	public void travelInsuranceMedicalDetails() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(medical_illnessdetails, "illness");
		SeleniumFunc.xpath_GenericMethod_Click(illness_complaint);
		SeleniumFunc.xpath_GenericMethod_Click(emergency_contacted);
		SeleniumFunc.xpath_GenericMethod_Click(other_health);
		SeleniumFunc.xpath_GenericMethod_Click(add_expenditure);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(exp_desc, "expen");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(exp_type, "type");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(amt_claimed, "200");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(curr, "AUD");

	}

	public void cancelPolicyMoreActions() throws Exception {
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(medical_illnessdetails,"illness");
		SeleniumFunc.xpath_GenericMethod_Click(more_actions);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(cancel_policy);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(cancel_date);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(cancel_reason, 1);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(cancel_declaration);
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);

	}

	public void DOGeneralDetails() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(DO_generaldetails_first_name);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(DO_generaldetails_first_name, "firstname");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(DO_generaldetails_last_name, "lastname");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(DO_employee_position, "manager");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(DO_employee_perform, "management");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(DO_employee_base_salary, "200");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(DO_employee_remuneration, "2000");
		SeleniumFunc.xpath_GenericMethod_Click(DO_written_contract);
		SeleniumFunc.xpath_GenericMethod_Click(DO_employee_covered);
		SeleniumFunc.xpath_GenericMethod_Click(DO_personnel_file);

	}

	public void DOUnlawfulTermination() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(DO_termination_occured);
		SeleniumFunc.xpath_GenericMethod_Click(DO_employee_resigned);
		SeleniumFunc.xpath_GenericMethod_Click(DO_commence_employment);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(DO_employee_age, "24");

		SeleniumFunc.xpath_GenericMethod_Click(DO_employee_disputes);
		SeleniumFunc.xpath_GenericMethod_Click(DO_employee_proceedings);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(DO_employee_complaint, "Complaint");

		SeleniumFunc.xpath_GenericMethod_Click(DO_qualifying_period);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(DO_employee_terminated_reason, "Unfit");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(DO_employee_challenge_reason, "not willing");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(DO_employee_payment, "200");

		SeleniumFunc.xpath_GenericMethod_Click(DO_org_less_than_15);
		SeleniumFunc.xpath_GenericMethod_Click(DO_engaged_legal_representation);

	}

	public void SelectProfile() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(Portfolio_profile);
		SeleniumFunc.xpath_GenericMethod_Click(Portfolio_profile_text);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(Report_Gear);
		// claims.switchToDefaultContent();
		// SeleniumFunc.xpath_GenericMethod_Click(Reportee_icon);

	}

	public void Selectreportee(String reportee) throws Exception {
		// SeleniumFunc.xpath_GenericMethod_Click(Reportee_icon);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Update_report_text);
		// SeleniumFunc.executeScript("arguments[0].click()",driver.findElement(By.xpath(Reportee_icon)));
		SeleniumFunc.xpath_GenericMethod_Click(Reportee_icon);
		// SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(Reportee_icon,
		// reportee);
		SeleniumFunc.waitFor(1);
		// claims.switchToDefaultContent();
		// SeleniumFunc.executeScript("arguments[0].click()",driver.findElement(By.xpath(reportee_value)));
		SeleniumFunc.waitFor(2);
		// SeleniumFunc.executeScript("arguments[0].click()",driver.findElement(By.xpath(reportee_value)));
		String Test_value = "Corporate";
		SeleniumFunc.xpath_GenericMethod_Sendkeys(operator_text, reportee);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(reportee_value1)));
		SeleniumFunc.waitFor(2);
		// SeleniumFunc.xpath_GenericMethod_Click(submit_button);

	}

	public void Deselectreportee() throws Exception

	{
		// SeleniumFunc.xpath_GenericMethod_Click(Report_Gear);
		// claims.switchToDefaultContent();
		// SeleniumFunc.waitFor(2);
		// SeleniumFunc.xpath_GenericMethod_Click(Update_report_text);
		// SeleniumFunc.waitFor(1);
		// SeleniumFunc.xpath_GenericMethod_Click(Reportee_icon);
		// SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(reportee_cross);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(submit_button);

	}

	public void Selectclaim() throws Exception

	{
		SeleniumFunc.xpath_GenericMethod_Click(left_toggle);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(New_triage)));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(Traige_Make_a_claim)));

	}

	public void fillInDataIntoQuestions1(String where, String what, String how) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(where_did_loss_occur_Triage, where);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(what_is_the_loss_Triage, what);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(how_loss_occur_triage, how);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(select_general_damages_Triage);
		SeleniumFunc.waitFor(1);
//	SeleniumFunc.xpath_GenericMethod_Click(general_damages_question_answer_no);

	}

	public void SelectCauseAndOutcome() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(claimClasification_Cause2,"Misplaced / lost");
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(claimClasification_outcome2,"Organisational asset");
		}

	public void SelectClaimIndeminityDecision(String decsion) throws Exception {
		
		SeleniumFunc.waitFor(3);
		if(decsion.equalsIgnoreCase("Granted")) {
		SeleniumFunc.xpath_GenericMethod_Click(Indeminity_Des_Granted);
		Thread.sleep(500);
		SeleniumFunc.xpath_GenericMethod_Click(Indemity_granted_full);
		}
		else if(decsion.equalsIgnoreCase("Denied")) {
			SeleniumFunc.xpath_GenericMethod_Click(Indeminity_Des_Denied);
			Thread.sleep(500);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(Indemity_comments, "Claims is pending");	
		}
		else if(decsion.equalsIgnoreCase("Pending")) {
			SeleniumFunc.xpath_GenericMethod_Click(Indeminity_Des_Pending); Thread.sleep(500);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(Indemity_comments, "Claims is pending");
		}
//		SeleniumFunc.xpath_GenericMethod_Click(btnSubmit);
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
				 
	}

	public void fillInScipDetails1(String description, String purchasecost, String salvagevalue) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(scip_add_button1);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(scip_purchase_desc1, description);
		SeleniumFunc.waitFor(1);
//	SeleniumFunc.xpath_GenericMethod_Click(scip_datepicker);
//	SeleniumFunc.xpath_GenericMethod_Click(today_link);

		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(scip_purchase_cost1, purchasecost);
//	SeleniumFunc.waitFor(1);
//	SeleniumFunc.xpath_GenericMethod_Sendkeys(scip_salvage_value, salvagevalue);

	}

	public void attachFileInAttachmentsPage2(String filepath, String category) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(attach_button_claims1);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(attach_button_claimss);

		SeleniumFunc.waitFor(1);

		SeleniumFunc.uploadFile(filepath);

		// SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(attachment_popup_category_dropdown,
		// "category");
		try {
			SeleniumFunc.xpath_GenericMethod_Sendkeys(attachment_popup_category_dropdown,
					"Repair or replacement invoices");
		} catch (Exception e) {
		}
		try {
			SeleniumFunc.xpath_GenericMethod_Sendkeys(attachment_popup_category_dropdown, "Invoice");
		} catch (Exception e) {
		}
		SeleniumFunc.waitFor(2);

		SeleniumFunc.xpath_GenericMethod_Click(attach_button);
		SeleniumFunc.waitFor(1);

	}

	public void clickContinueButtonTillEnabled(String contNum) throws Exception {
		SeleniumFunc.waitFor(2);
		// boolean flag =false;
		int contineBtnNum = Integer.parseInt(contNum);

		try {
			String framebytagName = goToFrameByTag_NameByXpath(continue_button);

			System.out.println("using getattribute name");
			if (framebytagName == null) {
				goToFrameByTag_IdByXpath(continue_button);
				System.out.println("using getattribute Id");
			}
			// flag = driver.findElement(By.xpath(continue_button)).isEnabled();
			System.out.println("Integer number after parsing string: "+contineBtnNum);
			for (int i = 0; i <= contineBtnNum; i++) {
				System.out.println("Entered into for statment");
				SeleniumFunc.xpath_GenericMethod_Click(continue_button);
				SeleniumFunc.waitFor(3);
			}
		} catch (Exception e) {
			// TODO: handle exception

		}

		// Boolean flag=driver.findElement(By.xpath(continue_button)).isEnabled();

//	WebElement webElement = driver.findElement(By.id("elementId"));
//	if(!webElement.getAttribute("class").contains("disabled")){
//	    webElement.click();
//	}
//	

	}

	public void clickOnManageComponent() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(Hamburger_menu_lnk);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(Mange_componet_Page);

	}

	public void entertherequireddetailsatAddpayeedetailspage(String payee, String payeeName, String payeeAccname,
			String payeebsb, String payeeAccNum, String payeeBankname, String payeeEmail) throws Exception {
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(Payee_Type_Drodwn,
				payee);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Payee_name_TxtBox, payeeName);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_account_name, payeeAccname);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_bsb, payeebsb);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_account_number, payeeAccNum);
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_bank_name, payeeBankname);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_email, payeeEmail);

	}

	public void SubmitandVerifytheStatus(String Status) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(SBT);

		SeleniumFunc.waitFor(3);
		String payeeStatus = xpath_Genericmethod_getElementText(lnkPendingStatus);
		Assert.assertTrue(Status.equals(payeeStatus));

	}

	public void riskAdimclaimsselection(String expectedstatus, String finalstatus) throws Exception {

		// SeleniumFunc.waitFor(5);
		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(queue_dropdown_1);
		SeleniumFunc.waitFor(1);
		// SeleniumFunc.xpath_GenericMethod_Click(triage_claims);
		SeleniumFunc.xpath_GenericMethod_Click(riskAdim);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.switchToDefaultContent();
		// SeleniumFunc.xpath_GenericMethod_Click(claimfile_id);
		SeleniumFunc.xpath_GenericMethod_Click(claimfile_id_1);
		// SeleniumFunc.xpath_GenericMethod_HoverOnDemoScreenPops(finance_caseid_filter);

		// SeleniumFunc.xpath_GenericMethod_Click(claimfileid_filter);
		// SeleniumFunc.executeScript("arguments[0].click()",
		// driver.findElement(By.xpath(claimfileid_filter_1)));
		// SeleniumFunc.xpath_GenericMethod_Click(claimfileid_filter_1);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text,
		// TestBase.currentClaimFileID);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text_1, TestBase.currentClaimFileID);
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text_1, "C-11022");
		SeleniumFunc.xpath_GenericMethod_Click(claims_apply);
		SeleniumFunc.waitFor(4);

		SeleniumFunc.switchToDefaultContent();
		// SeleniumFunc.xpath_GenericMethod_Click(claims_case_link);
		// SeleniumFunc.xpath_GenericMethod_Click(claims_case_link);
		// SeleniumFunc.xpath_GenericMethod_scrollIntoView(claims_case_link2);
		try {
			waitFor(5);
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(claims_case_link2);
			SeleniumFunc.xpath_GenericMethod_Click(claims_case_link2);
		} catch (Exception e) {
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(claims_case_link2);
			SeleniumFunc.xpath_GenericMethod_Click(claims_case_link2);
		}

		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_Genericmethod_getElementText(lnkPendingStatus);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_comments, "testing");
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(risk_admin_approve);

		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		String casestatusscreen = SeleniumFunc.xpath_Genericmethod_getElementText(riskAddim_Status);
		Assert.assertTrue(casestatusscreen.equals(finalstatus));

	}

	public void riskAdimRejectAddedPayee(String expectedstatus, String finalstatus) throws Exception {

		// SeleniumFunc.waitFor(5);
		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(queue_dropdown_1);
		SeleniumFunc.waitFor(1);
		// SeleniumFunc.xpath_GenericMethod_Click(triage_claims);
		SeleniumFunc.xpath_GenericMethod_Click(riskAdim);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.switchToDefaultContent();
		// SeleniumFunc.xpath_GenericMethod_Click(claimfile_id);
		SeleniumFunc.xpath_GenericMethod_Click(claimfile_id_1);
		// SeleniumFunc.xpath_GenericMethod_HoverOnDemoScreenPops(finance_caseid_filter);

		// SeleniumFunc.xpath_GenericMethod_Click(claimfileid_filter);
		// SeleniumFunc.executeScript("arguments[0].click()",
		// driver.findElement(By.xpath(claimfileid_filter_1)));
		// SeleniumFunc.xpath_GenericMethod_Click(claimfileid_filter_1);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text,
		// TestBase.currentClaimFileID);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text_1, TestBase.currentClaimFileID);
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text_1, "C-11022");
		SeleniumFunc.xpath_GenericMethod_Click(claims_apply);
		SeleniumFunc.waitFor(4);

		SeleniumFunc.switchToDefaultContent();
		// SeleniumFunc.xpath_GenericMethod_Click(claims_case_link);
		// SeleniumFunc.xpath_GenericMethod_Click(claims_case_link);
		// SeleniumFunc.xpath_GenericMethod_scrollIntoView(claims_case_link2);
		try {
			waitFor(5);
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(claims_case_link2);
			SeleniumFunc.xpath_GenericMethod_Click(claims_case_link2);
		} catch (Exception e) {
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(claims_case_link2);
			SeleniumFunc.xpath_GenericMethod_Click(claims_case_link2);
		}

		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_Genericmethod_getElementText(lnkPendingStatus);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_comments, "testing");
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(risk_admin_Reject);

		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		String casestatusscreen = SeleniumFunc.xpath_Genericmethod_getElementText(riskAddim_Status);
		Assert.assertTrue(casestatusscreen.equals(finalstatus));

	}

	public void clickAddPayeeButton() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(Add_Payee_Btn);
	}

	public void fetchCaseID() throws Exception {

		String fnolcaseid = xpath_Genericmethod_getElementText(claims_case_id);
		String caseid = fnolcaseid.substring(4);
		System.out.println(caseid + "--------------#rrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
		TestBase.currentCaseID = caseid;

	}

	public void fetchClaimsID() throws Exception {
		String fnolcaseid = xpath_Genericmethod_getElementText(claims_case_id);
		String claimsid = fnolcaseid.substring(4);
		System.out.println("Claim Number is:-------------- " +claimsid );
		TestBase.currentClaimNumber = claimsid;
	}

	public void verifytheaddedpayeeatClaimsHandler() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(payee_Name_filter);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(payee_name_search, "AutoTester");
		SeleniumFunc.xpath_GenericMethod_Click(claims_apply);
	}

	public void updatethepayeeandsubmit() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(payee_Action_lnk);
		SeleniumFunc.xpath_GenericMethod_Click(Payee_Modify_lnk);
	}

	public void enterthePayeeNameforupdate(String payeeName) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Sendkeys(Payee_name_TxtBox, payeeName);

	}

	public void ClickonLegalpanelandAddalegalpanel() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(tabLegalpanel);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(btnAddleagal);
		SeleniumFunc.switchToDefaultContent();
	}

	public void entertheDetailsatLeagalPanelpage(String leagalName, String SelectClaimsType, String Address1,
			String Address2, String suburb, String postcode, String SolicitoName, String SolicitoEmail,
			String SolicitoPhone) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtlegalName, leagalName);
		SeleniumFunc.xpath_GenericMethod_Click(lblOnpanel);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtClaimsType, SelectClaimsType);
		Robot robort = new Robot();
		robort.keyPress(KeyEvent.VK_ENTER);
		SeleniumFunc.xpath_GenericMethod_Click(lnkcannotFindAddress);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtAddress1, Address1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtAddress2, Address2);
		SeleniumFunc.name_GenericMethod_Sendkeys(txtSuburb, suburb);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(drpdwnState,
				"New South Wales");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtPostalCode, postcode);
		SeleniumFunc.xpath_GenericMethod_Click(btnAdd);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtSolicitorList, SolicitoName);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtEmail, SolicitoEmail);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtPhone, SolicitoPhone);
	}

	public void clickonSubmitandsearchAddedleagalPanel(String legalName) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(btnSubmit);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(imgIcon);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtSearch, legalName);
		SeleniumFunc.xpath_GenericMethod_Click(btnApply);
		SeleniumFunc.waitFor(2);

	}

	public void verifyclaimstype(String claimsType) throws Exception {
		// TODO Auto-generated method stub
		String ActualclaimsType = SeleniumFunc.xpath_Genericmethod_getElementText(gettxtClaimsType);
		Assert.assertTrue(ActualclaimsType.equals(claimsType));
	}

	public void verifytheCancelStatusfromTriageofficer(String pentingStatus) throws Exception {
		SeleniumFunc.waitFor(2);
		String ActualclaimsStatus = SeleniumFunc.xpath_Genericmethod_getElementText(caseStatusText);
		Assert.assertEquals(ActualclaimsStatus,pentingStatus);
	}

	public void selectUpdateoptionfromAction() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(lnkAction);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(lnkEditlegalpanel);

	}

	public void updateLegalPanelDetails(String updateName) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Clear(txtlegalName);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtlegalName, updateName);

	}

	public void verifyUpdatelegalName(String updateLegalPanelName) throws Exception {
		// TODO Auto-generated method stub
		String ActualclaimsType = SeleniumFunc.xpath_Genericmethod_getElementText(txtlegalName);
		Assert.assertTrue(ActualclaimsType.equals(updateLegalPanelName));
	}

	public void selectRemoveOptionfromAction() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(lnkAction);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(lnkRmoveLegalpanel);

	}

	public void clickSubmitforremovalofleagalpanel() throws Exception {
		// TODO Auto-generated method stub
		SeleniumFunc.xpath_GenericMethod_Click(btnSubmitLegal);
	}

	public void verifysystemAssignedLegalPanel() throws Exception {

		boolean displyedlegalPanel = xpath_Genericmethod_verifyElementPresent(txtNextlegalpanel);
		Assert.assertTrue((displyedlegalPanel));

	}

	public void select_the_Show_subsidiary_claims_check_box() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(lnkmyclaims);

		waitFor(10);
		try {
			SeleniumFunc.xpath_GenericMethod_Click(chkshowsubOrg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			SeleniumFunc.xpath_GenericMethod_Click(chkshowsubOrg);
		}

		SeleniumFunc.waitFor(2);

	}

	public void verify_the_Claiming_organisation_should_be_subsidiary_org(String subOrg) throws Exception {

		SeleniumFunc.xpath_Genericmethod_VerifyTextContains(txtClaimOrg, subOrg);
		SeleniumFunc.waitFor(2);
	}

	public void intiateRecoveries() throws Exception {

		SeleniumFunc.waitFor(2);

		SeleniumFunc.switchToDefaultContent();

		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(queue_dropdownn);
		SeleniumFunc.waitFor(1);
		// SeleniumFunc.xpath_GenericMethod_Click(unassigned_claimss);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(lnkRecoveries);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(Wave1);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(Wave2);
//		SeleniumFunc.xpath_GenericMethod_Click(claimfile_idd);
//		// SeleniumFunc.xpath_GenericMethod_HoverOnDemoScreenPops(finance_caseid_filter);
//		SeleniumFunc.waitFor(1);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(claimfileid_filter);
		SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text, TestBase.currentClaimFileID);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text, "CF-24425");

		SeleniumFunc.xpath_GenericMethod_Click(claims_apply);
		SeleniumFunc.waitFor(4);
		TestBase.currentClaimUnitID = xpath_Genericmethod_getElementText(claims_case_link);
		System.out.println("Claim Unit Id : " + TestBase.currentClaimUnitID);
		SeleniumFunc.xpath_GenericMethod_Click(claims_case_link);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtReasonformul, "Test");
		SeleniumFunc.xpath_GenericMethod_Click(radEmailforclient);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtAmtRecived, "200");
		SeleniumFunc.xpath_GenericMethod_Click(date_time_of_loss);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);
		SeleniumFunc.xpath_GenericMethod_Click(chkMarkrecoveyfnl);
		SeleniumFunc.xpath_GenericMethod_Click(SBT);

	}

	public void claimant_details_for_journy(String fristName, String lastName, String Addressline1, String Suburb,
			String State, String Postcode, String Country, String Emailaddress, String contactnumber, String Occupation,
			String Dateofbirth) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtfristName, fristName);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtlastName, lastName);
		SeleniumFunc.xpath_GenericMethod_Click(lnkcanntfindlink);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtAddressline1, Addressline1);
		SeleniumFunc.name_GenericMethod_Sendkeys(txtSuburb, Suburb);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtState, State);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtPostcode, Postcode);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtCountry, Country);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtEmailaddress, Emailaddress);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtcontactnumber, contactnumber);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtOccupation, Occupation);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtDateofbirth, Dateofbirth);

	}

	public void Claim_for_injury_or_death(String injury, String injuryoccur, String Dateinjuryoccured, String testWhenn)
			throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtinjury, injury);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtinjuryoccur, injuryoccur);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtDateinjuryoccured, Dateinjuryoccured);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txttestWhenn, testWhenn);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(didInjuryStopsYouWorking,"Yes");
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(haveYouReturnToWorking,"No");

	}

	public void Medical_details(String MedicalPractitioner, String Periodfrom, String Periodto, String Doctorsname,
			String Address, String condition, String Doctorsname1, String Address1) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtMedicalPractitioner, MedicalPractitioner);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtPeriodfrom, Periodfrom);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtPeriodto, Periodto);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtDoctorsname, Doctorsname);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtAddress, Address);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(txtcondition, condition);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtDoctorsname1, Doctorsname1);
		SeleniumFunc.name_GenericMethod_Sendkeys(txtAddress1, Address1);

	}
	public void clickpostButtontosubmmitthenote() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(post_note);
		waitFor(10);

	}
	public void clickbellNotificationImage() throws Exception {
		waitFor(10);
		try {
			xpath_GenericMethod_Click(Notification_bell);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			xpath_GenericMethod_Click(Notification_bell);
		}
	}
	
	public void verifytheNotification(String notification) throws Exception {

		driver.switchTo().defaultContent();
		driver.switchTo().activeElement();
		String notification_text = SeleniumFunc.xpath_Genericmethod_getElementText(detailsofnote_xpath);
		System.out.println("notification_text" + notification_text);
		Assert.assertEquals(notification_text, "Triage Claims Officer 1 R3");

	}
	public void clickonfristClaimcase() throws Exception {

		SeleniumFunc.waitFor(2);
		xpath_GenericMethod_Click(case_sort);

		List<WebElement> list_of_cases = driver.findElements(By.xpath(case_worklist));
		int total_cases = list_of_cases.size();
		System.out.println("total list of cases:" + total_cases);
		for (int i = 2; i <= total_cases + 1; i++) {
			waitFor(2);
			if (i == 2) {
				driver.findElement(By.xpath("(//table)[3]/tbody/tr[" + i + "]/td[2]")).click();
				waitFor(3);

			}

		}
	}
	public void clickOnViewnoteandentertextinpost(String noteText) throws Exception {

		waitFor(3);
		try {
			SeleniumFunc.xpath_GenericMethod_Click(view_note);
			System.out.println("try with try block");
		} catch (Exception e) {
			SeleniumFunc.xpath_GenericMethod_Click(view_note);
			System.out.println("try with catch block");

		}
		try {
			// SeleniumFunc.switchToDefaultContent();
			// SeleniumFunc.xpath_GenericMethod_Click_By_JavaScript(view_note);
			// SeleniumFunc.xpath_GenericMethod_Click(view_note);
			// SeleniumFunc.xpath_GenericMethod_Click(attach_button_claims);
			waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(enter_note);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(enter_note, noteText);
			waitFor(2);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void selectClaimFilefromListofcliamsTableprofileManger() throws Exception {

		// SeleniumFunc.waitFor(5);
		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {

			SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
			SeleniumFunc.switchToDefaultContent();
			// SeleniumFunc.xpath_GenericMethod_Click(queue_dropdown);
			SeleniumFunc.xpath_GenericMethod_Click(queue_dropdown_2);
			SeleniumFunc.waitFor(1);
			// SeleniumFunc.xpath_GenericMethod_Click(triage_claims);
			SeleniumFunc.xpath_GenericMethod_Click(Unassigned_school);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.switchToDefaultContent();
			// SeleniumFunc.xpath_GenericMethod_Click(claimfile_id);
			// SeleniumFunc.xpath_GenericMethod_Click(claimfile_id_port);
			SeleniumFunc.xpath_GenericMethod_Click(claimfile_id_port_1);
			// SeleniumFunc.xpath_GenericMethod_HoverOnDemoScreenPops(finance_caseid_filter);
			SeleniumFunc.waitFor(1);
			// SeleniumFunc.xpath_GenericMethod_Click(claimfileid_filter);
			// SeleniumFunc.xpath_GenericMethod_Click(claimfileid_filter_port);
			SeleniumFunc.xpath_GenericMethod_Click(claimfileid_filter_port_1);
			SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(claims_search_text, TestBase.currentClaimFileID);
			SeleniumFunc.xpath_GenericMethod_Click(claims_apply);
			SeleniumFunc.waitFor(2);
			// SeleniumFunc.xpath_GenericMethod_Click(claims_case_link);
			SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(claims_case_link)));
			SeleniumFunc.xpath_GenericMethod_Click(SBT);

		}

	}
	public void submitReopenclaimfromMywork() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(triage_actions_button);

		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(edit_claim_data);
		SeleniumFunc.waitFor(2);
		// claims.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(edit_claim_reason);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(edit_claim_reason, "Need to update");
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);

		// SeleniumFunc.xpath_GenericMethod_Click(SBT);

	}
	public void searchAndOpenClaimUnit1(String ClaimID ) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(caseID_sort);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(filter_worklist);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(claims_applyfilter);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(search_filter_textbox_worklist, ClaimID);
		Thread.sleep(2000);
		SeleniumFunc.xpath_GenericMethod_Click(apply_button_worklist);
		Thread.sleep(4000);
		SeleniumFunc.xpath_GenericMethod_Click(first_case_worklist);
		Thread.sleep(2000);

	}
	public void VerifyUnderWrttingtext() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(TR_Policy);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(uw_Section);
		SeleniumFunc.verifyElementPresent(uw_text);
		SeleniumFunc.verifyTextEquals(uw_text, "Testing for Us-4021");

	}
	// Us-4023 methods
		public void transfertheClaimsUnittoClaimsHandlers(String SerachClaim, String usertotransfer) throws Exception {

			SeleniumFunc.waitFor(2);
		
			SeleniumFunc.xpath_GenericMethod_Click(Bulk_Process_Lnk);
			SeleniumFunc.waitFor(3);
			try {
				SeleniumFunc.xpath_GenericMethod_Click(Filter_Work_claims_Unit);
				System.out.println("try with try block");
			} catch (Exception e) {
				SeleniumFunc.xpath_GenericMethod_Click(Filter_Work_claims_Unit);
				System.out.println("try with catch block");

			}
			SeleniumFunc.xpath_GenericMethod_Clear(Filter_Work_claims_Unit);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(Filter_Work_claims_Unit, SerachClaim);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(Click_On_Transfer);
			SeleniumFunc.xpath_GenericMethod_Click(Filter_cases);
			SeleniumFunc.waitFor(2);
			int Check_box_size = driver.findElements(By.xpath(Check_box_list)).size();
			if (Check_box_size > 0) {
				SeleniumFunc.xpath_GenericMethod_Click(Select_chk_Box_1);
				SeleniumFunc.xpath_GenericMethod_Click(Select_chk_Box_2);
				// for(int i=0;i<=Check_box_size;i++)

			}

			SeleniumFunc.xpath_GenericMethod_Click(Select_Action_Btn);
			SeleniumFunc.xpath_GenericMethod_Click(Option_list);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(user_to_transfer, usertotransfer);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(Click_On_Transfer1);
			SeleniumFunc.waitFor(1);
			SeleniumFunc.xpath_GenericMethod_Click(transfer_button);

		}
		
		public void editProfileUnavailability() throws Exception

		{
			// if(driver.findElement(By.xpath("//button[@data-test-id='2014100711485602125921']")).isDisplayed());
			// SeleniumFunc.xpath_GenericMethod_Click(Close_OpenPage);

			SeleniumFunc.xpath_GenericMethod_Click(Portfolio_profile);
			SeleniumFunc.xpath_GenericMethod_Click(Portfolio_profile_text);
			SeleniumFunc.switchToDefaultContent();
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(Actions_Btn);
			SeleniumFunc.xpath_GenericMethod_Click(Edit_Profile_lnk);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(Delete_segment_lnk);
			SeleniumFunc.waitFor(5);
			SeleniumFunc.xpath_GenericMethod_Click(Add_Skill_Lnk);
			SeleniumFunc.waitFor(5);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(Enter_New_Skill_Txt, "Commercial Property");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(Proficiency_of_Skill_Txt, "1");
			// SeleniumFunc.xpath_GenericMethod_Click(Add_Skill_Lnk);
			SeleniumFunc.xpath_GenericMethod_Click(EditProfile_Submit_Btn);
			SeleniumFunc.switchToDefaultContent();
			String Skill_verifiction_txt = SeleniumFunc.xpath_Genericmethod_getElementText(VerifyAddSkill);
			System.out.println("notification_text" + Skill_verifiction_txt);
			Assert.assertEquals(Skill_verifiction_txt, "Commercial Property");

			// claims.switchToDefaultContent();
			// SeleniumFunc.xpath_GenericMethod_Click(Reportee_icon);

		}
		public void enterUnavailabilitydateforclaimHandler(String startDate, String endDate) throws Exception

		{
			// if(driver.findElement(By.xpath("//button[@data-test-id='2014100711485602125921']")).isDisplayed());
			// SeleniumFunc.xpath_GenericMethod_Click(Close_OpenPage);

			SeleniumFunc.xpath_GenericMethod_Click(Portfolio_profile);
			SeleniumFunc.xpath_GenericMethod_Click(Portfolio_profile_text);
			SeleniumFunc.switchToDefaultContent();
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(Actions_Btn);
			SeleniumFunc.xpath_GenericMethod_Click(Availability_Profile_lnk);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(unaviability_strdate, startDate);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(unaviability_enddate, endDate);
			SeleniumFunc.xpath_GenericMethod_Click(EditProfile_Submit_Btn);

			System.out.println("Submited " + EditProfile_Submit_Btn);
			// Assert.assertEquals(Skill_verifiction_txt, "Commercial Property");

			// claims.switchToDefaultContent();
			// SeleniumFunc.xpath_GenericMethod_Click(Reportee_icon);

		}
		public void getClaimFile_triageClaimsOfficer() throws Exception {

			SeleniumFunc.waitFor(2);
			try {
				SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
			} catch (Exception e) {
			}

			OpenClaimFromQueue("Triage Claims");
			SeleniumFunc.switchToDefaultContent();
			SeleniumFunc.waitFor(2);
			// String casestatusscreen =
			// xpath_Genericmethod_getElementText(pending_triage_status);
			String casestatusscreen = xpath_Genericmethod_getElementText(pending_triage_status_1);

		}
		public void claimEnterBusiness() throws Exception {

			SeleniumFunc.xpath_GenericMethod_Sendkeys(zipcode, "2601");

			waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(find_button);
			waitFor(1);
			SeleniumFunc.xpath_GenericMethod_Click(select_business_checkbox);
			SeleniumFunc.xpath_GenericMethod_Click(continue_button);
			waitFor(2);

			SeleniumFunc.xpath_GenericMethod_Click(select_contact_checkbox);
			SeleniumFunc.xpath_GenericMethod_Click(continue_button);

		}
		public void clickoncapturelossdetailsandverifythefileds() throws Exception {
			xpath_GenericMethod_Click(Capture_loss_section);
			WebElement readOnly = driver.findElement(By.xpath("//textarea[@id='e11da520']"));
			Assert.assertTrue(readOnly.getAttribute("readOnly").equals("true"), "Element ReadOnly");

		}
	public void AddAction(String action) throws Exception
	{ 
		switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(actions_button);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+action+"']");
		SeleniumFunc.waitFor(2);
	} 
		public void LinkClaim(String claimant) throws Exception 
	{
		String actualText = "Claim "+TestBase.currentClaimNumber+" will be the primary/parent claim and the linked claim will be the child/secondary claim";
		//SeleniumFunc.getElement(By.xpath("//div[@data-test-id='202101261925060140920']")).getText();
		String expectedText = "Claim "+TestBase.currentClaimNumber+" will be the primary/parent claim and the linked claim will be the child/secondary claim";
		System.out.println("Actual text : "+ actualText + 
				"Expected Text : "+ expectedText );
		Assert.assertEquals(actualText, expectedText);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(link_claiment_name);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_GenericMethod_Sendkeys(txt_claimant, claimant);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Click(btn_search);   
		SeleniumFunc.waitFor(3);
		
//		List<WebElement> LinkCliams = SeleniumFunc.getElements(Link_List_claims);
//		for (WebElement ListOfClaims : LinkCliams) 
//				{
//					System.out.println("List of Claims to be linked: " + ListOfClaims.getText());
//		   		}
//		
		SeleniumFunc.waitFor(1);
			
		for (int i = 0; i < 3; i++) 
				{
			linkedClaim.add(SeleniumFunc.getElement(By.xpath(Link_claim_number)).getText());
			System.out.println("claim no : " + SeleniumFunc.getElement(By.xpath(Link_claim_number)).getText());
			
			SeleniumFunc.xpath_GenericMethod_Click(Linked_claim_button);
		Thread.sleep(5000);	
		}
	}
	public void UnLinkClaim() throws Exception 
	{
		SoftAssert soft = new SoftAssert();
		
		for(int i=2; i<=4;i++) {
			
			soft.assertTrue(linkedClaim.contains( SeleniumFunc.getElement(By.xpath(("(//table[@pl_prop='D_FetchLinkedClaimList.pxResults']//tr["+i+"]/td[2]//span)[1]"))).getText()));
			
			}
		soft.assertAll();
		
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(UnLinked_claim_button);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(button_back_to_claim);
	}
	public void VerifyLinkClaims(String Claiment) throws Exception
	{
		SeleniumFunc.xpath_GenericMethod_Click("//h3[@class='layout-group-item-title' and contains(text(),'"+Claiment+"')]");
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(claim_description);
		SoftAssert ClaimAdim = new SoftAssert();
		ClaimAdim.assertTrue(linkedClaim.contains( SeleniumFunc.getElement(By.xpath(("(//table[@pl_prop='D_FetchLinkedClaimList.pxResults']//tr[2]/td[2]//span)[1]"))).getText()));
		ClaimAdim.assertAll();
	}


	public void VerifyClaimsAdmin() throws Exception {
		
		//Claim Administration
//		SeleniumFunc.waitFor(2);
//		SeleniumFunc.xpath_GenericMethod_Click(ClaimsAdminGeneralButton);
		String GeneralActIndeminityText   = SeleniumFunc.xpath_Genericmethod_getElementText(GeneralIndeminityText);
		String GeneralExcepIndeminityText = "Indemnity";
		Assert.assertEquals(GeneralActIndeminityText, GeneralExcepIndeminityText, "General Indeminity Not Matched");
		
		SeleniumFunc.waitFor(2);
		String GeneralActSignificanceText   = SeleniumFunc.xpath_Genericmethod_getElementText(GeneralSignificanceText);
		String GeneralExcepSignificanceText = "Claim significance";
		Assert.assertEquals(GeneralActSignificanceText, GeneralExcepSignificanceText, "General Signifinance Not Matched");
		
		SeleniumFunc.waitFor(2);
		String GeneralActFinancialsText   = SeleniumFunc.xpath_Genericmethod_getElementText(GeneralFinancialText);
		String GeneralExcepFinancialsText = "Financials";
		Assert.assertEquals(GeneralActFinancialsText, GeneralExcepFinancialsText, "General Financials Not Matched");
		
		SeleniumFunc.waitFor(2);
		//Claim Description
		SeleniumFunc.xpath_GenericMethod_Click(claim_description);
		String ClaimDisActIncidentText   = SeleniumFunc.xpath_Genericmethod_getElementText(ClaimDisIncidentText);
		String ClaimDisExcepIncidentText = "Incident";
		Assert.assertEquals(ClaimDisActIncidentText, ClaimDisExcepIncidentText, "Claim Description Incident Not Matched");
		
		SeleniumFunc.waitFor(2);
		//Claim File Status And History
		SeleniumFunc.xpath_GenericMethod_Click(ClaimsFileStatusHistory);
		String ClaimFileActTriageText   = SeleniumFunc.xpath_Genericmethod_getElementText(ClaimFileTraigeText);
		String ClaimFileExcepTriageText = "Triage";
		Assert.assertEquals(ClaimFileActTriageText, ClaimFileExcepTriageText, "Claim File Status and History Triage Not Matched");
		
		SeleniumFunc.waitFor(2);
		String ClaimFileActClaimClosureText   = SeleniumFunc.xpath_Genericmethod_getElementText(ClaimFileClosureText);
		String ClaimFileExcepClaimClosureText = "Claim closure";
		Assert.assertEquals(ClaimFileActClaimClosureText, ClaimFileExcepClaimClosureText, "Claim File Status and History Claim closure Matched");
		
	}
	public void SelectClaimIndeminityDenialDecision(String denialAction,String DenailType) throws Exception
	{
		waitFor(2);
		if(denialAction.equalsIgnoreCase("Approve"))
		{
			SeleniumFunc.xpath_GenericMethod_Click(Denail_action_Approve);
		}
		else if (denialAction.equalsIgnoreCase("Request more information"))
		{
			SeleniumFunc.xpath_GenericMethod_Click(Denail_action_RMI);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(Denail_action_comments, "Request Denial info");
		}
		else if (denialAction.equalsIgnoreCase("Decline denial"))
		{
			SeleniumFunc.xpath_GenericMethod_Click(Denail_action_DD);
			if (denialAction.equalsIgnoreCase("Full")) {
				SeleniumFunc.xpath_GenericMethod_Click(Indemity_granted_full);
			}
			else if(denialAction.equalsIgnoreCase("Partial"))
			{
				SeleniumFunc.xpath_GenericMethod_Click(Indemity_granted_partial);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(Denail_action_comments, "claim to be denial");
			}

		}
		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);
	}
	public void SelectClaimIndeminityDecision(String decsion, String decisiontype) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(3);
		if(decsion.equalsIgnoreCase("Granted")) {
			Thread.sleep(500);
			SeleniumFunc.xpath_GenericMethod_Click(Indeminity_Des_Granted);
			Thread.sleep(500);
			if (decsion.equalsIgnoreCase("Full")) {
				SeleniumFunc.xpath_GenericMethod_Click(Indemity_granted_full);
			}
			else if(decsion.equalsIgnoreCase("Partial")){
				SeleniumFunc.xpath_GenericMethod_Click(Indemity_granted_partial);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(Indemity_comments, "Claims is pending");
			}
		}
		else if(decsion.equalsIgnoreCase("Denied")) {
			Thread.sleep(2000);
			SeleniumFunc.xpath_GenericMethod_Click(Indeminity_Des_Denied);
			Thread.sleep(500);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(Indemity_comments, "Claims is pending");
		}

		SeleniumFunc.xpath_GenericMethod_Click(triage_closing_submit);


	}
	public void AddLitigation() throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(actions_button);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(Add_litigation);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(Add_litigation1);

	}
	public void Litigation_SelectDefendentdetails(String Defendent, String Soli, String Insurer) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(add_defendant);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(lit_defendent,Defendent);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(lit_Solicitor,Soli);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(lit_insurer,Insurer);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(lit_Claim_number,  TestBase.currentClaimNumber);
		SeleniumFunc.waitFor(4);
	}

	public void Litigation_enterTheEventDetails(String eventName, String eventdate, String resoultioncoments) throws Exception {
		SeleniumFunc.waitFor(5);
//		SeleniumFunc.name_GenericMethod_Click(lit_update);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(lit_Timetable_tab);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Click(lit_add_event);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(lit_select_event,eventName);
		SeleniumFunc.name_GenericMethod_Click(lit_event_date1);
		SeleniumFunc.waitFor(1);
		selectDate(eventdate);
//		SeleniumFunc.name_GenericMethod_Clear(resoultioncoments);
//		SeleniumFunc.enterTheValueInComboBox(lit_responsible_person,resoultioncoments);
		SeleniumFunc.waitFor(3);


	}

	public void Litigation_enterConcludedAndNotesDetails(String concluded, String actualdate, String notes) throws Exception
	{
		if (concluded.equalsIgnoreCase("Yes")) {
			SeleniumFunc.xpath_GenericMethod_Click(concluded_Yes);
		}
		else {
			SeleniumFunc.xpath_GenericMethod_Click(concluded_No);
		}
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(Actual_date, actualdate);
		SeleniumFunc.waitFor(1);
		driver.switchTo().frame(SeleniumFunc.getElement(By.xpath(event_notes_frame)));
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(event_notes, notes);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.name_GenericMethod_Click(event_add_notes);
		SeleniumFunc.waitFor(1);
		String actualtext = notes;
		String expectedtext = SeleniumFunc.getElement(By.xpath("//input[@name='PNotesList1colWidthGBR']/parent::td/div/table//tr/td//p[text()='"+notes+"']")).getText();

		System.out.println("Actual text :"+ actualtext + "Expected text :"+ expectedtext);
		Assert.assertEquals(actualtext, expectedtext);

		WebElement ele = SeleniumFunc.getElement(By.xpath(lit_Save));
		Actions ac = new Actions(driver);
		ac.moveToElement(ele).click(ele).build().perform();
		SeleniumFunc.waitFor(4);
		try {
			ele.sendKeys(Keys.ENTER);
		} catch (StaleElementReferenceException e) {
			// TODO: handle exception
		}

//
//		SeleniumFunc.xpath_GenericMethod_Click(lit_Save);
		SeleniumFunc.waitFor(10);

	}

	public void AssertTimetableHistory(String concluded, String eventdate, String actualdate, String eventname, String responsible) throws Exception
	{
		SeleniumFunc.xpath_GenericMethod_Click(Timetable_history);
		List<WebElement> TimetableHistoryList = SeleniumFunc.getElements(Timetable_history_data);

		for (WebElement TimetableHistory : TimetableHistoryList) {
			System.out.println("List of the Timetable History : " + TimetableHistory.getText());
		}
		SoftAssert soft = new SoftAssert();
		Assert.assertEquals(TimetableHistoryList.get(0).getText(), concluded,"concluded not found" );
		Assert.assertEquals(TimetableHistoryList.get(1).getText(), eventdate, "Event date is differ");
		Assert.assertEquals(TimetableHistoryList.get(2).getText(), actualdate, "Acutal date is differ");
		Assert.assertEquals(TimetableHistoryList.get(3).getText(), eventname, "Event Nmae is same " );
		Assert.assertEquals(TimetableHistoryList.get(4).getText(), responsible, "Responsible person is differ");
	}

	public void Litigation_EnterTheLitigationSettlement(String Resolution, String ResoultionDate, String comment, String cost,String settlementMonetery) throws Exception {
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(lit_Settlement_tab);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(lit_Resouttion_resoultion,Resolution);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Click(lit_Resouttion_date);
		SeleniumFunc.waitFor(1);
		selectDate(ResoultionDate);
		//SeleniumFunc.name_GenericMethod_Sendkeys(lit_Resouttion_amount,Amount);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(lit_Resouttion_cost,cost);


	}

	public static void Select_Settlement_Monetary(String monetary, String vmia_contribution, String general_damages, String settlementMonetery) throws Exception {
		if(monetary.equalsIgnoreCase("Yes"))
		{
			SeleniumFunc.waitFor(1);
			SeleniumFunc.xpath_GenericMethod_Click(lit_settlement_Yes);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.name_GenericMethod_Clear(lit_VMIA_contribution);
			SeleniumFunc.name_GenericMethod_Sendkeys(lit_VMIA_contribution, vmia_contribution);
			SeleniumFunc.waitFor(3);
			SeleniumFunc.name_GenericMethod_Clear(lit_Gen_damage);
			SeleniumFunc.name_GenericMethod_Sendkeys(lit_Gen_damage, general_damages);
			SeleniumFunc.waitFor(2);
		}
		else if(monetary.equalsIgnoreCase("No"))
		{
			SeleniumFunc.waitFor(1);
			SeleniumFunc.xpath_GenericMethod_Click(lit_settlement_No);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.name_GenericMethod_Sendkeys(lit_Resouttion_nonmontery, settlementMonetery);
			SeleniumFunc.waitFor(1);
		}

	}
	public void Litigation_enterTheLitigationDetails(String LegalStatus, String Status, String Jurisdiction, String Courtnumber, String DateIssue,String dateservered,String Plaintiff) throws Exception {

		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(lit_legal_status, LegalStatus);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(lit_vima_litstatus, Status);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_GenericMethod_Sendkeys(lit_vima_lit_jurdication,Jurisdiction);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(lit_vima_courtnumer,Courtnumber);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtPlaintiffsolicitor,Plaintiff);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Click(lit_dateIssued);
		SeleniumFunc.waitFor(1);
		selectDate(DateIssue);
		SeleniumFunc.name_GenericMethod_Click(lit_dateServed);
		SeleniumFunc.waitFor(1);
		selectDate(dateservered);
	}
	public void addAttachmentToLitigation(String file) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(lit_Attachment_tab);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Click(Attachment_add_new);
		SeleniumFunc.waitFor(10);
		//SeleniumFunc.name_GenericMethod_Click(SelectFile_button);
		WebElement ele = SeleniumFunc.getElement(By.xpath(SelectFile_button));
		Actions ac = new Actions(driver);
		ac.moveToElement(ele).click(ele).build().perform();
		SeleniumFunc.waitFor(4);
		SeleniumFunc.uploadFile(file);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_SelectFromDropdownUsingVisibleText(Attach_Category, "Accounting");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(AttachFile_button);
	}
	public void CompleteLitigation(String user) throws Exception
	{
		if (user.equalsIgnoreCase("Triage"))
		{
			SeleniumFunc.name_GenericMethod_Click(lit_Submit);
		}
		else {
			SeleniumFunc.waitFor(5);
			SeleniumFunc.xpath_GenericMethod_Click(lit_mark_as_settled);
		}
	}
	public void Litigation_enterTheSolicitordetails(String partytype, String VMIASolicitor, String Appoinmentdate, String PlaintiffSolicitr, String ReasonforSolcitor) throws Exception {
		SeleniumFunc.waitFor(5);
		SeleniumFunc.name_GenericMethod_Click(lit_update);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_SelectFromDropdownUsingVisibleText(lit_vima_partyType, partytype);
		SeleniumFunc.enterTheValueInComboBox(lit_vima_Solicitor,VMIASolicitor);
		SeleniumFunc.name_GenericMethod_Click(lit_vima_appointmentdate);
		SeleniumFunc.waitFor(1);
		selectDate(Appoinmentdate);
		SeleniumFunc.waitFor(2);
//		SeleniumFunc.name_GenericMethod_Sendkeys(lit_reasonforSolitorsele,ReasonforSolcitor);
	}

		public void openCaseFromUnderwriterQueue(String queue) throws Exception {
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
			SeleniumFunc.waitFor(5);
			SeleniumFunc.xpath_GenericMethod_Click(selectQueueDropdown);
			SeleniumFunc.waitFor(1);

			SeleniumFunc.xpath_GenericMethod_Click(UnderwritingOption+"span[text()='"+queue+"']");
			SeleniumFunc.waitFor(4);
			SeleniumFunc.switchToDefaultContent();

			if(queue.equalsIgnoreCase("Asset Management")) {
				System.out.println("Control was here");
				SeleniumFunc.xpath_GenericMethod_Click(UW_Casenumber_TH1); SeleniumFunc.waitFor(4);
				SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(filterOption_Asset)));}
			else {
				SeleniumFunc.xpath_GenericMethod_Click(UW_Casenumber_TH);
				SeleniumFunc.waitFor(4);
				SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(filterOption)));}
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(uw_applyfilter);
//		testData.setCaseId("PRO-24622");
			System.out.println("The case id is:----"+TestBase.currentClaimNumber+"------------------");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(UW_search_text, TestBase.currentClaimNumber);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(UW_search_text, "AA-187");
			SeleniumFunc.xpath_GenericMethod_Click(UW_search_applyfilter);
			Thread.sleep(10000);
			SeleniumFunc.waitForElementToClikable(By.xpath(uw_clickcase),40);
			SeleniumFunc.xpath_GenericMethod_Click(uw_clickcase);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.acceptTheAlert();

		}
}
