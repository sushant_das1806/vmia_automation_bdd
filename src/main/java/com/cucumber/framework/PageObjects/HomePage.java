package com.cucumber.framework.PageObjects;

import com.cucumber.framework.context.TestData;
//import org.apache.log4j.Logger;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.cucumber.framework.CS.CustomerServ;
import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import com.cucumber.framework.TestBase.TestBase;
//import com.cucumber.framework.helper.Logger.LoggerHelper;

public class HomePage extends CustomerServ implements HomePageLoc{
	//private final Logger log = LoggerHelper.getLogger(HomePage.class);
	HomePage homepage;
	TestData testData;
	public HomePage(WebDriver driver, TestData data) {
		super(driver);
		testData=data;
	}

	public void sendAppObject(HomePage homepage) {
		this.homepage = homepage;
	}
	
	public void clickNewApplication() throws Exception
	{
//		SeleniumFunc.waitFor(3);
		SeleniumFunc.waitForElementToClikable(By.xpath(new_application),90);
//		SeleniumFunc.waitForElement(driver.findElement(By.xpath(new_application)),90);
		SeleniumFunc.jsClick(new_application);
	}

	public void clickMakeClaim() throws Exception
	{
		SeleniumFunc.waitFor(3);
		SeleniumFunc.jsClick(newClaim_externaluser);
	}
	
	public void clickUpdatePolicy() throws Exception
	{
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(update_policy);
	}
	
	public void clickUpdatePolicyforLiability() throws Exception
	{
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Update_policy_Liability);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(update_policy_logo);
	}
	
	public void clickUpdatePolicyforPro() throws Exception
	{
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(Update_policy_Pro);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(Update_logo_Pro);
	}
	
	
	public void  clickUpdatePolicyy() throws Exception
	{
//		TestBase.currentPolicyNumber="V012013";
		SeleniumFunc.waitFor(60);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+TestBase.currentPolicyNumber+"']/ancestor::span[1]/following-sibling::span//button[text()='More actions']");
//		SeleniumFunc.clickJS(driver.findElement(By.xpath("//span[text()='"+TestBase.currentPolicyNumber+"']/ancestor::span[1]/following-sibling::span//button[text()='More actions']/i")));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Update_logo_MVF);
		
		
	}
	
	public void clickMakeNewClaimsAsTraiageOfficer() throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(8);
		SeleniumFunc.xpath_GenericMethod_Click(BTN_Hamburger);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(NewOption);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(ClaimNew);
	}
	
	
		
	
	public  void clickUpdatePolicyforGPA() throws Exception
	{
		
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Update_policy_GPA);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(Update_logo_GPA);
		
		
		
	}
public void   clickUpdatePolicyforMSC() throws Exception
	
	{
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Update_policy_MSC);
		SeleniumFunc.waitFor(10);
		SeleniumFunc.xpath_GenericMethod_Click(Update_logo_MSC);
		
		
	}
	
	
	
public void   clickUpdatePolicyforMVF() throws Exception
	{
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Update_policy_MVF);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(Update_logo_MVF);
		
		
	}
	
	                      
	
	public void clickViewOrgDetails() throws Exception
	{
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(6);
		SeleniumFunc.xpath_GenericMethod_Click(view_org_profile);
	}
	
}
