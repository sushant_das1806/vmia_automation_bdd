package com.cucumber.framework.PageObjects;

import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import com.cucumber.framework.TestBase.TestBase;
import com.cucumber.framework.context.TestData;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class ClaimReviewHomePage implements ClaimReviewHomePageLoc{

   WebDriver driver;
   String newFileName;
   TestData testdata;
    public ClaimReviewHomePage(WebDriver driver, TestData data) {
        this.driver=driver;
        testdata=data;
    }


    public void AddAttachmentInTheclaim(String filename,String attachmentCategory,String author,String attachdocate) throws Exception {
        SeleniumFunc.waitFor(10);
        SeleniumFunc.xpath_GenericMethod_Click(addAttachment);
        SeleniumFunc.waitFor(2);
        SeleniumFunc.clickSelectFileButtonAndUploadTheFile("BulkUploadTemplateTest_Updated.csv");
        SeleniumFunc.waitFor(1);
        enterAttahcmentFileDetails(filename,attachmentCategory,author,attachdocate);
        SeleniumFunc.waitFor(1);
        SeleniumFunc.xpath_GenericMethod_Click(attachmentAttahcButton);
        SeleniumFunc.waitFor(60);
    }

    public void enterAttahcmentFileDetails(String filename,String attachmentcategory,String author,String attachdocate) throws Exception {
        SeleniumFunc.xpath_SelectFromDropdownUsingVisibleText(attachmentCategory,attachmentcategory);
        SeleniumFunc.waitFor(1);
        SeleniumFunc.xpath_GenericMethod_Sendkeys(attachmentAuthor,author);
        SeleniumFunc.waitFor(1);
        SeleniumFunc.xpath_GenericMethod_Click(attachmentDocDate);
        SeleniumFunc.waitFor(1);
        SeleniumFunc.xpath_GenericMethod_Click("//a[@class='today-link']");
        Thread.sleep(2);
        TestBase.attachmentDocname=filename+SeleniumFunc.generateRandomnumber();
        SeleniumFunc.xpath_GenericMethod_Sendkeys(attachmentFileName,TestBase.attachmentDocname);
        Thread.sleep(10);
    }



    public void clickUpdateFileMetadata(String filename,String author,String attachmentCategory,String attachdocate) throws Exception {
        SeleniumFunc.waitFor(2);
        SeleniumFunc.xpath_GenericMethod_Click("//a[text()='"+TestBase.attachmentDocname+"']"+documentActionlink);
        Thread.sleep(500);
        SeleniumFunc.xpath_GenericMethod_Click("//span[text()='Edit']//ancestor::a");
        SeleniumFunc.waitFor(3);
        SeleniumFunc.name_GenericMethod_Sendkeys(editAttachment_Author,author);
        Thread.sleep(200);
        SeleniumFunc.name_GenericMethod_Sendkeys(editAttachment_name,filename);
        Thread.sleep(200);
        SeleniumFunc.xpath_GenericMethod_Click(attachmentUpdatecButton);
    }

    public void getAttachmentDocHistory(String filname,String Category,String Documentdate) throws Exception {
        SeleniumFunc.xpath_GenericMethod_Click("//a[text()='"+TestBase.attachmentDocname+"']"+documentActionlink);
        Thread.sleep(500);
        SeleniumFunc.xpath_GenericMethod_Click("//span[text()='History']//ancestor::a");
        List<WebElement> history=SeleniumFunc.getElements(documentHistory);
        SoftAssert asserts=new SoftAssert();
        asserts.assertEquals(history.get(0).getText(),"Added \"Document\" '"+TestBase.attachmentDocname+"'");
        asserts.assertEquals(history.get(1).getText(),"Added \"Category\" '"+Category+"'");
//        asserts.assertEquals(history.get(2).getText(),"Added \"Document date\" '"+Documentdate+"'");
        asserts.assertEquals(history.get(3).getText(),"Added \"Author\" 'AutoTester'");
        asserts.assertAll();
        SeleniumFunc.xpath_GenericMethod_Click("//a[text()='"+TestBase.attachmentDocname+"']"+documentActionlink);
        SeleniumFunc.xpath_GenericMethod_Click(attahcmentHistoryClose);
    }

    public void selectTabinSearch(String option) throws Exception {
        SeleniumFunc.waitFor(8);
        SeleniumFunc.xpath_GenericMethod_Click("//label[text()='"+option+"']");
    }

    public void enterTheDocumentNameAndClickSearch(String document) throws Exception {
        SeleniumFunc.waitFor(8);
        TestBase.attachmentDocname=document;
        SeleniumFunc.name_GenericMethod_Sendkeys(documentNameSearch,TestBase.attachmentDocname);
        SeleniumFunc.xpath_GenericMethod_Click(searchButton);
    }

    public void editTheDocumentFromSearchResult() throws Exception {
        SeleniumFunc.waitFor(2);
        SeleniumFunc.xpath_GenericMethod_Click("//a[text()='"+TestBase.attachmentDocname+"']"+documentActionSearchResult);
        Thread.sleep(500);
        SeleniumFunc.xpath_GenericMethod_Click("//span[text()='Edit']//ancestor::a");
        SeleniumFunc.waitFor(3);
        newFileName="UpdatedInsearchResult"+SeleniumFunc.generateRandomnumber();
        SeleniumFunc.name_GenericMethod_Sendkeys(editAttachment_Author,newFileName);
        Thread.sleep(200);
        SeleniumFunc.xpath_GenericMethod_Click(attachmentUpdatecButton);
    }

    public void VerifyTheDocumentHistoryInSearchResult() throws Exception {
        SeleniumFunc.waitFor(4);
        SeleniumFunc.xpath_GenericMethod_Click("//a[text()='"+TestBase.attachmentDocname+"']"+documentActionSearchResult);
        Thread.sleep(500);
        SeleniumFunc.xpath_GenericMethod_Click("//span[text()='History']//ancestor::a");
        SeleniumFunc.waitFor(3);
        Assert.assertTrue("History is not added for the document update",SeleniumFunc.getElementText("//table[@pl_prop_class='History-Data-WorkAttach-File']//tr/td[3]//span[1]").contains(newFileName));
    }
}
