package com.cucumber.framework.PageObjects;

public interface ViewOrgDetailsLOC {
	
	String policy_number = "(//span[@data-test-id='202005082320070503934'])[1]";
	String policy_type ="(//span[@data-test-id='2015011509590404056600'])[1]";
	String policy_premium = "(//span[@data-test-id='2017053008154708403451301'])[1]";
	
	String add_contact = "//button[@data-test-id='202009221450120035441']";
	String job_title = "//input[@data-test-id='202008271133460744622']";
	String first_name = "//input[@data-test-id='20141229025049096710373']";
	String last_name = "//input[@data-test-id='20141229025049097112276']";
	String phone_number = "//input[@name='$PNewContactPage$pMobileNumber']";
	String email = "//input[@name='$PNewContactPage$ppyEmail1']";
	String account_type = "//input[@data-target='$PpyWorkPage$pBusinessContacts$l1$pRoleList']";
	//String submit = "//button[@data-test-id='2014121801251706289770']";
	String submit = "//button[text()='Submit']";
	String update_contact = "//button[@data-test-id='202009141335240330381']";
	String update_personal_info = "//button[@data-test-id='202009141929590446285']";
	String update_job_title_text = "//input[@data-test-id='202008271133460744622']";
	String update_submit = "//button[@title='Click here to submit work']";
	String close_button = "//button[@data-test-id='20141210043348063930294']";


	String account_status_enable_disable = "//label[@data-test-id='202008271859150508182-Label']";

	String AssetTab="(//h2[text()='Assets'])[1]";
	String assetidTH="//div[@class='gridHeaderLabel ' and text()='VMIA asset ID']";
	String AssetFilterIcon="//div[@class='gridHeaderLabel ' and text()='VMIA asset ID']//following-sibling::div[3]/a";
	String ApplyFilter="//span[@class='menu-item-title' and text()='Apply filter']";
	String SearchText="//input[contains(@name,'ppySearchText')]";
	String applybutton="//button[text()='Apply']";
	String assetDetails="//a[contains(@name,'AssetDetails_D_GetListOfAssetsByEntity') and text()='VAR000096']";
}
