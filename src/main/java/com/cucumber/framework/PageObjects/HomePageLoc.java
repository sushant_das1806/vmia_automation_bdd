package com.cucumber.framework.PageObjects;

public interface HomePageLoc {
	/*String new_application="//div[@data-test-id='202004141745590632369' and text()='Click here to start a new policy application']";
	String make_a_claim="//div[@data-test-id='202004141745590632369' and text()='Click here to start a new claim submission']";
*/

	String new_application="//div[@aria-label='Apply for a policy']";
	String make_a_claim="//div[@data-test-id='202004141745590631699']/following-sibling::div[text()='Make a claim']";
	String update_policy="//div[@data-test-id='202004141745590632369' and text()='Click here to update one of your policies']";
//	String view_org_profile="//button[@data-test-id='202008041311520919467']";
	String view_org_profile="//button[text()='View organisation profile']";
	String vmia_logo = "//img[@title='vmia logo']";
	String newClaim_externaluser="//div[@aria-label='Make a claim']";
	String Update_policy_Liability="//body/div[@id='PEGA_HARNESS']/form[1]/div[3]/div[1]/section[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/span[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[2]/div[1]/div[1]/div[4]/div[1]/div[3]/span[1]/button[1]";
			//"//div[3]/span/button[contains(@title,'Click for more actions on policy Liability for hall hirers and other users')]";
	
	String clientAdvisor_New="//a[@class='CS_create_new']";
	String ClientAdviosr_New_AssetManagement="//span[text()='Asset management']";
	
	String update_policy_logo="//span[text()='Update policy']";
	String Update_policy_GPA= "//button[@title=\"Click for more actions on policy Group Personal Accident\"]";
	String Update_logo_GPA= "//span[text()=\"Update policy\"]";
	String Update_policy_MVF= "//button[@title=\"Click for more actions on policy Motor Vehicle\"]";
	String Update_logo_MVF= "//span[text()=\"Update policy\"]";
	String Update_policy_MSC= "//button[@title=\"Click for more actions on policy Motor � Special Contingency\"]";
	String Update_logo_MSC= "//span[text()=\"Update policy\"]";
	String Update_policy_Pro= "//button[@title='Click for more actions on policy Property']";
	String Update_logo_Pro= "//span[text()='Update policy']";
	String BTN_Hamburger="//a[@id='appview-nav-toggle-one']";
	String NewOption="//span[text()='New']";
	String ClaimNew="//span[text()='Make a claim']";
	
	String strFrame1 = "PegaGadget0Ifr";

}
