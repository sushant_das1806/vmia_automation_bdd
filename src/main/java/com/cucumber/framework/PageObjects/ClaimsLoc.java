package com.cucumber.framework.PageObjects;

public interface ClaimsLoc {
	
//	String date_time_of_loss="//img[@data-test-id='20160805071841097826481-DatePicker']";
	String date_time_of_loss="//*[contains(@name,'CalendarImg')]";
			//"//img[@name='CalendarImg-8b3e1147']";	
	String contact="(//input[@type='radio'])[1]";
	String today_link="//a[@id='todayLink']";
	String today_linkk="//input[@id='e25c595f']";
//	String today_link="//a[@data-day='21']";
	String date_time_of_loss_text="//input[@data-test-id='20160805071841097826481']";
	//String loss_type="//select[@data-test-id='20160805071841097927435']";
	String  loss_type= "//option[@value=\"Group Personal Accident\"]/ancestor::select";
	String  Loss_type= "//select[@id='f358082d']";
	String Loss_type_New= "//option[@title=\"Select\"]//parent::select";
	String Losstypedropdown="//select[@id='f358082d']";
	//String continue_button="//button[@data-test-id='20161017110917023176385']";A
	String continue_button="//button[text()='Continue']";
	String claims_Coverage="$PpyWorkPage$pDamageType";
	String TravelInsurance_Coverage_Domestic="//label[@for='04054060TVD']";
	String TravelInsurance_Coverage_Internantional="//label[@for='04054060TVI']";
	String loss_Calender="//img[@alt='Choose from calendar']";
	String lossdetails_date="//a[text()='Today']";
	String lossdetails_detail="//textarea[@id='f4dc6506']";
	String lossdetails_thirdparty="//input[@id='a987529e']";
	String claimnumber_ele="//div[@class='flex-paragraph']//p[contains(text(),'Thank')]";
	String claimnumber_ele1="(//div[@class='flex-paragraph']//p[contains(text(),'Thank')])[4]";
	String finish_button="//button[text()='Finish']";
	String Cf_policynumer="(//h2[contains(text(),'Policy no. ')])";
	String policynumer="(//h2[contains(text(),'Policy no. ')])[1]";
	String claimnumber_link="//a[@class='custom_link_header_text']";
	String save_and_exit_button="//button[@data-test-id='202004221935150856869']";
	//String business_select_row = "//span[@data-test-id='20160805071930037372243' and text()='Buten Bearings']";
	String business_select_row = "//input[@name='$PpyWorkPage$pPolicyListOfInsured$l1$ppyRowSelected' and @type='radio']";
	String Policynumber="//input[@name='$PpyWorkPage$pPolicyListOfInsured$l1$ppyRowSelected' and @type='radio']//following::span[1]";
	String business_select_roww = "//input[@name='$PpyWorkPage$pPolicyListOfInsured$l2$ppyRowSelected' and @type='radio']";
	String policyeffectivdate="//div[1]/div[text()='Policy effective date']";
	String policy_table = "//table[@id='bodyTbl_right']/tbody/tr";
	String fnol_case_id = "//span[@data-test-id='20141009112850013217103']";
	//String case_status="//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style) and contains(@class,'remove-top-spacing')]/span";
	String case_status="//h1[text()='Insured client details']/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	String final_case_status = "//p/ancestor::span/preceding-sibling::span//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	String final_case_statuss = "//div[2]/span[@class='badge_text'][last()]";
	String error_message_identify_loss_screen = "//div[@class='custom_iconError']/following-sibling::div/div";
	String downloadClaimFormLink = "//a/span[text()='Download GPA claim form']"; 
	String error_message_cannot_be_future_date = "(//div[@class='custom_iconError']/following-sibling::div/div)[2]";
	String source_claims_dropdown="//i[@class='caret-down-img']";
	String source_claims_option="//div[@class='multiselect-main multiselect-scroll']//ul/li/";

	String ActionButtonInClaimCompletion="//div[@class='pzbtn-mid' and @role='button']";
	String RefershButton="//span[@class='menu-item-title-wrap']";

	String claim_lodge = "//label[text()='";

	String claim_Cancel_Sensitive_Claim_Yes="//label[@for='5a5c74c0true']";
	String claim_Cancel_Sensitive_Claim_No="//label[@for='5a5c74c0false']";
	String claim_cancel_records_Schedule="$PpyWorkPage$pClaimData$pNoticeData$pRecordsSchedule";
	String claim_cancel_cancelReason="$PpyWorkPage$pCancelReason";
	String claim_cancel_claim="//span[text()='Cancel claim' and @class='menu-item-title']";

	String mi_claim_contact_No="//input[@id='94753a50false']//following-sibling::label";
	String mi_claim_contact_Yes="//input[@id='94753a50false']//following-sibling::label";
	String mi_claim_status="(//span[@class='badge_text'])[3]";
	String claim_transfer_User="$PreAssignPage$ppyReassignOperatorName";
	String claim_transfer_button="TransferClaim_pyWorkPage_23";
	String userFromSystem="//label[text()='User from the system']";
	String claimsAdmin="//span[text()='ClaimsAdmin']";
	String claimsUser="//span[text()='ClaimsOfficer1R3']";
	String transferBtn = "//button[@title='Click here to submit']";
	String closeBtn = "//button[@title='Close modal']";
	String roleTF = "$PpyBulkProcessingPage$ppyConditionList$l2$ppyTempText";
	String filterBtn = "pyBulkProcessingSearch_pyBulkProcessingPage_20";
//	String claimFileNum = ".tStrCntr li:nth-child(2) tr td:nth-child(2) span";
	String claimFileNum = "//*[@class='tStrCntr']//li[2]/span//tr/td[2]/span";
	String claimFileFilter = "//div[text()='Claim file']/parent::div/parent::div//a";
	String claimsFileTF = "//input[@id='83966c1b']";
	String adminApplyBtn = "//button[text()='Apply']";

	
	String mi_claim_claimcatogery="//label[@for='010c7cf5";
	String mi_claim_solictorneed_No="//label[@for='9d6893dbfalse']";
	String mi_claim_solictorneed_Yes="//label[@for='9d6893dbtrue']";
	String mi_claim_isclaimsentive_No="//label[@for='bc38ef29false']";
	String mi_claim_isclaimsentive_Yes="//label[@for='bc38ef29true']";

	String claimDetails = "$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer2";
	String ClaimDOL = "//label[text()='Claim']";
	String EPLDOL = "//label[text()='EPL']";
    String Construction_AnyWitness_No="//label[@for='74ddc9c2false']";
	String Construction_AnyWitness_Yes="//label[@for='74ddc9c2true']";
	String Construction_ReportedPolicy_Yes="//label[@for='a41c0949true']";
	String Construction_ReportedPolicy_No="//label[@for='a41c0949false']";
	String construction_contract="$PpyWorkPage$pClaimData$pNoticeData$pContractData$pPartyName";
	String construction_contractDate="$PpyWorkPage$pClaimData$pNoticeData$pContractData$pDateOfContractSigned";
	String Construction_dueDate="$PpyWorkPage$pClaimData$pNoticeData$pContractData$pDateOfPracticalCompletion";
	String Construction_nameOfThePartymakingClaim="$PpyWorkPage$pClaimData$pNoticeData$pContractData$pClaimantPartyName";
	String construction_commenceofWorkdate="$PpyWorkPage$pClaimData$pNoticeData$pContractData$pDateOfCommencement";
	String construction_Valuecontract="$PpyWorkPage$pClaimData$pNoticeData$pContractData$pValueOfContract";
	String construction_namePartyClaim="$PpyWorkPage$pClaimData$pNoticeData$pContractData$pClaimantPartyName";
	String construction_WhenIncidentIsReported="$PpyWorkPage$pClaimData$pNoticeData$pDamageToProperty$pIncidentReported";
	String construction_HowDamageOccur="$PpyWorkPage$pClaimData$pNoticeData$pDamageToProperty$pDamageOccur";
	String construction_ValueOfDamage="$PpyWorkPage$pClaimData$pNoticeData$pDamageToProperty$pDamageValue";
	String construction_DescriptionDamage="//table[@pl_prop='.DamagedItemList']//input";

	String construction_WhenFirstIncidentRepored="$PpyWorkPage$pClaimData$pNoticeData$pPersonalInjury$pIncidentReported";
	String construction_names_InjuredPersons="$PpyWorkPage$pClaimData$pNoticeData$pPersonalInjury$pNameOfInjuredPeople";
	String construction_NatureOfInjury="$PpyWorkPage$pClaimData$pNoticeData$pPersonalInjury$pNatureOfInjury";
	String construction_TreatmentProvided="$PpyWorkPage$pClaimData$pNoticeData$pPersonalInjury$pTreatmentProvided";
	String construction_IncidentWhatHappended="$PpyWorkPage$pClaimData$pNoticeData$pPersonalInjury$pParticularsOfIncident";

	String checkDetailErrorMessge="//span[text()='Value can not be blank']";

	String fineArts_claims_typeincident="$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer1";
	String fineArts_claims_incidentDetails="$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer2";
	String fineArts_claims_incidentAddress="$PpyWorkPage$pClaimData$pNoticeData$pAddressLine1";
	String travelClaim_IsEmployeeOfInsuranceComapny_Yes="//label[@for='53b348fctrue']";
	String travelClaim_IsEmployeeOfInsuranceComapny_No="//label[@for='53b348fcfalse']";
	String Claimant_Name = "$PpyWorkPage$pClaimData$pNoticeData$pClaimantDetails$pClaimantName";
	String travelClaim_IsBusinessAuthorisedTravel_Yes="//label[@for='386c0e17true']";
	String travelClaim_IsBusinessAuthorisedTravel_No="//label[@for='386c0e17false']";
	String travelClaim_DepartureDate="$PpyWorkPage$pClaimData$pNoticeData$pTravelDetails$pDepartureDate";
	String travelClaim_ReturnDate="$PpyWorkPage$pClaimData$pNoticeData$pTravelDetails$pReturnDate";
	String travelClaim_DepartureCity="$PpyWorkPage$pClaimData$pNoticeData$pTravelDetails$pCityOfDeparture";
	String travelClaim_IncidentCity="$PpyWorkPage$pClaimData$pNoticeData$pTravelDetails$pIncidentCity";
	String travelClaim_IncidentCountry="$PpyWorkPage$pClaimData$pNoticeData$pTravelDetails$pIncidentCountry";
	String travelClaim_IncidentExactLocatin="$PpyWorkPage$pClaimData$pNoticeData$pTravelDetails$pIncidentLocation";
	String travelClaim_HowDamageOccur="$PpyWorkPage$pClaimData$pNoticeData$pArticleDamage$pDamageOccur";
	String travelClaim_StepsToRecoverArticle="$PpyWorkPage$pClaimData$pNoticeData$pArticleDamage$pRecoverLostArticleExplanation";
	String travelClaim_WasReportMadeTothePolice_Yes="//label[@for='f7597cc7true']";
	String travelClaim_WasReportMadeTothePolice_No="//label[@for='f7597cc7false']";
	String travelClaim_IsArticleLostByCarrier_Yes="//label[@for='d0d5c05etrue']";
	String travelClaim_IsArticleLostByCarrier_No="//label[@for='d0d5c05efalse']";
	String travelClaim_IsComplainLodgetAgainstCarrier_Yes="//label[@for='5f44f9fftrue']";
	String travelClaim_IsComplainLodgetAgainstCarrier_No="//label[@for='5f44f9fffalse']";
	String travelClaim_IsAllMissingArticlesYourProerpty_Yes="//label[@for='423252fetrue']";
	String travelClaim_IsAllMissingArticlesYourProerpty_No="//label[@for='423252fefalse']";
	String travelClaim_ComplainDesctiptionOfCarrier="$PpyWorkPage$pClaimData$pNoticeData$pArticleDamage$pLodgedComplaintDesc";
	String EmRePSSMotor_IncidentReportDate = "//span[@id='$PpyWorkPage$pClaimData$pNoticeData$pIncidentReportDateSpan']/input";
	String EmRePSSMotor_IncidentDetails = "f4dc6506";
	String EmRePSSMotor_name = "a987529e";
	String fineArts_claims_Artist="//input[contains(@name,'$PpyWorkPage$pClaimData$pNoticeData$pDamagedItemList') and  contains(@name,'Artist') ]";
	String fineArts_claims_titleOfWork="//input[contains(@name,'$PpyWorkPage$pClaimData$pNoticeData$pDamagedItemList') and  contains(@name,'LossDesciption') ]";
	String fineArts_claims_Dimensions="//input[contains(@name,'$PpyWorkPage$pClaimData$pNoticeData$pDamagedItemList') and  contains(@name,'Dimensions') ]";
	String fineArts_claims_Owner="//input[contains(@name,'$PpyWorkPage$pClaimData$pNoticeData$pDamagedItemList') and  contains(@name,'pOwnerName') ]";
	String fineArts_claims_Worth="//input[contains(@name,'$PpyWorkPage$pClaimData$pNoticeData$pDamagedItemList') and  contains(@name,'pAmount') ]";
	String fineArts_claims_thirdPartyResponsible_Yes="//label[@for='5e2c9b29true']";
	String fineArts_claims_thirdPartyResponsible_No="//label[@for='5e2c9b29false']";
	String fineArts_claims_ReportedPolice_Yes="//label[@for='db1a82fetrue']";
	String fineArts_claims_ReportedPolice_No="//label[@for='db1a82fefalse']";

	String DOAClaim_InsuredOfficer="$PpyWorkPage$pClaimData$pNoticeData$pInsuredOfficer";
	String DOAReporteddateImg="//*[@name='$PpyWorkPage$pClaimData$pNoticeData$pIncidentReportDate']/following-sibling::img";
	String DOATextField="//*[@name='$PpyWorkPage$pClaimData$pNoticeData$pClaimSummary']";
	String DOAcurrent_date="//a[@id='todayLink']";
	String DOAClaim_Firstname="$PpyWorkPage$pClaimData$pNoticeData$pFirstName";
	String DOAClaim_Lastname="$PpyWorkPage$pClaimData$pNoticeData$pLastName";
	String DOAClaim_EmployeePosition="$PpyWorkPage$pClaimData$pNoticeData$pEmployeePosition";
	String DOAClaim_EmployeeDuty="$PpyWorkPage$pClaimData$pNoticeData$pEmployeeDuty";
	String DOAClaim_Salary="$PpyWorkPage$pClaimData$pNoticeData$pEmployeeSalary";
	String DOAClaim_TotalRumenearation="$PpyWorkPage$pClaimData$pNoticeData$pEmployeeRemuneration";
	String DOAClaim_DoesEmployeeHaveContract_Yes="//label[@for='06026aedtrue']";
	String DOAClaim_DoesEmployeeHaveContract_No="//label[@for='06026aedfalse']";
	String DOAClaim_CoveredByEnterpriseAgreement_Yes="//label[@for='8bd258a3true']";
	String DOAClaim_CoveredByEnterpriseAgreement_No="//label[@for='8bd258a3false']";
	String DOAClaim_DoesEmployeeHavePersonalFile_Yes="//label[@for='afa88df2true']";
	String DOAClaim_DoesEmployeeHavePersonalFile_No="//label[@for='afa88df2false']";
	String DOAClaim_IsTerminiationOccured_Yes="//label[@for='59bbb1d4true']";
	String DOAClaim_IsTerminiationOccured_No="//label[@for='59bbb1d4false']";
	String DOAClaim_DidEmployeeResing_Yes="//label[@for='df11d6fatrue']";
	String DOAClaim_DidEmployeeResing_No="//label[@for='df11d6fafalse']";
	String DOAClaim_IsEmployeeActionDisrupts_Yes="//label[@for='31fd16aftrue']";
	String DOAClaim_IsEmployeeActionDisrupts_No="//label[@for='31fd16affalse']";
	String DOAClaim_HasEmployeeComencedProceding_Yes="//label[@for='a4e78031true']";
	String DOAClaim_HasEmployeeComencedProceding_NO="//label[@for='a4e78031false']";
	String DOAClaim_HasEmployeeInProbation_Yes="//label[@for='58deee96true']";
	String DOAClaim_HasEmployeeInProbation_No="//label[@for='58deee96false']";
	String DOAClaim_IsYourEmplyeeConuntLessThan15_Yes="//label[@for='9f4db69ctrue']";
	String DOAClaim_IsYourEmplyeeConuntLessThan15_No="//label[@for='9f4db69cfalse']";
	String DOAClaim_IsLegalRepresentationAdded_Yes="//label[@for='29998bd9true']";
	String DOAClaim_IsLegalRepresentationAdded_No="//label[@for='29998bd9false']";

	String DOAClaim_EmployeementJoindate="$PpyWorkPage$pClaimData$pNoticeData$pEmployeeCommencementDate";
	String DOAClaim_EmployeeAgeAtTermination="$PpyWorkPage$pClaimData$pNoticeData$pTerminationAge";
	String DOAClaim_EmployeeComplain="$PpyWorkPage$pClaimData$pNoticeData$pNatureOfComplain";
	String DOAClaim_EmployeeComplainXpath="//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pNatureOfComplain']";
	String DOAClaim_TerminationReason="$PpyWorkPage$pClaimData$pNoticeData$pTerminationReason";
	String DOAClaim_ChallengeReason="$PpyWorkPage$pClaimData$pNoticeData$pChallengeReason";
	String DOAClaim_TerminationAmount="$PpyWorkPage$pClaimData$pNoticeData$pTerminationAmount";

	String motorvechicle_WhereDamageOccured="$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer1";
	String motorvechicle_WhatDamageOccured="$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer2";
	String motorvechicle_OtherReleventDetail="$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer3";
	String motorvechicle_HowDamageOccured="$PpyWorkPage$pClaimData$pNoticeData$pMotorLossDescription";
	String motorvechicle_WhereLossDamaged="$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer1";
	String motorvechicle_wasIncidentReportToPolice_Yes="//label[@for='db1a82fetrue']";
	String motorvechicle_wasIncidentReportToPolice_No="//label[@for='db1a82fefalse']";
	String motorvechicle_AddVehicleButton="VehiclesDamageWrapper_pyWorkPage.ClaimData.NoticeData_26";
	String motorvechicle_VehicleType="//input[contains(@name,'$PpyWorkPage$pClaimData$pNoticeData$pVehicleDamageList') and contains(@name,'VehicleType')]";
	String motorvechicle_RegistrationNumber="//input[contains(@name,'$PpyWorkPage$pClaimData$pNoticeData$pVehicleDamageList') and contains(@name,'RegistrationNumber')]";
	String motorvechicle_OwnerLease="//input[contains(@name,'$PpyWorkPage$pClaimData$pNoticeData$pVehicleDamageList') and contains(@name,'pOwnerOrLessee')]";


	String mi_claim_patientname="$PpyWorkPage$pClaimData$pNoticeData$pPatientDetails$pFirstName";
	String mi_claim_patintFamilname="$PpyWorkPage$pClaimData$pNoticeData$pPatientDetails$pFamilyName";
	String mi_claim_patientGenderDropdwon="$PpyWorkPage$pClaimData$pNoticeData$pPatientDetails$pGender";
	String mi_claim_patientDOB="CalendarImg-164cfaab";
	String mi_claim_medicalreocrdno="$PpyWorkPage$pClaimData$pNoticeData$pPatientDetails$pUnitRecordNo";
	String mi_claim_admissonstaus="$PpyWorkPage$pClaimData$pNoticeData$pPatientDetails$pAdmissionStatus";
	String mi_claim_DOB_month="//table[@class='calendar']//tbody//span[@id='monthSpinner']/input";
	String mi_claim_DOB_year="//table[@class='calendar']//tbody//span[@id='yearSpinner']/input";
	
	String mi_claim_claiment_Yes="//label[@for='70e4c35atrue']";
	String mi_claim_claiment_No="//label[@for='70e4c35afalse']";
	String mi_claim_doIncident="CalendarImg-e07b40fc";
	String mi_claim_datefirstaware="CalendarImg-9049094e";
	String mi_incidentDetail="//textarea[@name='$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer2']";
	String mi_IncidentFact="$PpyWorkPage$pClaimData$pNoticeData$pFactualAccount";
 
	String mi_claim_clincalspc="$PpyWorkPage$pClaimData$pNoticeData$pClincalSpecialityDetails";
	String mi_claim_areaincident="$PpyWorkPage$pClaimData$pNoticeData$pAreaOfincident";
	String mi_claim_disclosureOccure_No="//label[@for='8d956f72false']";
	String mi_claim_disclosureOccure_Yes="//label[@for='8d956f72true']";
	String mi_claim_disclosureOccure_Unknown="//label[@for='8d956f72Unknown']";
	
	String lit_update="EnterLitigationDetails_pyWorkPage_31";
	String lit_legal_status = "$PpyWorkPage$pLitigationDataRef$pLitigationDetails$pLegalStatus";
	String lit_vima_litstatus="$PpyWorkPage$pLitigationDataRef$pLitigationDetails$pLitigationStatus";
	String lit_vima_lit_jurdication="$PpyWorkPage$pLitigationDataRef$pLitigationDetails$pJurisdiction";
	String lit_vima_courtnumer="$PpyWorkPage$pLitigationDataRef$pLitigationDetails$pCourtNumber";
	String lit_dateIssued="CalendarImg-311034b6";
	String lit_dateServed="CalendarImg-c607ae55";
	String lit_defendent="$PpyWorkPage$pLitigationDataRef$pLitigationDetails$pDefendantList$l1$ppyFullName";
	String lit_Solicitor = "$PpyWorkPage$pLitigationDataRef$pLitigationDetails$pDefendantList$l1$pSolicitorName";
	String lit_insurer = "$PpyWorkPage$pLitigationDataRef$pLitigationDetails$pDefendantList$l1$pInsurer";
	String lit_Claim_number  = "$PpyWorkPage$pLitigationDataRef$pLitigationDetails$pDefendantList$l1$pClaimNumber";


	String vima_logo="//img[@aria-label='vmia logo']";
	String myOpencase="//h3[text()='My open cases']";
	String ClaimNumber_MyOpenCase="(//table[@summary='My open cases table']//tr/td[1]/div/span)[1]";
	
	String lit_event_name="$PpyWorkPage$pLitigationDataRef$pEventsList$l1$pEventName";
	String lit_event_date="//img[@data-test-id='20200909124232002646-DatePicker']";
	String lit_event_rescomments="$PpyWorkPage$pLitigationDataRef$pEventsList$l1$pEventResolutionComment";
	String lit_Resouttion_resoultion="$PpyWorkPage$pLitigationDataRef$pResolution$pResolutionType";
	String lit_Resouttion_date="CalendarImg-e49e2f7d";
	String lit_Resouttion_amount="$PpyWorkPage$pLitigationDataRef$pResolution$pResolutionAmount";
	String lit_Resouttion_cost="$PpyWorkPage$pLitigationDataRef$pResolution$pResolutionCost";
	String lit_Resouttion_nonmontery="$PpyWorkPage$pLitigationDataRef$pResolution$pResolutionNonMonetary";
	
	String lit_Events_tab="//h3[@class='layout-group-item-title' and text()='Events']";
	String lit_Resoultions_tab="//h3[@class='layout-group-item-title' and text()='Resolution']";
	String lit_Timetable_tab = "//h3[@class='layout-group-item-title' and text()='Timetable']";
	String lit_Settlement_tab = "//h3[@class='layout-group-item-title' and text()='Settlement']";
	String lit_Attachment_tab = "//h3[@class='layout-group-item-title' and text()='Attachments']";

	String Add_litigation  = "//span[text()='Add litigation']";
	String Add_litigation1 = "//button[text()='Add litigation']";

	String lit_mark_as_settled = "//button[text()='Mark as settled']";

	String lit_settlement_Yes = "//label[@for='5057f59etrue']";
	String lit_settlement_No = "//label[@for='5057f59efalse']";

	String lit_VMIA_contribution = "$PpyWorkPage$pLitigationDataRef$pResolution$pVMIAContribution";
	String lit_Gen_damage = "$PpyWorkPage$pLitigationDataRef$pResolution$pSettlements$gContribution$pGeneralDamages";

	//Timetable
	String  lit_add_event = "LitigationEvents_pyWorkPage.LitigationDataRef_6";
	String lit_select_event ="$PEventPage$pEventName";
	String lit_event_date1 = "CalendarImg-967569e9";
	String lit_responsible_person = "$PEventPage$pName";
	String concluded_Yes = "//label[text()='Yes']";
	String concluded_No= "//label[text()='No']";
	String Actual_date = "$PEventPage$pActualDate";
	String event_notes_frame = "//iframe[@class='cke_wysiwyg_frame cke_reset']";
	String event_notes = "//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']";
	String event_add_notes = "EventDetails_EventPage_14";
	String lit_Save = "//button[@id='ModalButtonSubmit']";
	String Timetable_history_data = "//table[@pl_prop='D_EventsHistory.pxResults']//tr/td[5]//span";
	String Timetable_history = "//h4[text()='History']";
	//Settlement
	String lit_settlement_type = "$PpyWorkPage$pLitigationDataRef$pResolution$pResolutionType";

	//Attachment
	String Attachment_add_new = "AddCaseAttachments_pyWorkPage_5";
	String SelectFile_button = "//div[@id='uniqueIDforMultiFilePath']//table";
	String AttachFile_button="//button[@title='Attach']";
	String Attach_Category = "//select[@data-test-id='20170515091207074853986']";


	String lit_vima_partyType="//label[@for='bb0dbffa";
	String lit_vima_Solicitor="$PpyWorkPage$pLitigationDataRef$pLitigationDetails$pFirmName";
	String lit_vima_appointmentdate="CalendarImg-3501f8b8";
	String lit_reasonforSolitorsele="$PpyWorkPage$pLitigationDataRef$pLitigationDetails$pComments";
	String lit_add_defedent="LitigationDetails_pyWorkPage.LitigationDataRef.LitigationDetails_258";
	String lit_Submit="EnterLitigationDetails_pyWorkPage_81";
	//String where_did_loss_occur = "//textarea[@data-test-id='202011041543400496644' and @id ='6dd534bc']";
	String where_did_loss_occur = "//span/textarea[@id='6dd534bc']";
	String what_is_the_loss = "//textarea[@id ='f4dc6506']";
	//String what_is_the_loss = "//textarea[@data-test-id='202011041549560326225' and @id ='f4dc6506']";
	String how_loss_occur = "//textarea[@data-test-id='202004291654190427989' or @id ='35b1cbe9']";
	String Serch_orgname="$PpyWorkPage$pSearchCustomerInfo$pCustomerInfo$pBusinessUnit";
	String Search_findButton="//button[text()='Find']";
	String Search_searchrestult="//input[@type='radio']";
	String select_general_damages = "//input[@id ='98b57c38']";
	String isDet_Yes="//label[@for='0bf97a46true']";
	String isDet_No="//label[@for='0bf97a46false']";
	String isRepairedByAcer_Yes="//label[@for='a8596d1atrue']";
	String isRepairedByAcer_No="//label[@for='a8596d1afalse']";
	String addBtnCssClass = ".pi.pi-plus-box";
	String desriptionNameLoc = "$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l1$pItemDescription";
	String originalPurchaseNameLoc = "$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l1$pAmount";
	String assetNumberNameLoc = "$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l1$pSerialNumber";

	String addNewLinkXpath = "//a[text()='Add New Line']";
	String policyNumbrs = "$PpyWorkPage$pApplicationPage$pPolicyData$pInvoiceLineItems$l1$pPolicyNumber";
	String productNames = "$PpyWorkPage$pApplicationPage$pPolicyData$pInvoiceLineItems$l1$pProductName";
	String BasePremium = "$PpyWorkPage$pApplicationPage$pPolicyData$pInvoiceLineItems$l1$pBasePremium";
	String GSTValue = "$PpyWorkPage$pApplicationPage$pPolicyData$pInvoiceLineItems$l1$pSurchargeValue$gGST";
	String StampDuty = "$PpyWorkPage$pApplicationPage$pPolicyData$pInvoiceLineItems$l1$pSurchargeValue$gStampDuty";
	String justificationComments="$PpyWorkPage$pApplicationPage$ppyNote";
	String checkboxDeclaration="//input[@name='$PpyWorkPage$pApplicationPage$pTermsAcceptance' and @type='checkbox']";
	String beginCase = "(//button[@title='Begin'])[last()]";
	String selectQueueDropdown="//i[@title='Select work owner']";
	String UnderwritingOption="//div[@class='content-item content-field item-1 remove-top-spacing remove-left-spacing remove-bottom-spacing remove-right-spacing flex flex-row field-name-ellipsis dataValueRead']/";
	String UW_Casenumber_TH1="(//div[@class='gridHeaderLabel '] [text()='Case number'])[1]";
	String filterOption_Asset="//table[@id='gridLayoutTable']/tbody[@id='gridTableBody']/tr/th[2]//div/a[@title='Open Menu']";
	String UW_Casenumber_TH="(//div[@class='gridHeaderLabel '] [text()='Case ID'])[1]";
	String filterOption="(//table[@id='gridLayoutTable']/tbody[@id='gridTableBody']/tr/th[3]//div/a[@title='Open Menu'])[1]";
	String uw_applyfilter = "//span[text()='Apply filter']";
	String UW_search_text = "//div[@pyclassname=\"Embed-FilterColumn\"]//span[@data-control-mode='input']/input";
	String UW_search_applyfilter = "//button[@data-test-id='201604060130370006117741']";
	String uw_clickcase="//table[@id='gridLayoutTable']/tbody[@id='gridTableBody']/tr[2]//a";

	//String general_damages_question_answer_no = "//label[@data-test-id='202004212133380623557-Label' and @for='779919ac']//following-sibling::div//label[text()='No']";
	String scip_add_button = "//a[@data-test-id='20160809064449004925731']";
	String scip_purchase_desc = "(//input[@data-test-id='2016072109335505834280'])[1]";
	String scip_datepicker = "//img[@data-test-id='2016072109335505834280-DatePicker']";
	String scip_purchase_cost = "(//input[@data-test-id='2016072109335505834280'])[2]";
	String scip_salvage_value = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pItemListSCIP$l1$pResaleValue']";
	
	String UrgencyYes="//input[@id='9fdfd6ebtrue']//following-sibling::label";
	String UrgencyNo="//input[@id='9fdfd6ebfalse']//following-sibling::label";
	
	String clientbanking_account_name = "//input[@name='$PpyWorkPage$pCustomer$pCustomerInfo$pKeyFinancials$pAccountName']";
	String clientbanking_bsb = "//input[@name='$PpyWorkPage$pCustomer$pCustomerInfo$pKeyFinancials$pBSBValue']";
	String clientbanking_account_number = "//input[@name='$PpyWorkPage$pCustomer$pCustomerInfo$pKeyFinancials$pBankAccountNumber']";
	
	String clientbanking_bank_name = "//input[@name='$PpyWorkPage$pCustomer$pCustomerInfo$pKeyFinancials$pBankName']";
	String clientbanking_email = "//input[@name='$PpyWorkPage$pCustomer$pCustomerInfo$pKeyFinancials$pEmailAddress']";
	String student_address_hyperlink = "//a[@name='EditAddressDetails_pyWorkPage.ClaimData.NoticeData.StudentDetails.AddressDetails_7']";
	String lnkcannotFindAddress="//a[contains(@name,'EditAddressDetails')]";
	String claimantdetails_student_name = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pStudentDetails$ppyName']";
	String claimantdetails_date_of_birth = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pStudentDetails$pDateOfBirth']";
	String claimantdetails_address_line1 = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pStudentDetails$pAddressDetails$pAddressLine1']";
	String claimantdetails_address_line2 = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pStudentDetails$pAddressDetails$pAddressLine2']";
	String claimantdetails_suburb = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pStudentDetails$pAddressDetails$pcity']";
	String parent_address_hyperlink = "//a[@name='EditAddressDetails_pyWorkPage.ClaimData.NoticeData.ParentDetails.AddressDetails_7'][@title='Cannot find your address']";
	String claimantdetails_state = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pStudentDetails$pAddressDetails$pState']";
	//String claimantdetails_state = "//select[@id='18eaf412']";
	String claimantdetails_postcode = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pStudentDetails$pAddressDetails$pZipCode']";
	String claimantdetails_parent_name = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$ppyName']";
	String claimantdetails_relationship = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pRelationshipToStudent']";
	String claimantdetails_parent_address_line1 = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pAddressDetails$pAddressLine1']";
	String claimantdetails_parent_address_line2 = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pAddressDetails$pAddressLine2']";
	String claimantdetails_parent_suburb = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pAddressDetails$pcity']";
	String claimantdetails_parent_state = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pAddressDetails$pState']";
	String claimantdetails_parent_postcode = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pAddressDetails$pZipCode']";
	String claimantdetails_parent_telephone = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pMobileNumber']";
	String claimantdetails_parent_email = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pEmailAddress']";
	//String accidentdetails_description= "//textarea[@data-test-id=\"20201104151525087338\" and  @validationtype= \"required,calculateexpression\"]";
	String accidentdetails_description= "//textarea[@id='e11da520']";
	//String accidentdetails_description = "//textarea[@name ='$PpyWorkPage$pClaimData$pNoticeData$ppyQuestionGroup$gAccidentDetails$ppyQuestion$gAccidentDetails$ppyAnswer']";
	//String accidentdetails_accident_reportedto = "//textarea [@data-test-id=\"202004281741210837133\" and @validationtype = \"required\" ]";
	String accidentdetails_accident_reportedto = "//textarea[@id='1cccbaf1']";
	//String accidentdetails_injury_details = "//textarea[@name ='$PpyWorkPage$pClaimData$pNoticeData$pInjuryDetails']";
	String accidentdetails_injury_details = "//textarea[@id='93c8b1cd']";
	String checkbox_declaration = "//input[@name='$PpyWorkPage$pDeclarationConsent' and @type='checkbox']";
	String left_toggle = "//a[@id='appview-nav-toggle-one']";
	String my_work = "//span[text()='My work']";
	String my_review_tab="//h3[text()='My review cases']";
	String reports = "//span[text()='Reports']";
	String All_Claims="//a[text()=\"All claims\"]";
	String header="//h1[text()=\"All claims\"]";
	String header1="//h1[text()='All Litigated claims']";
	String All_Litigated_Claims = "//a[text()=\"All Litigated claims\"]";
	String Litigation_Reports = "//a[text()='Litigation reports']";
	String File_Claim= "(//div[@aria-describedby='titleDesc1383409968'])[1]";
	String Filter_claim_File = "//table[@pl_prop_class='VMIA-CSP-Work-Claims-File']//div[text()='Claim number']";
	String Filter_Lit_File = "//table[@pl_prop_class='VMIA-CSP-Work-Claims-Litigation']//div[text()='Claim number']";
	String Apply_Filter="//span[text()='Filter']";
	String Filter_icon= "//table[@pl_prop_class='VMIA-CSP-Work-Claims-File']//div[text()='Claim number']/parent::div//parent::div//span/a";
	String Filter_lit_icon =  "//table[@pl_prop_class='VMIA-CSP-Work-Claims-Litigation']//div[text()='Claim number']/parent::div//parent::div//span/a";
	String report_claim_status_1="//td[@data-attribute-name='Claim status']//span[text()=\"Pending-Triage\"]";
	String report_claim_number = "//table[@pl_prop_class='VMIA-CSP-Work-Claims-Litigation']//td[@data-attribute-name='Claim number']";
	String LitigationTableResult = "//table[@pl_prop_class='VMIA-CSP-Work-Claims-Litigation']//tr/td[2]";
	String new_ham = "//span[text()='New']";
	String client_search = "//span[text()='Search']";
	String claimsOption="//label[text()='Claims']";
	String Search_ClaimNumberTextBox="$PPortalSearch$pClaimNumber";
	String search_SearchButtonClaim="//button[@title='Search for claims']";
	String caseidRec = "//tr[@data-test-id='201905310627430956121-R1']/td[3]";
	String org_search_button = "//button[@title='Search for Customer' and contains(@name,'OrganisationSearchWrapper')]";
	String contact_search_button = "//button[@title='Search for Customer' and contains(@name,'ContactSearchWrapper')]";
	String org_link = "//a[@data-test-id='202008311759290364176']";
	String ham_new_claim = "//span[text()='Make a claim']";
	String reports_claim_cases = "//a[@data-test-id='20150114100045010339744' and text()='Claim cases with their status']";
	String caseID_sort = "//th[@data-test-id='202009220008160281287-th-1']";
	String caseID_menu = "//div[text()='Case ID']/child::div/a[@title='Open Menu']";
	String dashboard_filter_caseid ="(//h2[text()='My worklist']/following::a[@title='Click to filter'])[1]";
	String ClaimNumberTh_myreviewtab="(//th[@data-test-id='20200922000743062867-th-1'])[2]";
	String filter_worklist = "//*[@id='gridTableBody']/tr[1]/th[2]/div[1]/div[3]/a";
	String filterIcon_myreviewtab="(//th[@data-test-id='20200922000743062867-th-1'])[2]//a[@role='menu']";
	String search_filter_textbox = "//h2[text()='My worklist']/following::input[@data-test-id='201411181100280377101613' and @name]";
	String search_fileter_textbox_myreview="//input[contains(@name,'ppySearchText')]";
	String search_filter_textbox_worklist = "//label[text()='Search text']/following::input";
	String apply_button_filter = "//h2[text()='My worklist']/following::input[@data-test-id='201411181100280377101613' and @name]/following::button[contains(@onclick,'OK')]";
	String apply_button_worklist = "//button[@data-test-id='201604060130370006117741']";
	String apply_button = "//button[text()='Apply']";
	String first_case_in_filter_table = "//a[@data-test-id='201410081557130000198739']";
	String triage_closing_reason = "//textarea[@data-test-id='202004090343060408932']";
	String dateed = "//img[@src='webwb/pzspacer_11792674401.gif!!.gif']";
	String App = "//label[text()='Approve claim']";
	String Cue = "//a[@id='todayLink']";
	String triage_closing_submit = "//button[text()='Submit']";
	String Per ="//select[@data-test-id='20200908192558052377']";
	String records_schedule = "//select[@data-test-id='20200908192558052377']";
	String ClaimReviewSLAtext="//span[text()='Review due date']/parent::div/div/span";
	String ClaimReviewButton="//h2[contains(text(),'Claim Review ')]/parent::div/span//button";
	String ClaimReview_reserverEnsure_Yes="//label[@for='1d9fc2dbtrue']";
	String ClaimReview_dataAccurate_Yes="//label[@for='4a8bd9abtrue']";
	String ClaimReview_clientComnuicaation_date="//img[@name='CalendarImg-3748e1c4']";
	String ClaimReview_claimReadyToClose_Yes="//label[@for='bf1f829dtrue']";
	String ClaimReview_comments="$PpyWorkPage$pComments";
	String ClaimReview_resolvebtn="//button[text()='Resolve']";	
	
	
	String attachment_button = "//button[@data-test-id='202005050306460434325']";
	String attachment_button_1 = "(//button[@data-test-id='202005050306460434325'])[1]";
	String attach_button = "//button[@title='Attach']";//button[@id='ModalButtonSubmit']
	String attachment_popup_category_dropdown = "//select[@data-test-id='20170515091207074853986']";
	String categorydropdown= "//select [@data-test-id=\"20170515091207074853986\"]";
	String drpCategory="//div[text()='Category']//following::select[1]";
	//String liab_loss_details_date = "//img[@data-test-id='202009090854440345629-DatePicker']";
	String liab_loss_details_date = "//img[@src='webwb/pzspacer_11792674401.gif!!.gif']";
	String liab_loss_details_investigation_name="//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pFormalInvestiagtionName']";
	String liab_loss_details_what_happened = "//textarea[@id='f4dc6506']";
	String liab_loss_details_claimant_name = "//input[@id='a987529e']";
	String Liab_Loss_details_ClickonLink = "//a[@name='EditNonMendatoryAddressDetails_pyWorkPage.ClaimData.NoticeData.ClaimantDetails.AddressInfo_39']";
	String liab_loss_details_claimant_address_line3 = "$PpyWorkPage$pClaimData$pNoticeData$pClaimantDetails$pAddressInfo$pAddressSelected";
	String liab_loss_details_claimant_address_line1 = "//input[@id='7bd6e473']";
	String liab_loss_details_claimant_address_line2 = "//input[@id='e2dfb5c9']";
	String liab_loss_details_claimant_suburb = "//input[@id='f8f257ef']";
	String liab_loss_details_claimant_state = "//select[@id='73865ff6']";
	String loss_details_claimant_student_state = "//select[@name='$PpyWorkPage$pClaimData$pNoticeData$pStudentDetails$pAddressDetails$pState']";
	String loss_details_claimant_parent_state = "//select[@name='$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pAddressDetails$pState']";
	//String liab_loss_details_claimant_postcode = "(//input[@data-test-id='2016080906093508875245'])[3]";
	String liab_loss_details_claimant_postcode = "//input[@id='d399ff39']";
	String liab_loss_details_nature_of_injury = "//textarea[@id='bd9223e2']";
	String liab_loss_details_treatment_provided = "//textarea[@id='16dabe30']";
	String propertyDamage = "//label[text()='Property']";

//PropertyEmrepss

	String loss_proEmprss_how = "//textarea[@id='f4dc6506']";
	String loss_proEmprss_damage = "$PpyWorkPage$pClaimData$pNoticeData$pDamagedItemList$l1$pLossDesciption";

	//proTeachers

	String loss_teacher_where = "$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer1";
	String loss_teacher_what = "$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer2";
	String loss_teacher_how = "$PpyWorkPage$pClaimData$pNoticeData$pPropertyLossDescription";
	String loss_teacher_schedule = "//input[@id='98b57c38']";
	String loss_Add = "//a[text()='Add']";
	String loss_damage_ItemDecirp = "$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l1$pItemDescription";
	String loss_damage_Amount = "$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l1$pAmount";

	String queue_dropdown = "//i[@title='Select work owner']";
	String finance_Recoveries = "//span[contains(text(),'Recoveries')]";
	//String finance_caseid = "(//div[text()='Case ID'])[last()]";
	String finance_recoveryid= "//th[@aria-label='Recovery case id']";
	//String finance_caseid_filter = "(//div[text()='Case ID']//a[@role='menu' and @title])[last()]";
	String finance_recovery_caseid_filter = "//th[@aria-label='Recovery case id']/div/div[3]/a";
	String finance_manualpayment_casenumber="//th[@aria-label='Claim number']";
	String finance_manualpayment_fitericon="//th[@aria-label='Claim number']/div/div[3]/a";
	String finance_applyfilter = "//span[text()='Apply filter']";
	String finance_search_text = "//input[@data-test-id='20160524050907021913810']";
	String finance_apply = "//button[text()='Apply']";
	String finance_case_link = "//a[@data-test-id='201501071438430852311277']";
	String queue_dropdownn = "//i[@alt='Select work owner']";
	String Wave = "(//table[@id='gridLayoutTable'])[2]/tbody/tr/th[3]/div[1]";
	String Wave1="//tbody/tr[1]/th[4]";
	String Wave2="//tbody[1]/tr[1]/th[4]/div[1]/div[3]/a[1]";
	String Wavee = "(//div[text()='Claim file'])[2]";
	String triage_claims = "//span[text()='Triage Claims']";
	String unassigned_claimss = "(//div[@data-expr-id='d801e4677e3832858af5893d102b1516c9b3505b_36'])[1]";
	String estimate_approval_workbasket = "//span[@data-test-id='2015010908480108866877' and text()='EstimateApproval']";
	String claimfile_id = "//table[@class='gridTable  row-compact']//div[text()='Claim number']";
	String claimfile_idi = "//table[@class='gridTable  row-compact']//div[text()='Claim number']";
	String claimfile_idd = "(//*[@id=\"gridTableBody\"]/tr[1]/th[3]/div[1]/div[3]/a)[3]";	
	String reports_claimfile_id = "(//th[@data-attribute-name='Claim file ID'])[last()]";
	String ceo_claimfile_id = "//table[@class=\"gridTable  row-compact\"]//div[text()=\"Claim file\"]";
	String claimfileid_filter = "//span[text()='Apply filter']";
	String reports_claimfileid_filter = "(//th[@data-attribute-name='Claim file ID']//a[@role='menu' and @title])[last()]";
	String claims_applyfilter = "//span[text()='Apply filter']";
	String clientNameApplyfilter = "//span[text()='Apply filter']";
	String clientNameCheckbox = "//div[@data-node-id='pzUniqueValues']/div/child::div[2]/parent::div/div/input[2]";
	String listOfClientNames = "//div[@data-node-id='pzUniqueValues']/div/child::div[2]";
	String firstCaseNumber = "(//tbody[@id='gridTableBody'])[2]/tr[2]/td[@aria-describedby='Case number']/span/a";
	String claims_triage_workbasket_filter = "//h2[text()='Triage claim work basket']/following::div[text()='Case']/following::a[@title='Click to filter'][1]";
	String reports_claims_applyfilter = "//span[text()='Filter']";
	String claims_search_textt = "//span[@data-control-mode='input']/input";
	String claims_search_text = "//span[@data-control-mode='input']/input";
	String reports_search_text = "//input[@data-test-id='201411181100280377101613']";
	String claims_apply = "//button[text()='Apply']";
	String claims_case_link ="//a[@title='Click here to open the object']";
			//"//tr[2]//td[2]//a[@title='Click here to open the object']";
	//tr[2]//td[2]//a[@title='Click here to open the object' and  contains(text(),'CU-')]
			//"//tr[3]//td[2]//a[@title='Click here to open the object']";
			//"//a[@title='Click here to open the object']";
	String claims_case_link2="//tbody//tr[2]/td[@class='gridCell gridCellSelected worklist-table-column-width worklist-left-padding' and @data-attribute-name='Case number']//span/a[@title='Click here to open the object']";
	String first_case_worklist = "//h2[text()='Triage claim work basket']/following::table[@pl_prop_class='Assign-WorkBasket']/descendant::a[contains(text(),'CF-')]";
	String first_Claim_Number = "//td[@aria-describedby='Case ID']/span";
	String searchresult_case_reviewtab="//td//a[contains(@name,'UserReviewCases')][1]";
	String claimreview_due_date="//td[@aria-describedby='Review Due In']/span";
	
	String report_claim_status = "//td[@data-attribute-name='Claim status']";
	//String zipcode = "//input[@data-test-id='20141219073811007312693']";A
	String zipcode = "//input[@id='e2f52e25']";
	//String find_button = "//button[@data-test-id='201604240253100057139194']";A
	String find_button = "//button[text()='Find']";
	//String select_business_checkbox = "//input[@type='radio' and contains(@name,'pz$ppxResults$l1$ppySelected')]";A
	String select_business_checkbox = "//input[@type='radio' and contains(@name,'$PD_Bus')]";
			//"//input[@type='radio' and contains(@name,'pz$ppxResults$l12$ppySelected')]";
	String select_contact_checkbox = "//input[@type='radio' and contains(@name,'pz$ppxResults$l1$ppySelected')]";
	String attach_button_claims = "//body/div[@id='PEGA_HARNESS']/form[1]/div[3]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/span[1]/button[1]";
	String attach_button_claimss = "//input[@id='$PpyAttachmentPage$ppxAttachName']";
	//String pending_triage_status = "//span[@data-test-id='20150116110353086421138']/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	String pending_triage_status = "//span[@class='vmia_case_header_id']/parent::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div/span[text()='Pending-Triage']";
			//"//span[@class=\"vmia_case_header_id\"]/parent::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div/span[text()=\"Pending-Triage\"]";
	String pending_triage_statusse = "//div[@data-test-id=\"201806191351520571476\"]//span/parent::div//following-sibling::div//following-sibling::div/span[text()=\"Pending-Approval\"]";
	String pending_triage_statuss = "//div[@data-test-id='201806191351520571476']//following-sibling::div//following-sibling::div/span[text()='Pending-Triage']";
	//String continue_button="//button[@data-test-id='20161017110917023176385']";
	String triage_actions_button = "//button[text()='Actions']";
	String triage_actions_new_button="//*[text()='Actions']/i";
	String transferclaim="//span[@class='menu-item-title' and text()='Transfer claim']";
			//"//div[@class=' content layout-content-inline_middle  content-inline_middle set-width-auto buttons-group']//button[text()='Actions']";
	String edit_claim_data = "//span[text()='Edit claim data']";
	String edit_claim_reason = "//span//textarea[@aria-describedby='$PpyWorkPage$pEditClaimReasonError']";
	String not_a_duplicate = "//label[text()='Not a duplicate']";
	String resolve_claim_as_duplicate = "//label[text()='Resolve claim as duplicate']";
	//String status_pending_claimunit_comp = "//span[text()='Pending-ClaimUnit Completion']/parent::div[not(@style)]/span";
	String status_pending_claimunit_comp = "//div[@class='content-item content-field item-3 remove-bottom-spacing remove-right-spacing flex flex-row badge-bg-wait centered dataValueRead']";
	String status_resolved_duplicate = "//span[text()='Resolved-Duplicate']/parent::div[not(@style)]/span";
	String duplicateWith_dropdown = "//Select[@name='$PpyWorkPage$ppyDuplicateID']";
	
	String user_to_transfer = "//input[@data-test-id='2015031809055206748487']";
	
	String transfer_button = "//button[@data-test-id='2014121801251706289770']";
	String portfolio_manager_pending_investigation = "//span/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	
	String triage_claims_officer_error_message = "//div[@data-test-id='201806191352060317785']//div[text()=' Operator record does not exist']";
	String approve_claim_button = "//label[text()='Approve claim']";
	String deny_claim_button = "//label[text()='Deny claim']";
	String awaiting_action_claim_button = "//label[text()='Awaiting action']";
	//Pavan
//	String edit_estimate = "//button[@data-test-id='202008072112590421946' and @name='EstimatesOverview_pyWorkPage_4']";
	String edit_estimate = "//h2[text()='Financials']/following-sibling::span//button[text()='Open  ']";
//	String update_estimate = "//a[@data-test-id='202008121552540230281']";
	String update_estimate = "//a[text()='Update']";
//	String edit_estimate_payable = "//input[@data-test-id='202008110333520748457']";
	String edit_estimate_payable = "//input[@name='$PEstimateInitiatePage$pOutstandingAmount']";
	String edit_estimate_outstanding ="//select[@name='$PTempPage$pEstimateChangeReason']";
			//"//select[@data-test-id='202010140019110781713']";
	String edit_estimate_recoverable = "//textarea[@data-test-id='202010140019110781198']";
//	String estimate_change_reason_dropdown ="//select[@data-test-id='202010140019110781713']";
	String estimate_change_reason_dropdown = "//select[@name='$PEstimateInitiatePage$pEstimateChangeReason']";
			// "//select[@data-test-id='202010140019110781713']";
//	String submit_estimate = "//button[@title='Click here to submit work']";
	String submit_estimate = "//button[text()='Submit']";
	String back_from_estimate = "//button[@data-test-id='202008072250100442921']";
	String SBT = "//button[contains(text(),'Submit')]";
	String back_from_estimatde= "//button[@data-test-id='202008072112590421946']";
	String denial_reasom = "//select[@data-test-id='202008142203160302661']";
	String settlement_date = "//img[@name='CalendarImg-2ba711f9']";
	String await_reason = "//select[@data-test-id='20200817175559024670']";
	String approve_button = "//button[@data-test-id='201410290229040300882']";
	String reject_button = "//button[@data-test-id='2014102902290403019634']";
	String claim_file_link = "//a[@data-test-id='2016010506263105433190']";
	String begin_analyse_claim_file = "//button[@data-test-id='201609091025020567152987']";
	
	String close_all_anchor = "//a[@aria-label='Currently open']";
	String close_all_text = "//td[text()='Close All']";
	
	String actions_button = "//button[@data-test-id='20141009112850013521365']";
	String legal_panel_option ="//span[text()='Add litigation']";
			// "//span[text()='Add legal panel']";
	String Make_payment_option = "//button[text()='Make payment']";
	String Make_payment_Pay_button="//a[@data-test-id='202010072325470764547']";
	String Make_payment_pay_btn = "//a[text()='Pay']";
	String Make_payment_options = "//a[@data-test-id='202010072325470764547']";
	String event_management_option = "//span[text()='Incident category']";
	String Make_Payment_invoceno="$PPayments$ppxResults$l1$pInvoiceID";
//	String Make_Payment_invoceno="//label[text()='Invoice number']/following-sibling::div/span/input";
	String paymentdescription = "//textarea[@name='$PPayments$ppxResults$l1$pPaymentDesc']";
	String Make_Payment_Payee="$PPayments$ppxResults$l1$pPersonInfo$pPayeeName";
	String payment_method="$PPayments$ppxResults$l1$ppaymentMethod$ptheMethod";
	String Make_Payment_Select="$PpyAttachmentPage$ppxAttachName";
	String Make_Payment_Attach_categoery="//select[@name='$PdragDropFileUpload$ppxResults$l1$ppyCategory']";
	String Make_payment_attachmentpop_attachbtn="//button[@id='ModalButtonSubmit']";
	
	String payment_mode_dropdown = "$PPayments$ppxResults$l1$pPaymentMode";
	String payment_mode_dropdownXpath = "//select[@name='$PPayments$ppxResults$l1$pPaymentMode']";
	String transaction_type = "//select[@data-test-id='2016082408081509437725']";
//	String payment_code = "//select[@name='$PPayments$ppxResults$l1$pPaymentCode']";
	String payment_code = "//label[text()='Payment code']/following-sibling::div/select";
	String payee_type = "//input[@data-test-id='202008182140400889182']";
	String Descdf = "//textarea[@name='$PPayments$ppxResults$l1$pPaymentDesc']";
	String invoiceNumber_tfield = "//input[@name='$PPayments$ppxResults$l1$pInvoiceID']";
	String payee_name = "//input[@data-test-id='20160824080815094415179']";
	String payment_amount = "//input[@data-test-id='20160824080815094741269']";
	String gst_exempt = "//label[text()='GST exempt']";
	String attachfileXpath="//button[text()='Attach from Claim File']";
	String attachCheckbox = "//tr[@id='$PD_AllClaimsAttachmentsEditable_pa41243796080878pz$ppxResults$l1']/td[1]/div/input[2]";

	String make_payment_button = "//button[@data-test-id='202008281733040391297']";
	String records_Schedule="$PpyWorkPage$pClaimData$pNoticeData$pRecordsSchedule";
	String paymentSettleDate="//input[@name='$PpyWorkPage$pSettlementDate']";

	String make_payment_attachfile="PaymentsAttachments_pyWorkPage_7";
	String make_payment_attachfromclaimfile="PaymentsAttachments_pyWorkPage_8";
//	String checkboxInPopup = "//tr[@id='$PD_AllClaimsAttachmentsEditable_pa203373925894907pz$ppxResults$l1']//input[@type='checkbox']";
	String checkboxInPopup = "//label[@data-ctl='Checkbox']//parent::div/input[@type='checkbox']";
//	String completed_back_bun="pyCaseContainer_pyWorkPage_19";
	String completed_back_btn="//div[@uniqueid='yui-gen1']//button";
	String recovery_completed_back_btn="pyCaseContainer_pyWorkPage_22";
	String recovery_completed_back_btns = "//div[@uniqueid='yui-gen1']//button";
	String settlement_back_btn="//button[text()='Back']";
	String close_btn = "//button[text()='Close']";
	String finance_close_btn="//button[@title='Close and go back']";
	String viewPaymentsBtn = "//*[text()='Financials']/parent::div//button[@name='EstimatesOverview_pyWorkPage_6']";
	String noPayments = "//*[@param_name='EXPANDEDSubSectionDisplayPaymentCasesListBBBB']/div[2]/div/div";
	String withPayments = "//*[@param_name='EXPANDEDSubSectionDisplayPaymentCasesListBB']/div[2]/div/div";

	
	String recovery_open_btn="(//h2[text()='Recovery']//following-sibling::span/nobr/span/button[text()='Open  '])[last()]";
	String recoverry_add_btn="CaptureRecoveryDetailsWrapper_pyWorkPage_6";
	String recoverry_estimatetype="$PTempPage$pRecoveryType";
	String recovery_estimated_amt="$PTempPage$pRecoveryEstimated";
	String recovery_estimated_comments="$PTempPage$pComments";
	String recovery_estimate_recived="$PTempPage$pRecoveryAmount";
	String recovery_estimate_submit_id="ModalButtonSubmit";
	
	String rec_finance_manualproreason="$PpyWorkPage$pProcessingReason";
	String rec_finance_confr_mail="//label[@for='6f9cdcb9No']";
	String rec_finance_amountRecived="$PpyWorkPage$pRecoveryData$pAmountReceived";
	String rec_finance_amountreciveddate="CalendarImg-39d0c27b";
	String rec_finance_submit="pyCaseActionAreaButtons_pyWorkPage_10";
	String rec_finance_open_btn="(//h2[text()='Financials']//following-sibling::span/nobr/span/button[text()='Open  '])[last()]";
	
	
	String recovery_history_expandicon="//div[@title='Disclose History']/i[@class='icon icon-openclose']";
	String recovery_table_recamount="//table[@pl_prop='D_FetchRecoveryHistory.pxResults']//tr//td[3]/div/span/span";
	String recovery_table_desc="//table[@pl_prop='D_FetchRecoveryHistory.pxResults']//tr//td[2]";
	String recovery_id="//table[@pl_prop='D_FetchRecoveryHistory.pxResults']//tr[2]/td[1]";
	String recovery_estimate_initiate="DisplayInitiateForRecovery_pyWorkPage_0bbf67b6-6f59-40b0-aeba-ca69cf783d88";
	String recovery_estimate_estimatetype="$PRecoveryTempPage$pEstimateType";
	String recovery_historyExpand="(//h4[@class='header-title'])[1]";
	

	
	String recovery_estimate_estimatdes="$PRecoveryTempPage$pDescription";
	String recovery_estimatepartytype_business="//label[@for='2e6821e2Business']";
	String recovery_estimatepartytype_Individual="2e6821e2Individual";
	String recovery_estimate_firstname="$PRecoveryTempPage$pFirstName";
	String recovery_estimate_lastname="$PRecoveryTempPage$pLastName";
	String recovery_estimate_emailaddress="$PRecoveryTempPage$pEmailAddress";
	String recovery_estimate_recoveryamount="$PRecoveryTempPage$pRecoveryAmount";
	String recovery_initate_recovery_btn="InitiateRecoveryWrapper_pyWorkPage_8";
	String recovery_id_alter="//input[@name='D_FetchRecoveryHistoryPpxResults1colWidthGBR']/parent::td/div/table//tr[2]/td[1]";
	String recovery_sendEmail_No="//input[@name='$PRecoveryTempPage$pSendEmail']/following-sibling::label[text()='No']";
	
	String add_legal_panel_button = "//button[@data-test-id='202008281733040391297']";
			//"//button[@data-test-id='2014121801251706289770']";
	String legal_Update="//div[5]//span//button[@data-test-id='202009281332330795157' and text()='Update'][1]";
	String legal_reason="//textarea[@data-test-id='202102112257080080500']";
	String select_plaintiff = "//label[text()='Plaintiff']";
	String select_defendant = "//label[text()='Defendant']";
	String vmia_solicitor = "//input[@data-test-id='201609210041510863210']";
	String txtPlaintiffsolicitor="//input[@data-test-id='202008111708420890222']";
	String txtNextlegalpanel="//div[contains(text(),'Next legal panel based on the rotation :')]";
	String txtErrormsg="//span[@id='PegaRULESErrorFlag']";
	String litigation_status = "//select[@data-test-id='20200811170842089184']";
	String jurisdiction = "//input[@data-test-id='202008111708420891424']";
	String court_number = "//input[@data-test-id='20200811170842089198']";
	String add_defendant = "//a[@data-test-id='20160809064449004925731']";
	String defendant_name = "//input[@data-test-id='2016072109335505834280']";
	// Pavan
	String legal_save_btn="//button[@data-test-id='202010071258130290422']";
	String date_issued="//img[@data-test-id='202008111708420891625-DatePicker']";
	String date_served="//img[@data-test-id='202008111708420892539-DatePicker']";
	//String today_link="//a[@id='todayLink']";
	String number_of_litigations = "//span[@data-test-id='202008152239170348423-Label']/following-sibling::div/span";
	
	String property_damages_checkbox = "//input[@type = 'checkbox' and @name='$PpyWorkPage$pClaimData$pNoticeData$pIsGeneralLossDamageSelected']";
	String property_covered_SECS = "//input[ @name='$PpyWorkPage$pClaimData$pNoticeData$pIsCoveredBySECS']/following-sibling::label[text()='Yes']";
	String add_button_SECS = "//a[@name='LossScheduleForSECS_pyWorkPage.ClaimData.NoticeData_44']/i";
	String add_button_Det_Yes = "//a[@name='LossScheduleForAppleNoteBook_pyWorkPage.ClaimData.NoticeData_46']/i";
	String item_desc_1 = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l1$pItemDescription']";
	String item_desc_2 = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l2$pItemDescription']";
	String orig_purchase_price_1 = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l1$pAmount']";
	String orig_purchase_price_2 = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l2$pAmount']";
	String coverage_type_1 = "//select[@name='$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l1$pCoverageType']";
	String coverage_type_2 = "//select[@name='$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l2$pCoverageType']";
	String NotebookmodelNumber="$PpyWorkPage$pClaimData$pNoticeData$pAppleNoteBookList$l1$pModelNumber";
	String NotebookserialNumber="$PpyWorkPage$pClaimData$pNoticeData$pAppleNoteBookList$l1$pSerialNumber";

	String org_name = "(//input[@name='$PpyDisplayHarness$pSearchStringBusinessUnitName'])[1]";
	
	String contact_tab = "//label[text()='Contact']";
	String client_firstname = "//input[@name='$PpyDisplayHarness$pSearchStringFirstName']";
	String contact_id_link = "//a[@data-test-id='202009011627490629432']";
	
	String estimate_exceed_message = "//span[@data-test-id='202008271533530419433']";
	String review_comments_estimate = "//textarea[@data-test-id='20200826170912005244']";
	String estimate_approve_button = "//button[@data-test-id='202008261708140136551']";
	
	String add_payee_button = "//button[@data-test-id='202008242214180357762']";
	String add_payee_details_option = "//label[text()='Add payee details']";
	String payee_type_dropdown = "//select[@data-test-id='202008281632060680307']";
	String payee_name_new = "//input[@data-test-id='202008302102450634554']";
	String account_name_new = "//input[@name='$PpyWorkPage$pPayeeDetails$pPersonInfo$pKeyFinancials$pAccountName']";
	String bsb_new = "//input[@name='$PpyWorkPage$pPayeeDetails$pPersonInfo$pKeyFinancials$pBSBValue']";
	String account_number_new = "//input[@name='$PpyWorkPage$pPayeeDetails$pPersonInfo$pKeyFinancials$pBankAccountNumber']";
	String email_new = "//input[@name='$PpyWorkPage$pPayeeDetails$pPersonInfo$pKeyFinancials$pEmailAddress']";
	String submit_new = "//button[@data-test-id='2020082821083300693']";
	
	String payee_pending_approval = "//div[@class='content-item content-sub_section item-4 remove-top-spacing remove-bottom-spacing remove-left-spacing']/div/div/div/div/div";
	
	String first_withdraw_button = "(//a[@data-test-id='202009081139260928178'])[1]";
	String withdraw_reason = "//select[@data-test-id='2015072204461602903949']";//Premium
	//String submit_withdraw = "//button[@title='Click here to submit work']";
	String submit_withdraw = "//button[text()='  Submit ']";
	
	String organization_details_button = "//button[@data-test-id='202008041311520919467']";
	String org_claims_tab = "(//div[@data-test-id='202005042120290020161_header'])[2]";
	String claim_cancel_button = "(//button[@data-test-id='202009081344350824867'])[3]";
	String submit_cancel = "//button[@title='Click here to submit work']";
	String dess = "//textarea[@data-test-id='202009091725270328314']";
	String org_claim_status = "(//span[@data-test-id='202005082231450414757'])[1]";
	
	String insured_employee_no = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pIsInsuredCompanyEmployee']/following-sibling::label[text()='No']";
	
	String authorized_business_travel = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pTravelDetails$pAuthorisedBTV']/following-sibling::label[text()='Yes']";
	String date_of_departure = "//img[@data-test-id='202009132043090825810-DatePicker']";
	String date_of_return = "//img[@data-test-id='20200913204309082640-DatePicker']";
	String departure_city = "//input[@data-test-id='202009132044070334637']";
	String incident_city = "//input[@data-test-id='202009132054550353833']";
	String incident_country = "//input[@data-test-id='202009132054550354882']";
	String exact_location = "//input[@data-test-id='202009132103550181648']";
	String medical_checkbox = "//input[@type='checkbox' and @name='$PpyWorkPage$pClaimData$pNoticeData$pIsMedicalReasonSelected']";
	//a[@data-test-id='202008131131110964292']
	String medical_illnessdetails = "//textarea[@data-test-id='202009141726010447234']";
	String illness_complaint = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pPersonalInjury$pHasClaimantSufferedComplaint']/following-sibling::label[text()='No']";
	String emergency_contacted = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pPersonalInjury$pIsEmergencyACContacted']/following-sibling::label[text()='No']";
	String other_health = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pPersonalInjury$pIsClaimResultedByOtherHealth']/following-sibling::label[text()='No']";


 	String Gpa_Claim_FlightRisk_StudentName="$PpyWorkPage$pClaimData$pNoticeData$pStudentDetails$ppyName";
	String Gpa_Claim_FlightRisk_DateOfBirth="$PpyWorkPage$pClaimData$pNoticeData$pStudentDetails$pDateOfBirth";
	String Gpa_Claim_FlightRisk_Student_Address="$PpyWorkPage$pClaimData$pNoticeData$pStudentDetails$pAddressDetails$pAddressSelected";
	String Gpa_Claim_FlightRisk_Parent_Gaurdian_Name="$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$ppyName";
	String Gpa_Claim_FlightRisk_RelationshipToStudent="$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pRelationshipToStudent";
	String Gpa_Claim_FlightRisk_Parent_Adress="$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pAddressDetails$pAddressSelected";
	String Gpa_Claim_FlightRisk_emailAdress="$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pEmailAddress";
	String Gpa_Claim_FlightRisk_PhoneNumber="$PpyWorkPage$pClaimData$pNoticeData$pParentDetails$pMobileNumber";
	String Gpa_Claim_FlightRisk_IsReportedSchool="$PpyWorkPage$pClaimData$pNoticeData$pAccidentReportedto";
	String Gpa_Claim_FlightRisk_InjuryDetails="$PpyWorkPage$pClaimData$pNoticeData$pInjuryDetails";
	String Gpa_Claim_FlightRisk_AccidentDetails="$PpyWorkPage$pClaimData$pNoticeData$pAccidentDetails";
	String Gpa_Claim_FlightRisk_IsWitness_No="//label[@for='97a3344dfalse']";
	String Gpa_Claim_FlightRisk_IsWitness_Yes="//label[@for='97a3344dtrue']";
	String Gpa_Claim_FlightRisk_MedicalDetails_No="//label[@for='c34af23afalse']";
	String Gpa_Claim_FlightRisk_MedicalDetails_Yes="//label[@for='c34af23atrue']";
	String Gpa_Claim_FlightRisk_MedicalDetails_IsPrivateInsurace_No="//label[@for='95c1e7e3false']";
	String Gpa_Claim_FlightRisk_MedicalDetails_IsPrivateInsurace_Yes="//label[@for='95c1e7e3true']";
	String Gpa_Claim_FlightRisk_MedicalDetails_IsAmbulanceCover_No="//label[@for='f66d0bc2false']";
	String Gpa_Claim_FlightRisk_MedicalDetails_IsAmbulanceCover_Yes="//label[@for='f66d0bc2true']";
	String Gpa_Claim_FlightRisk_Add="OtherExpenses_pyWorkPage.ClaimData.NoticeData_23";
	String Gpa_Claim_FlightRisk_item="//input[contains(@name,'$PpyWorkPage$pClaimData$pNoticeData$pExpenses') and contains(@name,'pItem')]";
	String Gpa_Claim_FlightRisk_Amount="//input[contains(@name,'$PpyWorkPage$pClaimData$pNoticeData$pExpenses') and contains(@name,'pAmount')]";

 	String GPA_Volunteers_IncidentDate="$PpyWorkPage$pClaimData$pNoticeData$pIncidentReportDate";
	String GPA_Volunteers_IncidentDes="$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer2";
	String GPA_Volunteers_IncidentClaimantName="$PpyWorkPage$pClaimData$pNoticeData$pClaimantDetails$pClaimantName";

	
	String DO_generaldetails_first_name = "//input[@data-test-id='202009071859020377873']";
	String DO_generaldetails_last_name = "//input[@data-test-id='202009071859020378960']";
	String DO_employee_position = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pEmployeePosition']";
	String DO_employee_perform = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pEmployeeDuty']";
	String DO_employee_base_salary = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pEmployeeSalary']";
	String DO_employee_remuneration = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pEmployeeRemuneration']";
	String DO_written_contract = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pHasContract']/following-sibling::label[text()='No']";
	String DO_employee_covered = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pIsCovered']/following-sibling::label[text()='No']";
	String DO_personnel_file = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pHavePersonnelFile']/following-sibling::label[text()='No']";
	
	String DO_termination_occured = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pHasTerminationOccurred']/following-sibling::label[text()='No']";
	String DO_employee_resigned = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pIsEmployeeResigned']/following-sibling::label[text()='No']";
	String DO_commence_employment = "//img[@data-test-id='202009081533070299421-DatePicker']";
	String DO_employee_age = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pTerminationAge']";
	String DO_employee_disputes = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pHasOtherAction']/following-sibling::label[text()='No']";
	String DO_employee_proceedings = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pHasCommencedProceedings']/following-sibling::label[text()='No']";
	String DO_employee_complaint = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pNatureOfComplain']";
	String DO_qualifying_period = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pEmploymentSubjectedTo']/following-sibling::label[text()='No']";
	String DO_employee_terminated_reason = "//textarea[@name='$PpyWorkPage$pClaimData$pNoticeData$pTerminationReason']";
	String DO_employee_challenge_reason = "//textarea[@name='$PpyWorkPage$pClaimData$pNoticeData$pChallengeReason']";
	String DO_employee_payment = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pTerminationAmount']";
	String DO_org_less_than_15 = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pOrgEmployeeCount']/following-sibling::label[text()='Yes']";
	String DO_engaged_legal_representation = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pLegalRepresentationStatus']/following-sibling::label[text()='No']";
	
	String edit_recoverable = "//button[@data-test-id='202008072112590421946' and @name='RecoveryOverview_pyWorkPage_4']";
	String add_recoverable = "//button[@data-test-id='202008112114380656784']";
	String select_recovery_type = "//select[@data-test-id='202008112224170252954']";//Third party recovery //Reinsurance
	String recovery_amount = "//input[@data-test-id='202009142038570333653']";
	String txtrecovery_comments="//input[@data-test-id='20210303171959046835']";
	String party_type_individual = "//span/label[text()='Individual']";
	String first_name_recov = "//input[@data-test-id='202009141852520883977']";
	String last_name_recov = "//input[@data-test-id='202009141852520883814']";
	String submit_recovery = "//button[@title='Click here to submit work']";
	String reinsurance_apply = "//input[@name='$PTempPage$pIsInsuranceApply']/following-sibling::label[text()='Yes']";
	String Reinsurer_tfield = "//input[@name='$PTempPage$pReinsurerName']";
	String comments_tfield = "//textarea[@name='$PTempPage$pComments']";

	String create_event_cat = "//span[text()='Create incident category']";
	String event_name = "//input[@name='$PpyWorkPage$pEventName']";
	String submit_event_cat = "//button[@data-test-id='2014121801251706289770']";

	String adhocInvoice = "//span[text()='Create adhoc invoice']";
	String orgNameField = "//input[@name='$PpyWorkPage$pClientName']";
	
	String event_name_text = "//input[@data-test-id='202009091741000365542']";
	String event_search = "//button[@data-test-id='202009091742400386127']";
	String event_select = "//input[@name='$PEventList$ppxResults$l1$ppySelected' and @type='radio']";
	String link_event = "//button[@data-test-id='201608050712170425129243']";
	String verify_Event_link = "//span[@data-test-id='202009110347200428484']";
	
	String add_expenditure = "//a[@data-test-id='20160809064449004925731']";
	String exp_desc = "//input[@data-test-id='2016072109335505834280']";
	String exp_type = "//input[@data-test-id='202009142038130157103']";
	String amt_claimed = "//input[@data-test-id='202009142038130157861']";
	String curr = "//input[@data-test-id='202009142038130158443']";
	
	String more_actions = "(//button[@data-test-id='202007221908250903861'])[1]";
	String cancel_policy = "//span[text()='Cancel policy']";
	String cancel_date = "//img[@data-test-id='202004092226510989570-DatePicker']";
	String cancel_reason = "//select[@data-test-id='2015072204461602903949']";
	String cancel_declaration = "//input[@name='$PpyWorkPage$pApplicationPage$pTermsAcceptance' and @type='checkbox']";
	String queue_dropdown_1 ="//body/div[@id='PEGA_HARNESS']/form[1]/div[3]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]/div[1]/div[1]/div[1]/div[1]/div[5]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/span[1]/i[1]";
	//String triage_claims_1 = "//span[text()=\"Triage Claims\"]";
	String queueClaimsDropDown="//i[@title='Select work owner']";
	
	String triage_claims_1 = "//span[text()=\"Triage Claims\"]";
	//String claimfile_id_1= "//div[@class=\"grid-responsive-default  gPercent \"][1]//table//th[@data-order=\"1\"]/div[text()=\"Case number\"][1]";
	//String claimfileid_filter_1 = "//div[@class=\"grid-responsive-default  gPercent \"][1]//table//th[@data-order=\"1\"]//a[@title=\"Open Menu\"][1]";
	String claimfile_id_1= "//table[@class=\"gridTable  row-compact\"]//div[text()=\"Case number\"]";
	String ClaimnumerTH="(//div[@class='gridHeaderLabel ' and text()='Claim number'])[1]";
	String claimNameTH = "(//div[@class='gridHeaderLabel ' and text()='Client name'])[2]";
	String ClaimnumerTH1="(//div[@class='gridHeaderLabel ' and text()='Claim number'])[2]";
	//(//div[@class='gridHeaderLabel ' and text()='Case number']/parent::th/following-sibling::th[1]/div[text()='Claim number'])[1]
	//div[@pl_prop='D_ClaimsWorkBasket_pa2333259639082573pz.pxResults']//th[@class='standard_dataLabelRead  cellCont gridCell pointerStyle filterable categorisable']
	String claimfileid_filter_2= "(//div[text()='Claim number']/parent::th//a[@title='Open Menu'])[1]";
	String claimfileid_filter_1 = "//table [@class='gridTable row-compact']//div/a[@title='Open Menu'][1]";
	String claimnumerFilterIcon="(//table [@class='gridTable row-compact']//div/a[@title='Open Menu'])[2]";
	String claimNumberFilterIcon = "//div[contains(@pl_prop,'D_ClaimsWorkBasket')]//th[@aria-label='Claim number']//a[@class='columnMenu' and @title='Open Menu']";
//	String claimnameFilterIcon = "(//table [@class='gridTable row-compact']//div/a[@title='Open Menu'])[6]";
	String claimnameFilterIcon = "//div[contains(@pl_prop,'D_ClaimsWorkBasket')]//th[@aria-label='Client name']//a[@class='columnMenu' and @title='Open Menu']";
	String claims_search_text_1 = "//input[@class=\"leftJustifyStyle\"]";
	String claims_case_link_1= "//td[@data-attribute-name='Case number']//a[@title='Click here to open the object']";
	String triage_closing_submit_1= "//button[text()=\"Submit\"]";
	String status_pending_claimunit_comp_1= "(//span[@class='badge_text'])[last()]";
	String pending_triage_status_1="//span[@class=\"vmia_case_header_id\"]/parent::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div/span[text()=\"Pending-Triage\"]";
	String pending_triage_status_2 = "//span[@class=\"vmia_case_header_id\"]/parent::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div/span[text()=\"Pending-DETConfirmation\"]";
	String duplicateWith_dropdown_1= "//select[@aria-describedby=\"$PpyWorkPage$ppyDuplicateIDError\"]";
	String status_resolved_duplicate_1= "//span[@class=\"vmia_case_header_id\"]/parent::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div/span[text()=\"Resolved-Duplicate\"]";
	String queue_dropdown_2= "//i[@title=\"Select work owner\"]";
	String Unassigned_school= "//span[text()=\"Claims Unassigned School\"]";
	String lnkUnassigned="//span[contains(text(),'Unassigned Claims')]";
	String claimfile_id_port= "//div[@class=\"grid-responsive-default  gPercent \"]//div[text()=\"Claim file\"]";
	String pending_triage_status_1_port= "//div[@data-test-id=\"201806191351520571476\"]//following-sibling::div//following-sibling::div/span[text()=\"Pending-Triage\"]";
	String claimfileid_filter_port = "//div[@class=\"grid-responsive-default  gPercent \"]//div[text()=\"Claim file\"]//following-sibling::div//following-sibling::div/a[@title=\"Open Menu\"]";
	String Click_On_Transfer="//span[contains(text(),'Claims']";
			//"tr[2]//td[2]//span[text()='ClaimsOfficer1R3']";

	String Click_On_Transfer1="//span[text()='Claims Officer 2 R3']";
	String portfolio_manager_pending_investigation_1= "//span/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span[text()=\"Pending-Investigation\"]";
	String claimfile_id_port_1= "//table[@class=\"gridTable  row-compact\"]//div[text()=\"Claim file\"]";
	String claimfileid_filter_port_1="//th[@data-test-id=\"201708061024470363306-th-1\"]//a[@title=\"Open Menu\"]";
	String Portfolio_profile= "//a[@data-test-id=\"202004170924160508258\"]";
	String Portfolio_profile_text="//span[text()=\"Profile\"]";
	String Report_Gear="//h2[text()!=\"About\"]/parent::div//button[@data-test-id=\"20151105063323042170920\"]";
	String Update_report_text= "//span[text()=\"        Update direct reportees       \"]";
	String Reportee_icon="//input[@data-target=\"$POperatorInfoPage$pDirectReportList\"]/parent::div/i";
	String operator_text= "//input[@data-target=\"$POperatorInfoPage$pDirectReportList\"]";
	String reportee_value1="//div[@id=\"msresults-list\"]";
	String reportee_cross= "//span[@class=\"token\"][last()]//div[@class=\"token-cancel token-cancel-img\"]";
	String submit_button= "//button[text()=\"  Submit \"]";
	String Mi_claim_submit="pyCaseActionAreaButtons_pyWorkPage_10";
	String New_triage= "//span[text()=\"New\"]";
	String Traige_Make_a_claim="//span[text()=\"Make a claim\"]";
	String where_did_loss_occur_Triage="//textarea[@name=\"$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer1\" and @validationtype= \"required\"]";
	String what_is_the_loss_Triage= "//textarea[@name=\"$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer2\" and @id =\"f4dc6506\"]";
	String how_loss_occur_triage="//textarea[ @id ='35b1cbe9']";
	String select_general_damages_Triage= "//input[@name=\"$PpyWorkPage$pClaimData$pNoticeData$pIsGeneralLossDamageSelected\"and @value=\"true\"]";
	String scip_add_button1="//a[@data-ctl=\"Link\" and text()=\"Add\"]";
	String scip_purchase_desc1= "//input[@name=\"$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l1$pItemDescription\"]";
	String scip_purchase_cost1= "//input[@name=\"$PpyWorkPage$pClaimData$pNoticeData$pAssetList$l1$pAmount\"]";
	String attach_button_claims1="//h2[text()=\"Proof of claim\"]/parent::div/parent::div/parent::div/parent::div//span/button";
	String claimClasification_Cause1="$PpyWorkPage$pClaimData$pCauseL1";
	String claimClasification_Cause2="$PpyWorkPage$pClaimData$pCauseL2";

	String claimClasification_outcome1="$PpyWorkPage$pClaimData$pOutcomeL1";
	String claimClasification_outcome2="$PpyWorkPage$pClaimData$pOutcomeL2";
	String Indeminity_Des_Granted="//label[@for='c1f23020Granted']"; 
	String Indeminity_Des_Denied="//label[@for='c1f23020Denied']";
	String Indeminity_Des_Pending="//label[@for='c1f23020Pending']";
	
	String Indemity_granted_full="//label[@for='bf016e67Full']";
	String Indemity_granted_partial="//label[@for='bf016e67Partial']";
    String Indemity_comments="//textarea[@name='$PpyWorkPage$pComments']";
	
	//pavan for US-8009
	String Hamburger_menu_lnk = "//a[@id='appview-nav-toggle-one']";
	String Mange_componet_Page = "//span[text()='Manage components']";
	String Add_Payee_Btn = "//button[text()='Add payee']";
	String AddPayee_Details_Page = "//span[text()='Add payee details']";
	String Payee_Type_Drodwn = "//select[@id='a9e2ba52' and @class='standard']";
	String Payee_name_TxtBox = "//span/input[@name='$PpyWorkPage$pPayeeName' and @id='7b1f937d']";
	String payee_account_name = "//input[contains(@name,'$pKeyFinancials$pAccountName')]";
	String payee_bsb = "//input[contains(@name,'$pKeyFinancials$pBSBValue')]";
	String payee_account_number = "//input[contains(@name,'Financials$pBankAccountNumber')]";
	String payee_bank_name = "//input[contains(@name,'$pKeyFinancials$pBankName')]";
	String payee_email = "//input[contains(@name,'$pKeyFinancials$pEmailAddress')]";
	
	String lnkPendingStatus="//div[3]/span[contains(text(),'Pending-')]";
	String caseStatusText="(//span[@class='badge_text'])[last()]";
	String riskAddim_Status="//div[3]/span[text()='Approved']";
	String riskAddim_RejectStatus="//div[3]/span[text()='Pending-Clarification']";
	String riskAdim=" //span[@data-test-id='20160923044938017658606' and text()='RiskAdmin']";
	String claims_case_id="//span[@class='vmia_case_header_id']";
	String payee_comments="//textarea[@id='5e1978c7']";
	String risk_admin_approve="//button[@name='pyCaseActionAreaButtons_pyWorkPage_30']";
	String risk_admin_Reject="//button[@name='pyCaseActionAreaButtons_pyWorkPage_29']";
	String payee_Name_filter="//tr[1]/th[1]/div[1]//a[1][@id='pui_filter']";
	String payee_name_search="input[@id='fb056771']";
	String payee_Action_lnk="//a[@data-menuid='pyNavigation1614249440178']";
	String Payee_Modify_lnk="//span[text()='Modify']";
	String tabLegalpanel="//div[contains(@id,'headerlabel')]/h3[text()='Legal panel']";
	String btnAddleagal="//button[contains(text(),'Add legal panel')]";
	String txtlegalName="//input[contains(@name,'$pAttorneyName')]";
	String lblOnpanel="//label[contains(text(),'On-panel')]";
	String txtClaimsType="//input[@id='60484dfd']";
	String txtAddress1="$PpyWorkPage$pClaimData$pNoticeData$pOtherPractitionerAddress";
	String txtAddress2="//input[contains(@name,'$pAddressLine2')]";
	String txtSuburb="$PpyWorkPage$pClaimData$pNoticeData$pClaimantAddressDetails$pcity";
	String drpdwnState="//select[contains(@name,'$pAddressInfo$pState')]";
	String txtPostalCode="//input[contains(@name,'$pAddressInfo$pZipCode')]";
	String btnAdd="//a[contains(@name, 'CaptureLegalSolicitorList_LegalPanelPg')]";
	String txtSolicitorList="//input[contains(@name, '$pLegalPanelSolicitorList$l1$pName')]";
	String txtEmail="//input[contains(@name, 'SolicitorList$l1$pEmail')]";
	String txtPhone="//input[contains(@name, 'SolicitorList$l1$pMobileNumber')]";
	String btnSubmit="//button[contains(@id ,'ModalButtonSubmit')]";
	 
	String imgIcon="//tbody/tr[1]/th[2]/div[1]/div[1]/a";
	String txtSearch="//input[contains(@name,'ppySearchText')]";
	String btnApply="//button[text()='Apply']";
	String lnkAction="//tbody/tr[2]/td[6]/span[1]/a[text()='Actions']";
	String lnkEditlegalpanel="//span[contains(text(),'Edit legal panel')]";
	String lnkRmoveLegalpanel="//span[contains(text(),'Remove legal panel')]";
	String lnkHistorylegalpanel="//span[contains(text(),'Remove legal panel')]";
	String lnkWithdraw="//span[contains(text(),'Withdraw notice of loss')]";
	String gettxtClaimsType="//td[@data-attribute-name='Claim types']";
	String btnSubmitLegal="//button[@id='ModalButtonSubmit']";
	String lnkCanclefromAction="//span[text()='Cancel claim']";
    String drpdwnRecardShecudle="//select[@id='9972a016']"	;
    String txtCancelReason="//textarea[@id='c2e4464f']";
    String getTextWithdrawn="//span[contains(text(),'Do you want to withdraw the notice of loss FNOL')]";
    String btnWithDraw="//button[@name='pyCaseHeader_pyWorkPage_11']";
    String btnContinue="//button[text()='Continue']";
    String drpdwnPayementMode="//select[@id='77564771']";
    String chkFinalpayment="//input[@id='5653ff96']";
    String drpRecordsschedule="//select[@id='9972a016']";
    
    String drpdwnTransactiontype="//select[@id='3aa3a776']";
    String drpdwnPayementCode="//select[@id='97893042']";
    String txtPaymentDes="";
    String lnkManagePayment="//span[contains(text(),'Manage payments')]";
    String drpdwnAdjustAction="//button[@data-test-id='20160816023315008443588']";
    String lstAdjust="//span[contains(text(),'Adjust')]";
    String drpdwnAdjustmentType="//select[@id='9d8274a7']";
    String txtAdjustmentdescription="//textarea[@id='21b3b421']";
    String txtAdjustmentAmount="//input[@id='ac38090b']";
    String chksubOrg="//input[@id='f2b96c9d']";
    String txtSubsidiaryorganisation="//input[@id='e33b8dd3']";
    String optnSubOrg="//span[text()='Nelson School Council']";
    		//"//tbody/tr[@id='$PD_GetParentSubOrgDetails_pa1108013519547819pz$ppxResults$l1']/td[1]/div[1]/div[1]/div[1]/span[1]/span[1]";

    String lnkmyclaims="//h3[text()='My claims']";
    String chkshowsubOrg="//label[@for='db261545']";
    		//"//input[@data-test-id='202102152100440400191']";
    
    String txtClaimOrg="//span[@data-test-id='202005082255350343898']";
    String lnkInitiate="//a[contains(text(),'Initiate')]";
    String txtrecoveryDesc="//textarea[@id='6eddf119']";
    String radpartyType="//label[contains(text(),'Individual')]";
    String txtrecoveryEmail="//input[@id='f03f76e2']";
    String txtrecoveryAmt="//input[@id='9a83e8cd']";
    String btnintiateRecovery="//button[contains(text(),'Initiate recovery')]";
	String lnkRecoveries="//span[contains(text(),'Recoveries')]";
	String txtReasonformul="//textarea[@id='bdc134e1']";
	String radEmailforclient="//label[contains(text(),'No')]";
	String txtAmtRecived="//input[@id='8187d4d0']";
	String chkMarkrecoveyfnl="//input[@id='8154c06e']";
	String btnAttachment="//button[contains(text(),'Attach File')]";
	String btnSelectFile="//button[@id='Browsefiles']";
	
	//Claim for Journy
	String txtfristName="//input[@id='6f5d0a7f']";
	String txtlastName="//input[@id='37ad6da9']";
	String lnkcanntfindlink="//a[contains(text(),'find your address?')]";
	String txtAddressline1="//label/span[text()='Suburb']//preceding::input[2]";
	String txtSuburbJoury="//label/span[text()='Suburb']//following::input[2]";
	String txtState="//label/span[text()='Suburb']//following::select[1]";
	String txtPostcode="//label/span[text()='Suburb']//following::input[2]";
	String txtCountry="//label/span[text()='Suburb']//following::select[2]";
	String txtEmailaddress="//label/span[text()='Suburb']//following::input[3]";
	String txtcontactnumber="//label/span[text()='Suburb']//following::input[4]";
	String txtOccupation="//label/span[text()='Suburb']//following::input[5]";
	String txtDateofbirth="//label/span[text()='Suburb']//following::input[6]";
	String txtinjury="//label[text()='What is the injury?']//following::input[1]";
	String txtinjuryoccur="//label[text()='What is the injury?']//following::input[2]";
	String txtDateinjuryoccured="//label[text()='What is the injury?']//following::input[3]";
	String txttestWhenn="//label[text()='What is the injury?']//following::input[4]";
	String txtMedicalPractitioner="//input[@id='b39aa54c']";
	String didInjuryStopsYouWorking="$PpyWorkPage$pClaimData$pNoticeData$pStoppedWorking";
	String haveYouReturnToWorking="$PpyWorkPage$pClaimData$pNoticeData$pReturnToWork";
	String txtPeriodfrom="//label[text()='Period from']//following::input[1]"; 
	String txtPeriodto="//label[text()='Period from']//following::input[2]"; 
	String txtDoctorsname="//label[text()='Period from']//following::input[3]"; 
	String txtAddress="//label[text()='Period from']//following::input[4]"; 
	String txtcondition="$PpyWorkPage$pClaimData$pNoticeData$pOtherPractitioner";
	String txtDoctorsname1="//label[text()='Period from']//following::input[5]"; 
	String txtAddress3="//label[text()='Period from']//following::input[6]";

	String post_note="//button[text()='Post']";
	String click_onattachment="//i[@class='pi pi-paper-clip']";
	String Notification_bell="//a[contains(@name,'pyDesktopNotificationGadget_pyDisplayHarness_3')]/i[@class='pi pi-bell pi-white']";
	String detailsofnote_xpath="//b[text()='Triage Claims Officer 1 R3']";
	String case_sort="//div[text()='Case']";
	String case_worklist = "//a[contains(text(),'CF-')]";	
	String view_note="//label[@for='022f3283']";
	String enter_note="//textarea[@title='Enter a note �']";
	String post_button="//button[@title='Post message']";
	String attachment_btn="//button[@title='Add attachment']";
	String btnProductAttach="//div[text()='Proof of bank account:']//following::button[1]";
	String show_more="//*[@title='Show more']";
    String post_content="//div[contains(@class,'content-item content-field item-1 remove-top-spacing remove-left-spacing flex flex-row pos')]";
	//String case_sort="//div[text()='Case']";
	//String case_worklist = "//a[contains(text(),'CF-')]";
	String field_item="//b[contains(text(),'Triage Claims Officer')]";
	//String Notification_bell="//i[@class='pi pi-bell pi-white']";
	//String click_onattachment="//i[@class='pi pi-paper-clip']";
	//String post_note="//button[text()='Post']";
	String notification_verify="(//div[@data-test-id='20180817083824036754'])[2]";
	//String detailsofnote_xpath="//b[text()='Triage Claims Officer 1 R3']";
	String mouseovertoclaim="//div[text()=' Triage Claims Officer 1 R3 ']";
	String clickondots="//i[@data-menuid='pyNavigation1611134324686']";
	String clickondelete="//span[text()='Delete']";
	//US-7001
	
	String Describe_incident ="//textarea[@id='e11da520']";
	String Capture_loss_section="//h2[text()='Capture loss details']";
	
	//US-4056
	String ClaimsTab="//label[text()='Claims']";
	String ClaimsNumber="//input[@id='e8084e1d'][@type='text']";
	String claimsSearh="//button[@title='Search for claims']";
	String claimsFile="//span[contains(text(),'Claim file')]";
	String actionButton="//button[@name='pyCaseHeaderOuter_pyWorkPage_24']";
	String reOpenClaims="//span[text()='Reopen claim']";
	String reOpenSelec_dropDown="//select[@name='$PpyWorkPage$pReopenReason']";
	String reOpen_Other="//*[@id='089090fd']/option[2]";
	String Otherreason_edtbox="//textarea[@id='cc831d1b']";
	String reOpen_Submit_btn="//button[text()='Submit']";
	String close_Btn="//button[text()='Submit']";
	String uw_Section="//span[text()='Underwriting notes']";
	String uw_text="//tr[@pl_index='1']";
	String TR_Policy ="//button[@name='PolicyOverview_pyWorkPage_5']";
	String approveRequest ="//label[text()='Approve request']";
	//US-4023
	String Bulk_Process_Lnk="//a[@data-test-id=202010051821510764748][text()='Bulk process']";
	String Select_Action_Btn="//button[@title='Select Action']";
	String Select_all_check_Box_chck="//input[@title='Select all displayed results']";
	String Select_chk_Box_1="//input[@id='eb757c13']";
	String Select_chk_Box_2="//input[@id='56bf10dd'][1]";
	
	String Filter_Work_claims_Unit="//span/input[@data-test-id='201509010535080713413']";
	
	String Filter_cases="//button[@data-test-id='20150507150702094120904']";
	String Option_list="//span[text()='Transfer claim']";
	String Check_box_list="//input[@type='checkbox']";
	String Close_OpenPage="//button[@data-test-id='2014100711485602125921']";
	
  //US-2132
	String Edit_Profile_lnk="//span[text()='Edit profile']";
	String Availability_Profile_lnk="//span[text()='Availability']";
	String Actions_Btn="//button[@title='Actions']";
	String Delete_segment_lnk="//a[@name='pySkills_pyCurrentOperator.pySkills(7)_25']";
	String Add_Skill_Lnk="//a[text()='Add skill']";
	String Enter_New_Skill_Txt="//input[@id='9bb957ca']";
	String Proficiency_of_Skill_Txt="//input[ @id='d7c79c1']";
	String EditProfile_Submit_Btn="//button[text()='  Submit ']";
	String VerifyAddSkill="//span[contains(text(),'Commercial Property')]";
	
	String unaviability_strdate="//input[@id='36cff29f']";
	String unaviability_enddate="//input[@id='ca0f2cbe']";
	
	

	
	
	String link_claiment_name = "//label[text()='Claimant name']";
	String link_claim_number = "//label[text()='Claim number']";
	String txt_claimant = "$PTempSave$pSearchValue1";
	String btn_search = "LinkClaims_pyWorkPage_21";
	String Linked_claim_button = "//table[@pl_prop='TempLinkedList.pxResults']//tr[2]/td[7]";
	String Link_claim_number = "//table[@pl_prop='TempLinkedList.pxResults']//tr[2]/td[2]//span";
	String Link_List_claims = "//table[@pl_prop='TempLinkedList.pxResults']//tr";
	String UnLinked_claim_button = "//table[@pl_prop='D_FetchLinkedClaimList.pxResults']//tr//td[8]";
	String Unlink_List_claims = "//table[@pl_prop='D_FetchLinkedClaimList.pxResults']//tr";
	
	String ClaimsAdminGeneralButton = "//h2[@id='headerlabel8087']";
	String claim_description = "//h2[text()='Claims description']";
	String ClaimsFileStatusHistory = "(//h2[text()='Claim file status and history'])[1]";
	
	String GeneralIndeminityText = "(//h2[text()='Indemnity'])[1]";
	String GeneralSignificanceText = "(//h2[text()='Claim significance'])[1]";
	String GeneralFinancialText = "(//h2[text()='Financials'])[1]"; 	
	String ClaimDisIncidentText = "(//h2[text()='Incident'])[1]";
	String ClaimFileTraigeText = "(//h2[text()='Triage'])[1]";
	String ClaimFileClosureText = "(//h2[text()='Claim closure'])[1]";
	
	
	
	String button_back_to_claim = "//button[text()='Back to claim']";
		
	String Linked_claims_incident = "//table[@pl_prop_class='VMIA-Interface-Claims']//tr";

	//US-4044
	String Denail_action_Approve="//label[text()='Approve']";
	String Denail_action_RMI="//label[text()='Request more information']";
	String Denail_action_DD="//label[text()='Decline denial']";
	String Denail_action_comments = "//textarea[@name='$PpyWorkPage$pComments']";

	String logout_icon = "//a[@data-test-id='202004170924160508258']";
	String logout_iconn = "//a[@title='Logged in user name']";
	String bulkTransferLink = "//span[text()='Bulk transfer']";
	String selectAllCheckBox = "//input[@title='Select all displayed results']";
	String selectActionBtn = "//button[@title='Select Action']";
	String selectTransfer = "//span[text()='Transfer claim']";
	
}

