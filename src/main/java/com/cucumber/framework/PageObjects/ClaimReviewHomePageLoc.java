package com.cucumber.framework.PageObjects;

public interface ClaimReviewHomePageLoc {


    String addAttachment="//button[text()='Add new']";
    String attachmentAuthor="//input[contains(@name,'$PdragDropFileUpload$ppxResults') and  contains(@name,'$pAuthor')]";
    String attachmentCategory="//select[@name='$PdragDropFileUpload$ppxResults$l1$ppyCategory']";
    String attachmentDocDate="//img[@name='CalendarImg-cefd67ee']";
    String attachmentFileName="//input[contains(@name,'$PdragDropFileUpload$ppxResults') and  contains(@name,'ppyNote')]";
    String attachmentAttahcButton="//button[@title='Attach']";
    String documentActionlink="/ancestor::td[1]/following-sibling::td[4]//a[text()='Action']";
    String documentHistory="//table[@pl_prop_class='History-Data-WorkAttach-File']//tr/td[3]//span";
    String attachmentUpdatecButton="//button[@title='Update']";
    String attahcmentHistoryClose="//button[@title='Close']";
    String editAttachment_Category="$PAttachLinkFAPg$ppyCategory";
    String editAttachment_Author="$PAttachLinkFAPg$ppxPages$gAttachFile$pAuthor";
    String editAttachment_Docudate="//img[@aria-label='Open date picker']";
    String editAttachment_name="$PAttachLinkFAPg$ppyMemo";
    String searchButton="//button[text()='Search']";
    String documentNameSearch="$PDocumentSearch$pName";
    String documentActionSearchResult="/ancestor::td[1]//following-sibling::td//a[text()='Actions']";

}
