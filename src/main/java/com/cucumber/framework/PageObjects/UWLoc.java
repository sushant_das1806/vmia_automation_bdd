package com.cucumber.framework.PageObjects;

import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;

public interface UWLoc {
	String uw_group_personal_accident_button="(//h2[text()='Group Personal Accident']/parent::div/parent::div/following-sibling::div//button[@data-test-id='202004191943150280412'])[2]";
	String uw_property_button="(//h2[text()='Property']/parent::div/parent::div/following-sibling::div//button[@data-test-id='202004191943150280412'])[2]";
	String uw_liability_button="(//h2[text()='Liability for hall hirers and other users of school facilities']/parent::div/parent::div/following-sibling::div//button[@data-test-id='202004191943150280412'])[2]";
	String uw_msc_button="(//h2[text()='Motor ï¿½ Special Contingency']/parent::div/parent::div/following-sibling::div//button[@data-test-id='202004191943150280412'])[2]";
	String uw_mvf_button="(//h2[text()='Motor Vehicle']/parent::div/parent::div/following-sibling::div//button[@data-test-id='202004191943150280412'])[2]";
	String submit_button_uw_loss_select_screen="//button[@data-test-id='2014121801251706289770']";
//	String uw_case_id = "//span[@data-test-id='20141009112850013217103']";
	String uw_case_id = "(//span[@class='vmia_case_header_id'])";
//		"";
	String uw_case_id1 = "//span[@class='vmia_case_header_id']";
	String uw_case_status="//div[3]/span[@data-test-id='2016083016191602341167946']";
			//"//h1[text()='Client details']/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
//	String uw_case_status_accept_quote="//div[3]/span[text()='Pending-ClientAcceptance' and @data-test-id='2016083016191602341167946']";
	String uw_case_status_accept_quote = "//div[3]/span[@class='badge_text'][last()]";
	String capture_id_cancel = "//span[@data-test-id='20141009112850013217103']";
	String txt_annual_permium = "//input[@id='23f39d47']";
	String txt_uw_notes = "//span[@id='CTRL_TA']/textarea[@id]";
	String enable_prorate="//input[@name='$PTmpPage$pApplicationPage$pProRataEnabled' and @type='checkbox']";
			//"//span[text()='Accept Quote']/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	String resolved_manually_finance = "//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	String uw_case_status_pending_uw = "//div[contains(text(),'Thank you for applying')]/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	String uw_case_status_pending_payment="//p[contains(text(),'Thank you for accepting the')]/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	String clientadviser_final_status = "//p[contains(text(),'Thank you for accepting')]/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	String uw_case_status_resolved_cancelled="//div[contains(text(),'Your application has been cancelled')]/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	//String endorsement_pending_payment = "//div[contains(text(),'Thank you for accepting this quote')]/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	String endorsement_pending_payment = "//div[contains(text(),'Thank you')]/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	String client_number = "//span[text()='Client number']/following-sibling::div/span";
	String number_of_students = "//input[@name='$PpyWorkPage$pApplicationPage$pNumberOfStudents']";
	String number_of_parking_spots = "//input[@name='$PpyWorkPage$pApplicationPage$pNumberOfParkingSpots']";
	String insurance_start_date = "//img[@data-test-id='202004092226510989570-DatePicker']";
	String market_stall_datepicker = "//img[@data-test-id='202004291016470070424-DatePicker']";
	String future_date = "(//td[@class='calcell today selected']/following-sibling::td/a)[1]";
	String past_date = "(//td[@class='calcell today selected']/preceding-sibling::td/a)[1]";
	String current_date="//a[@id='todayLink']";
	//String checkbox_declaration = "//input[@name='$PpyWorkPage$pApplicationPage$pTermsAcceptance' and @type='checkbox']";
	String checkbox_declaration = "//*[label[text()='I hereby declare the above information is correct']]/input[@type='checkbox']";
	String finish_button = "//button[@data-test-id='20161017110917023277518']";
	//String continue_button="//button[@data-test-id='20161017110917023176385']";
	String continue_button="//button[@title='Click here to submit' and text()='Continue']";
	String fullterm_Cancel_No="//label[@for='136b067cfalse']";
	String fullterm_Cancel_Yes="//label[@for='136b067ctrue']";
	String cancellation_effectiveDate="//img[@name='CalendarImg-84987728']";
//	String base_premium="//span[@data-test-id='202004230142460318293']";
//	String gst_screen="//span[@data-test-id='202004230150410659399']";
//	String stamp_duty="//span[@data-test-id='202004230150410659389']";
//	String total_premium="//span[@data-test-id='202004230150410659334']";
	String base_premium="//span[text()='Base premium']/following-sibling::div//span[@data-ctl='Text']";
	String gst_screen="//span[text()='GST']/following-sibling::div//span[@data-ctl='Text']";
	String stamp_duty="//span[text()='Stamp duty']/following-sibling::div//span[@data-ctl='Text']";
	String total_premium="//div[text()='Total premium']/following-sibling::div//span[@data-ctl='Text']";
	String Return_premium="//div[text()='Return premium']/following-sibling::div//span[@data-ctl='Text']";
	String endorsement_premium="//div[text()='Endorsement Premium']/following-sibling::div//span[@data-ctl='Text']";
	String tester = "VMIA";

	//String accept_quote="//button[@data-test-id='2014121801251706289770']";
	String accept_quote="//button[@data-test-id='20200430115906070685']";
	String acceptName = "PremiumDetails_pyWorkPage.ApplicationPage_12";
	String searchbox ="//input[@id='24dbd519']";
			//"//input[@data-test-id='2015030515570700545405']";
	String btnnew = "//i[@class='pi pi-plus']";
	String txtMangproductno = "//span[text()='Manage purchase order no']";
	String btnpolicyID = "//input[@data-test-id='20180528083705005312' and @name='$PpyWorkPage$pSearchStringPolicyID']";
	String btnsearch = "//button[@data-test-id='20141229012540069819653' and text()='Search']";
	String framegeneric = "//div[@style='display: block;']/iframe[@border='0']";
	String radbtn = "//input[@type='radio' and @data-test-id='202004102041080557870']";	
	String txtpurchaseorder = "//input[@data-test-id='202004092226510991722']";
	//String txtresolvedcomplete = "// div[@class='content-item content-field item-2 flex flex-row badge-bg-old centered dataValueRead']/span[text()='Resolved-Completed']";
	String txtresolvedcomplete = "//div[2]/span[text()='Resolved-Completed']";
	String txtemailinputEXTL ="//input[@placeholder='Email' and @type='text']";
	String txtPwdinputEXTL ="//div[@class='attrEntry']/child::input[@type='password']";
	String btnloginEXTL = "//button[text()='Login']";
	String btndownloaddocument = "(//button[@title='Click to download policy documents for Group Personal Accident' and text()='Download documents'])[2]";
	String frame3EXtL = "//iframe[@name='PegaGadget0Ifr']";
	String btninvoice = "//button[text()='Invoice']";
	String btndownload = "(//a[text()='Download' and @data-test-id='202008242231470293420'])[1]";
	String btn_New_humberger = "//span[@class='menu-item-title' and text()='New']";
	String btn_mangorderno_hum = "//li[@title='Manage Purchase Order No']"; 
	String btnhamburger ="//a[@id='appview-nav-toggle-one']";
	String finalstatus = "(//span[@class='badge_text'])[3]";
	String txt_add_Pno_newpolicy = "//input[@name='$PpyWorkPage$pApplicationPage$pPONumber']";
	String policyNumber_ele = "(//div[@class='flex-paragraph']//p[contains(text(),'Thank')])[1]\n";
	
	String btn_approve_quote = "//button[text()='Approve Quote']";
	String btncancelpolicy = "//span[text()='Cancel policy']";
	String btn_approve_quote1 = "//button[text()='Approve Quote']";
	String button_NoteInterstedPolicy="//span[text()='Note Interested Party CoC']";

	String btn_approve_quote2 = "//button[text()='Approve quote']";

	
	
	String selectQueueDropdown="//i[@title='Select work owner']";
    String vmia_logo = "//img[@title='vmia logo']";
    String search_uw_portal = "//input[@id='24dbd519']";
    String UnderwritingOption="//div[@class='content-item content-field item-1 remove-top-spacing remove-left-spacing remove-bottom-spacing remove-right-spacing flex flex-row field-name-ellipsis dataValueRead']/";
    String filterOption_Asset="//table[@id='gridLayoutTable']/tbody[@id='gridTableBody']/tr/th[2]//div/a[@title='Open Menu']";
    String filterOption="(//table[@id='gridLayoutTable']/tbody[@id='gridTableBody']/tr/th[3]//div/a[@title='Open Menu'])[1]";
    String uw_applyfilter = "//span[text()='Apply filter']";
    String uw_clickcase="//table[@id='gridLayoutTable']/tbody[@id='gridTableBody']/tr[2]//a";
    String UW_Casenumber_TH0="(//div[@class='gridHeaderLabel '] [text()='Case number'])[1]";
    String UW_Casenumber_TH1="(//div[@class='gridHeaderLabel '] [text()='Case number'])[1]";
	String UW_Casenumber_TH="(//div[@class='gridHeaderLabel '] [text()='Case ID'])[1]";
	String UW_search_applyfilter = "//button[@data-test-id='201604060130370006117741']";
	String UW_search_text = "//div[@pyclassname=\"Embed-FilterColumn\"]//span[@data-control-mode='input']/input";
	String AccptQuoate_NO="//input[@id='5bdf9196No']//following-sibling::label";
	String AccptQuoate_YES="//input[@id='5bdf9196Yes']//following-sibling::label";
	String AccpetQuoate_purachseNo="//input[@name='$PpyWorkPage$pApplicationPage$pPONumber']";
	String uw_portal_dialog_submit_button_accept_quoate="AcceptQuoteModalTemplate_pyWorkPage.ApplicationPage_10";
	String AcceptQuate_Submit="//button[text()='Submit']";
//	String begincase = "(//button[@data-test-id='201609091025020567152987'])[last()]";
	String begincase = "(//button[@title='Begin'])[last()]";
	String uw_accept_quote = "//button[@data-test-id='20200430115906070685']";
	String uw_submit = "//button[@data-test-id='2014121801251706289770']";
	String dashboard_filter_caseid ="//table[@summary='My tasks table']/tbody//th[1]/div//a";
	String search_filter_textbox = "//input[@data-test-id='201411181100280377101613' and @name]";
	String apply_button_filter = "//button[contains(@onclick,'OK')]";
	String first_case_in_filter_table = "//a[@data-test-id='201410081557130001199937']";
	String start_date="//input[@name='$PpyWorkPage$pApplicationPage$pPolicyEffectiveDate']";
	String expiry_date = "//span[@data-test-id='202004092226510990863']";
	String prop_extra_amount = "//input[@name='$PpyWorkPage$pApplicationPage$pExtraAmount']";
	String liab_risk_question_1_No = "//label[@data-test-id='20200420183943018480-Label']//following-sibling::div//label[text()='No']";
	String liab_risk_question_1_Yes = "//label[@data-test-id='20200420183943018480-Label']//following-sibling::div//label[text()='Yes']";
	String hire_out_facilities_dropdown = "//select[@data-test-id='20200420183943018553']";
	String liab_risk_question_2_No = "//label[@data-test-id='202004201839430191914-Label']//following-sibling::div//label[text()='No']";
	String liab_risk_question_2_Yes = "//label[@data-test-id='202004201839430191914-Label']//following-sibling::div//label[text()='Yes']";
	String liab_number_unincorporated_market_stall_holders = "//input[@data-test-id='202004201839430193592']";
	String add_selected_date_button = "//button[@data-test-id='202004291016470071569']";
//	String btnno = "//label[text()='No']";
	String btnno = "//input[@name='$PpyWorkPage$pApplicationPage$pRouteBackToRiskAdviser']/following-sibling::label[text()='No']";
	String btnyes = "//label[text()='Yes']";
	String RiskAdvisorWorkBasket = "//h3[text()='My workbaskets']/parent::div";
	String RiskAdvisorQueue = "$PpyDisplayHarness$ppxUserDashboard$ppySlots$l1$ppyWidgets$l1$ppyWidget$pPropertyForParameters";
    String followingWork_queue="//div[@aria-label='Followed work']";

    String Hamburger="appview-nav-toggle-one";
    String Search="//span[@class='menu-item-title-wrap']/span[@class='menu-item-title' and text()='Business Rules']";
    String Search_option_policy="//label[@for='6ae06d44Policy']";
    String Search_criteria_policyNumber="$PpyDisplayHarness$pSearchStringPolicyID";
    String Search_button="(//div[text()='Search'])[2]";
    String Search_Select="//a[text()='V018161']";
    String policyHistory="//h3[text()='Policy history']";

	String travelDays = "//div[contains(@aria-label,'Do you expect')]//label[text()='Yes']";
	String countryList = "//div[contains(@aria-label,'Are you travelling')]//label[text()='Yes']";
	String countyTravellingToImg = "//*[@name='selected-placeholder']/following-sibling::i";
	String countrySelection = "//span[text()='Yemen']";
	String domesticTripAddBtn = "TravelRecord_pyWorkPage.ApplicationPage_52";
	String destinationname="$PpyWorkPage$pApplicationPage$pDomesticTrips$l1$pDestination";
	String internationalTripAddBtn = "TravelRecord_pyWorkPage.ApplicationPage_109";
	String domFromdateImg = "//input[@name='$PpyWorkPage$pApplicationPage$pDomesticTrips$l1$pEffectiveDate']/following-sibling::img";
	String domTodateImg = "//input[@name='$PpyWorkPage$pApplicationPage$pDomesticTrips$l1$pEndDate']/following-sibling::img";
	String domStudents = "$PpyWorkPage$pApplicationPage$pDomesticTrips$l1$pNoOfStudents";
	String domStaff = "$PpyWorkPage$pApplicationPage$pDomesticTrips$l1$pNoOfAdults";
	String intDestinitionName="$PpyWorkPage$pApplicationPage$pInternationalTrips$l1$pDestination";
	String effDateImg = "//input[@name='$PpyWorkPage$pApplicationPage$pChangeEffectiveDate']/following-sibling::img";
	String intFromdateImg = "//input[@name='$PpyWorkPage$pApplicationPage$pInternationalTrips$l1$pEffectiveDate']/following-sibling::img";
	String intTodateImg = "//input[@name='$PpyWorkPage$pApplicationPage$pInternationalTrips$l1$pEndDate']/following-sibling::img";
	String intStudent = "$PpyWorkPage$pApplicationPage$pInternationalTrips$l1$pNoOfStudents";
	String intStaff = "$PpyWorkPage$pApplicationPage$pInternationalTrips$l1$pNoOfAdults";

	String Apply_Filter_RA = "//div[text()='Case ID']/parent::div[1]/following-sibling::span/a";
	String Apply_Filter_followingworkQueue="//div[text()='Case ID']/parent::div[@aria-describedby='titleDesc69093509']/following-sibling::span/a";
	String first_case_in_ra_fliter = "//a[@title='Click here to open the object']";
	String liab_risk_question_gym_No = "//label[@data-test-id='202004201839430187998-Label']//following-sibling::div//label[text()='No']";
	String prop_security_B2B_Monitored = "//label[text()='B2B monitored']/preceding-sibling::input[@type='checkbox']";
	String prop_security_local_alarms = "//label[text()='Local alarms']/preceding-sibling::input[@type='checkbox']";
	String prop_security_security_camera = "//label[contains(text(),'Security camera')]/preceding-sibling::input[@type='checkbox']";
	String prop_security_lock_windows = "//label[contains(text(),'deadlock windows')]/preceding-sibling::input[@type='checkbox']";
	String prop_security_bollards = "//label[contains(text(),'Bollards on')]/preceding-sibling::input[@type='checkbox']";
	String prop_over_ten_million_yes = "//input[@name='$PpyWorkPage$pApplicationPage$pPropertyValueOver']/following-sibling::label[text()='Yes']";
	String prop_over_ten_million_no = "//input[@name='$PpyWorkPage$pApplicationPage$pPropertyValueOver']/following-sibling::label[text()='No']";
	String info_section = "//h2[text()='Information']";
	String info_number_of_students = "//span[@data-test-id='202004092226510991722']";
	String info_property_value = "//span[@data-test-id='202004201902020346744']";
	String attachment_button = "//button[@data-test-id='2014121601304102071215']";
	String attach_file_button = "//span[text()='Attach File']";
	String attach_button = "//button[@title='Attach']";
	String Switchorg = "//select[@id='c6d76341']";
	String Submit_UW_Dialogue="//button[text()='  Submit ']";
	
	String search_feild_internal = "//input[@id='24dbd519']";
	String three_dots = "//i[@class='pi pi-more pi-blue pi-regular']";
	String start_search = "//span[text()='Start research']";
	String btn_begin = "//button[@title='Begin']";

	
	String Asset_Approve="//label[@for='20d6d207Approve']";
	String Asset_Reject="//label[@for='20d6d207Reject']";
	String Asset_req_more_info="//label[@for='20d6d207Request more information']";
	String Asset_moreinfo="$PpyWorkPage$pComments";
	String Asset_Need_Endorstment_Yes="//label[@for='79bbcee9true']";
	String Asset_Need_Endorstment_No="//label[@for='79bbcee9false']";
	String Asset_Submit="//button[text()='Submit']";
	String btn_create_qoute = "//button[text()='Create quote']";


	String add_Dedectible_button="//h2[text()='Deductible']/parent::div/span//span";
	String add_Dedectible_marined_liablity="//span[text()='Marine Liability']/parent::div/following-sibling::div//input";
	String add_Dedectible_Property_damage="//span[text()='Property damage']/parent::div/following-sibling::div//input";
    String add_Dedectible_PersonalorOtherInujury="//span[text()='Personal Injury/ Other Injury']/parent::div/following-sibling::div//input";
    String Add_addtional_Dedectible="//button[text()='Add Additional Deductible']/parent::span";
	String Add_addtional_Dedectible_additeam="//button[text()='Add item']";
	String Add__addtional_Dedectible_addname="//input[contains(@name,'AdditionalDeductible') and contains(@name,'Name')]";
	String Add__addtional_Dedectible_addCAmount="(//input[contains(@name,'AdditionalDeductible') and contains(@name,'ComponentValue') ])[2]";

	String Add_addtional_Dedectibles="//button[text()='Add additional deductible(s)']/parent::span";
	String Add_addtional_deductible_description = "(//input[contains(@name,'Description') and contains(@name,'AdditionalDeductible')])[2]";


	//construction_mutliyear
	String finalize_Quoate_Options="//h2[text()='Territorial Limits']/parent::div/span//button/i";
	String masterPolicyNumber="$PpyWorkPage$pApplicationPage$pQuotes$l1$pMasterPolicyNo";
	String constructionPeriod="$PpyWorkPage$pApplicationPage$pQuotes$l1$pConstructionPeriod";
	String updateDefectsLibilityPeriod="$PpyWorkPage$pApplicationPage$pQuotes$l1$pDefectsLiabilityPeriod";
	String performancetestingPeriod="$PpyWorkPage$pApplicationPage$pQuotes$l1$pPerformanceTestingPeriod";



	String FrameTextArea="//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']/p";
	String RiskAdvisorCaseNumber = "//div[text()='Case number']/parent::div[@aria-describedby='titleDesc1252144927']";


	String add_Endorsemnt_text_button = "//a[text()='Add endorsement text']";
	String extend_Endo_Yes = "//label[@for='2e56453atrue']";
	String extend_Endo_No = "//label[@for='2e56453afalse']";
	String Endorsement_text_button = "(//h2[text()='Endorsement text'])[1]";

	String internal_frame = "//iframe[@class='cke_wysiwyg_frame cke_reset']";
	String internal_text = "//html[@dir='ltr']/body/p";

	String mixed_construction = "//label[text()='Mixed construction']";
	String brick_construction = "//label[text()='Brick']";
	String construction_timber = "//label[contains(text(),'Timber')]/preceding-sibling::input[@type='checkbox']";
	String construction_asbestos = "//label[contains(text(),'Asbestos')]/preceding-sibling::input[@type='checkbox']";
	String construction_cladding = "//label[contains(text(),'Cladding')]/preceding-sibling::input[@type='checkbox']";
	String construction_eps_panelling = "//label[contains(text(),'EPS Panelling')]/preceding-sibling::input[@type='checkbox']";
	String fire_protection_sprinklers = "//label[contains(text(),'Sprinklers')]/preceding-sibling::input[@type='checkbox']";
	String fire_protection_fire_detectors = "//label[contains(text(),'Fire Detectors')]/preceding-sibling::input[@type='checkbox']";
	String fire_protection_extinguishers = "//label[contains(text(),'Extinguishers')]/preceding-sibling::input[@type='checkbox']";
	String actions_button = "//button[@data-test-id='20141009112850013521365']";
	String call_us_now_radio = "//label[text()='Call us now']";
	String call_back_radio = "//label[text()='Request a call back']";
	String call_back_notes ="//textarea[@data-test-id='202005302232580309641']";
	String actions_button_dec_page = "//button[@data-test-id='20141210043348063729986']";
	String action_dec="//button[@data-test-id='20141009112850013521365']";
	String withdraw= "//span[text()=\"Withdraw application\"]";
	String withdraw_reason= "//option[text()=\"Premium\"]/ancestor::select";
	String btn_submit= "//button[@data-test-id='2014121801251706289770']";
	String case_status_withdrawn = "//span[@data-test-id=\"20141009112850013217103\"]/parent::div/following-sibling::div/div/div/div/following-sibling::div/following-sibling::div/span";
	String actions_cancel = "//span[@class='menu-item-title' and text()='Cancel Application']";
	String actions_withdraw = "//span[@class='menu-item-title' and text()='Withdraw Application']";
	String actions_update = "//span[@class='menu-item-title' and text()='Update Information']";
	String actions_discuss = "//span[@class='menu-item-title' and text()='Discuss With VMIA']";
	String cancel_reason = "//select[@data-test-id='2015072204461602903949']";
	String cancel_comments = "//textarea[@data-test-id='202004272223120290214']";
	String withdraw_comments = "//textarea[@data-test-id='202004272223120290214']";
	String left_toggle = "//a[@id='appview-nav-toggle-one']";
	String my_open_actions = "//span[text()='My Open Actions']";
	String uw_portal_create_quote_button = "//button[@data-test-id='202004252105520388734']";
	String uw_portal_dialog_base_premium_textbox = "//input[@data-test-id='202004252104470317166']";
	String uw_portal_manual_prricing_reason="$PTmpPage$pApplicationPage$pManualPricingReason";
	String uw_portal_dialog_notes = "//textarea[@data-test-id='202004241734320377112']";
	String uw_portal_dialog_submit_button = "//button[@id='ModalButtonSubmit' and @title='Click here to submit work']";
	String mv_light_vehicle_tab = "//h3[text()='Light vehicle']/ancestor::div[@data-test-id='202005052304160161671_header']";
	String mv_bus_tab = "//h3[text()='Bus']/ancestor::div[@data-test-id='202005052304160161671_header']";
	String mv_trailer_tab = "//h3[text()='Trailer']/ancestor::div[@data-test-id='202005052304160161671_header']";
	String mv_machinery_tab = "//h3[text()='Machinery and plant']/ancestor::div[@data-test-id='202005052304160161671_header']";
	String mv_other_tab = "//h3[text()='Other']/ancestor::div[@data-test-id='202005052304160161671_header']";
	String lightvehicle_add_vehicle_button = "//div[@data-lg-child-id='1']//button[@data-test-id='2015071603054009744126']";
	String bus_add_vehicle_button = "//div[@data-lg-child-id='2']//button[@data-test-id='2015071603054009744126']";
	String trailer_add_vehicle_button = "//div[@data-lg-child-id='3']//button[@data-test-id='2015071603054009744126']";
	String other_add_vehicle_button = "//div[@data-lg-child-id='5']//button[@data-test-id='2015071603054009744126']";
	String machinery_add_vehicle_button = "//div[@data-lg-child-id='4']//button[@data-test-id='2015071603054009744126']";
	String other_vehicle_type = "//div[@data-lg-child-id='5']//input[@data-test-id='202005052335110612252']";
	String lighvehicle_manufacture_year = "//div[@data-lg-child-id='1']//input[@data-test-id='2016072109335505834280']";
	String bus_manufacture_year = "//div[@data-lg-child-id='2']//input[@data-test-id='2016072109335505834280']";
	String trailer_manufacture_year = "//div[@data-lg-child-id='3']//input[@data-test-id='2016072109335505834280']";
	String other_manufacture_year = "//div[@data-lg-child-id='5']//input[@data-test-id='2016072109335505834280']";
	String lighvehicle_make = "//div[@data-lg-child-id='1']//input[@data-test-id='202005052304160187214']";
	String bus_make = "//div[@data-lg-child-id='2']//input[@data-test-id='202005052304160187214']";
	String trailer_make = "//div[@data-lg-child-id='3']//input[@data-test-id='202005052304160187214']";
	String other_make = "//div[@data-lg-child-id='5']//input[@data-test-id='202005052304160187214']";
	String lighvehicle_model = "//div[@data-lg-child-id='1']//input[@data-test-id='202005052304160188455']";
	String bus_model = "//div[@data-lg-child-id='2']//input[@data-test-id='202005052304160188455']";
	String trailer_model = "//div[@data-lg-child-id='3']//input[@data-test-id='202005052304160188455']";
	String other_model = "//div[@data-lg-child-id='5']//input[@data-test-id='202005052304160188455']";
	String lighvehicle_regno = "//div[@data-lg-child-id='1']//input[@data-test-id='202005052304160188702']";
	String bus_regno = "//div[@data-lg-child-id='2']//input[@data-test-id='202005052304160188702']";
	String trailer_regno = "//div[@data-lg-child-id='3']//input[@data-test-id='202005052304160188702']";
	String other_regno = "//div[@data-lg-child-id='5']//input[@data-test-id='202005052304160188702']";
	String bus_current_market_value = "//div[@data-lg-child-id='2']//input[@data-test-id='202005052304160189956']";
	String other_current_market_value = "//div[@data-lg-child-id='5']//input[@data-test-id='202005052304160189956']";
	String mv_claim_or_loss_exp_ques_no = "//label[@data-test-id='202005060003070627137-Label']//following-sibling::div//label[text()='No']";
	String mv_claim_or_loss_exp_ques_yes = "//label[@data-test-id='202005060003070627137-Label']//following-sibling::div//label[text()='Yes']";
	String mv_use_of_vehicle_ques_no = "//label[@data-test-id='202005060419550486494-Label']//following-sibling::div//label[text()='No']";
	String mv_use_of_vehicle_ques_yes = "//label[@data-test-id='202005060419550486494-Label']//following-sibling::div//label[text()='Yes']";
	String mv_vehicles_on_hire_ques_no = "//label[@data-test-id='202005060546320359755-Label']//following-sibling::div//label[text()='No']";
	String mv_vehicles_on_hire_ques_yes = "//label[@data-test-id='202005060546320359755-Label']//following-sibling::div//label[text()='Yes']";
	String endorsement_policyid_filter = "(//div[@data-test-id='202004282310440856371']/following::a[@title='Click to filter'])[1]";
	String policyid_search_box = "//input[@data-test-id='201411181100280377101613']";
	String update_policy_button = "//button[@data-test-id='202004290218390950185']";
	String more_updations_ques_no = "//label[contains(text(),'Do you need to update any other information about your policy')]//following-sibling::div//label[text()='No']";
	String cancellation_cover_no = "//label[contains(text(),'Do you need event cancellation cover')]//following-sibling::div//label[text()='No']";
	String queue_dropdown = "//i[@data-test-id='20160914073910060318989']";
	String finance_pending = "//span[contains(text(),'Pending finance')]";
	String financecaseid = "(//div[text()='Case ID'])[last()]";
	String finance_caseid= "//h2[text()=\"Work queue view\"]/parent::div//following-sibling::div/parent::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div//table//th[@data-test-id=\"201708061024470363306-th-1\"]/div[text()=\"Case number\"]";
	String financeCaseId = "//h2[text()=\"Work queue view\"]/parent::div//following-sibling::div/parent::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div//table//th//div[text()='Case ID']//a[@role='menu']";
	String financeCaseIdFilter = "//ul[@id='pyNavigation1658138796330']//span[text()='Apply filter']";
	String financecaseid_filter = "(//div[text()='Case ID']//a[@role='menu' and @title])[last()]";
	String finance_caseid_filter = "//h2[text()=\"Work queue view\"]/parent::div//following-sibling::div/parent::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div//following-sibling::div//table//th[@data-test-id=\"201708061024470363306-th-1\"]/div[text()=\"Case number\"]//a[@role=\"menu\"]";
	String finance_applyfilter = "//span[text()='Apply filter']";
	String finance_search_text = "//input[@data-test-id='20160524050907021913810']";
	String finance_apply = "//button[text()='Apply']";
	String finance_case_link = "//a[@data-test-id='201501071438430852311277']";
	//String finance_reason_manual_processing = "//textarea[@data-test-id='2020050816044305011']";
	String finance_reason_manual_processing = "//textarea[@data-test-id='2020050816044305011']";
	String financeReasonManualProcessing = "//textarea[@pn='.ProcessingReason']";
	String financePaymentDateLink = "(//input[@name='$PpyWorkPage$pApplicationPage$pSelectedQuote$pPolicyData$pPremiumBilling$pPayment$pPaymentDate']/parent::span/div/preceding::img)[5]";
	String financeSubmitButton = "//textarea[@pn='.ProcessingReason']/following::div/following::button[text()='Submit']";
	String finance_payment_date_link = "//img[@data-test-id='20150113072103010971982-DatePicker']";
	//String update_basepremium_uwportal = "//button[@data-test-id='202004301158040272157']";
	String update_basepremium_uwportal ="//body/div[@id='PEGA_HARNESS']/form[1]/div[3]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/span[2]/div[1]/div[1]/div[1]/div[1]/span[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/span[1]/button[1]/i[1]";
			//"//button[@data-test-id='202004301158040272157' and @name='PremiumDetails_pyWorkPage.ApplicationPage_27'][text()='Update']";
			//"//span[@data-test-id='202004230142460318293']//parent::span//parent::span//parent::div//parent::div/following-sibling::div/span/button";
	String update_riskinformation_uwportal = "//button[@data-test-id='202004301200200324889']";
	String exit_button = "//button[@data-test-id='20141210043348063930294']";
	String Quotes_tab_Riskadviosr="//div[@title='List of quotes']/h2";
	String policynumbertext_riskadvisor="//span[text()='Policy number']/following-sibling::div/span";
	String exitButton="//button[@name='pyCaseHeader_pyWorkPage_14']";


	String machinery_year = "//div[@data-lg-child-id='4']//input[@data-test-id='2016072109335505834280']";
	String machinery_make = "//div[@data-lg-child-id='4']//input[@data-test-id='202005052304160187214']";
	String machinery_model = "//div[@data-lg-child-id='4']//input[@data-test-id='202005052304160188455']";
	String machinery_regn_no = "//div[@data-lg-child-id='4']//input[@data-test-id='202005052304160188702']";
	String machinery_current_market_value = "//div[@data-lg-child-id='4']//input[@data-test-id='202005052304160189956']";
	String machinery_use_of_vehicle = "//div[@data-lg-child-id='4']//input[@data-test-id='202008251524140539204']";
	
//	String more_actions = "(//button[@data-test-id='202007221908250903861' and not(@tabindex)])[1]";
	String more_actions = "(//button[contains(@title, 'Click for more actions on policy')])[1]";
	String cancel_policy = "//span[text()='Cancel policy']";
//	String cancel_date = "//img[@data-test-id='202004092226510989570-DatePicker']";
	String cancel_date = "//input[contains(@name,'CancellationEffectiveDate')]/following-sibling::img";
	//String cancel_reason = "//select[@data-test-id='2015072204461602903949']";
	String cancel_declaration = "//input[@name='$PpyWorkPage$pApplicationPage$pTermsAcceptance' and @type='checkbox']";
	String StatusCase="//div[3]/span[@data-test-id='2016083016191602341167946']";
			//"
	String Exit_Button="//button[@title=\"Exit case\"]";
	String Refresh_policy = "//button[@title=\"Refresh policies\"]";
	String urgency= "//span[@data-test-id=\"2016081118080001821385\"]";
	String case_id= "//a[@title=\"Click here to open the object\"]";
	String uw_mlcb_button="(//h2[text()='Motor � Loss of No Claim Bonus']/parent::div/parent::div/following-sibling::div//button[@data-test-id='202004191943150280412'])[2]";
    String btnCBL="(//h2[text()='Combined liability']/parent::div/parent::div/following-sibling::div//button[@title='Select product'])[2]";
	String Number_Vehicles_txt="//input[@name='$PpyWorkPage$pApplicationPage$pNumberOfVehicles']";
	String Time_Period_Vehicles_Dropdwn="//select[@name='$PpyWorkPage$pApplicationPage$pVehicleUsage']";
	String Select_Daily="//*[@id='748d4fc5']/option[2]";
	String uw_jrn_button="(//h2[text()='Journey']/parent::div/parent::div/following-sibling::div//button[@title='Select product'])[2]";
	String uw_bt_button="(//h2[text()='Business travel']/../../following-sibling::*//button[@title='Select product'])[2]";
	
	String number_of_FTE = "//input[@name='$PpyWorkPage$pApplicationPage$pNumberofEmployees']";
	String jrn_risk_question_2_Yes = "//label[text()='Have you purchased a Journey insurance previously?']//following-sibling::div//label[text()='Yes']";
	String jrn_risk_question_2_No = "//label[text()='Have you purchased a Journey insurance previously?']//following-sibling::div//label[text()='No']";
	String lbl_FTE_number = "//span[span[text()='Total number of employees (Full Time Employees) that currently require journey insurance']]/following-sibling::div//span[text()]";
	
	/* Search and select org and contact */
	String zipcode = "//input[@data-test-id='20141219073811007312693']";
	String Text_box="//input[@data-test-id=\"20180528094158075422997\"]";
	String find_button = "//button[@data-test-id='201604240253100057139194']";
	String select_business_checkbox = "//input[@data-test-id='202004102041080557870']";
	String select_contact_checkbox = "//input[@data-test-id='202005252038490828115']";
	
	String update_policy = "//span[text()='Update policy']";
	
	String more_actions_policy_based = "//span[text()='dummypolicynumber']/ancestor::div[@node_name='ActivePolicyDetails']//button[contains(@title, 'Click for more actions on policy')]";
	String more_actions_disabled_policy_based = "//span[text()='dummy']/ancestor::div[@node_name='ActivePolicyDetails']//button[contains(@title, 'Click for more actions on policy') and @disabled]";
	String case_status = "//div[not(@style)]/span[@class='badge_text']";
	String Endorsement_EffectiveDate="//img[@name='CalendarImg-647b5417']";
	String Endorsment_policyChangeInfo="$PpyWorkPage$pApplicationPage$pUpdatePolicyDetails";
	String Endorsement_selectOption = "//div[@aria-label='Do you need to update any other information about your policy?']//label[text()='No']";
	
	/* Prop and Planning - GPA New application */
	String numberOfVolunteersGPAPropNPlan = "//input[@name='$PpyWorkPage$pApplicationPage$pNumberofVolunteers']";
	String numberOfBoardMembersGPAPropNPlan = "//input[@name='$PpyWorkPage$pApplicationPage$pNumberofBoardMembers']";
	String numberOfWorkExpGPAPropNPlan = "//input[@name='$PpyWorkPage$pApplicationPage$pWorkExp']";
	String numberOfPracPlacemntGPAPropNPlan = "//input[@name='$PpyWorkPage$pApplicationPage$pNumberofPracPlacemnt']";
	String numberOfAllHlthProfGPAPropNPlan = "//input[@name='$PpyWorkPage$pApplicationPage$pNumofAllHlthProf']";
	String numberOfOvrsMedPracGPAPropNPlan = "//input[@name='$PpyWorkPage$pApplicationPage$pNumofOvrsMedPrac']";
	String numberOfOthersGPAPropNPlan = "//input[@name='$PpyWorkPage$pApplicationPage$pNumofOthers']";
	String prevPolicyQuestion = "//label[text()='Have you purchased a Personal Accident insurance previously?']";
	String prevPolicyQuestionYesGPAPropNPlan = prevPolicyQuestion + "//following-sibling::*//label[text()='Yes']";
	String prevPolicyQuestionNoGPAPropNPlan = prevPolicyQuestion + "//following-sibling::*//label[text()='No']";
	String lbl_numberOfVolunteersGPAPropNPlan = "//span[text()='Volunteers']/following-sibling::div/span/span";
	
	/* Business Travel policy */
	String txtNoOfInternationalTrips = "//label[text()='International']/following-sibling::*//input";
	String txtNoOfDomesticTrips = "//label[text()='Domestic']/following-sibling::*//input";
	String lblTravelToListedCountries = "//label[contains(text(),'Are you travelling to any one of the following countries')]";
	String radTravelToListedCountriesYes = lblTravelToListedCountries + "//following-sibling::*//label[text()='Yes']";
	String radTravelToListedCountriesNo = lblTravelToListedCountries + "//following-sibling::*//label[text()='No']";
	String lblTravelExceed180days = "//label[contains(text(),'Do you expect any travel to exceed 180 days in duration')]";
	String radTravelExceed180daysYes = lblTravelExceed180days + "//following-sibling::*//label[text()='Yes']";
	String radTravelExceed180daysNo = lblTravelExceed180days + "//following-sibling::*//label[text()='No']";
	String txtEstimateNumber = "//label[text()='Estimate number of days']/following-sibling::*//input";
	
	
	
	String interaction_button = "//span[text()='Interaction']";
	

	String txtPreviousyear="//div[2]/div[1][contains(@class,'field-item dataValueWrite')]/span[1]/input";
	String txtCurrentyear="//div[3]/div[1][contains(@class,'field-item dataValueWrite')]/span[1]/input";
	String viewMore="//button[text()='View more']";
	String nomoreData="//div[text()='No more data to load']";
    String PolicyDetails="//div[contains(@class,'vmia-case-header-text')]/span";
    String radwrittendataprotectionyes = "//label [text()='Is there a written data protection policy and privacy policy that applies to the Organisation?']//parent::div//label[text()='Yes']";
    String radareallemployessyes= "//label[@for='90ce9113Yes']";
    String datereview = "//img[@alt='Choose from calendar']";
    String todayLink= "//a[@id='todayLink']";
    String txtbywhom= "//label [text()='By whom?'] //parent::div//following-sibling::div//input[1]";
    String raddoesorganisationdatayes= "//label[@for='23324963Yes']";
    String raddoesorganisationemployeeyes = "//label[@for='167f9e0eYes']";
    String radareemployessrequiredNo= "//label[@for='3d8e3b33No']";

  //Data Access recovery

    String raddoestheorganisationusefirewallNo= "//label [text()='Does the Organisation use firewalls to prevent unauthorised access connections from external networks and computer systems to internal networks?']//parent::div//label[text()='No']";
    String radDoestheOrganisationuseanti_virusNo = "//label [text()='Does the Organisation use anti-virus protections on all desktops, e-mail systems and mission critical servers to protect against viruses, worms, spyware and other malware?']//parent::div//label[text()='No']";
    String radDoestheOrganisationmonitor_no = "//label [text()='Does the Organisation monitor its network and computer systems for breaches of data security?']//parent::div//label[text()='No']";
    String radDoestheOrganisationhasphysicalsecurityNo ="//label [text()='Does the Organisation have physical security controls in place to prohibit and detect unauthorised access to their computer system and data centre?']//parent::div//label[text()='No']";
    String radDoestheOrganisationcollectNo="//label [text()='Does the Organisation collect, store, maintain or distribute credit card or other sensitive personally identifiable data?']//parent::div//label[text()='No']";
    String radistheaccessNo="//label [text()='Is the access to such sensitive data restricted?']//parent::div//label[text()='No']";
    String raddoestheorgprocessNo= "//label [text()='Does the Organisation process payments on behalf of others, including eCommerce transactions?']//parent::div//label[text()='No']";
    String raddoestheorghaveencryptionNo= "//label [text()='Does the Organisation have encryption requirements for data-in-transit data-at-rest to protect the integrity of sensitive data including data on portable media (e.g., laptops, DVD backup tapes, disk drives, USB devices, etc.)?']//parent::div//label[text()='No']";
    String raddoestheorganisationhasmaiantainbackupNo= "//label [text()='Does the Organisation have and maintain backup and recovery procedures for mission critical systems?']//parent::div//label[text()='No']";
    String radinformationassetesNo = "//label [text()='Does the Organisation have and maintain backup and recovery procedures for data and information assets?']//parent::div//label[text()='No']";
    String raddoestheorghasresponseplanNo= "//label [text()='Does the organisation have a digital recovery and response plan?']//parent::div//label[text()='No']";
    String Is_Remote_User_authticated_No="//label[@for='eeb48995No']";
    String raddoestheorgrequiresremoteplanNo= "//label [text()='Does the organisation have a digital recovery and response plan?']//parent::div//label[text()='No']";
    String raddoestheorghaveanendoflifeNo= "//label [text()='Does the organisation have an end of life management plan for redundant or unsupported software / hardware?']//parent::div//label[text()='No']";
    String raddoestheorgimplememetnetworkNo= "//label [text()='Does the organisation implement any network segmentation?']//parent::div//label[text()='No']";
    String radMultifactorauthinticationNo= "//label [text()='Does the organisation use multi factor authentication?']//parent::div//label[text()='No']";
    String radDoestheorghavebusinesscontinuityNo= "//label [text()='Does the organisation have a business continuity plan which enables the organisation to respond to cyber incidents and disruptions in order to continue operation of critical of critical business processes?']//parent::div//label[text()='No']";
    String RaddoestheorgrequireNo= "//label [text()='Does the organisation require any employee to use their own personal devices for the purpose of work activities?']//parent::div//label[text()='No']";
    String RadfrequentlyNo = "//input[@name='$PpyWorkPage$pApplicationPage$pSoftwarePatchFrequency']//parent::span/label[text()='Weekly']";
    String Txtpercentagefield= "//input[@data-test-id='202101292339370607406']";
    String raddailyfrequentlyNo= "//input[@name='$PpyWorkPage$pApplicationPage$pOrgNetworkVulnFrequency']/parent::span//label[text()='Daily']";
	String How_Frequently_you_Applied="//label[@for='594d970fQuarterly']";
	String WhatPercentagetFund_ItSecurity_gets="$PpyWorkPage$pApplicationPage$pOrgBudgetforIT";
	String IsSystemProtectedFromPotentialRisk="//label[@for='ac609b89No']";
	String howFrequentVunerablityTestDone="//label[@for='8247fefdWeekly']";
	String isAnyRelinanceOnSharedITPlatform_yes="//label[@for='096bd020No']";
  //OutsourcingActivities


    String raddoestheorgoutsourceNo = "//label[text()='Does the Organisation outsource any part of its network, computer system or information security functions?']//parent::div//label[text()='No']";
    String raddoestheorgoutsourceanydataNo = "//label[text()='Does the Organisation outsource any data collection and/or data processing?']//parent::div//label[text()='No']";
    String raddoestheorgrequireNo= "//label[text()='Does the Organisation require indemnification from outsourcers for any liability attributable to them?']//parent::div//label[text()='No']";
    String raddoestheOrgselectNo= "//label[text()='Does the Organisation select and manage outsourcers?']//parent::div//label[text()='No']";
    String raddoestheorgrequireoutsourceNo="//label[@for='65f0520fNo']";
    String raddoestheorgrequireoutsource_No= "//label [text()='Does the organisation require outsourcers to comply with, be accredited or certified for any information security frameworks or information management schemes?']//parent::div//label[text()='No']";
    String rad_org_missioncriticalNo="//label[text()='If the organisation outsources mission-critical or revenue-generating operations to a third party, does the organisation have an alternative solution in the event of a third party interruption or failure?']//parent::div//label[text()='No']";

    //Claims Information

    String radOrgsubjectNo="//label[text()='Has the organisation been the subject of any investigation or audit in relation to data protection by a Data Protection Authority or other regulator?']//parent::div//label[text()='No']";
    String radOrgbeenNo = "//label[text()='Has the organisation ever been subject to a Data Subject Access Request?']//parent::div//label[text()='No']";
    String OrgregulatorNo= "//label[text()='Has the organisation ever been subject to an Enforcement Notice by a Data Protection Authority or any other regulator?']//parent::div//label[text()='No']";
    String OrgthispolicyNo = "//label[text()='Is the organisation after due inquiry aware of any actual or alleged fact or circumstance which may give rise to a claim under this policy?']//parent::div//label[text()='No']";

    //Review screen 

    String Checkboxreview = "//input[@type='checkbox' and @aria-describedby='$PpyWorkPage$pApplicationPage$pTermsAcceptanceError']";

  //Us-6036
    
    // Information screen

     String FromDateField= "//img[@alt='Choose from calendar']";
     
     String BtnAddLocation ="//button[@data-test-id='202102102208150099847']";
     String TxtstreetNumberField= "//input[@name='$PpyWorkPage$pApplicationPage$pAssetsInformation$l1$pAssetsLocation$pStreetNumber']";
     String TxtstreetName= "//input[@name='$PpyWorkPage$pApplicationPage$pAssetsInformation$l1$pAssetsLocation$pAddressLine1']";
     String Txtsuburb= "//input[@name='$PpyWorkPage$pApplicationPage$pAssetsInformation$l1$pAssetsLocation$pcity']";
     String zipcode1= "//input[@name='$PpyWorkPage$pApplicationPage$pAssetsInformation$l1$pAssetsLocation$pZipCode']";
     String state= "//input[@name='$PpyWorkPage$pApplicationPage$pAssetsInformation$l1$pAssetsLocation$pState']";
     String countrydropdown= "//select[@name='$PpyWorkPage$pApplicationPage$pAssetsInformation$l1$pAssetsLocation$pCountryCode']";
     String BtnAddFacility= "//button[@name='AddFecilityInfo_pyWorkPage.ApplicationPage.AssetsInformation(1)_52']";
     String DropFacilityType= "//select[@name='$PpyWorkPage$pApplicationPage$pAssetsInformation$l1$pFacilitiesInformation$l1$pFacilityType']";
     String TxtDescriptionofAsset="//td[@data-attribute-name='Name / Description of asset']//textarea";
     String TxtBuildingValue="//td[@data-attribute-name='Building Value at this address ($)']";
     String DropBuildingvaluetype="//select[@name='$PpyWorkPage$pApplicationPage$pAssetsInformation$l1$pFacilitiesInformation$l1$pBuildingValueType']";
     String TextContentValue= "//input[@data-test-id='202102052101500993562']";
     String DropContentValuetype= "//select[@data-test-id='202102052101500994484']";
     String TxtAnnualgross= "//input[@aria-describedby='$PpyWorkPage$pApplicationPage$pAnnualGrossRentPayableError']";
     String Txtestimateincreased= "//input[@aria-describedby='$PpyWorkPage$pApplicationPage$pEstimatedCoverError']";
     String DropOrgpreferred = "//select[@aria-describedby='$PpyWorkPage$pApplicationPage$pDeductibleForClaimsError']";
     String radhadanyclaimsNo= "//input[@name='$PpyWorkPage$pApplicationPage$pPreviousYearClaims'and @value='No']//parent::span/label[text()='No']";


   //  Review screen 

     String Checkbox_review = "//input[@type='checkbox' and @aria-describedby='$PpyWorkPage$pApplicationPage$pTermsAcceptanceError']";

//#US-6133	Policy process for remaining products (Motor vehicle, Cons risk
	String txtExecutiveVehicles1 = "//input[@id='eb17c690']";
	String txtExecutiveVehicles2 = "//input[@id='b83ee8f2']";
	String lblvehicles1 = "//label[contains(text(),'Do you have any vehicles with a value exceeding $500,000?')]";
	String radvehiclesYes1 = lblvehicles1 + "//following-sibling::*//label[text()='Yes']";
	String radvehiclesNo1 = lblvehicles1 + "//following-sibling::*//label[text()='No']";
	String lblvehicles3 = "//label[contains(text(),'Are your vehicles or plant and equipment used in underground mining')]";
	String radvehiclesYes3 = lblvehicles3 + "//following-sibling::*//label[text()='Yes']";
	String DoYouAnticipatePlantBelongingContractor = "//label[@for='31d63d9eNo']";
	String lblvehiclese4 = "//label[contains(text(),'Do you hire or lend any of your vehicles or plant & equipment out to anyone?')]";
	String radvehiclesYes4 = lblvehiclese4 + "//following-sibling::*//label[text()='Yes']";
	String radvehiclesNo4 = lblvehiclese4 + "//following-sibling::*//label[text()='No']";
	String lblvehiclese5 = "//label[contains(text(),'Are any of your employees, board members or voluntary workers authorised to use their privately-owned vehicles on your business?')]";
	String radvehiclesYes5 = lblvehiclese5 + "//following-sibling::*//label[text()='Yes']";
	String radvehiclesNo5 = lblvehiclese5 + "//following-sibling::*//label[text()='No']";
	String lblvehiclese2 = "//label[contains(text(),'Do you anticipate using plant & equipment belonging to contractors')]";
	String radvehiclesYes2 = lblvehiclese2 + "//following-sibling::*//label[text()='Yes']";
	String radvehiclesNo2 = "//label[@for='b27d01d3No']";
	String txtcarryingvehicles = "//textarea[@id='fce01e9b']";
	String lblDrivers1 = "//label[contains(text(),'Check their driving records for licence currency, offences and accidents?Do you use anticipate using plant & equipment belonging to contractors?')]";
	String radDriversYes1 = lblDrivers1 + "//following-sibling::*//label[text()='Yes']";
	String radDriverNo1 = lblDrivers1 + "//following-sibling::*//label[text()='No']";
	String lblDrivers2 = "//label[contains(text(),'Ensure drivers are adequately trained to operate the vehicles they will be using?')]";
	String radDriversYes2 = lblDrivers2 + "//following-sibling::*//label[text()='Yes']";
	String radDriverNo2 = lblDrivers2 + "//following-sibling::*//label[text()='No']";
	String btnaddInsurer = "//button[@data-test-id='2015071603054009744126']";
	String txtInsuerer1 = "//input[@id='f05c5355']";
	String txtInsuer2 = "//input[@id='2466922a']";

	//motorvehicleVicFleet

	String doesVechicleValueExceds5000_No="//label[@for='b27d01d3No']";
	String DoYouAnticipatePlantBelongingContractor_No="//label[@for='31d63d9eNo']";
	String isUnderGroundMiningHappening_No="//label[@for='50189d75No']";
	String doYouHireLendVehiclePlantEquipement_No="//label[@for='4455e86aNo']";
	String isEmployeeAuthorisedToUseOwnedVehicle_No="//label[@for='d9c803c7No']";




//Insurance history

	String lblInsurancehistory = "//label[contains(text(),'Are you presently insured (or have been insured within the last 5 years)')]";
	String radInsurancehistoryYes1 = lblInsurancehistory + "//following-sibling::*//label[text()='Yes']";
	String radInsurancehistoryNo1 = lblInsurancehistory + "//following-sibling::*//label[text()='No']";

	// Accumulation
	String lblAccumulation = "//label[contains(text(),'Is any single location where the accumulated value of your vehicles will exceed $10,000,000 at any one time')]";
	String radAccumulationYes1 = lblAccumulation + "//following-sibling::*//label[text()='Yes']";
	String radAccumulationNo1 = lblAccumulation + "//following-sibling::*//label[text()='No']";
	String txtnumofVehicles1 = "$PpyWorkPage$pApplicationPage$pVehicleDetails$l1$pVicFleetFunded";
	String txtnumVehicles2 = "$PpyWorkPage$pApplicationPage$pVehicleDetails$l1$pOthers";
	//sprint-4 release3 ---US-3050
	String chkprorata="//input[@id='eadeccf6']";
	String radcustomPremium="//label[contains(text(),'Custom premium')]";
	String txtcustombasrPrium="//input[@id='63f2dd76']";
	String txtcustomUWnote="//textarea[@id='5fde7c8a']";

	String Uw_Profession_Update="$PpyWorkPage$pApplicationPage$pProfession";
	String Uw_Profession_Update_Org="(//input[@name='$PpyWorkPage$pApplicationPage$pIsAtPolicyLevel'])[2]";
	String Uw_clientSpecific_Update_btn="//h2[text()='Client specific details']/parent::div/span//button//i";
	String Uw_clientSpecific_professional="//h2[text()='Profession']/ancestor::div[2]/div/i";
	String Profession_text="//h2[text()='Profession']/ancestor::div[4]/div[@id='EXPAND-INNERDIV']/div/div";


	
	String txtdel="//div[contains(text(),'Delegation limit is not sufficient to approve the ')]";
	
	//Public and products Liability:-

		String From_Date_Field= "//img[@data-test-id='202004092226510989570-DatePicker']";
		String TodayLink="//div[@class='pz-po-c controlcaldiv']//a[text()='Today']";
		String txtoperational_revenue= "//input[@name='$PpyWorkPage$pApplicationPage$pTotalAnnualRevenue$l1$pOperationalRevenue']";
		String txtVictorian_gov_funding= "//input[@name='$PpyWorkPage$pApplicationPage$pTotalAnnualRevenue$l1$pVictorianGovernmentFunding']";
		String txtVictorian_gov_other="//input[@name='$PpyWorkPage$pApplicationPage$pTotalAnnualRevenue$l1$pOtherFunding']";

		//Incident and potential claims:-

		String radIncident_No= "//label[text()='If your organisation is aware of any incidents or potential claims (where a claim has not been formally notified to VMIA) arising out of any occurrence with your business, please complete the table below.']//parent::div//label[text()='No']";

		//Interstate and overseas work :-


		String radInterstate_No="//label[text()='Does your organisation has any employees or contractors who carry out work on your agency’s behalf overseas, please complete the table below.']//parent::div//label[text()='No']";



		//Affilation :-

		String radaffilation_No = "//label[text()='Do you have any affiliations or shared services with research bodies, universities or other health service providers?']//parent::div//label[text()='No']";

		String radactivities_No= "//label[text()='Are there activities / services conducted on your sites by third parties for use by your customers, staff or patients? For example; canteens, cafes, imaging services, medical centres orflorists?']//parent::div//label[text()='No']";

		//Storage of hazardous substances or dangerous goods:-

		String Activities_Dropdown_icon= "//div[2]/div[1]/div[1]/i[1]";
				//"//input[@id='7abad305']";
		String Storage_dropdown_value= "//div[2]/div[1]/div[1]/ul[1]/li[1]/span[1]";
		String Aware_drpdown_icon= "//div[3]/div[1]/div[1]/i[1]";
		String Contaminated_dropdown = "//div[2]/div[1]/div[1]/ul[1]/li[1]/span[1]";

		//Airfield Liability :-

		String radorg_Liability= "//label [text()='If your organisation owns, operates, leases or maintains any landing strip, airfield or helipad, please complete the table below.']//parent::div//label[text()='No']";

		//Product Liability :-

		String radprod_liability = "//label [text()='Does your organisation manufacture, supply, import or export any products?']//parent::div//label[text()='No']";


		//Contractors / Sub contractors:-

		String contractors_Dropdown_icon = "//div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/i[1]";
		String contractors_Dropdown_value= "//div[2]/div[1]/div[1]/ul[1]/li[1]/span[1]";

		//Visitors :-

		String btnVisitor_Add_Button= "//button[@data-test-id='202102092125420015958']";
		String txtMax_no_of_visitors = "//input[@name='$PTempInterstateWork$pNumberOfVisitors']";
		String txtstreetno = "//input[@name='$PTempInterstateWork$pInterstateOrOverseasLocation$pStreetNumber']";
		String txtstreetname = "//input[@name='$PTempInterstateWork$pInterstateOrOverseasLocation$pAddressLine1']";
		String txtsuburb = "//input[@name='$PTempInterstateWork$pInterstateOrOverseasLocation$pcity']";
		String drpstate_dropdown ="//select[@name='$PTempInterstateWork$pInterstateOrOverseasLocation$pState']";
		String txtpost_code= "//input[@name='$PTempInterstateWork$pInterstateOrOverseasLocation$pZipCode']";


		//General Information 

		String radgeneral_info_No = "//label [text()='Has the name of the entity been changed, or has any other business/organisation been purchased or has any merger or consolidation of your business(es) taken place?']//parent::div//label[text()='No']";
		String txtquality_staff = "//input[@data-test-id='202102042353450395642']";

		//Insurance history :-


		String radorg_No="//label [text()='Is your organisation currently insured for professional indemnity?']//parent::div//label[text()='No']";
		String radorg_past_No = "//label [text()='Has your organisation been insured in the past?']//parent::div//label[text()='No']";
		String raddecline_No = "//label [text()='Declined a request for cover']//parent::div//label[text()='No']";
		String radimpose_No = "//label [text()='Impose special terms?']//parent::div//label[text()='No']";
		String radDecRenew_No = "//label [text()='Decline to renew your insurance?']//parent::div//label[text()='No']";
		String radCancRenew_No = "//label [text()='Cancel your insurance']//parent::div//label[text()='No']";

		//Professional Activities:-
		String btnaddItem="//a[contains(text(),'Add item')]";
		String txtstate= "//textarea[@name='$PpyWorkPage$pApplicationPage$pProfessionalServices']";
		String radnature_No= "//label [text()='Does the nature or type of the professional services or advice now undertaken by your organisation (or on your behalf) as described above, differ from the nature or type of professional services provided at any time in the past?']//parent::div//label[text()='No']";
		String txtnature_of_work = "//input[@name='$PpyWorkPage$pApplicationPage$pActivityDetails$l1$pNatureOfWork']";
		String txtpercentage= "//input[@name='$PpyWorkPage$pApplicationPage$pActivityDetails$l1$pCompletedWork']";
		String radorgu_No="//label [text()='Does your organisation seek to limit your liability in respect of advice or information given, or services provided to third parties?']//parent::div//label[text()='No']";

		//Joint Ventures:-

		String radjoint_ven_No="//label [text()='Has your organisation or any Principal been or are currently a member of any joint venture(s)']//parent::div//label[text()='No']";

		//Overseas work
		String Overseas_work_No= "//label [text()='Has your organisation ever undertaken, or is likely to undertake work overseas?']//parent::div//label[text()='No']";




		//Fee income:-

		String txtAustralia = "//input[@data-test-id='202102042353450394640']";

		String txtOverseas= "//input[@name='$PpyWorkPage$pApplicationPage$pFeeIncomeDetails$l1$pOverseasIncome']";


		//Contractors and Sub contractors:-

		String radportion_No="//label [text()='Is any portion of your organisation�s professional service sublet/contracted to others?']//parent::div//label[text()='No']";

		String radrequire_No= "//label [text()='Do you require contractors/sub-contractors as a condition of their appointment to maintain adequate insurance to indemnify you with respect to liabilities caused by their negligence?']//parent::div//label[text()='No']";

		String txtprovide= "//textarea[@name='$PpyWorkPage$pApplicationPage$pContractorsIndemnifyNoDetails']";

		String  txtstate1= "//textarea[@name='$PpyWorkPage$pApplicationPage$pGrossProfessionalFeeDetails']";

		String radcontract_no = "//label [text()='Does any contract or client represent more than 25% of your annual work?']//parent::div//label[text()='No']";

		//Risk Management:-

		String radwriiten_No= "//label [text()='Are written disclaimers included with advice being given?']//parent::div//label[text()='No']";
		String radverbal_No=  "//label [text()='Are verbal reports or advice always confirmed in writing?']//parent::div//label[text()='No']";
		String radtheorg_No=  "//label [text()='Does your organisation have a documented risk management program which addresses your professional duty?']//parent::div//label[text()='No']";
		String radindependent_No=  "//label [text()='Is the program independently reviewed/monitored/audited?']//parent::div//label[text()='No']";

		//Claims and circumstances:-

		String radpast10_No= "//label [text()='In the past 10 years has any claim been made or has negligence been alleged against any entity or individual to be insured by this insurance (including any prior entity and any of the present or former Principals), or have any circumstances which may give rise to a claim against any of these been notified to VMIA or other insurers?']//parent::div//label[text()='No']";
		String radanycircum_No= "//label [text()='Are there any circumstances not already notified to VMIA or other insurers that may give rise to a claim against any entity or individual to be covered by this insurance (including any prior entity and any of the present or former Principals)?']//parent::div//label[text()='No']";
		String radanyclaim_No= "//label [text()='Are there any claims against previous practices or an organisation which have been identified in this form which may give rise to a claim against any entity, organisation or individual to be covered by this insurance including any prior entity and any of the present or former Principals/Executives/Staff members)?']//parent::div//label[text()='No']";
		String radanyprinci_No= "//label [text()='Has any Principal/Executive or staff member ever been subject to disciplinary proceedings for professional misconduct?']//parent::div//label[text()='No']";
		String lnkRecoveries="//span[contains(text(),'Recoveries')]";	

		//US-4021
		String Add_note="//a[text()='Add notes']";
		
	
	
	String lblOrgName = "//div[contains(@class, 'vmia-portal-name')]";
	String imgSearch_UWPortal = "(//i[@title='Search'])[last()]";
	
	//Construction Risks
	String uw_con_button = "(//h2[contains(text(),'Construction risks')]/../../following-sibling::*//button[@title='Select product'])[2]";
	String lblPrevInsuranceDeclined = "//label[contains(text(),'Has anyone to be insured under this policy ever had contract works insurance declined, cancelled, renewal declined or special conditions imposed')]";
	String radPrevInsuranceDeclinedYes = lblPrevInsuranceDeclined + "//following-sibling::*//label[text()='Yes']";
	String radPrevInsuranceDeclinedNo ="//label[@for='346ef360No']";
	String lblLossOfContractWork = "//label[contains(text(),'Has anyone to be insured under this policy ever had any losses of a contract works or liability nature')]";
	String radLossOfContractWorkYes = lblLossOfContractWork + "//following-sibling::*//label[text()='Yes']";
	String radLossOfContractWorkNo = lblLossOfContractWork + "//following-sibling::*//label[text()='No']";
	String lblContractWorkType = "//label[contains(text(),'Do your contracts normally involve any of the following')]";
	String radContractWorkTypeYes = lblContractWorkType + "//following-sibling::*//label[text()='Yes']";
	String radContractWorkTypeNo = lblContractWorkType + "//following-sibling::*//label[text()='No']";
	String lblLocationWithAuthority = "//label[contains(text(),'Do you always identify and confirm the location of any underground services with the relevant authority')]";
	String radLocationWithAuthorityYes = lblLocationWithAuthority + "//following-sibling::*//label[text()='Yes']";
	String radLocationWithAuthorityNo = lblLocationWithAuthority + "//following-sibling::*//label[text()='No']";
	String btnContractAdd = "//button[contains(@name,'ContractTotalEstimation_pyWorkPage')]";
	String RndProduct="(//h2[text()='RnD Product']/ancestor::div[3]/div[2]//button)[2]";
	String RndPolicy_AddDocument_icon="//i[@class='pi pi-circle-plus-solid']";
	String attach_policy_details_option="//span[@class='menu-item-title' and text()='Attach File']";
	String Rnd_SelectFile_button="$PpyAttachmentPage$ppxAttachName";
	String Rnd_Attach_button="//button[@title='Attach']";
	String policyEffectdate="//input[@name='$PpyWorkPage$pApplicationPage$pPolicyEffectiveDate']";
	String policy_effective_Date_calender="//img[@name='CalendarImg-1c3d7095']";
	String policyExpiryDate="//span[text()='To (Expiry)']/parent::span/following-sibling::div/span";
	String getPolicyEffectdatetext="//span[text()='From (Start date)']/parent::span/following-sibling::div/span";
    String multiYeareffectivedate="//img[@name='CalendarImg-1c3d7095']";
	String multiYearExpirydate="//img[@name='CalendarImg-645c7c21']";
	//Newly Added - Construction Risk Policy
	String caseId="//td[@aria-describedby='Case ID']/span";
	String lblLossOfWorkContract = "//label[contains(text(),'After enquiry, please advise if anyone to be insured under this policy has ever had any significant losses (greater than $500,000) of a contract works or liability nature? (claims for material damage or for liability to third parties). If “Yes” please provide details')]";
	String lblLossOfWorkContractYes = lblLossOfWorkContract + "//following-sibling::*//label[text()='Yes']";
	String lblLossOfWorkContractNo = lblLossOfWorkContract + "//following-sibling::*//label[text()='No']";
	String view_edit_button="//button[text()='View/Edit all contracts']";
	String add_item_button="//a[text()='Add item']";
	String nameOfTheContractTextField = "//td[@data-attribute-name='Name of the contract']//input";
	String suburbTextField = "//td[@data-attribute-name='Suburb']//input";
	String postCodeTextField = "//td[@data-attribute-name='Postcode']//input";
	String contractTypeDropdown = "//td[@data-attribute-name='Contract type']//select";
	String contractorArrangedInsurance = "//td[@data-attribute-name='Principal arranged or contractor arranged insurance']//select";
	String estimatedProjectValueTextField = "//td[@data-attribute-name='Estimated project value']//input";
	String submitButton = "//*[@id='ModalButtonSubmit']";
	//Newlyadded - BasicProductPolicies - AircraftInsurance
	String riskCaseID = "//div[text()='Case ID']/parent::div/parent::div//a";
	String clientCaseNumber = "//div[text()='Case number']/parent::div/parent::div//a";
	String riskSearchTestField = "//input[@id='c8ea9fc9']";
	String clientSearchTextField = "//input[@id='6307c0b3']";
	String riskApplyBtn = "//button[text()='Apply']";
	String viewQueueTableCaseID = "(//tr[@class='cellCont'])[2]/following-sibling::tr/td[2]";
	String tablenumberID = "//tr[@class='cellCont']/following-sibling::tr/td[2]/div/span/a";
	String rejecCom = ".content-item.content-field.item-1.flex.vmia-field-fulldetails div span";

	//Combined liability
	String oprational_Revenue="$PpyWorkPage$pApplicationPage$pTotalAnnualRevenue$l1$pOperationalRevenue";
	String Victorion_Government_Funding="$PpyWorkPage$pApplicationPage$pTotalAnnualRevenue$l1$pVictorianGovernmentFunding";
	String OtherFunding="$PpyWorkPage$pApplicationPage$pTotalAnnualRevenue$l1$pOtherFunding";
	String isOrgAwareOfPotentialClaim_No="//label[@for='5c62fe83No']";
	String isAnyEmployeesAreOverseaWorker_No="//label[@for='4371af8dNo']";
	String DoYouHaveAnyAffiliations_No="//label[@for='7722f044No']";
	String AreThereThirdPartyVendors_No="//label[@for='d041bdfbNo']";
	String ActivitiesSubjectToAnEnvironmentProtectionLicense="//input[@data-target='$PpyWorkPage$pApplicationPage$pHazardousSubstancesList']";
	String AwareOfPresencePremisies="//input[@data-target='$PpyWorkPage$pApplicationPage$pPresenceOfPremises']";
	String AirfieldLiability_No="//label[@for='b9d574b1No']";
	String ProductLiability_No="//label[@for='6c87c955No']";
	String Orgnanization_Contractors="//input[@data-target='$PpyWorkPage$pApplicationPage$pContractors']";
	String Visitors_AddButton="Visitors_pyWorkPage.ApplicationPage_6";
	String Visitors_No_Visitors="$PTempInterstateWork$pNumberOfVisitors";
	String Visitors_StreetNumber="$PTempInterstateWork$pInterstateOrOverseasLocation$pStreetNumber";
	String Visitors_Town="$PTempInterstateWork$pInterstateOrOverseasLocation$pcity";
	String Visitors_Streetname="$PTempInterstateWork$pInterstateOrOverseasLocation$pAddressLine1";
	String Visitors_State="$PTempInterstateWork$pInterstateOrOverseasLocation$pState";
	String Visitors_Postcode="$PTempInterstateWork$pInterstateOrOverseasLocation$pZipCode";
	String Visitors_HasAnyOther="$PTempInterstateWork$pInterstateOrOverseasLocation$pcity";
	String HasNameChanged_No="//label[@for='07b1832fNo']";
	String Qualifiedstaff="$PpyWorkPage$pApplicationPage$pStaffDetails$l1$pStaffCount";
	String Is_InsuredProfessinalIndemity_No="//label[@for='c52b26bbNo']";
	String Is_InsuredProfessinalIndemity_InPast_No="//label[@for='42b3cf49No']";
	String IsDeclineRequestForCover_No="//label[@for='e0facff5No']";
	String IsImposedSpecialTerms_No="//label[@for='1781cc2fNo']";
	String IsDeclineRenewInsurance_No="//label[@for='2d070f5bNo']";
	String IsCancelYourInsurance_No="//label[@for='72e5c897No']";
	String AdviceByOrg="$PpyWorkPage$pApplicationPage$pProfessionalServices";
	String IsAdviceDifferFromService_No="//label[@for='491a9497No']";
	String IsOrgLimitYourLiability_Respect="//label[@for='afc02ca8No']";
	String Add_Activeites_Undertaken_btn="pzPegaDefaultGridIcons_pyWorkPage.ApplicationPage_4";
	String Add_Activities_NatureOfWork="$PpyWorkPage$pApplicationPage$pActivityDetails$l1$pNatureOfWork";
	String Add_Activities_Percentage="$PpyWorkPage$pApplicationPage$pActivityDetails$l1$pCompletedWork";
	String JoinVentures_Yes="//label[@for='5007d473No']";
	String isUndertakeOverseasWork_No="//label[@for='c0b35f44No']";
	String IsOrgProfessionalServiceContracted_No="//label[@for='23640811No']";
	String IsOrgNeedContract_No="//label[@for='a5fee01cNo']";
	String IscontractHold25PerWork="//label[@for='3630ce31No']";
	String ProfessionalIndeminites="$PpyWorkPage$pApplicationPage$pContractorsIndemnifyNoDetails";
	String YearfeesPaidconsultants="$PpyWorkPage$pApplicationPage$pGrossProfessionalFeeDetails";
	String DisclaimerOnAdvice_No="//label[@for='8b882488No']";
	String AdviceConfimedInWriting_No="//label[@for='4e771174No']";
	String IsRiskManagemenDocumented_No="//label[@for='916fa0f7No']";
	String Is_ProgramReviewed_No="//label[@for='23e74d91No']";
	String IsClaimMadeOnPast10Years_no="//label[@for='0e356416No']";
	String IsAnyClaimsisNotifiedtoVmia="//label[@for='b0801166No']";
	String IsAnyclaimAgainstPreviousPractices_No="//label[@for='2852b34aNo']";
	String Is_ExcutiveSubjectDiscplinaryAction_No="//label[@for='1fbd4677No']";

	// Construction Risks - Contract Detailss
	String btnExpandContractDetails = "(//div[@aria-label='Disclose Contract details'])[last()]";
	String lstContractType = "//select[@name='$PpyWorkPage$pApplicationPage$pContractInformation$l1$pContractType']";
	String txtNameOfTheContract = "//label[text()='Name of the contract']/following-sibling::*//input";
	String txtContractorsOrAdditionalInsuredsToBeNamed = "//label[contains(text(),'or Additional insureds to be named under this policy')]/following-sibling::*//textarea";
	String txtContractNoOrIdentification = "//label[contains(text(),'Contract No')]/following-sibling::*//input";
	String txtEstimatedProjectValue = "//label[text()='Estimated project value']/following-sibling::*//input";
	String imgContractCommencementDate = "//input[contains(@name,'ContractCommencementDate')]/following-sibling::img";
	String imgEstContractCompletionDate = "//input[contains(@name,'EstContractCompletionDate')]/following-sibling::img";
	String txtGeospatialCoordinates = "//label[text()='Geospatial coordinates']/following-sibling::*//input";
	String txtStreetNumber = "//label[text()='Street Number']/following-sibling::*//input";
	String txtStreetName = "//label[text()='Street Name']/following-sibling::*//input";
	String StreetAddress="//input[contains(@name,'AddressSelected')]";
	String contractpostcode="//input[contains(@name,'Postcode')]";
	String txtSuburbTown = "//label[text()='Suburb/Town']/following-sibling::*//input";
	String lstContractState = "//select[@name='$PpyWorkPage$pApplicationPage$pContractInformation$l1$pStreetAddress$pState']";
	String txtPostcode = "(//label[text()='Postcode']/following-sibling::*//input)[last()]";
	String lstContractCountry = "//select[@name='$PpyWorkPage$pApplicationPage$pContractInformation$l1$pStreetAddress$pcountry']";
	String lnkCantFindAddress = "//a[contains(text(),\"Can't find your address\")]";
	String txtLargestSingleContractValue = "//label[contains(text(),'What is the estimated maximum value of the largest single contract expected to be undertaken during the next 12 months on the one site')]/following-sibling::*//input";
	String lblDamageToExistingStructures = "//label[contains(text(),'Damage to Existing Structures')]";
	String radDamageToExistingStructuresYes = lblDamageToExistingStructures + "//following-sibling::*//label[text()='Yes']";
	String radDamageToExistingStructuresNo = lblDamageToExistingStructures + "//following-sibling::*//label[text()='No']";
	String lblContractorsPlantEquipment = "//label[contains(text(),'Contractors Plant')]";
	String radContractorsPlantEquipmentYes = lblContractorsPlantEquipment + "//following-sibling::*//label[text()='Yes']";
	String radContractorsPlantEquipmentNo = lblContractorsPlantEquipment + "//following-sibling::*//label[text()='No']";
	String txtChangesToConstructionPolicy = "//label[contains(text(),'Please provide the details of changes to the policy required')]/following-sibling::*//textarea";

	//Director and officer
	String limitOfIndemnity = "//label[text()='$10 million']";
	String officerDeductable= "//div[@aria-label='Officer']//label[text()='$5,000']";
	String publicBodyDeductable= "//div[@aria-label='Public Body']//label[text()='$5,000']";
	String EPLclaimDeductible = "//div[@aria-label='EPL Claim']//label[text()='$100,000']";
	String PollutionLiabilityDeductible = "//div[@aria-label='Pollution Liability']//label[text()='$50,000']";
	String publicBodyDescription = "$PpyWorkPage$pApplicationPage$pPublicBodyDesc";
	String dateFeild = "//span[@aria-describedby='$PpyWorkPage$pApplicationPage$pPublicBodyCommecedDTError']";
	String LegalStatus = "//label[text()='Statutory Authority']";
	String publicBodyAnotherEntity = "//label[text()='Is the Public Body a subsidiary of another entity?']/parent::div//label[text()='No']";
	String publicBodyAnnualReport = "//label[text()='Not applicable']";
	String publicBodyAquisition = "//label[text()='Has there been any acquisition or disposal, merger or takeover, or other substantial change / machinery of Government change undertaken by the Public Body or any of its subsidiaries in the last 24 months?']/parent::div//label[text()='No']";
	String publicBodySubsidiaries3rdParty = "//label[text()='Does the Public Body or any of its subsidiaries act as a manager of any fund or property for or on behalf of any third party?']/parent::div//label[text()='No']";
	String publicBodyAttached = "//label[text()='Is there any subsequent information of a material nature not disclosed in the attached financial statements / annual reports (refer “documents to be attached”) that could affect the financial position, capital structure or operation of the Public Body?']/parent::div//label[text()='No']";
	String publicBodySubsidiaresBussActUSA = "//label[text()='Does the Public Body or its subsidiaries have any assets or business activities in the USA or Canada']/parent::div//label[text()='No']";
	String publicBodySubsidiaresBussActAustralia = "//label[text()='Does the Public Body or any of its subsidiaries currently conduct or intend to conduct any business activities from a permanent location outside of Australia?']/parent::div//label[text()='No']";
	String publicBodyDeptTreasury = "//label[text()='Is the Department of Treasury and Finance’s Whole of Government Financial Management package applicable to the Public Body?']/parent::div//label[text()='No']";
	String CurrentAssets = "$PpyWorkPage$pApplicationPage$pCurrentAssets";
	String CurrentLiabilities ="$PpyWorkPage$pApplicationPage$pCurrentLiabilities";
    String BudgetedAnnualExpenditure = "$PpyWorkPage$pApplicationPage$pBudgetedAnnualExpenditure";
    String ConsoGovtFunds = "$PpyWorkPage$pApplicationPage$pConsoGovtFunds";
    String OtherBussActivities = "$PpyWorkPage$pApplicationPage$pOtherBussActivities";

    String directorAddBtn = "//button[text()=' Add']";
    String directorName = "$PpyWorkPage$pApplicationPage$pOfficersList$l1$pName";
    String directorProfessinal = "$PpyWorkPage$pApplicationPage$pOfficersList$l1$pProfessionalQualification";
    String dateIcon = "//img[@data-ctl='[\"DatePicker\"]']";
	String directorExp = "$PpyWorkPage$pApplicationPage$pOfficersList$l1$pYearsOfExp";
	String directorCurrentlyMembers = "//label[text()='Are any of the Officers of the Public Body currently members of any professional association (e.g. Australian Institute of Company Directors)?']/parent::div//label[text()='No']";
	String directorDeregistor = "//label[text()='Has any Officer of the Public Body been deregistered by any professional association?']/parent::div//label[text()='No']";
	String directorBankrupt = "//label[text()='Has any Officer of the Public Body been declared bankrupt or entered into a deed of assignment, composition or a scheme of arrangement with creditors?']/parent::div//label[text()='No']";
	String directorOfCompany = "//label[text()='Has any Officer of the Public Body been a director of a company or entity placed in administration, a scheme of arrangement, receivership, liquidation or provisional liquidation?']/parent::div//label[text()='No']";
	String directorResigned = "//label[text()='Have any Officers of the Public Body resigned or been replaced in the last 12 months?']/parent::div//label[text()='No']";
	String directorPBInterset = "//label[text()='Do any of the Officers or other employees of the Public Body hold (at the specified written request of the Public Body) any Board positions with any entities which are not subsidiaries for the purpose of representing the Public Body’s interest?']/parent::div//label[text()='No']";

	String directorAccounting = "//label[text()='Has the Public Body implemented an internal controls system?']/parent::div//label[text()='No']";
	String directoraudit = "//label[text()='Has external audit reviewed accounting processes and procedures?']/parent::div//label[text()='No']";
	String directorControl = "$PpyWorkPage$pApplicationPage$pControlReason";

	String EmployementEvent = "//label[text()='Does the Public Body anticipate any of the events referred to in the question above,  happening in the next 18 months']/parent::div//label[text()='No']";
	String EmplyeeHandbook = "//label[text()='Does the Public Body have an Employee Handbook or Manual which is issued to all employees?']/parent::div//label[text()='No']";
	String EmployeeInductiion = "//label[text()='Does the Public Body conduct formal induction of new employees and all employees?']/parent::div//label[text()='No']";
	String EmployeeInductiionCoverage = "//label[text()='Does the induction process cover all major policies and expected behaviours of employees?']/parent::div//label[text()='No']";
	String EmployeeInductiionRefresher = "//label[text()='Is refresher training conducted of all employees on all major policies and expected employee behaviour on a regular basis?']/parent::div//label[text()='No']";
	String noBtnPolicyProcedure = "//table[@pl_prop_class='VMIA-Ins-Data-Application-Comm-DAO-Generic']//tr/td[2]//label[text()='No']";
	String noBtnAvailableEmployees = "//table[@pl_prop_class='VMIA-Ins-Data-Application-Comm-DAO-Generic']//tr/td[3]//label[text()='No']";
	String EmployeeHR ="//label[text()='Is the Personnel / Human Resource Department and Risk Manager aware of any wrongful act which might afford grounds for any future claim such as would fall within the scope of the proposed insurance?']/parent::div//label[text()='No']";
	String EmployeePBPractise = "//label[text()='Does the Public Body have Employment Practices Liability or similar insurance?']/parent::div//label[text()='No']";

	String PollutionDangerous ="//label[text()='Are management of the Public Body aware of asbestos or other dangerous/hazardous substances in any of their owned buildings or property?']/parent::div//label[text()='No']";
	String PollutionManagement = "//label[text()='Does the Public Body have an environmental management system/plan in place?']/parent::div//label[text()='No']";
	String PollutionAsbestos = "//label[text()='Does the Public Body have an asbestos or dangerous goods register?']/parent::div//label[text()='No']";
	String PollutionAsbestosManage = "//label[text()='Does the Public Body have an asbestos management plan?']/parent::div//label[text()='No']";
	String PollutionHazardousGoods = "//label[text()='Does the Public Body have a hazardous goods management plan?']/parent::div//label[text()='No']";

	String InsuranceCurrentlyInForce ="//label[text()='Does the Public Body or any of subsidiaries have Directors & Officers Liability insurance currently in force?']/parent::div//label[text()='No']";
	String InsuranceClaimInIndeminity = "//label[text()='(a) Any claim which falls for indemnity under a policy similar to that proposed for (or would have fallen for indemnity under such a policy had such a policy been in effect)']/parent::div//label[text()='No']";
	String InsuranceIncidentToCliam = "//label[text()='(b) Any circumstance or incident which might potentially give rise to a claim under the scope of cover the policy for which this proposal is being completed']/parent::div//label[text()='No']";
	String InsuranceProsecution = "//label[text()='(c) Any prosecution of the corporation or its subsidiaries (or any person proposing for insurance) under the Corporations law, Occupational Health and Safety legislation, Workers Compensation legislation, the Trade Practices Act, or any other statute?']/parent::div//label[text()='No']";
	String InsuranceSubsidaryProsecution = "//label[text()='(d) Any Officer of the Public Body or its subsidiaries ever been subject to any prosecution, disciplinary action, been fined or penalised, or been subject of an inquiry or investigation in their capacity as a Director or Officer of any company?']/parent::div//label[text()='No']";
	String InsurancePracticesLiability  = "//label[text()='Have any Employment Practices Liability Claims been made against the Public Body or any Director, Officer of employee in their official capacity with the Public Body in the last 5 years?']/parent::div//label[text()='No']";


	//Motor
	String NumberOfVehicle = "$PpyWorkPage$pApplicationPage$pVehicleDetails$l1$pNumberOfVehicles";
	String MotorExceeding = "//label[text()='Do you have any vehicles with a value exceeding $500,000?']/parent::div//label[text()='No']";
	String MotorAnticipate ="//label[text()='Do you use anticipate using plant & equipment belonging to contractors?']/parent::div//label[text()='No']";
	String MotorBeach = "//label[text()='Are your vehicles or plant and equipment used in underground mining, mining of beach or river sand, work in, on, around, under, or over water, bush clearing/forestry or crane operations?']/parent::div//label[text()='No']";
	String MotorHire = "//label[text()='Do you hire or lend any of your vehicles or plant & equipment out to anyone?']/parent::div//label[text()='No']";
	String MotorPrivatelyOwned = "//label[text()='Are any of your employees, board members or voluntary workers authorised to use their privately-owned vehicles on your business?']/parent::div//label[text()='No']";
	String MotorDrivers = "//label[text()='Ensure drivers are adequately trained to operate the vehicles they will be using?']/parent::div//label[text()='No']";
	String MotorCurrency = "//label[text()='Check their driving records for licence currency, offences and accidents?Do you use anticipate using plant & equipment belonging to contractors?']/parent::div//label[text()='No']";
	String MotorInsured = "//label[text()='Are you presently insured (or have been insured within the last 5 years)']/parent::div//label[text()='No']";
	String MotorAccumulation = "//label[text()='Is any single location where the accumulated value of your vehicles will exceed $10,000,000 at any one time']/parent::div//label[text()='No']";
String updateMotor = "$PpyWorkPage$pApplicationPage$pUpdatePolicyDetails";
}
