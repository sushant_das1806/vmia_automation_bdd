package com.cucumber.framework.PageObjects;

public interface LoginPageLoc {
	
	String username_xpath="//input[@id='txtUserID']";
	String username_azure_xpath="//input[@id='signInName']";
	String password_xpath="//input[@id='txtPassword']";
	String password_azure_xpath="//input[@id='password']";
	String login_btn_xpath="//span[@class='loginButtonText']";
	String login_btn_azure_xpath="//button[@id='continue']";
	String Exit = "//button[@title='Exit case']";
	String verify_msg_xpath="//span[@data-test-id='2018031408355709381206']"; //span[text()='O2CKatowice']
	String loginclosebtn_xpath="//div[@class='pzbtn-mid' and text()='Close']";
	String logout_icon = "//a[@data-test-id='202004170924160508258']";
	String logout_iconn = "//a[@title='Logged in user name']";
	String logoff_button = "//span[@class='menu-item-title' and text()='Log off']";
    String error_msg_xpath="//div[@id='error' and text()='                   The information you entered was not recognized.          ']";
    String logout_icon_1= "//a[@title='Logged in user name']";
    String vmia_logo = "//img[@title='vmia logo']";
    String username="Username";
    String Password="Password";
	String dsLogoffIcon="//*[@title='Prashanth Lamba']";
	String dsLogoffLink="//*[text()='Log off']";
}

