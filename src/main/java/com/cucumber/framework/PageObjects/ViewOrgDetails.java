package com.cucumber.framework.PageObjects;

import com.cucumber.framework.context.TestData;
//import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
//import com.cucumber.framework.helper.Logger.LoggerHelper;

public class ViewOrgDetails extends SeleniumFunc implements ViewOrgDetailsLOC {
	//private final Logger log = LoggerHelper.getLogger(ViewOrgDetails.class);
	ViewOrgDetails vieworgdetails;
	TestData testdata;
	public ViewOrgDetails(WebDriver driver, TestData data) {
		super(driver);
		testdata=data;
	}

	public void sendAppObject(ViewOrgDetails vieworgdetails) {
		this.vieworgdetails = vieworgdetails;
	}
	
	public void verifyPolicyDetails() throws Exception {
		String policynumber = xpath_Genericmethod_getElementText(policy_number);
		String policypremium = xpath_Genericmethod_getElementText(policy_premium);
		String policytype = xpath_Genericmethod_getElementText(policy_type);
		
		if(policytype.equalsIgnoreCase("Group Personal Accident"))
		{
			System.out.println("Can go ahead");
			System.out.println("policy number : "+policynumber);
			System.out.println("policy type : "+policytype);
		}
		
	}
	
	public void createNewContact(String title, String firstname, String lastname, String phonenumber, String emailid, String acnttype) throws Exception {
		
		SeleniumFunc.xpath_GenericMethod_Click(add_contact);
		switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Sendkeys(job_title, title);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(first_name, firstname);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(last_name, lastname);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(phone_number, phonenumber);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(email, emailid);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(account_type, acnttype);
		SeleniumFunc.xpath_GenericMethod_Click(submit);
	}

	public void openAssetFromOrgnization(String assetid) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(AssetTab);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(assetidTH);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(AssetFilterIcon);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(ApplyFilter);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Sendkeys(SearchText,assetid);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(applybutton);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click("//a[contains(@name,'AssetDetails_D_GetListOfAssetsByEntity') and text()='"+assetid+"']");
	}
	
public void updateNewContact(String updatedtitle) throws Exception {
		
		SeleniumFunc.xpath_GenericMethod_Click(update_contact);
		switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(update_personal_info);
		SeleniumFunc.xpath_GenericMethod_Clear(update_job_title_text);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(update_job_title_text, updatedtitle);
		SeleniumFunc.xpath_GenericMethod_Click(update_submit);
		SeleniumFunc.xpath_GenericMethod_Click(submit);
		switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(close_button);
	}

public void enabledisableContact() throws Exception {
	
	SeleniumFunc.xpath_GenericMethod_Click(update_contact);
	SeleniumFunc.waitFor(2);	
	switchToDefaultContent();
	SeleniumFunc.xpath_GenericMethod_Click(submit);
//	try{
//		SeleniumFunc.executeScript("arguments[0].click()",driver.findElement(By.xpath(submit)));
//	}catch(Exception e) {
//		
//	}
	switchToDefaultContent();
	SeleniumFunc.xpath_GenericMethod_Click(close_button);
}
	
}
