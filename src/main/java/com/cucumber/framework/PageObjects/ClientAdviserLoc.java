package com.cucumber.framework.PageObjects;

import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;

public interface ClientAdviserLoc {
	String closeall_anchor = "//a[@aria-label='Currently open']";
	String closeall = "//td[@title='Close All']";
	String create_new_advisor_button = "//a[@data-test-id='2014100609491604293426']";
	String interaction_button = "//span[text()='Interaction']";
	String new_application_button = "//span[text()='New application']";
	String make_a_claim_button = "//span[text()='Make a claim']";
	String new_Org_orgname="$PCustomerSearch$pSearchStringBusinessUnitName";
	String new_org_searchbutton="//button[@title='Search for Customer']";
	String new_org_create_new="//button[text()='Create New']";
	String new_org_clentSegment="$PNewBUPage$pCategory";
	String new_org_clientType="$PNewBUPage$pCustomerType";
	String new_org_government_dep="$PNewBUPage$pGovernmentDepartment";
	String new_org_gov_fun_dep="$PNewBUPage$pGovernmentFundingDepartment";
	String new_org_profession="$PNewBUPage$pProfession";
	String new_org_phonenumeber="$PNewBUPage$pMobileNumber";
	String new_org_search_address="$PNewAddressPage$pAddressSelected";
	String new_org_create="//button[@title='Create']";
	String new_org_Yes = "//label[@for='834ddcadtrue']";
	String new_org_No = "//label[@for='834ddcadfalse']";

	String actual_vgrmf_flag = "(//span[text()='VGRMF']/following::div/span)[1]";

	String searchOrg_update="//a[contains(@name,'ActionsOrUpdateClientAdviser_D_BusinessUnitListWithSubsidiary')]";
	String updateorg_governChange_no="//label[@for='e12a0e73false']";
	String updateorg_governChange_yes="//label[@for='e12a0e73true']";
	String updateorg_isOrgMerged_no="//label[@for='10471a30No']";
	String updateorg_isOrgMerged_merge="//label[@for='10471a30Merge']";
	String updateorg_SplitOrgList="//input[@data-target='$PpyWorkPage$pCustomer$pCustomerInfo$pOrganisationSplitList']";
	String updateorg_MergedOrg="//input[@name='$PpyWorkPage$pCustomer$pCustomerInfo$pMergedToOrg']";
	String updateorg_isOrgMerged_split="//label[@for='10471a30Split']";
	String updateOrg_effectivedate="//input[@id='a73d5adf']";
	String updateOrg_govern_change_comments="$PpyWorkPage$pComments";
    String updateOrg_UnicorprateName="//div[@pl_prop='pyWorkPage.Customer.CustomerInfo.UnincorporatedOrgList']/div/table//tr[2]/td[2]/span";

    String updateOrg_unincorprateorg_add="//button[text()='Add']";
	String updateOrg_unincorprateorg_name="$PUpdateOrgEntityPg$pEntityName";
	String updateOrg_unincorprateorg_active="//label[@for='d5db0d45Active']";
	String updateOrg_unincorprateorg_inactive="//label[@for='d5db0d45Inactive']";
	String updateOrg_unincorprateorg_donebutton="//button[@title='Done']";

	String updateOrg_submit="//button[@title='Click here to submit']";

	String searchContact_ContactId="//input[@id='e78add2f']";
	String searchContact_ContactIdName = "$PCustomerSearch$pSearchStringContactID";
	String searchContact="CustomerSearch_CustomerSearch_10";
	String searchContact_update="//a[text()='Update'][1]";
	String updateContact_addorg="//a[text()='Add organisation']";
	String updateContact_Orgname="(//input[contains(@name,'$PpyWorkPage$pBusinessContacts$') and contains(@name,'pBusinessUnitName')])";
	String updateContact_orgType="(//label[text()='Account type']/following-sibling::div/div/input[@role='listbox'])";
	String Update_contact_submit="//button[text()='Submit']";
   	String Policy_bundle_package="//a[contains(@name,'ReviewPackageCases_pyWorkPage.SelectedClientPackages')]";
   	String policy_bundle_package_attachmenttab="(//h2[text()='Attachments'])[2]";
   	String policy_bundle_attachment="(//a[@title='Your new insurance with VMIA' and @class='Case_tools'])[2]";

	String create_policyBundle_clientSegment="$PpyWorkPage$pClientSegment";
	String create_policyBundle_Orgname="$PpyWorkPage$pClientName";
	String create_policyBundle_bundleType="$PpyWorkPage$pBundleType";
	String bundleType="$PpyWorkPage$pBundleType";
	String create_policyBundle_find="//button[text()='Find']";
	String create_policyBunle_orgname="//label[text()='Csoes']";
	String create_continueButton="//button[text()='Continue']";
    String cancellation_select_org="//input[contains(@name,'PpyWorkPage$pApplicableClients')  and @type='radio']";
    String cancellation_bundle_SelectContact="//input[contains(@name,'$PD_LoadContactsbyBusinessUnit')  and @type='radio']";
	String cancellation_bundle_productselection="//input[contains(@name,'$PpyWorkPage$pProductList')  and @type='radio']";
	String cso_bundle_productselection="//span[contains(text(),'service')]/parent::div/parent::td/parent::tr//input[@type='radio']";

	String BundlePolicy_AddIncorpratedBtn="//a[text()='Add']/i";
	String Bundlepolicy_AddIncorprateAddnew="//button[text()='Add new']";
	String Bundlepolicy_UnIncorprateOrg="$PUnIncorporatedPage$pOtherInsured$l1$pEntityName";
	String BudlePolicy_UnincorprateOrg_Donebtn="//button[text()='  Done ']";

	String effectivedatePicker="//img[@class='inactvIcon']";

    String create_policyBundle_UnincorprateOrg="//button[text()='Add unincorporated services']";
	String create_Add_UnincorprateOrg="//button[text()='Add']";
	String create_UnincorprateOrg_Name="//input[contains(@name,'PUnIncorporatedPage') and contains(@name,'pEntityName')]";
	String create_UnincorprateOrg_DoneButton="//button[text()='  Done ']";
	String create_Unicorprate_effictivedate="//input[contains(@name,'ClientDetails$pEffectiveDate')]";
	String getCreate_policyBundle_Unincorprateextorg="//table[@pl_prop_class='VMIA-Data-OrgLink-Entity']/tbody/tr[2]/td//span";

	String new_app_button = "//span[text()='New application']";
	String create_new_organisation_button = "//span[text()='Manage organisation']";
	String update_policy_button = "//span[text()='Update a policy']";
	String create_policy_bundle_button = "//span[text()='Create policy bundle']";
	String find_policy_bundle_button = "//button[@data-test-id='202007312135050431101']";
	String select_all_clients_policy_bundle = "//input[@data-test-id='202008051755510355229']";
	String client_name_bundle_textbox = "//input[@data-test-id='20200813231300053936']";
	String policy_bundle_status = "//div[text()='Thank you for initiating package policies']/preceding::header//span[@data-test-id='2016083016191602341167946']/parent::div[not(@style)]/span";
	String search_by_org_textbox = "//input[@data-test-id='2015052601162806102832']";
	String search_button = "//button[@data-test-id='20150526011933006028737']";
	String search_row = "//tr[@data-test-id='201705190611150979789-R1']";
	String select_row_button = "//button[@data-test-id='2015021306370007097332']";
	String Org_Name="//span[@data-test-id='2016030908231804984648']";
	// String Contact_Name="//span[@data-test-id='20160526015929037325722']";
	String Contact_Name="//tr[@data-test-id='201808270550120343899-R1']";
			//"//span[@data-test-id=\"20160526015929037325722\"]";
	
	
	String Asset_orgname="$PpyWorkPage$pSearchCustomerInfo$pCustomerInfo$pBusinessUnit";
	String Asset_org_findbutton="//button[text()='Find']";
	String Asset_org_search_result_radio="//table[@pl_prop_class='VMIA-Data-Party-Org-Company']//input[@type='radio']";
	String Asset_contact_assoc_No="//label[@for=\"5e404969false\"]";
	String Asset_contact_assoc_yes="//label[@for=\"5e404969true\"]";
	String Asset_AddLocationbtn="SelectLocations_pyWorkPage_11";
	String Asset_Cannot_FindyouLocation="//a[@title='Cannot find your address']";
	String Asset_postalCode="//input[@name='$PpyWorkPage$pAddressInfo$pZipCode']";
	String Asset_CrestaZone="//span[text()='Cresta zone']/following-sibling::div/span";
	String Asset_Searchaddress="$PpyWorkPage$pAddressInfo$pAddressSelected";
	String Asset_Addlocation_submit="//button[text()='  Submit ']";
	String Asset_AddAsset="//button[text()='Add Asset']";
	String Asset_AddPropertytype="//input[@data-test-id='202105071735030942422']";
	String Asset_BulkUpload_button="(//button[contains(@name,'BulkAssetDocList')])[1]";
	String Asset_Select_file="//div[@id='uniqueIDforMultiFilePath']//table";
	String Asset_attach_fin_btn="//button[@title='Attach']";
	String Asset_PricentDetails_Expand="//h3[text()='Precinct details']";
	String Asset_Pricent_id="$PpyWorkPage$pAssetList$l1$pAssetPrecinct$pPrecinctID";
	String Asset_pricent_name="//input[@data-test-id='202108051858130152467']";


	String Geo_Hamburger="//a[@id='appview-nav-toggle-one']";
	String Geo_businessrules_edit="//div[text()='Edit']";
	String Geo_businessrules_add="//a[text()='+ Add record']";
	String Geo_businessrules_crestaid="//input[ contains(@name,'$PpyVirtualRecordEditorREResultPage') and contains(@name,'pCrestaID')]";
	String Geo_businessrules_postalCode="//input[ contains(@name,'$PpyVirtualRecordEditorREResultPage') and contains(@name,'pPostalCode')]";
	String Geo_businessrules_lowcrestaid="//input[ contains(@name,'$PpyVirtualRecordEditorREResultPage') and contains(@name,'pLowResCrestaID')]";
	String Geo_businessrules_highzonename="//input[ contains(@name,'$PpyVirtualRecordEditorREResultPage') and contains(@name,'pHighResZoneName')]";
	String Geo_businessrules_lowzonename="//input[ contains(@name,'$PpyVirtualRecordEditorREResultPage') and contains(@name,'pLowResZoneName')]";

	String SearchAsset_Assetid="$PpyDisplayHarness$pAssetID";
	String SearchAsset_Assetsearh="//button[text()='Search']";
	String SearchAsset_Actions="//table[@summary='Asset List']//tr/td[13]";
	String SearchAsset_History="//span[@class='menu-item-title-wrap']/span[text()='History']";

	String Asset_Assetname="//input[@data-test-id='2016072109335505834280']";
	String Asset_AddDetails="//a[text()='Add details']";
	String Asset_expand_assetvaluation="//h3[text()='Asset valuation']";
	String Asset_valution_year="//select[contains(@name,'$PpyWorkPage$pAssetList') and contains(@name,'pAssetValuation$pValuationYear')]";
	String Asset_valution_date="//input[contains(@name,'$PpyWorkPage$pAssetList') and contains(@name,'pValuationDate')]";
	String Asset_Confirm_detail_Accurate_No="//label[@for='75123adffalse']";
	String Asset_Confirm_detail_Accurate_yes="//label[@for='75123adftrue']";
	String Asset_expand_assetID="//table[@pl_prop='.AssetList']//tr[2]/td[@data-attribute-name='Asset ID']/div/span";
	String Asset_final_submit="//button[text()='Submit']";
	String Asset_Search="//button[text()='Search']";
	String Asset_caseid="//span[@class='vmia_case_header_id']";
	String CaseStatus="(//span[@class=\"badge_text\"])[3]";
	String AssetCaseID="//span[@class='vmia_case_header_id']";
	String policyBundle_caseStatus="(//span[@data-test-id='2016083016191602341167946'])[2]";
	String Asset_Link_policy="//select[contains(@name,'PolicyID')]";
	
	String Asset_searchBy="$PpyWorkPage$pAssetSearchType";
	String Asset_AssetypeVal="$PpyWorkPage$pSearchAssetVal";
	String Asset_AssetAddressVal="$PpyWorkPage$pAddressSelected";
	String AssetAsset_Search="//button[text()='Search']";
	String Asset_SearchResult_AssetName="//table[@pl_prop='TempAssetList.pxResults']/tbody/tr/td[2]";
	String Asset_SearchResult_SelectedAssetName="//input[@name='PAssetList1colWidthGBR']/parent::td/div/table//tr//td[1]";
	String Asset_SelectedAssets="//div[@aria-label='Selected assets']";
	String Asset_Update_detail_primdetail="//h3[text()='Primary details']";
	String Asset_Client_AssetId="//input[contains(@name,'ClientAssetID')]";
	String Asset_related_assetid="//input[@data-test-id=\"202105061654150153534\"]";
	String Asset_update_comments="//textarea[@name='$PpyWorkPage$pInitiatorComments']";
	String contact_row = "//tr[@data-test-id='201705041406430959676-R1']";
	String add_task_button = "//button[@data-test-id='2014111401004903823658']";
	String general_service_enquiry_button = "//a[@data-test-id='2014123005242607302524']";
	String add_task_popup_button = "//button[@data-test-id='20150527044600067319555']";
	String enquiry_area = "//select[@name='$PpyWorkPage$pEnquiryArea']";
	String enquiry_category = "//select[@name='$PpyWorkPage$pCollectInformationCategory']";
	String urgency_no = "//input[@name='$PpyWorkPage$pGetUrgency']/following-sibling::label[text()='No']";
	String resolved_in_call_yes = "//input[@name='$PpyWorkPage$pResolvedInCall']/following-sibling::label[text()='Yes']";
	String resolved_in_call_no = "//input[@name='$PpyWorkPage$pResolvedInCall']/following-sibling::label[text()='No']";
	String adv_description = "//textarea[@name='$PpyWorkPage$pGeneralRequestDescription']";
	String thankyou_message_general_service_case = "//div[@data-test-id='2015031106000209843748' and not(@style)]";
	String general_enquiry_case_status = "//span[@data-test-id='201602220830460492170260']";
	String general_enquiry_case_confirm = "//button[@data-test-id='20150216042226087912495']";
	String wrapup = "//button[@data-test-id='2018080902461007071328']";
	String wrapup_confirm_popup = "//button[@data-test-id='20180305072357039640364']";
	String logoff_icon = "//i[@data-test-id='20151105053638078965592']";
	String logoff = "//span[text()='Logout']";
	String preferred_response_channel = "//select[@name='$PpyWorkPage$pPreferedChannelResponse']";
	String Email_enq= "//input[@id='3da2024a']";
	String my_workbaskets_tab = "//h3[text()='My workbaskets']";
	String zipcode = "//input[@data-test-id='20141219073811007312693']";
	String Text_box="//input[@data-test-id='20180528094158075422997']";
	String find_button = "//button[@data-test-id='201604240253100057139194']";
	String select_business_checkbox = "//input[@data-test-id='202004102041080557870']";
	String select_contact_checkbox = "//input[@data-test-id='202005252038490828115']";
	String continue_button="//button[@data-test-id='20161017110917023176385']";
	String new_organisation_name = "//input[@data-test-id='20180528083705005312']";
	String search_button_client = "//button[@data-test-id='20141229012540069819653']";
	String create_new_customer = "//button[@data-test-id='2014123100554709717933']";
	String organisation_name = "//input[@data-test-id='2014123100525802207641' and @id='37edb1ee']";
//	String govt_dept = "//select[@data-test-id='2014123100525802207641']";
	String govt_dept ="//select[@name='$PNewBUPage$pGovernmentDepartment']";
	String phone_number = "//input[@data-test-id='2014123100525802207641' and @id='53d00d04']";
	String Click = "//a[@name='EditAddressDetails_NewAddressPage_7']";
	String client_segment = "//select[@data-test-id='202004030244440886773']";
	String org_category = "//input[@data-test-id='202008062113310601983']";
	String org_type = "//select[@data-test-id='2014123100525802239652']";
	String estimated_revenue = "//input[@data-test-id='20141231005258022814255']";
	String bundle_revenue = "//input[@data-test-id='202008031555270444288']";
	String bundle_revenue_1 = "//input[@name='$PpyWorkPage$pSelectedClientPackages$l1$pClientDetails$pClientRevenue']";
	String bundle_revenue_2 = "//input[@name='$PpyWorkPage$pSelectedClientPackages$l2$pClientDetails$pClientRevenue']";
	String email_org = "//input[@data-test-id='2015010806535605609281']";
	String add_line1_org = "//input[@name='$PNewAddressPage$pAddressLine1']";
	String add_line2_org = "//input[@name='$PNewAddressPage$pAddressLine2']";
	String suburb = "//input[@name='$PNewAddressPage$pcity']";
	String state = "//select[@name='$PNewAddressPage$pState']";
	String zip_code = "//input[@name='$PNewAddressPage$pZipCode']";
	String create_org_button = "//button[@title='Create']";

	
	String paying_org_text = "//input[@id='85ebec7b']";
	String paying_org_list_select = "//li[@data-rowid='0']/span";
	String date_time_of_loss="//img[@data-test-id='202008031557480170722-DatePicker']";
	String datepicker_1 = "(//img[@data-test-id='202008031557480170722-DatePicker'])[1]";
	String datepicker_2 = "(//img[@data-test-id='202008031557480170722-DatePicker'])[2]";
	String today_link="//a[@id='todayLink']";	
	String finish_button = "//button[@data-test-id='20161017110917023277518']";
	
	String bun = "(//span[@class='checkbox'])[2]";
	String bundle_select_client_1 = "//input[@type='checkbox' and @name='$PpyWorkPage$pApplicableClients$l2$ppySelected']";
	String bun1 = "(//span[@class='checkbox'])[3]";
	String bundle_select_client_2 = "//input[@type='checkbox' and @name='$PpyWorkPage$pApplicableClients$l3$ppySelected']";
	String package_case = "(//a[@data-test-id='202008042003180696779'])[1]";
	String begincase = "(//button[@data-test-id='201609091025020567152987'])[last()]";
	String submit = "//button[@data-test-id='2014121801251706289770']";
	String policy_bundle_details = "//h2[text()='Policy bundle details']";
	
	String bundle_GPA_Link = "(//a[contains(text(),'GPA') and @data-test-id='202008042003180696779'])[last()]";
	String bundle_CBR_Link = "(//a[contains(text(),'CBR') and @data-test-id='202008042003180696779'])[last()]";
	String bundle_DAO_Link = "(//a[contains(text(),'DAO') and @data-test-id='202008042003180696779'])[last()]";
	String bundle_PRO_Link = "(//a[contains(text(),'PRO') and @data-test-id='202008042003180696779'])[last()]";
	String bundle_CBL_Link = "(//a[contains(text(),'CBL') and @data-test-id='202008042003180696779'])[last()]";
	
	String org_name_adviser = "//input[@data-test-id='20180528094158075422997']";
	String policy_number_1= "//div[text()='Policy number']";
	String point_1= "//span[@class='highlight-ele  pointerStyle ASC']";
	String policytobeselected_1="//input[@type='radio']";
	String Finish_1 = "//button[text()='Finish']";
	String My_Work="//span[text()='My Work']";
	String My_WorkBusket_clv= "//h3[text()='My workbaskets']";
	String My_WorkBusket_clv_Dropdown= "//select[@data-test-id='2015021706395105541298']";
	String UW_Filter= "//table[@pl_prop_class='Assign-WorkBasket']//div[text()='Case number']/parent::div/parent::div/span/a[@title='Click to filter']";
	String UW_Search_Text_Box= "//input[@data-test-id='201411181100280377101613']";
	String txtWorkbasketSearchBox = "//span[@class='search_box']/input";
	String imgWorkbasketSearchBoxSearchIcon = "//span[@class='search_box']/../following-sibling::*//img";
	String Filter_Button_Apply= "//button[text()='Apply']";
	String UW_case_Link= "//a[@data-test-id='201501071438430852311277']";
	String Approve_clAdv_UW = "//button[@data-test-id='20200430115906070685']";
	String Approve_clAdv_UV2 = "//div[@data-context='pyWorkPage.ApplicationPage']//button";
	String Approve_clAdv_UW_Submit="//Button[text()='Submit']";
	String Exit_ClAdv="//button[@title='Exit case']";
	String begin_button= "//button[@data-test-id='201609091025020567152987']";
	String Zip_code_Triage= "//input[@name='$PpyWorkPage$pSearchCustomerInfo$pCustomerInfo$pAddressInfo$pZipCode']";
	String Text_Box_Triage= "//input[@name='$PpyWorkPage$pSearchCustomerInfo$pCustomerInfo$pBusinessUnit']";
	String Find_Button_Triage= "//button[text()='Find']";
	String Business_checkBox_Traige="//input[@type='radio']";
	String Continue_Triage="//button[text()='Continue']";
	String select_contact_checkbox_Triage="//input[@type='radio']";

	String All_product_Segment_tab = "//div[@aria-label='All products in segment']";
	String Reapply_Yes = "//label[text()='Yes']";
	String Reapply_No = "//label[text()='No']";

	String lnkManagecontact= "//span[text()='Manage contact']";
	String txtFristName="//input[@id='8b3da897']";
	String txtlastName="//input[@id='59c08e25']";
	String btnSearch="//body/div[@id='PEGA_HARNESS']/form[1]/div[3]/div[2]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/button[1]/div[1]/div[1]/div[1]/div[1]";
	
	String lnkOrgnization="//body/div[@id='PEGA_HARNESS']/form[1]/div[3]/div[1]/section[1]/div[1]/span[2]/div[1]/div[1]/div[1]/div[1]/span[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[2]/div[1]/span[1]/div[1]/div[2]/div[1]/span[1]/a[1]";
	
	String txtOrganizationName="//input[@id='54ccfd41']";
	String txtCommunicationpreferences="//input[@id='6fba1078']";
	String txtAccountType="//input[@id='a2cb1516']";
	
	//Update Organization
	String imgOrgUpdate="//a[@data-test-id='202009141335240330381']";
	String drpNameChangereson="//label[text()='Name change reason']//following::select[@id='745b6e9a']";
	String updateOrganisation_name ="//input[@id='2b8eabf2']";
			// "//label[text()='Organisation name']//following::input[1]";
	String ClientSubmit="//button[text()='Submit']";
	String mesgsuccessful="//div[contains(text(),'Your change of organisation name has been successf')]";
	String txtOrgSerach="//input[@id='24dbd519']";
	String btnSerach="//button[@title='Search for an item']/i";
	String drpOrg="//select[@data-test-id='201610190747440120129777']";
	String lnkOrganizations="//div[1]/h2[text()='Organisations']";
	String btnOrgthreedot="//tbody/tr[@id='$PpySearchResults$ppxResults$l1']/td[6]/div[1]/span[1]/button[1]/i";
	String optnStartSearch="//span[text()='Start research']";
	String lnkOrganizationDetails="//h2[@id='headerlabel9175']";
	String txtupdatedOrgname="//span[text()='Organisation name']//following::div[1]/span[@data-test-id='2014123100525802207641']";
	String primaryContact="//input[@id='dd0a425c']";
	String SecondryConact="//input[@id='8345b497']";
	String Click_On_Transfer="//span[contains(text(),'ClaimsOfficer1R3')]";
	String imgBellNotification="//div[1]/span[1]/a[1][@data-test-id='2016082902530604545164' and @title='Notifications']/i[1]";
	String lnkNotification="//h2[text()='Notifications']//following::div[@class='field-item'][1]";
	
	String txtApprovalNote="//textarea[@id='59c09632']";
	String btnAprrove="//button[text()='Approve']";
	String txtPrivousName = "//div[contains(text(),'Previous name')]//following::span[@data-test-id='2016072109335505834280']";
	String txtStatus="//div[@class='cellIn '] [text()='Status']//following::div/span[@data-test-id='20141231005547096567728' and text()='Active']";
	String chkMarkInactive="//label[text()='Mark organisation as inactive']//following::input[@id='e4be8854']";
    String chkAllowclient="//label[text()='Mark organisation as inactive']//following::input[@id='1e746ece']";
    

	String tabMyCases = "//div[@title='My Cases' and @role='tab']/h3[text()='My cases']";
	String imgCaseNumberFilter = "//div[text()='Case number']/parent::div/following-sibling::span/a[@title='Click to filter']";
	String txtPopupFilterTextbox = "//input[contains(@name,'ppySearchText') and contains(@name,'ppyColumnFilterCriteria')]";


	String newButton = "//i[@class='pi pi-plus']";
	String endorsementManagement = "//span[text()='Endorsement management']";
	String editEndorsementText = "//span[text()='Edit endorsement text']";
	String deactivateEndorsement = "//span[text()='Deactivate Endorsement']";
	String bulkAddEndorsementText="//span[text()='Bulk add endorsement text']";
	String current_date="//a[@id='todayLink']";
	String productIDDdName = "$PpyWorkPage$pProductID";
	String clientCategoryImg = "//*[@name='selected-placeholder']/following-sibling::i";
	String bulkTextSearch = "//*[@title='Search for Policies']//div[text()='Search']";
	String firstCheckbox = "#gridLayoutTable tbody[id='gridTableBody'] tr:nth-child(2) td:nth-child(2) input:nth-child(2)";
	String continueBtnName = "pyCaseActionAreaButtons_pyWorkPage_19";


	String policyCheckBox = "//input[@type='radio']";
	String searchPolicyTf = "$PpyWorkPage$pPolicyID";
	String continueBtn = "//button[text()='Continue']";
	String endorsementFrameTextfield = ".cke_wysiwyg_frame.cke_reset";
	String endorsementTextfield = "//body[@aria-label='This is a rich text editor control.']/p";
	String commentsTextfield = "//textarea[@name='$PpyWorkPage$pInitiatorComments']";
	String reviewerTextfield = "//input[@name='$PpyWorkPage$pReviewer']";
	String sumitBtn = "//button[@data-test-id='20161017110917023277518']";
	String endorseText = "(//span[@class='vmia_case_header_id'])";
	String rejectComments = "//input[@name='$PpyWorkPage$pComments']";
	String submitBtn = "//button[text()='Submit']";
	String UW_Casenumber_TH = "(//div[@class='gridHeaderLabel '] [text()='Case ID'])[1]";
	String filterOption = "(//table[@id='gridLayoutTable']/tbody[@id='gridTableBody']/tr/th[3]//div/a[@title='Open Menu'])[1]";
	String uw_applyfilter = "//span[text()='Apply filter']";
	String UW_search_text = "//div[@pyclassname=\"Embed-FilterColumn\"]//span[@data-control-mode='input']/input";
	String UW_search_applyfilter = "//button[@data-test-id='201604060130370006117741']";
	String uw_clickcase = "//table[@id='gridLayoutTable']/tbody[@id='gridTableBody']/tr[2]//a";

	String cancellationOption = "//span[text()='Cancellation']";
	String cancelReasonDP = "//*[@name='$PpyWorkPage$pApplicationPage$pPolicyCancellationReason']";
	String checkQuote = "//*[@aria-describedby='$PpyWorkPage$pApplicationPage$pTermsAcceptanceError ']";

	//Portfolio Manager
	String manageComponents="//span[text()='Manage components']";
	String vmiaSolicitor="//div[@aria-label='VMIA Solicitor']/h3";
	String addVmiaSolicitor="DisplayLegalPanels_pyDisplayHarness_6";
	String s_name="$PLegalPanelPg$pAttorneyName";
	String onPanelBtn="//label[text()='On-panel']";
	String claimTypeImg="//*[@name='selected-placeholder']/following-sibling::i";
	String claimTypeOptions = "//div[@id='msresults-list']//li/span[@class='ms-primary-option'][text()='Aircraft Insurance']";
	String claimTypeDD = "//input[@data-target='$PLegalPanelPg$pClaimTypeList']";
	String s_address="$PLegalPanelPg$pAddressInfo$pAddressSelected";
	String f_address="//table[@pl_prop='D_AddressValSearch.pxResults']//tr[2]/td";
	String solAddList = "//a[@name='CaptureLegalSolicitorList_LegalPanelPg_8']";
	String solName = "$PLegalPanelPg$pLegalPanelSolicitorList$l1$pName";
	String solEmail="$PLegalPanelPg$pLegalPanelSolicitorList$l1$pEmail";
	String solNumber="$PLegalPanelPg$pLegalPanelSolicitorList$l1$pMobileNumber";
	String solPersonalDD="$PLegalPanelPg$pLegalPanelSolicitorList$l1$pPersonnelCategory";
	String solServiceImg="//div[@id='msContainere888b55c']/i";
	String serviceOption = "//span[text()='Insurance']";
	String datereview = "//img[@alt='Choose from calendar']";
	String todayLink= "//a[@id='todayLink']";
	String submitSol = "//button[@id='ModalButtonSubmit']";
	String nameFilterIcon = "//th[@aria-label='Name']/div/div/a";
	String searchTextField = "//label[text()='Search text']/following-sibling::div//input";
	String applyBtn = "//button[text()='Apply']";
	String actionsLink = "//a[text()='Actions']";
	String editSolicitor = "//*[text()='Edit VMIA Solicitor']";
	String inactiveStatus="//*[@name='$PpyDisplayHarness$pStatus']/following-sibling::label[text()='Inactive']";
	String onPanel="//td[@data-attribute-name='On-panel']/span";

//	Search function for Historical Policy
	String myWork = "//li[@title='My Work']";
	String searchLeftTab = "//li[@title='My Reports']/following-sibling::li[1]//span[text()='Search']";
	String historicalPolicy = "//label[contains(text(),'Historical Policy')]";
	String warMsg = "//span[contains(text(),'Please provide')]";
	String policyTypeDd = "//select[@name='$PpyDisplayHarness$pSearchStringPolicyType']";
	String policyYearTf = "//input[@name='$PpyDisplayHarness$pSearchStringPolicyYear']";
	String searchBtnName = "ArchivePolicySearchWrapper_pyDisplayHarness_7";
	String listOfRecords = "//table[@prim_page='pyDisplayHarness']//tr";
}
