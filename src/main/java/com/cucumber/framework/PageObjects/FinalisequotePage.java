package com.cucumber.framework.PageObjects;

import com.cucumber.framework.GeneralHelperSel.CSVFileReader;
import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import com.cucumber.framework.TestBase.TestBase;
import com.cucumber.framework.context.TestData;
import com.opencsv.CSVReader;
import org.junit.Assert;
import org.openqa.selenium.By;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import org.testng.asserts.SoftAssert;

public class FinalisequotePage {

    WebDriver driver;
    TestData testData;
    UW uw;
    public FinalisequotePage(WebDriver driver, TestData data) {
        this.driver=driver;
        testData=data;
        uw=new UW(driver,testData);
    }
    By limitsofLibilityPID=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pAggregateLimitOfLiability");
    By NOL_LimitsOfLiability_Section1=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommCoverages$l1$pLimits$l1$pValue");
    By NOL_LimitsOfLiability_Section2=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommCoverages$l2$pLimits$l1$pValue");
    By masterPolicyNumber=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pMasterPolicyNo");
    By limitsOfInditmity=By.name("$PpyWorkPage$pApplicationPage$pLimitOfIndemnity");
    By addItemButton_Excess=By.xpath("(//button[contains(@name,'AdditionalDeductiblesInnerFor')])[1]");
    By constructionPeriod=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pConstructionPeriod");
    By updateDefectsLibilityPeriod=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pDefectsLiabilityPeriod");
    By performancetestingPeriod=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPerformanceTestingPeriod");
    By FrameTextArea=By.xpath("//body[@class='cke_editable cke_editable_themed cke_contents_ltr cke_show_borders']/p");
    By Submit_UW_Dialogue=By.xpath("//button[text()='  Submit ']");
    By PolicyOverride=By.xpath("//input[@name='$PpyWorkPage$pApplicationPage$pIsPolicyDocOverride' and @type='checkbox']");
    By PolicyScheduleReplacementUpload=By.xpath("(//input[@name='appendUniqueIdToFileName'])[1]/parent::div/div/input");
    By COCReplacementUpload=By.xpath("(//input[@name='appendUniqueIdToFileName'])[2]/parent::div/div/input");
    By claimManagerReference=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pClaimsManagerReference");
    By ExcessName=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommRisks$l1$pAdditionalDeductible$l1$pName");
    By ExcessAmount=By.xpath("(//table[@pl_prop='.AdditionalDeductible'])[1]//td[2]//input[@name='$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommRisks$l1$pAdditionalDeductible$l1$pComponentValue']");
    By ExcessAddMaterialDamage=By.name("AdditionalDeductiblesInnerForMYCON_pyWorkPage.ApplicationPage.Quotes(1).PolicyData.CommRisks(1)_9");
    By AddtionalSubLimits_AddItem=By.name("AdditionalSubLimitsInnerForCONMY_pyWorkPage.ApplicationPage.Quotes(1).PolicyData.CommCoverages(1)_8");
    By AddtionalSubLimits_AddItem_Name=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommCoverages$l1$pAdditionalSubLimits$l1$pName");
    By AddtionalSubLimits_AddItem_Amount=By.xpath("(//table[@pl_prop='.AdditionalSubLimits'])[1]//td[2]//input");
    By LimitsOfLiability_AddItem=By.name("AdditionalLimitsOfLiability_pyWorkPage.ApplicationPage.Quotes(1).PolicyData.CommRisks(1)_8");
    By LimitsOfLiability_AddItem_Name=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommRisks$l1$pAdditionalLimitsOfLiability$l1$pName");
    By LimitsOfLiability_AddItem_Amount=By.xpath("(//table[@pl_prop='.AdditionalLimitsOfLiability'])[1]//td[2]//input");
    By Cancel_basePremium=By.name("$PTmpPage$pApplicationPage$pQuotes$l1$pPolicyData$pPremiumData$pBasePremium");
    By UWNotes=By.xpath("//span[@id='CTRL_TA']/textarea[@id]");
    By createQuoateButton=By.xpath("//button[text()='Create quote']");
//    By btn_approve_quote = By.xpath("//button[text()='Approve Quote']"); //Its commented because they frequently changing button text(q/Q in quote)
    By btn_approve_quote = By.xpath("//button[contains(text(),'Approve')]");
    By InsuredPerson_AddNewButton=By.name("//button[text()='Add new']/i");
    By btn_AddItem=By.xpath("//button[text()='Add item']");
    By addOneDeductible=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommCoverages$l1$pAdditionalDeductible$l1$pName");
    By add_AddtionalDeductible=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommCoverages$l5$pAdditionalDeductible$l1$pName");
    By AnnualBasePremium=By.name("$PTmpPage$pApplicationPage$pQuotes$l1$pPolicyData$pPremiumData$pAnnualPremium");
    By enable_prorate=By.xpath("//input[@name='$PTmpPage$pApplicationPage$pProRataEnabled' and @type='checkbox']");
    By Submitbtn=By.xpath("//button[text()='Submit']");
    By BenifitName=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommRisks$l1$pCommCoverages$l1$pRiskName");
    By Addbutton=By.xpath("(//button[text()='Add'])[1]/i");
    By EndorsementTextLink=By.xpath("//a[text()='Add endorsement text']");
    By isThisShorEndo_Yes=By.xpath("//label[@for='2e56453atrue']");
    By isThisShorEndo_No=By.xpath("//label[@for='2e56453afalse']");
    By endorsementEndDate=By.name("$PpyWorkPage$pApplicationPage$pEndorsementEndDate");
    By EndorsementerrorMessage=By.xpath("//div[@id='$PpyWorkPage$pApplicationPage$pEndorsementEndDateError']");
    By GstValue=By.name("$PTmpPage$pApplicationPage$pQuotes$l1$pPolicyData$pPremiumData$pSurchargeValue$gGST");
    By StamDutyValue=By.name("$PTmpPage$pApplicationPage$pQuotes$l1$pPolicyData$pPremiumData$pSurchargeValue$gStampDuty");
    By RetroactiveDate=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pRetroactiveDate");
    By ExcessName_PID=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommCoverages$l1$pAdditionalDeductible$l1$pName");
    By SelfInsuredRentenationName=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommCoverages$l1$pAdditionalDeductible$l1$pName");
    By MI_between_45000And75000=By.xpath("//label[@for='47313c49Between $40,000 and $75,000']");
    By MI_75000more=By.xpath("//label[@for='47313c49$75,000 or more']");
    By MI_Lessthan45000=By.xpath("//label[@for='47313c49Less than $40,000']");
    By MI_isGpRegiester_Yes=By.xpath("//label[@for='60291dc8Yes']");
    By MI_isGpRegiester_No=By.xpath("//label[@for='60291dc8No']");
    By MI_SctionApply_sction1a=By.xpath("//label[@for='482aa09dSection 1a']");
    By MI_SctionApply_sction1b=By.xpath("//label[@for='482aa09dSection 1b']");
    By MI_RetroactiveCover_Yes=By.xpath("//label[@for='78434d44Yes']");
    By MI_RetroactiveCover_No=By.xpath("//label[@for='78434d44No']");
    By MI_Prorata_Yes=By.xpath("//label[@for='eadeccf6true']");
    By MI_Prorata_No=By.xpath("//label[@for='eadeccf6false']");
    By UpdateRiskInfo=By.name("PremiumDetails_pyWorkPage.ApplicationPage_33");
    By UWnotesMI=By.xpath("//textarea[contains(@name,'$PD_CaptureNotes')]");
    By SubLimits1=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommRisks$l1$pCommCoverages$l1$pRiskName");
    By SubLimits2=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommRisks$l2$pCommCoverages$l1$pRiskName");
    By SubLimits3=By.name("$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommRisks$l3$pCommCoverages$l1$pRiskName");
    //
//    By addNewLink = By.xpath("//h2[@id='headerlabel7175']/following-sibling::span//button/i");
    By addNewLink = By.xpath("//*[text()='Add new']/parent::span");
    By activeButton = By.xpath("//label[text()='Active']");
    By employeeTextField = By.name("$PTempInsuredPerson$pEmployeeName");
    By spouseTextField = By.name("$PTempInsuredPerson$pPartnerName");
    By dependentTextField = By.name("$PTempInsuredPerson$pDependents");
    By countryTextField = By.name("$PTempInsuredPerson$pPostingCountry");
    By exclusionsTextField = By.name("$PTempInsuredPerson$pInsuredPersonExclusions");
    By calenderIcon = By.xpath("//span[@id='$PTempInsuredPerson$pStartDateSpan']/img");
    By todaysDate = By.cssSelector(".calcell.today.selected");
    By submitButton = By.xpath("//button[@id='ModalButtonSubmit']");
    By addadditionalBtn = By.xpath("//button[text()='Add additional benefits']/i");
    By eachInsuredAddBtn = By.xpath("(//button[text()='Add item'])[1]");
    By benifitTextField = By.xpath("(//input[@name='$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pCommRisks$l1$pAdditionalSubLimits$l1$pName'])[1]");
    By BenifitDropdown = By.xpath("//span[@class='match-highlight']");
    By AggregateLimitUpdateBtn = By.xpath("//div/h2[text()='Aggregate Limit of Liability']/following-sibling::span//button[text()='Update']/i");
//    By AggregateLimitTextArea = By.xpath("//body[@aria-label='This is a rich text editor control.']");
    By AggregateLimitTextArea= By.xpath("//body/p[contains(text(),'Period of Insurance')]");
    By AggregateTextArea = By.cssSelector(".cke_editable.cke_editable_themed.cke_contents_ltr.cke_show_borders");

    By standardPolicyEndBtn = By.xpath("//div[@title='Disclose Standard policy endorsements']");
    By practiceCompanyStaff = By.xpath("//input[@name='$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pEndorsementsForms$l1$pValueToDisplay']/following-sibling::label[text()='Not Endorsed']");
    By professionalLocums = By.xpath("//input[@name='$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pEndorsementsForms$l3$pValueToDisplay']/following-sibling::label[text()='Not Endorsed']");
    By crossBorderWork = By.xpath("//input[@name='$PpyWorkPage$pApplicationPage$pQuotes$l1$pPolicyData$pEndorsementsForms$l4$pValueToDisplay']/following-sibling::label[text()='Not Endorsed']");
//    By addEndorsemnetText = By.xpath("//a[text()='Add endorsement text']");
//    By addEndorsementFrame = By.cssSelector(".cke_wysiwyg_frame.cke_reset");
//    By endorsementTextfield = By.xpath("//body[@aria-label='This is a rich text editor control.']/p");
//    By endorsementNoBtn = By.xpath("//input/following-sibling::label[text()='No']");
//    By endorsementTitle = By.xpath("//label[text()='Exclude']/parent::span/parent::nobr/parent::span/parent::div/h2");
//    By excludeCheckbox = By.xpath("//label[text()='Exclude']/parent::span/input[@title='Exclude from Policy Schedule print for this case']");
//    By modelsubmitBtn = By.xpath("//button[@id='ModalButtonSubmit']");


    public void enterValueInFinaliseQuoateTextbox(String element,By by,String value) throws Exception {
        clickUpdateButtonOfFinalisQuoate(element);
        SeleniumFunc.waitInMilliSeconds(5);
        SeleniumFunc.enterValue(by,value);
        SeleniumFunc.waitInMilliSeconds(5);
        SeleniumFunc.click(Submit_UW_Dialogue);
    }

    public String capturePolicyInfoInFinaliseQuoteScreen(String info) throws Exception {
        SeleniumFunc.waitInMilliSeconds(3);
        return SeleniumFunc.getElement(By.xpath("//span[text()='"+info+"']/parent::div/div/span")).getText();
    }
    public void clickUpdateButtonOfFinalisQuoate(String option) throws Exception {
        SeleniumFunc.click(By.xpath("(//h2[text()='"+option+"']/parent::div/span//button/i)[1]"));
    }

    public void enterTheValueInFrameTextArea(String option,String value) throws Exception {
        clickUpdateButtonOfFinalisQuoate(option);
        SeleniumFunc.waitFor(1);
        TestBase.getDriver().switchTo().frame(SeleniumFunc.getElement(By.xpath("//iframe[@class='cke_wysiwyg_frame cke_reset']")));
        SeleniumFunc.enterValue(FrameTextArea, value);
        SeleniumFunc.clickElement(Submit_UW_Dialogue);
        SeleniumFunc.waitFor(1);
    }

    public void enterSubLimtsLiabilityIMI() throws Exception {
        SeleniumFunc.waitInMilliSeconds(2);
        clickUpdateButtonOfFinalisQuoate("Sub-Limit(s) of Liability");
        SeleniumFunc.waitInMilliSeconds(2);
        SeleniumFunc.click(By.xpath("(//button[text()='Add'])[1]"));
        SeleniumFunc.selectLinkDropDownOption(SubLimits1,"Crisis Expenses");
        SeleniumFunc.waitInMilliSeconds(2);
        SeleniumFunc.click(By.xpath("(//button[text()='Add'])[2]"));
        SeleniumFunc.selectLinkDropDownOption(SubLimits2,"Crisis Expenses");
        SeleniumFunc.waitInMilliSeconds(2);
        SeleniumFunc.click(By.xpath("(//button[text()='Add'])[3]"));
        SeleniumFunc.selectLinkDropDownOption(SubLimits3,"Crisis Expenses");
        clickSubmitInDialougeBox();
    }

    public void enterLimitsOfIndemnity(String value) throws Exception {
        enterValueInFinaliseQuoateTextbox("Limit of Indemnity",limitsOfInditmity,value);
    }

    public void enterMasterPolicyNumber(String value) throws Exception {
        enterValueInFinaliseQuoateTextbox("Master policy number",masterPolicyNumber,value);
    }

    public void clickPolicyOverride() throws Exception {
        SeleniumFunc.clickElement(PolicyOverride);
        SeleniumFunc.waitFor(2);
    }

    public void enterExcess(String excessName,String excesAmount) throws Exception {
        clickUpdateButtonOfFinalisQuoate("Excess");
        SeleniumFunc.waitFor(1);
        SeleniumFunc.clickElement(ExcessAddMaterialDamage);
        SeleniumFunc.waitForElementToClikable(ExcessName,10);
        SeleniumFunc.selectLinkDropDownOption(ExcessName,excessName);
        SeleniumFunc.enterValue(ExcessAmount,excesAmount);
        SeleniumFunc.clickElement(Submit_UW_Dialogue);
    }

    public void enterExcessForChildIdminityProduct(String excessName) throws Exception {
        clickUpdateButtonOfFinalisQuoate("Excess");
        SeleniumFunc.waitForElementToClikable(addItemButton_Excess,10);
        SeleniumFunc.clickElement(addItemButton_Excess);
        SeleniumFunc.waitForElementToClikable(ExcessName,10);
        SeleniumFunc.selectLinkDropDownOption(ExcessName,excessName);
        SeleniumFunc.clickElement(Submit_UW_Dialogue);
    }

    public void enterLimitsOfLiablityforProfessionalIndeminity(String value) throws Exception {
        SeleniumFunc.waitInMilliSeconds(3);
        enterValueInFinaliseQuoateTextbox("Limit(s) of Liability",limitsofLibilityPID,value);
    }

    public void clickUpdateRiskInformation() throws Exception {
        SeleniumFunc.waitInMilliSeconds(2);
        try{
            SeleniumFunc.clickElement(UpdateRiskInfo);
        }catch(Exception e){
            SeleniumFunc.clickElementUsingActions(UpdateRiskInfo);
        }

    }

    public void SelectRevenue(String Revenue) throws Exception {
        SeleniumFunc.waitFor(2);
        if(Revenue.equalsIgnoreCase("75,000 more")){
            SeleniumFunc.clickElement(MI_75000more);
        }
        else if(Revenue.equalsIgnoreCase("Between 40,000 and 75,000")){
            SeleniumFunc.clickElement(MI_between_45000And75000);
        }
        else if(Revenue.equalsIgnoreCase("Less than $40,000")){
            SeleniumFunc.clickElement(MI_Lessthan45000);
        }
    }

    public void selecRetroactivecover(String option) throws Exception {
        SeleniumFunc.waitFor(2);
        if(option.equalsIgnoreCase("Yes"))
           SeleniumFunc.clickElement(MI_RetroactiveCover_Yes);
        else
            SeleniumFunc.clickElement(MI_RetroactiveCover_No);
    }

    public void selecPolicytoApply(String option) throws Exception {
        SeleniumFunc.waitFor(2);
        if(option.equalsIgnoreCase("Section1a"))
            SeleniumFunc.clickElement(MI_SctionApply_sction1a);
        else if(option.equalsIgnoreCase("Section1b"))
            SeleniumFunc.clickElement(MI_SctionApply_sction1b);
    }

    public void selecProrata(String option) throws Exception {
        SeleniumFunc.waitFor(2);
        if(option.equalsIgnoreCase("Yes"))
            SeleniumFunc.clickElement(MI_Prorata_Yes);
        else
            SeleniumFunc.clickElement(MI_Prorata_No);
    }

    public void selecGPRegiester(String option) throws Exception {
        SeleniumFunc.waitFor(2);
        if(option.equalsIgnoreCase("Yes"))
            SeleniumFunc.clickElement(MI_isGpRegiester_Yes);
        else
            SeleniumFunc.clickElement(MI_isGpRegiester_No);
    }

    public void enterRiskInfoForMedicalIndiminityRuralGP(String Revenue,String isGPRegister,String Aopplypolicy,String Retroactivecover,String prorata) throws Exception {
        clickUpdateRiskInformation();
        SeleniumFunc.waitFor(2);
        SelectRevenue(Revenue);
//        SeleniumFunc.waitInMilliSeconds(10);
        SeleniumFunc.waitFor(2);
        selecGPRegiester(isGPRegister);
//        SeleniumFunc.waitInMilliSeconds(10);
        SeleniumFunc.waitFor(2);
        selecProrata(prorata);
//        SeleniumFunc.waitInMilliSeconds(10);
//        selecPolicytoApply(Aopplypolicy);
//        SeleniumFunc.waitInMilliSeconds(10);
        SeleniumFunc.waitFor(2);
        selecRetroactivecover(Retroactivecover);//comment
//        SeleniumFunc.enterValue(UWNotes,"test");
        SeleniumFunc.waitFor(2);
        SeleniumFunc.enterValue(UWnotesMI,"test");
        SeleniumFunc.waitFor(2);
        clickSubmitInDialougeBox();
        SeleniumFunc.waitFor(6);
    }



    public void enterAddtionalSubLimits(String sublimitName,String sublimitAmount) throws Exception {
        clickUpdateButtonOfFinalisQuoate("Sub limits of liability");
        SeleniumFunc.waitFor(1);
        SeleniumFunc.clickElement(AddtionalSubLimits_AddItem);
        SeleniumFunc.waitForElementToClikable(AddtionalSubLimits_AddItem_Name,10);
        SeleniumFunc.selectLinkDropDownOption(AddtionalSubLimits_AddItem_Name,sublimitName);
        SeleniumFunc.enterValue(AddtionalSubLimits_AddItem_Amount,sublimitAmount);
        SeleniumFunc.clickElement(Submit_UW_Dialogue);
    }

    public void enterLimitsOfLiability(String sublimitName,String sublimitAmount) throws Exception {
        SeleniumFunc.waitFor(2);
        clickUpdateButtonOfFinalisQuoate("Limits of liability");
        SeleniumFunc.waitFor(1);
        SeleniumFunc.clickElement(LimitsOfLiability_AddItem);
        SeleniumFunc.waitFor(1);
//        SeleniumFunc.waitForElementToClikable(AddtionalSubLimits_AddItem_Name,10);
        SeleniumFunc.selectLinkDropDownOption(LimitsOfLiability_AddItem_Name,sublimitName);
        SeleniumFunc.enterValue(LimitsOfLiability_AddItem_Amount,sublimitAmount);
        SeleniumFunc.clickElement(Submit_UW_Dialogue);
    }


    public void enterLimitsLiabilityForConstructionMetroTunnel(String LimitsOfLiablity) throws Exception {
        enterTheValueInFrameTextArea("Limits of Liability",LimitsOfLiablity);
    }

    public void UploadOverridePolicyScheduleDocument(String fiename) throws Exception {
        SeleniumFunc.clickElementUsingActions(PolicyScheduleReplacementUpload);
        SeleniumFunc.UploadDocument(fiename);
    }

    public void UploadOverrideCOCDocument(String fiename) throws Exception {
        SeleniumFunc.clickElementUsingActions(COCReplacementUpload);
        SeleniumFunc.waitFor(3);
        SeleniumFunc.UploadDocument(fiename);
//        SeleniumFunc.enterValue(COCReplacementUpload,filePath);
    }

    public void enterInsuredOperations(String value) throws Exception {
        enterTheValueInFrameTextArea("Insured Operations",value);
    }

    public void enterNameInsured(String value) throws Exception {
        enterTheValueInFrameTextArea("Named Insured",value);
    }

    public void enterInsuredProject(String value) throws Exception {
        SeleniumFunc.waitInMilliSeconds(9);
        enterTheValueInFrameTextArea("Insured Project",value);
    }

    public void enterBusiness(String value) throws Exception {
        enterTheValueInFrameTextArea("Business",value);
    }

    public void enterAddtionalInsured(String value) throws Exception {
        enterTheValueInFrameTextArea("Additional Insureds",value);
    }

    public void enterConstructionPeriod(String value) throws Exception {
        SeleniumFunc.waitInMilliSeconds(9);
        enterValueInFinaliseQuoateTextbox("Construction period",constructionPeriod,value);
    }

    public void enterProject(String value) throws Exception {
        enterTheValueInFrameTextArea("Project",value);
    }

    public void enterDefectLiabilityPeriod(String value) throws Exception {
        enterValueInFinaliseQuoateTextbox("Defects Liability period",updateDefectsLibilityPeriod,value);
    }

    public void enterPerformanceTestingPeriod(String value) throws Exception {
        enterValueInFinaliseQuoateTextbox("Performance testing period",performancetestingPeriod,value);
    }

    public void enterClaimsManagerReference(String value) throws Exception {
        enterValueInFinaliseQuoateTextbox("Claims manager reference",claimManagerReference,value);
    }

    public void enterTerritorialLimits(String value) throws Exception {
        enterTheValueInFrameTextArea("Territorial Limits",value);
    }

    public void enterExcludedContracts(String value) throws Exception {
        enterTheValueInFrameTextArea("Excluded contracts",value);
    }

    public void enterSpecifiedInclusions(String value) throws Exception {
        enterTheValueInFrameTextArea("Additional Insureds",value);
    }


    public void enterTerrorismExtension(String value) throws Exception {
        enterTheValueInFrameTextArea("Terrorism Extension",value);
    }

    public void enterBasisOfSettlement(String value) throws Exception {
        enterTheValueInFrameTextArea("Basis of Settlement",value);
    }

    public void enterNomineeForVMIANotices(String value) throws Exception {
        enterTheValueInFrameTextArea("Nominee for VMIA Notices",value);
    }

    public void enterLossAdjusters(String value) throws Exception {
        enterTheValueInFrameTextArea("Loss adjusters",value);
    }

    public void enterLossPayee(String value) throws Exception {
        enterTheValueInFrameTextArea("Loss Payee",value);
    }

    public void enterAdditionalInformation(String value) throws Exception {
        enterTheValueInFrameTextArea("Additional information",value);
    }

    public void enterInsuredInfo(String value) throws Exception {
        SeleniumFunc.waitInMilliSeconds(5);
        enterTheValueInFrameTextArea("Insured",value);
    }

    public void enterNamedPilots(String value) throws Exception {
        SeleniumFunc.waitInMilliSeconds(5);
        enterTheValueInFrameTextArea("Named Pilots",value);
    }

    public void enterAircraftPilots(String value) throws Exception {
        SeleniumFunc.waitInMilliSeconds(5);
        enterTheValueInFrameTextArea("Description of Aircraft",value);
    }

    public void enterExtensions(String value) throws Exception {
        SeleniumFunc.waitInMilliSeconds(5);
        enterTheValueInFrameTextArea("Extensions",value);
    }

    public void enterLimitLiabilitySection1and2(String value) throws Exception {
        SeleniumFunc.waitInMilliSeconds(5);
        clickUpdateButtonOfFinalisQuoate("Limit of Liability");
        SeleniumFunc.waitFor(1);
        TestBase.getDriver().switchTo().frame(SeleniumFunc.getElement(By.xpath("(//iframe[@class='cke_wysiwyg_frame cke_reset'])[1]")));
        SeleniumFunc.enterValue(FrameTextArea, value);
        SeleniumFunc.switchToDefaultContent();
        TestBase.getDriver().switchTo().frame(SeleniumFunc.getElement(By.xpath("(//iframe[@class='cke_wysiwyg_frame cke_reset'])[2]")));
        SeleniumFunc.enterValue(FrameTextArea, value);
        SeleniumFunc.clickElement(Submit_UW_Dialogue);
        SeleniumFunc.waitFor(1);
    }

    public void VerifyAllConstructionMultiyearElementexist() throws Exception {
        String[] finaliseQuaoteEle=new String[]{"Additional information","Loss Payee","Master policy number","Business","Additional Insureds","Project","Class of Insurance","Construction period","Defects Liability period","Performance testing period","Limits of liability","Sub limits of liability","Territorial Limits","Excluded contracts","Specified inclusions","Terrorism Extension","Basis of Settlement","Nominee for VMIA Notices","Loss adjusters","Insured Operations","Excess","Claims manager reference","Policy document override","Additional information","Supplementary Documents"};
        SoftAssert softAssert = new SoftAssert();
        for(String ele:finaliseQuaoteEle)
            softAssert.assertTrue(SeleniumFunc.getElements(By.xpath("//h2[text()='" + ele + "']")).size() > 0);
        softAssert.assertAll();
    }

    public void VerifyAllElementexistInFinaliseQuoateScreen(String product) throws Exception {
//        String[] finaliseQuaoteEle=new String[]{"Master policy number","Named Insured","Class of Insurance","Construction period","Defects Liability period","Performance testing period","Limits of liability","Sub limits of liability","Terrorism Extension","Excess","Claims manager reference","Information"};
        CSVFileReader dataReader=new CSVFileReader();
        System.out.println("Product is :"+product);
        if(dataReader.readCSV(product).size()>0) {
            System.out.println(dataReader.readCSV(product));
            testData.getDataMap().putAll(dataReader.readCSV(product));
            String[] finaliseQuaoteEle = testData.getData("OptionsInFinaliseQuoate").split(";");
            SoftAssert softAssert = new SoftAssert();
            for (String ele : finaliseQuaoteEle){
                SeleniumFunc.waitInMilliSeconds(5);
                softAssert.assertTrue(SeleniumFunc.getElements(By.xpath("//h2[text()='" + ele + "']")).size() > 0);
            }

            softAssert.assertAll();
            System.out.println("Opetions displayed In Finalise Quoate Screen :"+testData.getData("OptionsInFinaliseQuoate"));
        }
    }

    public void enterCompletedOperationsPeriod(String value) throws Exception {
        enterTheValueInFrameTextArea("Completed Operations Period",value);
    }

    public void enterMandatoryIMI() throws Exception {
        SeleniumFunc.waitFor(1);
        enterSubLimtsLiabilityIMI();
        enterValueDeductilbleInIMI();
        clickSubmit();
    }


    public void enterMandatoryInformationContractorPollutionLiability() throws Exception {
//            VerifyAllElementexistInFinaliseQuoateScreen(testData.getData("Product"));
            enterInsuredOperations("Testing");
            enterTheValueInFrameTextArea("Territorial Limit","test");
            enterCompletedOperationsPeriod("Testing");
            SeleniumFunc.clickElement(By.xpath("//button[text()='Submit']"));

    }


    public void enterMandatoryInformationConstructionMultiyearPolicy() throws Exception {
//            VerifyAllConstructionMultiyearElementexist();
        enterNameInsured("Testing");
        enterConstructionPeriod("12");
        enterLimitsOfLiability("Liability","5000");
        enterTerrorismExtension("Testing1234");
        enterExcess("Liability","5000");
        enterAddtionalSubLimits("Offsite Storage","5000");
        SeleniumFunc.clickElement(By.xpath("//button[text()='Submit']"));
    }

    public void enterRetroactiveDateInPIDFinlaiseQuoate() throws Exception {
        SeleniumFunc.waitInMilliSeconds(9);
        enterValueInFinaliseQuoateTextbox("Retroactive date",RetroactiveDate,"600");
    }

    public void enterExcessForProfessionlIndeminity() throws Exception {
        SeleniumFunc.waitInMilliSeconds(9);
        clickUpdateButtonOfFinalisQuoate("Excess");
        SeleniumFunc.clickElement(btn_AddItem);
        SeleniumFunc.selectLinkDropDownOption(ExcessName_PID,"Test");
        clickSubmitInDialougeBox();
    }

    public void enterMandatoryInformationForProfessionalIndeminityAlliance() throws Exception {
        enterLimitsOfLiablityforProfessionalIndeminity("500");
        enterRetroactiveDateInPIDFinlaiseQuoate();
        enterConstructionPeriod("12");
        enterInsuredProject("Test");
        enterExcessForProfessionlIndeminity();
        clickApproveQuoate();
        clickSubmit();
    }

    public void enterMandatoryInformationAircraftInsurance() throws Exception {
        enterInsuredInfo("Test");
        enterNamedPilots("John Cena");
        enterAircraftPilots("Virat");
        enterLimitLiabilitySection1and2("Test");
        enterExtensions("test");
        enterValueDeductilbleInAVI();
        clickSubmit();
    }

    public void enterMandatoryInformationMedicaIndeminity() throws Exception {
        enterMemberNumber();
        admissionRights();
        retroactiveDate();
        addAtleastOneDeductible();
        standardPolicyEndorsements();

    }

    public void enterMandatoryInformationExpatriateMedicalExpenses() throws Exception {
//        SeleniumFunc.waitFor(2);
//        clickUpdateButtonOfFinalisQuoate("Aggregate Limit of Liability");
        SeleniumFunc.waitFor(4);
        SeleniumFunc.clickElement(addNewLink);
        SeleniumFunc.clickElement(activeButton);
        SeleniumFunc.clickElement(calenderIcon);
        SeleniumFunc.waitFor(2);
        SeleniumFunc.clickElement(todaysDate);
        SeleniumFunc.enterValue(employeeTextField,"johny");
        SeleniumFunc.enterValue(spouseTextField,"carlee");
        SeleniumFunc.enterValue(dependentTextField,"3");
        SeleniumFunc.enterValue(countryTextField,"australia");
        SeleniumFunc.enterValue(exclusionsTextField,"NA");
        SeleniumFunc.clickElement(submitButton);
        SeleniumFunc.waitFor(3);
        SeleniumFunc.clickElement(addadditionalBtn);
        SeleniumFunc.clickElement(eachInsuredAddBtn);
        SeleniumFunc.enterValue(benifitTextField ,"test");
        SeleniumFunc.waitFor(2);
        driver.findElement(benifitTextField).sendKeys(Keys.ARROW_DOWN);
        SeleniumFunc.clickElement(BenifitDropdown);
        SeleniumFunc.clickElement(submitButton);

        SeleniumFunc.waitFor(3);
        SeleniumFunc.clickElement(AggregateLimitUpdateBtn);
        SeleniumFunc.waitFor(2);
//        SeleniumFunc.clearTextBox(AggregateLimitTextArea);
        TestBase.getDriver().switchTo().frame(SeleniumFunc.getElement(By.cssSelector(".cke_wysiwyg_frame.cke_reset")));
        SeleniumFunc.clearTextUsingActionsAndSendValues(AggregateLimitTextArea);
//        driver.findElement(AggregateLimitTextArea).sendKeys(Keys.CONTROL+"A");
//        SeleniumFunc.xpath_GenericMethod_Clear("//body[@aria-label='This is a rich text editor control.']");
        SeleniumFunc.enterValue(AggregateTextArea,"$123 in respect of all claims in any one Period of Insurance");
        SeleniumFunc.clickElement(submitButton);
        SeleniumFunc.waitFor(2);
        clickSubmit();
    }

    public void enterSelfInsuredRententionAndSubmit() throws Exception {
        SeleniumFunc.waitFor(3);
        try{
            //Insurance/BPP/PPL_GRIPEVlotuion feature
            clickUpdateButtonOfFinalisQuoate("Self Insured Retention");
        }catch(Exception e){
            //Insurance/BasicProductPolicies/PropertyGrip
            clickUpdateButtonOfFinalisQuoate("Self insured retentions");
        }
        SeleniumFunc.waitFor(1);
        SeleniumFunc.clickElement(btn_AddItem);
        SeleniumFunc.waitFor(1);
//        SeleniumFunc.selectLinkDropDownOption(SelfInsuredRentenationName,"Test 1");
//        Above step got commented and replaced with below step because the value provided has no data
        SeleniumFunc.selectLinkDropDownOption(SelfInsuredRentenationName,"Test");
        clickSubmitInDialougeBox();
        enterTheValueInFrameTextArea("Business","Test");
        clickSubmit();

    }

    public void enterSelfInsuredRententionAndSubmitforPPLGRIP() throws Exception {
        SeleniumFunc.waitFor(2);
        clickUpdateButtonOfFinalisQuoate("Self Insured Retention");
        SeleniumFunc.waitFor(1);
        SeleniumFunc.clickElement(btn_AddItem);
        SeleniumFunc.waitFor(1);
        SeleniumFunc.selectLinkDropDownOption(SelfInsuredRentenationName,"Test 1");
        clickSubmitInDialougeBox();
    }

    public void clickSubmitInDialougeBox() throws Exception {
        SeleniumFunc.clickElement(Submit_UW_Dialogue);
    }

    public void clickApproveQuoate() throws Exception {
        SeleniumFunc.waitFor(2);
        SeleniumFunc.clickElement(btn_approve_quote);
    }

    public void enterUWNoteAndApproveQuate(String UWNote,String isProrataEnabled) throws Exception {
        SeleniumFunc.waitInMilliSeconds(15);
        SeleniumFunc.clickElement(UWNotes);
        SeleniumFunc.waitFor(3);
        SeleniumFunc.enterValue(UWNotes,UWNote);
        clickSubmitInDialougeBox();
//        validatePremiumStampDutyAndGST(isProrataEnabled);
        SeleniumFunc.waitFor(2);
        clickApproveQuoate();
    }

    public void clickCreateQuate() throws Exception {
        SeleniumFunc.clickElement(createQuoateButton);
    }

    public void clickSubmit() throws Exception {
        SeleniumFunc.waitFor(3);
        SeleniumFunc.clickElement(Submitbtn);
    }

    public void enterPremiumForCancellation(String premium,String notes) throws Exception {
        clickCreateQuate();
        SeleniumFunc.waitFor(2);
        SeleniumFunc.clearTextBox(Cancel_basePremium);
        SeleniumFunc.enterValue(Cancel_basePremium,premium);
        enterUWNoteAndApproveQuate(notes,"");
    }

    public void validatePremiumStampDutyAndGST(String isProRataEnabled) throws Exception {
            if(testData.getPolicyAction().equalsIgnoreCase("New Business"))
                getPolicyDurationForNewBusiness();
            else if(testData.getPolicyAction().equalsIgnoreCase("Endorsment"))
                getPolicyDurationForEndorsement();
            getPolicyDurationForNewBusiness();
                uw.validatePremium("NewBusness",testData.getData("Premium"),isProRataEnabled );
    }

    public void enterBusinessAndSubmitQuoate() throws Exception {
        SeleniumFunc.waitInMilliSeconds(5);
        enterTheValueInFrameTextArea("Business","test");
        clickSubmit();
    }

    public void enterTextForDeductibletype(String deductibleType, String text) throws Exception{
        SeleniumFunc.waitInMilliSeconds(15);
        SeleniumFunc.getElement(By.xpath("(//span[text()='"+deductibleType+"']/following::input[@type='text'])[1]")).sendKeys(text);
    }

    public void enterDeductiblesForPPLandSubmitQuote() throws Exception{
        SeleniumFunc.waitInMilliSeconds(5);
        clickUpdateButtonOfFinalisQuoate("Deductible");
        enterTextForDeductibletype("Marine Liability", "Test");
        enterTextForDeductibletype("Property damage", "Test");
        enterTextForDeductibletype("Personal Injury/ Other Injury", "Test");
        SeleniumFunc.clickElement(Submit_UW_Dialogue);
        SeleniumFunc.waitFor(2);
        clickSubmit();
    }


    public void enterMandatoryInfoPPLGrip() throws Exception {
        SeleniumFunc.waitInMilliSeconds(5);
        enterTheValueInFrameTextArea("Business","Test");
        SeleniumFunc.waitInMilliSeconds(5);
        enterSelfInsuredRententionAndSubmitforPPLGRIP();
        clickSubmit();
    }

    public void enterPremiumAndNotes(String premium,String notes,String isProrataNeedtoBeEnabled) throws Exception {
//        VerifyAllElementexistInFinaliseQuoateScreen(testData.getData("Product"));
//        VerifyAllElementexistInFinaliseQuoateScreen("Construction Risks Public Liability");
        SeleniumFunc.waitFor(2);
        clickCreateQuate();
        SeleniumFunc.waitFor(1);
        testData.setData("Premium",premium);
        SeleniumFunc.clearTextBox(AnnualBasePremium);
        SeleniumFunc.waitInMilliSeconds(8);
        SeleniumFunc.enterValue(AnnualBasePremium,premium);
        if(isProrataNeedtoBeEnabled.equalsIgnoreCase("Yes"))
            SeleniumFunc.clickElement(enable_prorate);
        else if(isProrataNeedtoBeEnabled.equalsIgnoreCase("Default Yes")){SeleniumFunc.clickElement(enable_prorate);
            SeleniumFunc.waitForElementToClikable(enable_prorate,10);
            SeleniumFunc.waitInMilliSeconds(3);
        SeleniumFunc.clickElement(enable_prorate);
            SeleniumFunc.waitInMilliSeconds(10);
        }
        else SeleniumFunc.waitInMilliSeconds(8);
        enterUWNoteAndApproveQuate(notes,isProrataNeedtoBeEnabled);

    }

    public void addEndorsementText(String updatedText) throws Exception {
        SeleniumFunc.waitFor(1);
        SeleniumFunc.xpath_GenericMethod_Click("//a[text()='Add endorsement text']");
        driver.switchTo().frame(driver.findElement(By.cssSelector(".cke_wysiwyg_frame.cke_reset")));
        SeleniumFunc.xpath_GenericMethod_Click("//body[@aria-label='This is a rich text editor control.']/p");
        SeleniumFunc.xpath_GenericMethod_Sendkeys("//body[@aria-label='This is a rich text editor control.']/p",updatedText);
        SeleniumFunc.xpath_GenericMethod_Click("//input/following-sibling::label[text()='No']");
        SeleniumFunc.xpath_GenericMethod_Click("//button[@id='ModalButtonSubmit']");
    }

    public void addEndorsementTextAndCheckExcludeBtn(String updatedText) throws Exception {
        SeleniumFunc.waitFor(1);
        SeleniumFunc.xpath_GenericMethod_Click("//a[text()='Add endorsement text']");
        driver.switchTo().frame(driver.findElement(By.cssSelector(".cke_wysiwyg_frame.cke_reset")));
        SeleniumFunc.xpath_GenericMethod_Click("//body[@aria-label='This is a rich text editor control.']/p");
        SeleniumFunc.xpath_GenericMethod_Sendkeys("//body[@aria-label='This is a rich text editor control.']/p",updatedText);
        SeleniumFunc.xpath_GenericMethod_Click("//input/following-sibling::label[text()='No']");
//        String text = driver.findElement(By.xpath("//label[text()='Exclude']/parent::span/parent::nobr/parent::span/parent::div/h2")).getText();
//        System.out.println("The endorsement title is******* "+text);
//        SeleniumFunc.clickElementUsingActions(By.xpath("//label[text()='Exclude']/parent::span/input[@title='Exclude from Policy Schedule print for this case']"));
        SeleniumFunc.xpath_GenericMethod_Click("//button[@id='ModalButtonSubmit']");
    }

    public void enterInsuredDetails(String value) throws Exception {
        SeleniumFunc.waitFor(1);
        clickUpdateButtonOfFinalisQuoate("The Insured");
        SeleniumFunc.waitFor(1);
        TestBase.getDriver().switchTo().frame(SeleniumFunc.getElement(By.xpath("//iframe[@class='cke_wysiwyg_frame cke_reset']")));
        SeleniumFunc.enterValue(AggregateLimitTextArea,value);
        SeleniumFunc.waitFor(1);
        SeleniumFunc.click(Submit_UW_Dialogue);
    }

    public void enterAggregateLimitOfLiability(String value) throws Exception {
        SeleniumFunc.waitInMilliSeconds(5);
        enterTheValueInFrameTextArea("Aggregate Limit of Liability",value);
    }

    public void enterScheduleOfBenefits(String EachInsuredPerson) throws Exception {
        clickUpdateButtonOfFinalisQuoate("Schedule of Benefits");
        SeleniumFunc.clickElement(Addbutton);
        SeleniumFunc.waitFor(1);
        SeleniumFunc.selectLinkDropDownOption(BenifitName,EachInsuredPerson);
        clickSubmitInDialougeBox();
    }

    public void AddInsuredpersonExpartialMedicalExpense() throws Exception {
        SeleniumFunc.clickElement(InsuredPerson_AddNewButton);
        SeleniumFunc.waitFor(1);

    }
    public void enterMandatoryInfoForExparitalMedicalExpense() throws Exception {
        enterAggregateLimitOfLiability("enterAggregateLimitOfLiability");
        enterScheduleOfBenefits("Test1");

    }

    public void enterPremiumForNewAppplication(String premium,String notes,String isProrataNeedtoBeEnabled) throws Exception {
        enterPremiumAndNotes(premium,notes,isProrataNeedtoBeEnabled);
        clickSubmit();
    }



    public void UploadCOCAndPolicyScheduleDocument(String PolicySchduleDocPath,String CocPath) throws Exception {
        SeleniumFunc.waitFor(3);
        UploadOverrideCOCDocument(CocPath);
        SeleniumFunc.waitFor(5);
        UploadOverridePolicyScheduleDocument(PolicySchduleDocPath);
        SeleniumFunc.waitFor(5);
    }

    public void enterMandatoryInfoForConstructionMertoTunnel(String PolicySchduleDocPath,String CocPath) throws Exception {
        UploadCOCAndPolicyScheduleDocument(PolicySchduleDocPath,CocPath);
        enterLimitsLiabilityForConstructionMetroTunnel("Test");
        clickApproveQuoate();
        clickSubmit();
    }

    public void submitPolicy() throws Exception {
        clickSubmit();
    }

    public void UploadCOCAndPolicyScheduleClickSubmit(String PolicySchduleDocPath,String CocPath) throws Exception {
        UploadCOCAndPolicyScheduleDocument(PolicySchduleDocPath,CocPath);
        clickSubmit();
    }

    public void enterEndorsementTextAndValidate(String endorsementendDate) throws Exception {
        SeleniumFunc.clickElement(EndorsementTextLink);
        SeleniumFunc.waitFor(4);
        SeleniumFunc.clickElement(isThisShorEndo_Yes);
        SeleniumFunc.waitForElementToClikable(endorsementEndDate,10);
        SeleniumFunc.enterValue(endorsementEndDate,endorsementendDate);
        SeleniumFunc.clickElement(Submit_UW_Dialogue);
        Assert.assertEquals("End date must be before policy expiry date",SeleniumFunc.getElement(EndorsementerrorMessage).getText());
        SeleniumFunc.clickElement(isThisShorEndo_No);
        Assert.assertTrue(SeleniumFunc.getElements(endorsementEndDate).size()>0);
        SeleniumFunc.clickElement(Submit_UW_Dialogue);
    }

    public void enterMandatoryInformationForChildIndeminity(String excessname,String limitsofIndimity) throws Exception {
        SeleniumFunc.waitFor(1);
        enterExcessForChildIdminityProduct(excessname);
        enterLimitsOfIndemnity(limitsofIndimity);
        clickSubmit();
    }
    public void getPolicyDurationForNewBusiness() throws Exception {
        SeleniumFunc.waitFor(1);
        testData.setPolicyduration(SeleniumFunc.findTheDateDifference(capturePolicyInfoInFinaliseQuoteScreen("Policy start date"),capturePolicyInfoInFinaliseQuoteScreen("Policy end date")));
    }

    public void getPolicyDurationForCancellation() throws Exception {
        SeleniumFunc.waitFor(1);
        testData.setPolicyduration(SeleniumFunc.findTheDateDifference(capturePolicyInfoInFinaliseQuoteScreen("Cancellation effective date"),capturePolicyInfoInFinaliseQuoteScreen("Period to")));
    }

    public void getPolicyDurationForEndorsement() throws Exception {
        SeleniumFunc.waitFor(1);
        testData.setPolicyduration(SeleniumFunc.findTheDateDifference(capturePolicyInfoInFinaliseQuoteScreen("Endorsement effective date"),capturePolicyInfoInFinaliseQuoteScreen("Period to")));
    }

    public void validatePremiumforCancellation() throws Exception {
        getPolicyDurationForCancellation();
        String basePremimum=capturePolicyInfoInFinaliseQuoteScreen("Current annual premium");
        System.out.print(basePremimum);
        UW uw=new UW(driver,testData);
        uw.validatePremium("Cancellation",SeleniumFunc.removeCommaInString(basePremimum.substring(1,basePremimum.indexOf("."))),"Yes");
    }

    public void enterLimitsOfLibilityforNOL() throws Exception {

        clickUpdateButtonOfFinalisQuoate("Limit of Liability");
        SeleniumFunc.waitFor(1);
        SeleniumFunc.enterValue(NOL_LimitsOfLiability_Section1,"3000");
        SeleniumFunc.enterValue(NOL_LimitsOfLiability_Section2,"6000");
        SeleniumFunc.clickElement(Submit_UW_Dialogue);

    }



    public void enterValueDeductilbleInMIPI() throws Exception {
        SeleniumFunc.waitFor(2);
        clickUpdateButtonOfFinalisQuoate("Deductible(s)");
        SeleniumFunc.waitFor(1);
        SeleniumFunc.clickElement(btn_AddItem);
        SeleniumFunc.waitFor(1);
        SeleniumFunc.selectLinkDropDownOption(add_AddtionalDeductible,"Any once occurance");
        clickSubmitInDialougeBox();
    }

    public void enterValueDeductilbleInIMI() throws Exception {
        SeleniumFunc.waitFor(2);
        clickUpdateButtonOfFinalisQuoate("Deductibles");
        SeleniumFunc.waitFor(1);
        SeleniumFunc.clickElement(btn_AddItem);
        SeleniumFunc.selectLinkDropDownOption(ExcessName_PID,"TEST");
        clickSubmitInDialougeBox();
    }

    public void enterValueDeductilbleInAVI() throws Exception {
        SeleniumFunc.waitFor(2);
        clickUpdateButtonOfFinalisQuoate("Deductible");
        SeleniumFunc.waitFor(1);
        SeleniumFunc.clickElement(btn_AddItem);
        SeleniumFunc.selectLinkDropDownOption(ExcessName_PID,"Test");
        clickSubmitInDialougeBox();
    }

    public void enterMandatoryInfoForMIPIProduct() throws Exception {
        enterTheValueInFrameTextArea("Business","Test");
        enterLimitsOfIndemnity("200");
        enterValueDeductilbleInMIPI();
        clickSubmit();

    }

    public void enterMemberNumber() throws Exception {
        SeleniumFunc.waitFor(1);
        SeleniumFunc.xpath_GenericMethod_Click("//h2[text()='Member Number']/parent::div//nobr/span");
        driver.switchTo().frame(driver.findElement(By.cssSelector(".cke_wysiwyg_frame.cke_reset")));
        SeleniumFunc.xpath_GenericMethod_Click("//body[@aria-label='This is a rich text editor control.']/p");
        SeleniumFunc.xpath_GenericMethod_Sendkeys("//body[@aria-label='This is a rich text editor control.']/p","test");
        SeleniumFunc.click(Submit_UW_Dialogue);
    }

    public void admissionRights() throws Exception {
        SeleniumFunc.waitFor(1);
        SeleniumFunc.xpath_GenericMethod_Click("//h2[text()='Admission Rights']/parent::div//nobr/span");
        driver.switchTo().frame(driver.findElement(By.cssSelector(".cke_wysiwyg_frame.cke_reset")));
        SeleniumFunc.xpath_GenericMethod_Click("//body[@aria-label='This is a rich text editor control.']/p");
        SeleniumFunc.xpath_GenericMethod_Sendkeys("//body[@aria-label='This is a rich text editor control.']/p","test");
        SeleniumFunc.click(Submit_UW_Dialogue);
    }

    public void retroactiveDate() throws Exception {
        SeleniumFunc.waitFor(1);
        SeleniumFunc.xpath_GenericMethod_Click("//h2[text()='Retroactive Date']/parent::div//nobr/span");
        driver.switchTo().frame(driver.findElement(By.cssSelector(".cke_wysiwyg_frame.cke_reset")));
        SeleniumFunc.xpath_GenericMethod_Click("//body[@aria-label='This is a rich text editor control.']/p");
        SeleniumFunc.xpath_GenericMethod_Sendkeys("//body[@aria-label='This is a rich text editor control.']/p","test");
        SeleniumFunc.click(Submit_UW_Dialogue);
    }

    public void addAtleastOneDeductible() throws Exception {
        SeleniumFunc.waitFor(1);
        SeleniumFunc.xpath_GenericMethod_Click("//h2[text()='Deductible']/parent::div//nobr/span//button[text()='Add Additional Deductible']/i");
        SeleniumFunc.clickElement(btn_AddItem);
        SeleniumFunc.waitFor(1);
        SeleniumFunc.selectLinkDropDownOption(addOneDeductible,"Test");
        clickSubmitInDialougeBox();
    }
    public void standardPolicyEndorsements() throws Exception {
        SeleniumFunc.waitFor(1);
        SeleniumFunc.click(standardPolicyEndBtn);
        SeleniumFunc.waitFor(1);
        SeleniumFunc.click(practiceCompanyStaff);
        SeleniumFunc.click(professionalLocums);
        SeleniumFunc.click(crossBorderWork);
    }

}

