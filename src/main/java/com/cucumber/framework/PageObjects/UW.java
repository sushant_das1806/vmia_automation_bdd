package com.cucumber.framework.PageObjects;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import com.cucumber.framework.context.TestData;
import io.cucumber.java.eo.Se;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import org.testng.Assert;

import com.cucumber.framework.CS.CustomerServ;
import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import com.cucumber.framework.TestBase.TestBase;
//import com.cucumber.framework.helper.Logger.LoggerHelper;
import org.testng.asserts.Assertion;
import org.testng.asserts.SoftAssert;

public class UW extends CustomerServ implements UWLoc {
	//private final Logger log = LoggerHelper.getLogger(UW.class);
	UW uw;
	String ID_no;
	String Endo;
	String EnteredEndoText;
	double annaulPremim,baseprem,gst,basepremwithgst,stampduty,totalpremium,floatgst;
	TestData testData;
	public UW(WebDriver driver, TestData data) {
		super(driver);
		testData=data;
	}

	By AlProduct_RiskAdvisor=By.xpath("//h2[text()='All products in segment']");
	By yes_DublicateProduct=By.xpath("//input[contains(@name,'pIsDuplicateCheck')]/following-sibling::label[text()='Yes']");
	By nameOfTheParty=By.name("$PpyWorkPage$pName");
	By cerifticateEffectiveDate=By.xpath("//input[@name='$PpyWorkPage$pCoCEffectiveDate']");
	By cerifticateFromDate=By.xpath("//input[@name='$PpyWorkPage$pFromDate']");
	By cerifticateToDate=By.xpath("//input[@name='$PpyWorkPage$pToDate']");
	By InititorComments=By.name("$PpyWorkPage$pInitiatorComments");

	public void sendAppObject(UW uw) {
		this.uw = uw;
	}

	public void selectGPA() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(uw_group_personal_accident_button);
	}

	public void clickSubmitUWLossType() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(submit_button_uw_loss_select_screen);
	}

	public void verifyUWCaseformat(String caseformat) throws Exception {
		// SeleniumFunc.xpath_Genericmethod_HandleStaleElement(uw_case_id);
		String fnolcaseid = xpath_Genericmethod_getElementText(uw_case_id1);
		String format = fnolcaseid.substring(4, 7);
		SeleniumFunc.writeToAssertionFile("Expected case format : " + caseformat + " , Actual format : " + format,
				(format.equals(caseformat) ? "PASS" : "FAIL"));
		Assert.assertTrue(format.equals(caseformat));
	}

	public void enterthepolicy(String policyno) throws Exception
	{
	SeleniumFunc.xpath_GenericMethod_Sendkeys(btnpolicyID, policyno);
	}

	public void enterthepurchaseorder(String purchaseorder) throws Exception
	{
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtpurchaseorder, purchaseorder);
	}

	public void clicknew() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(btnnew);
		//SeleniumFunc.xpath_GenericMethod_Click(txtMangproductno);
	}

	public void clickapprovebtn() throws Exception {
		try{
			SeleniumFunc.click(By.xpath(btn_approve_quote));
		}catch(Exception e){
			SeleniumFunc.click(By.xpath(btn_approve_quote2));
		}
	}

	public void approveQuoteAndSubmit() throws Exception {
		try{
			SeleniumFunc.click(By.xpath(btn_approve_quote1));
		}catch(Exception e){
			SeleniumFunc.click(By.xpath(btn_approve_quote2));
		}

		SeleniumFunc.click(By.xpath(AcceptQuate_Submit));
	}

	public void clickonmanagebtn() throws Exception
	{
		SeleniumFunc.xpath_GenericMethod_Click(btn_mangorderno_hum);

	}
	public void clickOnManageBtn() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(txtMangproductno);
	}
	public void clicknewhum() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(btn_New_humberger);

	}

	public void clicksearchButton() throws Exception {

		 SeleniumFunc.xpath_GenericMethod_Click(btnsearch);
	}

	public void clickradbtnn() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(radbtn);

	}
	public void enterthepurchaseorderr(String purchaseorder) throws Exception
	{
		 SeleniumFunc.xpath_GenericMethod_Clear(txtpurchaseorder);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtpurchaseorder, purchaseorder);
	}
	public void humbrger() throws Exception
	{
		SeleniumFunc.xpath_GenericMethod_Click(btnhamburger);

	}

	public void fetchCaseID() throws Exception {
		SeleniumFunc.waitFor(6);

		String fnolcaseid = null;
//		List<WebElement> caseidtexts = SeleniumFunc.getElement(uw_case_id);
//		for(WebElement caseidtext:caseidtexts){
//		       if(caseidtext.getText().length()>0) { fnolcaseid=caseidtext.getText();break;}
//		}
		fnolcaseid=SeleniumFunc.getElement(By.xpath(uw_case_id)).getText();
		System.out.println("fetching the fnolID "+fnolcaseid );
		String caseid = fnolcaseid.substring(3).trim();
		System.out.println("fetching the caseid "+caseid );

		testData.setCaseId(caseid);


	}

	public void verifyUWCaseStatus(String casestatus) throws Exception {

		String casestatusscreen = xpath_Genericmethod_getElementText(uw_case_status);
		System.out.println(casestatusscreen + "************************");
		System.out.println(casestatus + "***********************");
		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + casestatus + " , Actual status : " + casestatusscreen,
				(casestatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatus.equals(casestatusscreen));
	}

	public void verifyClientNumber(String clientnumber) throws Exception {
		String clientnumberfromscreen = xpath_Genericmethod_getElementText(client_number);
		SeleniumFunc.writeToAssertionFile(
				"Expected client number : " + clientnumber + " , Actual client number : " + clientnumberfromscreen,
				(clientnumber.equals(clientnumberfromscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(clientnumber.equals(clientnumberfromscreen));
	}

	public void enterNumberOfStudents(String students) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Clear(number_of_students);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(number_of_students, students);
	}

	public void checkDeclaration() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(checkbox_declaration);
	}

	public void clickContinueButton() throws Exception {
		try {
			 SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		} catch (Exception e) {

		}
	}


//	public void enterEndorsementEffectivedateForMultiYearProduct(String date){
//
//	}


	public void addAttachmentToRnDPRodcut(String filename) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(RndPolicy_AddDocument_icon);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(attach_policy_details_option);
		SeleniumFunc.clickSelectFileButtonAndUploadTheFile(filename);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(Rnd_Attach_button);
		SeleniumFunc.waitFor(20);
	}

	public void clickFinish() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
	}

	public void clickCommentsAndFinish(String comments) throws Exception {
		SeleniumFunc.name_GenericMethod_Sendkeys(Asset_moreinfo,comments);
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
	}

	public void clickExitButton() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(exit_button );
	}

	public void clickExitButtonInClaims() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(exitButton);
	}

	public void enterStartDateOfInsurance(String date) throws Exception {
		try {
			if (date.equalsIgnoreCase("currentdate")) {
				SeleniumFunc.xpath_GenericMethod_Click(insurance_start_date);
				// SeleniumFunc.xpath_GenericMethod_Click(current_date);
				SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(current_date)));
			} else if (date.equalsIgnoreCase("pastdate")) {
				SeleniumFunc.xpath_GenericMethod_Click(past_date);
			} else if (date.equalsIgnoreCase("futuredate")) {
				SeleniumFunc.xpath_GenericMethod_Click(future_date);
			} else {
				// SeleniumFunc.waitFor(2);
				SeleniumFunc.xpath_GenericMethod_Click(insurance_start_date);
				// SeleniumFunc.xpath_GenericMethod_Click(current_date);
				SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(current_date)));
				try {
					SeleniumFunc.executeScript("arguments[0].value='" + date + "';",
							driver.findElement(By.xpath(start_date)));
				} catch (Exception e) {
					SeleniumFunc.executeScript("arguments[0].value='" + date + "';", driver.findElement(
							By.xpath("//input[@name='$PpyWorkPage$pApplicationPage$pChangeEffectiveDate']")));
				}
			}
		} catch (Exception e) {
			Thread.sleep(5000);
		}

	}

	public void enterStartDateInSameFinancialYear() throws Exception {
		String fiscalYearEndDate = SeleniumFunc.getRAPolicyYearDate();
//		SeleniumFunc.xpath_GenericMethod_Click(insurance_start_date);
		SeleniumFunc.SelectDateWithDropDown(insurance_start_date,fiscalYearEndDate);
		System.out.println("The expected date is:"+fiscalYearEndDate);
		// SeleniumFunc.xpath_GenericMethod_Click(current_date);
//		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(fiscalYearEndDate)));

	}

	public void enterStartDateOfInsurance1() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(insurance_start_date);
		// SeleniumFunc.xpath_GenericMethod_Click(current_date);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(current_date)));
		try {
			SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(start_date)));
		} catch (Exception e) {
			SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(current_date)));
		}

	}

	public void verifyUWFinalCaseStatus(String caseStatus) throws Exception {
//		SeleniumFunc.waitForElement(By.xpath(uw_case_status_accept_quote),80);
		SeleniumFunc.waitFor(10);
		String caseStatusScreen = xpath_Genericmethod_getElementText(uw_case_status_accept_quote);
		System.out.println("Expected Status --------- "+caseStatus);
		System.out.println("Actual Status --------- "+caseStatusScreen);
		Assert.assertEquals(caseStatusScreen, caseStatus);
	}

	public void openCaseFromFollowingWorkQueue() throws Exception {
		SeleniumFunc.waitFor(10);
		SeleniumFunc.xpath_GenericMethod_Click(followingWork_queue);
		SeleniumFunc.waitFor(20);
		WebElement case_filter = driver.findElement(By.xpath(Apply_Filter_followingworkQueue));
		Actions Ac_Filter = new Actions(driver);
		Ac_Filter.moveToElement(case_filter).click().perform();
		SeleniumFunc.waitFor(10);
//      testData.
		SeleniumFunc.xpath_GenericMethod_Sendkeys(search_filter_textbox, testData.getCaseId());
		SeleniumFunc.waitFor(3);
		WebElement ApplyButton = driver.findElement(By.xpath(apply_button_filter));
		Ac_Filter.moveToElement(ApplyButton).click().perform();
		SeleniumFunc.waitForElementToClikable(By.xpath("//a[text()='"+testData.getCaseId()+"']"),20);
		SeleniumFunc.xpath_GenericMethod_Click("//a[text()='"+testData.getCaseId()+"']");

	}

	public void clickSearchResultOfRiskAdvisorQueue() throws Exception {
		SeleniumFunc.waitForElementToClikable(By.xpath(first_case_in_ra_fliter),20);
		SeleniumFunc.xpath_GenericMethod_Click(first_case_in_ra_fliter);
		SeleniumFunc.waitFor(2);
	}

	public void verifyDueDateOfCaseInRiskAdvisorQueue(String duedate) throws Exception {
		Assert.assertEquals(SeleniumFunc.getElement(By.xpath("//a[text()='"+testData.getCaseId()+"']/ancestor::td[1]/following-sibling::td[6]/div")).getText(),duedate);
	}

	public void EnterPremiumAndNotes(String premium,String notes) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(btn_create_qoute);
		SeleniumFunc.waitFor(1);
		WebElement ele =  driver.findElement(By.xpath(txt_annual_permium));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].value = '';", ele);
		ele.clear();
		ele.sendKeys(premium);
		SeleniumFunc.xpath_GenericMethod_Click(enable_prorate);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txt_uw_notes, notes);
		SeleniumFunc.xpath_GenericMethod_Click(Submit_UW_Dialogue);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(btn_approve_quote1);
		SeleniumFunc.waitFor(7);
	}


	public void EnterPremiumAndNotesInCancellation(String premium,String notes) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(btn_create_qoute);
		SeleniumFunc.waitFor(1);
		WebElement ele =  driver.findElement(By.xpath(txt_annual_permium));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].value = '';", ele);
		ele.clear();
		ele.sendKeys(premium);
		SeleniumFunc.xpath_GenericMethod_Click(enable_prorate);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txt_uw_notes, notes);
		SeleniumFunc.xpath_GenericMethod_Click(Submit_UW_Dialogue);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(btn_approve_quote1);
		SeleniumFunc.waitFor(7);
	}
	public String getPolicyEffectivedateErrorMessageConstructionpolicy() throws Exception {
		SeleniumFunc.waitForElementToClikable(By.xpath("//span[@class='inputError dynamic-icon-error']"),10);
		return getElement(By.xpath("//span[@class='inputError dynamic-icon-error']")).getText();
	}

	public void enterThePremiumAndSubmitQuoate(String premium,String notes) throws Exception {
		EnterPremiumAndNotes(premium,notes);
		SubmitQuote();
	}

	public void enterTheValueInFrameTextArea(String xpath,String value) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(xpath);
		SeleniumFunc.waitFor(2);
		TestBase.getDriver().switchTo().frame(SeleniumFunc.getElement(By.xpath("//iframe[@class='cke_wysiwyg_frame cke_reset']")));
		SeleniumFunc.xpath_GenericMethod_Sendkeys(FrameTextArea, value);
		SeleniumFunc.xpath_GenericMethod_Click(Submit_UW_Dialogue);
		SeleniumFunc.waitFor(5);
	}

	public void enterNamedInsured(){

	}




	public void enterConstructionPeriod(){

	}

	public void enterTerrisomExtensionPeriod(){

	}

	public void addDedctibleForPolicies(String name,String amount,String description) throws Exception {
		SeleniumFunc.waitFor(3);
		try {
			SeleniumFunc.xpath_GenericMethod_Click(Add_addtional_Dedectible);
		} catch (Exception e) {
			SeleniumFunc.xpath_GenericMethod_Click(Add_addtional_Dedectibles);
		}
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Add_addtional_Dedectible_additeam);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.SelectValueInAutoPopulateDropDownUsingXpath(Add__addtional_Dedectible_addname, name);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.clearTextBox(By.xpath(Add__addtional_Dedectible_addCAmount));
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Add__addtional_Dedectible_addCAmount, amount);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.clearTextBox(By.xpath(Add_addtional_deductible_description));
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Add_addtional_deductible_description, description);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Submit_UW_Dialogue);
		SeleniumFunc.waitFor(2);
	}


	public void SubmitQuote() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_Submit);
	}

	public void addDedctibleForPPL(String marinedlibality,String propDamage,String personInujury) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(add_Dedectible_button);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(add_Dedectible_marined_liablity,marinedlibality);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(add_Dedectible_Property_damage,propDamage);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(add_Dedectible_PersonalorOtherInujury,personInujury);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(Submit_UW_Dialogue);

	}

	public void addDedcutibleForProfessionalIndeminity() throws Exception {
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(Add_addtional_Dedectible);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(Add_addtional_Dedectible_additeam);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Add__addtional_Dedectible_addname,"Each Occurance");
//		Thread.sleep(500);
		SeleniumFunc.waitFor(2);
		driver.findElement(By.xpath(Add__addtional_Dedectible_addname)).sendKeys(Keys.ARROW_DOWN);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='Each Occurance']");
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(Submit_UW_Dialogue);
		SeleniumFunc.waitFor(3);
		clickSubmit();
	}

	public void SearchAndBeginTheCaseInRiskAdivisor() throws Exception {
		   WebElement po = SeleniumFunc.getElement(By.xpath(search_feild_internal));
//		   testData.setCaseId("P-12821");
		   po.sendKeys(testData.getCaseId());
			SeleniumFunc.waitFor(1);
		   po.sendKeys(Keys.ENTER);
		   SeleniumFunc.waitFor(20);
		   SeleniumFunc.xpath_GenericMethod_Click(three_dots);
		   SeleniumFunc.waitFor(3);
		   SeleniumFunc.xpath_GenericMethod_Click(start_search);
		   SeleniumFunc.waitFor(6);

		   try {
		      SeleniumFunc.xpath_GenericMethod_Click(btn_begin);
		   } catch (Exception e) {
		      SeleniumFunc.xpath_GenericMethod_Click(btn_begin);
		   }
		   SeleniumFunc.waitFor(2);
	}





	public void verifyUWCasePaymentStatus1(String casestatus) throws Exception {
//		WebDriverWait wait = new WebDriverWait(driver, 10);
//	   wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(StatusCase))));
		// SeleniumFunc.xpath_GenericMethod_scrollIntoView(StatusCase);
		String casestatusscreen = xpath_Genericmethod_getElementText(StatusCase);
		System.out.println("Insurance status after clicking finish button : " + casestatusscreen);
		Assert.assertTrue(casestatus.equals(casestatusscreen));
		SeleniumFunc.writeToAssertionFile(
				"Expected payment case status : " + casestatus + " , Actual status : " + casestatusscreen,
				(casestatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
	}
	public void verifyUWCasePaymentStatus2(String casestatus) throws Exception {

		String casestatusscreen = xpath_Genericmethod_getElementText(txtdel);
		System.out.println("Insurance status after clicking finish button : " + casestatusscreen);
		Assert.assertTrue(casestatus.equals(casestatusscreen));
		SeleniumFunc.writeToAssertionFile(
				"Expected payment case status : " + casestatus + " , Actual status : " + casestatusscreen,
				(casestatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
	}

	public void clickOnExit() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(Exit_Button);
	}

	public void ClickonRefresh() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(Refresh_policy);
	}




	public double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;

	}

	public String roundWithString(double value, int places) {
		String s;
		if (places < 0)
			throw new IllegalArgumentException();
		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		System.out.println("String lengeth"+Double.toString((double) tmp / factor).length());
		s=Double.toString((double) tmp / factor);
		if((s.length()-(s.indexOf(".")+1))!=1) return Double.toString((double) tmp / factor);
		else return (Double.toString((double) tmp / factor))+0;
	}

	public void calculateAndValidatePremiumDetails(int intstudents, int intdays, float floatmul, float floatgst,float floatstamp) throws Exception {
		double annualbasepremium = intstudents * floatmul;
		double basepremium = (annualbasepremium / 365) * intdays;
		double gst = basepremium * floatgst;
		double basepremiumincludinggst = basepremium + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(basepremium, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
		String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
		String totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);

		basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
		gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
		totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
		stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

		SeleniumFunc.writeToAssertionFile(
				"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen,
				(basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + gstscreen,
				(gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen,
				(stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
						+ totalpremiumscreen,
				(totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));
	}

	public void calculateAndValidatePremiumDetails1(int intStudents, long intDays, float floatMul, float floatGST,
			float floatStamp) throws Exception {

		double annualBasePremium = intStudents * floatMul;
		double basePremium = (annualBasePremium / 365) * intDays;
		double gst = basePremium * floatGST;
		double basePremiumIncludingGST = basePremium + gst;
		double stampDuty = basePremiumIncludingGST * floatStamp;
		double totalPremium = basePremiumIncludingGST + stampDuty;
		String basePremiumInString = Double.toString(round(basePremium, 2));
		String GSTInString = Double.toString(round(gst, 2));
		String stampDutyInString = Double.toString(round(stampDuty,2));
		String totalExpectedPremiumInString = Double.toString(round(totalPremium, 2));

		String basePremiumScreen = xpath_Genericmethod_getElementText(base_premium);
		String GSTScreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampDutyScreen = xpath_Genericmethod_getElementText(stamp_duty);
		String totalPremiumScreen = xpath_Genericmethod_getElementText(total_premium);

		// NumberFormat format = NumberFormat.getCurrencyInstance();

		basePremiumScreen = Double.toString(round(Double.parseDouble(basePremiumScreen.replaceAll("[^-.0-9]", "")), 2));
		GSTScreen = Double.toString(round(Double.parseDouble(GSTScreen.replaceAll("[^-.0-9]", "")), 2));
		totalPremiumScreen = Double.toString(round(Double.parseDouble(totalPremiumScreen.replaceAll("[^-.0-9]", "")), 2));
		stampDutyScreen = Double.toString(round(Double.parseDouble(stampDutyScreen.replaceAll("[^-.0-9]", "")), 2));

		// Number numberbasepremiumscreen = format.parse(basePremiumScreen);
		// basePremiumScreen = numberbasepremiumscreen.toString();
		// System.out.println("Compare 1");
		// System.out.println(basePremiumScreen);
		// System.out.println("Compare 2");
		// System.out.println(basepremiuminstring);

//		String s1 = "$";
		String s1 = "";
		String concatenatedBasePremium = s1.concat(basePremiumInString);
		// System.out.println(S2);
		basePremiumScreen = basePremiumScreen.replaceAll(",", "");
		// System.out.println("After comma removing :"+basePremiumScreen);

		if (basePremiumScreen.equals(concatenatedBasePremium)) {
			System.out.println("Expected Base premium is :" + concatenatedBasePremium);
			System.out.println("Actual Base premium is :" + basePremiumScreen);
			// System.out.println("Base premium is coming Fine");
		} else {
			System.out.println("Expected Base premium is :" + concatenatedBasePremium);
			System.out.println("Actual Base premium is :" + basePremiumScreen);
			// System.out.println("Base premium is not coming correct");
		}
		String concatenatedGST = s1.concat(GSTInString);
		// System.out.println(S3);
		GSTScreen = GSTScreen.replaceAll(",", "");
		// System.out.println("After comma removing :"+GSTScreen);

		if (GSTScreen.equals(concatenatedGST)) {
			System.out.println("Expected GST value is :" + concatenatedGST);
			System.out.println("Actual GST value:" + GSTScreen);

			// System.out.println("GST is coming Fine");
		} else {
			System.out.println("Expected GST value is :" + concatenatedGST);
			System.out.println("Actual GST value:" + GSTScreen);
			// System.out.println("GST is not coming correct");
		}

		String concatenatedTotalPremium = s1.concat(totalExpectedPremiumInString);
		totalPremiumScreen = totalPremiumScreen.replaceAll(",", "");
		if (totalPremiumScreen.equals(concatenatedTotalPremium)) {
			// System.out.println("Total premium is coming fine");
			System.out.println("Expected total  premium is :" + concatenatedTotalPremium);
			System.out.println("Actual total  premium is :" + totalPremiumScreen);
			// System.out.println("Total premium is coming fine");

		} else {
			// System.out.println("Total premium is not coming correct");
			System.out.println("Expected total  premium is :" + concatenatedTotalPremium);
			System.out.println("Actual total  premium is :" + totalPremiumScreen);
		}

		// SeleniumFunc.writeToAssertionFile("Expected base premium :
		// "+basepremiuminstring+" , Actual base premium :
		// "+basePremiumScreen,(basepremiuminstring.equals(basePremiumScreen)?"PASS":"FAIL"));
		// SeleniumFunc.writeToAssertionFile("Expected gst : "+gstinstring+" , Actual
		// gst : "+GSTScreen,(gstinstring.equals(GSTScreen)?"PASS":"FAIL"));
		// SeleniumFunc.writeToAssertionFile("Expected stamp duty :
		// "+stampDutyInString+" , Actual stamp duty :
		// "+stampDutyScreen,(stampDutyInString.equals(stampDutyScreen)?"PASS":"FAIL"));
		// SeleniumFunc.writeToAssertionFile("Expected total premium :
		// "+totalExpectedPremiumInString+" , Actual total premium :
		// "+totalPremiumScreen,(totalExpectedPremiumInString.equals(totalPremiumScreen)?"PASS":"FAIL"));

	}

	public void calculateAndValidateEndorsementPremiumDetails(int intstudents, int updatedstudents, int intdays,
			float floatmul, float floatgst, float floatstamp) throws Exception {
		double annualbasepremium = (updatedstudents - intstudents) * floatmul;
		double basepremium = (annualbasepremium / 365) * intdays;
		double gst = basepremium * floatgst;
		double basepremiumincludinggst = basepremium + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(basepremium, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
		String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
		String endorsementpremiumscreen = xpath_Genericmethod_getElementText(endorsement_premium);

		basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
		gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
		endorsementpremiumscreen = endorsementpremiumscreen.replaceAll("[^-.0-9]", "");
		stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

		SeleniumFunc.writeToAssertionFile(
				"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen,
				(basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + gstscreen,
				(gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen,
				(stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
						+ endorsementpremiumscreen,
				(totalexpectedpremiuminstring.equals(endorsementpremiumscreen) ? "PASS" : "FAIL"));

	}

	public void calculateAndValidatePremiumDetailsForProperty(int propertyvalue, int minimumvalue, float percentage,
			float floatgst, float floatstamp, int days) throws Exception {

		double basepremium;
		/*
		 * if(propertyvalue<=250000) { basepremium = minimumvalue; }
		 * if(propertyvalue>250000 && propertyvalue<=500000) { basepremium =
		 * minimumvalue; } if(propertyvalue>500000 && propertyvalue<=1000000) { double
		 * interprem = propertyvalue*percentage; if(interprem<minimumvalue) {
		 * basepremium = minimumvalue; } else basepremium = interprem; }
		 */
		if (propertyvalue <= 500000) {
			basepremium = minimumvalue;
		} else {
			double interprem = propertyvalue * percentage;
			if (interprem < minimumvalue) {
				basepremium = minimumvalue;
			} else {
				basepremium = interprem;
			}
		}

		basepremium = (basepremium * days) / 365;
		double gst = basepremium * floatgst;
		double basepremiumincludinggst = basepremium + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(basepremium, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basePremiumScreen = xpath_Genericmethod_getElementText(base_premium);
		String GSTScreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampDutyScreen = xpath_Genericmethod_getElementText(stamp_duty);
		String totalPremiumScreen = xpath_Genericmethod_getElementText(total_premium);

		basePremiumScreen = Double.toString(round(Double.parseDouble(basePremiumScreen.replaceAll("[^-.0-9]", "")), 2));
		GSTScreen = Double.toString(round(Double.parseDouble(GSTScreen.replaceAll("[^-.0-9]", "")), 2));
		totalPremiumScreen = Double.toString(round(Double.parseDouble(totalPremiumScreen.replaceAll("[^-.0-9]", "")), 2));
		stampDutyScreen = Double.toString(round(Double.parseDouble(stampDutyScreen.replaceAll("[^-.0-9]", "")), 2));

		SeleniumFunc.writeToAssertionFile(
				"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basePremiumScreen,
				(basepremiuminstring.equals(basePremiumScreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + GSTScreen,
				(gstinstring.equals(GSTScreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampDutyScreen,
				(stampdutyinstring.equals(stampDutyScreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
						+ totalPremiumScreen,
				(totalexpectedpremiuminstring.equals(totalPremiumScreen) ? "PASS" : "FAIL"));

	}

	public void calculateAndValidateEndorsementPremiumDetailsForProperty(int propertyvalue, int minimumvalue,
			int updatedminimumvalue, float percentage, float floatgst, float floatstamp, int days) throws Exception {

		double basepremium;
		/*
		 * if(propertyvalue<=250000) { basepremium = minimumvalue; }
		 * if(propertyvalue>250000 && propertyvalue<=500000) { basepremium =
		 * minimumvalue; } if(propertyvalue>500000 && propertyvalue<=1000000) { double
		 * interprem = propertyvalue*percentage; if(interprem<minimumvalue) {
		 * basepremium = minimumvalue; } else basepremium = interprem; }
		 */
		if (propertyvalue <= 500000) {
			basepremium = updatedminimumvalue - minimumvalue;
		} else {

			double interprem = propertyvalue * percentage;
			if (interprem < minimumvalue) {
				basepremium = minimumvalue;
			} else
				basepremium = interprem;

		}

		basepremium = (basepremium * days) / 365;
		double gst = basepremium * floatgst;
		double basepremiumincludinggst = basepremium + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(basepremium, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
		String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
		String totalpremiumscreen;
		try{
			totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);
		}catch(Exception e){
			totalpremiumscreen = xpath_Genericmethod_getElementText(endorsement_premium);
		}


		basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
		gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
		totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
		stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

		SeleniumFunc.writeToAssertionFile(
				"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen,
				(basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + gstscreen,
				(gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen,
				(stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
						+ totalpremiumscreen,
				(totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));

	}
	public void acceptQuotewithNo() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(accept_quote);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Click(uw_portal_dialog_submit_button_accept_quoate);
		SeleniumFunc.waitFor(35);
	}

	public void acceptQuote() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(accept_quote);

//		SeleniumFunc.waitFor(2);
//		SeleniumFunc.xpath_GenericMethod_Click(AccptQuoate_YES);
//		SeleniumFunc.waitFor(4);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(AccpetQuoate_purachseNo, "12455434");
		SeleniumFunc.waitFor(1);
		SeleniumFunc.click(By.xpath(AcceptQuate_Submit));
		SeleniumFunc.waitFor(8);
//		SeleniumFunc.waitFor(60);
	}



	public String verifyUpdatedProfessionInAcceptQuoate() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Uw_clientSpecific_professional);
		return SeleniumFunc.getElement(By.xpath(Profession_text)).getText();
	}

	public void selectNoAndClickSubmitInPurchaseOrderPopup() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(AccptQuoate_NO);
		SeleniumFunc.id_GenericMethod_Click(AcceptQuate_Submit);
		SeleniumFunc.waitFor(3);
	}

	public void clickHamburger(String option) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(btnhamburger);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click("//span[@class='menu-item-title-wrap']/span[@class='menu-item-title' and text()='"+option+"']");
	}

	public void SearchPolicyUsingPolicyNumber(String policynumber) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(Search_option_policy);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(Search_criteria_policyNumber,policynumber);
		SeleniumFunc.xpath_GenericMethod_Click(Search_button);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click("//a[text()='"+policynumber+"']");
	}

	public void clickPolicyHisotryAndDollorIcon() throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(policyHistory);
		SeleniumFunc.waitFor(10);

	}

	public void ValidatePremimumDetailsInPolicyHistoryTable(String CalculationType,String policyTracnstion) throws Exception {
		String policyDuration=null,basePremium=null,policytype=null,autobasePremium=null;

		List<WebElement> policyhistoryTableicon=SeleniumFunc.getElements("//table[@pl_prop='D_FetchPreviousTransactions.pxResults']/tbody/tr/td[12]//span/i");
		for(int i=0;i<policyhistoryTableicon.size();i++)
		{
			if(policyhistoryTableicon.get(i).isDisplayed()){
				policytype=SeleniumFunc.getElement(By.xpath("//table[@pl_prop='D_FetchPreviousTransactions.pxResults']/tbody/tr/td[2]/div/div/div/div/div[1]/span")).getText();
				policyDuration=SeleniumFunc.getElement(By.xpath("//table[@pl_prop='D_FetchPreviousTransactions.pxResults']/tbody/tr["+i+"]/td[6]//span")).getText();
				basePremium=SeleniumFunc.getElement(By.xpath("//table[@pl_prop='D_FetchPreviousTransactions.pxResults']/tbody/tr["+i+"]/td[9]//span/span")).getText();
				autobasePremium=SeleniumFunc.getElement(By.xpath("//table[@pl_prop='D_FetchPreviousTransactions.pxResults']/tbody/tr["+i+"]/td[10]//span/span")).getText();
				policyhistoryTableicon.get(i).click();
			}

			//h2[text()='Client charges']/ancestor::div[@id='EXPAND-OUTERFRAME']//span[text()='Endorsement duration']/following-sibling::div/span/span
			//h2[text()='Client charges']/ancestor::div[@id='EXPAND-OUTERFRAME']//span[text()='Endorsement premium']/following-sibling::div/span/span
		}
	}

	public void validatePopUpDetails(String premiumCalc,String basepremium){
		List<String> popupElements=new ArrayList<>();
		if(premiumCalc.equals("POA")){
			String[] POA_Endorsement=new String[]{"Endorsement duration","Endorsement premium"};
			String[] POA_NewApplication=new String[]{"Base premium","Policy duration"};
		}else if(premiumCalc.equals("Automatic")){
			String[] POA_Endorsement=new String[]{"Endorsement duration","Endorsement premium","Price difference","Auto base premium"};
			String[] POA_NewApplication=new String[]{"Base premium","Policy duration","Auto base premium","Price difference"};
		}
	}

	public void validatePopUpDetails_Automatic(String policyDuraion,String basepremium,String AutomBasePremium){

	}

	public void selectYesAndEnterPurchaseOrderNoInSubmitInPurchaseOrderPopup(String PurchaseOrderNo) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(AccptQuoate_YES);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(AccpetQuoate_purachseNo, PurchaseOrderNo);
		SeleniumFunc.id_GenericMethod_Click(AcceptQuate_Submit);
		SeleniumFunc.waitFor(3);
	}


	public void verifyUWPendingCaseStatus(String casestatus) throws Exception {
		SeleniumFunc.waitFor(5);
		String casestatusscreen = xpath_Genericmethod_getElementText(uw_case_status_pending_uw);
		System.out.println(casestatusscreen + "************************");
		System.out.println(casestatus + "***********************");
		Assert.assertTrue(casestatus.equals(casestatusscreen));
		SeleniumFunc.waitFor(8);

	}

	public void enterIdInSearchBox(String caseid) throws Exception {
		Thread.sleep(3000);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(searchbox, caseid);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(searchbox,Keys.ENTER);
	}

	public void beginTheCase() throws Exception {

		SeleniumFunc.waitFor(10);
		if(SeleniumFunc.getElements(begincase).size()>0)
			SeleniumFunc.xpath_GenericMethod_Click(begincase);
		else
			openCaseFromUnderwriterQueue("Underwriting");
	}

	public void clickOnNoteInterstedPolicy() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='V024629']/ancestor::span[1]/following-sibling::span//button[text()='More actions']");
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(button_NoteInterstedPolicy);
	}


	public void enterNoteInterstedPartydetails(String nameOfThePart,String effectiveDate,String fromDate,String toDate) throws Exception {

		SeleniumFunc.enterValue(nameOfTheParty,nameOfThePart);
		SeleniumFunc.enterValue(cerifticateEffectiveDate,effectiveDate);
		SeleniumFunc.enterValue(cerifticateFromDate,fromDate);
		SeleniumFunc.enterValue(cerifticateToDate,toDate);
		SeleniumFunc.enterValue(InititorComments,nameOfThePart);

	}

	public void clickonmoreactionsandcancelpolicy() throws Exception
	{
    	 //TestBase.currentPolicyNumber = "V010885";
		  SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Click(vmia_logo);
		Thread.sleep(2000);
		SeleniumFunc.waitFor(30);
	    SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+TestBase.currentPolicyNumber+"']/ancestor::span[1]/following-sibling::span//button[text()='More actions']");
//	    SeleniumFunc.clickJS(driver.findElement(By.xpath("//span[text()='"+TestBase.currentPolicyNumber+"']/ancestor::span[1]/following-sibling::span//button[text()='More actions']/i")));
	    SeleniumFunc.waitFor(2);
	    SeleniumFunc.xpath_GenericMethod_Click(btncancelpolicy);
		//SeleniumFunc.xpath_GenericMethod_Click(btn_veiw_more);

//		//.xpath_GenericMethod_Click(btnmoreactions);
//		Thread.sleep(2000);
//		SeleniumFunc.xpath_GenericMethod_Click(btncancelpolicy);
	}

	public void selectsearchandenterpolicyID() throws Exception
	{

		WebElement s = driver.findElement(By.xpath(search_uw_portal));
		s.sendKeys(ID_no);
		s.sendKeys(Keys.ENTER);

	}
	public void capturecanceIDno() throws Exception {
		String Cancel_policy_no;
		try {
		Cancel_policy_no = 	xpath_Genericmethod_getElementText(capture_id_cancel);
		} catch (Exception e) {
			Cancel_policy_no = 	xpath_Genericmethod_getElementText(capture_id_cancel);
		}
		System.out.println(Cancel_policy_no);
		testData.setCaseId(Cancel_policy_no.substring(4, 11)) ;
		System.out.println(ID_no);

	}

	public void openCaseFromUnderwriterQueue(String queue) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(selectQueueDropdown);
		SeleniumFunc.waitFor(1);

		SeleniumFunc.xpath_GenericMethod_Click(UnderwritingOption+"span[text()='"+queue+"']");
		SeleniumFunc.waitFor(4);
		SeleniumFunc.switchToDefaultContent();

		if(queue.equalsIgnoreCase("Asset Management")) {
			System.out.println("Control was here");
			SeleniumFunc.xpath_GenericMethod_Click(UW_Casenumber_TH1); SeleniumFunc.waitFor(4);
			SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(filterOption_Asset)));}
		else {
		SeleniumFunc.xpath_GenericMethod_Click(UW_Casenumber_TH);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(filterOption)));}
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(uw_applyfilter);
//		testData.setCaseId("PRO-24622");
		System.out.println("The case id is:----"+testData.getCaseId()+"------------------");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(UW_search_text, testData.getCaseId());
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(UW_search_text, "AA-187");
		SeleniumFunc.xpath_GenericMethod_Click(UW_search_applyfilter);
		Thread.sleep(10000);
		SeleniumFunc.waitForElementToClikable(By.xpath(uw_clickcase),40);
		SeleniumFunc.xpath_GenericMethod_Click(uw_clickcase);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.acceptTheAlert();

	}

	public void selectDeactivateEndorsementID() throws Exception {
//		SeleniumFunc.xpath_GenericMethod_Click(UW_Casenumber_TH);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(filterOption)));
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(uw_applyfilter);

		System.out.println("The case id is:----"+testData.getCaseId()+"------------------");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(UW_search_text,testData.getCaseId());
		SeleniumFunc.xpath_GenericMethod_Click(UW_search_applyfilter);
//		Thread.sleep(10000);
//		SeleniumFunc.waitForElementToClikable(By.xpath(uw_clickcase),40);
		SeleniumFunc.waitFor(10);
		SeleniumFunc.xpath_GenericMethod_Click(uw_clickcase);
//		SeleniumFunc.waitFor(2);
//		SeleniumFunc.acceptTheAlert();
	}

	public void validateRejectedComments(String actualComments){
		SeleniumFunc.waitFor(2);
		try{
			SeleniumFunc.switchToDefaultContent();
		}catch(Exception e){
			driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@name='PegaGadget1Ifr']")));
		}

		String expectedComments = SeleniumFunc.getElementText(driver.findElement(By.cssSelector(rejecCom)));
		Assert.assertEquals(actualComments,expectedComments);
	}

	public void approveRequest() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click("//*[@name='$PpyWorkPage$pRequestApproved']/following-sibling::label[text()='Approve']");
		SeleniumFunc.xpath_GenericMethod_Click(Asset_Submit);
	}

	public void rejectRequest(String comments) throws Exception {
			SeleniumFunc.xpath_GenericMethod_Click("//*[@name='$PpyWorkPage$pRequestApproved']/following-sibling::label[text()='Reject']");
			SeleniumFunc.waitFor(2);
			SeleniumFunc.name_GenericMethod_Sendkeys(Asset_moreinfo,comments);
			SeleniumFunc.xpath_GenericMethod_Click(Asset_Submit);
	}


	public void ActionOnAsset(String Action,String endorseNeed) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		if(Action.equalsIgnoreCase("Approve"))
			SeleniumFunc.xpath_GenericMethod_Click(Asset_Approve);
		else if(Action.equalsIgnoreCase("Reject")) {
			SeleniumFunc.xpath_GenericMethod_Click(Asset_Reject);
			Thread.sleep(900);
			SeleniumFunc.name_GenericMethod_Sendkeys(Asset_moreinfo, "Asset got rejected");
			}
		else if(Action.equalsIgnoreCase("Request more information")) {
			SeleniumFunc.xpath_GenericMethod_Click(Asset_req_more_info); Thread.sleep(900);
			SeleniumFunc.name_GenericMethod_Sendkeys(Asset_moreinfo, "Need More info to approve the asset");
			}
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(Asset_Submit);

	if(Action.equalsIgnoreCase("Approve")) {
		if(endorseNeed.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(Asset_Need_Endorstment_Yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(Asset_Need_Endorstment_No);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(Asset_Submit);	}
	}

	public void verifyTheCaseStatus(String expstatus) throws Exception {
		String actualstatus=SeleniumFunc.getElementTextWithXpath(case_status);
		Assert.assertEquals(actualstatus, expstatus);
	}
	public void acceptQuoteUW() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(uw_accept_quote);
	}

	public void submitQuoteUW() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(uw_submit);
	}

	public void verifyNewApplication() throws Exception {
		for(int i=0; i<10; i++){
			try{
				System.out.println("Its in try block");
				driver.findElement(By.xpath("//td[@aria-describedby='Case type']/span[text()='New Application']"));
				break;
			}catch(Exception e){
				System.out.println("Its came to catch block");
				SeleniumFunc.scrollIntoView(driver.findElement(By.xpath("//a[contains(text(),'Next')]")));
				SeleniumFunc.xpath_GenericMethod_Click("//a[contains(text(),'Next')]");

			}

		}
//		List<WebElement> newApp =
//		for(int i=0; i<10; i++){
//			if(newApp.size()==0){
//				driver.findElement(By.xpath("//a[contains(text(),'Next')]"));
//			}else{
//				break;
//			}
//		}
		SeleniumFunc.clickElementUsingActions(By.xpath("//td[@aria-describedby='Case type']/span[text()='New Application']/parent::td/preceding-sibling::td[@aria-describedby='Case ID']/span/a"));

	}

	public void verifyEndorseApplication() throws Exception {
		for(int i=0; i<10; i++){
			try{
				System.out.println("Its in try block");
				driver.findElement(By.xpath("//td[@aria-describedby='Case type']/span[text()='Update Policy']"));
				break;
			}catch(Exception e){
				System.out.println("Its came to catch block");
				SeleniumFunc.scrollIntoView(driver.findElement(By.xpath("//a[contains(text(),'Next')]")));
				SeleniumFunc.xpath_GenericMethod_Click("//a[contains(text(),'Next')]");

			}
		}
		SeleniumFunc.clickElementUsingActions(By.xpath("//td[@aria-describedby='Case type']/span[text()='Update Policy']/parent::td/preceding-sibling::td[@aria-describedby='Case ID']/span/a"));

	}

	public void verifyUpdatePolicyDetails() throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click("//*[text()='Update']/i");
		Assert.assertTrue(driver.findElement(By.xpath("//div[@id='modaldialog_hd']/span[contains(text(),'Update')]")).isDisplayed());
		SeleniumFunc.xpath_GenericMethod_Click("//button[text()='  Cancel ']");
	}

	public void verifyUpdateRiskInfo() throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click("//button[text()='Update risk information']/i");
//		SeleniumFunc.clickElementUsingActions(By.xpath("//button[@name='PremiumDetails_pyWorkPage.ApplicationPage_33']/i"));
		Assert.assertTrue(driver.findElement(By.xpath("//div[@id='modaldialog_hd']/span[contains(text(),'Risk ')]")).isDisplayed());
		SeleniumFunc.xpath_GenericMethod_Click("//button[text()='  Cancel ']");
	}

	public void verifyAddEndorsementText() throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(add_Endorsemnt_text_button);
		Assert.assertTrue(driver.findElement(By.xpath("//span[text()='        Endorsement text       ']")).isDisplayed());
		SeleniumFunc.xpath_GenericMethod_Click("//button[text()='  Cancel ']");
	}

	public void searchAndOpenUWApprovedCase() throws Exception {
		SeleniumFunc.waitForElementToClikable(By.xpath(dashboard_filter_caseid),120);
//		Thread.sleep(2000);
		SeleniumFunc.xpath_GenericMethod_Click(dashboard_filter_caseid);
		Thread.sleep(2000);
//		testData.setCaseId("PRO-24622");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(search_filter_textbox,testData.getCaseId());
		Thread.sleep(2000);
		SeleniumFunc.xpath_GenericMethod_Click(apply_button_filter);
		Thread.sleep(2000);
		SeleniumFunc.xpath_GenericMethod_Click(first_case_in_filter_table);
		Thread.sleep(2000);
	}


	public void moreEndorsementsQues() throws Exception {
		SeleniumFunc.waitFor(1);
		// SeleniumFunc.xpath_GenericMethod_Click(more_updations_ques_no);
		try {
			SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(more_updations_ques_no)));
		} catch (Exception e) {

		}
	}

	public void cancellationQuesProEndorse() throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(cancellation_cover_no);
	}

	public void searchAndOpenPolicyForEndorsement() throws Exception {
		Thread.sleep(2000);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(endorsement_policyid_filter);
		Thread.sleep(2000);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(policyid_search_box, TestBase.currentPolicyNumber);

		Thread.sleep(2000);
		SeleniumFunc.xpath_GenericMethod_Click(apply_button_filter);
		Thread.sleep(2000);
		SeleniumFunc.xpath_GenericMethod_Click(update_policy_button);
		Thread.sleep(2000);
	}

	public void enterValueForPropertyTypeInsurance(String value, String propertyval) throws Exception {
		String property_value_xpath = "//label[text()='" + value + "']";
		SeleniumFunc.xpath_GenericMethod_Click(property_value_xpath);
		int propvalininteger = Integer.parseInt(propertyval);
		if (value.equals("Over $500,000")) {
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(prop_extra_amount, propertyval);
			if (propvalininteger >= 10000000) {
				SeleniumFunc.xpath_GenericMethod_Click(property_value_xpath);
				SeleniumFunc.waitFor(1);
				// SeleniumFunc.xpath_GenericMethod_Click(prop_over_ten_million_no);
			}
		}

	}

	public void selectPRO() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(uw_property_button);
	}

	public void selectLiability() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(uw_liability_button);
	}

	public void selectMotorContingency() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(uw_msc_button);
	}

	public void answerRiskquestionNo() throws Exception {

		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(liab_risk_question_1_No);
		Thread.sleep(2000);

		SeleniumFunc.xpath_GenericMethod_Click(liab_risk_question_2_No);

	}

	public void selectIBPSFromRiskInformation(String option) throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click("//button[text()='Update risk information']/i");
		SeleniumFunc.xpath_GenericMethod_Click("//input[@name='$PTmpPage$pApplicationPage$pIsIBPSEndorsement']/following-sibling::label[text()='"+option+"']");
		SeleniumFunc.xpath_GenericMethod_Click("//button[@id='ModalButtonSubmit']");

	}

	public void validateTheMessages(String ibpsmsg) throws Exception {
		WebElement ibpsmessage = driver.findElement(By.xpath("//div[text()='This endorsement has been initiated for IBPS refund.']"));
		System.out.println("Expected Message***** "+ibpsmsg);
		System.out.println("Received message***** "+ibpsmessage.getText());
		Assert.assertEquals(ibpsmessage.getText(),ibpsmsg);
	}

	public void selectPolicyDecisionAndSubmitTheCase(String decison) throws Exception {
	 	SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(3);
		System.out.println("Able to process here");
		if(decison.equals("Yes")){
//			SeleniumFunc.xpath_GenericMethod_Click(btnyes);
			SeleniumFunc.clickElementUsingActions(By.xpath(btnyes));
		}
		else {
//			SeleniumFunc.xpath_GenericMethod_Click(btnno);
			SeleniumFunc.clickElementUsingActions(By.xpath(btnno));
		}
		SeleniumFunc.xpath_GenericMethod_Click(Asset_Submit);
		System.out.println("Cliked on submit button");
	}
	public void SearchAndReviewTheCaseInRiskAdivisor() throws Exception {
		SeleniumFunc.waitFor(10);
		SeleniumFunc.xpath_GenericMethod_Click(RiskAdvisorWorkBasket);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(RiskAdvisorQueue , "Risk Adviser Queue");
		SeleniumFunc.waitFor(5);

	}

	public void clickOnCaseID() throws Exception {
		SeleniumFunc.waitFor(8);
		SeleniumFunc.xpath_GenericMethod_Click(riskCaseID);

	}

	public void clickOnCaseNumber() throws Exception {
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(clientCaseNumber);

	}

	public void enterIDAndClickApply() throws Exception {
		SeleniumFunc.waitFor(5);
		WebElement searchField = SeleniumFunc.getElement(By.xpath(riskSearchTestField));
//		   testData.setCaseId("P-12821");
		System.out.println("The Case ID is: "+testData.getCaseId());
		searchField.sendKeys(testData.getCaseId());
		SeleniumFunc.xpath_GenericMethod_Click(riskApplyBtn);
	}

	public void entercaseNumberIDAndClickApply() throws Exception {
		SeleniumFunc.waitFor(5);
		WebElement searchField = SeleniumFunc.getElement(By.xpath(clientSearchTextField));
//		   testData.setCaseId("P-12821");
		searchField.sendKeys(testData.getCaseId());
		SeleniumFunc.xpath_GenericMethod_Click(riskApplyBtn);
	}

	public void selectFirstCaseIDFromTable() throws Exception {
		SeleniumFunc.waitFor(4);
		List<WebElement> caseIDList = driver.findElements(By.xpath(viewQueueTableCaseID));
		int listSize = SeleniumFunc.getElements(viewQueueTableCaseID).size();
		if(listSize==1){
			SeleniumFunc.xpath_GenericMethod_Click(viewQueueTableCaseID);
		}else if(listSize>1){
			Assert.assertEquals(1,listSize);
		}
	}

	public void selectFirstCaseIDFromMycaseTable() throws Exception {
		SeleniumFunc.waitFor(4);
//		List<WebElement> caseIDList = driver.findElements(By.xpath(tablenumberID));
		int listSize = SeleniumFunc.getElements(tablenumberID).size();
		if(listSize==1){
			SeleniumFunc.xpath_GenericMethod_Click(tablenumberID);
		}else if(listSize>1){
			Assert.assertEquals(1,listSize);
		}
	}

	public void scearchCaseNumberInRiskAdvisor() throws Exception{
		SeleniumFunc.waitFor(10);
		WebElement case_filter = driver.findElement(By.xpath(Apply_Filter_RA));
		Actions Ac_Filter = new Actions(driver);
		Ac_Filter.moveToElement(case_filter).click().perform();
		SeleniumFunc.waitFor(10);
		//testData.setCaseId("HKL-43");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(search_filter_textbox, testData.getCaseId());
		SeleniumFunc.waitFor(3);
		WebElement ApplyButton = driver.findElement(By.xpath(apply_button_filter));
		Ac_Filter.moveToElement(ApplyButton).click().perform();
//		jsClick(apply_button_filter);
		SeleniumFunc.waitFor(7);
		SeleniumFunc.xpath_GenericMethod_Click(first_case_in_ra_fliter);
		SeleniumFunc.waitFor(2);
	}

	public String endorsementText(String Endo) throws Exception{
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(add_Endorsemnt_text_button);
		SeleniumFunc.waitFor(1);
		WebElement frame = driver.findElement(By.xpath(internal_frame));
		driver.switchTo().frame(frame);
		WebElement Endo_text = driver.findElement(By.xpath(internal_text));
		Endo_text.sendKeys(Keys.CONTROL + "a");
		Endo_text.sendKeys(Keys.DELETE);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(internal_text,Endo);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(extend_Endo_No);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(Submit_UW_Dialogue);
		SeleniumFunc.waitFor(2);
//		SeleniumFunc.xpath_GenericMethod_Click(Endorsement_text_button);
//		SeleniumFunc.waitFor(2);
//		EnteredEndoText = SeleniumFunc.getElementTextWithXpath("//div[@class='rteReadOnlyWithoutTB']/p[text()='"+Endo+"']");
//		SeleniumFunc.getElementText()
//		SeleniumFunc.waitFor(1);
		return Endo;
	}
	public void verifyendorsementText() throws Exception{
		//SeleniumFunc.xpath_GenericMethod_Click(Endorsement_text_button);
		SeleniumFunc.waitFor(1);
		String expectedEndrosementText = Endo;
		EnteredEndoText = SeleniumFunc.getElementText("//div[@class='rteReadOnlyWithoutTB']/p[text()='"+Endo+"']");
		System.out.println("Acutal Endorsement Text : " + EnteredEndoText + "Expected Endorsement Text : " + expectedEndrosementText );
		SeleniumFunc.waitFor(1);
		SoftAssert endoAssert = new SoftAssert();
		endoAssert.assertEquals(EnteredEndoText,expectedEndrosementText);
		endoAssert.assertAll();


	}
	public void answerRiskquestionYes(String hirevalue) throws Exception {

		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(liab_risk_question_1_Yes);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(
				hire_out_facilities_dropdown, hirevalue);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(liab_risk_question_gym_No);
		SeleniumFunc.xpath_GenericMethod_Click(liab_risk_question_gym_No);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(liab_risk_question_2_No);

	}

	public void doMarketStallOperationsForLiability(String marketstallholders, String numberofstallevents)
			throws Exception {

		int numberofstalleventsint = Integer.parseInt(numberofstallevents);
		SeleniumFunc.xpath_GenericMethod_Click(liab_risk_question_2_Yes);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(liab_number_unincorporated_market_stall_holders, marketstallholders);

		for (int marketstalleventcounter = 0; marketstalleventcounter < numberofstalleventsint; marketstalleventcounter++) {
			SeleniumFunc.xpath_GenericMethod_Click(market_stall_datepicker);
			SeleniumFunc.xpath_GenericMethod_Click(current_date);
			SeleniumFunc.waitFor(1);
			SeleniumFunc.xpath_GenericMethod_Click(add_selected_date_button);
			SeleniumFunc.waitFor(1);
		}

	}

	public void feedBasePremiumFromUWPortal(String basepremium, String notes) throws Exception {

		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(uw_portal_create_quote_button);
//		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		SeleniumFunc.clearTextBox(By.xpath(uw_portal_dialog_base_premium_textbox));
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(uw_portal_dialog_base_premium_textbox, basepremium);
		SeleniumFunc.xpath_GenericMethod_Click(uw_portal_dialog_notes);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(uw_portal_dialog_notes, notes);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(uw_portal_dialog_submit_button);

	}

	public void UpdateBasePremiumFromUWPortal(String basePremium, String notes) throws Exception {

		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Clear(uw_portal_dialog_base_premium_textbox);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(uw_portal_dialog_base_premium_textbox, basePremium);
		Thread.sleep(200);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(uw_portal_manual_prricing_reason, "Customer requested");
		Thread.sleep(500);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(uw_portal_dialog_notes, notes);
		SeleniumFunc.xpath_GenericMethod_Click(uw_portal_dialog_submit_button);

	}


	public void UpdateCustomPremiumFromUWPortal(String basepremium, String notes) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Clear(txtcustombasrPrium);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtcustombasrPrium, basepremium);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtcustomUWnote, notes);
		SeleniumFunc.xpath_GenericMethod_Click(uw_portal_dialog_submit_button);
	}

	public void updateProfessionOnFinalizeQuoate(String profession,String isNeedtoBeUpdateOnlyInPolicy) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(Uw_clientSpecific_Update_btn);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(Uw_Profession_Update,profession);
		if(isNeedtoBeUpdateOnlyInPolicy.equals("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(Uw_Profession_Update_Org);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_Submit);
		SeleniumFunc.waitFor(8);
	}


	public void feedBasePremiumFromUWPortal1(String basepremium, String notes) throws Exception {
		switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(uw_portal_create_quote_button);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Clear(uw_portal_dialog_base_premium_textbox);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(uw_portal_dialog_base_premium_textbox, basepremium);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(uw_portal_dialog_notes, notes);
		SeleniumFunc.xpath_GenericMethod_Click(uw_portal_dialog_submit_button);

	}

	public void calculateAndValidatePremiumDetailsForLiability(int baseprem, float floatgst, float floatstamp)
			throws Exception {

		double gst = baseprem * floatgst;
		double basepremiumincludinggst = baseprem + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(baseprem, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
		String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
		String totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);

		basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
		gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
		totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
		stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

		SeleniumFunc.writeToAssertionFile(
				"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen,
				(basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + gstscreen,
				(gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen,
				(stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
						+ totalpremiumscreen,
				(totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));
	}

	public void calculateAndValidateEndorsementPremiumDetailsForLiability(int baseprem, int updatedbaseprem,
			float floatgst, float floatstamp) throws Exception {
		int baseprem1 = updatedbaseprem - baseprem;
		double gst = baseprem1 * floatgst;
		double basepremiumincludinggst = baseprem1 + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(baseprem1, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
		String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
		String totalpremiumscreen;
		try{
			totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);
		}catch(Exception e){
			totalpremiumscreen = xpath_Genericmethod_getElementText(endorsement_premium);
		}
		basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
		gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
		totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
		stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

		SeleniumFunc.writeToAssertionFile(
				"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen,
				(basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + gstscreen,
				(gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen,
				(stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
						+ totalpremiumscreen,
				(totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));
	}

	public void calculateAndValidatePremiumDetailsForMVUW(float baseprem, float floatgst, float floatstamp, int days)
			throws Exception {

		baseprem = baseprem * days / 365;
		double gst = baseprem * floatgst;
		double basepremiumincludinggst = baseprem + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(baseprem, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
		String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
		String totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);

		basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
		gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
		totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
		stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

		SeleniumFunc.writeToAssertionFile(
				"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen,
				(basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + gstscreen,
				(gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen,
				(stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
						+ totalpremiumscreen,
				(totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));
	}

	public void verifyUWCasePaymentStatus(String casestatus) throws Exception {
//
//		WebDriverWait wait = new WebDriverWait(driver, 30);
//		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(uw_case_status_pending_payment))));
//
		String casestatusscreen;
		try{
			casestatusscreen = xpath_Genericmethod_getElementText(uw_case_status_pending_payment);
		}catch(Exception e){
			casestatusscreen = xpath_Genericmethod_getElementText(uw_case_status_accept_quote);
		}
		// String casestatusscreen =
		// xpath_Genericmethod_getElementText(uw_case_status_pending_payment);

		System.out.println(casestatusscreen + "************************");
		System.out.println(casestatus + "***********************");
		SeleniumFunc.writeToAssertionFile(
				"Expected payment case status : " + casestatus + " , Actual status : " + casestatusscreen,
				(casestatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatus.equals(casestatusscreen));

	}

	public void verifyUWCaseManualResolveStatus(String casestatus) throws Exception {

		String casestatusscreen = xpath_Genericmethod_getElementText(resolved_manually_finance);
		System.out.println(casestatusscreen + "************************");
		System.out.println(casestatus + "***********************");
		SeleniumFunc.writeToAssertionFile(
				"Expected payment case status : " + casestatus + " , Actual status : " + casestatusscreen,
				(casestatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatus.equals(casestatusscreen));

	}

	public void verifyUWCaseEndPaymentStatus(String casestatus) throws Exception {

		String casestatusscreen = xpath_Genericmethod_getElementText(endorsement_pending_payment);
		System.out.println(casestatusscreen + "***********************");
		System.out.println(casestatus + "***********************");
		SeleniumFunc.writeToAssertionFile(
				"Expected payment case status : " + casestatus + " , Actual status : " + casestatusscreen,
				(casestatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatus.equals(casestatusscreen));

	}

	public void enterValueForPropertyMoreThanTenMillion(String propertyval, String singlelocation,
			String locationsecurity, String construction, String additionaldetails, String fireprotection)
			throws Exception {
		if (singlelocation.equalsIgnoreCase("No")) {
			SeleniumFunc.xpath_GenericMethod_Click(prop_over_ten_million_no);
		} else {
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(prop_over_ten_million_yes);
			SeleniumFunc.xpath_GenericMethod_Click(prop_over_ten_million_yes);
			SeleniumFunc.waitFor(1);
			switch (locationsecurity) {

			case "B2B monitored":
				SeleniumFunc.xpath_GenericMethod_Click(prop_security_B2B_Monitored);
				break;
			case "Local alarms":
				SeleniumFunc.xpath_GenericMethod_Click(prop_security_local_alarms);
				break;
			case "Security camera":
				SeleniumFunc.xpath_GenericMethod_Click(prop_security_security_camera);
				break;
			case "Locks windows":
				SeleniumFunc.xpath_GenericMethod_Click(prop_security_lock_windows);
				break;
			case "Bollards":
				SeleniumFunc.xpath_GenericMethod_Click(prop_security_bollards);
				break;
			}

			SeleniumFunc.waitFor(1);
			switchToDefaultContent();
			if (construction.equalsIgnoreCase("brick")) {
				SeleniumFunc.xpath_GenericMethod_Click(brick_construction);
			} else {
				SeleniumFunc.xpath_GenericMethod_Click(mixed_construction);
				SeleniumFunc.waitFor(1);

				/*
				 * switch (additionaldetails) {
				 *
				 * case "Timber": SeleniumFunc.xpath_GenericMethod_Click(construction_timber);
				 * break; case "Asbestos":
				 * SeleniumFunc.xpath_GenericMethod_Click(construction_asbestos); break; case
				 * "Cladding": SeleniumFunc.xpath_GenericMethod_Click(construction_cladding);
				 * break; case "EPS Panelling":
				 * SeleniumFunc.xpath_GenericMethod_Click(construction_eps_panelling); break;
				 *
				 * }
				 */

			}
			SeleniumFunc.waitFor(1);
			switchToDefaultContent();
			switch (fireprotection) {

			case "Sprinklers":
				SeleniumFunc.xpath_GenericMethod_scrollIntoView(fire_protection_sprinklers);
				SeleniumFunc.xpath_GenericMethod_Click(fire_protection_sprinklers);
				break;
			case "Fire Detectors":
				SeleniumFunc.xpath_GenericMethod_Click(fire_protection_fire_detectors);
				break;
			case "Extinguishers":
				SeleniumFunc.xpath_GenericMethod_Click(fire_protection_extinguishers);
				break;

			}

		}

	}

	public void changeSTPConditionAsPerSelectedAction(String action, String updatedstudents) throws Exception {

		if (action.equalsIgnoreCase("cancel")) {
			SeleniumFunc.xpath_GenericMethod_Click(actions_button);
			SeleniumFunc.xpath_GenericMethod_Click(actions_cancel);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(cancel_reason,
					"Premium");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(cancel_comments, "Test");
			SeleniumFunc.xpath_GenericMethod_Click(submit_button_uw_loss_select_screen);
		} else if (action.equalsIgnoreCase("update")) {
			SeleniumFunc.xpath_GenericMethod_Click(action_dec);
			SeleniumFunc.xpath_GenericMethod_Click(actions_update);
			SeleniumFunc.waitFor(1);
			SeleniumFunc.xpath_GenericMethod_Clear(number_of_students);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(number_of_students, updatedstudents);
			SeleniumFunc.waitFor(1);
			// SeleniumFunc.switchToDefaultContent();
			SeleniumFunc.executeScript("arguments[0].click();", driver.findElement(By.xpath(uw_submit)));
			// SeleniumFunc.xpath_GenericMethod_Click(uw_submit);

		} else if (action.equalsIgnoreCase("discuss call us now")) {
			SeleniumFunc.xpath_GenericMethod_Click(action_dec);
			SeleniumFunc.xpath_GenericMethod_Click(actions_discuss);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(call_us_now_radio);
			SeleniumFunc.waitFor(1);
			SeleniumFunc.executeScript("arguments[0].click();", driver.findElement(By.xpath(uw_submit)));
		}

		else if (action.equalsIgnoreCase("discuss call back")) {
			SeleniumFunc.xpath_GenericMethod_Click(action_dec);
			SeleniumFunc.xpath_GenericMethod_Click(actions_discuss);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(call_back_radio);
			SeleniumFunc.waitFor(1);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(call_back_notes, "notes");
			SeleniumFunc.waitFor(1);
			SeleniumFunc.executeScript("arguments[0].click();", driver.findElement(By.xpath(uw_submit)));
		} else if (action.equalsIgnoreCase("Withdraw application")) {
			// SeleniumFunc.xpath_GenericMethod_Click(actions_button_dec_page);
			SeleniumFunc.xpath_GenericMethod_Click(action_dec);
			SeleniumFunc.xpath_GenericMethod_Click(withdraw);
			// SeleniumFunc.xpath_GenericMethod_Click(actions_withdraw);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(withdraw_reason, 2);
			SeleniumFunc.waitFor(1);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(withdraw_comments, "Withdrawn");
			SeleniumFunc.xpath_GenericMethod_Click(btn_submit);
			// SeleniumFunc.xpath_GenericMethod_Click(finish_button);
		} else {
			System.out.println("No valid action provided");
		}
	}

	public void verifyUWCaseCancelledStatus(String status) throws Exception {
		SeleniumFunc.waitFor(1);
		String casestatusscreen = xpath_Genericmethod_getElementText(case_status_withdrawn);
		// String casestatusscreen =
		// xpath_Genericmethod_getElementText(uw_case_status_resolved_cancelled);
		System.out.println(casestatusscreen + "************************");
		System.out.println(status + "***********************");
		SeleniumFunc.writeToAssertionFile(
				"Expected cancelled case status : " + status + " , Actual status : " + casestatusscreen,
				(status.equals(casestatusscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(status.equals(casestatusscreen));

	}

	public void routeToMyOpenActions() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(left_toggle);
		SeleniumFunc.xpath_GenericMethod_Click(my_open_actions);
		Assert.assertTrue(SeleniumFunc.verifyElementPresent(
				"(//table[@summary='My open actions' and @data-test-id='201803070515400998297-layout'])[1]"));

	}

	public void enterNumberOfParkingSlots(String parking) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Clear(number_of_parking_spots);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(number_of_parking_spots, parking);

	}

	public void validateInfo(String students) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(info_section);
		SeleniumFunc.waitFor(1);
		String infoscreen = xpath_Genericmethod_getElementText(info_number_of_students);
		infoscreen = infoscreen.replaceAll(",", "");
		SeleniumFunc.writeToAssertionFile(
				"Expected information in declaration screen : " + students
						+ " , Actual information in declaration screen : " + infoscreen,
				(students.equals(infoscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(students.equals(infoscreen));

	}

	public void validatePropValue(String propvalue) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(info_section);
		SeleniumFunc.waitFor(1);
		String infoscreen = xpath_Genericmethod_getElementText(info_property_value);
		SeleniumFunc.writeToAssertionFile(
				"Expected information in declaration screen : " + propvalue
						+ " , Actual information in declaration screen : " + infoscreen,
				(propvalue.equals(infoscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(propvalue.equals(infoscreen));

	}

	public void attachFileInRiskPage(String filepath) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(attachment_button);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(attach_file_button);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.uploadFile(filepath);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(attach_button);
		SeleniumFunc.waitFor(2);

	}

	public void enterValueInMVRiskPage(String lightvehicleflag, String busflag, String trailerflag, String otherflag,
			String typeofvehicle, String manufactureyear, String make, String model, String regno,
			String currentmarketvalue, int numberofvehicles) throws Exception {
		if (lightvehicleflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_light_vehicle_tab);
			SeleniumFunc.waitFor(1);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_Click(lightvehicle_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(lighvehicle_manufacture_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(lighvehicle_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(lighvehicle_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(lighvehicle_regno, regno);
			}
		}
		if (busflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_bus_tab);
			SeleniumFunc.waitFor(1);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_Click(bus_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_manufacture_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_regno, regno);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_current_market_value, currentmarketvalue);
			}
		}
		if (trailerflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_trailer_tab);
			SeleniumFunc.waitFor(1);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_Click(trailer_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(trailer_manufacture_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(trailer_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(trailer_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(trailer_regno, regno);
			}
		}
		if (otherflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_other_tab);
			SeleniumFunc.waitFor(1);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_Click(other_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_vehicle_type, typeofvehicle);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_manufacture_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_regno, regno);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_current_market_value, currentmarketvalue);
			}
		}
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(mv_claim_or_loss_exp_ques_no);
		SeleniumFunc.xpath_GenericMethod_Click(mv_use_of_vehicle_ques_no);
		SeleniumFunc.xpath_GenericMethod_Click(mv_vehicles_on_hire_ques_no);

	}

	public void enterValueInMVRiskPageMLP2(String lightvehicleflag, String busflag, String trailerflag,
			String machineryflag, String otherflag, String typeofvehicle, String manufactureyear, String make,
			String model, String regno, String currentmarketvalue, int numberofvehicles) throws Exception {
		if (lightvehicleflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_light_vehicle_tab);
			SeleniumFunc.waitFor(1);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_Click(lightvehicle_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(lighvehicle_manufacture_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(lighvehicle_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(lighvehicle_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(lighvehicle_regno, regno);
			}
		}
		if (busflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_bus_tab);
			SeleniumFunc.waitFor(1);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_Click(bus_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_manufacture_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_regno, regno);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_current_market_value, currentmarketvalue);
			}
		}
		if (trailerflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_trailer_tab);
			SeleniumFunc.waitFor(1);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_Click(trailer_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(trailer_manufacture_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(trailer_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(trailer_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(trailer_regno, regno);
			}
		}

		if (machineryflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_machinery_tab);
			SeleniumFunc.waitFor(3);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_scrollIntoView(machinery_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Click(machinery_add_vehicle_button);
				// SeleniumFunc.xpath_GenericMethod_Sendkeys(machinery_year, typeofvehicle);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(machinery_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(machinery_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(machinery_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(machinery_regn_no, regno);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(machinery_current_market_value, currentmarketvalue);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(machinery_use_of_vehicle, typeofvehicle);
			}
		}

		if (otherflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_other_tab);
			SeleniumFunc.waitFor(1);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_Click(other_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_vehicle_type, typeofvehicle);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_manufacture_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_regno, regno);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_current_market_value, currentmarketvalue);
			}
		}
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(mv_claim_or_loss_exp_ques_no);
		SeleniumFunc.xpath_GenericMethod_Click(mv_claim_or_loss_exp_ques_no);
		SeleniumFunc.xpath_GenericMethod_Click(mv_use_of_vehicle_ques_no);
		SeleniumFunc.xpath_GenericMethod_Click(mv_vehicles_on_hire_ques_no);

	}

	public void enterValueInMVRiskPageEndorsement(String lightvehicleflag, String busflag, String trailerflag,
			String otherflag, String typeofvehicle, String manufactureyear, String make, String model, String regno,
			String currentmarketvalue, int numberofvehicles) throws Exception {
		if (lightvehicleflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_light_vehicle_tab);
			SeleniumFunc.waitFor(1);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_Click(lightvehicle_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(lighvehicle_manufacture_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(lighvehicle_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(lighvehicle_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(lighvehicle_regno, regno);
			}
		}
		if (busflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_bus_tab);
			SeleniumFunc.waitFor(1);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_Click(bus_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_manufacture_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_regno, regno);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(bus_current_market_value, currentmarketvalue);
			}
		}
		if (trailerflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_trailer_tab);
			SeleniumFunc.waitFor(1);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_Click(trailer_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(trailer_manufacture_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(trailer_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(trailer_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(trailer_regno, regno);
			}
		}
		if (otherflag.equalsIgnoreCase("true")) {
			SeleniumFunc.xpath_GenericMethod_Click(mv_other_tab);
			SeleniumFunc.waitFor(1);
			for (int vehiclecounter = 0; vehiclecounter < numberofvehicles; vehiclecounter++) {
				SeleniumFunc.xpath_GenericMethod_Click(other_add_vehicle_button);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_vehicle_type, typeofvehicle);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_manufacture_year, manufactureyear);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_make, make);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_model, model);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_regno, regno);
				SeleniumFunc.xpath_GenericMethod_Sendkeys(other_current_market_value, currentmarketvalue);
			}
		}
		SeleniumFunc.waitFor(1);
		// SeleniumFunc.xpath_GenericMethod_Click(mv_claim_or_loss_exp_ques_no);
		SeleniumFunc.xpath_GenericMethod_Click(mv_use_of_vehicle_ques_no);
		SeleniumFunc.xpath_GenericMethod_Click(mv_vehicles_on_hire_ques_no);

	}

	public void selectMotorVehicle() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(uw_mvf_button);
	}

	public void manualProcessCase() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click("//button[@data-test-id='20141210043348063930294']");
		} catch (Exception e) {
		}

		SeleniumFunc.xpath_GenericMethod_Click(queue_dropdown);
		SeleniumFunc.waitForElementToClikable(By.xpath(finance_pending),35);
		SeleniumFunc.xpath_GenericMethod_Click(finance_pending);
//		SeleniumFunc.waitFor(90);
//		SeleniumFunc.waitForElementToClikable(By.xpath(financecaseid),600);
//		SeleniumFunc.xpath_GenericMethod_Click(financecaseid);
		// SeleniumFunc.xpath_GenericMethod_HoverOnDemoScreenPops(finance_caseid_filter);
//		SeleniumFunc.waitFor(4);

//		SeleniumFunc.waitForElementToClikable(By.xpath(financecaseid_filter),120);

		//Regression/Regression_MLP1/Start a new policy application for GPA with no deviation and then manually process payment
//		SeleniumFunc.waitForElement(By.xpath(financeCaseId),10);
		SeleniumFunc.waitFor(10);
		SeleniumFunc.executeScript("arguments[0].click()", SeleniumFunc.getElement(By.xpath(financeCaseId)));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.executeScript("arguments[0].click()", SeleniumFunc.getElement(By.xpath(finance_applyfilter)));
//		try{
//			SeleniumFunc.waitFor(2);
//			SeleniumFunc.xpath_GenericMethod_Click(financeCaseId);
//			SeleniumFunc.waitFor(2);
//			SeleniumFunc.xpath_GenericMethod_Click(financeCaseIdFilter);
//		}catch(Exception e){
//			SeleniumFunc.xpath_GenericMethod_Click(financecaseid_filter);
//			SeleniumFunc.waitFor(2);
//			SeleniumFunc.xpath_GenericMethod_Click(finance_applyfilter);
//		}
//		SeleniumFunc.xpath_GenericMethod_Click(finance_applyfilter);
//		SeleniumFunc.waitFor(20);
//		SeleniumFunc.waitFor(10);
//		SeleniumFunc.xpath_GenericMethod_Click(financeCaseIdFilter);
//		SeleniumFunc.waitFor(4);
		SeleniumFunc.waitForElementToClikable(By.xpath(finance_search_text),120);
//		SeleniumFunc.waitFor(10);
//		testData.setCaseId("GPA-30654");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(finance_search_text, testData.getCaseId());
		SeleniumFunc.xpath_GenericMethod_Click(finance_apply);
//		SeleniumFunc.waitFor(8);
		// SeleniumFunc.xpath_GenericMethod_Click(finance_case_link);
		// SeleniumFunc.switchToDefaultContent();
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(finance_reason_manual_processing,
		// "approved");
//		SeleniumFunc.xpath_GenericMethod_Click(urgency);
		SeleniumFunc.waitForElementToClikable(By.xpath(case_id),120);
		SeleniumFunc.xpath_GenericMethod_Click(case_id);
//		SeleniumFunc.executeScript("arguments[0].click()", SeleniumFunc.getElement(By.xpath(case_id)));
//		Thread.sleep(2000);
		// SeleniumFunc.xpath_GenericMethod_Click(case_id);
//		try {
//			SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(case_id)));
//		} catch (Exception e) {
//
//		}
//		Thread.sleep(8000);
//		SeleniumFunc.xpath_GenericMethod_Click(finance_case_link);
//		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Sendkeys(finance_reason_manual_processing, "approved");
		SeleniumFunc.xpath_GenericMethod_Click(finance_payment_date_link);
		SeleniumFunc.xpath_GenericMethod_Click(current_date);
		SeleniumFunc.xpath_GenericMethod_Click(uw_submit);

//		SeleniumFunc.xpath_GenericMethod_Sendkeys(financeReasonManualProcessing, "approved");
//		SeleniumFunc.xpath_GenericMethod_Click(financePaymentDateLink);
//		SeleniumFunc.xpath_GenericMethod_Click(current_date);
//		SeleniumFunc.xpath_GenericMethod_Click(financeSubmitButton);

	}

	public void validateMVPricing(String lightvehicleflag, String busflag, String trailerflag, String otherflag,
			double pricelightvehicle, double pricebusunder50, double pricebusover50, double pricetrailer,
			int integernumberofvehicles, double floatgst, double floatstamp, double currentmarketvalue, int days)
			throws Exception {

		double baseprem = 0.0;

		if (lightvehicleflag.equalsIgnoreCase("true")) {
			baseprem = baseprem + (integernumberofvehicles * pricelightvehicle);
		}

		if (busflag.equalsIgnoreCase("true")) {
			if (currentmarketvalue < 50000)
				baseprem = baseprem + (integernumberofvehicles * pricebusunder50);
			else
				baseprem = baseprem + (integernumberofvehicles * pricebusover50);
		}

		if (trailerflag.equalsIgnoreCase("true")) {
			baseprem = baseprem + (integernumberofvehicles * pricetrailer);
		}
		baseprem = baseprem * days / 365;
		double gst = baseprem * floatgst;
		double basepremiumincludinggst = baseprem + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(baseprem, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
		String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
		String totalpremiumscreen;
		try{
			totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);
		}catch(Exception e){
			totalpremiumscreen = xpath_Genericmethod_getElementText(endorsement_premium);
		}

		basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
		gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
		totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
		stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

		SeleniumFunc.writeToAssertionFile(
				"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen,
				(basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + gstscreen,
				(gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen,
				(stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
						+ totalpremiumscreen,
				(totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));
	}


	public void validateMVPricingMachinery(String lightvehicleflag, String busflag, String trailerflag,
			String machineryflag, String otherflag, double pricelightvehicle, double pricebusunder50,
			double pricebusover50, double pricetrailer, double pricemachinery, int integernumberofvehicles,
			double floatgst, double floatstamp, double currentmarketvalue, int days) throws Exception {

		double baseprem = 0.0;

		if (lightvehicleflag.equalsIgnoreCase("true")) {
			baseprem = baseprem + (integernumberofvehicles * pricelightvehicle);
		}

		if (busflag.equalsIgnoreCase("true")) {
			if (currentmarketvalue < 50000)
				baseprem = baseprem + (integernumberofvehicles * pricebusunder50);
			else
				baseprem = baseprem + (integernumberofvehicles * pricebusover50);
		}
		if (machineryflag.equalsIgnoreCase("true")) {

			baseprem = baseprem + (integernumberofvehicles * pricemachinery);

		}

		if (trailerflag.equalsIgnoreCase("true")) {
			baseprem = baseprem + (integernumberofvehicles * pricetrailer);
		}
		baseprem = baseprem * days / 365;
		double gst = baseprem * floatgst;
		double basepremiumincludinggst = baseprem + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(baseprem, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
		String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
		String totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);

		basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
		gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
		totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
		stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

		SeleniumFunc.writeToAssertionFile(
				"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen,
				(basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + gstscreen,
				(gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen,
				(stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
						+ totalpremiumscreen,
				(totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));
	}

	public void clickUpdateBasePremButtonFinalizeQuote() throws Exception {

		SeleniumFunc.waitFor(3);
//           JavascriptExecutor executor = (JavascriptExecutor) driver;
//           executor.executeScript("arguments[0].click();", driver.update_basepremium_uwportal);
		SeleniumFunc.xpath_GenericMethod_Click(update_basepremium_uwportal);
		// SeleniumFunc.executeScript("arguments[0].click();",
		// driver.findElement(By.xpath(update_basepremium_uwportal)));

	}

	public void savePolicyNumberRiskAdviosr() throws Exception {
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(Quotes_tab_Riskadviosr);
		testData.setPolicynumber(SeleniumFunc.getElementText(policynumbertext_riskadvisor));
		System.out.println(testData.getPolicynumber());

	}

	public void validatePremium(String policyaction,String annualPremium,String isProrataEnabled) throws Exception {
		double annaulPremim,baseprem,gst,basepremwithgst,stampduty,totalpremium,floatgst;
//		testData.setData("MultiYear","No");
//		policyaction="Cancellation";
		String expectedtotalexpectedpremium;
		String prefixtext="$";
		if(policyaction.equalsIgnoreCase("Cancellation")) {prefixtext="-$";}
		annaulPremim = Double.parseDouble(annualPremium);
		if(isProrataEnabled.contains("Yes") && testData.getData("MultiYear").equalsIgnoreCase("No"))
			baseprem = annaulPremim * (testData.getPolicyduration() + 1) / 365;
		else if(isProrataEnabled.contains("Yes") && testData.getData("MultiYear").equalsIgnoreCase("Yes"))
			baseprem = annaulPremim * (testData.getPolicyduration()) / 365;
		else
			baseprem=annaulPremim;
		floatgst = 10;
		gst = (baseprem * floatgst) / 100;
		basepremwithgst = baseprem + gst;
		stampduty = (basepremwithgst * floatgst) / 100;
		totalpremium = baseprem + stampduty + gst;
		String expectedbasepremiuminm = prefixtext+roundWithString(baseprem, 2);
		String expectedgstin = prefixtext+roundWithString(gst, 2);
		String expectedstampduty = prefixtext+roundWithString(stampduty, 2);
		String actualbasepremium = SeleniumFunc.removeCommaInString(xpath_Genericmethod_getElementText(base_premium));
		String actualgst= SeleniumFunc.removeCommaInString(xpath_Genericmethod_getElementText(gst_screen));
		String actualstampduty =SeleniumFunc.removeCommaInString( xpath_Genericmethod_getElementText(stamp_duty));
		String actualtotalpremium = SeleniumFunc.removeCommaInString(xpath_Genericmethod_getElementText(Return_premium));

		if(testData.getData("Product").equalsIgnoreCase("Fine Art NGV")) {
			double premiumAdded = round(baseprem, 2) + round(gst, 2) + round(stampduty, 2);
			expectedtotalexpectedpremium = prefixtext + premiumAdded;
		}
		else
			expectedtotalexpectedpremium=prefixtext+roundWithString(totalpremium,2);
		SoftAssert softassert=new SoftAssert();
		softassert.assertEquals(actualtotalpremium, expectedtotalexpectedpremium);
		softassert.assertEquals(actualbasepremium,expectedbasepremiuminm);
		softassert.assertEquals(actualgst,expectedgstin);
		softassert.assertEquals(actualstampduty,expectedstampduty);
		softassert.assertEquals(actualtotalpremium,expectedtotalexpectedpremium);
//		softassert.assertAll();
	}

	public void validateTotalPremium(String policyaction,String annualPremium,String isProrataEnabled) throws Exception {
		double annaulPremim,baseprem,gst,basepremwithgst,stampduty,totalpremium,floatgst;
//		testData.setData("MultiYear","No");
//		policyaction="Cancellation";
		String expectedtotalexpectedpremium;
		String prefixtext="$";
		if(policyaction.equalsIgnoreCase("Cancellation")) {prefixtext="-$";}
		annaulPremim = Double.parseDouble(annualPremium);
		if(isProrataEnabled.contains("Yes") && testData.getData("MultiYear").equalsIgnoreCase("No"))
			baseprem = annaulPremim * (testData.getPolicyduration() + 1) / 365;
		else if(isProrataEnabled.contains("Yes") && testData.getData("MultiYear").equalsIgnoreCase("Yes"))
			baseprem = annaulPremim * (testData.getPolicyduration()) / 365;
		else
			baseprem=annaulPremim;
		floatgst = 10;
		gst = (baseprem * floatgst) / 100;
		basepremwithgst = baseprem + gst;
		stampduty = (basepremwithgst * floatgst) / 100;
		totalpremium = baseprem + stampduty + gst;
		String expectedbasepremiuminm = prefixtext+roundWithString(baseprem, 2);
		String expectedgstin = prefixtext+roundWithString(gst, 2);
		String expectedstampduty = prefixtext+roundWithString(stampduty, 2);
		String actualbasepremium = SeleniumFunc.removeCommaInString(xpath_Genericmethod_getElementText(base_premium));
		String actualgst= SeleniumFunc.removeCommaInString(xpath_Genericmethod_getElementText(gst_screen));
		String actualstampduty =SeleniumFunc.removeCommaInString( xpath_Genericmethod_getElementText(stamp_duty));
		String actualtotalpremium = SeleniumFunc.removeCommaInString(xpath_Genericmethod_getElementText(total_premium));

		if(testData.getData("Product").equalsIgnoreCase("Fine Art NGV")) {
			double premiumAdded = round(baseprem, 2) + round(gst, 2) + round(stampduty, 2);
			expectedtotalexpectedpremium = prefixtext + premiumAdded;
		}
		else
			expectedtotalexpectedpremium=prefixtext+roundWithString(totalpremium,2);
		SoftAssert softassert=new SoftAssert();
		softassert.assertEquals(actualtotalpremium, expectedtotalexpectedpremium);
		softassert.assertEquals(actualbasepremium,expectedbasepremiuminm);
		softassert.assertEquals(actualgst,expectedgstin);
		softassert.assertEquals(actualstampduty,expectedstampduty);
		softassert.assertEquals(actualtotalpremium,expectedtotalexpectedpremium);
//		softassert.assertAll();
	}

	public void clickUpdateRiskInformationButtonFinalizeQuote() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(update_riskinformation_uwportal);

	}
	public void clickUpdatePolicy() throws Exception {
		SeleniumFunc.waitFor(30);
//		testData.setPolicynumber("V031407");
		System.out.println("The policy number is: "+testData.getPolicynumber());
		SeleniumFunc.waitForElementToClikable(By.xpath("//span[text()='"+testData.getPolicynumber()+"']/ancestor::div[5]/div[4]/div/div[3]/span/button"),120);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+testData.getPolicynumber()+"']/ancestor::div[5]/div[4]/div/div[3]/span/button");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(update_policy);
	}

	public void enterEndorsementDetails(String effectiveDate,String updateInfo) throws Exception {
		SeleniumFunc.waitInMilliSeconds(4);
		clickContinueButton();
		SeleniumFunc.waitInMilliSeconds(4);
		SeleniumFunc.SelectDateWithDropDown(Endorsement_EffectiveDate,effectiveDate);
		SeleniumFunc.name_GenericMethod_Sendkeys(Endorsment_policyChangeInfo,updateInfo);
		clickContinueButton();
		SeleniumFunc.waitInMilliSeconds(4);
		checkDeclaration();
		clickFinish();
	}

	public void enterTheEndorsementDetails() throws Exception {
		SeleniumFunc.waitInMilliSeconds(4);
		clickContinueButton();
		SeleniumFunc.waitInMilliSeconds(4);
		SeleniumFunc.xpath_GenericMethod_Click(effDateImg);
		SeleniumFunc.actionClick(By.xpath(current_date));
//		SeleniumFunc.SelectDateWithDropDown(Endorsement_EffectiveDate,effectiveDate);
//		SeleniumFunc.name_GenericMethod_Sendkeys(Endorsment_policyChangeInfo,updateInfo);
		SeleniumFunc.xpath_GenericMethod_Click(Endorsement_selectOption);
		clickContinueButton();
		SeleniumFunc.waitInMilliSeconds(4);
		checkDeclaration();
		clickFinish();
	}


	public void cancelPolicy(String iscancelfullterm,String cancelReason,String effectivedate) throws Exception {
		String effectiveDate,policyexpirydate;
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		if(iscancelfullterm.equalsIgnoreCase("Yes")){
			effectiveDate=SeleniumFunc.getElementText(getPolicyEffectdatetext);
			SeleniumFunc.xpath_GenericMethod_Click(fullterm_Cancel_Yes);
		}
		else {
			effectiveDate=effectivedate;
			SeleniumFunc.xpath_GenericMethod_Click(fullterm_Cancel_No);
			SeleniumFunc.SelectDateWithDropDown(cancellation_effectiveDate,effectivedate);
		}
		policyexpirydate=SeleniumFunc.getElementText(policyExpiryDate);
		testData.setPolicyduration(SeleniumFunc.findTheDateDifference(effectiveDate,policyexpirydate));
		SeleniumFunc.xpath_SelectFromDropdownUsingVisibleText(cancel_reason, cancelReason);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		SeleniumFunc.xpath_GenericMethod_Click(cancel_declaration);
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
	}

	public void cancelBundlePolicy(String iscancelfullterm,String cancelReason) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		if(iscancelfullterm.equalsIgnoreCase(iscancelfullterm)){
			SeleniumFunc.xpath_GenericMethod_Click(fullterm_Cancel_Yes);
		}
		else {
			SeleniumFunc.xpath_GenericMethod_Click(fullterm_Cancel_No);
		}
		SeleniumFunc.xpath_SelectFromDropdownUsingVisibleText(cancel_reason, cancelReason);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		SeleniumFunc.xpath_GenericMethod_Click(cancel_declaration);
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
	}

	public void cancelPolicyMoreActions() throws Exception {

//		TestBase.currentPolicyNumber="V014615";
		SeleniumFunc.waitFor(30);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+testData.getPolicynumber()+"']/ancestor::div[5]/div[4]/div/div[3]/span/button");
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(cancel_policy);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		SeleniumFunc.xpath_GenericMethod_Click(fullterm_Cancel_Yes);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(cancel_reason, 1);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		SeleniumFunc.xpath_GenericMethod_Click(cancel_declaration);
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
	}



	public void cancelPolicyByClickingMoreActions(String iscancelfullterm,String cancelReason,String effectivedate) throws Exception {
		SeleniumFunc.waitFor(5);
//		testData.setPolicynumber("V027017");
//		String yes = "C03549";
		System.out.println("The policy number is****** "+testData.getPolicynumber());
		SeleniumFunc.waitFor(10);
//		SeleniumFunc.waitForElementToClikable(By.xpath("//span[text()='"+testData.getPolicynumber()+"']/ancestor::div[5]/div[4]/div/div[3]/span/button"),120);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+testData.getPolicynumber()+"']/ancestor::div[5]/div[4]/div/div[3]/span/button");
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(cancel_policy);
		cancelPolicy(iscancelfullterm,cancelReason,effectivedate);
	}



	public void iSelectTheOrganisation(String name) throws Exception {

//		String currentOrgName = SeleniumFunc.getElementText(lblOrgName);
//		if (!currentOrgName.equalsIgnoreCase("Client portal - " + name)) {
			SeleniumFunc.xpath_GenericMethod_Sendkeys(Switchorg, name);
//		SeleniumFunc.xpath_SelectFromDropdownUsingVisibleText(Switchorg,name);
			System.out.println("Selected Organisation is : "+name);
			SeleniumFunc.waitFor(20);
//		}
	}

	public void selectTheOrganizationBundle() throws Exception {
//		testData.setOrgname("Csoes-5199");
		SeleniumFunc.xpath_SelectFromDropdownUsingVisibleText(Switchorg,testData.getOrgname());
		SeleniumFunc.waitFor(10);
	}

	public void clickViewMoreTillLastPolicy() throws Exception {

		while(driver.findElement(By.xpath(viewMore)).isDisplayed()) {
				SeleniumFunc.waitInMilliSeconds(5);
				SeleniumFunc.xpath_GenericMethod_Click(viewMore);
				SeleniumFunc.waitInMilliSeconds(5);
		}
	}

	public ArrayList getAllThePolicyElement() throws Exception {
		SeleniumFunc.waitFor(2);
		clickViewMoreTillLastPolicy();
		SeleniumFunc.waitFor(1);
		List<WebElement> policies=getElements((PolicyDetails));
		ArrayList<String> policyText=new ArrayList<>();
		for(WebElement policy:policies){
			if(!policy.getText().equalsIgnoreCase("")){
				policyText.add(policy.getText().trim()); }
		}
		return policyText;
	}

	public List getDisabledButtons() throws Exception {
		return getElements("//button[contains(@title,'Click for more actions on policy') and @disabled]");
	}

	public boolean verifyApplyForPolicyNotDisplayed() throws Exception {
		return SeleniumFunc.verifyElementIsPresent(("//div[@aria-label='Apply for a policy']"));
	}


	public void calculateAndValidatePremiumDetailsForMLBN(String baseprem, String floatgst, String floatstamp,
			String days) throws Exception {

		float baseprem1 = 530;
		int intdays = Integer.parseInt(days);
		float floatgst1 = Float.parseFloat(floatgst);
		float floatstamp1 = Float.parseFloat(floatstamp);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

		Date requestStartDate = sdf.parse("10/02/2021");
		Date requestendDate = sdf.parse("30/06/2021");
		long diffInMillies0 = Math.abs(requestStartDate.getTime() - requestendDate.getTime());
		long days1 = TimeUnit.DAYS.convert(diffInMillies0, TimeUnit.MILLISECONDS);
		Date firstDate = sdf.parse("01/07/2020");
		Date secondDate = sdf.parse("30/06/2021");
		long diffInMillies1 = Math.abs(secondDate.getTime() - firstDate.getTime());
		long diff1 = TimeUnit.DAYS.convert(diffInMillies1, TimeUnit.MILLISECONDS);

		// assertEquals(6, diff);
		float baseprem2 = (baseprem1 / (diff1 + 1)) * (days1 + 1);

		// baseprem/(diff1+1)*days1/365;
		double gst = baseprem2 * floatgst1;
		double basepremiumincludinggst = baseprem2 + gst;
		double stampduty = basepremiumincludinggst * floatstamp1;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(baseprem2, 2));
	}
	public void selectJourney() throws Exception {
		 SeleniumFunc.waitFor(30);
		SeleniumFunc.xpath_GenericMethod_Click(uw_jrn_button);
	}

	public void enterNumberOfFTE(String FTE) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Clear(number_of_FTE);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(number_of_FTE, FTE);
	}

	public void enterPrevInsuranceDetails() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(jrn_risk_question_2_No);
	}

	public void validateJourneyInfo(String FTE) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(info_section);
		SeleniumFunc.waitFor(1);
		String infoscreen = xpath_Genericmethod_getElementText(lbl_FTE_number);
		infoscreen = infoscreen.replaceAll(",", "");
		SeleniumFunc
				.writeToAssertionFile(
						"Expected information in declaration screen : " + FTE
								+ " , Actual information in declaration screen : " + infoscreen,
						(FTE.equals(infoscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(FTE.equals(infoscreen));

	}

	public void claimEnterBusiness() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(zipcode, "2601");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Text_box, "Lowannaa");
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(find_button);
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(select_business_checkbox);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(select_contact_checkbox);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
	}

	public void validateJourneyFTEInfo(String fte) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(info_section);
		SeleniumFunc.waitFor(1);
		String infoscreen = xpath_Genericmethod_getElementText(lbl_FTE_number);
		infoscreen = infoscreen.replaceAll(",", "");
		SeleniumFunc
				.writeToAssertionFile(
						"Expected information in declaration screen : " + fte
								+ " , Actual information in declaration screen : " + infoscreen,
						(fte.equals(infoscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(fte.equals(infoscreen));
	}

	public void selectUpdatePolicyFromMoreActions() throws Exception {
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(medical_illnessdetails,"illness");
		SeleniumFunc.waitFor(10);
//		SeleniumFunc.xpath_GenericMethod_Click(more_actions);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+testData.getPolicynumber()+"']/ancestor::div[5]/div[4]/div/div[3]/span/button");
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(update_policy);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
	}

	public void fillUpdatePolicyInforFrorJoureyPolicy(String updatedFTE) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Clear(number_of_FTE);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(number_of_FTE, updatedFTE);

		SeleniumFunc.xpath_GenericMethod_Click(cancel_date);
		SeleniumFunc.xpath_GenericMethod_Click(current_date);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(cancel_reason, 1);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(cancel_declaration);
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
	}

	public int findDaysRemaining(String effectiveFromDate) {
		int days = 0;

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/MM/yyyy");
		String fromDate = null, toDate;

		if (effectiveFromDate.equalsIgnoreCase("currentdate")) {
			fromDate = getDate(0, "d/MM/yyyy");
		} else if (effectiveFromDate.equalsIgnoreCase("pastdate")) {
			fromDate = getDate(-1, "d/MM/yyyy");
		} else if (effectiveFromDate.equalsIgnoreCase("futuredate")) {
			fromDate = getDate(1, "d/MM/yyyy");
		}

		toDate = getPolicyYearEndDate();

		LocalDate date1 = LocalDate.parse(fromDate, dtf);
		LocalDate date2 = LocalDate.parse(toDate, dtf);
		long daysBetween = ChronoUnit.DAYS.between(date1, date2);
		System.out.println("Days: " + daysBetween);
		return days;
	}

	public void searchOrgAndSelectContact(String OrgName, String zipCode) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Text_box, OrgName);
		waitFor(2);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(zipcode, zipCode);
		SeleniumFunc.xpath_GenericMethod_Click(find_button);
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(select_business_checkbox);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(select_contact_checkbox);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
	}

	public void verifyMoreActionsIsDisabled() throws Exception {
		SeleniumFunc.waitFor(15);
		Assert.assertTrue(SeleniumFunc.getElements("//button[contains(@title,'Click for more actions on policy') and @disabled]").size()>0);
	}

	public void validateCaseStatus(String expectedCaseStatus) throws Exception {

		String actualCaseStatus = xpath_Genericmethod_getElementText(case_status);
		System.out.println("Expected:" + expectedCaseStatus);
		System.out.println("Actual: " + actualCaseStatus);
		Assert.assertEquals(actualCaseStatus,expectedCaseStatus);
	}

	public void enterPersonsDetailsForCategories(String volunteers, String boardMembers, String workExperienceStudents,
			String practicalPlacementStudents, String alliedHealthProfessionals, String overseasMedicalPractitioners,
			String other) throws Exception {
		if (volunteers != "") {
			SeleniumFunc.xpath_GenericMethod_Clear(numberOfVolunteersGPAPropNPlan);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(numberOfVolunteersGPAPropNPlan, volunteers);
		}
		if (boardMembers != "") {
			SeleniumFunc.xpath_GenericMethod_Clear(numberOfBoardMembersGPAPropNPlan);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(numberOfBoardMembersGPAPropNPlan, boardMembers);
		}
		if (workExperienceStudents != "") {
			SeleniumFunc.xpath_GenericMethod_Clear(numberOfWorkExpGPAPropNPlan);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(numberOfWorkExpGPAPropNPlan, workExperienceStudents);
		}
		if (practicalPlacementStudents != "") {
			SeleniumFunc.xpath_GenericMethod_Clear(numberOfPracPlacemntGPAPropNPlan);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(numberOfPracPlacemntGPAPropNPlan, practicalPlacementStudents);
		}
		if (alliedHealthProfessionals != "") {
			SeleniumFunc.xpath_GenericMethod_Clear(numberOfAllHlthProfGPAPropNPlan);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(numberOfAllHlthProfGPAPropNPlan, alliedHealthProfessionals);
		}
		if (overseasMedicalPractitioners != "") {
			SeleniumFunc.xpath_GenericMethod_Clear(numberOfOvrsMedPracGPAPropNPlan);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(numberOfOvrsMedPracGPAPropNPlan, overseasMedicalPractitioners);
		}
		if (other != "") {
			SeleniumFunc.xpath_GenericMethod_Clear(numberOfOthersGPAPropNPlan);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(numberOfOthersGPAPropNPlan, other);
		}
	}

	public void selectNoForPrevGPAPolicy() throws Exception {
		SeleniumFunc.xpath_GenericMethod_scrollIntoView(prevPolicyQuestion);
		SeleniumFunc.xpath_GenericMethod_Click(prevPolicyQuestionNoGPAPropNPlan);
	}

	public void calculateAndValidatePnPGPAPremiumDetails(long intVolunteers, int intdays, float floatmul,
			float floatgst, float floatstamp) throws Exception {
		double annualbasepremium = intVolunteers * floatmul;
		if (annualbasepremium < 250) {
			annualbasepremium = 250;
		}
		double basepremium = (annualbasepremium / 365) * intdays;
		double gst = basepremium * floatgst;
		double basepremiumincludinggst = basepremium + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(basepremium, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
		String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
		String totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);

		basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
		gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
		totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
		stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

		SeleniumFunc.writeToAssertionFile(
				"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen,
				(basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + gstscreen,
				(gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen,
				(stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
						+ totalpremiumscreen,
				(totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));
	}

	public void selectMotorLossofNoClaimBonus() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(uw_mlcb_button);
	}

	public void enterNumberofvechil(String vechil) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Sendkeys(Number_Vehicles_txt, vechil);
		SeleniumFunc.xpath_GenericMethod_Click(Time_Period_Vehicles_Dropdwn);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(Time_Period_Vehicles_Dropdwn,
				1);
		// SeleniumFunc.xpath_SelectFromDropdownUsingValue(Select_Daily, "Daily");

		SeleniumFunc.waitFor(2);
	}

	public void enterRevenuevalues(String previousval, String currentValue) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtPreviousyear, previousval);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtCurrentyear, currentValue);

	}

	public void selectOptiononDataprotecttion() throws Exception {
		// SeleniumFunc.xpath_GenericMethod_Click();

	}

	public void enterrequiredfilledforDataProtectionScreen(String byWhom, String provideDetails) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(radwrittendataprotectionyes);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(radareallemployessyes);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(datereview);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(todayLink);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtbywhom, byWhom);
		SeleniumFunc.xpath_GenericMethod_Click(raddoesorganisationdatayes);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(raddoesorganisationemployeeyes);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(radareemployessrequiredNo);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtbywhom, provideDetails);
		SeleniumFunc.waitFor(3);

	}

	public void enterRequiredDetaisinDataaccessandrecovery() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorganisationusefirewallNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(radDoestheOrganisationuseanti_virusNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(radDoestheOrganisationmonitor_no);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(radDoestheOrganisationhasphysicalsecurityNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(radDoestheOrganisationcollectNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(radistheaccessNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorgprocessNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorghaveencryptionNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorganisationhasmaiantainbackupNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(radinformationassetesNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorghasresponseplanNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(Is_Remote_User_authticated_No);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorgrequiresremoteplanNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorghaveanendoflifeNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorgimplememetnetworkNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(radMultifactorauthinticationNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(radDoestheorghavebusinesscontinuityNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(RaddoestheorgrequireNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Txtpercentagefield,"10");
		SeleniumFunc.xpath_GenericMethod_Click(raddailyfrequentlyNo);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(How_Frequently_you_Applied);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.name_GenericMethod_Click(WhatPercentagetFund_ItSecurity_gets);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(IsSystemProtectedFromPotentialRisk);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(howFrequentVunerablityTestDone);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(isAnyRelinanceOnSharedITPlatform_yes);
		SeleniumFunc.waitFor(2);
	}

	public void enterSelectRequiredvalueforOutsourcing() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorgoutsourceNo);
		SeleniumFunc.waitFor(1);

		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorgoutsourceanydataNo);

		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorgrequireNo);
		SeleniumFunc.xpath_GenericMethod_Click(raddoestheOrgselectNo);
		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorgrequireoutsourceNo);
		SeleniumFunc.xpath_GenericMethod_Click(raddoestheorgrequireoutsource_No);
		SeleniumFunc.xpath_GenericMethod_Click(rad_org_missioncriticalNo);
	}

	public void calculateAndValidatePnPGPAEndorsementPremiumDetails(long lngCurrentPersonsCount,
			long lngEndorsedPersonsCount, int intdays, float floatmul, float floatgst, float floatstamp)
			throws Exception {
		double annualbasepremium = (lngEndorsedPersonsCount - lngCurrentPersonsCount) * floatmul;
		if (annualbasepremium < 250) {
			annualbasepremium = 250;
		}
		double basepremium = (annualbasepremium / 365) * intdays;
		double gst = basepremium * floatgst;
		double basepremiumincludinggst = basepremium + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(basepremium, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
		String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
//		String totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);
		String totalpremiumscreen = xpath_Genericmethod_getElementText(endorsement_premium);

		basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
		gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
		totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
		stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

		SeleniumFunc.writeToAssertionFile(
				"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen,
				(basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + gstscreen,
				(gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen,
				(stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile(
				"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
						+ totalpremiumscreen,
				(totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));

	}

	public void selectBusinessTravel() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(uw_bt_button);
	}

	public void enterInternationalTripsWithNoTravelToListedCountries(String internationalTrips) throws Exception{
		try {
			SeleniumFunc.xpath_GenericMethod_Clear(txtNoOfInternationalTrips);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtNoOfInternationalTrips, internationalTrips);
			SeleniumFunc.xpath_GenericMethod_Click(txtNoOfDomesticTrips);
			Thread.sleep(2000);
			SeleniumFunc.xpath_GenericMethod_Click(radTravelToListedCountriesNo);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void enterDomesticTrips(String domesticTrips) throws Exception {
		try {
			SeleniumFunc.xpath_GenericMethod_Clear(txtNoOfDomesticTrips);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtNoOfDomesticTrips, domesticTrips);
			SeleniumFunc.pressTab();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void travelNotExceed180Days() throws Exception {
		try {
			SeleniumFunc.xpath_GenericMethod_Click(radTravelExceed180daysNo);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void travelExceed180Days(String estimateNumber) throws Exception {
		try {
			SeleniumFunc.switchToDefaultContent();
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(radTravelExceed180daysYes);
			SeleniumFunc.xpath_GenericMethod_Click(radTravelExceed180daysYes);
			Thread.sleep(1000);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtEstimateNumber, estimateNumber);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	///////// Refer US-9001 : Business travel enhancements for the latest pricing logic
	public void calculateAndValidateBTVPremiumDetails(int intDays, int internationalTrips, int domesticTrips,
			float internationalMul, float domesticMul, float GST, float internationalStamp, float domesticStamp)
			throws Exception {
		try {
			double domesticPremiumShare = 0, internationalPremiumShare = 0;
			double internationalPremium = internationalTrips * internationalMul;
			double domesticPremium = domesticTrips * domesticMul;
			double annualBasePremium = internationalPremium + domesticPremium;
			if (annualBasePremium < 250) {
				annualBasePremium = 250;
			}

			domesticPremiumShare = annualBasePremium * domesticPremium / (domesticPremium + internationalPremium);
			internationalPremiumShare = annualBasePremium - domesticPremiumShare;

			double domesticGST = domesticPremiumShare * GST;
			double domesticPremiumIncludingGST = domesticPremium + domesticGST;
			double domesticStampDuty = domesticPremiumIncludingGST * domesticStamp;
			double internationalStampDuty = internationalPremiumShare * internationalStamp;
			double totalStampDuty = domesticStampDuty + internationalStampDuty;
			double totalpremium = annualBasePremium + domesticGST + totalStampDuty;
			String basepremiuminstring = Double.toString(round(annualBasePremium, 2));
			String gstinstring = Double.toString(round(domesticGST, 2));
			String stampdutyinstring = Double.toString(round(totalStampDuty, 2));
			String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

			String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
			String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
			String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
			String totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);

			basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
			gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
			totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
			stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

			SeleniumFunc.writeToAssertionFile(
					"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen,
					(basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
			SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + gstscreen,
					(gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
			SeleniumFunc.writeToAssertionFile(
					"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen,
					(stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
			SeleniumFunc.writeToAssertionFile(
					"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
							+ totalpremiumscreen,
					(totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	///////// Refer US-9001 : Business travel enhancements for the latest pricing logic
	public void calculateAndValidateBTVEndorsementPremiumDetails(int intDays, int internationalTrips, int domesticTrips,
			int updatedInternationalTrips, int updatedDomesticTrips, float internationalMul, float domesticMul,
			float GST, float internationalStamp, float domesticStamp) throws Exception {
		try {
			double domesticPremiumShare = 0, internationalPremiumShare = 0;
			double internationalPremium = internationalTrips * internationalMul;
			double domesticPremium = domesticTrips * domesticMul;
			double annualBasePremium = internationalPremium + domesticPremium;
			if (annualBasePremium < 250) {
				annualBasePremium = 250;
			}
			domesticPremiumShare = annualBasePremium * domesticPremium / (domesticPremium + internationalPremium);
			internationalPremiumShare = annualBasePremium - domesticPremiumShare;
			double domesticGST = domesticPremiumShare * GST;
			double domesticPremiumIncludingGST = domesticPremium + domesticGST;
			double domesticStampDuty = domesticPremiumIncludingGST * domesticStamp;
			double internationalStampDuty = internationalPremiumShare * internationalStamp;
			double totalStampDuty = domesticStampDuty + internationalStampDuty;
			double totalpremium = annualBasePremium + domesticGST + totalStampDuty;
			String basepremiuminstring = Double.toString(round(annualBasePremium, 2));
			String gstinstring = Double.toString(round(domesticGST, 2));
			String stampdutyinstring = Double.toString(round(totalStampDuty, 2));
			String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

			String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
			String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
			String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
			String totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);

			basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
			gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
			totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
			stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

			SeleniumFunc.writeToAssertionFile(
					"Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen,
					(basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
			SeleniumFunc.writeToAssertionFile("Expected GST : " + gstinstring + " , Actual GST : " + gstscreen,
					(gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
			SeleniumFunc.writeToAssertionFile(
					"Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen,
					(stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
			SeleniumFunc.writeToAssertionFile(
					"Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : "
							+ totalpremiumscreen,
					(totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void enterdetailstheClaimsinformation() throws Exception {
		SeleniumFunc.waitForElementToClikable(By.xpath(radOrgsubjectNo),10);
		SeleniumFunc.xpath_GenericMethod_Click(radOrgsubjectNo);
		SeleniumFunc.waitForElementToClikable(By.xpath(radOrgbeenNo),10);
		SeleniumFunc.xpath_GenericMethod_Click(radOrgbeenNo);
		SeleniumFunc.waitForElementToClikable(By.xpath(OrgregulatorNo),10);
		SeleniumFunc.xpath_GenericMethod_Click(OrgregulatorNo);
		SeleniumFunc.waitForElementToClikable(By.xpath(OrgthispolicyNo),10);
		SeleniumFunc.xpath_GenericMethod_Click(OrgthispolicyNo);

	}

	public void assetsOrganization(String streetNumber, String streetName, String suburb, String pindcode, String state1,
			String country) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(BtnAddFacility);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(TxtstreetNumberField,streetNumber);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(TxtstreetName,streetName);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Txtsuburb,suburb);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(zipcode1,pindcode);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(
				state,state1);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(
				countrydropdown,country);

	}

	public void details_for_Vehicle_details_pagdetails_for_Vehicle_details_page(String vehicle1, String vehicle2, String vehicle3) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtExecutiveVehicles1,vehicle1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtExecutiveVehicles2,vehicle2);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.jsClick(doesVechicleValueExceds5000_No);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.waitForElementToClikable(By.xpath(DoYouAnticipatePlantBelongingContractor_No),10);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.jsClick(DoYouAnticipatePlantBelongingContractor_No);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.jsClick(isUnderGroundMiningHappening_No);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.waitForElementToClikable(By.xpath(doYouHireLendVehiclePlantEquipement_No),10);
		SeleniumFunc.jsClick(doYouHireLendVehiclePlantEquipement_No);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.waitForElementToClikable(By.xpath(isEmployeeAuthorisedToUseOwnedVehicle_No),10);
		SeleniumFunc.jsClick(isEmployeeAuthorisedToUseOwnedVehicle_No);
	}

	public void enter_the_required_details_for_Drivers_page() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(radDriverNo1);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(radDriverNo2);
		SeleniumFunc.waitFor(2);
	}

	public void enter_the_required_details_for_Insurance_history_page() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(radInsurancehistoryNo1);
		SeleniumFunc.waitFor(3);

	}

	public void enter_the_requred_details_for_Accumulation_page() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(radAccumulationNo1);
		SeleniumFunc.waitFor(3);
	}

	public void OrganisationnameFromCleintAdviser(String orgname) {
		// TODO Auto-generated method stub

	}

	public void details_for_Vehicle_details_pagdetails_for_Vehicle_details_page_CT(String vehicle1, String vehicle2,
			String vehicle3) throws Exception {

		SeleniumFunc.name_GenericMethod_Sendkeys(txtnumofVehicles1,vehicle1);
		SeleniumFunc.name_GenericMethod_Sendkeys(txtnumVehicles2,vehicle2);
		SeleniumFunc.xpath_GenericMethod_Click(radvehiclesNo1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtcarryingvehicles,vehicle3);
		SeleniumFunc.xpath_GenericMethod_Click(radvehiclesNo2);
		SeleniumFunc.xpath_GenericMethod_Click(DoYouAnticipatePlantBelongingContractor);
		SeleniumFunc.xpath_GenericMethod_Click(radvehiclesNo4);
		SeleniumFunc.xpath_GenericMethod_Click(radvehiclesNo5);
	}

	public void enterPublic_and_products_liability_page(String operationalrevenue, String victorGovfunding,
			String otherfunding) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtoperational_revenue,operationalrevenue);
		SeleniumFunc.waitFor(3);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtVictorian_gov_funding,victorGovfunding);
		SeleniumFunc.waitFor(3);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtVictorian_gov_other,otherfunding);
		SeleniumFunc.waitFor(3);
	}

	public void Storage_of_hazardous_substances_or_dangerous_goods(String protectionLicense, String yourpremises) throws Exception {
		SeleniumFunc.switchToDefaultContent();
        SeleniumFunc.xpath_GenericMethod_Click(Activities_Dropdown_icon);

        SeleniumFunc.xpath_GenericMethod_Click(Storage_dropdown_value);


		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(Aware_drpdown_icon);
     	//SeleniumFunc.xpath_GenericMethod_Sendkeys(Aware_drpdown_icon,yourpremises);
		SeleniumFunc.xpath_GenericMethod_Click(Contaminated_dropdown);
		//SeleniumFunc.robotEnter();
		SeleniumFunc.waitFor(3);

	}

	public void enters_the_required_details_for_Incidents_and_potential_claims() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(radIncident_No);
		SeleniumFunc.waitFor(3);
	}

	public void enters_the_required_details_for_Interstate_or_overseas_work() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(radInterstate_No);
		SeleniumFunc.waitFor(3);
	}

	public void user_enters_the_required_details_for_Affliation() throws Exception {
		// TODO Auto-generated method stub
		SeleniumFunc.xpath_GenericMethod_Click(radaffilation_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radactivities_No);



	}

	public void user_enters_the_required_details_for_Visitors(String nofvisitors, String unitLevel, String southWales,
			String streetNumber, String streetName, String suburb, String state, String postcode, String country) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(btnVisitor_Add_Button);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtMax_no_of_visitors, nofvisitors);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtstreetno,streetNumber);
		SeleniumFunc.waitFor(3);


		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtstreetno,txtstreetname);
		SeleniumFunc.waitFor(3);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtstreetno,txtsuburb);
		SeleniumFunc.waitFor(3);

		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtstreetno,drpstate_dropdown);
		SeleniumFunc.waitFor(3);


		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtstreetno,txtpost_code);
		SeleniumFunc.waitFor(3);
	}

	public void user_enters_the_required_details_for_General_information(String qualifiedstaff) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(radgeneral_info_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtquality_staff, qualifiedstaff);

	}

	public void user_enters_the_required_details_for_Contractors_Sub_contractors() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(contractors_Dropdown_icon);
		SeleniumFunc.xpath_GenericMethod_Click(contractors_Dropdown_value);
	}

	public void user_enters_the_required_details_for_Product_liability() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(radprod_liability);
		SeleniumFunc.waitFor(3);
	}

	public void user_enters_the_required_details_for_Airfield_liability() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(radorg_Liability);
		SeleniumFunc.waitFor(3);

	}

	public void user_enters_the_required_details_for_Insurance_history() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(radorg_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radorg_past_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(raddecline_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radimpose_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radDecRenew_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radCancRenew_No);
		SeleniumFunc.waitFor(3);

	}

	public void user_enters_the_required_details_for_Professional_activities(String proact, String natureofwork, String percentage) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtstate,proact);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radnature_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(btnaddItem);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtnature_of_work,natureofwork);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtpercentage, percentage);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radorgu_No);
		SeleniumFunc.waitFor(3);

	}

	public void user_enters_the_required_details_for_Joint_ventures() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(radjoint_ven_No);
		SeleniumFunc.waitFor(3);

	}

	public void user_enters_the_required_details_for_Overseas_work() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(Overseas_work_No);
		SeleniumFunc.waitFor(3);

	}

	public void user_enters_the_required_details_for_Fee_income() throws Exception {
		// TODO Auto-generated method stub
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtAustralia,"Test");
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtOverseas,"10");
		SeleniumFunc.waitFor(3);

	}

	public void user_enters_the_required_details_for_Contractors_and_sub_contractors(String potentialindemnities,String grossprofessionalfees) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(radportion_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radrequire_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtprovide,potentialindemnities);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtstate1,potentialindemnities);
		SeleniumFunc.xpath_GenericMethod_Click(radcontract_no);
		SeleniumFunc.waitFor(3);

	}

	public void enterTotalAnnualRevenue(String OperationalRevenue,String VicrorianRevenue,String OtherFund) throws Exception {
		SeleniumFunc.name_GenericMethod_Sendkeys(oprational_Revenue,OperationalRevenue);
		SeleniumFunc.name_GenericMethod_Sendkeys(Victorion_Government_Funding,VicrorianRevenue);
		SeleniumFunc.name_GenericMethod_Sendkeys(OtherFunding,OtherFund);
		clickContinueButton();
	}

	public void enterTheIncident_OverStateWork_AffilationWorkDetails() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(isOrgAwareOfPotentialClaim_No);
		clickContinueButton();
		SeleniumFunc.xpath_GenericMethod_Click(isAnyEmployeesAreOverseaWorker_No);
		clickContinueButton();
		SeleniumFunc.xpath_GenericMethod_Click(DoYouHaveAnyAffiliations_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(AreThereThirdPartyVendors_No);
		clickContinueButton();
	}

	public void enterStoargeOfHazardsDetails(String activityCasuedDamege,String presenceWater ) throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.enterTheValueInComboBoxUsingXpath(ActivitiesSubjectToAnEnvironmentProtectionLicense,activityCasuedDamege);
		SeleniumFunc.waitInMilliSeconds(3);
		SeleniumFunc.enterTheValueInComboBoxUsingXpath(AwareOfPresencePremisies,presenceWater);
		clickContinueButton();

	}

	public void enterAirfieldAndProductLiablity_ContractDetails(String contracterInfo) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(AirfieldLiability_No);
		clickContinueButton();
		SeleniumFunc.xpath_GenericMethod_Click(ProductLiability_No);
		clickContinueButton();
		SeleniumFunc.enterTheValueInComboBoxUsingXpath(Orgnanization_Contractors,contracterInfo);
		clickContinueButton();
	}

	public void enterTheVisitorDetails(String noOfVisitors,String StreetName,String SubrubOrTown,String State,String postcode) throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Click(Visitors_AddButton);
		SeleniumFunc.waitForElementToClikable(By.name(Visitors_No_Visitors),8);
		SeleniumFunc.name_GenericMethod_Sendkeys(Visitors_No_Visitors,noOfVisitors);
		SeleniumFunc.name_GenericMethod_Sendkeys(Visitors_StreetNumber,"124212");
		SeleniumFunc.name_GenericMethod_Sendkeys(Visitors_Streetname,StreetName);
		SeleniumFunc.name_GenericMethod_Sendkeys(Visitors_Town,SubrubOrTown);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(Visitors_State,State);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(Visitors_Postcode,postcode);
		SeleniumFunc.xpath_GenericMethod_Click(Submit_UW_Dialogue);
		SeleniumFunc.waitFor(1);
		clickContinueButton();
	}

	public void enterTheProfessionalIndiminityInfo() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(HasNameChanged_No);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(Qualifiedstaff,"1000");
		clickContinueButton();
	}

	public void enterInsuranceHistoryDetails() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(Is_InsuredProfessinalIndemity_No);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(Is_InsuredProfessinalIndemity_InPast_No);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(IsDeclineRequestForCover_No);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(IsImposedSpecialTerms_No);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(IsDeclineRenewInsurance_No);
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.xpath_GenericMethod_Click(IsCancelYourInsurance_No);
		clickContinueButton();
	}

	public void enterProfessionalActivities() throws Exception {
		SeleniumFunc.name_GenericMethod_Sendkeys(AdviceByOrg,"Advice");
		SeleniumFunc.waitForElementToClikable(By.xpath(IsAdviceDifferFromService_No),6);
		SeleniumFunc.xpath_GenericMethod_Click(IsAdviceDifferFromService_No);
		SeleniumFunc.waitForElementToClikable(By.xpath(IsOrgLimitYourLiability_Respect),6);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(IsOrgLimitYourLiability_Respect);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_GenericMethod_Click(Add_Activeites_Undertaken_btn);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(Add_Activities_NatureOfWork,"Risk validation");
		SeleniumFunc.name_GenericMethod_Sendkeys(Add_Activities_Percentage,"50");
		clickContinueButton();
	}

	public void enterJointVenturesOverseaInfo() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(JoinVentures_Yes);
		clickContinueButton();
		SeleniumFunc.xpath_GenericMethod_Click(isUndertakeOverseasWork_No);
		clickContinueButton();
		SeleniumFunc.waitInMilliSeconds(15);

	}

	public void EnterFeeIncomeInfo() throws Exception {
		SeleniumFunc.waitFor(1);
		clickContinueButton();
	}

	public void EnterContractorAndSubContractorInfor() throws Exception {
		SeleniumFunc.waitForElementToClikable(By.xpath(IsOrgProfessionalServiceContracted_No),8);
		SeleniumFunc.xpath_GenericMethod_Click(IsOrgProfessionalServiceContracted_No);
		SeleniumFunc.waitForElementToClikable(By.xpath(IsOrgNeedContract_No),8);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(IsOrgNeedContract_No);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.waitForElementToClikable(By.xpath(IscontractHold25PerWork),8);
		SeleniumFunc.xpath_GenericMethod_Click(IscontractHold25PerWork);
		SeleniumFunc.waitForElementToClikable(By.name(YearfeesPaidconsultants),8);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(YearfeesPaidconsultants,"Test");
		SeleniumFunc.waitForElementToClikable(By.name(ProfessionalIndeminites),8);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(ProfessionalIndeminites,"Test");
		clickContinueButton();
	}

	public void enterRiskInformation() throws Exception {
		SeleniumFunc.waitForElementToClikable(By.xpath(DisclaimerOnAdvice_No),8);
		SeleniumFunc.xpath_GenericMethod_Click(DisclaimerOnAdvice_No);
		SeleniumFunc.waitForElementToClikable(By.xpath(AdviceConfimedInWriting_No),8);
		SeleniumFunc.xpath_GenericMethod_Click(AdviceConfimedInWriting_No);
		SeleniumFunc.waitForElementToClikable(By.xpath(IsRiskManagemenDocumented_No),8);
		SeleniumFunc.xpath_GenericMethod_Click(IsRiskManagemenDocumented_No);
		SeleniumFunc.waitForElementToClikable(By.xpath(Is_ProgramReviewed_No),8);
		SeleniumFunc.xpath_GenericMethod_Click(Is_ProgramReviewed_No);
		clickContinueButton();
	}

	public void enterClaimsAndCircucentancesInfo() throws Exception {
		SeleniumFunc.waitForElementToClikable(By.xpath(IsClaimMadeOnPast10Years_no),8);
		SeleniumFunc.xpath_GenericMethod_Click(IsClaimMadeOnPast10Years_no);
		SeleniumFunc.waitForElementToClikable(By.xpath(IsAnyClaimsisNotifiedtoVmia),8);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(IsAnyClaimsisNotifiedtoVmia);
		SeleniumFunc.waitForElementToClikable(By.xpath(IsAnyclaimAgainstPreviousPractices_No),8);
		SeleniumFunc.xpath_GenericMethod_Click(IsAnyclaimAgainstPreviousPractices_No);
		SeleniumFunc.waitForElementToClikable(By.xpath(Is_ExcutiveSubjectDiscplinaryAction_No),8);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(Is_ExcutiveSubjectDiscplinaryAction_No);
		clickContinueButton();
	}

	public void user_enters_the_required_details_for_Risk_management() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(radwriiten_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radverbal_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radtheorg_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radindependent_No);
		SeleniumFunc.waitFor(3);

	}

	public void user_enters_the_required_details_for_Claims_and_circumstances() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(radpast10_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radanycircum_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radanyclaim_No);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(radanyprinci_No);
		SeleniumFunc.waitFor(3);

	}

	public void SelectCBL() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(btnCBL);
	}

	public void enterNoteasUW() throws Exception
	{
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Add_note);
		switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(uw_portal_dialog_notes, "Testing for Us-4021");
		SeleniumFunc.xpath_GenericMethod_Click(uw_portal_dialog_submit_button);

	}


	public void clickUWPortalSearchButton() {
		try {
			SeleniumFunc.xpath_GenericMethod_Click(imgSearch_UWPortal);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void selectConstructionRisks() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(uw_con_button);
	}

	public void selectRnDProduct() throws Exception {
		// SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(RndProduct);
	}

	public void selectProduct(String product) throws Exception {
//		SeleniumFunc.waitForElementToClikable(By.xpath("(//h2[text()='"+product+"']/ancestor::div[3]/div[2]//button)[2]"),60);
		SeleniumFunc.xpath_GenericMethod_Click("(//h2[text()='"+product+"']/ancestor::div[3]/div[2]//button)[2]");
	}

	public void selectProductByVerifyingDescription(String product, String description) throws Exception {
//		String description = "associated liability";
//		String description = "associated legal liability";
		if(product.equals("Construction Risks - Material Damage and Liability Annual")){
			if(description.equals("associated legal liability")){
				SeleniumFunc.xpath_GenericMethod_Click("//span[contains(text(),'associated legal liability')]/parent::div/parent::div/parent::div/following-sibling::div/following-sibling::div//button");
			}else if(description.equals("associated liability")){
				SeleniumFunc.xpath_GenericMethod_Click("//span[contains(text(),'associated liability')]/parent::div/parent::div/parent::div/following-sibling::div/following-sibling::div//button");
			}
		}else{
			SeleniumFunc.xpath_GenericMethod_Click("(//h2[text()='"+product+"']/ancestor::div[3]/div[2]//button)[2]");
		}
	}

	public void SelectProdctPPLNetFlow() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click("(//h2[text()='Public & Products Liability']/ancestor::div[3]/div[2]//button)[4]");
	}

	public void addDetailsForTravelRecord(String destination) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(travelDays);
		SeleniumFunc.xpath_GenericMethod_Click(countryList);
		SeleniumFunc.xpath_GenericMethod_Click(countyTravellingToImg);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(countrySelection);
		SeleniumFunc.name_GenericMethod_Click(domesticTripAddBtn);
		SeleniumFunc.name_GenericMethod_Sendkeys(destinationname,destination);
		SeleniumFunc.xpath_GenericMethod_Click(domFromdateImg);
		SeleniumFunc.actionClick(By.xpath(current_date));
		SeleniumFunc.waitForElementToClikable(By.xpath(domTodateImg),60);
		SeleniumFunc.xpath_GenericMethod_Click(domTodateImg);
		SeleniumFunc.actionClick(By.xpath(current_date));
		SeleniumFunc.name_GenericMethod_Sendkeys(domStudents,"50");
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(domStaff,"150");
//		SeleniumFunc.name_GenericMethod_Click(internationalTripAddBtn);
//		SeleniumFunc.waitFor(2);
//		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(intDestinitionName,destination);
//		SeleniumFunc.xpath_GenericMethod_Click(intFromdateImg);
//		SeleniumFunc.actionClick(By.xpath(current_date));
//		SeleniumFunc.xpath_GenericMethod_Click(intTodateImg);
//		SeleniumFunc.actionClick(By.xpath(current_date));
//		SeleniumFunc.name_GenericMethod_Sendkeys(intStudent,"2");
//		SeleniumFunc.waitFor(1);
//		SeleniumFunc.name_GenericMethod_Sendkeys(intStaff,"4");
	}

	public void selectProductInRiskAdvisor(String product) throws Exception {

		SeleniumFunc.clickElement(AlProduct_RiskAdvisor);
		SeleniumFunc.xpath_GenericMethod_Click("//h2[text()='"+product+"']/ancestor::div[3]/div[2]//button");
		if(SeleniumFunc.getElements(yes_DublicateProduct).size()>0){
			SeleniumFunc.jsClick(yes_DublicateProduct);
			SeleniumFunc.clickElement(By.xpath(Asset_Submit));
		}

	}

	public void enterTheEffectiveAndExpirationdate(String effectiveDate,String expiration) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.SelectDateWithDropDown(multiYeareffectivedate,effectiveDate);
		SeleniumFunc.SelectDateWithDropDown(multiYearExpirydate,expiration);
	}

	public void SelectTheEffecttiveDateAndCaptureTheExpiryDate(String effectivedate) throws Exception {
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(policyEffectdate,date);
		SeleniumFunc.SelectDateWithDropDown(policy_effective_Date_calender,effectivedate);
		String expirydate=SeleniumFunc.getElementText(policyExpiryDate);
		testData.setPolicyduration(SeleniumFunc.findTheDateDifference(effectivedate,expirydate));
		System.out.println("ExpiryDate :"+expirydate +"  Policyduration: "+testData.getPolicyduration());
	}



	public void fillConsRiskQuestions() throws Exception {
		try {
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(radPrevInsuranceDeclinedNo);
			SeleniumFunc.waitInMilliSeconds(5);
			SeleniumFunc.jsClick(radPrevInsuranceDeclinedNo);
			SeleniumFunc.waitInMilliSeconds(5);
			SeleniumFunc.jsClick(radLossOfContractWorkNo);
			SeleniumFunc.waitInMilliSeconds(5);
			SeleniumFunc.jsClick(radContractWorkTypeNo);
			SeleniumFunc.waitInMilliSeconds(5);
			SeleniumFunc.jsClick(radLocationWithAuthorityNo);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void fillConstructionRiskQuestions() throws Exception {
		try {
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(lblLossOfWorkContractNo);
			SeleniumFunc.waitInMilliSeconds(5);
			SeleniumFunc.jsClick(lblLossOfWorkContractNo);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void clickAddButton() throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.jsClick(btnContractAdd);
		SeleniumFunc.waitFor(2);
	}

	public void clickViewEditButton() throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.jsClick(view_edit_button);
	}

	public void clickAddItemButton() throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.jsClick(add_item_button);
	}

	public void clickCaseID() throws Exception {
		SeleniumFunc.waitFor(1);
		try {
			SeleniumFunc.xpath_GenericMethod_Click(caseId);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void searchDeactivateID(String queue) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(selectQueueDropdown);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(UnderwritingOption+"span[text()='"+queue+"']");
	}

	public void FillContractDetailsAndClickSubmit(String contractType, String estimatedProjectValue){
		SeleniumFunc.waitFor(1);
		try {
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(contractTypeDropdown);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(contractTypeDropdown, contractType);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(nameOfTheContractTextField, "Test Automation Contract");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(suburbTextField, "Test Automation - Additional Insureds");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(postCodeTextField, "865438");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(estimatedProjectValueTextField, estimatedProjectValue);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(contractorArrangedInsurance, "PCIP - Liability");
			SeleniumFunc.jsClick(submitButton);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}


	public void expandContractDetails() {
		String eleCurrentStatus = SeleniumFunc.getElementAttribute(btnExpandContractDetails, "aria-label");
		if(eleCurrentStatus.equalsIgnoreCase("Disclose Contract details")) {
			try {
				SeleniumFunc.xpath_GenericMethod_Click(btnExpandContractDetails);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void fillContractDetails(String contractType, String estimatedProjectValue) throws Exception {
		try {
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(lstContractType);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(lstContractType, contractType);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtNameOfTheContract, "Test Automation Contract");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtContractorsOrAdditionalInsuredsToBeNamed, "Test Automation - Additional Insureds");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtContractNoOrIdentification, "865438");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtEstimatedProjectValue, estimatedProjectValue);
			SeleniumFunc.SelectDateWithDropDown(imgContractCommencementDate,"30/06/2021");
			Thread.sleep(500);
			SeleniumFunc.SelectDateWithDropDown(imgEstContractCompletionDate,"04/12/2022");
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtGeospatialCoordinates, "12345678,98765432");
			SeleniumFunc.waitFor(2);
			SeleniumFunc.enterTheValueInComboBox(StreetAddress,"Sydney ");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(contractpostcode,"678975");

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void clickCantFindAddress() {
		try {
			SeleniumFunc.xpath_GenericMethod_Click(lnkCantFindAddress);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public void fillContractAddress() throws Exception {
		try {

			SeleniumFunc.xpath_GenericMethod_scrollIntoView(txtStreetNumber);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtStreetNumber, "12");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtStreetName, "Collins St");
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtSuburbTown, "Docklands");
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(lstContractState, "Victoria");
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtPostcode, "3000");
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(lstContractCountry, "AUSTRALIA");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void fillLargestSingleContractValue(String largestSingleContractVal) throws Exception {
		try {
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(txtLargestSingleContractValue);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtLargestSingleContractValue, largestSingleContractVal);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void fillOptionalExtensionDetails() throws Exception {
		try {
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(radDamageToExistingStructuresNo);
			SeleniumFunc.xpath_GenericMethod_Click(radDamageToExistingStructuresNo);
			SeleniumFunc.xpath_GenericMethod_Click(radContractorsPlantEquipmentNo);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void calculateAndValidateConstructionRisksPremiumDetails(float floatEstimatedProjectValue, float floatMaterialDamageMul, float floatLiabilityMul, float floatgst, float floatstamp) throws Exception {
		double materialDamagePremium = floatEstimatedProjectValue * floatMaterialDamageMul/100;
		double floatLiabilityPremium = floatEstimatedProjectValue * floatLiabilityMul/100;
		double basepremium = materialDamagePremium + floatLiabilityPremium;
		double gst = basepremium * floatgst;
		double basepremiumincludinggst = basepremium + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(basepremium, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
		String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
		String totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);

		basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
		gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
		totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
		stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

		SeleniumFunc.writeToAssertionFile("Expected base premium : " + basepremiuminstring + " , Actual base premium : " + basepremiumscreen, (basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected gst : " + gstinstring + " , Actual gst : " + gstscreen, (gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected stamp duty : " + stampdutyinstring + " , Actual stamp duty : " + stampdutyscreen, (stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected total premium : " + totalexpectedpremiuminstring + " , Actual total premium : " + totalpremiumscreen, (totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));
	}

	public void fillUpdateContractDetails(String contractType, String estimatedProjectValue) throws Exception {
		try {
			SeleniumFunc.xpath_GenericMethod_scrollIntoView(lstContractType);
			SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(lstContractType, contractType);
			SeleniumFunc.xpath_GenericMethod_Clear(txtEstimatedProjectValue);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtEstimatedProjectValue, estimatedProjectValue);
			SeleniumFunc.waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Sendkeys(txtChangesToConstructionPolicy, "Test Automation - Update Construction Risks policy");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void calculateAndValidateConstructionRisksEndorsementPremiumDetails(float floatEstimatedProjectValue, float floatMaterialDamageMul, float floatLiabilityMul, float floatNewEstimatedProjectValue, float floatNewMaterialDamageMul, float floatNewLiabilityMul, float floatgst, float floatstamp) throws Exception {
		double materialDamagePremium = (floatNewEstimatedProjectValue * floatNewMaterialDamageMul/100) - (floatEstimatedProjectValue * floatMaterialDamageMul/100);
		double floatLiabilityPremium = (floatNewEstimatedProjectValue * floatNewLiabilityMul/100) - (floatEstimatedProjectValue * floatLiabilityMul/100);
		double basepremium = materialDamagePremium + floatLiabilityPremium;
		double gst = basepremium * floatgst;
		double basepremiumincludinggst = basepremium + gst;
		double stampduty = basepremiumincludinggst * floatstamp;
		double totalpremium = basepremiumincludinggst + stampduty;
		String basepremiuminstring = Double.toString(round(basepremium, 2));
		String gstinstring = Double.toString(round(gst, 2));
		String stampdutyinstring = Double.toString(round(stampduty, 2));
		String totalexpectedpremiuminstring = Double.toString(round(totalpremium, 2));

		String basepremiumscreen = xpath_Genericmethod_getElementText(base_premium);
		String gstscreen = xpath_Genericmethod_getElementText(gst_screen);
		String stampdutyscreen = xpath_Genericmethod_getElementText(stamp_duty);
		String totalpremiumscreen = xpath_Genericmethod_getElementText(total_premium);

		basepremiumscreen = basepremiumscreen.replaceAll("[^-.0-9]", "");
		gstscreen = gstscreen.replaceAll("[^-.0-9]", "");
		totalpremiumscreen = totalpremiumscreen.replaceAll("[^-.0-9]", "");
		stampdutyscreen = stampdutyscreen.replaceAll("[^-.0-9]", "");

		SeleniumFunc.writeToAssertionFile("Expected Endorsement base premium : " + basepremiuminstring + " , Actual Endorsement base premium : " + basepremiumscreen, (basepremiuminstring.equals(basepremiumscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected Endorsement gst : " + gstinstring + " , Actual Endorsement gst : " + gstscreen, (gstinstring.equals(gstscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected Endorsement stamp duty : " + stampdutyinstring + " , Actual Endorsement stamp duty : " + stampdutyscreen, (stampdutyinstring.equals(stampdutyscreen) ? "PASS" : "FAIL"));
		SeleniumFunc.writeToAssertionFile("Expected Endorsement total premium : " + totalexpectedpremiuminstring + " , Actual Endorsement total premium : " + totalpremiumscreen, (totalexpectedpremiuminstring.equals(totalpremiumscreen) ? "PASS" : "FAIL"));
	}
	public void coverDetails() throws Exception {
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(limitOfIndemnity);
		SeleniumFunc.xpath_GenericMethod_Click(officerDeductable);
		SeleniumFunc.xpath_GenericMethod_Click(publicBodyDeductable);
		SeleniumFunc.xpath_GenericMethod_Click(EPLclaimDeductible);
		SeleniumFunc.xpath_GenericMethod_Click(PollutionLiabilityDeductible);
		SeleniumFunc.xpath_GenericMethod_Click(btnno);
	}

	public void publicBodyDetails(String description, String legalStatus, String currentAssets, String currentLiabilities, String annualExpens, String govtFunds, String otherActivities) throws Exception {
		SeleniumFunc.name_GenericMethod_Sendkeys(publicBodyDescription,description);
		SeleniumFunc.xpath_GenericMethod_Click(dateFeild);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(current_date);
		SeleniumFunc.waitFor(3);
		driver.findElement(By.xpath("//label[text()='"+legalStatus+"']")).click();
		SeleniumFunc.xpath_GenericMethod_Click(publicBodyAnotherEntity);
		SeleniumFunc.xpath_GenericMethod_Click(publicBodyAnnualReport);
		SeleniumFunc.xpath_GenericMethod_Click(publicBodyAquisition);
		SeleniumFunc.xpath_GenericMethod_Click(publicBodySubsidiaries3rdParty);
		SeleniumFunc.xpath_GenericMethod_Click(publicBodyAttached);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_GenericMethod_Sendkeys(CurrentAssets,currentAssets);
		SeleniumFunc.name_GenericMethod_Sendkeys(CurrentLiabilities,currentLiabilities);
		SeleniumFunc.name_GenericMethod_Sendkeys(BudgetedAnnualExpenditure, annualExpens);
		SeleniumFunc.name_GenericMethod_Sendkeys(ConsoGovtFunds, govtFunds);
		SeleniumFunc.name_GenericMethod_Sendkeys(OtherBussActivities,otherActivities );
		SeleniumFunc.xpath_GenericMethod_Click(publicBodySubsidiaresBussActUSA);
		SeleniumFunc.xpath_GenericMethod_Click(publicBodySubsidiaresBussActAustralia);
		SeleniumFunc.xpath_GenericMethod_Click(publicBodyDeptTreasury);
		SeleniumFunc.waitFor(2);
	}
	public void directorDetails(String name,String qualification,String experience) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(directorAddBtn);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_GenericMethod_Sendkeys(directorName, name);
		SeleniumFunc.name_GenericMethod_Sendkeys(directorProfessinal, qualification);
		SeleniumFunc.xpath_GenericMethod_Click(dateIcon);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(current_date);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.name_GenericMethod_Sendkeys(directorExp, experience);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(directorCurrentlyMembers);
		SeleniumFunc.xpath_GenericMethod_Click(directorDeregistor);
		SeleniumFunc.xpath_GenericMethod_Click(directorBankrupt);
		SeleniumFunc.xpath_GenericMethod_Click(directorOfCompany);
		SeleniumFunc.xpath_GenericMethod_Click(directorResigned);
		SeleniumFunc.xpath_GenericMethod_Click(directorPBInterset);
		SeleniumFunc.waitFor(3);
	}

	public void ControlPractices() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(directorAccounting);
//		SeleniumFunc.xpath_GenericMethod_Click(directoraudit);
		waitFor(3);
//		String Reason = "High Expense";
		SeleniumFunc.name_GenericMethod_Sendkeys(directorControl, "High Expense");

	}
	public void  employementDetails() throws Exception{
//		wait(60);
		SeleniumFunc.xpath_GenericMethod_Click(EmployementEvent);
		SeleniumFunc.xpath_GenericMethod_Click(EmplyeeHandbook);
		SeleniumFunc.xpath_GenericMethod_Click(EmployeeInductiion);
		SeleniumFunc.xpath_GenericMethod_Click(EmployeeInductiionCoverage);
		SeleniumFunc.xpath_GenericMethod_Click(EmployeeInductiionRefresher);
		SeleniumFunc.waitFor(3);
//		List<WebElement> EmployeeDetailsList = SeleniumFunc.getElements(noBtnPolicyProcedure);
//		for(WebElement EmployeeDetails : EmployeeDetailsList){
//			System.out.println(EmployeeDetails.getText());
//			EmployeeDetails.click();
//		}

		for(int i=1;i<=17;i++){
			driver.findElement(By.xpath("(//table[@pl_prop_class='VMIA-Ins-Data-Application-Comm-DAO-Generic']//tr/td[2]//label[text()='No'])["+i+"]")).click();
			driver.findElement(By.xpath("(//table[@pl_prop_class='VMIA-Ins-Data-Application-Comm-DAO-Generic']//tr/td[3]//label[text()='No'])["+i+"]")).click();

		}

		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(EmployeeHR);
		SeleniumFunc.xpath_GenericMethod_Click(EmployeePBPractise);



	}

	public void  pollutionDetails() throws Exception{
		SeleniumFunc.xpath_GenericMethod_Click(PollutionDangerous);
		SeleniumFunc.xpath_GenericMethod_Click(PollutionManagement);
		SeleniumFunc.xpath_GenericMethod_Click(PollutionAsbestos);
		SeleniumFunc.xpath_GenericMethod_Click(PollutionAsbestosManage);
		SeleniumFunc.xpath_GenericMethod_Click(PollutionHazardousGoods);
	}

	public void  InsuranceAandClaimDetails() throws Exception{
		SeleniumFunc.xpath_GenericMethod_Click(InsuranceCurrentlyInForce);
		SeleniumFunc.xpath_GenericMethod_Click(InsuranceClaimInIndeminity);
		SeleniumFunc.xpath_GenericMethod_Click(InsuranceIncidentToCliam);
		SeleniumFunc.xpath_GenericMethod_Click(InsuranceProsecution);
		SeleniumFunc.xpath_GenericMethod_Click(InsuranceSubsidaryProsecution);
		SeleniumFunc.xpath_GenericMethod_Click(InsurancePracticesLiability);
	}
	public void NumberOfVehicles(String number) throws Exception{
		SeleniumFunc.name_GenericMethod_Sendkeys(NumberOfVehicle,number);
		SeleniumFunc.xpath_GenericMethod_Click(MotorExceeding);
		SeleniumFunc.xpath_GenericMethod_Click(MotorAnticipate);
		SeleniumFunc.xpath_GenericMethod_Click(MotorBeach);
		SeleniumFunc.xpath_GenericMethod_Click(MotorHire);
		SeleniumFunc.xpath_GenericMethod_Click(MotorPrivatelyOwned);
	}

	public void employementDrivers() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(MotorCurrency);
		SeleniumFunc.xpath_GenericMethod_Click(MotorDrivers);
	}
	public void InsuranceHistory() throws Exception {
			SeleniumFunc.xpath_GenericMethod_Click(MotorInsured);
	}
	public void Accumulation() throws Exception {
				SeleniumFunc.xpath_GenericMethod_Click(MotorAccumulation);

	}
	public void EndorsementMotor()throws Exception{
		SeleniumFunc.name_GenericMethod_Sendkeys(updateMotor,"Motor Vehicle");

	}
}
