package com.cucumber.framework.PageObjects;

import com.cucumber.framework.context.TestData;
//import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.cucumber.framework.CS.CustomerServ;
import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import com.cucumber.framework.configreader.ObjectRepo;
//import com.cucumber.framework.helper.Logger.LoggerHelper;

import java.util.concurrent.TimeUnit;

public class LoginPage extends CustomerServ implements LoginPageLoc {
	//private final Logger log = LoggerHelper.getLogger(LoginPage.class);
	LoginPage loginpage;
	TestData testData;
	public LoginPage(WebDriver driver, TestData data) {
		super(driver);
		testData=data;
	}

	public void testLoginPage() {
		// System.out.println("In TestLoginPage method :"+ driver);
	}

	public void sendLoginObject(LoginPage loginpage) {
		this.loginpage = loginpage;
		// System.out.println("In sendLoginObject method search page"+this.loginpage);
	}


	public void clickOnLoginbtn() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(login_btn_xpath);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	}
	
	public void performLogout() throws Exception
	{
		SeleniumFunc.switchToDefaultContent();
		try { SeleniumFunc.xpath_GenericMethod_Click(logout_iconn); }
		catch (Exception e) {
			SeleniumFunc.xpath_GenericMethod_Click(logout_icon);
		}
		finally{
			SeleniumFunc.xpath_GenericMethod_Click(logoff_button);
		}
//		try {
//		SeleniumFunc.xpath_GenericMethod_Click(logout_icon); }
//		catch (Exception e) {}
//		SeleniumFunc.xpath_GenericMethod_Click(logoff_button);
	}

	public void devStudioLogout() throws Exception{
		SeleniumFunc.clickElementUsingActions(By.xpath(dsLogoffIcon));
		SeleniumFunc.waitForElementToClikable(By.xpath(dsLogoffLink),20);
		SeleniumFunc.xpath_GenericMethod_Click(dsLogoffLink);
	}
	
	public void performLogout_1() throws Exception
	{
		SeleniumFunc.xpath_GenericMethod_Click(logout_icon_1);
		SeleniumFunc.xpath_GenericMethod_Click(logoff_button);
	}
	
	
	public void openInternalLink() throws Exception
	{
		driver.navigate().to(ObjectRepo.reader.getWebsite());
    }

	public void verifyMsg() throws Exception {
        System.out.println("In verify login successful");
        String verifyTextLogin=xpath_Genericmethod_getElementText(verify_msg_xpath);
        if(verifyTextLogin.equalsIgnoreCase("O2CKatowice")) {	
		Assert.assertTrue(verifyTextLogin.equalsIgnoreCase("O2CKatowice"),
				"Login is not successful");
		Reporter.log("Login is Successful for the region: "+verifyTextLogin);
        }else if(verifyTextLogin.equalsIgnoreCase("UnileverO2CNorthAmerica")) {
        	Assert.assertTrue(verifyTextLogin.equalsIgnoreCase("UnileverO2CNorthAmerica"),
    				"Login is not successful");
        	Reporter.log("Login is Successful for the region: "+verifyTextLogin);
        }else {
        	Assert.assertTrue(false);
        	
        }

	}

	public void verifyLoginFail() {

		Assert.assertTrue(driver.findElement(By.xpath(error_msg_xpath)).getText()
				.contains("The information you entered was not recognized"), "User logged in");

	}

	public void setUserNameAzure(String user) throws Exception {
		SeleniumFunc.clearTextBox(By.xpath(username_azure_xpath));
		SeleniumFunc.xpath_GenericMethod_Sendkeys(username_azure_xpath, user);
		
	}

	public void setPasswordAzure(String pass) throws Exception {
		SeleniumFunc.clearTextBox(By.xpath(password_azure_xpath));
		SeleniumFunc.xpath_GenericMethod_Sendkeys(password_azure_xpath, pass);
		
	}

	
	public void setUserName(String username) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(username_xpath, username);
	}

	public void setPassword(String password) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(password_xpath, password);

	}

	public void clickOnLoginBtnAzure() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(login_btn_azure_xpath);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(vmia_logo);
//		SeleniumFunc.waitFor(3);
//		try {
//			SeleniumFunc.xpath_GenericMethod_Click(Exitt);
//			driver.navigate().refresh();
//		} catch (Exception e) {
//		}
//		SeleniumFunc.waitFor(1);

	}

}
