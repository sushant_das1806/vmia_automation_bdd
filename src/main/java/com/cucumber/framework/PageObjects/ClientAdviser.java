package com.cucumber.framework.PageObjects;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cucumber.framework.GeneralHelperSel.CSVFileReader;
import com.cucumber.framework.context.TestData;
//import org.apache.log4j.Logger;
import io.cucumber.java.eo.Se;
import org.apache.poi.util.SystemOutLogger;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cucumber.framework.CS.CustomerServ;
import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import com.cucumber.framework.TestBase.TestBase;
//import com.cucumber.framework.helper.Logger.LoggerHelper;
import org.testng.asserts.Assertion;
import org.testng.asserts.SoftAssert;

public class ClientAdviser extends CustomerServ implements ClientAdviserLoc {
	//private final Logger log = LoggerHelper.getLogger(ClientAdviser.class);
	ClientAdviser clientadv;
	TestData testdata;
	String solicitorName;
	public ClientAdviser(WebDriver driver, TestData data) {
		super(driver);
		testdata=data;
	}

	By contactSerchFirstName=By.name("$PCustomerSearch$pSearchStringFirstName");
	By contactSerchLastName=By.name("$PCustomerSearch$pSearchStringLastName");
	By SearchContactButton=By.name("CustomerSearch_CustomerSearch_10");
	By Policy_BundleType=By.name("$PpyWorkPage$pBundleType");
	By Expand_Org_BundlePolicy=By.xpath("//div[@class='header header-bar clearfix']//i[@class='icon icon-openclose']");
	By BundlePolicy_Revenue=By.name("$PpyWorkPage$pSelectedClientPackages$l1$pClientDetails$pClientRevenue");
	By BundlePolicy_PolicyEffectiveDate=By.name("$PpyWorkPage$pSelectedClientPackages$l1$pClientDetails$pEffectiveDate");
    By Auswide_RevenueBreakDown_ACT=By.name("$PTmpRevenuePg$pClientDetails$pRevenuePercentages$ga$pPercentageValue");
	By Auswide_RevenueBreakDown_NSW=By.name("$PTmpRevenuePg$pClientDetails$pRevenuePercentages$gb$pPercentageValue");
	By Auswide_RevenueBreakDown_NT=By.name("$PTmpRevenuePg$pClientDetails$pRevenuePercentages$gc$pPercentageValue");
	By Auswide_RevenueBreakDown_QLD=By.name("$PTmpRevenuePg$pClientDetails$pRevenuePercentages$gd$pPercentageValue");
	By Auswide_RevenueBreakDown_SA=By.name("$PTmpRevenuePg$pClientDetails$pRevenuePercentages$ge$pPercentageValue");
	By Auswide_RevenueBreakDown_TAS=By.name("$PTmpRevenuePg$pClientDetails$pRevenuePercentages$gf$pPercentageValue");
	By Auswide_RevenueBreakDown_VIC=By.name("$PTmpRevenuePg$pClientDetails$pRevenuePercentages$gg$pPercentageValue");
	By Auswide_RevenueBreakDown_WA=By.name("$PTmpRevenuePg$pClientDetails$pRevenuePercentages$gh$pPercentageValue");
	By Submit_UWDialougeBox=By.xpath("//button[text()='  Submit ']");
	By tabCloseButton = By.xpath("//span[@id='close']");



	public void sendAppObject(ClientAdviser clientadv) {
		this.clientadv = clientadv;
	}

	public void closeAllInteractions() throws Exception {
		SeleniumFunc.waitForElementToClikable(By.xpath(closeall_anchor),10);
		SeleniumFunc.xpath_GenericMethod_Click(closeall_anchor);
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(closeall);
	}

	public void goToSearchAndSelectHistoricpolicy() throws Exception {
		SeleniumFunc.clickElementUsingActions(By.xpath(myWork));
		SeleniumFunc.waitFor(4);
		SeleniumFunc.clickElementUsingActions(By.xpath(searchLeftTab));
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(historicalPolicy);

	}

	public void validateWarningMessageBySelectingOnlyOneField(String actualMsg, Integer index) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(policyTypeDd,index);
		SeleniumFunc.name_GenericMethod_Click(searchBtnName);
		SeleniumFunc.waitFor(1);
		String expectedMsg = driver.findElement(By.xpath(warMsg)).getText();
		System.out.println("Expected Message: "+expectedMsg);
		System.out.println("Actual Message: "+actualMsg);
		Assert.assertEquals(actualMsg,expectedMsg);
	}

	public void selectMinTwoFields(Integer index, String year) throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingIndexbyclickingOnDropdown(policyTypeDd,index);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(policyYearTf,year);
	}

	public void verifySearchFunction() throws Exception {
		SeleniumFunc.name_GenericMethod_Click(searchBtnName);
		List<WebElement> recordList = SeleniumFunc.getElements(By.xpath(listOfRecords));
		Assert.assertTrue(recordList.size()>0);


	}

	public void clickNewButtonForEndorsemanrText() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(newButton);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.clickElementUsingActions(By.xpath(endorsementManagement));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(editEndorsementText);
	}

	public void clickNewButtonForDeactivateEndorsement() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(newButton);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.clickElementUsingActions(By.xpath(endorsementManagement));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(deactivateEndorsement);
	}

	public void clickNewButtonForBulkAddEndorsementText() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(newButton);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.clickElementUsingActions(By.xpath(endorsementManagement));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(bulkAddEndorsementText);
	}

	public void selectAllTheDetails(String startDate, String productID, String clientCategory) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(datereview);
//		SeleniumFunc.xpath_GenericMethod_Click(todayLink);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(current_date)));
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(productIDDdName,productID);
		SeleniumFunc.xpath_GenericMethod_Click(clientCategoryImg);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click("//*[text()='"+clientCategory+"']");
	}

	public void clickSearchAndContinue() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(bulkTextSearch);
		SeleniumFunc.waitFor(4);
//		driver.findElement(By.cssSelector(firstCheckbox)).click();
		SeleniumFunc.actionClick(By.cssSelector(firstCheckbox));
		SeleniumFunc.name_GenericMethod_Click(continueBtnName);
	}

	public void addendorsementTextAndContinue(String endtext) throws Exception {
		driver.switchTo().frame(driver.findElement(By.cssSelector(endorsementFrameTextfield)));
		SeleniumFunc.xpath_GenericMethod_Clear(endorsementTextfield);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(endorsementTextfield,endtext);
		SeleniumFunc.xpath_GenericMethod_Click(Reapply_No);
		SeleniumFunc.xpath_GenericMethod_Click(continueBtn);
	}

	public void addCommentsAndClickFinish(String comments) throws Exception {
		SeleniumFunc.name_GenericMethod_Sendkeys(updateOrg_govern_change_comments,comments);
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
	}

	public void selectPolicyAndEndorsement() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(policyCheckBox);
		SeleniumFunc.xpath_GenericMethod_Click(continueBtn);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(policyCheckBox);
		SeleniumFunc.xpath_GenericMethod_Click(continueBtn);
	}

	public void editEndorsementtext(String endText) throws Exception {
		driver.switchTo().frame(driver.findElement(By.cssSelector(endorsementFrameTextfield)));
		SeleniumFunc.xpath_GenericMethod_Clear(endorsementTextfield);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(endorsementTextfield,endText);
		SeleniumFunc.xpath_GenericMethod_Click(continueBtn);
	}

	public void selectReviewer(String reviewer,String comments) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(commentsTextfield,comments);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(reviewerTextfield,reviewer);
		SeleniumFunc.xpath_GenericMethod_Click(sumitBtn);
	}

	public void fetchEndorseID() throws Exception {
		SeleniumFunc.waitFor(2);
		String endorsementid = null;
		endorsementid=SeleniumFunc.getElement(By.xpath(endorseText)).getText();
		System.out.println("fetching the fnolID "+endorsementid );
		String endorseid = endorsementid.substring(3).trim();
		System.out.println("fetching the endorsementid "+endorseid );
		testdata.setEndorseId(endorseid);
	}

	public void searchEndorsementID() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(HomePageLoc.vmia_logo);
		SeleniumFunc.xpath_GenericMethod_Click(UW_Casenumber_TH);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(filterOption)));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(uw_applyfilter);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(UW_search_text, testdata.getEndorseId());
		SeleniumFunc.xpath_GenericMethod_Click(UW_search_applyfilter);
		Thread.sleep(10000);
		SeleniumFunc.waitForElementToClikable(By.xpath(uw_clickcase),40);
		SeleniumFunc.xpath_GenericMethod_Click(uw_clickcase);
	}

	public void requestRegejected(String status) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click("//label[text()='"+status+"']");
		try{
			SeleniumFunc.xpath_GenericMethod_Sendkeys(rejectComments,status);
		}catch(Exception e){}
		SeleniumFunc.xpath_GenericMethod_Click(submitBtn);

	}

	public void addFlag(String flag) throws Exception {
		if(flag.equalsIgnoreCase("Yes")){
			SeleniumFunc.xpath_GenericMethod_Click(new_org_Yes);
		}
		else{
			SeleniumFunc.xpath_GenericMethod_Click(new_org_No);
		}
	}


	public void verifyFlag(String flag) throws Exception {
		String ActualFlag = SeleniumFunc.getElementText(actual_vgrmf_flag);
		System.out.println("Actual VGRMF Flag : " +ActualFlag + "----- Expected VGRMF Flag : " + flag);
		Assert.assertEquals(ActualFlag, flag);
	}

	public void clickNewInteraction() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(create_new_advisor_button);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(interaction_button);
	}

	public void clickMakeClaim() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(create_new_advisor_button);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(make_a_claim_button);
		SeleniumFunc.waitFor(2);
	}

	public void createOrgFromDataSheet(String sheetname) throws Exception {

		CSVFileReader fileReader=new CSVFileReader();
		List<HashMap<String,String>> data=fileReader.ReadAllDataAsListOfMap(sheetname);
//		List<String> paywayNumber = new ArrayList<String>();
		HashMap<String,String> Test = data.get(1);
		for(HashMap testdata:data) {
			clickNewAndSelectOptions("Manage organisation");
			enterTheOrgnameForSearch(testdata.get("OrgnizationName").toString());
			List<WebElement> rows = driver.findElements(By.xpath("//tr[contains(@oaargs, 'VMIA-Data-Party-Org-Company')]"));
			if(rows.size()>0) {
				System.out.println("Organization already Exist");
			}else {

				SeleniumFunc.xpath_GenericMethod_Click(new_org_create_new);
				SeleniumFunc.waitFor(3);
				clickEnterTheNewOrgDetails(testdata.get("ClientSegment").toString(), testdata.get("OrgnizationType").toString(), testdata.get("GovernmentDepartment").toString(), testdata.get("Profession").toString(), testdata.get("PhoneNumber").toString(), testdata.get("Address").toString());
				String paywayText = SeleniumFunc.getElementText("//div[contains(text(),'finance team to issue payway account number')]");
				Pattern p = Pattern.compile("[A-Z]{2}-([0-9]){3}");
				Matcher m = p.matcher(paywayText);
				m.matches();
				if(m.find()) {
					System.out.println("Control comes here");
					TestBase.paywayNumber.add(m.group(0));
					System.out.println("Payway number"+m.group(0));
				}
				closeCurrentInteraction();

				}

			}
		for(int i=0 ; i<TestBase.paywayNumber.size();i++){
			System.out.println(TestBase.paywayNumber.get(i));

		}
		closeCurrentTab();
		clickNewAndSelectOptions("Manage contact");

			searchContactUsingName(Test.get("FirstName"), Test.get("LastName"));

		for(HashMap testdata:data) {
			enterOrgDetailsInContact(testdata.get("OrgnizationName").toString(), "Insurance;Finanace");
		}
		clickSubmitInOrgDetails();
	}


	public void clickNewAndSelectOptions(String Options) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
//		SeleniumFunc.waitForElementToClikable(By.xpath(create_new_advisor_button),10);
		SeleniumFunc.xpath_GenericMethod_Click(create_new_advisor_button);
		SeleniumFunc.waitForElementToClikable(By.xpath("//ul[@class='menu menu-format-standard menu-regular']//span[text()='"+Options+"']/ancestor::a"),60);
//		SeleniumFunc.waitFor(10);
		SeleniumFunc.xpath_GenericMethod_Click("//ul[@class='menu menu-format-standard menu-regular']//span[text()='"+Options+"']/ancestor::a");
		SeleniumFunc.waitFor(2);
	}

	public void clickNewButtonForCancelPolicy(String type) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(newButton);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.clickElementUsingActions(By.xpath(cancellationOption));
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+type+"']");
	}

	public void selectPolicy() throws Exception {
		SeleniumFunc.waitFor(2);
//		String plo = testdata.getPolicynumber();
		System.out.println("The policy which we are selected is: "+testdata.getPolicynumber());
		SeleniumFunc.name_GenericMethod_Sendkeys(searchPolicyTf,testdata.getPolicynumber());
		SeleniumFunc.xpath_GenericMethod_Click(create_policyBundle_find);
		SeleniumFunc.waitFor(8);
		SeleniumFunc.xpath_GenericMethod_Click(policyCheckBox);
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
	}

	public void selectEndorsementID() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(policyCheckBox);
		SeleniumFunc.xpath_GenericMethod_Click(continueBtn);
	}

	public void deactivatingReason(String dereason) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Asset_update_comments,dereason);
		SeleniumFunc.xpath_GenericMethod_Click(Finish_1);
	}

	public void cancelPolicyDetails(String reason) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click("//label[text()='Yes']");
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(cancelReasonDP,reason);
		SeleniumFunc.xpath_GenericMethod_Click(create_continueButton);
	}

	public void acceptQuote() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(checkQuote);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.click(By.xpath(Finish_1));
	}

	public void clickCancelPolicyAndSelectOptions(String Options) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(create_new_advisor_button);
		SeleniumFunc.waitFor(2);
		WebElement element=SeleniumFunc.getElement(By.xpath("//ul[@class='menu menu-format-standard menu-regular']//span[text()='Cancellation']/ancestor::a"));
		Actions action=new Actions(driver);
		action.moveToElement(element).build().perform();
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click("//li[@class='menu-item menu-item-enabled menu-item-active']//ul//li/a/span/span[text()='"+Options+"']/ancestor::a");
	}

	public void clickPolicyBundlePackage() throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(Policy_bundle_package);
	}

	public WebElement getAttachMailDocumentElement() throws Exception {
		SeleniumFunc.waitFor(7);
		SeleniumFunc.xpath_GenericMethod_Click(policy_bundle_package_attachmenttab);
		SeleniumFunc.waitFor(4);
		SeleniumFunc.xpath_GenericMethod_Click(policy_bundle_attachment);
		return getElement(By.xpath(policy_bundle_attachment));
	}

	public void validateProductList(String prodctype) throws Exception {
		SeleniumFunc.waitFor(2);
		for(Object product:testdata.getCSOBundleProducts(prodctype)){
			System.out.println("Product :"+product);
			SeleniumFunc.getTextFromElement(By.xpath("//div[@node_name='BundleProducts']/div/div/span[text()='"+product.toString()+"']"));
		}
		SeleniumFunc.xpath_GenericMethod_Click("//button[text()='Continue']");
	}



	public void enterOrgnameAndSelectBundlePolicy() throws Exception {
		SeleniumFunc.waitFor(6);
//		testdata.setOrgname("Csoes-3439");
		SeleniumFunc.name_GenericMethod_Sendkeys(create_policyBundle_Orgname,testdata.getOrgname());
		SeleniumFunc.enterValue(By.name(create_policyBundle_Orgname),Keys.TAB);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(create_policyBundle_find);
//		testdata.setOrgname("Csoes-6735");
		SeleniumFunc.waitForElementToClikable(By.xpath("//label[text()='"+testdata.getOrgname()+"']"),15);
		SeleniumFunc.xpath_GenericMethod_Click("//label[text()='"+testdata.getOrgname()+"']");

	}

	public void searchOrgByClientSegmentBundleTypeAndOrgName(String clientSegment,String bundleType) throws Exception {
		SeleniumFunc.waitFor(4);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(create_policyBundle_clientSegment,clientSegment);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(create_policyBundle_bundleType,bundleType);
		SeleniumFunc.waitInMilliSeconds(2);
		enterOrgnameAndSelectBundlePolicy();
//		try{
//			validateProductList("CSOEducation");
//		}catch(Exception e){
//			validateProductList("Directors and Officers Liability incorporating Entity Liability");
//		}

	}

	public void searchOrgByClientSegmentAndOrgName(String clientSegment,String BundleType) throws Exception {
		SeleniumFunc.waitFor(7);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(create_policyBundle_clientSegment,clientSegment);
		SeleniumFunc.waitForElementToClikable(By.name(bundleType),20);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(bundleType,BundleType);
		enterOrgnameAndSelectBundlePolicy();
		validateProductList(BundleType);
	}

	public void AddUnicorpOrgAndSubmitPolicy(String UnicorpOrg,String EffectiveDate) throws Exception {
		SeleniumFunc.clickElement(Expand_Org_BundlePolicy);
		SeleniumFunc.waitInMilliSeconds(3);
		SeleniumFunc.clickElement(By.xpath(BundlePolicy_AddIncorpratedBtn));
		SeleniumFunc.waitInMilliSeconds(5);
		SeleniumFunc.clickElement(By.xpath(Bundlepolicy_AddIncorprateAddnew));
		SeleniumFunc.waitInMilliSeconds(2);
		SeleniumFunc.enterValue(By.name(Bundlepolicy_UnIncorprateOrg),UnicorpOrg);
		SeleniumFunc.clickElement(By.xpath(create_UnincorprateOrg_DoneButton));
//		SeleniumFunc.enterValue(BundlePolicy_PolicyEffectiveDate,EffectiveDate);
		SeleniumFunc.clickElement(By.xpath(effectivedatePicker));
		SeleniumFunc.clickElement(By.xpath(today_link));
		SeleniumFunc.clickElement(By.xpath(finish_button));
		SeleniumFunc.waitFor(30);
	}

	public void VerifyTheUnicorpOrgAndSubmitPolicy(String UnicorpOrg) throws Exception {
		SeleniumFunc.waitFor(2);
		Assert.assertEquals(getElement(By.xpath(getCreate_policyBundle_Unincorprateextorg)).getText(),UnicorpOrg);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(create_Unicorprate_effictivedate,"11/06/2021");
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
	}


	public void enterRevenueAndEffetiveDate(String Revneue,String Effectivedate) throws Exception {
//		SeleniumFunc.clickElement(Expand_Org_BundlePolicy);
		SeleniumFunc.waitInMilliSeconds(3);
		SeleniumFunc.enterValue(BundlePolicy_Revenue,Revneue);
		SeleniumFunc.enterValue(BundlePolicy_PolicyEffectiveDate,Effectivedate);
		SeleniumFunc.clickElement(By.xpath(finish_button));
	}

	public void enterRevenueBreakdown() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(BundlePolicy_AddIncorpratedBtn);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.enterValue(Auswide_RevenueBreakDown_ACT,"10");
		SeleniumFunc.waitInMilliSeconds(3);
		SeleniumFunc.enterValue(Auswide_RevenueBreakDown_NSW,"10");
		SeleniumFunc.waitInMilliSeconds(3);
		SeleniumFunc.enterValue(Auswide_RevenueBreakDown_NT,"20");
		SeleniumFunc.waitInMilliSeconds(3);
		SeleniumFunc.enterValue(Auswide_RevenueBreakDown_QLD,"10");
		SeleniumFunc.waitInMilliSeconds(3);
		SeleniumFunc.enterValue(Auswide_RevenueBreakDown_SA,"20");
		SeleniumFunc.waitInMilliSeconds(3);
		SeleniumFunc.enterValue(Auswide_RevenueBreakDown_WA,"10");
		SeleniumFunc.waitInMilliSeconds(3);
		SeleniumFunc.enterValue(Auswide_RevenueBreakDown_VIC,"10");
		SeleniumFunc.waitInMilliSeconds(3);
		SeleniumFunc.enterValue(Auswide_RevenueBreakDown_TAS,"10");
		SeleniumFunc.waitInMilliSeconds(3);
		SeleniumFunc.clickElement(Submit_UWDialougeBox);

	}




	public String getUnincorpratedOrgFromOrgnization() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		return SeleniumFunc.getElement(By.xpath(updateOrg_UnicorprateName)).getText();
	}

	public void enterTheOrgnameForSearch() throws Exception {

		testdata.setOrgname("Csoes"+SeleniumFunc.generateRandomnumber());
		SeleniumFunc.name_GenericMethod_Sendkeys(new_Org_orgname,testdata.getOrgname());
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(new_org_searchbutton);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(new_org_create_new);
	}

	public void enterTheOrgnameForSearch(String orgname) throws Exception {

		SeleniumFunc.name_GenericMethod_Sendkeys(new_Org_orgname,orgname);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(new_org_searchbutton);
		SeleniumFunc.waitFor(2);

	}

	public void verifyOrgTypeDropdownforClientSegment(String clientSegment,String orgtype) throws Exception {
		SeleniumFunc.waitFor(2);
		String[] dropDownoption=new String[]{"Select","Section 25A","Core Client","Section 4","Stakeholder","VicFleet","Fund","Prospect","Vendor"};
		List<String> list= Arrays.asList(dropDownoption);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(new_org_clentSegment,clientSegment);
		SeleniumFunc.waitFor(2);
		System.out.println("DropDown values"+SeleniumFunc.GetOptionsFromDropdown(By.name(new_org_clientType)));
		Assert.assertTrue(list.equals(SeleniumFunc.GetOptionsFromDropdown(By.name(new_org_clientType))));
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(new_org_clientType,orgtype);
	}

	public void enterTheOrgnameAndClickUpdate() throws Exception {
		SeleniumFunc.switchToDefaultContent();
//		TestBase.Orgname="Csoes-2985";
		SeleniumFunc.name_GenericMethod_Sendkeys(new_Org_orgname,testdata.getOrgname());
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(new_org_searchbutton);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(searchOrg_update);
	}

	public void updateOrg(String governchange,String IsOrgMerged,String effdate,String comments,String mergeOrgList) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(4);
		if(governchange.equalsIgnoreCase("Yes"))
			SeleniumFunc.xpath_GenericMethod_Click(updateorg_governChange_yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(updateorg_governChange_no);
		SeleniumFunc.waitFor(1);
		if(IsOrgMerged.contains("Split")){

			SeleniumFunc.xpath_GenericMethod_Click(updateorg_isOrgMerged_split);
			Thread.sleep(1500);
			SeleniumFunc.enterMultipleValueToComboBox(updateorg_SplitOrgList,mergeOrgList);

		}else if(IsOrgMerged.contains("Merged")){
			SeleniumFunc.xpath_GenericMethod_Click(updateorg_isOrgMerged_merge);
			Thread.sleep(1500);
			SeleniumFunc.enterTheValueInComboBoxUsingXpath(updateorg_MergedOrg,mergeOrgList);
		}else { SeleniumFunc.xpath_GenericMethod_Click(updateorg_isOrgMerged_no); }
		Thread.sleep(4);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(updateOrg_effectivedate,effdate);
		Thread.sleep(900);
		SeleniumFunc.name_GenericMethod_Sendkeys(updateOrg_govern_change_comments,comments);
	}

	public void clickSubmitInOrgDetails() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(updateOrg_submit);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click("//span[@class='iconCloseSmall']");
	}


	public void AddUnincorpraorgInOrg(String orgname) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(updateOrg_unincorprateorg_add);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(updateOrg_unincorprateorg_name,orgname);
		SeleniumFunc.xpath_GenericMethod_Click(updateOrg_unincorprateorg_active);
		SeleniumFunc.xpath_GenericMethod_Click(updateOrg_unincorprateorg_donebutton);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(updateOrg_submit);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click("//span[@class='iconCloseSmall']");
	}


	public void searchOrgForBundleCancallation(String clientSegment) throws Exception {
//		TestBase.Orgname="Csoes-9382";
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(create_policyBundle_clientSegment,clientSegment);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(create_policyBundle_Orgname,testdata.getOrgname());
		SeleniumFunc.xpath_GenericMethod_Click(create_policyBundle_find);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(cancellation_select_org);
		SeleniumFunc.xpath_GenericMethod_Click(create_continueButton);
	}

	public void searchOrgForBundleCancallation(String clientSegment, String Orgname) throws Exception {
//		TestBase.Orgname="Csoes-9382";
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(create_policyBundle_clientSegment,clientSegment);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.name_GenericMethod_Sendkeys(create_policyBundle_Orgname,Orgname);
		SeleniumFunc.xpath_GenericMethod_Click(create_policyBundle_find);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(cancellation_select_org);
		SeleniumFunc.xpath_GenericMethod_Click(create_continueButton);
	}

	public void selectContactAndProduct(String product) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(cancellation_bundle_SelectContact);
		SeleniumFunc.xpath_GenericMethod_Click(create_continueButton);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.switchToDefaultContent();
//		SeleniumFunc.xpath_GenericMethod_Click(cancellation_bundle_productselection);
		SeleniumFunc.xpath_GenericMethod_Click(cso_bundle_productselection);
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
	}

	public void closeCurrentInteraction() throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click("(//span[@class='iconCloseSmall'])[last()]");
	}
	public void clickEnterTheNewOrgDetails(String clientSegment,String orgType,String goverdep, String profession, String phonenumber,String address) throws Exception {
		SeleniumFunc.waitFor(4);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(new_org_clentSegment,clientSegment);
		SeleniumFunc.waitForElementToClikable(By.name(new_org_clientType),10);
		SeleniumFunc.waitFor(2);
		if(!orgType.equals(""))
			SeleniumFunc.name_SelectFromDropdownUsingVisibleText(new_org_clientType,orgType);
		SeleniumFunc.waitForElementToClikable(By.name(new_org_government_dep),10);
		Thread.sleep(500);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(new_org_government_dep,goverdep);
		Thread.sleep(900);
		SeleniumFunc.name_GenericMethod_Sendkeys(new_org_profession,profession);
		SeleniumFunc.waitInMilliSeconds(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(new_org_phonenumeber,phonenumber);
		SeleniumFunc.waitInMilliSeconds(2);
		SeleniumFunc.enterTheValueInComboBox(new_org_search_address,address);
		SeleniumFunc.waitInMilliSeconds(2);
		SeleniumFunc.xpath_GenericMethod_Click(new_org_create);

	}


	public void clickEnterTheNewOrgDetails(String clientSegment, String goverdep, String profession, String phonenumber,String address) throws Exception {
		SeleniumFunc.waitFor(4);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(new_org_clentSegment,clientSegment);
		Thread.sleep(500);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(new_org_government_dep,goverdep);
		Thread.sleep(900);
		SeleniumFunc.name_GenericMethod_Sendkeys(new_org_profession,profession);
		SeleniumFunc.waitInMilliSeconds(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(new_org_phonenumeber,phonenumber);
		SeleniumFunc.waitInMilliSeconds(2);
		SeleniumFunc.enterTheValueInComboBox(new_org_search_address,address);
		SeleniumFunc.waitInMilliSeconds(2);
		SeleniumFunc.xpath_GenericMethod_Click(new_org_create);
		closeCurrentInteraction();
	}

	public void enterTheNewOrgDetails(String clientSegment,String goverdep, String profession, String phonenumber,String address, String govfundep) throws Exception {
		SeleniumFunc.waitFor(4);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(new_org_clentSegment,clientSegment);
		Thread.sleep(500);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(new_org_government_dep,goverdep);
		Thread.sleep(900);
		try {
			SeleniumFunc.name_SelectFromDropdownUsingVisibleText(new_org_gov_fun_dep, govfundep);
		}catch(Exception e){

		}
		SeleniumFunc.waitInMilliSeconds(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(new_org_profession,profession);
		SeleniumFunc.waitInMilliSeconds(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(new_org_phonenumeber,phonenumber);
		SeleniumFunc.waitInMilliSeconds(2);
		SeleniumFunc.enterTheValueInComboBox(new_org_search_address,address);
		SeleniumFunc.waitInMilliSeconds(2);
		SeleniumFunc.xpath_GenericMethod_Click(new_org_create);
		SeleniumFunc.waitFor(80);
	}


	public void searchContactByContactId(String contactid) throws Exception {
		SeleniumFunc.waitFor(8);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(searchContact_ContactId,contactid);
		try{
			driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@name='PegaGadget2Ifr']")));
			System.out.println("Its try");
		}catch(Exception e){
			System.out.println("Its catch");
			SeleniumFunc.switchToDefaultContent();
		}
		SeleniumFunc.waitFor(10);
//		JavascriptExecutor executor = (JavascriptExecutor) driver;
//		executor.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.name(searchContact_ContactIdName)));
		SeleniumFunc.name_GenericMethod_Sendkeys(searchContact_ContactIdName,contactid);
		SeleniumFunc.name_GenericMethod_Click(searchContact);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(searchContact_update);
	}

	public void updateOrgDetailsInContact(String orgname,String accounttype) throws Exception {
		enterOrgDetailsInContact(orgname,accounttype);
		clickSubmitInContact();
	}

	public void enterOrgDetailsInContact(String orgname,String accounttype) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(updateContact_addorg);
		SeleniumFunc.waitFor(2);
		List<WebElement> orgele=SeleniumFunc.getElements(updateContact_Orgname);
		if(!orgname.equalsIgnoreCase(""))
			SeleniumFunc.enterTheValueInComboBoxUsingXpath(updateContact_Orgname+"["+orgele.size()+"]",orgname);
		else
			SeleniumFunc.enterTheValueInComboBoxUsingXpath(updateContact_Orgname+"["+orgele.size()+"]",testdata.getOrgname());
		System.out.println("AccountType;"+accounttype);
		SeleniumFunc.waitForElementToClikable(By.xpath(updateContact_orgType + "[" + orgele.size() + "]"),10);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.enterMultipleValueToComboBox(updateContact_orgType + "[" + orgele.size() + "]",accounttype);
	}

	public void clickSubmitInContact() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(Update_contact_submit);
	}


	public void searchContactUsingName(String firstname,String Lastname) throws Exception {
		SeleniumFunc.waitFor(2);

		SeleniumFunc.enterValue(contactSerchFirstName,firstname);

		SeleniumFunc.enterValue(contactSerchLastName,Lastname);
		SeleniumFunc.clickElement(SearchContactButton);
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click(searchContact_update);
	}

	public void selectFromUWQueue(String Q_value) throws Exception {
		waitFor(2);
		SeleniumFunc.scrollDownVertically();
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(My_WorkBusket_clv);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(My_WorkBusket_clv_Dropdown, Q_value);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtWorkbasketSearchBox, testdata.getCaseId());
		SeleniumFunc.xpath_GenericMethod_Click(imgWorkbasketSearchBoxSearchIcon);
		waitFor(10);
		SeleniumFunc.xpath_GenericMethod_Click(UW_case_Link);

	}

	public void selectFromClAdvUW() throws Exception {
		waitFor(2);
//		SeleniumFunc.xpath_GenericMethod_Click(Approve_clAdv_UW);
		SeleniumFunc.xpath_GenericMethod_Click(Approve_clAdv_UV2);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Approve_clAdv_UW_Submit);

	}

	public void clickNewApp() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(create_new_advisor_button);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(new_app_button);
	}

	public void clickCreateNewOrganisation() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(create_new_advisor_button);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(create_new_organisation_button);
	}

	public void clickPolicyBundle() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(create_new_advisor_button);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(create_policy_bundle_button);
	}

	public void clickUpdatePolicy() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(create_new_advisor_button);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(update_policy_button);
	}

	public void policybundledetails(String clientname, String revenue) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(client_name_bundle_textbox, clientname);
		SeleniumFunc.xpath_GenericMethod_Click(find_policy_bundle_button);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(select_all_clients_policy_bundle);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Clear(bundle_revenue);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(bundle_revenue, revenue);
		SeleniumFunc.xpath_GenericMethod_Click(date_time_of_loss);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);

		// SeleniumFunc.xpath_GenericMethod_Click(update_policy_button);
	}

	public void policybundledetailsMultipleClients(String revenue) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(find_policy_bundle_button);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(bun);
		SeleniumFunc.xpath_GenericMethod_Click(bundle_select_client_1);
		SeleniumFunc.xpath_GenericMethod_Click(bun1);
		SeleniumFunc.xpath_GenericMethod_Click(bundle_select_client_2);
		SeleniumFunc.xpath_GenericMethod_Click(bun);
		SeleniumFunc.xpath_GenericMethod_Click(bundle_select_client_1);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Clear(bundle_revenue_1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(bundle_revenue_1, revenue);
		SeleniumFunc.xpath_GenericMethod_Clear(bundle_revenue_2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(bundle_revenue_2, revenue);
		SeleniumFunc.xpath_GenericMethod_Click(datepicker_1);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);
		SeleniumFunc.xpath_GenericMethod_Click(datepicker_2);
		SeleniumFunc.xpath_GenericMethod_Click(today_link);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);

		// SeleniumFunc.xpath_GenericMethod_Click(update_policy_button);
	}

	public void verfyPackageCase() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(package_case);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(begincase);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(submit);

		// SeleniumFunc.xpath_GenericMethod_Click(update_policy_button);
	}

	public void verifyPolicyBundleDetails() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(policy_bundle_details);
		SeleniumFunc.waitFor(1);
		String gpa = xpath_Genericmethod_getElementText(bundle_GPA_Link);

		SeleniumFunc.writeToAssertionFile("Expected format : GPA" + " , Actual status : " + gpa.substring(0, 3),
				("GPA".equals(gpa.substring(0, 3)) ? "PASS" : "FAIL"));
		Assert.assertTrue(gpa.startsWith("GPA"));

		String CBL = xpath_Genericmethod_getElementText(bundle_CBL_Link);

		SeleniumFunc.writeToAssertionFile("Expected format : GPA" + " , Actual status : " + gpa.substring(0, 3),
				("GPA".equals(gpa.substring(0, 3)) ? "PASS" : "FAIL"));
		Assert.assertTrue(gpa.startsWith("GPA"));

		// SeleniumFunc.xpath_GenericMethod_Click(update_policy_button);
	}

	public void verifyBundleStatus(String expectedstatus) throws Exception {

		String casestatusscreen = xpath_Genericmethod_getElementText(policy_bundle_status);

		SeleniumFunc.writeToAssertionFile(
				"Expected case status : " + expectedstatus + " , Actual status : " + casestatusscreen,
				(expectedstatus.equals(casestatusscreen) ? "PASS" : "FAIL"));
		Assert.assertTrue(casestatusscreen.equals(expectedstatus));

	}

	public void selectOrgToStartInteraction(String orgname) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(search_by_org_textbox, orgname);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(search_button);
		SeleniumFunc.waitFor(1);
		// SeleniumFunc.xpath_GenericMethod_Click(search_row);
		// SeleniumFunc.waitFor(1);
		// SeleniumFunc.xpath_GenericMethod_Click(select_row_button);
		// SeleniumFunc.waitFor(1);
		// SeleniumFunc.xpath_GenericMethod_Click(contact_row);
		// SeleniumFunc.waitFor(5);
		// SeleniumFunc.xpath_GenericMethod_Click(select_row_button);
		// SeleniumFunc.waitFor(15);
	}
	
	public void SelectOrgNameForAddingAsset(String orgname) throws Exception {

		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Sendkeys(Asset_orgname, orgname);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_org_findbutton);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_org_search_result_radio);
	}
	
	public void selectOptionInAsset(String option) throws Exception {
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click("//span[@class='menu-item-title' and text()='"+option+"']");
	}

	public void AddLocationAndVerifyTheCrestazone(String postalcode, String crestazone) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.name_GenericMethod_Click(Asset_AddLocationbtn);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_Cannot_FindyouLocation);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Asset_postalCode,postalcode);
		Thread.sleep(500);
		SeleniumFunc.getElement(By.xpath(Asset_postalCode)).sendKeys(Keys.TAB);
		SeleniumFunc.waitFor(2);
		Assert.assertEquals(getElement(By.xpath(Asset_CrestaZone)).getText(),crestazone);

	}

	public void searchAssetAndOpenHistory(String assetid,String actionOptions) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		if(assetid.equalsIgnoreCase(""))
			SeleniumFunc.name_GenericMethod_Sendkeys(SearchAsset_Assetid, TestBase.AssetID);
		else
			SeleniumFunc.name_GenericMethod_Sendkeys(SearchAsset_Assetid, assetid);
		SeleniumFunc.xpath_GenericMethod_Click(SearchAsset_Assetsearh);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(SearchAsset_Actions);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click("//span[@class='menu-item-title-wrap']/span[text()='"+actionOptions+"']");
		SeleniumFunc.waitFor(10);

	}


	public boolean verifyAssetDetailsFieldDisplayedInAssetNameSection(String field) throws Exception {
		return SeleniumFunc.verifyElementIsPresent("(//div[@class='layout layout-noheader layout-noheader-case_secondary_content']//span[text()='"+field+"'])");
	}

	public String getAssetDetailsFieldValuesFromAssetNameSection(String field) throws Exception {
		return SeleniumFunc.getElementText("//div[@class='layout layout-noheader layout-noheader-case_secondary_content']//span[text()='"+field+"']//following-sibling::div");
	}


	public void validateAssetInfoHistory(String assetname,String AssetType) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(5);
		List<WebElement> history=SeleniumFunc.getElements("//table[@pl_prop='D_AssetHistory.pxResults']//tr/td[3]");
		System.out.println("history :"+history);
		SoftAssert asserts=new SoftAssert();
		asserts.assertEquals(history.get(1).getText(),"Added \"Asset type\" '"+AssetType+"'");
		asserts.assertEquals(history.get(2).getText(),"Added \"Asset name\" '"+assetname+"'");
		asserts.assertEquals(history.get(3).getText(),"Added \"Asset ID\" '"+TestBase.AssetID+"'");
		asserts.assertAll();
	}
	
	public void selectContactAssociationWithProperty(String association) throws Exception {
		if(association.equalsIgnoreCase("Yes"))
		SeleniumFunc.xpath_GenericMethod_Click(Asset_contact_assoc_yes);
		else
			SeleniumFunc.xpath_GenericMethod_Click(Asset_contact_assoc_No);
		
	}
	public void clickcontiune() throws Exception {

		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
	}
	
	public void addAssetLocationAndType(String Location, String Assetype, String assetname, String Band) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.name_GenericMethod_Click(Asset_AddLocationbtn);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.enterTheValueInComboBox(Asset_Searchaddress, Location);
		Thread.sleep(800);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_Addlocation_submit);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_AddAsset);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Asset_AddPropertytype, Assetype);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Asset_Assetname, assetname);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_AddDetails);
		addAssetDetail("1997/1998","24/06/2021");

	}
	public void addAssetDetail(String valuetionYear,String Valuationdate) throws Exception {
		SeleniumFunc.waitFor(5);
		SeleniumFunc.xpath_SelectFromDropdownUsingVisibleText(Asset_valution_year, valuetionYear);
		Thread.sleep(500);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Asset_valution_date,Valuationdate);
//		SeleniumFunc.xpath_GenericMethod_Click(Asset_PricentDetails_Expand);
//		SeleniumFunc.waitInMilliSeconds(10);
//		SeleniumFunc.name_GenericMethod_Sendkeys(Asset_Pricent_id,"12121");
//		SeleniumFunc.waitFor(2);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(Asset_pricent_name,"12111121");
//		SeleniumFunc.waitInMilliSeconds(20);
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(Asset_pricent_name,"Test");
		SeleniumFunc.waitInMilliSeconds(50);
		SeleniumFunc.actionClick(By.xpath(Asset_Addlocation_submit));
		SeleniumFunc.waitInMilliSeconds(10);
	}
	
	public void AttachTheBulkUploadFile(String filepath) throws Exception{
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_BulkUpload_button);
		SeleniumFunc.waitFor(8);
		WebElement select=driver.findElement(By.xpath(Asset_Select_file));
		Actions action=new Actions(driver);
		action.moveToElement(select).click(select).build().perform();
		SeleniumFunc.waitFor(5);
		String filePath="\""+System.getProperty("user.dir")+"\\src\\main\\resources\\Attachments\\"+filepath+"\"";
//		URI u = new URI(filePath.trim().replaceAll("\\u0020", "%20"));
//		System.out.println("FielUploade path :"+System.getProperty("user.dir")+"/src/main/resources/Attachments/"+filepath);
		Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\src\\main\\resources\\autoitscript\\fileUpload.exe"+" "+filePath);
		SeleniumFunc.waitFor(10);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_attach_fin_btn);
		SeleniumFunc.waitFor(45);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_final_submit);
	}

	public void clickHamBurgerAndSelectOptions(String Options) throws Exception {


		SeleniumFunc.waitFor(10);
		SeleniumFunc.xpath_GenericMethod_Click(Geo_Hamburger);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+Options+"']");
	}

	public void editCrestazoneAndAddRecord() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Geo_businessrules_edit);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(Geo_businessrules_add);
	}
	public void enterTheCrestaZone(String crestaid,String postalcode,String lowcresid,String highzone,String lowzone) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Geo_businessrules_crestaid,crestaid);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Geo_businessrules_postalCode,postalcode);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Geo_businessrules_lowcrestaid,lowcresid);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Geo_businessrules_highzonename,highzone);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Geo_businessrules_lowzonename,lowzone);
	}


	public void clickFinishButton() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(finish_button);
	}
	
	public void extractAssetIdAndSSubmitTheAsset() throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.switchToDefaultContent();
		String AssetID=SeleniumFunc.getElementTextWithXpath(Asset_expand_assetID);
		System.out.println("Asset id :"+AssetID);
		TestBase.recoveryid=AssetID;
		SeleniumFunc.xpath_GenericMethod_Click(Asset_Confirm_detail_Accurate_yes);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_final_submit);	
	}
	
	public void SearchAsset(String AssetSearchBy,String SearchString) throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.waitFor(5);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(Asset_searchBy, AssetSearchBy);
		SeleniumFunc.waitFor(2);
		if(Asset_searchBy.equalsIgnoreCase("Asset address"))
		SeleniumFunc.name_GenericMethod_Sendkeys(Asset_AssetAddressVal, SearchString);  
		else
		SeleniumFunc.name_GenericMethod_Sendkeys(Asset_AssetypeVal, SearchString);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_Search);		
	}
	
	public void selectAsset(String AssetName) throws Exception {
	   SeleniumFunc.waitFor(5);
	   SeleniumFunc.switchToDefaultContent();
	   List<WebElement> assetSearchResultsName=SeleniumFunc.getElements(Asset_SearchResult_AssetName);
	   System.out.println("Asset Name returned"+assetSearchResultsName.size());
	   for(int i=2;i<assetSearchResultsName.size()+2;i++) {
		   if(assetSearchResultsName.get(i-2).getText().equalsIgnoreCase(AssetName)) {
			   System.out.println("Asset Name"+assetSearchResultsName.get(i-2).getText());
			   SeleniumFunc.xpath_GenericMethod_Click("//table[@pl_prop='TempAssetList.pxResults']/tbody/tr["+i+"]/td[1]//input[@type='checkbox']"); 
			   break;
		   }
		  }
	}
	
	public void UpdateAsset(String AssetName) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Asset_update_comments,"Testcommentes");
		SeleniumFunc.waitFor(1);
		   List<WebElement> assetSearchResultsName=driver.findElements(By.xpath(Asset_SearchResult_SelectedAssetName));
		   System.out.println(" Asset Name"+assetSearchResultsName.get(0).getText());
		   for(int i=2;i<assetSearchResultsName.size()+2;i++) {
			   if(assetSearchResultsName.get(i-2).getText().equalsIgnoreCase(AssetName)) {
//				   System.out.println("text from table "+assetSearchResultsName.get(i).getText()+" AssetName"+AssetName);
				   SeleniumFunc.xpath_GenericMethod_Click("//input[@name='PAssetList1colWidthGBR']/parent::td/div/table//tr["+i+"]/td[4]//a"); 
				   break;
			   }
		   }

		   updateDetail();
	}
	
	public void switchToSelectecAsset() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_SelectedAssets);
		SeleniumFunc.waitFor(2);
	}
	
	public void updateDetail() throws Exception {
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Asset_Client_AssetId, "testdata");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Asset_related_assetid, "testdata");
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(Asset_expand_assetvaluation);
		SeleniumFunc.waitFor(2);
		addAssetDetail("1997/1998","24/06/2021");

	}
	
	
	public void verifyTheStatusAndCaptureCaseID(String expstatus) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(Asset_final_submit);
		verifyStatusAndCaptureCaseId(expstatus);
	}

	public void verifyTheStatusAndCaptureCaseIDForBundlePolicy(String expstatus) throws Exception {
		SeleniumFunc.waitFor(2);
		String actualCaseStatus=SeleniumFunc.getElementTextWithXpath(policyBundle_caseStatus);
		testdata.setCaseId(SeleniumFunc.getElementTextWithXpath(AssetCaseID).substring(2));
		Assert.assertEquals(expstatus,actualCaseStatus);
//		TestBase.currentCaseID=TestBase.currentCaseID.substring(2);
		System.out.println("CaseId"+ testdata.getCaseId());

	}

	public void verifyTheStatusForBundlePolicy(String expstatus) throws Exception {
		SeleniumFunc.waitFor(2);
		String actualCaseStatus=SeleniumFunc.getElementTextWithXpath(policyBundle_caseStatus);
		Assert.assertEquals(expstatus,actualCaseStatus);
//		TestBase.currentCaseID=TestBase.currentCaseID.substring(2);

	}

	public void verifyStatusAndCaptureCaseId(String expstatus) throws Exception {
		SeleniumFunc.waitFor(2);
		String actualCaseStatus=SeleniumFunc.getElementTextWithXpath(CaseStatus);
		testdata.setCaseId(SeleniumFunc.getElementTextWithXpath(AssetCaseID).substring(2));
		Assert.assertEquals(expstatus,actualCaseStatus);
		System.out.println("CaseId"+testdata.getCaseId());
	}

	
	public void linkPolicy(String policy) throws Exception {
		SeleniumFunc.waitFor(3);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_SelectFromDropdownUsingVisibleText(Asset_Link_policy, policy);
		
	}
	
	public void SelectOrgName() throws Exception {

		Thread.sleep(2000);
		SeleniumFunc.xpath_GenericMethod_Click(Org_Name);
		Thread.sleep(2000);
		SeleniumFunc.xpath_GenericMethod_Click(select_row_button);
		Thread.sleep(3000);
		SeleniumFunc.xpath_GenericMethod_Click(Contact_Name);
		Thread.sleep(2000);
		SeleniumFunc.xpath_GenericMethod_Click(select_row_button);
		SeleniumFunc.waitFor(10);

	}

	public void enterOrgName(String orgname) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Clear(new_organisation_name);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(new_organisation_name, orgname);
		SeleniumFunc.xpath_GenericMethod_Click(search_button_client);
		SeleniumFunc.waitFor(1);
	}

	public void addOrverifyIBPSOption() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click("//a[contains(text(),'Update')]");
//		SeleniumFunc.scrollIntoView(driver.findElement(By.xpath("//h3[contains(text(),'Cluster Details')]")));
		SeleniumFunc.clickElementUsingActions(By.xpath("//h3[contains(text(),'Cluster Details')]"));
		String ibpsOption = driver.findElement(By.xpath("//td[contains(text(),'No Data')]")).getText();
		System.out.println("The value is---------------> "+ibpsOption);
		if(ibpsOption.contains("No Data Found")){
			System.out.println("NO IBPS Found");
			SeleniumFunc.name_GenericMethod_Click("UpdateOrgClusterDetails_pyWorkPage.Customer.CustomerInfo_105");
			SeleniumFunc.name_GenericMethod_Sendkeys("$PUpdateOrgClusterPg$pClusterName","IBPS");
			SeleniumFunc.xpath_GenericMethod_Click("//tr//span[text()='IBPS']");
			SeleniumFunc.xpath_GenericMethod_Click("//*[@id='ModalButtonSubmit']");
			SeleniumFunc.xpath_GenericMethod_Click("//button[text()='Submit']");
		}else{
			System.out.println("IBPS Found");
		}
		SeleniumFunc.clickElementUsingActions(By.xpath("//img[@name='CPMInteractionPortalHeaderTop_pyDisplayHarness_1']"));

	}

	public void addGeneralServiceRequest() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(add_task_button);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(general_service_enquiry_button);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(add_task_popup_button);
		SeleniumFunc.waitFor(10);

	}

	public void fillUpNewCustomerForm() throws Exception {
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(organisation_name, "general");
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(client_segment,
				"Cemetery trusts");
		// SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(org_category,
		// "Department of Health");
		// SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(org_type,
		// "Core");
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(govt_dept,
				"Department of Health");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(paying_org_text, "Automation School");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(org_category, "Department of Health");
		SeleniumFunc.waitFor(2);
		
		SeleniumFunc.xpath_GenericMethod_Sendkeys(estimated_revenue, "2000");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(phone_number, "7896875476");
		
		SeleniumFunc.xpath_GenericMethod_Click(Click);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(add_line1_org, "Street");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(add_line2_org, "Cross");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(suburb, "Chatswood");
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(state,
				"New South Wales");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(zip_code, "8978");
		SeleniumFunc.xpath_GenericMethod_Click(create_org_button);
		SeleniumFunc.waitFor(2);

	}

	public void createNewCustomer() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(create_new_customer);
		SeleniumFunc.waitFor(1);
	}

	public void createGeneralServiceRequest(String enquiryarea, String enquirycat, String thankyoumsg,
			String statustobechecked) throws Exception {

		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(enquiry_area,
				enquiryarea);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(enquiry_category,
				enquirycat);
		SeleniumFunc.xpath_GenericMethod_Click(urgency_no);
		SeleniumFunc.xpath_GenericMethod_Click(resolved_in_call_yes);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(adv_description, "general");
		SeleniumFunc.xpath_GenericMethod_Click(select_row_button);
		waitFor(3);
		String thankyou = xpath_Genericmethod_getElementText(thankyou_message_general_service_case);
		String status = xpath_Genericmethod_getElementText(general_enquiry_case_status);

		System.out.println("Thank u msg : " + thankyou);
		System.out.println("status of general service request : " + statustobechecked);
		 Assert.assertTrue(thankyou.equals(thankyoumsg));
		 Assert.assertTrue(status.equals(statustobechecked));

		SeleniumFunc.xpath_GenericMethod_Click(general_enquiry_case_confirm);
		waitFor(2);
		switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(wrapup);
		waitFor(1);
//		SeleniumFunc.xpath_GenericMethod_Click(test);
//		waitFor(2);

	}

	public void createGeneralServiceRequestNoPhone(String enquiryarea, String enquirycat, String thankyoumsg,
			String statustobechecked) throws Exception {

		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(enquiry_area,
				enquiryarea);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(enquiry_category,
				enquirycat);
		SeleniumFunc.xpath_GenericMethod_Click(urgency_no);
		SeleniumFunc.xpath_GenericMethod_Click(resolved_in_call_no);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(adv_description, "general");
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(
				preferred_response_channel, "Email");
		// SeleniumFunc.xpath_GenericMethod_Sendkeys(Email_enq,"abc@gmail.com");
		SeleniumFunc.xpath_GenericMethod_Click(select_row_button);
		waitFor(3);
		String thankyou = xpath_Genericmethod_getElementText(thankyou_message_general_service_case);
		String status = xpath_Genericmethod_getElementText(general_enquiry_case_status);

		System.out.println("Thank u msg : " + thankyou);
		System.out.println("status of general service request : " + statustobechecked);
		// Assert.assertTrue(thankyou.equals(thankyoumsg));
		// Assert.assertTrue(status.equals(statustobechecked));

		SeleniumFunc.xpath_GenericMethod_Click(general_enquiry_case_confirm);
		waitFor(2);
		switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(wrapup);
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(wrapup_confirm_popup);
		waitFor(2);

	}

	public void logoff() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(logoff_icon);
		waitFor(2);
		try {
			SeleniumFunc.xpath_GenericMethod_Click(logoff);
		} catch (NotFoundException e) {
			SeleniumFunc.xpath_GenericMethod_Click(logoff_icon);
			waitFor(2);
			SeleniumFunc.xpath_GenericMethod_Click(logoff);
		}

		waitFor(3);
		SeleniumFunc.acceptTheAlert();
		SeleniumFunc.acceptTheAlert();
		SeleniumFunc.clearCache();
	}

	public void claimEnterBusiness() throws Exception {
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(zipcode, "5001");
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(Text_box, "MLP1Regression1");
//		SeleniumFunc.xpath_GenericMethod_Sendkeys(Text_box, "ClaimOrgRegressionMLP2");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Text_box, "MLP2Regression1");
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(find_button);
		waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(select_business_checkbox);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		waitFor(6);
		SeleniumFunc.xpath_GenericMethod_Click(select_contact_checkbox);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);

	}

	public void OrganisationnameFromCleintAdviser(String OrgName) throws Exception {

		SeleniumFunc.xpath_GenericMethod_Sendkeys(Text_box, OrgName);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(find_button);
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(select_business_checkbox);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(select_contact_checkbox);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);

	}

	public void Selectpolicy() throws Exception {
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(policy_number_1);
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(point_1);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(policytobeselected_1);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Finish_1);

	}

	public void SelectPolicyUsingPolicyNumber(String value) throws Exception {
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click("//span[text()='"+value+"']/ancestor::td[1]/preceding-sibling::td/div/input[@type='radio']");
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(Finish_1);
	}


	public void claimEnterBusinessUsingOrgname(String orgname) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(org_name_adviser, orgname);

		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(find_button);
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(select_business_checkbox);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(select_contact_checkbox);
		SeleniumFunc.xpath_GenericMethod_Click(continue_button);

	}

	public void claimEnterBusiness_Traige() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Zip_code_Triage, "2601");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(Text_Box_Triage, "Lowannaa");

		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Find_Button_Triage);
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(Business_checkBox_Traige);
		// SeleniumFunc.xpath_GenericMethod_Click(continue_button);
		waitFor(1);
		SeleniumFunc.executeScript("arguments[0].click()", driver.findElement(By.xpath(Continue_Triage)));
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(select_contact_checkbox_Triage);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Continue_Triage);

	}
	
	public void UpdatetheOrganizationname(String UpdateOrgName, String changeName) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(imgOrgUpdate);
		waitFor(10);
		SeleniumFunc.switchToDefaultContent();
		try {
			SeleniumFunc.xpath_GenericMethod_Click(updateOrganisation_name);
			SeleniumFunc.xpath_GenericMethod_Clear(updateOrganisation_name);
		} catch (Exception e) {
			SeleniumFunc.xpath_GenericMethod_Click(updateOrganisation_name);
			SeleniumFunc.xpath_GenericMethod_Clear(updateOrganisation_name);
		}
		SeleniumFunc.switchToDefaultContent();
		try {
			
			SeleniumFunc.xpath_GenericMethod_Sendkeys(updateOrganisation_name, UpdateOrgName);
		} catch (Exception e) {
			
			SeleniumFunc.xpath_GenericMethod_Sendkeys(updateOrganisation_name, UpdateOrgName);
		}
		waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Sendkeys(updateOrganisation_name, UpdateOrgName);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(drpNameChangereson,
				changeName);
		waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(ClientSubmit);
		waitFor(3);
		
	}

	public void verify_the_succesful_msg_for_Organization() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		waitFor(3);
		SeleniumFunc.xpath_Genericmethod_VerifyTextContains(mesgsuccessful, "Your change of organisation name has been successful");
		
	}

	public void updated_organization_and_verify_the_updated_org_name(String UpdateOrgName) throws Exception {
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(drpOrg,
				"Organisations");
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtOrgSerach, UpdateOrgName);
		SeleniumFunc.xpath_GenericMethod_Click(btnSearch);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(btnOrgthreedot);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(optnStartSearch);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(lnkOrganizationDetails);
		waitFor(2);
		SeleniumFunc.xpath_Genericmethod_VerifyTextContains(txtupdatedOrgname, UpdateOrgName);
		
		
	}

	public void UpdatetheOrganizationnameandprimarycontact(String updateOrag, String changeReason,
			String primarycontact) throws Exception {
		waitFor(10);
		SeleniumFunc.xpath_Genericmethod_VerifyTextEquals(txtStatus, "Active");
		
		SeleniumFunc.xpath_GenericMethod_Click(imgOrgUpdate);
		
		SeleniumFunc.switchToDefaultContent();
		try {
			SeleniumFunc.xpath_GenericMethod_Click(updateOrganisation_name);
			SeleniumFunc.xpath_GenericMethod_Clear(updateOrganisation_name);
		} catch (Exception e) {
			SeleniumFunc.xpath_GenericMethod_Click(updateOrganisation_name);
			SeleniumFunc.xpath_GenericMethod_Clear(updateOrganisation_name);
		}
		SeleniumFunc.switchToDefaultContent();
		try {
			
			SeleniumFunc.xpath_GenericMethod_Sendkeys(updateOrganisation_name, updateOrag);
		} catch (Exception e) {
			
			SeleniumFunc.xpath_GenericMethod_Sendkeys(updateOrganisation_name, updateOrag);
		}
		waitFor(2);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Sendkeys(updateOrganisation_name, updateOrag);
		SeleniumFunc.xpath_GenericMethod_selectFromDropdownUsingVisibleTextbyclickingOnDropdown(drpNameChangereson,
				changeReason);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(primaryContact, primarycontact);
		SeleniumFunc.xpath_GenericMethod_Click(Click_On_Transfer);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(SecondryConact, "TriageClaimsOfficer1R3");
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Click_On_Transfer);
		SeleniumFunc.switchToDefaultContent();
		SeleniumFunc.xpath_GenericMethod_Click(ClientSubmit);
		waitFor(3);
	}

	public void Bell_nofication_and_select_update_Oraganization_notification() throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(imgBellNotification);
		SeleniumFunc.xpath_GenericMethod_Click(lnkNotification);
		
		
	}

	public void provide_required_information_before_approval_from_client_officer(String aprrovalNote,String updateOrag) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtApprovalNote, aprrovalNote);
		SeleniumFunc.xpath_GenericMethod_Click(btnAprrove);
		waitFor(5);
		SeleniumFunc.xpath_Genericmethod_VerifyTextContains(txtPrivousName, updateOrag);
		
	}
	public void selectCaseFromMyTasksTab() throws Exception {
		waitFor(1);
		SeleniumFunc.scrollDownVertically();
		waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(tabMyCases);
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(imgCaseNumberFilter);
		waitFor(2);
//		TestBase.currentCaseID = "E-21861";
		SeleniumFunc.xpath_GenericMethod_Sendkeys(txtPopupFilterTextbox, testdata.getCaseId());
		waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(Filter_Button_Apply);
		waitFor(5);
		SeleniumFunc.xpath_GenericMethod_Click("//a[text()='" + testdata.getCaseId() + "']");

	}

	public void AllProductInSegment() throws Exception {
		SeleniumFunc.switchToDefaultContent();
		waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(All_product_Segment_tab);
	}

	public void clickAnyOption(String decision) throws Exception {
		if(decision.equalsIgnoreCase("Yes"))
		{
			SeleniumFunc.xpath_GenericMethod_Click(Reapply_Yes);
		}
		else {
			SeleniumFunc.xpath_GenericMethod_Click(Reapply_No);
		}
	}

	public void ReapplyPolicy(String decision) throws Exception {
		if(decision.equalsIgnoreCase("Yes"))
		{
			SeleniumFunc.xpath_GenericMethod_Click(Reapply_Yes);
		}
		else {
			SeleniumFunc.xpath_GenericMethod_Click(Reapply_No);
		}
		SeleniumFunc.xpath_GenericMethod_Click(ClientSubmit);
	}

	public void notePaywayCaseID() throws Exception{
		SeleniumFunc.waitFor(5);
//		SeleniumFunc.waitFor(50);
		String paywayText = SeleniumFunc.getElementText("//div[contains(text(),'finance team to issue payway account number')]");
		Pattern p = Pattern.compile("[A-Z]{2}-([0-9]){4}");
		Matcher m = p.matcher(paywayText);
		m.matches();
		if(m.find()) {
			System.out.println("Control comes here");
			TestBase.paywayNumber.add(m.group(0));
			System.out.println("Payway number"+m.group(0));
		}
	}
	public void searchOrgByClientSegmentAndOrgName(String clientSegment,String BundleType,String orgname) throws Exception {
		SeleniumFunc.waitFor(7);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(create_policyBundle_clientSegment,clientSegment);
		SeleniumFunc.waitForElementToClikable(By.name(bundleType),20);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(bundleType,BundleType);
		enterOrgnameAndSelectBundlePolicy(orgname);
//		validateProductList(BundleType);
		SeleniumFunc.click(By.xpath(continue_button));
	}

	public void enterOrgnameAndSelectBundlePolicy(String orgname) throws Exception {
		SeleniumFunc.waitFor(6);
//		testdata.setOrgname("Csoes-3439");
		SeleniumFunc.name_GenericMethod_Sendkeys(create_policyBundle_Orgname,orgname);
		SeleniumFunc.enterValue(By.name(create_policyBundle_Orgname),Keys.TAB);
		SeleniumFunc.waitFor(3);
		SeleniumFunc.xpath_GenericMethod_Click(create_policyBundle_find);
//		testdata.setOrgname("Csoes-6735");
		SeleniumFunc.waitForElementToClikable(By.xpath("//label[text()='"+orgname+"']"),15);
		SeleniumFunc.xpath_GenericMethod_Click("//label[text()='"+orgname+"']");

	}

	public void enterEffectiveDate() throws Exception{

		SeleniumFunc.enterValue(BundlePolicy_PolicyEffectiveDate,SeleniumFunc.getCurrentDate());
	}

	public void notePackageCaseID() throws Exception{

		String PackageCaseID = SeleniumFunc.getElementText("//td[@data-attribute-name='Package case']/div/span/a");
		System.out.println(PackageCaseID);
		testdata.setCaseId(PackageCaseID);
	}

	public void closeCurrentTab() throws Exception{
		try{
			SeleniumFunc.click(tabCloseButton);
		}catch(Exception e){

		}
		SeleniumFunc.waitFor(3);

	}

	//Portfolio Manager
	public void selectManageComponentsAndGoToSolicitor() throws Exception{
		SeleniumFunc.xpath_GenericMethod_Click(Geo_Hamburger);
		SeleniumFunc.waitFor(1);
		SeleniumFunc.xpath_GenericMethod_Click(manageComponents);
		SeleniumFunc.xpath_GenericMethod_Click(vmiaSolicitor);
		SeleniumFunc.name_GenericMethod_Click(addVmiaSolicitor);
	}

	public void addSolicitorDetails(String Address) throws Exception {
		String ranNumb = generateRandomnumber();
		SeleniumFunc.name_GenericMethod_Sendkeys(s_name, "AshtonAgar"+ranNumb);
		solicitorName= "AshtonAgar"+ranNumb;
		SeleniumFunc.xpath_GenericMethod_Click(onPanelBtn);
		SeleniumFunc.xpath_GenericMethod_Click(claimTypeImg);
		SeleniumFunc.xpath_GenericMethod_Click(claimTypeOptions);
		SeleniumFunc.xpath_GenericMethod_Click(claimTypeDD);
		SeleniumFunc.name_GenericMethod_Sendkeys(s_address, Address);
		SeleniumFunc.waitFor(2);
		SeleniumFunc.xpath_GenericMethod_Click(f_address);
	}

	public void solicitorsList(String Name,String Email,String PhoneNumber,String Personnel) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(solAddList);
		SeleniumFunc.name_GenericMethod_Sendkeys(solName,Name);
		SeleniumFunc.name_GenericMethod_Sendkeys(solEmail,Email);
		SeleniumFunc.name_GenericMethod_Sendkeys(solNumber,PhoneNumber);
		SeleniumFunc.name_SelectFromDropdownUsingVisibleText(solPersonalDD,Personnel);
		SeleniumFunc.xpath_GenericMethod_Click(solServiceImg);
		SeleniumFunc.xpath_GenericMethod_Click(serviceOption);
		SeleniumFunc.xpath_GenericMethod_Click(datereview);
		SeleniumFunc.xpath_GenericMethod_Click(todayLink);
		SeleniumFunc.xpath_GenericMethod_Click(submitSol);
	}

	public void updateTheStatus(String status, String option) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(nameFilterIcon);
		SeleniumFunc.xpath_GenericMethod_Click(searchTextField);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(searchTextField,solicitorName);
		SeleniumFunc.xpath_GenericMethod_Click(applyBtn);
		SeleniumFunc.waitFor(2);
		try{
			SeleniumFunc.xpath_GenericMethod_Click(actionsLink);
		}catch(Exception e){
			SeleniumFunc.clickElementUsingActions(By.xpath(actionsLink));
		}
		SeleniumFunc.xpath_GenericMethod_Click(editSolicitor);
		SeleniumFunc.xpath_GenericMethod_Click("//*[@name='$PLegalPanelPg$pAttorneyStatus']/following-sibling::label[text()='"+status+"']");
		SeleniumFunc.xpath_GenericMethod_Click("//*[@name='$PLegalPanelPg$pOnPanel']/following-sibling::label[text()='"+option+"']");
		SeleniumFunc.xpath_GenericMethod_Click(submitSol);
	}

	public void verifyAndValidateRecordInInactiveTab(String actual) throws Exception {
		SeleniumFunc.xpath_GenericMethod_Click(inactiveStatus);
		SeleniumFunc.waitFor(2);
		try{
			SeleniumFunc.xpath_GenericMethod_Click(nameFilterIcon);
		}catch(Exception e){
			SeleniumFunc.clickElementUsingActions(By.xpath(nameFilterIcon));
		}
		SeleniumFunc.xpath_GenericMethod_Click(searchTextField);
		SeleniumFunc.xpath_GenericMethod_Sendkeys(searchTextField,solicitorName);
		SeleniumFunc.xpath_GenericMethod_Click(applyBtn);
		SeleniumFunc.waitFor(2);
		String expected = SeleniumFunc.getElementText(onPanel);
		System.out.println("Actual*********** "+actual);
		System.out.println("Expected********* "+expected);
		Assert.assertEquals(expected,actual);
	}


}
