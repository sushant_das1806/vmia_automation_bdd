package com.cucumber.framework.PageObjects;

public interface CemetryClaimsLoc {
	
	String interment_holder_name = "//input[@data-test-id='202009032345240225167']";
	String interment_last_name = "//input[@data-test-id='20200903234524022614']";	
	String contact_first_name = "//input[@data-test-id='202009032024080212319']";
	String contact_last_name = "//input[@data-test-id='202009032029500226597']";
	String address_lookup = "//input[@data-test-id='202008121730090479503']";
	String address_select = "//span[text()='Victoria Hotel, 82 Queen Street, BARRABA  NSW 2347']";	
	String contact_phone_preferred = "//input[@data-test-id='202009032039150218831']";
	String interment_Address1="//input[@id='c473344a']";
	String interment_subrb="//input[@id='33196fe9']";
	String interment_postcode="//input[@id='f753044b']";
	String interment_State="//select[@id='9a2e11fb']";
	String interment_contactphone="//input[@id='f557045a']";
	String interment_email="//input[@id='43592c09']";
	String interment_hyperlnk="//a[@name='EditAddressDetails_pyWorkPage.ClaimData.NoticeData.IntermentDetails.ContactAddressDetails_7'][@title='Cannot find your address']";
	String incident_first_name = "//input[@data-test-id='202009031308360235536']";
	String incident_last_name = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pInterredSurname']";
	String incident_location = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pIncidentLocation']";
	String plot_number = "//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pPlotNumber']";
	String how_damage = "//textarea[@data-test-id='202009031350040635242']";
	String amount_claimed = "//input[@data-test-id='202009031425040306951']";
	
	String how_damage_property = "//span[@class='textAreaExpandStyle ' ]//textarea[@id='f4dc6506']";
	String DOID = "//input[@data-test-id='2016072109335505834280']";
	
	String cyber_incident_response_no =	"//input[@name='$PpyWorkPage$pClaimData$pNoticeData$pReportedToCyberIncidentResponseService']//following-sibling::label[text()='No']";
	
	String date_time_of_incident="//img[@name='CalendarImg-8a4cf5ef']";
	String what_incident = "$PpyWorkPage$pClaimData$pNoticeData$pGeneralLossAnswer2";
	String action_incident = "$PpyWorkPage$pClaimData$pNoticeData$pActionsTaken";
	
	String claim_log = "//label[text()='Claim']";
	String notification_log = "//label[text()='Notification']";
	
	String date_combined_liabiliy ="//img[@data-test-id='202009090854440345629-DatePicker']";
	String incident_liab = "//textarea[@data-test-id='202009241626110507588']";
	String name_of_claimant = "//input[@data-test-id='202009241632050280976']";
	String addrees_lookup_liab = "//a[@data-test-id='202008131131110964292']";
	
}
