package com.cucumber.framework.context;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class TestData {

    String ClaimUnitID = StringUtils.EMPTY;
    String ClaimFileID = StringUtils.EMPTY;
    String PolicyNumber = StringUtils.EMPTY;
    String ClaimNumber=StringUtils.EMPTY;
    String recoveryid=StringUtils.EMPTY;
    String AssetID=StringUtils.EMPTY;
    String Orgname=StringUtils.EMPTY;
    String attachmentDocname=StringUtils.EMPTY;

    public String getOrgnization() {
        return Orgnization;
    }

    public void setOrgnization(String orgnization) {
        Orgnization = orgnization;
    }

    String Orgnization=StringUtils.EMPTY;


    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    WebDriver driver;
    long policyduration;
    String[] csoeClsBundleProducts=new String[]{"Directors and Officers Liability incorporating Entity Liability","Entity Fidelity","Group Personal Accident","Professional Indemnity","Public & Products Liability"};
    String[] csoBundleProducts=new String[]{"Directors and Officers Liability","Entity Fidelity","Medical Indemnity","Personal Accident","Professional Indemnity","Property - Volunteer Care Givers Property Damage","Public & Products Liability"};
    String[] AUSWideBundleProducts=new String[]{"Auswide - Directors and Officers Liability","Auswide - Entity Fidelity","Auswide - Medical Indemnity","Auswide - Personal Accident","Auswide - Professional Indemnity","Auswide - Property - Volunteer Care Givers Property Damage","Auswide - Public & Products Liability"};
    String[] csoBundleProductsPolicies=new String[]{"Directors and Officers Liability","Entity Fidelity","Medical Indemnity","Personal Accident","Professional Indemnity","Public & Products Liability","Property - Volunteer Care Give..."};
    String[] AUSWideBundleProductsPolicies=new String[]{"Auswide - Directors and Officers Liability","Auswide - Entity Fidelity","Auswide - Medical Indemnity","Auswide - Personal Accident","Auswide - Professional Indemnity","Auswide - Property - Volunteer Care Givers Property Damage","Property - Volunteer Care Givers Property Damage","Auswide - Public & Products Liability","Auswide - Public & Products Li...","Auswide - Property - Volunteer...","Auswide - Directors and Office..."};
    String[] EmRePSsBundleProductPolicies = new String[]{"Directors & Officers - EmRePSS","Motor - EmRePSS","Professional Indemnity - EmRePSS","Property - EmRePSS","Public & Products Liability - ..."};

    public String getOrgname() {
        return Orgname;
    }
    public void setOrgname(String orgname) {
        Orgname = orgname;
    }

    public List getCSOBundleProducts(String BundleProducts ){
        List<String> products;
        if(BundleProducts.equalsIgnoreCase("CSO (Auswide)")) products=new ArrayList<String>(Arrays.asList(AUSWideBundleProducts));
        else if(BundleProducts.equalsIgnoreCase("EmRepS")) products=new ArrayList<String>(Arrays.asList(EmRePSsBundleProductPolicies));
        else if(BundleProducts.equalsIgnoreCase("CSOE (CLS)")) products=new ArrayList<String>(Arrays.asList(csoeClsBundleProducts));
        else products=new ArrayList<String>(Arrays.asList(csoBundleProducts));
        return products;
    }

    public List getCSOBundleProductsPolicies(String BundleProducts ){
        List<String> products;
        products=new ArrayList<String>(Arrays.asList(csoBundleProductsPolicies));
       if(BundleProducts.equalsIgnoreCase("AuswideValidate"))
            products.addAll(Arrays.asList(AUSWideBundleProductsPolicies));
       else if (BundleProducts.equalsIgnoreCase("EmRePsValidate"))
           products.addAll(Arrays.asList(EmRePSsBundleProductPolicies));
            return products;
    }




    public String getPolicyAction() {
        return policyAction;
    }

    public void setPolicyAction(String policyAction) {
        this.policyAction = policyAction;
    }

    String policyAction=StringUtils.EMPTY;


    HashMap<String,String> data=new HashMap<>();
    private String caseId=null;
    private String endorseId=null;

    public String getPolicynumber() {
        return policynumber;
    }

    public void setPolicynumber(String policynumber) {
        this.policynumber = policynumber;
    }

    private String policynumber;


    public long getPolicyduration() {
        return policyduration;
    }

    public void setPolicyduration(long policyduration) {
        this.policyduration = policyduration;
    }

    public String getData(String key){
        if(!key.isEmpty())
        return data.get(key);
        else return null;
    }

    public void setData(String key,String value){
        data.put(key,value);
    }

    public String getCaseId(){
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
        setData("caseId",caseId);
    }

    public String getEndorseId(){
        return endorseId;
    }

    public void setEndorseId(String endorseId) {
        this.endorseId = endorseId;
        setData("endorseId",endorseId);
    }

    public HashMap<String, String> getDataMap() {
       return data;
    }


}
