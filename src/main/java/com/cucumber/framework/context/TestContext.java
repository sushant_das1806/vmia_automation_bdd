package com.cucumber.framework.context;

import com.cucumber.framework.PageObjects.*;
import com.cucumber.framework.TestBase.TestBase;
import com.cucumber.framework.stepDef.GeneralStepDefinition;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class TestContext {

    WebDriver driver;
    UW uw;
    Claims claims;
    LoginPage loginPage;
    LogoutPage logoutPage;
    HomePage homePage;
    ClientAdviser clientAdviser;
    ClaimReviewHomePage claimReviewHomePage;
    ViewOrgDetails viewOrgDetails;
    FinalisequotePage finalisequotePage;
    GeneralStepDefinition generalStepDefinition;
    TestData data;
    Finance finance;

    public UW getUnderWritingPage(){
        return uw==null?uw=new UW(TestBase.getDriver(),getData()):uw;
    }

    public Claims getClaimsPage(){
        return claims==null?claims=new Claims(getData().getDriver(),getData()):claims;
    }

    public LoginPage getLoginPage(){
        return loginPage==null?loginPage=new LoginPage(getData().getDriver(),getData()):loginPage;
    }

    public LogoutPage getLogOutPage(){
        return logoutPage==null?logoutPage=new LogoutPage(getData().getDriver(),getData()):logoutPage;
    }

    public HomePage getHomePage(){
        return homePage==null?homePage=new HomePage(getData().getDriver(),getData()):homePage;
    }

    public ClientAdviser getClientAdviserPage(){
        return clientAdviser==null?clientAdviser=new ClientAdviser(getData().getDriver(),getData()):clientAdviser;
    }

    public ClaimReviewHomePage getClaimReviewHomePage(){
        return claimReviewHomePage==null?claimReviewHomePage=new ClaimReviewHomePage(getData().getDriver(),getData()):claimReviewHomePage;
    }

    public ViewOrgDetails getViewOrgDetailsPage(){
        return viewOrgDetails==null?viewOrgDetails=new ViewOrgDetails(getData().getDriver(),getData()):viewOrgDetails;
    }

    public FinalisequotePage getFinalisequotePage(){
        return finalisequotePage==null? finalisequotePage=new FinalisequotePage(getData().getDriver(),getData()):finalisequotePage;
    }

    public TestData getData(){
        return data==null?data=new TestData():data;
    }

    public Finance getFinance(){
        return finance==null? finance=new Finance(getData().getDriver(), getData()):finance;
    }

}
