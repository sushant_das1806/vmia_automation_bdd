package com.cucumber.framework.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(

        glue = { "com.app.stepdefinition",
                "classpath:com.cucumber.framework.stepDef",
                "classpath:com.cucumber.framework.PageObjects",
                "classpath:com.cucumber.framework.TestBase",
                "classpath:com.cucumber.framework.helper"},
        // path of step definition

        plugin = { "pretty",
                "html:./reports/cucumber-reports/cucumber-html/index.html",
                "rerun:target/failedrerun.txt"},
        monochrome =true,
        features = { "@target/failedrerun.txt" }

)

//public class FailedRun extends AbstractTestNGCucumberTests{}
public class testRunner extends AbstractTestNGCucumberTests {
}
