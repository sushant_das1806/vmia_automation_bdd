package com.cucumber.framework.runner;

import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;

@CucumberOptions(

        features = {"classpath:featurefiles",},
//        tags = "@RegressionMLP5Set1","@Verified","@RegressionMLP1 ", "@RegMLP5Set1","@mlp5set02", "@RegressionClaims5", "@RegressionClaimMLP5Set2",
        tags = "@testeri",
        glue = {
                "classpath:com.cucumber.framework.stepDef",
                "classpath:com.cucumber.framework.PageObjects",
                "classpath:com.cucumber.framework.TestBase",
                "classpath:com.cucumber.framework.helper",
        },

        plugin = {"html:target/Sprint9/RegressionMLP5Set1/bility Single policy application and endorse and then Cancel Policy.html",
                "json:target/cucumber-report/cucumber.json",
                "pretty:target/cucumber-pretty.txt",
                "pretty","html:target/cucumber-reports",
                "junit:target/cucumber-results.xml",
//                "rerun:target/failedrerun.txt",
//                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:target/cucumber-reports/report.html"
        },

        monochrome = true,

        dryRun = false

)

public class MLPRunner extends AbstractTestNGCucumberTests {
}
