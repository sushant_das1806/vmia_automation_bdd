package com.cucumber.framework.runner;


import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		
features = {"classpath:featurefiles/Sanity",
		
		
           }, 
//tags = {"@ValidLogin,@NewApplication"},
glue = {
		"classpath:com.cucumber.framework.stepDef",
		"classpath:com.cucumber.framework.PageObjects",
		"classpath:com.cucumber.framework.TestBase",
		"classpath:com.cucumber.framework.helper",
		}, 

plugin = {"html:target/cucumber-html-report",
		"json:target/cucumber-report/cucumber.json",
		"pretty:target/cucumber-pretty.txt",
		"junit:target/cucumber-results.xml"
		//"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:target/cucumber-reports/report.html"
		},

monochrome = true,

dryRun = false



)
public class SanityRunner extends AbstractTestNGCucumberTests {
	/*@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig(new File(ObjectRepo.reader.getReportConfigPath()));
	}*/

}



