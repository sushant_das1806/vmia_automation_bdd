package com.cucumber.framework.TestBase;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import com.cucumber.framework.GeneralHelperSel.ExcelUtil;
import com.cucumber.framework.GeneralHelperSel.SeleniumFunc;
import com.cucumber.framework.context.TestContext;
import com.cucumber.framework.context.TestData;
import com.cucumber.framework.stepDef.ClaimsCommonStepDef;
import com.cucumber.framework.utility.ResourceHelper;
import io.cucumber.java.*;
import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
import org.junit.Test;
import org.openqa.selenium.*;

import com.cucumber.framework.configreader.ObjectRepo;
import com.cucumber.framework.configreader.PropertyFileReader;
import com.cucumber.framework.configuration.browser.BrowserType;
import com.cucumber.framework.configuration.browser.ChromeBrowser;
import com.cucumber.framework.configuration.browser.FirefoxBrowser;
import com.cucumber.framework.configuration.browser.HtmlUnitBrowser;
import com.cucumber.framework.configuration.browser.IExploreBrowser;
//import com.cucumber.framework.helper.Logger.LoggerHelper;


//import cucumber.api.Scenario;
import io.cucumber.java.en.*;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;

import javax.swing.*;

public class TestBase {

	//private final Logger log = LoggerHelper.getLogger(TestBase.class);
	public static ThreadLocal<WebDriver> dr = new ThreadLocal<WebDriver>();
	private  WebDriver driver=null;
	TestContext context;
	ExcelUtil excelUtil;

	public static String scenarioname = StringUtils.EMPTY;
	public static String currentCaseID = StringUtils.EMPTY;
	public static String currentClaimUnitID = StringUtils.EMPTY;
	public static String currentClaimFileID = StringUtils.EMPTY;
	public static String currentPolicyNumber = StringUtils.EMPTY;
	public static String currentClaimNumber=StringUtils.EMPTY;
	public static String recoveryid=StringUtils.EMPTY;
	public static String AssetID=StringUtils.EMPTY;
	public static String Orgname=StringUtils.EMPTY;
	public static String attachmentDocname=StringUtils.EMPTY;
	public static long policyduration;
	public static List<String> paywayNumber = new ArrayList<String>();
	public static int i = 0;


	
	public  WebDriver getBrowserObject(BrowserType bType) throws Exception {
		
		try {
		
			//log.info(bType);

			switch (bType) {

			case Chrome:
				ChromeBrowser chrome = ChromeBrowser.class.newInstance();
				return chrome.getChromeDriver(chrome.getChromeCapabilities());

			case Firefox:
				FirefoxBrowser firefox = FirefoxBrowser.class.newInstance();
				return firefox.getFirefoxDriver(firefox.getFirefoxCapabilities());

			case Edge:
					System.setProperty("webdriver.edge.driver", ResourceHelper.getResourcePath("/src/main/resources/drivers/msedgedriver.exe"));
					EdgeDriver edgeDriver = new EdgeDriver();
					return edgeDriver;

				case HtmlUnitDriver:
				HtmlUnitBrowser htmlUnit = HtmlUnitBrowser.class.newInstance();
//				return htmlUnit.getHtmlUnitDriver(htmlUnit.getHtmlUnitDriverCapabilities());

			case Iexplorer:
				IExploreBrowser iExplore = IExploreBrowser.class.newInstance();
				return iExplore.getIExplorerDriver(iExplore.getIExplorerCapabilities());
			default:
				throw new Exception(" Driver Not Found : " + new PropertyFileReader().getBrowser());
			}
			
		} catch (Exception e) {
			//log.equals(e);
			throw e;
		}
		
		
	}

	public TestBase(TestContext context){
		this.context=context;
	}
	
	public void setUpDriver(BrowserType bType) throws Exception {
		
		System.out.println("Driver Before instantiation is: "+ driver);
		driver = getBrowserObject(bType);
		//driver.manage().window().setSize(new Dimension(1920,1080));
		setWebDriver(driver);
		System.out.println("Driver After instantiation is: "+ driver);
	//	log.debug("InitializeWebDrive : " + driver.hashCode());
		//driver.manage().timeouts().pageLoadTimeout(ObjectRepo.reader.getPageLoadTimeOut(), TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(ObjectRepo.reader.getImplicitWait(), TimeUnit.SECONDS);
		driver.manage().window().maximize();
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("document.body.style.transform='scale(0.5)';");
		
		 for(int i=0; i<8; i++){
			   driver.findElement(By.tagName("html")).sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
			  }
		 System.out.println("Java scripts executor");
		
		System.out.println("Height : "+driver.manage().window().getSize().getHeight());
		System.out.println("Width : "+driver.manage().window().getSize().getWidth());

	}
	
	
	@Before
	public void before() throws Exception {

		ObjectRepo.reader = new PropertyFileReader();
		if (Objects.equals("null",String.valueOf(System.getProperty("QA")))) setUpDriver(ObjectRepo.reader.getBrowser());
		else setUpDriver(BrowserType.valueOf(System.getProperty("Browser")));
		context.getData().setDriver(TestBase.getDriver());
	}


	@After
	public void after(Scenario scenario) throws Exception {
		 if (scenario.isFailed()) {
			 WebDriverWait wait = new WebDriverWait(driver, 10);
				 if(driver.getTitle().equalsIgnoreCase("interaction portal")){
					 SeleniumFunc.xpath_GenericMethod_Click("//i[@name='CPMInteractionPortalHeaderTopRight_pyDisplayHarness_1']");
					 SeleniumFunc.xpath_GenericMethod_Click("//span[text()='Logout']");
					 wait.until(ExpectedConditions.alertIsPresent());
					 SeleniumFunc.acceptTheAlert();
				 }else {
					 try {
						 SeleniumFunc.xpath_GenericMethod_Click("//a[@title='Logged in user name']");
					 }
					 catch (Exception e) {
						 SeleniumFunc.xpath_GenericMethod_Click("//a[@data-test-id='202004170924160508258']");
					 }
					 SeleniumFunc.xpath_GenericMethod_Click("//span[text()='Log off']");
				 }
	        }
		SeleniumFunc.waitFor(2);
		driver.quit();
	}
	
	
	public static WebDriver getDriver() {
        return dr.get();
    }
 
    public static void setWebDriver(WebDriver driver) {
        dr.set(driver);
    }
    
    @BeforeStep
    public static void beforeStep(Scenario scenario)
    {
    	scenarioname=scenario.getName();
    }

    @AfterStep
    public void afterStep(Scenario scenario) throws InterruptedException {
		SeleniumFunc.acceptTheAlert();
		Thread.sleep(1);
    	scenario.attach(((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.BYTES),"image/png" ,"");
    }

}

