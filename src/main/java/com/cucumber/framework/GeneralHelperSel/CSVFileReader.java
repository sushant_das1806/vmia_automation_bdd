package com.cucumber.framework.GeneralHelperSel;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CSVFileReader {

    public HashMap readCSV(String productname) throws Exception {

        String filePath=""+System.getProperty("user.dir")+"\\src\\main\\resources\\Attachments\\finaliseQuoateOptions.csv";
        Reader reader = new FileReader(filePath);
        CSVReader csvreader = new CSVReader(reader);
        List<String[]> readedData = csvreader.readAll();
        HashMap<String,String> data=new HashMap<>();
        for(int i=1;i<readedData.size();i++){
            if(readedData.get(i)[0].equalsIgnoreCase(productname)) {
                for(int j=0;j<readedData.get(0).length;j++)
                data.put(readedData.get(0)[j],readedData.get(i)[j]);
            }
        }
        return data;
    }

    public static int getLengthArray(String[] a){
        int lenght=0;
        for(int i=0;i<a.length;i++){
            if(!a[i].equalsIgnoreCase("")){
                System.out.println(a[i]);
                lenght++;
            }
        }
        return lenght;
    }


    public static String getOrgFromCSV(String productName,String type) throws IOException, CsvException {
        String orgType=null;
        List<String[]> readedData=readCSVFromFile(type+"ClientSegmentMapping");
        System.out.println("Complete size:-----> "+readedData.size());
        for(int i=1;i<readedData.size();i++) {
            if (readedData.get(i)[0].equalsIgnoreCase(productName)) {
                int t = getLengthArray(readedData.get(i));
                System.out.println("array length :-----> "+t);
                if (getLengthArray(readedData.get(i)) < 2) {
                    orgType = readedData.get(i)[1];
                    System.out.println("First Length:"+readedData.get(i).length);
                    break;
                } else {
                    int random=SeleniumFunc.generateRandomnumber(getLengthArray(readedData.get(i)));
                    System.out.println("Random number"+random);
                    orgType = readedData.get(i)[random];
                    System.out.println("Client Segment:"+orgType);
                    break;
                }
            }
        }

        return getOrgvalue(orgType,type);

    }

    public static String getOrgvalue(String orgType,String type) throws IOException, CsvException {
        List<String[]> OrgvalueData=readCSVFromFile("OrgnizationValue");
        System.out.println("Size is: "+OrgvalueData.size());
        for(int i=1;i<OrgvalueData.size();i++) {
            if (OrgvalueData.get(i)[0].trim().equalsIgnoreCase(orgType.trim()))

                if(type.equalsIgnoreCase("Insurance"))
                    return OrgvalueData.get(i)[1];
                if(type.equalsIgnoreCase("Claim"))
                    return OrgvalueData.get(i)[2];
        }
        return null;
    }

    public static List<String[]> readCSVFromFile(String fileName) throws IOException, CsvException {
        String filePath=""+System.getProperty("user.dir")+"\\src\\main\\resources\\TestData\\"+fileName+".csv";
        Reader reader = new FileReader(filePath);
        CSVReader csvreader = new CSVReader(reader);
        return csvreader.readAll();
    }


    public  List ReadAllDataAsListOfMap(String SheetName) throws Exception {

        String filePath=""+System.getProperty("user.dir")+"\\src\\main\\resources\\TestData\\"+SheetName+".csv";
        Reader reader = new FileReader(filePath);
        CSVReader csvreader = new CSVReader(reader);
        List<String[]> readedData = csvreader.readAll();
        List<HashMap<String,String>> data=new ArrayList<>();

        for(int i=1;i<readedData.size();i++) {
            HashMap<String,String> rowData=new HashMap<>();
            for (int j = 0; j < readedData.get(0).length; j++) {
                rowData.put(readedData.get(0)[j], readedData.get(i)[j]);
            }
            data.add(rowData);
        }
        return data;
    }


    public static List readDataOfOrgnization(String productname) throws Exception {

        String filePath=""+System.getProperty("user.dir")+"\\src\\main\\resources\\TestData\\ProductOrgMappingForRegression.csv";
        Reader reader = new FileReader(filePath);
        CSVReader csvreader = new CSVReader(reader);
        List<String[]> readedData = csvreader.readAll();
        List<String> orgname=new ArrayList<>();
        for(int i=1;i<readedData.size();i++){
            if(readedData.get(i)[0].equalsIgnoreCase(productname)) {
                for(int j=1;j<readedData.get(0).length;j++){
                    orgname.add(readedData.get(i)[j]);
                    System.out.println(readedData.get(i)[j]);}
            }
        }
        return orgname;
    }

    public static void main(String[] a) throws Exception {

//        System.out.println(getOrgFromCSV("Excess Liability","Insurance"));


//       System.out.println(str.get(SeleniumFunc.generateRandomnumber( str.size()-1)));
    }

}


