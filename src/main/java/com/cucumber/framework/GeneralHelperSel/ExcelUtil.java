package com.cucumber.framework.GeneralHelperSel;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

public class ExcelUtil {

     HashMap<String,String> testdata=new HashMap<>();
     XSSFWorkbook workbook;
     XSSFSheet sheet;
     int i;
     String filePath;


   public void ExcelUtil() throws IOException {
       filePath=""+System.getProperty("user.dir")+"\\src\\main\\resources\\Testoutput\\Testoutput.xlsx";
       workbook = new XSSFWorkbook(getFileInputStream(filePath));
       sheet = workbook.getSheetAt(0);
   }

    public HashMap<String, String> getTestdata(String scenario) throws IOException {

        for(i=0;i<=sheet.getLastRowNum();i++){
            if(scenario.equalsIgnoreCase(sheet.getRow(i).getCell(0).getStringCellValue())){
                for(int j=0;j<sheet.getRow(i).getLastCellNum();j++){
                    testdata.put(sheet.getRow(0).getCell(j).getStringCellValue().trim(),sheet.getRow(i).getCell(j).getStringCellValue().trim());
                }
                break;
            }
        }
        return testdata;
    }

    public void storeTestOutput(HashMap<String,String> data) throws IOException {
        filePath=""+System.getProperty("user.dir")+"\\src\\main\\resources\\Testoutput\\Testoutput.xlsx";
        workbook = new XSSFWorkbook(getFileInputStream(filePath));
        sheet = workbook.getSheetAt(1);
            System.out.println("Last row number: "+sheet.getPhysicalNumberOfRows());
            int lastrow=sheet.getPhysicalNumberOfRows();
            sheet.createRow(lastrow);
            sheet.getRow(lastrow).createCell(0).setCellValue(data.get("Orgnization"));
            sheet.getRow(lastrow).createCell(1).setCellValue(data.get("Product"));
            sheet.getRow(lastrow).createCell(2).setCellValue(data.get("caseId"));
            sheet.getRow(lastrow).createCell(3).setCellValue(data.get("PolicyNumber"));

        try{
        FileOutputStream file=new FileOutputStream(filePath);
        workbook.write(file);
        file.close();}
        catch (Exception e){
            e.printStackTrace();
        }
    }






    public static InputStream getFileInputStream(String filePath) throws IOException {
        return  Files.newInputStream(Paths.get(filePath));
    }
}

